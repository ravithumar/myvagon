<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\NewShipperRegister;
use App\Mail\ShipperEmailOTP;
use Illuminate\Support\Facades\Mail as FacadesMail;
use Mail;

class ShipperEmailOTPJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    public $code = "456789";

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
       
        $this->data = $data;
       
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $code = $this->code;
        
        $email = new ShipperEmailOTP($code);
        
        FacadesMail::to($this->data)->send($email, ['code' => $code]);
    }
}
