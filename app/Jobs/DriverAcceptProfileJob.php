<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\DriverAcceptProfile;
use Mail;

class DriverAcceptProfileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    public $name;
    public $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->name = $data['first_name'];
        $this->user_id = $data['id'];
       
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $name = $this->name;
        $user_id = $this->user_id;
        
        $email = new DriverAcceptProfile($name, $user_id);
        
        Mail::to($this->data['email'])->send($email, ['name' => $name, 'user_id', $user_id]);
    }
}
