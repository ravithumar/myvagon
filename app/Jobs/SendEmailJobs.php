<?php
namespace App\Jobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail as FacadesMail;
use Mail;
   
class SendEmailJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
  
    protected $data;
    public $link;
  
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
         
        $this->data = $data;
    }
   
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $link = $this->link;
        $email = new SendMail($link);
        
        FacadesMail::to($this->data)->send($email);
    }
}
?>