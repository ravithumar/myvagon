<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\DriverCommisionInvoiceEmail;
use Mail;

class DriverCommisionInvoiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    public $link;
    public $invoice_data;
    public $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->link = $data['link'];
        $this->invoice_data = $data['invoice_data'];
        $this->email = $data['email'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $link = $this->link;
        // $email = $this->email;
        $invoice_data = $this->invoice_data;
        
        $email = new DriverCommisionInvoiceEmail($link, $invoice_data);
        
        Mail::to($this->data['email'])->send($email, ['link' => $link, 'invoice_data', $invoice_data]);
    }
}
