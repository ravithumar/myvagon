<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\ShipperManagerCredential;
use Illuminate\Support\Facades\Mail as FacadesMail;
use Mail;

class ShipperManagerCredentialJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    public $username;
    public $password;
    public $email;
    public $company_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->email = $data['email'];
        $this->username = $data['email'];
        $this->password = $data['password'];
        $this->company_name = $data['company_name'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $username = $this->username;
        $password = $this->password;
        $company_name = $this->company_name;
        
        $email = new ShipperManagerCredential($username,$password,$company_name);
        // echo '<pre>';print_r($email);die();
        
        FacadesMail::to($this->email)->send($email, ['username' => $username,'password' => $password,'company_name' => $company_name]);
    }
}
