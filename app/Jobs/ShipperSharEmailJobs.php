<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\ShipperShareEmail;

use Mail;
class ShipperSharEmailJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

   protected $data;

   public $email_or_number;
   public $booking_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->email_or_number=$data['email_or_number'];
        $this->booking_id=$data['booking_id'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email_or_number=$this->email_or_number;
        $booking_id=$this->booking_id;
        
        $email = new ShipperShareEmail($email_or_number,$booking_id);
        
        Mail::to($this->data['email'])->send($email,['email_or_number' => $email_or_number,'booking_id'=>$booking_id]);
    }
}
