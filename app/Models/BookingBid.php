<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Users;

class BookingBid extends Model
{
    
    protected $table = 'booking_bid';

    public function driver(){
        return $this->belongsTo(User::class,'driver_id','id');
    }

    public function driver_vehicle(){
        return $this->belongsTo(DriverVehicle::class,'driver_id','user_id');
    }

}
