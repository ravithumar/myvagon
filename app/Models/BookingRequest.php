<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Users;

class BookingRequest extends Model
{
    
    protected $table = 'booking_request';
    protected $guarded = [];

    public function driver(){
        return $this->belongsTo(User::class,'driver_id','id');
    }

    protected $appends = ['time_difference'];

    public function getTimeDifferenceAttribute()
    {
        $time_diff = strtotime(date('Y-m-d H:i:s')) - strtotime($this->request_time);
        return $time_diff;
    }

    public function availability()
    {
        return $this->belongsTo(Availability::class,'availability_id','id');
    }

    public function booking_info()
    {
        return $this->belongsTo(Booking::class,'booking_id','id');
    }

}
