<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingLocations extends Model
{
    use HasFactory;

    protected $table = 'booking_locations';

    protected $fillable = [
        'truck_id ', 'booking_id','company_name', 'drop_location', 'drop_lat', 'drop_lng', 'is_pickup', 'note', 'delivered_at', 'created_at',
        'distance','journey','deadhead','otp','verify_otp','delivery_time_from','delivery_time_to'];

    protected $appends = ['products'];

     public function products()
    {
        return $this->hasMany(BookingProduct::class,'location_id','id');
    }

    public function getFullDateAttribute($delieved_at,$delivery_time_to){
        return $delieved_at." ".$delivery_time_to;
    }

    public function getProductsAttribute()
    {
        return BookingProduct::whereLocationId($this->id)->get();
    }

    public function getCompanyNameAttribute($company_name){
        return $company_name ? $company_name : "";
    }

    public function getDropLocationAttribute($drop_location){
        return $drop_location ? $drop_location : "";
    }

    public function getDropLngAttribute($drop_lng){
        return $drop_lng ? $drop_lng : "";
    }

    public function getDropLatAttribute($drop_lat){
        return $drop_lat ? $drop_lat : "0.00";
    }

    public function getDistanceAttribute($distance){
        return $distance ? $distance : "0";
    }

    public function getJourneyAttribute($journey){
        return $journey ? $journey : "0.00";
    }

    public function getDeliveryTimeFromAttribute($delivery_time_from){
        return $delivery_time_from ? $delivery_time_from : "";
    }
    
    public function getDeliveryTimeToAttribute($delivery_time_to){
        return $delivery_time_to ? $delivery_time_to : "";
    }

    public function getNoteAttribute($note){
        return $note ? $note : "";
    }

    public function getDeliveredAtAttribute($delivered_at){
        return $delivered_at ? $delivered_at : "";
    }
    
    public function getArrivedAtAttribute($arrived_at){
        return $arrived_at ? $arrived_at : "";
    }
    
    public function getStartLoadingAttribute($start_loading){
        return $start_loading ? $start_loading : "";
    }
    
    public function getStartJourneyAttribute($start_journey){
        return $start_journey ? $start_journey : "";
    }

    public function getCreatedAtAttribute($created_at){
        return $created_at ? date('Y-m-d H:i:s',strtotime($created_at)) : "";
    }

    public function getUpdatedAtAttribute($updated_at){
        return $updated_at ? date('Y-m-d H:i:s',strtotime($updated_at)) : "";
    }
}
