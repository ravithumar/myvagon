<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{
    use HasFactory;
    
    protected $table = 'bank_details';

    protected $fillable = [
        'user_id','iban', 'account_number', 'bank_name', 'country', 'created_at'];

}
