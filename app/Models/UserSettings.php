<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    use HasFactory;

    public $timestamps = false;


    protected $table = 'user_settings';

    protected $fillable = [
        'notification',
        'message',
        'bid_received',
        'bid_accepted',
        'load_assign_by_dispatcher',
        'start_trip_reminder',
        'complete_trip_reminder',
        'pdo_remider',
        'match_shippment_near_you',
        'match_shippment_near_delivery',
        'rate_shipper',

    ];


}
