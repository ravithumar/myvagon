<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $table = 'notification';

    protected $appends = ['date'];

    protected $guarded = [];

   public function getDateAttribute(){
        return date('Y-m-d',strtotime($this->created_at));
    }
}
