<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruckType extends Model
{
    use HasFactory;

    protected $table = 'truck_type';

    protected $fillable = [
        'truck_type_id',
        'name',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function TruckUnit(){
        return $this->belongsTo('App\Models\TruckUnit','unit_id', 'id');
    }


    public function category(){
        return $this->hasMany(TruckCategory::class, 'truck_type_id', 'id');
    }

    public function getUnitIdAttribute($unit_id){
        return $unit_id ? $unit_id : "";
    }

    public function getCapacityAttribute($capacity){
        return $capacity ? $capacity : "";
    }
     

}
