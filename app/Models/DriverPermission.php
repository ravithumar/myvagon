<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DriverPermission extends Model
{
    protected $table = 'driver_permissions';
    public $timestamps=false;

    protected $fillable = [
        'user_id',
        'search_loads',
        'my_loads',
        'my_profile',
        'setting',
        'statistics',
        'change_password',
        'allow_bid',
        'view_price',
        'post_availibility',
    ];

}
