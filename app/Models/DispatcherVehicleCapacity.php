<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DispatcherVehicleCapacity extends Model
{
    use HasFactory;

    protected $table='dispatcher_vehicles_capacity';
}
