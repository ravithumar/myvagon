<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    use HasFactory;

    protected $table = 'availability';

    public function trucks_type()
    {
        return $this->belongsTo(TruckType::class,'truck_type_id','id');
    }

    public function driver_info()
    {
        return $this->belongsTo(User::class,'driver_id','id');
    }

    public function booking_info()
    {
        return $this->belongsTo(Booking::class,'booking_id','id');
    }

    public function booking_request()
    {
        return $this->hasMany(BookingRequest::class,'availability_id','id');
    }
}
