<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingProduct extends Model
{
    use HasFactory;

    protected $table = 'booking_product';

    protected $fillable = [
        'booking_id', 'location_id', 'product_id', 'product_type', 'qty', 'weight', 'unit', 'note', 'is_fragile',
        'is_sensetive', 'is_pickup'];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function truck_unit()
    {
        return $this->belongsTo(TruckUnit::class,'unit','id');
    }

    public function getProductIdAttribute($product_id)
    {
        if(!empty($product_id)){
            return Product::select('id','name')->whereId($product_id)->first();
        }
    }

    public function getUnitAttribute($unit)
    {
        return TruckUnit::select('id','name')->whereId($unit)->first();
    }

    public function getProductTypeAttribute($product_type)
    {
        return PackageType::select('id','name')->whereId($product_type)->first();
    }

    public function getQtyAttribute($qty){
        return $qty ? $qty : "0";
    }
    
    public function getWeightAttribute($weight){
        return $weight ? $weight : "";
    }
    
    public function getNoteAttribute($note){
        return $note ? $note : "";
    }

    public function getCreatedAtAttribute($created_at){
        return $created_at ? date('Y-m-d H:i:s',strtotime($created_at)) : "";
    }

    public function getUpdatedAtAttribute($updated_at){
        return $updated_at ? date('Y-m-d H:i:s',strtotime($updated_at)) : "";
    }

}
