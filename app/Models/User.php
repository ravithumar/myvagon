<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function hasRole($user_id,$role)
    {
        $user = UserRole::whereUserId($user_id)->whereRoleId($role)->first();
        if(!empty($user))
        {
            return true;
        }
        return false;
    }

    protected $appends = ['shipper_rating'];

    public function shipper_permissions()
    {
        return $this->hasMany(ShipperPermission::class,'user_id','id');
    }
    public function driver_permissions()
    {
        return $this->hasMany(DriverPermission::class,'user_id','id');
    }

    public function getFirstNameAttribute($first_name){
        return $first_name ? $first_name : "";
    }
    public function getLastNameAttribute($last_name){
        return $last_name ? $last_name : "";
    }
    public function getProfileAttribute($profile){
        return $profile ? $profile : "";
    }
    public function getSelfDesAttribute($self_des){
        return $self_des ? $self_des : "";
    }
    public function getPhoneAttribute($phone){
        return $phone ? $phone : "";
    }
    public function getCompanyNameAttribute($company_name){
        return $company_name ? $company_name : "";
    }
    public function getPostcodeAttribute($post_code){
        return $post_code ? $post_code : "";
    }
    public function getCityAttribute($city){
        return $city ? $city : "";
    }
    public function getStateAttribute($state){
        return $state ? $state : "";
    }
    public function getCountryAttribute($country){
        return $country ? $country : "";
    }
    public function getSsnAttribute($ssn){
        return $ssn ? $ssn : "";
    }
    public function getCompanyPhoneAttribute($company_phone){
        return $company_phone ? $company_phone : "";
    }
   
    public function getEmailVerifyAttribute($email_verify){
        return $email_verify ? $email_verify : "";
    }
    public function getPhoneVerifyAttribute($phone_verify){
        return $phone_verify ? $phone_verify : "";
    }
    public function getEmailVerifiedAtAttribute($email_verified_at){
        return $email_verified_at ? $email_verified_at : "";
    }

    public function country_name()
    {
        return $this->belongsTo(Country::class,'country','id');
    }

    public function state_name()
    {
        return $this->belongsTo(State::class,'state','id');
    }

    public function city_name()
    {
        return $this->belongsTo(City::class,'city','id');
    }

    public function getShipperRatingAttribute()
    {
        $ratings = ReviewRating::where(['to_user_id' => $this->id, 'user_type' => 'shipper'])->get();
        $rating = 0;
        // foreach ($ratings as $rate) {
        if(isset($ratings) && !empty($ratings)){
            $rating = $ratings->average('rating');
        }
        // return ReviewRating::select(DB::raw('avg(rating)'))->where(['to_user_id' => $this->user_id, 'user_type' => 'shipper'])->first();
        return !empty($rating) ? round($rating) : 0;
    }
    public function drive_vehicles()
    {
        return $this->hasOne('App\Models\DriverVehicle');
    }
}
