<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverVehicle extends Model
{
    use HasFactory;
    protected $table = 'driver_vehicle';
    public $timestamps = false;
    protected $attribute = ['feature'];    
    protected $hidden = ['hydraulic_door','cooling'];

    public function getfeatureAttribute(){
        return TruckFeatures::whereIn('id',explode(',',$this->truck_features))->get(['id','name']);
    }

    public function TruckType(){
        return $this->belongsTo('App\Models\TruckType', 'truck_type', 'id');
    }

    public function brands(){
        return $this->belongsTo('App\Models\TruckBrand', 'brand', 'id');
    }

    public function TruckSubCategory(){
        return $this->belongsTo('App\Models\TruckCategory', 'truck_sub_category', 'id');
    }

    public function VehicleCapacity(){
        return $this->hasMany('App\Models\VehicleCapacity', 'driver_vehicle_id', 'id');
    }

    ///Removed 21-02-2022 Image does not get  //
    public function getImagesAttribute($images){
        return $images != NULL? explode(',', $images) : [];
    }

    public function getTruckTypeAttribute($truck_type){
        return $truck_type ? $truck_type : "";
        // return TruckType::select('id','name')->find($truck_type);
    }

    // public function getTruckSubCategoryAttribute($truck_sub_category){
    //     return $truck_type ? $truck_type : "";
        // return TruckCategory::select('id','name')->find($truck_sub_category);
    // }

    public function getLoadCapacityAttribute($load_capacity){
        return $load_capacity ? $load_capacity : "";
    }
    
    public function getWeightUnitAttribute($unit){
        return TruckUnit::select('id','name')->find($unit);
    }

    public function getLoadCapacityUnitAttribute($unit){
        return TruckUnit::select('id','name')->find($unit);
    }

    // public function getWeightUnitAttribute($weight_unit){
    //     return $weight_unit ? $weight_unit : "";
    // }



    

    



}
