<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DispatcherVehicle extends Model
{
    use HasFactory;

    protected $table='dispatcher_vehicles';

    public function truck()
    {
    	return $this->belongsTo('App\Models\TruckType','truck_type_id','id');
    }
    public function brand()
    {
    	return $this->belongsTo('App\Models\TruckBrand','brand_id','id');
    }
}
