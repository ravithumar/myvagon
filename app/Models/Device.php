<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasFactory;
    protected $table = 'devices';

    public function getLatAttribute($lat){
    	return $lat ? $lat : "";
    }

    public function getLngAttribute($lng){
    	return $lng ? lng : "";
    }
}
