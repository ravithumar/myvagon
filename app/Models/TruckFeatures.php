<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruckFeatures extends Model
{
    use HasFactory;
    protected $table = 'truck_feature';
    
}
