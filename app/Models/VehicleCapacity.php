<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VehicleCapacity extends Model
{
    use HasFactory;
    
    protected $table = 'vehicle_capacity';

    protected $fillable = ['driver_vehicle_id ','package_type_id', 'value'];

    public function package_type_id(){
        return $this->belongsTo(PackageType::class, 'package_type_id', 'id');
    }

    public function getPackageTypeIdAttribute($package_type_id)
    {
        if(!empty($package_type_id)){
            return PackageType::select('id','name')->whereId($package_type_id)->first();
        }
    }
}