<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Users;

class Booking extends Model
{
    use HasFactory;
    
    protected $table = 'booking';
    use SoftDeletes;

    protected $fillable = [
        'txn_id ','user_id', 'booking_type', 'pickup_address', 'pickup_lat', 'pickup_lng', 'pickup_date', 'pickup_time_to', 'pickup_time_from','pickup_note' ,'distance',
        'journey','journey_type','is_public', 'bid_amount', 'cancellation_term', 'cancellation_charge', 'status', 'created_at'];

    protected $appends = ['trucks','shipper_details'];   

    public function booking_location()
    {
        return $this->hasMany(BookingLocations::class,'booking_id','id');
    }

    public function booking_products()
    {
        return $this->hasMany(BookingProduct::class,'booking_id','id');
    }

    public function booking_truck()
    {
        return $this->hasMany(BookingTruck::class,'booking_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function getTrucksAttribute()
    {
        return BookingTruck::whereBookingId($this->id)->first();
    }

    public function getShipperDetailsAttribute()
    {
        return User::select('id','name','profile','company_name')->whereId($this->user_id)->first();
    }

    public function getDeletedAtAttribute($deleted_at){
        return $deleted_at ? $deleted_at : "";
    }
    
    public function getEmailOrNumberAttribute($email_or_number){
        return $email_or_number ? $email_or_number : "";
    }
    
    public function getShipperCancelDriverAttribute($shipper_cancel_driver){
        return $shipper_cancel_driver ? $shipper_cancel_driver : "";
    }
    
    public function getBookingTypeAttribute($booking_type){
        return $booking_type ? $booking_type : "";
    }

    public function getPickupAddressAttribute($pickup_address){
        return $pickup_address ? $pickup_address : "";
    }

    public function getPickupLatAttribute($pickup_lat){
        return $pickup_lat ? $pickup_lat : "";
    }
    
    public function getPickupLngAttribute($pickup_lng){
        return $pickup_lng ? $pickup_lng : "";
    }
    
    public function getPickupDateAttribute($pickup_date){
        return $pickup_date ? $pickup_date : "";
    }
    
    public function getPickupTimeToAttribute($pickup_time_to){
        return $pickup_time_to ? $pickup_time_to : "";
    }

    public function getPickupTimeFromAttribute($pickup_time_from){
        return $pickup_time_from ? $pickup_time_from : "";
    }
    
    public function getPickupNoteAttribute($pickup_note){
        return $pickup_note ? $pickup_note : "";
    }
    
    public function getDistanceAttribute($distance){
        return $distance ? $distance : "0";
    }
    
    public function getJourneyAttribute($journey){
        return $journey ? $journey : "0 mins";
    }

    public function getJourneyTypeAttribute($journey_type){
        return $journey_type ? $journey_type : "";
    }
    
    public function getAmountAttribute($amount){
        return $amount ? $amount : "0.00";
    }
    
    public function getStatusAttribute($status){
        return $status ? $status : "";
    }
    
    public function getEndTripAttribute($end_trip){
        return $end_trip ? $end_trip : "";
    }
    
    public function getPodImageAttribute($pod_image){
        return $pod_image ? $pod_image : "";
    }
    
    public function getPaymentStatusAttribute($payment_status){
        return $payment_status ? $payment_status : "";
    }
    
    public function getTransactionIdAttribute($transaction_id){
        return $transaction_id ? $transaction_id : 0;
    }

    public function getCreatedAtAttribute($created_at){
        return $created_at ? date('Y-m-d H:i:s',strtotime($created_at)) : "";
    }

    public function getUpdatedAtAttribute($updated_at){
        return $updated_at ? date('Y-m-d H:i:s',strtotime($updated_at)) : "";
    }

    public function getPaymentTypeAttribute($payment_type){
        return $payment_type ? $payment_type : "";
    }

}
