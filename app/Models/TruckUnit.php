<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruckUnit extends Model
{
    use HasFactory;

    protected $table = 'truck_unit';
    protected $fillable = [
        'name','unit'
    ];

    public function getUnitAttribute($unit){
        return $unit ? $unit : "";
    }

    
}
