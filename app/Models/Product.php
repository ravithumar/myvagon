<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use BinaryCats\Sku\HasSku;
use BinaryCats\Sku\Concerns\SkuOptions;
class Product extends Model {
    
    
    use HasFactory, HasSku;
    
    
    protected $table = 'products';
    protected $fillable = [
        'id',
        'name',
        'canteen_id',
        'user_id',
        'insert_by',
        'sku_name',
        'sku_no',
        'sku_type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function skuOptions() : SkuOptions
    {
        return SkuOptions::make()
            ->from(['label', 'name'])
            ->target('sku_no')
            ->using('_')
            ->forceUnique(false)
            ->generateOnCreate(true)
            ->refreshOnUpdate(false);
    }

}
