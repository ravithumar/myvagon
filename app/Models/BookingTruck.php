<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingTruck extends Model
{
    use HasFactory;

    protected $table = 'booking_trucks';

    protected $fillable = [
        'driver_id ', 'booking_id', 'truck_id', 'payout', 'truck_type', 'truck_type_category','note','is_hydraulic', 'is_cooling', 'created_at'];
     protected $appends = ['locations'];   

    public function truck_type()
    {
        return $this->belongsTo(TruckType::class,'truck_type','id');
    }

    public function driver_details()
    {
        return $this->belongsTo(User::class,'driver_id','id');
    }

    public function getLocationsAttribute()
    {
    	return BookingLocations::whereBookingId($this->booking_id)->get();
    }

    public function getTruckTypeCategoryAttribute($category)
    {
        if(!empty($category)){
            return TruckCategory::select('id','name')->whereIn('id',explode(',',$category))->get();
        }
    }

    public function getNoteAttribute($note){
        return $note ? $note : "";
    }

    public function getCreatedAtAttribute($created_at){
        return $created_at ? date('Y-m-d H:i:s',strtotime($created_at)) : "";
    }

    public function getUpdatedAtAttribute($updated_at){
        return $updated_at ? date('Y-m-d H:i:s',strtotime($updated_at)) : "";
    }

    public function truck_details()
    {
        return $this->belongsTo(TruckType::class,'truck_type','id');
    }
}
