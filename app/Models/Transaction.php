<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transaction';

    protected $fillable = [
        'booking_id', 'driver_id', 'amount', 'transaction_id', 'transaction_type', 'status', 'date', 'created_at'];

    public function booking_data()
    {
        return $this->belongsTo(Booking::class,'booking_id','id');
    }

    public function getAmountAttribute($amount){
        return $amount ? $amount : "0.00";
    }
    
    public function getTransactionIdAttribute($transaction_id){
        return $transaction_id ? $transaction_id : 0;
    }

    public function getDeletedAtAttribute($deleted_at){
        return $deleted_at ? $deleted_at : "";
    }
    
    public function getTransactionTypeAttribute($transaction_type){
        return $transaction_type ? $transaction_type : "";
    }
    
    public function getStatusAttribute($status){
        return $status ? $status : "";
    }
    
    public function getDateAttribute($date){
        return $date ? $date : "";
    }

    public function getCreatedAtAttribute($created_at){
        return $created_at ? date('Y-m-d H:i:s',strtotime($created_at)) : "";
    }

    public function getUpdatedAtAttribute($updated_at){
        return $updated_at ? date('Y-m-d H:i:s',strtotime($updated_at)) : "";
    }
}
