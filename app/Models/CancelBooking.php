<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Users;

class CancelBooking extends Model
{
    use HasFactory;
    
    protected $table = 'cancel_booking';
    public $timestamps = false;

    protected $fillable = [
        'booking_id','driver_id', 'shipper_id', 'request_time', 'status', 'cancellation_charge'];

}
