<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Facility extends Model
{
    use HasFactory;

    protected $table = 'facility_address';
    protected $primaryKey = 'id';


    protected $fillable = [
        'user_id ', 'name', 'address', 'city', 'state', 'country', 'zipcode', 'phone', 'email',
    ];


    public function city_obj()
    {   
        return $this->belongsTo(City::class, 'city', 'id');
    }

    public function state_obj()
    {   
        return $this->belongsTo(State::class, 'state', 'id');
    }

    public function country_obj()
    {   
        return $this->belongsTo(Country::class, 'country', 'id');
    }


}
