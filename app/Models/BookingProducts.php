<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingProducts extends Model
{
    use HasFactory;

    protected $table = 'booking_product';
}
