<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReviewRating extends Model
{
    use HasFactory;
    protected $table = 'review_rating';

    protected $fillable = [
        'booking_id','from_user_id', 'to_user_id', 'user_type', 'rating', 'review'];
}
