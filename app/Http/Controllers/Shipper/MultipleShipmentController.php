<?php

namespace App\Http\Controllers\Shipper;

use App\Models\City;
use App\Models\State;
use App\Models\Booking;
use App\Models\Country;
use App\Models\Product;
use App\Models\Facility;
use App\Models\TruckType;
use App\Models\TruckUnit;
use App\Models\PackageType;
use Illuminate\Http\Request;
use App\Models\BookingProduct;
use App\Models\BookingLocations;
use App\Http\Controllers\Controller;

use App\Models\BookingLocation;
use App\Models\BookingProducts;
use App\Models\BookingTruck;
use App\Traits\ApiResponser;
use ElephantIO\Engine\SocketIO\Session as SocketIOSession;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Twilio\Tests\Request as TestsRequest;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;

class MultipleShipmentController extends Controller
{

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
      }



    use ApiResponser;

    public function index($id = null)
    {
        $user = Auth::user();
        // $booking= Booking::whereUserId($user->id)->whereStatus('pending')->orderBy('id','desc')->limit(1)->first();
        // $booking= Booking::with(['booking_location','booking_products.product','booking_products.truck_unit','booking_truck'])->whereId($id)->whereStatus('pending')->orderBy('id','desc')->limit(1)->first();
        $booking = Booking::whereId($id)->orderBy('booking.id','desc')->limit(1)->first();
        $booking_locations = BookingLocations::with('products')->whereBookingId($id)->get()->toArray();
        $drop_location = BookingLocations::whereBookingId($id)->orderBy('id','desc')->first();
        $booking_truck = BookingTruck::with(['truck_type'])->whereBookingId($id)->get()->toArray();
        if(empty($booking))
        {
            return redirect()->route('shipper.shipment.create')->with('error',__('Shipment not found!'));
        }
        $location_aray = array();
        if(isset($booking_locations) && !empty($booking_locations)){
            for($i=0;$i<count($booking_locations);$i++){
                array_push($location_aray,[$booking_locations[$i]['drop_location'],$booking_locations[$i]['drop_lat'],$booking_locations[$i]['drop_lng'],$i+1]);  
            } 
        }
        return view('shipper.pages.shipment.index',compact('booking','location_aray','booking_locations','booking_truck'));
    }

    public function calculate_distance($pick_location,$drop_location)
    {
        if ($pick_location != '' && $drop_location != '') {
            $distance_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($pick_location) . "&destinations=" . urlencode($drop_location) . "&mode=driving&key=" . env('MAP_KEY');

            $distance = (array) json_decode(file_get_contents($distance_url));
            if(isset($distance['rows']) && !empty($distance['rows']) && sizeof($distance['rows']) > 0){
                $km = (Float) $distance['rows'][0]->elements[0]->distance->value;
                $km = $km / 1000;
                $duration = $distance['rows'][0]->elements[0]->duration->text;
                // $duration = $duration / 60;
                // $duration = $duration / 60;
                // $data['miles'] = number_format($km * 0.621371,2); // kilometer to miles conversion
                $data['miles'] = number_format($km,1); // kilometer to miles conversion
                $data['journey'] = $duration;
            }else{
                $data['miles'] = 0;
                $data['journey'] = 0;    
            }
        } else {
            $data['miles'] = 0;
            $data['journey'] = 0;
        }
        return $data;
    }

    /**
     *  returns the faqs add page
     */
    public function create()
    {
      
       Session::put('running','running');
       Session::forget('truck_data');
       Session::forget('product_data');
        $truckDetails = TruckType::where('status',1)->with(['category' => function ($q) {
            $q->active();
        }])->get()->toArray();
        $count = count($truckDetails);
        return view('shipper.pages.multiple-shipment.shipment_truck', compact('truckDetails','count'));
    }

    // public function create_truck()
    // {
       
        
    //    Session::put('running','running');

    //     $truckDetails = TruckType::where('status',1)->with(['category' => function ($q) {
    //         $q->active();
    //     }])->get()->toArray();
    //     $count = count($truckDetails);
    //     return view('shipper.pages.shipment.shipment_truck_design', compact('truckDetails','count'));
    // }

    public function create_step_2(Request $request) {
        $validator = Validator::make($request->all(), [
            'truck_type' => 'required',
            'truck_categories' => 'required',
        ]);
        if ($validator->fails() && empty(Session::get('truck_data'))) {
            return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        }
        else
        {
            $data = $request->except('_token');
            $user = Auth::user();
            $productData = Product::where('user_id', $user->id)->orderBy('id','desc')->get()->toArray();
            $packageData = PackageType::where('status',1)->get()->toArray();
            $unitData = TruckUnit::where('deleted_at',NULL)->orderBy('id','desc')->get()->toArray();
            $data['hydraulic'] = isset($data['hydraulic'])?$data['hydraulic']:0;
            if(empty(Session::get('truck_data'))){
                Session::put('truck_data', $data);
            }
            return view('shipper.pages.multiple-shipment.shipment_product', compact('productData','packageData','unitData'));
        }

        
    }

    public function create_step_3(Request $request) {
       
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'package' => 'required',
            'package_quantity' => 'required',
            'weight' => 'required',
            'unit' => 'required',
            // 'note' => 'required|string',
        ]);

        if ($validator->fails() && empty(Session::get('product_data'))) {
            return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        }else
        {
            if(empty(Session::get('truck_data'))){
                return redirect()->back()->with('error',__('Something went wrong please try again later!'));
            }else{
                $user = Auth::user();
                $data = $request->except('_token');
                $unitData = TruckUnit::where('deleted_at',NULL)->get()->toArray();
                $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
                $countries =  Country::whereIn('name', ['Greece'])->get();
                $addresses = Facility::where('user_id', $user->id)->select('*')->with('city_obj')->orderBy('id','desc')->get();
                $count = count($addresses);
                
                if(!empty($data))
                {
                    $products = count($data) === 0 ? Session::get('product_data') : [];
                    
                    $fragile = "0";
                    $sensitive = "0";
                    for($i=0;$i<count($data['product']);$i++)
                    {
                        $products[$i]['product_name'] = Product::where('id', $data['product'][$i])->value('name');
                        $products[$i]['product_id'] = $data['product'][$i];
                        $products[$i]['package'] = $data['package'][$i];
                        $products[$i]['package_quantity'] = $data['package_quantity'][$i];
                        $products[$i]['weight'] = $data['weight'][$i];
                        $products[$i]['unit'] = $data['unit'][$i];
                        $products[$i]['product_description'] = $data['product_description'][$i];
    
                        $products[$i]['fragile'] = isset($data['fragile_'.$i])?1:0; 
                        $products[$i]['sensitive'] = isset($data['sensitive_'.$i])?1:0; 
                    }    
                  
                    if(empty(Session::get('product_data')))
                    { 
                        Session::put('product_data', $products);
                    }
                    $products = Session::get('product_data');
                }
                else
                {
                    $products = Session::get('product_data');
                }
                
                return view('shipper.pages.multiple-shipment.shipment_locations', compact('products','unitData','countries','addresses','count'));
            }
        }

    }

    public function getProductData(Request $request) {
        $id = $request->get('id');
        $product_data = Product::findOrFail($id)->toArray();
        return json_encode($product_data);
    }

    public function getFacilityAddress(Request $request)
    {
        $user = Auth::user();
        $addresses = Facility::where('user_id', $user->id)->select('*')->with('city_obj')->orderBy('id','desc')->get();
        return json_encode($addresses);
    }



    public function store_step_3(Request $request) {
        $validator = Validator::make($request->all(), [
            'pickupAddress' => 'required',
            'deliveryAddress' => 'required',
            'pickup_date' => 'required',
            'delivery_date' => 'required',
            // 'unit' => 'required',
            // 'note' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        }else{
            if(empty(Session::get('product_data')) || empty(Session::get('truck_data'))){
                return redirect()->back()->with('error',__('Something went wrong please try again later!'));
            }else{
                // echo '<pre>';print_r(Session::get('truck_data'));die();
                $data = $request->except('_token');
                echo '<pre>';print_r($data);die();
                $user =   Auth::user();
                $pickup_address = Facility::where('id',$data['pickupAddress'])->first();
                $delivery_address = Facility::where('id',$data['deliveryAddress'])->first();
                $data['pickupAddress'] = $pickup_address['address'];
                $data['deliveryAddress'] = $delivery_address['address'];
    
                
                if(isset($data['pickup_time_range_check']) && $data['pickup_time_range_check'] == '1'){
                    $data['pickup_hour'] = '';
                    $data['pickup_time_format'] = '';
                    $pickup_hour_from = explode(':',$data['pickup_hour_from']);
                    if($pickup_hour_from[0] >= '12'){
                        $data['pickup_time_from'] = 'PM';
                    }else{
                        $data['pickup_time_from'] = 'AM';
                    }
                    $pickup_hour_to = explode(':',$data['pickup_hour_to']);
                    if($pickup_hour_to[0] >= '12'){
                        $data['pickup_time_to'] = 'PM';
                    }else{
                        $data['pickup_time_to'] = 'AM';
                    }
                }
                if(isset($data['pickup_hour']) && $data['pickup_hour'] !== '')
                {
                    $data['pickup_hour_from'] = '';
                    $data['pickup_time_from'] = '';
                    $data['pickup_hour_to'] = '';
                    $data['pickup_time_to'] = '';
                    $pickup_hour = explode(':',$data['pickup_hour']);
                    if($pickup_hour[0] >= '12'){
                        $data['pickup_time_format'] = 'PM';
                    }else{
                        $data['pickup_time_format'] = 'AM';
                    }
                }
    
                if(isset($data['delivery_time_range_check']) && $data['delivery_time_range_check'] == '1'){
                    $data['delivery_hour'] = '';
                    $data['delivery_time_format'] = '';
                    $delivery_hour_from = explode(':',$data['delivery_hour_from']);
                    if($delivery_hour_from[0] >= '12'){
                        $data['delivery_time_from'] = 'PM';
                    }else{
                        $data['delivery_time_from'] = 'AM';
                    }
                    $delivery_hour_to = explode(':',$data['delivery_hour_to']);
                    if($delivery_hour_to[0] >= '12'){
                        $data['delivery_time_to'] = 'PM';
                    }else{
                        $data['delivery_time_to'] = 'AM';
                    }
                }
                if(isset($data['delivery_hour']) && $data['delivery_hour'] !== ''){
                    $data['delivery_hour_from'] = '';
                    $data['delivery_hour_to'] = '';
                    $delivery_hour = explode(':',$data['delivery_hour']);
                    if($delivery_hour[0] >= '12'){
                        $data['delivery_time_format'] = 'PM';
                    }else{
                        $data['delivery_time_format'] = 'AM';
                    }
                }
    
                Session::put('direct_shipment_data', $data);
                
                $pickup_time_from = $data['pickup_hour_from'] !== '' ? $data['pickup_hour_from'].' '.$data['pickup_time_from'] : $data['pickup_hour'].' '.$data['pickup_time_format'];
                $pickup_time_to = $data['pickup_hour_to'] !== '' ? $data['pickup_hour_to'].' '.$data['pickup_time_to'] : $data['pickup_hour'].' '.$data['pickup_time_format'];
    
    
                //To calculate distance between coordinates
                //$distance = $this->distance('23.0225', '72.5715','22.7739','71.6673','K');
                $calculated_distance['miles'] = 0;
                $calculated_distance['journey'] = 0;
                if(!empty($data['pickupAddress']) && !empty($data['deliveryAddress'])){
                    $calculated_distance = $this->calculate_distance($data['pickupAddress'],$data['deliveryAddress']);
                }
                $journey_type = __("Short haul shipment");
                if(!empty($calculated_distance['miles'])){
                    if($calculated_distance['miles'] >= 0 && $calculated_distance['miles'] <= 100){
                        $journey_type = __("Short haul shipment");
                    }else if($calculated_distance['miles'] > 100 && $calculated_distance['miles'] <= 300){
                        $journey_type = __("Medium haul shipment");
                    }else if($calculated_distance['miles'] > 300){
                        $journey_type = __("Long haul shipment");
                    }
                }
                $booking = Booking::create([
                    'user_id' => $user->id,
                    'pickup_address' => $data['pickupAddress'],
                    'pickup_lat' => $pickup_address['address_lat'],
                    'pickup_lng' => $pickup_address['address_lng'],
                    'pickup_date' => $data['pickup_date'],
                    'pickup_time_from' => $pickup_time_from,
                    'pickup_time_to' => $pickup_time_to,
                    'distance' => (isset($calculated_distance['miles']) && !empty($calculated_distance['miles']) ? $calculated_distance['miles'] : '0'),
                    'journey' => (isset($calculated_distance['journey']) && !empty($calculated_distance['journey']) ? $calculated_distance['journey'] : '0'),
                    'journey_type' => $journey_type,
                    //'pickup_note' => $data['pickup_note'],
                ]);
    
                $truck_type_data = Session::get('truck_data');
                $t_type = $truck_type_data['truck_type'];
                foreach($t_type as $truck_type){
                    $truck_data = [];
                    $truck_data['booking_id'] = $booking->id;
                    $truck_data['truck_type'] = $truck_type;
                    foreach($truck_type_data['truck_categories'] as $key => $truck_cat){
                        if($key == $truck_type){
                            $truck_data['truck_type_category'] = implode(',',$truck_cat);
                        }
                    }
                    $truck_data['is_hydraulic'] = $truck_type_data['hydraulic'];
                    // $truck_data['is_cooling'] = $truck_type_data['cooling'];
                    $truck_data['note'] = $truck_type_data['shipment_note'];
    
                    BookingTruck::create($truck_data);
                }
    
                $delivery_time_from = $data['delivery_hour_from'] != '' ? $data['delivery_hour_from'].' '.$data['delivery_time_from'] : $data['delivery_hour'].' '.$data['delivery_time_format'];
                $delivery_time_to = $data['delivery_hour_to'] != '' ? $data['delivery_hour_to'].' '.$data['delivery_time_to'] : $data['delivery_hour'].' '.$data['delivery_time_format'];
    
                $booking_location_id_1 = BookingLocations::create([
                    'booking_id' => $booking->id,
                    'drop_location' => $data['pickupAddress'],
                    'company_name' => $pickup_address['name'],
                    'delivered_at' => $data['pickup_date'],
                    'drop_lat' => $pickup_address['address_lat'],
                    'drop_lng' => $pickup_address['address_lng'],
                    'note' => $data['pickup_note'],
                    'is_pickup' => 1,
                    'delivery_time_from' => $pickup_time_from,
                    'delivery_time_to' => $pickup_time_to,
                ])->id;
    
                $booking_location_id_2 = BookingLocations::create([
                    'booking_id' => $booking->id,
                    'drop_location' => $data['deliveryAddress'],
                    'delivered_at' => $data['delivery_date'],
                    'company_name' => $delivery_address['name'],
                    'drop_lat' => $delivery_address['address_lat'],
                    'drop_lng' => $delivery_address['address_lng'],
                    'note' => $data['delivery_note'],
                    'is_pickup' => 0,
                    'delivery_time_from' => $delivery_time_from,
                    'delivery_time_to' => $delivery_time_to,
                    'distance' => (isset($calculated_distance['miles']) && !empty($calculated_distance['miles']) ? $calculated_distance['miles'] : ''),
                    'journey' => (isset($calculated_distance['journey']) && !empty($calculated_distance['journey']) ? $calculated_distance['journey'] : ''),
                ])->id;
    
                $products = Session::get('product_data');
                foreach($products as $product)
                {
                    $productDetails = Product::findOrFail($product['product_id'])->toArray();
                    BookingProduct::create([
                        'booking_id' => $booking->id,
                        'location_id' => $booking_location_id_1,
                        'product_id' => $product['product_id'],
                        'product_type' => $product['package'],
                        'qty' => $product['package_quantity'],
                        'weight' => $product['weight'],
                        'unit' => $product['unit'],
                        'note' => $product['product_description'],
                        'is_fragile' => $product['fragile'],
                        'is_sensetive' => $product['sensitive'],
                    ]);
    
                    BookingProduct::create([
                        'booking_id' => $booking->id,
                        'location_id' => $booking_location_id_2,
                        'product_id' => $product['product_id'],
                        'product_type' => $product['package'],
                        'qty' => $product['package_quantity'],
                        'weight' => $product['weight'],
                        'unit' => $product['unit'],
                        'note' => $product['product_description'],
                        'is_fragile' => $product['fragile'],
                        'is_sensetive' => $product['sensitive'],
                    ]);
    
                }
                Session::forget('truck_data');
                Session::forget('product_data');
                return redirect('shipper/shipment/search/'.$booking->id)->with('success',__('Shipment Created Successfully'));
            }
        }

        

        // echo "<pre>";
        // print_r(Session::get('truck_data'));
        // echo "</pre>";
        // echo "<br>";
        // echo "<pre>";
        // print_r(Session::get('product_data'));
        // echo "</pre>";
        // echo "<br>";
        // echo "<pre>";
        // print_r(Session::get('direct_shipment_data'));exit;
        // return view('shipper.pages.shipment.create-shipment-pickup-and-delivery-infomation-direct-shipment', compact('products','unitData','countries','states','cities','addresses','count'));
    }

    public function addAddress(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postcode' => 'required',
            'phone' => 'required',
            'email' => 'required|email',

        ],[
            'required' => 'This value is required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors()->first())
                        ->withInput();
        }
        $user = Auth::user();
        $data = $request->all();

        $address = new Facility();
        $address->user_id = $user->id;
        $address->name = $data['name'];
        $address->address = $data['address'];
        $address->address_lat = $data['lat'];
        $address->address_lng = $data['lng'];
        $address->city = $data['city'];
        $address->state = $data['state'];
        $address->country = $data['country'];
        $address->zipcode = $data['postcode'];
        $address->phone = $data['phone'];
        $address->email = $data['email'];

        $address->restroom_service = $request->get('restroom_service') == null ? '0' : '1';
        $address->food_service = $request->get('food_service') == null ? '0' : '1';
        $address->rest_area_service = $request->get('rest_area_service') == null ? '0' : '1';

        if($request->get('restroom_service') == '1'){
            $address->restroom_service = "1";
        }
        if($request->get('food_service') == '1'){
            $address->food_service = "1";
        }
        if($request->get('rest_area_service') == '1'){
            $address->rest_area_service = "1";
        }

        $address->save();
        $response['message'] = __('Address added Successfully');
        echo json_encode($response);

        // return redirect()->back()->with('success',__('Address added Successfully'));
    }

    public function addProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sku_type' => 'required',
        ],[
            'required' => 'This value is required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors()->first())
                        ->withInput();
        }
        $user = Auth::user();
        $data = $request->all();
        $product = new Product();
        $product->user_id = $user->id;
        $product->name = $data['name'];
        $product->sku_type = $data['sku_type'];
        $product->save();

        $product->sku_name = $product->id . $product->sku_no;
        $product->update();
        return redirect()->back()->with('success',__('Product added Successfully'));
    }

    public function public_shipment(Request $request,$id){
        
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
        ],[
            'required' => 'This value is required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors()->first())
                        ->withInput();
        }
        $data = $request->all();

        $booking = Booking::where('id', $id)->first();
        if(isset($data['all_bidding']) && !empty($data['all_bidding']) && $data['all_bidding'] == 'on'){
            $is_bid = 1;
        }else{
            $is_bid = 0;
        }
        $booking->is_bid = $is_bid;
        $booking->amount = $data['amount'];
        $booking->status = 'pending';
        $booking->is_public = 1;
        $booking->update();

        return redirect(url('shipper/manage-shipment'))->with('success',__('Your shipment is public'));
    }

    public function live_search()
    {
        return view('shipper.pages.shipment.live_search');
    }

    public function selectSearch(Request $request)
    {
    	$movies = [];

        if($request->has('q')){
            $search = $request->q;
            $movies =Movie::select("id", "name")
            		->where('name', 'LIKE', "%$search%")
            		->get();
        }
        return response()->json($movies);
    }

}
