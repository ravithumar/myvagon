<?php

namespace App\Http\Controllers\Shipper;

use App\Models\City;
use App\Models\State;
use App\Models\Booking;
use App\Models\Country;
use App\Models\Product;
use App\Models\Facility;
use App\Models\TruckType;
use App\Models\TruckUnit;
use App\Models\User;
use App\Models\PackageType;
use Illuminate\Http\Request;
use App\Models\BookingProduct;
use App\Models\BookingBid;
use App\Models\BookingRequest;
use App\Models\BookingLocations;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Shipper\AuthController;
use App\Models\Availability;

use App\Models\BookingLocation;
use App\Models\BookingProducts;
use App\Models\BookingTruck;
use App\Models\Transaction;
use App\Models\SystemConfig;
use App\Models\Invoice;

use ElephantIO\Engine\SocketIO\Session as SocketIOSession;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Twilio\Tests\Request as TestsRequest;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use PermissionHelper;
use Illuminate\Support\Str;
use Twilio\Http\Response;
use MyPDF;

class InvoiceController extends Controller
{


    public function __construct()
    {
        // $this->middleware(function ($request, $next) {
        //     $user =Auth::user();
        //     if($user->type == 'manager'){
        //         $permission = PermissionHelper::check_access('create_shipment');
        //         if(empty($permission)){
        //             return redirect()->back()->with('error',__("You can't access this module!"));
        //         }
        //     }
        //     return $next($request);
        // });
    }

    public function index($data)
    {
        if(isset($data) && !empty($data)){
                // dd();exit;
            $user_id = $data[0];
            $start_date = $data[1];
            $end_date = $data[2];
            $cancellation_charge = SystemConfig::where('path', 'shipper_cancellation_charge')->value('value');
            if(empty($user_id) || empty($start_date) || empty($end_date)){
                return redirect(url('shipper/login'))->with('error', __("Sorry No Shipment found in this month!"));
            }
            $users_data = User::where(['id' => $user_id])->first();
            $booking = Booking::with(['user'])->where('user_id',$user_id)->whereIn('status',['completed','cancelled'])->whereBetween('pickup_date',[$start_date,$end_date])->get()->toArray();
            $response['cancellation_charge'] = $cancellation_charge;
            $response['user'] = $users_data;
            $response['booking'] = $booking;
            $response['invoice_id'] = $data[3];
            if(isset($response['booking']) && !empty($response['booking'])){
                $this->genrate_view($response);
            }else{
                 return redirect()->route('shipper.login');
            }
        }else{
            return redirect(url('shipper/login'))->with('error', __("Sorry No Shipment found in this month!"));
        }
    }

    public function genrate_view($response = []) {
        // echo '<pre>';print_r($response);die();
        $final_amount = 0;
        if(isset($response['booking']) && !empty($response['booking'])){
            foreach ($response['booking'] as $key => $value) {
                if ($value['status'] == 'cancelled') {
                    if ($response['cancellation_charge'] > 0) {
                        $final_amount = $final_amount + round($response['cancellation_charge']);
                    }
                }else{
                    $final_amount = $final_amount + round($response['user']['commission_fee']);
                }
            }
        }
        $invoice_data = Invoice::where(['id' => $response['invoice_id']])->first();
        
        // $pickup_address = $booking->trucks->locations[0]->pickup_location;
        // $dropoff_address = $booking->trucks->locations[0]->pickup_location;
        $truck_no = 1;
        


        $base_url = url();

        $pdf = new MyPDF('P', 'mm', 'A4', true, 'UTF-8', false);
        MyPDF::setHeaderCallback(function($pdf){
            //Set Header
            MyPDF::SetLineStyle(array('width' => 0.25 / 200, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 255, 255)));

            $base_url = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $base_url .= '://' . $_SERVER['HTTP_HOST'];
            $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $pdf_logo = $base_url . 'shipper/images/logo.png';
            

            MyPDF::Image($pdf_logo, 25, 25, 45, '');

            MyPDF::SetY(15);
            MyPDF::setCellPaddings(0, 0, 5, 0);
            MyPDF::SetFont('helvetica', 'B', 23);
            MyPDF::Cell(0, 8, "TAX INVOICE", 0, 1, 'R', 0, '', 1);

            MyPDF::SetY(MyPDF::GetY() + 9);
            MyPDF::SetFont('helvetica', 'B', 14);
            MyPDF::Cell(0, 6, "MY VAGON", 0, 1, 'R', 0, '', 1);

            MyPDF::setCellPaddings(0, 0, 5, 0);
            MyPDF::SetFont('helvetica', '', 10);
            MyPDF::Cell(0, 5, "Lorem ipsum dolor sit amet,", 0, 1, 'R', 0, '', 1);
            MyPDF::MultiCell(0, 0, "Lorem ipsum dolor sit amet\nconsetetur sadipscing elitr, sed\ndiam nonumy eirmod tempor", 0, 'R', 0, 1, '', '', true, 0, false);

            /*MyPDF::SetY(MyPDF::GetY() + 6);
            MyPDF::Cell(0, 5, "VAT#" . config('vat_no'), 0, 1, 'R', 0, '', 1);*/
            MyPDF::SetY(MyPDF::GetY() + 6);
            MyPDF::Cell(0, 5, "www.myvagon.com", 0, 1, 'R', 0, '', 1);

            MyPDF::SetY(MyPDF::GetY() + 2);
            MyPDF::SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(73, 52, 143)));
            MyPDF::Line(10, MyPDF::GetY(), 200, MyPDF::GetY());

            MyPDF::SetMargins(10, 87, 10, true);
        });
        MyPDF::AddPage();

        //Set Body

// / Bill Details /
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0.5);
        MyPDF::Cell(95, 5, "BILL TO", 0, 0, 'L', 0, '', 1);

        MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::setCellPaddings(0.5, 0.5, 5, 0.5);
        MyPDF::Cell(64.5, 5, "INVOICE NUMBER:", 0, 0, 'R', 0, '', 1);

        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::setCellPaddings(0, 0.5, 0, 0.5);
        MyPDF::Cell(30, 5, "INV-" . isset($invoice_data['invoice_number']) && !empty($invoice_data['invoice_number']) ? $invoice_data['invoice_number'] : strtotime($response['user']->created_at), 0, 1, 'L', 0, '', 1);


// / ----- /
        MyPDF::SetFont('aealarabiya', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0.5);
        // MyPDF::Cell(95, 5, $booking->customer['first_name'] . ' ' . $booking->customer['last_name'], 0, 0, 'L', 0, '', 1);
        MyPDF::Cell(95, 5, (isset($response['user']['first_name']) && !empty($response['user']['first_name']) ? $response['user']['first_name'] : '') . ' ' . (isset($booking['user']['last_name']) && !empty($booking['user']['last_name']) ? $booking['user']['last_name'] : ''), 0, 0, 'L', 0, '', 1);

        MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::setCellPaddings(0.5, 0.5, 5, 0.5);
        MyPDF::Cell(64.5, 5, "", 0, 0, 'R', 0, '', 1);

        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::setCellPaddings(0, 0.5, 0, 0.5);
        MyPDF::Cell(30, 5, "", 0, 1, 'L', 0, '', 1);

// / ----- /
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0.5);
        MyPDF::Cell(95, 5, (isset($response['user']['phone']) && !empty($response['user']['phone']) ? $response['user']['phone'] : ''), 0, 0, 'L', 0, '', 1);

        MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::setCellPaddings(0.5, 0.5, 5, 0.5);
        MyPDF::Cell(64.5, 5, "INVOICE DATE:", 0, 0, 'R', 0, '', 1);

        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::setCellPaddings(0, 0.5, 0, 0.5);
        MyPDF::Cell(30, 5, isset($invoice_data['created_at']) && !empty($invoice_data['created_at']) ? date('M d Y',strtotime($invoice_data['created_at'])) : date('M d Y'), 0, 1, 'L', 0, '', 1);
// / ----- /
        /*MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0.5);
        MyPDF::Cell(95, 5, "", 0, 0, 'L', 0, '', 1);

        MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::setCellPaddings(0.5, 0.5, 5, 0.5);
        MyPDF::Cell(64.5, 5, "PAYMENT DUE DATE:", 0, 0, 'R', 0, '', 1);*/

        /*MyPDF::SetFont('helvetica', '', 10);
        MyPDF::setCellPaddings(0, 0.5, 0, 0.5);
        MyPDF::Cell(30, 5, date('M d Y',strtotime($booking->created_at)), 0, 1, 'L', 0, '', 1);*/

        MyPDF::SetY(MyPDF::GetY() + 4);
// / $pdf->SetX($pdf->GetX() + 4); /

        MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0.5);
        MyPDF::Cell(40, 5, "INVOICE STATUS:", 0, 0, 'L', 0, '', 1);

        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::setCellPaddings(0, 0.5, 0, 0.5);
        MyPDF::Cell(75, 5, isset($invoice_data['status']) && !empty($invoice_data['status']) && $invoice_data['status'] == '1' ? "UNPAID" : "PAID", 0, 0, 'L', 0, '', 1);

        MyPDF::SetFillColor(255, 230, 210);
        MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::setCellPaddings(0.5, 1, 5, 1);
        MyPDF::Cell(44.5, 5, "AMOUNT DUE:", 0, 0, 'R', 1, '', 1);

        MyPDF::setCellPaddings(0, 1, 0, 1);
        MyPDF::Cell(30, 5,number_format($final_amount,2) . "EUR", 0, 1, 'L', 1, '', 1);

        /*MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0.5);
        MyPDF::Cell(40, 5, "PAYMENT TYPE:", 0, 0, 'L', 0, '', 1);*/

        /*MyPDF::SetFont('helvetica', '', 10);
        MyPDF::setCellPaddings(0, 0.5, 0.5, 0.5);
        MyPDF::Cell(75, 5, ($booking->payment_type == "cash") ? "CASH" : "CARD", 0, 1, 'L', 0, '', 1);*/

        /*MyPDF::setCellPaddings(5, 1.5, 0.5, 0.5);
        MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::Cell(40, 5, "NO. OF TRUCKS:", 0, 0, 'L', 0, '', 1);*/

        /*MyPDF::SetFont('helvetica', '', 10);
        MyPDF::setCellPaddings(0, 0.5, 0.5, 0.5);
        MyPDF::Cell(75, 5, 1, 0, 1, 'L', 0, '', 1);*/

        MyPDF::SetY(MyPDF::GetY() + 4);
        MyPDF::SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(73, 52, 143)));
        MyPDF::Line(10, MyPDF::GetY(), 200, MyPDF::GetY());

// / PICKUP and DROP-OFF /
        MyPDF::SetY(MyPDF::GetY() + 4);

        /*MyPDF::SetFont('helvetica', 'B', 10);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0);
        MyPDF::Cell(115, 5, "PICKUP", 0, 0, 'L', 0, '', 1);

        MyPDF::setCellPaddings(0, 0.5, 0.5, 0);
        MyPDF::Cell(74.5, 5, "DROP-OFF", 0, 1, 'L', 0, '', 1);*/

        /* $print_p_address = str_replace(", ", ",", $pickup_address);
        $print_d_address = str_replace(", ", ",", $dropoff_address); */

        /*MyPDF::SetFont('aealarabiya', '', 10);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0.5);
        MyPDF::MultiCell(115, 0, $print_p_address, 0, 'L', 0, 0, '', '', true, 0, false);

        MyPDF::setCellPaddings(0, 0.5, 0.5, 0.5);
        MyPDF::MultiCell(74.5, 0, $print_d_address, 0, 'L', 0, 1, '', '', true, 0, false);*/

        /*MyPDF::SetFont('aealarabiya', '', 8.5);
        MyPDF::setCellPaddings(5, 0.5, 0.5, 0.5);
        MyPDF::Cell(115, 5, "Date: " . date('M d, Y',strtotime($booking->trucks->locations[0]->pickup_date)) . " Time " . $booking->trucks->locations[0]->pickup_time_from, 0, 0, 'L', 0, '', 1);
        MyPDF::setCellPaddings(0, 0.5, 0.5, 0.5);
        MyPDF::Cell(74.5, 5, "Date: " . date('M d, Y',strtotime($booking->trucks->locations[0]->pickup_date)) . " Time " . $booking->trucks->locations[0]->pickup_time_from, 0, 1, 'L', 0, '', 1);*/

        MyPDF::SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(100, 100, 100)));

        MyPDF::SetY(MyPDF::GetY() + 8);

        MyPDF::setCellPaddings(2, 0.3, 1, 0.3);
        MyPDF::SetFillColor(220, 220, 220);

        if(isset($response['booking']) && !empty($response['booking'])){
            foreach ($response['booking'] as $key => $value) {
                if ($value['status'] == 'cancelled') {
                    if ($response['cancellation_charge'] > 0) {
                        $final_amount = round($response['cancellation_charge']);
                    }

                    if ($value['status'] == "cancelled" && $response['cancellation_charge'] == 0) {
                        $final_amount = 0;
                    }
                }
               
                MyPDF::SetX(MyPDF::GetX() + 4);
                MyPDF::SetFont('helvetica', '', 10);
                MyPDF::SetTextColor(100, 100, 100);
                MyPDF::Cell(115, 8, "Booking ID #".$value['id']." was ".$value['status'], "T", 0, 'L', 0, '', 1);
                MyPDF::SetFont('helvetica', '', 11);
                MyPDF::SetTextColor(0, 0, 0);
                MyPDF::Cell(67, 8,number_format(($value['status'] == 'completed' ? (isset($response['user']['commission_fee']) && !empty($response['user']['commission_fee']) ? $response['user']['commission_fee'] : 0) : (isset($response['cancellation_charge']) && !empty($response['cancellation_charge']) ? $response['cancellation_charge'] : 0)),2), "T", 1, 'R', 0, '', 1);        
            }
        }

        

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::Cell(115, 8, "Off-loading charges", "T", 0, 'L', 1, '', 1);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8,number_format(500,2), "T", 1, 'R', 1, '', 1);*/

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::Cell(115, 8, "Trip charges", 0, 0, 'L', 0, '', 1);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8, number_format(20 - 6, 2), 0, 1, 'R', 0, '', 1);*/

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::Cell(115, 8, "Other charges", 0, 0, 'L', 1, '', 1);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8, "0.00", 0, 1, 'R', 1, '', 1);*/

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::Cell(115, 8, "Sub total", "T", 0, 'L', 0, '', 1);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8, number_format(20 - 6, 2), "T", 1, 'R', 0, '', 1);*/

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::Cell(115, 8, "VAT(".config('vat')."%)", "T", 0, 'L', 1, '', 1);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8, number_format(6, 2), "T", 1, 'R', 1, '', 1);*/

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::Cell(115, 8, "Cancellation charges", 0, 0, 'L', 0, '', 1);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8, number_format($booking->cancellation_charges, 2), 0, 1, 'R', 0, '', 1);*/

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::Cell(115, 8, "", 0, 0, 'L', 1, '', 1);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8, "", 0, 1, 'R', 1, '', 1);*/

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::Cell(115, 8, "Refunded", 0, 0, 'L', 0, '', 1);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8, number_format(20 - $booking->cancellation_charges, 2), 0, 1, 'R', 0, '', 1);*/

        /*MyPDF::SetX(MyPDF::GetX() + 4);
        MyPDF::SetFont('helvetica', '', 10);
        MyPDF::SetTextColor(100, 100, 100);
        MyPDF::Cell(115, 8, "TOTAL", "B", 0, 'L', 1, '', 1);
        MyPDF::SetFont('helvetica', '', 11);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(67, 8, number_format($final_amount,2) . "SR", "B", 1, 'R', 1, '', 1);*/

        /*MyPDF::SetY(MyPDF::GetY() + 4);
        MyPDF::SetFont('aealarabiya', '', 10);
        MyPDF::SetTextColor(0, 0, 0);
        MyPDF::Cell(0, 6, "ملخص سياسة الاسترداد والالغاء لإضافتها على الفاتورة الالكترونية:", 0, 1, 'C', 0, '', 1);*/

        /*MyPDF::SetTextColor(100, 100, 100);
        MyPDF::SetFont('aealarabiya', '', 9);
        MyPDF::setCellPaddings(5, 0.5, 5, 0.5);
        MyPDF::MultiCell(0, 0, "في حال إلغاء الرحلة قبل موعدها بأربع ساعات يُسترد كامل المبلغ وفي حال إلغاء الرحلة قبل البدء فيها لخطأ في النظام يُرد كامل المبلغ وإذا كان الإلغاء بعد توجُّه الناقل لنقطة التحميل يخصم (10%) من المبلغ وإذا كان بعد وصول الناقل لنقطة التحميل يخصم (20%) من المبلغ وإذا كان بعد بدء التحميل يخصم (30%) وإذا كان بعد قطع مسافة أقل من (25%) يخصم (50%) من المبلغ وإذا كان بعد قطع مسافة أكثر من (25%) يخصم (100%) من المبلغ وإذا كان بعد قطع مسافة أكثر من (50%) يخصم المبلغ بالكامل بالإضافة الى تكاليف رحلة جديدة مضاف اليها رسوم ساعات الانتظار، ولمزيد من التفاصيل يرجى مراجعة الشروط والاحكام." . "\n", 0, 'J', 0, 1, '', '', true, 0, false);*/


        ob_end_clean();
        MyPDF::Output('INV-' .strtotime($response['user']->created_at). '.pdf', 'I');
        $this->tank_you();
        // return redirect(url('shipper/login'))->with('error',__('Invoice download successfully.'));
    }

    public function tank_you()
    {
        echo '<pre>';print_r('Thank You');die();
    }

    public function shipperCommisionInvoice($key){
        if(isset($key) && !empty($key)){
            $invoice_data = base64_decode($key);
            $invoice_data = explode('.',$invoice_data);
            if(count($invoice_data) == '4'){
                $user_id = $invoice_data[0];
                $start_date = $invoice_data[1];
                $end_date = $invoice_data[2];
                $cancellation_charge = SystemConfig::where('path', 'shipper_cancellation_charge')->value('value');
                if(empty($user_id) || empty($start_date) || empty($end_date)){
                    return redirect(url('shipper/login'))->with('error', __("Something went wrong!"));
                }
                $users_data = User::where(['id' => $user_id])->first();
                $booking = Booking::with(['user'])->where('user_id',$user_id)->whereIn('status',['completed','cancelled'])->whereBetween('pickup_date',[$start_date,$end_date])->get()->toArray();
                $response['cancellation_charge'] = $cancellation_charge;
                $response['user'] = $users_data;
                $response['booking'] = $booking;
                $response['invoice_id'] = $invoice_data[3];
                if(isset($response['booking']) && !empty($response['booking'])){
                    $this->genrate_view($response);
                }else{
                    return redirect(url('shipper/login'))->with('error', __("Sorry No Shipment found in this month!"));
                }
            }else{
                return redirect(url('shipper/login'))->with('error',__('Your link was expired.'));    
            }
            
        }else{
            return redirect(url('shipper/login'))->with('error',__('Something went wrong! please try again.'));
        }
    }

}
