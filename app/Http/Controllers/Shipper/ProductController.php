<?php

namespace App\Http\Controllers\Shipper;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\Product;
use App\Models\State;
use Illuminate\Http\Request;
use App\Models\BookingProduct;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use PermissionHelper;
use SocketHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductsImport;
class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user =Auth::user();
            if($user->type == 'manager'){
                $permission = PermissionHelper::check_access('view_products');
                if(empty($permission)){
                    return redirect()->back()->with('error',__("You can't access this module!"));
                }
            }
            return $next($request);
        });
    }

    public function index(Request $request, $id = null) {
        // SocketHelper::emit('accept_request',['shipper_id'=>382]);
        $user = Auth::user();   
        if ($request->ajax()) {
            if($user['type'] == 'manager'){
                $data = Product::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->orderBy('id','desc');
            }else{
                $data = Product::where('user_id', $user->id)->select('*')->orderBy('id','desc');    
            }
            // $data = Product::where('user_id', $user->id)->select('*')->orderBy('id','desc');

            return FacadesDataTables::of($data)
                ->addIndexColumn()
                ->editColumn('action', function ($row) {
                    
                    $updateLink =   route('shipper.product.data', $row['id']);
                    $option = '';

                    $updateLink = "'" . $updateLink . "'";
                    $name = "'" . $row['name'] . "'";
                    $number = "'" . $row['sku_no'] . "'";
                    $type = "'" . $row['sku_type'] . "'";
                    $id = "'" . $row['id'] . "'";
                   
                    // $btn = '
                    // <a href=""  onclick="updateProduct(' . $updateLink . ',' . $name . ',' . $number .','. $type . ','.$id.')" data-id="'. $row['id'] .'" data-toggle="modal" class="btn btn-white btn-xs"><i class="fa fa-eye"></i></a>
                    // <a href=""  onclick="GetProduct(' . $updateLink . ',' . $name . ',' . $number .','. $type . ','.$id.')" data-id="'. $row['id'] .'" data-toggle="modal" class="btn btn-white btn-xs"><i class="fa fa-pencil-square-o"></i></a>';
                    $btn = '<a href="'.url('shipper/product/show',$row['id']).'" style="color: #9B51E0;text-decoration:underline;font-weight: bold;" class="font-14"> View Details</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        } else {

           
            $columns = [
                            ['data' => 'id', 'name' => "Id",'title' => __("S.No.") ], 
                            ['data' => 'name', 'name' => __("Name"),'title' => __("Name")],
                            // ['data' => 'sku_name', 'name' => __("SKU_NAME"),'title' => __("SKU Name")],
                            ['data' => 'sku_no', 'name' => __("SKU_No"),'title' => __("SKU Number")],
                            ['data' => 'sku_type', 'name' => __("SKU_TYPE"),'title' => __("SKU Type")],
                            ['data' => 'action', 'name' =>__("Action"),'title'=>__("Action"), 'searchable' => false, 'orderable' => false]
                       ];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('shipper.product.index');
            $params['dateTableTitle'] = "Products";
            $params['addUrl'] = "Products";
            $params['dataTableId'] = time();
           
            return view('shipper.pages.product.index', $params);
        }
    }

    public function create()
    {
        return view('shipper.pages.product.create');
    }

    public function store(Request $request) {
        // echo '<pre>';print_r($request->all());die();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sku_type' => 'required',
            'sku_no' => 'required',
            
        ],[
            'name.required' => 'please enter name',
            'sku_type.required' => 'please enter sku type',
            'sku_no.required' => 'please enter sku no',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }
        $product = new Product();
        $user = Auth::user();
        if($user['type'] == 'manager'){
            $product->user_id = $user->company_ref_id;
            $product->insert_by = $user->id;
        }else{
            $product->user_id = $user->id;
            $product->insert_by = $user->id;
        }
        $product->name = $request->input('name');
        $product->sku_type = $request->input('sku_type');
        $product->sku_no = $request->input('sku_no');
        $product->save();

        $product->sku_no = $request->input('sku_no');
        $product->sku_name = $product->id . $product->sku_no;
        $product->update();

        return redirect()->route('shipper.product.index')->with('success',__('Product created successfully.'));
    }

    public function edit($id)
    {
        if(!empty($id) && isset($id)){
            $product = Product::where('id', $id)->first();
            if(isset($product) && !empty($product)){
                return view('shipper.pages.product.edit',compact('product'));
            }else{
                return redirect()->route('shipper.product.index')->with('error',__('Something went wrong.'));    
            }
        }else{
            return redirect()->route('shipper.product.index')->with('error',__('Something went wrong.'));
        }
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sku_type' => 'required',
            'sku_no' => 'required',
            
        ],[
            'name.required' => 'please enter name',
            'sku_type.required' => 'please enter sku type',
            'sku_no.required' => 'please enter sku no',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }
        $product = Product::where('id', $id)->first();
        $product->name = $request->input('name');
        $product->sku_type = $request->input('sku_type');
        $product->sku_no = $request->input('sku_no');
        $product->update();
        return redirect()->route('shipper.product.index')->with('success',__('Product update successfully.'));
    }

    public function show($id)
    {
        if(isset($id) && !empty($id)){
            $product = Product::where('id', $id)->first();
            if(isset($product) && !empty($product)){
                return view('shipper.pages.product.show',compact('product'));
            }else{
                return redirect()->route('shipper.product.index')->with('error',__('Something went wrong.'));
            }
        }else{
            return redirect()->route('shipper.product.index')->with('error',__('Something went wrong.'));
        }
    }


    public function destroy(Request $request, $id = null) {
        $check_is_exists = BookingProduct::where('product_id',$id)->exists();
        if($check_is_exists){
            return \Response::json(['error' => 'You can not remove this product because this product already used in shipment.'], 200);    
        }else{
            $product = Product::where('id', $id)->first();
            $product->delete();
            return \Response::json(['success' => 'Product removed successfully.'], 200);
        }
    }
    public function productImport(Request $request)
    {
        
                Excel::import(new ProductsImport,request()->file('file'));
                return back();
    }


}
