<?php

namespace App\Http\Controllers\Shipper;

use App\Models\City;
use App\Models\State;
use App\Models\Booking;
use App\Models\Country;
use App\Models\Product;
use App\Models\Facility;
use App\Models\TruckType;
use App\Models\TruckUnit;
use App\Models\PackageType;
use Illuminate\Http\Request;
use App\Models\BookingProduct;
use App\Models\BookingBid;
use App\Models\BookingRequest;
use App\Models\BookingLocations;
use App\Http\Controllers\Controller;
use App\Models\Availability;

use App\Models\BookingLocation;
use App\Models\BookingProducts;
use App\Models\BookingTruck;
use App\Models\Transaction;
use App\Models\DriverVehicle;
use App\Models\Device;
use App\Traits\ApiResponser;
use ElephantIO\Engine\SocketIO\Session as SocketIOSession;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Twilio\Tests\Request as TestsRequest;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use PermissionHelper;
use CommonHelper;
use Illuminate\Support\Str;
use Twilio\Http\Response;

class ShipmentController extends Controller
{

    protected $projects;

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return number_format($miles,2);
        }
      }



    use ApiResponser;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user =Auth::user();
            if($user->type == 'manager'){
                $permission = PermissionHelper::check_access('create_shipment');
                if(empty($permission)){
                    return redirect()->back()->with('error',__("You can't access this module!"));
                }
            }
            return $next($request);
        });
    }

    public function index($id = null)
    {
        $user = Auth::user();
        // $booking= Booking::whereUserId($user->id)->whereStatus('pending')->orderBy('id','desc')->limit(1)->first();
        // $booking= Booking::with(['booking_location','booking_products.product','booking_products.truck_unit','booking_truck'])->whereId($id)->whereStatus('pending')->orderBy('id','desc')->limit(1)->first();
        if($user['type'] == 'manager'){
            $booking = Booking::whereId($id)->where('user_id',$user['company_ref_id'])->orderBy('booking.id','desc')->limit(1)->first();
        }else{
            $booking = Booking::whereId($id)->where('user_id',$user['id'])->orderBy('booking.id','desc')->limit(1)->first();
        }
        $booking_locations = BookingLocations::with('products')->whereBookingId($id)->get()->toArray();
        $drop_location = BookingLocations::whereBookingId($id)->orderBy('id','desc')->first();
        $booking_truck = BookingTruck::with(['truck_type'])->whereBookingId($id)->get()->toArray();

        if(empty($booking))
        {
            return redirect()->route('shipper.shipment.create')->with('error',__('Shipment not found!'));
        }
        $location_aray = array();
        if(isset($booking_locations) && !empty($booking_locations)){
            for($i=0;$i<count($booking_locations);$i++){
                array_push($location_aray,[$booking_locations[$i]['drop_location'],$booking_locations[$i]['drop_lat'],$booking_locations[$i]['drop_lng'],$i+1]);  
            } 
        }
        $user = Auth::user();
        if($user->type == 'manager'){
            $manage_price = PermissionHelper::check_access('manage_price');
        }else{
            $manage_price = 1;
        }

        $bookingss = Booking::where('pickup_date', '>=', date('Y-m-d'))
        ->where(function($query) use ($id){
            if($id > 0){
                $query->where('id',$id);
            }
        })
        ->where('user_id',$user->id)->whereIn('status',['pending','create'])->orderBy('id','asc')->get()->toArray();
        $available_trucks = [];
        if(isset($bookingss) && !empty($bookingss)){
            $i = 0;
            foreach($bookingss as $book){
                $truck_type = TruckType::where(['id'=>$book['trucks']['truck_type']])->limit(1)->get()->toArray();
                $availability = $this->nearByDriver($book);
                foreach ($availability->toArray() as $row) {
                    $row['trucks_type'] = $truck_type[0];
                    $row['booking'] = $book;
                    // $available_trucks[] = $row;
                    if(empty($row['booking_request'])){
                        $available_trucks[] = $row;
                    }else{
                        if(isset($row['booking_request']) && !empty($row['booking_request'])){
                            foreach($row['booking_request'] as $booking_request){
                                if($booking_request['shipper_id'] != ($user['type'] == 'manager' ? $user['company_ref_id'] : $user['id'])){
                                    $available_trucks[] = $row;
                                }
                            }
                        }
                    }
                }
            }
        }
        $available_trucks = $available_trucks;
        // $available_trucks['price'] = Str::limit($available_trucks['price'], 11);
        $columns = [
                        ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px'], 
                        ['data' => 'truck_type', 'name' => "truck_type",'title' => __("Truck Type"),'width'=>'60px'], 
                        ['data' => 'start_date', 'name' => __("amount"),'title' => __("Start Date"),'width'=>'60px'],
                        ['data' => 'start_location', 'name' => __("status"),'title' => __("Start Location"),'width'=>'100px'],
                        ['data' => 'end_location', 'name' => __("end_location"),'title' => __("End Location"),'width'=>'100px'],
                        ['data' => 'carrier_info', 'name' => __("end_date"),'title' => __("Carrier Info"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                        ['data' => 'price', 'name' => __("drop_location"),'title' => __("Price"),'width'=>'80px'],
                        ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                    ];
        $dateTableFields = $columns;
        $dataTableId = time();
        // echo '<pre>';print_r($available_trucks);die();
        return view('shipper.pages.shipment.index',compact('booking','location_aray','booking_locations','booking_truck','manage_price','dataTableId','dateTableFields','available_trucks'));
    }

    public function calculate_distance($pick_location,$drop_location)
    {
        if ($pick_location != '' && $drop_location != '') {
            $distance_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($pick_location) . "&destinations=" . urlencode($drop_location) . "&mode=driving&key=" . env('MAP_KEY');

            $distance = (array) json_decode(file_get_contents($distance_url));
            if(isset($distance['rows']) && !empty($distance['rows']) && sizeof($distance['rows']) > 0 && isset($distance['rows'][0]->elements[0]->distance->value) && !empty($distance['rows'][0]->elements[0]->distance->value)){
                $km = (Float) $distance['rows'][0]->elements[0]->distance->value;
                $km = $km / 1000;
                $duration = $distance['rows'][0]->elements[0]->duration->text;
                $duration_value = $distance['rows'][0]->elements[0]->duration->value;
                // $duration = $duration / 60;
                // $duration = $duration / 60;
                // $data['miles'] = number_format($km * 0.621371,2); // kilometer to miles conversion
                $data['miles'] = number_format($km,1); // kilometer to miles conversion
                $data['journey'] = $duration;
                $data['journey_value'] = $duration_value;
            }else{
                $data['miles'] = 0;
                $data['journey'] = 0;    
                $data['journey_value'] = 0;    
            }
        } else {
            $data['miles'] = 0;
            $data['journey'] = 0;
            $data['journey_value'] = 0;
        }
        return $data;
    }

    /**
     *  returns the faqs add page
     */
    public function create()
    {        
       Session::put('running','running');
    //    Session::forget('truck_data');
       Session::forget('search_available_truck_detail');
    //    Session::forget('edit_truck_data');
    //    Session::forget('product_data');
    //    Session::forget('edit_product_data');
        $truckDetails = TruckType::where('status',1)->with(['category' => function ($q) {
            $q->active();
        }])->get()->toArray();
        $count = count($truckDetails);
        return view('shipper.pages.shipment.shipment_truck', compact('truckDetails','count'));
    }

    // public function create_truck()
    // {
       
        
    //    Session::put('running','running');

    //     $truckDetails = TruckType::where('status',1)->with(['category' => function ($q) {
    //         $q->active();
    //     }])->get()->toArray();
    //     $count = count($truckDetails);
    //     return view('shipper.pages.shipment.shipment_truck_design', compact('truckDetails','count'));
    // }

    public function create_step_2(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'truck_type' => 'required',
            'truck_categories' => 'required',
        ]);
        if ($validator->fails() && empty(Session::get('truck_data'))) {
            return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        }
        else
        {
            $data = $request->except('_token');
            $user = Auth::user();
            if($user['type'] == 'manager'){
                $productData = Product::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->orderBy('id','desc')->get()->toArray();
            }else{
                $productData = Product::where('user_id', $user->id)->select('*')->orderBy('id','desc')->get()->toArray();    
            }
            // $productData = Product::where('user_id', $user->id)->orderBy('id','desc')->get()->toArray();
            $packageData = PackageType::where('status',1)->get()->toArray();
            $unitData = TruckUnit::where('deleted_at',NULL)->orderBy('id','desc')->get()->toArray();
            $data['hydraulic'] = isset($data['hydraulic'])?$data['hydraulic']:0;
            if(empty(Session::get('truck_data')) || !empty($data['truck_type'])){
                Session::put('truck_data', $data);
            }
            $permission = 1;
            if($user->type == 'manager'){
                $permission = PermissionHelper::check_access('view_products');
            }
            // echo '<pre>';print_r(Session::get('truck_data'));die();
            return view('shipper.pages.shipment.shipment_product', compact('productData','packageData','unitData','permission'));
        }

        
    }

    public function create_step_3(Request $request) {
        // echo '<pre>';print_r(Session::get('product_data'));
        // echo '<pre>';print_r($request->all());
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'package' => 'required',
            'package_quantity' => 'required',
            'weight' => 'required',
            'unit' => 'required',
            // 'note' => 'required|string',
        ]);

        if ($validator->fails() && empty(Session::get('product_data'))) {
            return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        }else
        {
            if(empty(Session::get('truck_data'))){
                return redirect()->back()->with('error',__('Something went wrong please try again later!'));
            }else{
                // echo '<pre>';print_r(Session::get('product_data'));die();
                $user = Auth::user();
                $data = $request->except('_token');
                $unitData = TruckUnit::where('deleted_at',NULL)->get()->toArray();
                $packageData = PackageType::get()->toArray();
                $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
                $countries =  Country::whereIn('name', ['Greece'])->get();
                if($user['type'] == 'manager'){
                    // $productData = Product::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->orderBy('id','desc')->get()->toArray();
                    $addresses = Facility::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->with('city_obj')->orderBy('id','desc')->get();
                }else{
                    $addresses = Facility::where('user_id', $user->id)->select('*')->with('city_obj')->orderBy('id','desc')->get();
                }
                $count = count($addresses);
                
                if(!empty($data))
                {
                    // echo '<pre>';print_r(Session::get('product_data'));die();
                    $products = count($data) === 0 ? Session::get('product_data') : [];
                    $fragile = "0";
                    $sensitive = "0";
                    for($i=0;$i<count($data['product']);$i++)
                    {
                        $products[$i]['product_name'] = Product::whereId($data['product'][$i])->value('name');
                        $products[$i]['product_id'] = $data['product'][$i];
                        $products[$i]['package'] = $data['package'][$i];
                        $products[$i]['package_quantity'] = $data['package_quantity'][$i];
                        $products[$i]['weight'] = $data['weight'][$i];
                        $products[$i]['unit'] = $data['unit'][$i];
                        $products[$i]['product_description'] = $data['product_description'][$i];
    
                        $products[$i]['fragile'] = isset($data['fragile_'.$i])?1:0; 
                        $products[$i]['sensitive'] = isset($data['sensitive_'.$i])?1:0; 
                    }    
                    if(empty(Session::get('product_data')) || !empty($products))
                    {
                        Session::forget('product_data');
                        Session::put('product_data', $products);
                    }
                    $products = Session::get('product_data');
                    
                }
                else
                {
                    $products = Session::get('product_data');
                }
                $permission = 1;
                if($user->type == 'manager'){
                    $permission = PermissionHelper::check_access('view_address');
                }
                return view('shipper.pages.shipment.shipment_locations', compact('products','packageData','unitData','countries','addresses','count','permission'));
            }
        }

    }

    public function getProductData(Request $request) {
        $id = $request->get('id');
        $product_data = Product::findOrFail($id)->toArray();
        return json_encode($product_data);
    }

    public function getFacilityAddress(Request $request)
    {
        $user = Auth::user();
        if($user['type'] == 'manager'){
            $addresses = Facility::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->with('city_obj')->orderBy('id','desc')->get();
        }else{
            $addresses = Facility::where('user_id', $user->id)->select('*')->with('city_obj')->orderBy('id','desc')->get();
        }
        return json_encode($addresses);
    }



    public function store_step_3(Request $request) {
        // echo '<pre>';print_r($request->all());
        $validator = Validator::make($request->all(), [
            'pickupAddress' => 'required',
            'deliveryAddress' => 'required',
            'pickup_date' => 'required',
            'delivery_date' => 'required',
            // 'unit' => 'required',
            // 'note' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        }else{
            if(empty(Session::get('product_data')) || empty(Session::get('truck_data'))){
                return redirect(url('shipper/shipment/create'))->with('error',__('Something went wrong please try again later!'));
            }else{
                $data = $request->except('_token');
                // echo '<pre>';print_r($data);die();
                $user =   Auth::user();
                $pickup_address = Facility::where('id',$data['pickupAddress'])->first();
                $delivery_address = Facility::where('id',$data['deliveryAddress'])->first();
                $data['pickupAddress'] = $pickup_address['address'];
                $data['deliveryAddress'] = $delivery_address['address'];
    
                
                // if(isset($data['pickup_time_range_check']) && $data['pickup_time_range_check'] == '1'){
                //     $data['pickup_hour'] = '';
                //     $data['pickup_time_format'] = '';
                //     $pickup_hour_from = explode(':',$data['pickup_hour_from']);
                //     if($pickup_hour_from[0] >= '12'){
                //         $data['pickup_time_from'] = 'PM';
                //     }else{
                //         $data['pickup_time_from'] = 'AM';
                //     }
                //     $pickup_hour_to = explode(':',$data['pickup_hour_to']);
                //     if($pickup_hour_to[0] >= '12'){
                //         $data['pickup_time_to'] = 'PM';
                //     }else{
                //         $data['pickup_time_to'] = 'AM';
                //     }
                // }
                // if(isset($data['pickup_hour']) && $data['pickup_hour'] !== '')
                // {
                //     $data['pickup_hour_from'] = '';
                //     $data['pickup_time_from'] = '';
                //     $data['pickup_hour_to'] = '';
                //     $data['pickup_time_to'] = '';
                //     $pickup_hour = explode(':',$data['pickup_hour']);
                //     if($pickup_hour[0] >= '12'){
                //         $data['pickup_time_format'] = 'PM';
                //     }else{
                //         $data['pickup_time_format'] = 'AM';
                //     }
                // }
    
                // if(isset($data['delivery_time_range_check']) && $data['delivery_time_range_check'] == '1'){
                //     $data['delivery_hour'] = '';
                //     $data['delivery_time_format'] = '';
                //     $delivery_hour_from = explode(':',$data['delivery_hour_from']);
                //     if($delivery_hour_from[0] >= '12'){
                //         $data['delivery_time_from'] = 'PM';
                //     }else{
                //         $data['delivery_time_from'] = 'AM';
                //     }
                //     $delivery_hour_to = explode(':',$data['delivery_hour_to']);
                //     if($delivery_hour_to[0] >= '12'){
                //         $data['delivery_time_to'] = 'PM';
                //     }else{
                //         $data['delivery_time_to'] = 'AM';
                //     }
                // }
                // if(isset($data['delivery_hour']) && $data['delivery_hour'] !== ''){
                //     $data['delivery_hour_from'] = '';
                //     $data['delivery_hour_to'] = '';
                //     $delivery_hour = explode(':',$data['delivery_hour']);
                //     if($delivery_hour[0] >= '12'){
                //         $data['delivery_time_format'] = 'PM';
                //     }else{
                //         $data['delivery_time_format'] = 'AM';
                //     }
                // }
    
                Session::put('direct_shipment_data', $data);
                
                $pickup_time_from = $data['pickup_hour_from'] !== '' && !empty($data['pickup_hour_from']) ? $data['pickup_hour_from'] : $data['pickup_hour'];
                $pickup_time_to = $data['pickup_hour_to'] !== '' && !empty($data['pickup_hour_to']) ? $data['pickup_hour_to'] : $data['pickup_hour'];
    
                // echo '<pre>';print_r(date('h:i A',strtotime($data['pickup_hour'])));
                // echo '<pre>';print_r($pickup_time_from);die();
                //To calculate distance between coordinates
                //$distance = $this->distance('23.0225', '72.5715','22.7739','71.6673','K');
                $calculated_distance['miles'] = 0;
                $calculated_distance['journey'] = 0;
                if(!empty($data['pickupAddress']) && !empty($data['deliveryAddress'])){
                    $calculated_distance = $this->calculate_distance($data['pickupAddress'],$data['deliveryAddress']);
                }
                $journey_type = __("Long haul shipment");
                if(!empty($calculated_distance['miles'])){
                    if($calculated_distance['miles'] >= 0 && $calculated_distance['miles'] <= 50){
                        $journey_type = __("Short haul shipment");
                    }else if($calculated_distance['miles'] > 50 && $calculated_distance['miles'] <= 150){
                        $journey_type = __("Medium haul shipment");
                    }else if($calculated_distance['miles'] > 150){
                        $journey_type = __("Long haul shipment");
                    }
                }
                $booking = Booking::create([
                    'user_id' => $user->id,
                    'pickup_address' => $data['pickupAddress'],
                    'pickup_lat' => $pickup_address['address_lat'],
                    'pickup_lng' => $pickup_address['address_lng'],
                    'pickup_date' => $data['pickup_date'],
                    'pickup_time_from' => $pickup_time_from,
                    'pickup_time_to' => $pickup_time_to,
                    'distance' => (isset($calculated_distance['miles']) && !empty($calculated_distance['miles']) ? str_replace(',','',$calculated_distance['miles']) : '0'),
                    'journey' => (isset($calculated_distance['journey']) && !empty($calculated_distance['journey']) ? $calculated_distance['journey'] : '0'),
                    'journey_type' => $journey_type,
                    //'pickup_note' => $data['pickup_note'],
                ]);
    
                $truck_type_data = Session::get('truck_data');
                if(isset($truck_type_data['driver_detail']) && !empty($truck_type_data['driver_detail'])){
                    $t_type = $truck_type_data['truck_detail']['truck_type'];
                }else{
                    $t_type = $truck_type_data['truck_type'];
                }
                foreach($t_type as $truck_type){
                    $truck_data = [];
                    $truck_data['booking_id'] = $booking->id;
                    $truck_data['truck_type'] = $truck_type;
                    if(isset($truck_type_data['driver_detail']) && !empty($truck_type_data['driver_detail'])){
                        foreach($truck_type_data['truck_detail']['truck_categories'] as $key => $truck_cat){
                            if($key == $truck_type){
                                $truck_data['truck_type_category'] = implode(',',$truck_cat);
                            }
                        }
                        $truck_data['is_hydraulic'] = $truck_type_data['truck_detail']['hydraulic'];
                        $truck_data['note'] = $truck_type_data['truck_detail']['shipment_note'];
                    }else{
                        foreach($truck_type_data['truck_categories'] as $key => $truck_cat){
                            if($key == $truck_type){
                                $truck_data['truck_type_category'] = implode(',',$truck_cat);
                            }
                        }
                        $truck_data['is_hydraulic'] = $truck_type_data['hydraulic'];
                        $truck_data['note'] = $truck_type_data['shipment_note'];
                    }
                    
                    // $truck_data['is_cooling'] = $truck_type_data['cooling'];
    
                    BookingTruck::create($truck_data);
                }

                if(isset($truck_type_data['driver_detail']) && !empty($truck_type_data['driver_detail'])){
                    $search_available_driver_session['driver_detail'] = $truck_type_data['driver_detail']; 
                    $search_available_driver_session['availability_data'] = $truck_type_data['availability_data']; 
                    $search_available_driver_session['availability_data'] = $truck_type_data['availability_data']; 
                    $search_available_driver_session['driver_vehicle_detail'] = $truck_type_data['driver_vehicle_detail']; 
                    Session::put('search_available_truck_detail',$search_available_driver_session);
                }
    
                $delivery_time_from = $data['delivery_hour_from'] != '' && !empty($data['delivery_hour_from']) ? $data['delivery_hour_from'] : $data['delivery_hour'];
                $delivery_time_to = $data['delivery_hour_to'] != '' && !empty($data['delivery_hour_to']) ? $data['delivery_hour_to'] : $data['delivery_hour'];
    
                $booking_location_id_1 = BookingLocations::create([
                    'booking_id' => $booking->id,
                    'drop_location' => $data['pickupAddress'],
                    'company_name' => $pickup_address['name'],
                    'delivered_at' => $data['pickup_date'],
                    'drop_lat' => $pickup_address['address_lat'],
                    'drop_lng' => $pickup_address['address_lng'],
                    'note' => $data['pickup_note'],
                    'is_pickup' => 1,
                    'delivery_time_from' => $pickup_time_from,
                    'delivery_time_to' => $pickup_time_to,
                ])->id;
    
                $booking_location_id_2 = BookingLocations::create([
                    'booking_id' => $booking->id,
                    'drop_location' => $data['deliveryAddress'],
                    'delivered_at' => $data['delivery_date'],
                    'company_name' => $delivery_address['name'],
                    'drop_lat' => $delivery_address['address_lat'],
                    'drop_lng' => $delivery_address['address_lng'],
                    'note' => $data['delivery_note'],
                    'is_pickup' => 0,
                    'delivery_time_from' => $delivery_time_from,
                    'delivery_time_to' => $delivery_time_to,
                    'distance' => (isset($calculated_distance['miles']) && !empty($calculated_distance['miles']) ? str_replace(',','',$calculated_distance['miles']) : ''),
                    'journey' => (isset($calculated_distance['journey']) && !empty($calculated_distance['journey']) ? $calculated_distance['journey'] : ''),
                ])->id;
    
                $products = Session::get('product_data');
                foreach($products as $product)
                {
                    $productDetails = Product::findOrFail($product['product_id'])->toArray();
                    BookingProduct::create([
                        'booking_id' => $booking->id,
                        'location_id' => $booking_location_id_1,
                        'product_id' => $product['product_id'],
                        'product_type' => $product['package'],
                        'qty' => $product['package_quantity'],
                        'weight' => $product['weight'],
                        'unit' => $product['unit'],
                        'note' => $product['product_description'],
                        'is_fragile' => $product['fragile'],
                        'is_sensetive' => $product['sensitive'],
                        'is_pickup' => '1',
                    ]);
    
                    BookingProduct::create([
                        'booking_id' => $booking->id,
                        'location_id' => $booking_location_id_2,
                        'product_id' => $product['product_id'],
                        'product_type' => $product['package'],
                        'qty' => $product['package_quantity'],
                        'weight' => $product['weight'],
                        'unit' => $product['unit'],
                        'note' => $product['product_description'],
                        'is_fragile' => $product['fragile'],
                        'is_sensetive' => $product['sensitive'],
                    ]);
    
                }
                $truck_session_data = Session::get('truck_data');
                if(isset($truck_session_data['driver_detail']) && !empty($truck_session_data['driver_detail'])){
                    Session::put('edit_truck_data',$truck_session_data);
                }
                Session::forget('truck_data');
                Session::forget('product_data');
                return redirect('shipper/shipment/search/'.$booking->id)->with('success',__('Shipment Created Successfully'));
            }
        }

        

        // echo "<pre>";
        // print_r(Session::get('truck_data'));
        // echo "</pre>";
        // echo "<br>";
        // echo "<pre>";
        // print_r(Session::get('product_data'));
        // echo "</pre>";
        // echo "<br>";
        // echo "<pre>";
        // print_r(Session::get('direct_shipment_data'));exit;
        // return view('shipper.pages.shipment.create-shipment-pickup-and-delivery-infomation-direct-shipment', compact('products','unitData','countries','states','cities','addresses','count'));
    }

    public function store_multiple_location(Request $request)
    {
        $data = $request->except('_token');
        // echo '<pre>';print_r($data);die();
        $pickup_address = Facility::where('id',$data['location_1'])->first();
        $delivery_address = Facility::where('id',$data['location_2'])->first();
        // $distance = $this->SimpleDistance($pickup_address['address_lat'],$pickup_address['address_lng'],$delivery_address['address_lat'],$delivery_address['address_lng'],'K');
        // echo '<pre>';print_r($products);
        // echo '<pre>';print_r(array_search('63',$products));
        // echo '<pre>';print_r();
        // echo '<pre>';print_r($data);
        // die();
        if(isset($data['multiple_shipment_location_length']) && !empty($data['multiple_shipment_location_length'])){
            $array_validation = [];
            for($i=1;$i<=$data['multiple_shipment_location_length'];$i++){
                $array_validation['location_'.$i] = 'required';
                $array_validation['date_'.$i] = 'required';
            }
            if(isset($array_validation) && !empty($array_validation)){
                $validator = Validator::make($request->all(), $array_validation);
            }

            if(empty(Session::get('product_data')) || empty(Session::get('truck_data'))){
                return redirect()->back()->with('error',__('Something went wrong please try again later!'));
            }else{
                $user =   Auth::user();

                $location = Facility::where('id',$data['location_1'])->first();
                    // $delivery_address = Facility::where('id',$data['deliveryAddress'])->first();
                    // $data['pickupAddress'] = $pickup_address['address'];
                    // $data['deliveryAddress'] = $delivery_address['address'];

                    // if(isset($data['location_time_range_check_1']) && $data['location_time_range_check_1'] == '1'){
                    //     $data['location_hour_1'] = '';
                    //     $data['location_time_format_1'] = '';
                    //     $pickup_hour_from = explode(':',$data['location_hour_from_1']);
                    //     if($pickup_hour_from[0] >= '12'){
                    //         $data['location_time_from_1'] = 'PM';
                    //     }else{
                    //         $data['location_time_from_1'] = 'AM';
                    //     }

                    //     $pickup_hour_to = explode(':',$data['location_hour_to_1']);
                    //     if($pickup_hour_to[0] >= '12'){
                    //         $data['location_time_to_1'] = 'PM';
                    //     }else{
                    //         $data['location_time_to_1'] = 'AM';
                    //     }
                    // }
                    // if(isset($data['location_hour_1']) && $data['location_hour_1'] !== '')
                    // {
                    //     $data['location_hour_from_1'] = '';
                    //     $data['location_time_from_1'] = '';
                    //     $data['location_hour_to_1'] = '';
                    //     $data['location_time_to_1'] = '';
                    //     $pickup_hour = explode(':',$data['location_hour_1']);
                    //     if($pickup_hour[0] >= '12'){
                    //         $data['location_time_format_1'] = 'PM';
                    //     }else{
                    //         $data['location_time_format_1'] = 'AM';
                    //     }
                    // }

                    // Session::put('multiple_shipment_data', $data);

                    $pickup_time_from = $data['location_hour_from_1'] !== '' && !empty($data['location_hour_from_1']) ? $data['location_hour_from_1'] : $data['location_hour_1'];
                    $pickup_time_to = $data['location_hour_to_1'] !== '' && !empty($data['location_hour_to_1']) ? $data['location_hour_to_1'] : $data['location_hour_1'];

                    //To calculate distance between coordinates
                    // $distance['miles'] = 0;
                    // $distance['journey'] = 0;
                    // if(isset($location) && !empty($location) && !empty($previous_address) && isset($previous_address)){
                    //     $distance = $this->distance($previous_address['address_lat'], $previous_address['address_lng'],$location['address_lat'],$location['address_lng'],'K');
                    // }

                $booking = Booking::create([
                    'user_id' => $user->id,
                    'pickup_address' => (!empty($location['address']) && isset($location['address']) ? $location['address'] : '-'),
                    'pickup_lat' => (!empty($location['address_lat']) && isset($location['address_lat']) ? $location['address_lat'] : '-'),
                    'pickup_lng' => (!empty($location['address_lng']) && isset($location['address_lng']) ? $location['address_lng'] : '-'),
                    'pickup_date' => $data['date_1'],
                    'pickup_time_from' => $pickup_time_from,
                    'pickup_time_to' => $pickup_time_to,
                    'distance' => '0',
                    'journey' => '0',
                    'journey_type' => '',
                    'booking_type' => 'multiple_shipment',
                    //'pickup_note' => $data['pickup_note'],
                ]);

                $truck_type_data = Session::get('truck_data');
                $t_type = $truck_type_data['truck_type'];
                foreach($t_type as $truck_type){
                    $truck_data = [];
                    $truck_data['booking_id'] = $booking->id;
                    $truck_data['truck_type'] = $truck_type;
                    foreach($truck_type_data['truck_categories'] as $key => $truck_cat){
                        if($key == $truck_type){
                            $truck_data['truck_type_category'] = implode(',',$truck_cat);
                        }
                    }
                    $truck_data['is_hydraulic'] = $truck_type_data['hydraulic'];
                    // $truck_data['is_cooling'] = $truck_type_data['cooling'];
                    $truck_data['note'] = $truck_type_data['shipment_note'];
    
                    BookingTruck::create($truck_data);
                }

                $previous_address_lat = '';
                $previous_address_lng = '';
                $previous_address = '';
                $total_distance = 0;
                $total_journey = 0;
                for($i=1;$i<=$data['multiple_shipment_location_length'];$i++){
                    $location_new = Facility::where('id',$data['location_'.$i])->first();

                    // if(isset($data['location_time_range_check_'.$i]) && $data['location_time_range_check_'.$i] == '1'){
                    //     $data['location_hour_'.$i] = '';
                    //     $data['location_time_format_'.$i] = '';
                    //     $pickup_hour_from = explode(':',$data['location_hour_from_'.$i]);
                    //     if($pickup_hour_from[0] >= '12'){
                    //         $data['location_time_from_'.$i] = 'PM';
                    //     }else{
                    //         $data['location_time_from_'.$i] = 'AM';
                    //     }

                    //     $pickup_hour_to = explode(':',$data['location_hour_to_'.$i]);
                    //     if($pickup_hour_to[0] >= '12'){
                    //         $data['location_time_to_'.$i] = 'PM';
                    //     }else{
                    //         $data['location_time_to_'.$i] = 'AM';
                    //     }
                    // }
                    // if(isset($data['location_hour_'.$i]) && $data['location_hour_'.$i] !== '')
                    // {
                    //     $data['location_hour_from_'.$i] = '';
                    //     $data['location_time_from_'.$i] = '';
                    //     $data['location_hour_to_'.$i] = '';
                    //     $data['location_time_to_'.$i] = '';
                    //     $pickup_hour = explode(':',$data['location_hour_'.$i]);
                    //     if($pickup_hour[0] >= '12'){
                    //         $data['location_time_format_'.$i] = 'PM';
                    //     }else{
                    //         $data['location_time_format_'.$i] = 'AM';
                    //     }
                    // }
                    $pickup_time_from = $data['location_hour_from_'.$i] !== '' && !empty($data['location_hour_from_'.$i]) ? $data['location_hour_from_'.$i] : $data['location_hour_'.$i];
                    $pickup_time_to = $data['location_hour_to_'.$i] !== '' && !empty($data['location_hour_to_'.$i]) ? $data['location_hour_to_'.$i] : $data['location_hour_'.$i];

                    // To calculate distance between coordinates
                    $calculated_distance['miles'] = '0';
                    $calculated_distance['journey'] = '0';
                    $calculated_distance['journey_value'] = '0';
                    if(isset($location_new) && !empty($location_new) && !empty($previous_address) && isset($previous_address)){
                        // $distance = $this->distance($previous_address_lat, $previous_address_lng,$location_new['address_lat'],$location_new['address_lng'],'K');
                        $calculated_distance = $this->calculate_distance($previous_address,$location_new['address']);
                    }
                    // echo '<pre>';print_r($location_new);
                    $previous_address = $location_new['address'];
                    // echo '<pre>';print_r($previous_address);
                    // $previous_address_lng = $location_new['address_lng'];
                    $total_distance += str_replace(',','',$calculated_distance['miles']);
                    $total_journey += $calculated_distance['journey_value'];
                    $booking_location_id = BookingLocations::create([
                        'booking_id' => $booking->id,
                        'drop_location' => (!empty($location_new['address']) && isset($location_new['address']) ? $location_new['address'] : '-'),
                        'company_name' => (!empty($location_new['name']) && isset($location_new['name']) ? $location_new['name'] : '-'),
                        'delivered_at' => $data['date_'.$i],
                        'drop_lat' => (!empty($location_new['address_lat']) && isset($location_new['address_lat']) ? $location_new['address_lat'] : '-'),
                        'drop_lng' => (!empty($location_new['address_lng']) && isset($location_new['address_lng']) ? $location_new['address_lng'] : '-'),
                        'note' => $data['locationNote_'.$i],
                        'is_pickup' => ($i == '1' ? '1' : '0'),
                        'delivery_time_from' => $pickup_time_from,
                        'delivery_time_to' => $pickup_time_to,
                        'distance' => (isset($calculated_distance['miles']) && !empty($calculated_distance['miles']) ? str_replace(',','',$calculated_distance['miles']) : '0'),
                        'journey' => (isset($calculated_distance['journey']) && !empty($calculated_distance['journey']) ? $calculated_distance['journey'] : '0'),
                    ])->id;
                    
                    if(isset($data['location_products_'.$i]) && !empty($data['location_products_'.$i])){
                        $products = Session::get('product_data');
                        foreach($data['location_products_'.$i] as $key => $value){
                            $products_key = array_search($value, array_column($products, 'product_id'));
                            // $productDetails = Product::findOrFail($data['location_products_'.$i])->toArray();
                            BookingProduct::create([
                                'booking_id' => $booking->id,
                                'location_id' => $booking_location_id,
                                'product_id' => $value,
                                'product_type' => $products[$products_key]['package'],
                                'qty' => $data['quantity_'.$i][$key],
                                'weight' => $data['weight_'.$i][$key],
                                'unit' => $data['unit_'.$i][$key],
                                'note' => $products[$products_key]['product_description'],
                                'is_fragile' => $products[$products_key]['fragile'],
                                'is_sensetive' => $products[$products_key]['sensitive'],
                                'is_pickup' => ($data['shipment_type_'.$i][$key] == 'pickup' ? '1' : '0')
                            ]);
                        }
                    }
                    $journey_type = __("Long haul shipment");
                    if(!empty($total_distance)){
                        if($total_distance >= 0 && $total_distance <= 50){
                            $journey_type = __("Short haul shipment");
                        }else if($total_distance > 50 && $total_distance <= 150){
                            $journey_type = __("Medium haul shipment");
                        }else if($total_distance > 150){
                            $journey_type = __("Long haul shipment");
                        }
                    }
                    $total_journey_final = $this->secondsToTime($total_journey);
                    $total_journey_string = (isset($total_journey_final['h']) && $total_journey_final['h'] != 0 ? $total_journey_final['h'].' hours' : '').' '.(isset($total_journey_final['m']) && $total_journey_final['m'] != 0 ? $total_journey_final['m'].' mins' : '');
                    Booking::whereId($booking->id)->update(['distance' => number_format($total_distance,2),'journey_type' => $journey_type, 'journey' => $total_journey_string]);
                    // $previous_address = $location_new;
                }
                Session::forget('truck_data');
                Session::forget('product_data');
                return redirect('shipper/shipment/search/'.$booking->id)->with('success',__('Shipment Created Successfully'));
            }
        }

        // if ($validator->fails()) {
        //     return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        // }else{
            
        // }
    }

    function secondsToTime($seconds) {

        // extract hours
        $hours = floor($seconds / (60 * 60));
      
        // extract minutes
        $divisor_for_minutes = $seconds % (60 * 60);
        $minutes = floor($divisor_for_minutes / 60);
      
        // extract the remaining seconds
        $divisor_for_seconds = $divisor_for_minutes % 60;
        $seconds = ceil($divisor_for_seconds);
      
        // return the final array
        $obj = array(
            "h" => (int) $hours,
            "m" => (int) $minutes,
            "s" => (int) $seconds,
         );
      
        return $obj;
      }

    public function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
      
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
      
        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
          cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    public function addAddress(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            // 'state' => 'required',
            // 'country' => 'required',
            // 'postcode' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email',

        ],[
            'name.required' => 'please enter name',
            'address.required' => 'please enter address',
            'lat.required' => 'please select address using google suggestion',
            'lng.required' => 'please select address using google suggestion',
            'phone.required' => 'please enter phone',
            'email.required' => 'please enter email',
        ]);

        if ($validator->fails()) {
            $response['status'] = false;
            $response['message'] = $validator->errors();
            // echo json_encode($response);
            return response()->json($response);
            // return redirect()->back()
            //             ->withErrors($validator->errors()->first())
            //             ->withInput();
        }
        $user = Auth::user();
        $data = $request->all();

        $address = new Facility();
        if($user['type'] == 'manager'){
            $address->user_id = $user->company_ref_id;
            $address->insert_by = $user->id;
        }else{
            $address->user_id = $user->id;
            $address->insert_by = $user->id;
        }
        
        $address->name = $data['name'];
        $address->address = $data['address'];
        $address->address_lat = $data['lat'];
        $address->address_lng = $data['lng'];
        $address->city = (isset($data['city']) && !empty($data['city']) ? $data['city'] : '');
        $address->state = (isset($data['state']) && !empty($data['state']) ? $data['state'] : '');
        $address->country = (isset($data['country']) && !empty($data['country']) ? $data['country'] : '');
        $address->zipcode = (isset($data['postcode']) && !empty($data['postcode']) ? $data['postcode'] : '');
        $address->phone = $data['phone'];
        $address->email = $data['email'];

        $address->restroom_service = $request->get('restroom_service') == null ? '0' : '1';
        $address->food_service = $request->get('food_service') == null ? '0' : '1';
        $address->rest_area_service = $request->get('rest_area_service') == null ? '0' : '1';

        if($request->get('restroom_service') == '1'){
            $address->restroom_service = "1";
        }
        if($request->get('food_service') == '1'){
            $address->food_service = "1";
        }
        if($request->get('rest_area_service') == '1'){
            $address->rest_area_service = "1";
        }

        $address->save();
        $response['message'] = __('Address added Successfully');
        echo json_encode($response);

        // return redirect()->back()->with('success',__('Address added Successfully'));
    }

    public function addProduct(Request $request)
    {
        // echo '<pre>';print_r($request->all());die();
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sku_type' => 'required',
        ],[
            'required' => 'This value is required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors()->first())
                        ->withInput();
        }
        $user = Auth::user();
        $data = $request->all();
        $product = new Product();
        if($user['type'] == 'manager'){
            $product->user_id = $user->company_ref_id;
            $product->insert_by = $user->id;
        }else{
            $product->user_id = $user->id;
            $product->insert_by = $user->id;
        }
        $product->name = $data['name'];
        $product->sku_type = $data['sku_type'];
        $product->save();

        $product->sku_no = $data['sku_no'];
        $product->sku_name = $product->id . $product->sku_no;
        $product->update();
        $response['message'] = __('Product added Successfully');
        echo json_encode($response);
    }

    public function getProducts()
    {
        $user = Auth::user();
        if($user['type'] == 'manager'){
            $products = Product::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->orderBy('id','desc')->get()->toArray();
        }else{
            $products = Product::where('user_id', $user->id)->select('*')->orderBy('id','desc')->get()->toArray();    
        }
        // $products = Product::where('user_id', $user->id)->select('*')->orderBy('id','desc')->limit(1)->get();
        return json_encode($products);
    }

    public function public_shipment(Request $request,$id){
        
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
        ],[
            'required' => 'This value is required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors()->first())
                        ->withInput();
        }
        $data = $request->all();
        $booking = Booking::where('id', $id)->first();
        if(isset($data['all_bidding']) && !empty($data['all_bidding']) && $data['all_bidding'] == 'on'){
            $is_bid = 1;
        }else{
            $is_bid = 0;
        }

        if(isset($data['public_shipment']) && !empty($data['public_shipment']) && $data['public_shipment'] == '4'){
            $is_public = 1;
            $booking_truck_ids = BookingTruck::where(['booking_id' => $id])->pluck('truck_type')->toArray();
            if(isset($booking_truck_ids) && !empty($booking_truck_ids)){
                $driver_ids = DriverVehicle::whereIn('truck_type',$booking_truck_ids)->pluck('user_id')->toArray();
                if(isset($driver_ids) && !empty($driver_ids)){
                    $device_tokens = Device::whereIn('user_id',$driver_ids)->get();
                    if(isset($device_tokens) && !empty($device_tokens)){
                        foreach($device_tokens as $key => $value){
                            CommonHelper::send_push($value['type'],$value['token'],'New Shipment Available','Shipper creates new shipment','new_shipment',[],$value['user_id']);        
                        }
                    }                    
                }
            }
        }else{
            $is_public = 0;
        }
        $booking->is_bid = $is_bid;
        $booking->amount = $data['amount'];
        $booking->status = 'pending';
        $booking->is_public = $is_public;
        $booking->update();
        $user = Auth::user();
        if(!empty(Session::get('search_available_truck_detail'))){
            $search_available_data = Session::get('search_available_truck_detail');
            if(isset($search_available_data) && !empty($search_available_data)){
                $request_data['booking_id'] = $id;
                $request_data['driver_id'] = $search_available_data['availability_data']['driver_id'];
                if($user['type'] == 'manager'){
                    $request_data['shipper_id'] = $user['company_ref_id'];
                }else{
                    $request_data['shipper_id'] = $user['id'];
                }
                $request_data['availability_id'] = $search_available_data['availability_data']['id'];
                $request_data['amount'] = $data['amount'];
                $request_data['status'] = 0;
                $request_data['is_bid'] = $search_available_data['availability_data']['is_bid'];
                $request_data['request_time'] = date('Y-m-d H:i:s');
                BookingRequest::create($request_data);
                // BookingTruck::where('booking_id',$id)->where('truck_type',$search_available_data['availability_data']['truck_type_id'])->update(['driver_id' => $search_available_data['availability_data']['driver_id']]);
                // Booking::where('id', $id)->update(['status'=>'scheduled']);
                // Availability::where('id', $search_available_data['availability_data']['id'])->update(['load_status'=>'1']);
                Session::forget('search_available_truck_detail');
            }
        }

        return redirect(url('shipper/manage-shipment'))->with('success',__('Your shipment is public'));
    }

    public function live_search()
    {
        return view('shipper.pages.shipment.live_search');
    }

    public function selectSearch(Request $request)
    {
    	$movies = [];

        if($request->has('q')){
            $search = $request->q;
            $movies =Movie::select("id", "name")
            		->where('name', 'LIKE', "%$search%")
            		->get();
        }
        return response()->json($movies);
    }

    public function track_shipment_link(Request $request)
    {
        // echo '<pre>';print_r($request->all());die();
        $validator = Validator::make($request->all(), [
            'share_track_link' => 'required',
            'email' => 'required',
        ],[
            'required' => 'This value is required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors()->first())
                        ->withInput();
        }
        $data = $request->all();
        $booking = Booking::where('id', $data['booking_id'])->first();
        if(isset($booking) && !empty($booking)){
            $booking->is_tracking = $data['share_track_link'];
            $booking->email_or_number = $data['email'];
            $booking->update();
             $data['email_or_number']=$data['email'];
            $data['booking_id']=$data['booking_id'];

            dispatch(new \App\Jobs\ShipperSharEmailJobs($data));
            // return redirect()->back()->with('error',__('Thank you for submitting data'));
            $response['message'] = __('Thank you for submitting data');
            echo json_encode($response);
        }else{
            // return redirect()->back()->with('error',__('Something went wrong'));
            $response['message'] = __('Something went wrong');
            echo json_encode($response);
        }
    }

    public function edit($id){
        $check_is_exists_bid_request = BookingBid::where('booking_id',$id)->where('status', '0')->exists();
        if($check_is_exists_bid_request){
            return redirect(url('shipper/manage-shipment'))->with('error',__('Bid or book request sent you can not edit shipment.'));
        }
        if(!empty($id) && isset($id)){
            $is_exists = Booking::whereId($id)->exists();
            if($is_exists){
                // Session::forget('edit_truck_data');
                // Session::forget('edit_product_data');
                $booking_trucks = BookingTruck::where('booking_id',$id)->get()->toArray();
                // echo '<pre>';print_r($booking_trucks);die();
                $truck_category = [];
                $truck_subcategory = [];
                foreach($booking_trucks as $truck){
                    $truck_category[] = $truck['truck_type'];
                    if(isset($truck['truck_type_category']) && !empty($truck['truck_type_category'])){
                        foreach($truck['truck_type_category'] as $subcategory){
                            $truck_subcategory[] = $subcategory['id'];
                        }
                    }
                }
                $truckDetails = TruckType::where('status',1)->with(['category' => function ($q) {
                    $q->active();
                }])->get()->toArray();
                $count = count($truckDetails);
                return view('shipper.pages.shipment.shipment_truck_edit', compact('truckDetails','count','truck_category','truck_subcategory','id','booking_trucks'));
            }
        }else{
            return redirect()->back()->with('error',__('Something went wrong'));
        }
    }

    public function edit_step_2(Request $request, $id)
    {
        // echo '<pre>';print_r();die();
        // echo '<pre>';print_r($request->all());die();
        $quote = $request->input('quote');
        if(isset($quote) && !empty($quote) && $quote == '1'){
            $old_session_data = Session::get('edit_truck_data');
            $session_data = array();
            $booking_trucks = BookingTruck::where('booking_id',$id)->get()->toArray();
            if(isset($booking_trucks) && !empty($booking_trucks)){
                foreach($booking_trucks as $key => $value){
                    $session_data['truck_type'][] = $value['truck_type'];
                    if(isset($value['truck_type_category']) && !empty($value['truck_type_category'])){
                        foreach($value['truck_type_category'] as $category){
                            $session_data['truck_categories'][$value['truck_type']][] = $category['id'];
                        }
                    }
                    $session_data['shipment_note'] = $value['note'];
                    $session_data['hydraulic'] = $value['is_hydraulic'];
                    if(isset($old_session_data['driver_detail']) && !empty($old_session_data['driver_detail'])){
                        $session_data['driver_detail'] = $old_session_data['driver_detail'];
                        $session_data['availability_data'] = $old_session_data['availability_data'];
                        $session_data['driver_vehicle_detail'] = $old_session_data['driver_vehicle_detail'];
                    }
                }

            }
            if(isset($session_data) && !empty($session_data)){
                Session::put('edit_truck_data', $session_data);
            }
        }
        $validator = Validator::make($request->all(), [
            'truck_type' => 'required',
            'truck_categories' => 'required',
        ]);
        if ($validator->fails() && empty(Session::get('edit_truck_data'))) {
            return redirect(url('shipper/manage-shipment'))->with('error',__('Something went wrong please try again later!'));
        }
        else
        {
            $data = $request->except('_token');
            $user = Auth::user();
            // $productData = Product::where('user_id', $user->id)->orderBy('id','desc')->get()->toArray();
            if($user['type'] == 'manager'){
                $productData = Product::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->orderBy('id','desc')->get()->toArray();
            }else{
                $productData = Product::where('user_id', $user->id)->select('*')->orderBy('id','desc')->get()->toArray();    
            }
            $packageData = PackageType::where('status',1)->get()->toArray();
            $unitData = TruckUnit::where('deleted_at',NULL)->orderBy('id','desc')->get()->toArray();
            $data['hydraulic'] = isset($data['hydraulic'])?$data['hydraulic']:0;
            // echo '<pre>';print_r($data);die();
            if(empty(Session::get('edit_truck_data')) || !empty($data['truck_type'])){
                Session::put('edit_truck_data', $data);
            }
            // $inserted_products = BookingProduct::where('booking_id',$id)->where('is_pickup','1')->get()->toArray();
             $inserted_products = BookingProduct::select('booking_product.*')->selectRaw('SUM(qty) as qty')->selectRaw('SUM(weight) as weight')->where('booking_id',$id)->where('is_pickup','1')->groupBy('product_id')->get()->toArray();
            //  echo '<pre>';print_r($inserted_products);die();
            return view('shipper.pages.shipment.shipment_product_edit', compact('productData','packageData','unitData','inserted_products','id'));
        }
    }

    public function edit_step_3(Request $request,$id) {
        
        // echo '<pre>';print_r(Session::get('truck_data'));die();
        $quote = $request->input('quote');
        if(isset($quote) && !empty($quote) && $quote == '1'){
            
            // echo '<pre>';print_r($inserted_products);die();
            $old_session_data = Session::get('edit_truck_data');
            $session_data = array();
            $booking_trucks = BookingTruck::where('booking_id',$id)->get()->toArray();
            // echo '<pre>';print_r($id);die();
            if(isset($booking_trucks) && !empty($booking_trucks)){
                foreach($booking_trucks as $key => $value){
                    $session_data['truck_type'][] = $value['truck_type'];
                    if(isset($value['truck_type_category']) && !empty($value['truck_type_category'])){
                        foreach($value['truck_type_category'] as $category){
                            $session_data['truck_categories'][$value['truck_type']][] = $category['id'];
                        }
                    }
                    $session_data['shipment_note'] = $value['note'];
                    $session_data['hydraulic'] = $value['is_hydraulic'];
                    if(isset($old_session_data['driver_detail']) && !empty($old_session_data['driver_detail'])){
                        $session_data['driver_detail'] = $old_session_data['driver_detail'];
                        $session_data['availability_data'] = $old_session_data['availability_data'];
                        $session_data['driver_vehicle_detail'] = $old_session_data['driver_vehicle_detail'];
                    }
                }

            }
            if(isset($session_data) && !empty($session_data)){
                Session::put('edit_truck_data', $session_data);
            }
            $inserted_products = BookingProduct::select('booking_product.*')->selectRaw('SUM(qty) as qty')->selectRaw('SUM(weight) as weight')->where('booking_id',$id)->where('is_pickup','1')->groupBy('product_id')->get()->toArray();
            $final_data = [];
            if(isset($inserted_products) && !empty($inserted_products)){
                foreach($inserted_products as $product){
                    $session_product_data['product_name'] = $product['product_id']['name'];
                    $session_product_data['product_id'] = $product['product_id']['id'];
                    $session_product_data['package'] = $product['product_type']['id'];
                    $session_product_data['package_quantity'] = $product['qty'];
                    $session_product_data['weight'] = $product['weight'];
                    $session_product_data['unit'] = $product['unit']['id'];
                    $session_product_data['product_description'] = $product['note'];
                    $session_product_data['fragile'] = isset($product['is_fragile']) && $product['is_fragile'] == 1 ? 1 : 0;
                    $session_product_data['sensitive'] = isset($product['is_sensetive']) && $product['is_sensetive'] == 1 ? 1 : 0;
                    $final_data[] = $session_product_data;
                }
            }
            if(isset($final_data) && !empty($final_data)){
                Session::put('edit_product_data', $final_data);
            }
        }

        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'package' => 'required',
            'package_quantity' => 'required',
            'weight' => 'required',
            'unit' => 'required',
            // 'note' => 'required|string',
        ]);

        if ($validator->fails() && empty(Session::get('edit_product_data'))) {
            return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        }else
        {
            if(empty(Session::get('edit_truck_data'))){
                return redirect()->back()->with('error',__('Something went wrong please try again later!'));
            }else{
                $user = Auth::user();
                $data = $request->except('_token');
                $unitData = TruckUnit::where('deleted_at',NULL)->get()->toArray();
                $packageData = PackageType::get()->toArray();
                $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
                $countries =  Country::whereIn('name', ['Greece'])->get();
                if($user['type'] == 'manager'){
                    $addresses = Facility::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->with('city_obj')->orderBy('id','desc')->get();
                }else{
                    $addresses = Facility::where('user_id', $user->id)->select('*')->with('city_obj')->orderBy('id','desc')->get();
                }
                $count = count($addresses);
                unset($data['quote']);
                // echo '<pre>';print_r($data);die();
                if(!empty($data))
                {
                    $products = count($data) === 0 ? Session::get('edit_product_data') : [];
                    $fragile = "0";
                    $sensitive = "0";
                    for($i=0;$i<count($data['product']);$i++)
                    {
                        $products[$i]['product_name'] = Product::whereId($data['product'][$i])->value('name');
                        $products[$i]['product_id'] = $data['product'][$i];
                        $products[$i]['package'] = $data['package'][$i];
                        $products[$i]['package_quantity'] = $data['package_quantity'][$i];
                        $products[$i]['weight'] = $data['weight'][$i];
                        $products[$i]['unit'] = $data['unit'][$i];
                        $products[$i]['product_description'] = $data['product_description'][$i];
                        $products[$i]['fragile'] = isset($data['fragile_'.$i])?1:0; 
                        $products[$i]['sensitive'] = isset($data['sensitive_'.$i])?1:0; 
                    }    
                    if(empty(Session::get('edit_product_data')) || !empty($data))
                    {
                        Session::put('edit_product_data', $products);
                    }
                    $products = Session::get('edit_product_data');
                }
                else
                {
                    $products = Session::get('edit_product_data');
                }
                $booking_data = Booking::where('id',$id)->first();
                $booking_locations_data = BookingLocations::where('booking_id',$id)->get()->toArray();
                $final_products = [];
                $actual_product_quantity = [];
                if(isset($products) && !empty($products)){
                    if(isset($booking_locations_data) && !empty($booking_locations_data)){
                        foreach($booking_locations_data as $k => $booking_locations){
                            $final_products[] = $booking_locations;
                            // echo '<pre>';print_r($booking_locations['products']);die();
                            $existing_products_id = array_column(array_column($booking_locations['products'],'product_id'),'id');
                            foreach($products as $s => $session_product){
                                
                                $actual_data['package_quantity'] = $session_product['package_quantity'];
                                $actual_data['weight'] = $session_product['weight'];
                                $actual_product_quantity[$session_product['product_id']] = $actual_data;

                                if(!in_array($session_product['product_id'],$existing_products_id)){
                                    $push_product['product_id'] = ['id' => $session_product['product_id'],'name' => $session_product['product_name']];
                                    $push_product['product_type'] = ['id' => $session_product['package']];
                                    $push_product['qty'] = $session_product['package_quantity'];
                                    $push_product['weight'] = $session_product['weight'];
                                    $push_product['unit'] = ['id' => $session_product['unit']];
                                    $push_product['note'] = $session_product['product_description'];
                                    $push_product['is_fragile'] = $session_product['fragile'];
                                    $push_product['is_sensetive'] = $session_product['sensitive'];
                                    $push_product['is_pickup'] = '1';
                                    $push_product['is_inserted'] = '0';
                                    // echo '<pre>';print_r($booking_locations['id']);
                                    // echo '<pre>';print_r($push_product);
                                    // echo '<pre>';print_r($k);
                                    $final_products[$k]['products'][] = $push_product;
                                }
                            }
                            array_multisort(array_column($final_products[$k]['products'],'product_id'),SORT_ASC,$final_products[$k]['products']);
                        }
                    }       
                }
                if(isset($final_products) && !empty($final_products)){
                    $booking_locations_data = $final_products;
                }
                // echo '<pre>';print_r(Session::get('edit_product_data'));
                // die();
                return view('shipper.pages.shipment.shipment_locations_edit', compact('products','packageData','unitData','countries','addresses','count','id','booking_data','booking_locations_data','actual_product_quantity'));
            }
        }

    }

    public function update_step_3(Request $request,$id) {
        $validator = Validator::make($request->all(), [
            'pickupAddress' => 'required',
            'deliveryAddress' => 'required',
            'pickup_date' => 'required',
            'delivery_date' => 'required',
            // 'unit' => 'required',
            // 'note' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        }else{
            if(empty(Session::get('edit_product_data')) || empty(Session::get('edit_truck_data'))){
                return redirect()->back()->with('error',__('Something went wrong please try again later!'));
            }else{
                $data = $request->except('_token');
                // echo '<pre>';print_r($data);die();
                $user =   Auth::user();
                $pickup_address = Facility::where('id',$data['pickupAddress'])->first();
                $delivery_address = Facility::where('id',$data['deliveryAddress'])->first();
                $data['pickupAddress'] = $pickup_address['address'];
                $data['deliveryAddress'] = $delivery_address['address'];
    
                
                // if(isset($data['pickup_time_range_check']) && $data['pickup_time_range_check'] == '1'){
                //     $data['pickup_hour'] = '';
                //     $data['pickup_time_format'] = '';
                //     $pickup_hour_from = explode(':',$data['pickup_hour_from']);
                //     if($pickup_hour_from[0] >= '12'){
                //         $data['pickup_time_from'] = 'PM';
                //     }else{
                //         $data['pickup_time_from'] = 'AM';
                //     }
                //     $pickup_hour_to = explode(':',$data['pickup_hour_to']);
                //     if($pickup_hour_to[0] >= '12'){
                //         $data['pickup_time_to'] = 'PM';
                //     }else{
                //         $data['pickup_time_to'] = 'AM';
                //     }
                // }
                // if(isset($data['pickup_hour']) && $data['pickup_hour'] !== '')
                // {
                //     $data['pickup_hour_from'] = '';
                //     $data['pickup_time_from'] = '';
                //     $data['pickup_hour_to'] = '';
                //     $data['pickup_time_to'] = '';
                //     $pickup_hour = explode(':',$data['pickup_hour']);
                //     if($pickup_hour[0] >= '12'){
                //         $data['pickup_time_format'] = 'PM';
                //     }else{
                //         $data['pickup_time_format'] = 'AM';
                //     }
                // }
    
                // if(isset($data['delivery_time_range_check']) && $data['delivery_time_range_check'] == '1'){
                //     $data['delivery_hour'] = '';
                //     $data['delivery_time_format'] = '';
                //     $delivery_hour_from = explode(':',$data['delivery_hour_from']);
                //     if($delivery_hour_from[0] >= '12'){
                //         $data['delivery_time_from'] = 'PM';
                //     }else{
                //         $data['delivery_time_from'] = 'AM';
                //     }
                //     $delivery_hour_to = explode(':',$data['delivery_hour_to']);
                //     if($delivery_hour_to[0] >= '12'){
                //         $data['delivery_time_to'] = 'PM';
                //     }else{
                //         $data['delivery_time_to'] = 'AM';
                //     }
                // }
                // if(isset($data['delivery_hour']) && $data['delivery_hour'] !== ''){
                //     $data['delivery_hour_from'] = '';
                //     $data['delivery_hour_to'] = '';
                //     $delivery_hour = explode(':',$data['delivery_hour']);
                //     if($delivery_hour[0] >= '12'){
                //         $data['delivery_time_format'] = 'PM';
                //     }else{
                //         $data['delivery_time_format'] = 'AM';
                //     }
                // }
    
                // Session::put('direct_shipment_data', $data);
                
                $pickup_time_from = $data['pickup_hour_from'] !== '' && !empty($data['pickup_hour_from']) ? date('h:i A',strtotime($data['pickup_hour_from'])) : date('h:i A',strtotime($data['pickup_hour']));
                $pickup_time_to = $data['pickup_hour_to'] !== '' && !empty($data['pickup_hour_to']) ? date('h:i A',strtotime($data['pickup_hour_to'])) : date('h:i A',strtotime($data['pickup_hour']));
    
    
                //To calculate distance between coordinates
                //$distance = $this->distance('23.0225', '72.5715','22.7739','71.6673','K');
                $calculated_distance['miles'] = 0;
                $calculated_distance['journey'] = 0;
                if(!empty($data['pickupAddress']) && !empty($data['deliveryAddress'])){
                    $calculated_distance = $this->calculate_distance($data['pickupAddress'],$data['deliveryAddress']);
                }
                $journey_type = __("Long haul shipment");
                if(!empty($calculated_distance['miles'])){
                    if($calculated_distance['miles'] >= 0 && $calculated_distance['miles'] <= 100){
                        $journey_type = __("Short haul shipment");
                    }else if($calculated_distance['miles'] > 100 && $calculated_distance['miles'] <= 300){
                        $journey_type = __("Medium haul shipment");
                    }else if($calculated_distance['miles'] > 300){
                        $journey_type = __("Long haul shipment");
                    }
                }
                $booking = Booking::where('id',$id)->update([
                    'user_id' => $user->id,
                    'pickup_address' => $data['pickupAddress'],
                    'pickup_lat' => $pickup_address['address_lat'],
                    'pickup_lng' => $pickup_address['address_lng'],
                    'pickup_date' => $data['pickup_date'],
                    'pickup_time_from' => $pickup_time_from,
                    'pickup_time_to' => $pickup_time_to,
                    'distance' => (isset($calculated_distance['miles']) && !empty($calculated_distance['miles']) ? str_replace(',','',$calculated_distance['miles']) : '0'),
                    'journey' => (isset($calculated_distance['journey']) && !empty($calculated_distance['journey']) ? $calculated_distance['journey'] : '0'),
                    'journey_type' => $journey_type,
                    //'pickup_note' => $data['pickup_note'],
                ]);
    
                $truck_type_data = Session::get('edit_truck_data');
                $t_type = $truck_type_data['truck_type'];
                BookingTruck::where('booking_id',$id)->delete();
                foreach($t_type as $truck_type){
                    $truck_data = [];
                    $truck_data['booking_id'] = $id;
                    $truck_data['truck_type'] = $truck_type;
                    foreach($truck_type_data['truck_categories'] as $key => $truck_cat){
                        if($key == $truck_type){
                            $truck_data['truck_type_category'] = implode(',',$truck_cat);
                        }
                    }
                    $truck_data['is_hydraulic'] = $truck_type_data['hydraulic'];
                    // $truck_data['is_cooling'] = $truck_type_data['cooling'];
                    $truck_data['note'] = $truck_type_data['shipment_note'];
    
                    BookingTruck::create($truck_data);
                }
    
                $delivery_time_from = $data['delivery_hour_from'] != '' && !empty($data['delivery_hour_from']) ? date('h:i A',strtotime($data['delivery_hour_from'])) : date('h:i A',strtotime($data['delivery_hour']));
                $delivery_time_to = $data['delivery_hour_to'] != '' && !empty($data['delivery_hour_to']) ? date('h:i A',strtotime($data['delivery_hour_to'])) : date('h:i A',strtotime($data['delivery_hour']));
                BookingLocations::where('booking_id',$id)->delete();
                $booking_location_id_1 = BookingLocations::create([
                    'booking_id' => $id,
                    'drop_location' => $data['pickupAddress'],
                    'company_name' => $pickup_address['name'],
                    'delivered_at' => $data['pickup_date'],
                    'drop_lat' => $pickup_address['address_lat'],
                    'drop_lng' => $pickup_address['address_lng'],
                    'note' => $data['pickup_note'],
                    'is_pickup' => 1,
                    'delivery_time_from' => $pickup_time_from,
                    'delivery_time_to' => $pickup_time_to,
                ])->id;
    
                $booking_location_id_2 = BookingLocations::create([
                    'booking_id' => $id,
                    'drop_location' => $data['deliveryAddress'],
                    'delivered_at' => $data['delivery_date'],
                    'company_name' => $delivery_address['name'],
                    'drop_lat' => $delivery_address['address_lat'],
                    'drop_lng' => $delivery_address['address_lng'],
                    'note' => $data['delivery_note'],
                    'is_pickup' => 0,
                    'delivery_time_from' => $delivery_time_from,
                    'delivery_time_to' => $delivery_time_to,
                    'distance' => (isset($calculated_distance['miles']) && !empty($calculated_distance['miles']) ? str_replace(',','',$calculated_distance['miles']) : ''),
                    'journey' => (isset($calculated_distance['journey']) && !empty($calculated_distance['journey']) ? $calculated_distance['journey'] : ''),
                ])->id;
    
                BookingProduct::where('booking_id',$id)->delete();
                $products = Session::get('edit_product_data');
                foreach($products as $product)
                {
                    $productDetails = Product::findOrFail($product['product_id'])->toArray();
                    BookingProduct::create([
                        'booking_id' => $id,
                        'location_id' => $booking_location_id_1,
                        'product_id' => $product['product_id'],
                        'product_type' => $product['package'],
                        'qty' => $product['package_quantity'],
                        'weight' => $product['weight'],
                        'unit' => $product['unit'],
                        'note' => $product['product_description'],
                        'is_fragile' => $product['fragile'],
                        'is_sensetive' => $product['sensitive'],
                        'is_pickup' => '1',
                    ]);
    
                    BookingProduct::create([
                        'booking_id' => $id,
                        'location_id' => $booking_location_id_2,
                        'product_id' => $product['product_id'],
                        'product_type' => $product['package'],
                        'qty' => $product['package_quantity'],
                        'weight' => $product['weight'],
                        'unit' => $product['unit'],
                        'note' => $product['product_description'],
                        'is_fragile' => $product['fragile'],
                        'is_sensetive' => $product['sensitive'],
                    ]);
    
                }
                $session_data = Session::get('edit_truck_data');
                if(!isset($session_data['driver_detail']) && empty($session_data['driver_detail'])){
                    Session::forget('edit_truck_data');
                }
                Session::forget('edit_product_data');
                // return redirect('shipper/manage-shipment')->with('success',__('Shipment Updated Successfully'));
                return redirect('shipper/shipment/edit-search/'.$id)->with('success',__('Shipment Updated Successfully'));
            }
        }
    }

    public function update_multiple_location(Request $request,$id)
    {
        // echo '<pre>';print_r($request->all());die();
        $data = $request->except('_token');
        // echo '<pre>';print_r($data);die();
        $pickup_address = Facility::where('id',$data['location_1'])->first();
        $delivery_address = Facility::where('id',$data['location_2'])->first();
        // $distance = $this->SimpleDistance($pickup_address['address_lat'],$pickup_address['address_lng'],$delivery_address['address_lat'],$delivery_address['address_lng'],'K');
        // echo '<pre>';print_r($products);
        // echo '<pre>';print_r(array_search('63',$products));
        // echo '<pre>';print_r();
        // echo '<pre>';print_r($data);
        // die();
        if(isset($data['multiple_shipment_location_length']) && !empty($data['multiple_shipment_location_length'])){
            $array_validation = [];
            for($i=1;$i<=$data['multiple_shipment_location_length'];$i++){
                $array_validation['location_'.$i] = 'required';
                $array_validation['date_'.$i] = 'required';
            }
            if(isset($array_validation) && !empty($array_validation)){
                $validator = Validator::make($request->all(), $array_validation);
            }

            if(empty(Session::get('edit_product_data')) || empty(Session::get('edit_truck_data'))){
                return redirect()->back()->with('error',__('Something went wrong please try again later!'));
            }else{
                $user =   Auth::user();

                $location = Facility::where('id',$data['location_1'])->first();
                    // $delivery_address = Facility::where('id',$data['deliveryAddress'])->first();
                    // $data['pickupAddress'] = $pickup_address['address'];
                    // $data['deliveryAddress'] = $delivery_address['address'];

                    // if(isset($data['location_time_range_check_1']) && $data['location_time_range_check_1'] == '1'){
                    //     $data['location_hour_1'] = '';
                    //     $data['location_time_format_1'] = '';
                    //     $pickup_hour_from = explode(':',$data['location_hour_from_1']);
                    //     if($pickup_hour_from[0] >= '12'){
                    //         $data['location_time_from_1'] = 'PM';
                    //     }else{
                    //         $data['location_time_from_1'] = 'AM';
                    //     }

                    //     $pickup_hour_to = explode(':',$data['location_hour_to_1']);
                    //     if($pickup_hour_to[0] >= '12'){
                    //         $data['location_time_to_1'] = 'PM';
                    //     }else{
                    //         $data['location_time_to_1'] = 'AM';
                    //     }
                    // }
                    // if(isset($data['location_hour_1']) && $data['location_hour_1'] !== '')
                    // {
                    //     $data['location_hour_from_1'] = '';
                    //     $data['location_time_from_1'] = '';
                    //     $data['location_hour_to_1'] = '';
                    //     $data['location_time_to_1'] = '';
                    //     $pickup_hour = explode(':',$data['location_hour_1']);
                    //     if($pickup_hour[0] >= '12'){
                    //         $data['location_time_format_1'] = 'PM';
                    //     }else{
                    //         $data['location_time_format_1'] = 'AM';
                    //     }
                    // }

                    // Session::put('multiple_shipment_data', $data);

                    $pickup_time_from = $data['location_hour_from_1'] !== '' && !empty($data['location_hour_from_1']) ? date('h:i A',strtotime($data['location_hour_from_1'])) : date('h:i A',strtotime($data['location_hour_1']));
                    $pickup_time_to = $data['location_hour_to_1'] !== '' && !empty($data['location_hour_to_1']) ? date('h:i A',strtotime($data['location_hour_to_1'])) : date('h:i A',strtotime($data['location_hour_1']));

                    //To calculate distance between coordinates
                    // $distance['miles'] = 0;
                    // $distance['journey'] = 0;
                    // if(isset($location) && !empty($location) && !empty($previous_address) && isset($previous_address)){
                    //     $distance = $this->distance($previous_address['address_lat'], $previous_address['address_lng'],$location['address_lat'],$location['address_lng'],'K');
                    // }

                $booking = Booking::where('id',$id)->update([
                    'user_id' => $user->id,
                    'pickup_address' => (!empty($location['address']) && isset($location['address']) ? $location['address'] : '-'),
                    'pickup_lat' => (!empty($location['address_lat']) && isset($location['address_lat']) ? $location['address_lat'] : '-'),
                    'pickup_lng' => (!empty($location['address_lng']) && isset($location['address_lng']) ? $location['address_lng'] : '-'),
                    'pickup_date' => $data['date_1'],
                    'pickup_time_from' => $pickup_time_from,
                    'pickup_time_to' => $pickup_time_to,
                    'distance' => '0',
                    'journey' => '0',
                    'journey_type' => '',
                    'booking_type' => 'multiple_shipment',
                    //'pickup_note' => $data['pickup_note'],
                ]);

                $truck_type_data = Session::get('edit_truck_data');
                $t_type = $truck_type_data['truck_type'];
                BookingTruck::where('booking_id',$id)->delete();
                foreach($t_type as $truck_type){
                    $truck_data = [];
                    $truck_data['booking_id'] = $id;
                    $truck_data['truck_type'] = $truck_type;
                    foreach($truck_type_data['truck_categories'] as $key => $truck_cat){
                        if($key == $truck_type){
                            $truck_data['truck_type_category'] = implode(',',$truck_cat);
                        }
                    }
                    $truck_data['is_hydraulic'] = $truck_type_data['hydraulic'];
                    // $truck_data['is_cooling'] = $truck_type_data['cooling'];
                    $truck_data['note'] = $truck_type_data['shipment_note'];
    
                    BookingTruck::create($truck_data);
                }

                $previous_address_lat = '';
                $previous_address_lng = '';
                $previous_address = '';
                $total_distance = 0;
                $total_journey = 0;
                BookingLocations::where('booking_id',$id)->delete();
                BookingProduct::where('booking_id',$id)->delete();
                for($i=1;$i<=$data['multiple_shipment_location_length'];$i++){
                    $location_new = Facility::where('id',$data['location_'.$i])->first();

                    // if(isset($data['location_time_range_check_'.$i]) && $data['location_time_range_check_'.$i] == '1'){
                    //     $data['location_hour_'.$i] = '';
                    //     $data['location_time_format_'.$i] = '';
                    //     $pickup_hour_from = explode(':',$data['location_hour_from_'.$i]);
                    //     if($pickup_hour_from[0] >= '12'){
                    //         $data['location_time_from_'.$i] = 'PM';
                    //     }else{
                    //         $data['location_time_from_'.$i] = 'AM';
                    //     }

                    //     $pickup_hour_to = explode(':',$data['location_hour_to_'.$i]);
                    //     if($pickup_hour_to[0] >= '12'){
                    //         $data['location_time_to_'.$i] = 'PM';
                    //     }else{
                    //         $data['location_time_to_'.$i] = 'AM';
                    //     }
                    // }
                    // if(isset($data['location_hour_'.$i]) && $data['location_hour_'.$i] !== '')
                    // {
                    //     $data['location_hour_from_'.$i] = '';
                    //     $data['location_time_from_'.$i] = '';
                    //     $data['location_hour_to_'.$i] = '';
                    //     $data['location_time_to_'.$i] = '';
                    //     $pickup_hour = explode(':',$data['location_hour_'.$i]);
                    //     if($pickup_hour[0] >= '12'){
                    //         $data['location_time_format_'.$i] = 'PM';
                    //     }else{
                    //         $data['location_time_format_'.$i] = 'AM';
                    //     }
                    // }
                    $pickup_time_from = $data['location_hour_from_'.$i] !== '' && !empty($data['location_hour_from_'.$i]) ? date('h:i A',strtotime($data['location_hour_from_'.$i])) : date('h:i A',strtotime($data['location_hour_'.$i]));
                    $pickup_time_to = $data['location_hour_to_'.$i] !== '' && !empty($data['location_hour_to_'.$i]) ? date('h:i A',strtotime($data['location_hour_to_'.$i])) : date('h:i A',strtotime($data['location_hour_'.$i]));

                    // To calculate distance between coordinates
                    $calculated_distance['miles'] = '0';
                    $calculated_distance['journey'] = '0';
                    $calculated_distance['journey_value'] = '0';
                    if(isset($location_new) && !empty($location_new) && !empty($previous_address) && isset($previous_address)){
                        // $distance = $this->distance($previous_address_lat, $previous_address_lng,$location_new['address_lat'],$location_new['address_lng'],'K');
                        $calculated_distance = $this->calculate_distance($previous_address,$location_new['address']);
                    }
                    // echo '<pre>';print_r($location_new);
                    $previous_address = $location_new['address'];
                    // echo '<pre>';print_r($previous_address);
                    // $previous_address_lng = $location_new['address_lng'];
                    $total_distance += str_replace(',','',$calculated_distance['miles']);
                    $total_journey += $calculated_distance['journey_value'];
                    $booking_location_id = BookingLocations::create([
                        'booking_id' => $id,
                        'drop_location' => (!empty($location_new['address']) && isset($location_new['address']) ? $location_new['address'] : '-'),
                        'company_name' => (!empty($location_new['name']) && isset($location_new['name']) ? $location_new['name'] : '-'),
                        'delivered_at' => $data['date_'.$i],
                        'drop_lat' => (!empty($location_new['address_lat']) && isset($location_new['address_lat']) ? $location_new['address_lat'] : '-'),
                        'drop_lng' => (!empty($location_new['address_lng']) && isset($location_new['address_lng']) ? $location_new['address_lng'] : '-'),
                        'note' => $data['locationNote_'.$i],
                        'is_pickup' => ($i == '1' ? '1' : '0'),
                        'delivery_time_from' => $pickup_time_from,
                        'delivery_time_to' => $pickup_time_to,
                        'distance' => (isset($calculated_distance['miles']) && !empty($calculated_distance['miles']) ? str_replace(',','',$calculated_distance['miles']) : '0'),
                        'journey' => (isset($calculated_distance['journey']) && !empty($calculated_distance['journey']) ? $calculated_distance['journey'] : '0'),
                    ])->id;
                    
                    if(isset($data['location_products_'.$i]) && !empty($data['location_products_'.$i])){
                        $products = Session::get('edit_product_data');
                        foreach($data['location_products_'.$i] as $key => $value){
                            $products_key = array_search($value, array_column($products, 'product_id'));
                            // $productDetails = Product::findOrFail($data['location_products_'.$i])->toArray();
                            BookingProduct::create([
                                'booking_id' => $id,
                                'location_id' => $booking_location_id,
                                'product_id' => $value,
                                'product_type' => $products[$products_key]['package'],
                                'qty' => $data['quantity_'.$i][$key],
                                'weight' => $data['weight_'.$i][$key],
                                'unit' => $data['unit_'.$i][$key],
                                'note' => $products[$products_key]['product_description'],
                                'is_fragile' => $products[$products_key]['fragile'],
                                'is_sensetive' => $products[$products_key]['sensitive'],
                                'is_pickup' => ($data['shipment_type_'.$i][$key] == 'pickup' ? '1' : '0')
                            ]);
                        }
                    }
                    $journey_type = __("Long haul shipment");
                    if(!empty($total_distance)){
                        if($total_distance >= 0 && $total_distance <= 100){
                            $journey_type = __("Short haul shipment");
                        }else if($total_distance > 100 && $total_distance <= 300){
                            $journey_type = __("Medium haul shipment");
                        }else if($total_distance > 300){
                            $journey_type = __("Long haul shipment");
                        }
                    }
                    $total_journey_final = $this->secondsToTime($total_journey);
                    $total_journey_string = (isset($total_journey_final['h']) && $total_journey_final['h'] != 0 ? $total_journey_final['h'].' hours' : '').' '.(isset($total_journey_final['m']) && $total_journey_final['m'] != 0 ? $total_journey_final['m'].' mins' : '');
                    Booking::whereId($id)->update(['distance' => number_format($total_distance,2),'journey_type' => $journey_type, 'journey' => $total_journey_string]);
                    // $previous_address = $location_new;
                }
                Session::forget('edit_truck_data');
                Session::forget('edit_product_data');
                // return redirect('shipper/manage-shipment')->with('success',__('Shipment Updated Successfully.'));
                return redirect('shipper/shipment/edit-search/'.$id)->with('success',__('Shipment Updated Successfully'));
            }
        }

        // if ($validator->fails()) {
        //     return redirect()->back()->with('error',__('Something went wrong please try again later!'));
        // }else{
            
        // }
    }

    public function edit_search($id)
    {
        $user = Auth::user();
        // $booking= Booking::whereUserId($user->id)->whereStatus('pending')->orderBy('id','desc')->limit(1)->first();
        // $booking= Booking::with(['booking_location','booking_products.product','booking_products.truck_unit','booking_truck'])->whereId($id)->whereStatus('pending')->orderBy('id','desc')->limit(1)->first();
        $booking = Booking::whereId($id)->orderBy('booking.id','desc')->limit(1)->first();
        $booking_locations = BookingLocations::with('products')->whereBookingId($id)->get()->toArray();
        $drop_location = BookingLocations::whereBookingId($id)->orderBy('id','desc')->first();
        $booking_truck = BookingTruck::with(['truck_type'])->whereBookingId($id)->get()->toArray();
        if(empty($booking))
        {
            return redirect(url('shipper/manage-shipment'))->with('error',__('Shipment not found!'));
        }
        $location_aray = array();
        if(isset($booking_locations) && !empty($booking_locations)){
            for($i=0;$i<count($booking_locations);$i++){
                array_push($location_aray,[$booking_locations[$i]['drop_location'],$booking_locations[$i]['drop_lat'],$booking_locations[$i]['drop_lng'],$i+1]);  
            } 
        }
        $user = Auth::user();
        if($user->type == 'manager'){
            $manage_price = PermissionHelper::check_access('manage_price');
        }else{
            $manage_price = 1;
        }

        $bookingss = Booking::where('pickup_date', '>=', date('Y-m-d'))
        ->where(function($query) use ($id){
            if($id > 0){
                $query->where('id',$id);
            }
        })
        ->where('user_id',$user->id)->whereIn('status',['pending','create'])->orderBy('id','asc')->get()->toArray();
        $available_trucks = [];
        if(isset($bookingss) && !empty($bookingss)){
            $i = 0;
            foreach($bookingss as $book){
                $truck_type = TruckType::where(['id'=>$book['trucks']['truck_type']])->limit(1)->get()->toArray();
                $availability = $this->nearByDriver($book);
                foreach ($availability->toArray() as $row) {
                    $row['trucks_type'] = $truck_type[0];
                    $row['booking'] = $book;
                    // $available_trucks[] = $row;
                    if(empty($row['booking_request'])){
                        $available_trucks[] = $row;
                    }else{
                        if(isset($row['booking_request']) && !empty($row['booking_request'])){
                            foreach($row['booking_request'] as $booking_request){
                                if($booking_request['shipper_id'] != ($user['type'] == 'manager' ? $user['company_ref_id'] : $user['id'])){
                                    $available_trucks[] = $row;
                                }
                            }
                        }
                    }
                }
            }
        }
        $available_trucks = $available_trucks;

        $available_trucks = $available_trucks;
        // $available_trucks['price'] = Str::limit($available_trucks['price'], 11);
        $columns = [
                        ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px'], 
                        ['data' => 'truck_type', 'name' => "truck_type",'title' => __("Truck Type"),'width'=>'60px'], 
                        ['data' => 'start_date', 'name' => __("amount"),'title' => __("Start Date"),'width'=>'60px'],
                        ['data' => 'start_location', 'name' => __("status"),'title' => __("Start Location"),'width'=>'100px'],
                        ['data' => 'end_location', 'name' => __("end_location"),'title' => __("End Location"),'width'=>'100px'],
                        ['data' => 'carrier_info', 'name' => __("end_date"),'title' => __("Carrier Info"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                        ['data' => 'price', 'name' => __("drop_location"),'title' => __("Price"),'width'=>'80px'],
                        ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                    ];
        $dateTableFields = $columns;
        $dataTableId = time();

        // echo '<pre>';print_r($available_trucks);die();
        return view('shipper.pages.shipment.search_edit',compact('booking','location_aray','booking_locations','booking_truck','manage_price','available_trucks','dataTableId','dateTableFields'));
    }

    public function check_validation(Request $request){
        // echo '<pre>';print_r($request->all());die();
        $data = $request->all();
        $pickup_quantity_sum = [];
        $pickup_weight_sum = [];
        $delivery_quantity_sum = [];
        $delivery_weight_sum = [];
        $product_data = Session::get('product_data');
        $previous_date = '';
        $previous_time = '';
        $error_msg = '';
        if(isset($data) && !empty($data)){
            for($i=1;$i<=$data['multiple_shipment_location_length'];$i++){
                if(!empty($previous_date) && !empty($previous_time)){
                    if($previous_date == $data['date_'.$i]){
                        if($previous_time < date('H:i',strtotime($data['location_hour_'.$i]))){
                            $previous_time = date('H:i',strtotime($data['location_hour_'.$i]));
                            $previous_date = $data['date_'.$i];
                        }else{
                            $error_msg = __('Pickup time not be same.');    
                        }
                    }else{
                        if($previous_time <= date('H:i',strtotime($data['location_hour_'.$i]))){
                            $previous_time = date('H:i',strtotime($data['location_hour_'.$i]));
                            $previous_date = $data['date_'.$i];
                        }else{
                            $previous_time = date('H:i',strtotime($data['location_hour_'.$i]));
                            $previous_date = $data['date_'.$i];
                        }
                    }
                }else{
                    $previous_time = date('H:i',strtotime($data['location_hour_'.$i]));
                    $previous_date = $data['date_'.$i];
                }
                for($j=0;$j<count($data['old_quantity_1']);$j++){
                    if(isset($data['location_products_'.$i][$j]) && !empty($data['location_products_'.$i][$j]) && $data['shipment_type_'.$i][$j] == 'pickup'){
                        if(isset($pickup_quantity_sum[$data['location_products_'.$i][$j]]) && !empty($pickup_quantity_sum[$data['location_products_'.$i][$j]])){
                            $pickup_quantity_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['quantity_'.$i][$j]),2));
                            $pickup_weight_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['weight_'.$i][$j]),2));
                        }else{
                            $pickup_quantity_sum[$data['location_products_'.$i][$j]] = 0;
                            $pickup_quantity_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['quantity_'.$i][$j]),2));
                            $pickup_weight_sum[$data['location_products_'.$i][$j]] = 0;
                            $pickup_weight_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['weight_'.$i][$j]),2));
                        }                        
                    }
                    if(isset($data['location_products_'.$i][$j]) && !empty($data['location_products_'.$i][$j]) && $data['shipment_type_'.$i][$j] == 'delivery'){
                        if(isset($delivery_quantity_sum[$data['location_products_'.$i][$j]]) && !empty($delivery_quantity_sum[$data['location_products_'.$i][$j]])){
                            $delivery_quantity_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['quantity_'.$i][$j]),2));
                            $delivery_weight_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['weight_'.$i][$j]),2));
                        }else{
                            $delivery_quantity_sum[$data['location_products_'.$i][$j]] = 0;
                            $delivery_quantity_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['quantity_'.$i][$j]),2));
                            $delivery_weight_sum[$data['location_products_'.$i][$j]] = 0;
                            $delivery_weight_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['weight_'.$i][$j]),2));
                        }
                    }
                }
            }
        }
        if(isset($pickup_quantity_sum) && !empty($pickup_quantity_sum)){
            for($i=0;$i<count($pickup_quantity_sum);$i++){
                if(isset($pickup_quantity_sum[$product_data[$i]['product_id']]) && !empty($pickup_quantity_sum[$product_data[$i]['product_id']])){
                    if(str_replace(',','',number_format(str_replace(',','.',$data['old_quantity_1'][$i]),2)) != str_replace(',','',number_format($pickup_quantity_sum[$product_data[$i]['product_id']],2))){
                        $error_msg = __('Please check products for pickup and delivery which is not match.');
                    }
                    if(str_replace(',','',number_format(str_replace(',','.',$data['old_weight_1'][$i]),2)) != str_replace(',','',number_format($pickup_weight_sum[$product_data[$i]['product_id']],2))){
                        $error_msg = __('Please check products for pickup and delivery which is not match.');
                    }
                }else{
                    $error_msg = __('Please check products for pickup and delivery which is not match.');
                }
                
            }
        }
        if(array_diff($pickup_quantity_sum,$delivery_quantity_sum)){
            $error_msg = __('Please check products for pickup and delivery which is not match.');
        }
        if(array_diff($delivery_quantity_sum,$pickup_quantity_sum)){
            $error_msg = __('Please check products for pickup and delivery which is not match.');
        }
        echo json_encode($error_msg);
        
    }

    public function check_validation_edit(Request $request){
        $data = $request->all();
        $pickup_quantity_sum = [];
        $pickup_weight_sum = [];
        $delivery_quantity_sum = [];
        $delivery_weight_sum = [];
        $product_data = Session::get('edit_product_data');
        $previous_date = '';
        $previous_time = '';
        $error_msg = '';
        if(isset($data) && !empty($data)){
            for($i=1;$i<=$data['multiple_shipment_location_length'];$i++){
                if(!empty($previous_date) && !empty($previous_time)){
                    if($previous_date == $data['date_'.$i]){
                        if($previous_time < date('H:i',strtotime($data['location_hour_'.$i]))){
                            $previous_time = date('H:i',strtotime($data['location_hour_'.$i]));
                            $previous_date = $data['date_'.$i];
                        }else{
                            $error_msg = __('Pickup time not be same.');    
                        }
                    }else{
                        if($previous_time <= date('H:i',strtotime($data['location_hour_'.$i]))){
                            $previous_time = date('H:i',strtotime($data['location_hour_'.$i]));
                            $previous_date = $data['date_'.$i];
                        }else{
                            $previous_time = date('H:i',strtotime($data['location_hour_'.$i]));
                            $previous_date = $data['date_'.$i];
                        }
                    }
                }else{
                    $previous_time = date('H:i',strtotime($data['location_hour_'.$i]));
                    $previous_date = $data['date_'.$i];
                }
                for($j=0;$j<count($data['old_quantity_1']);$j++){
                    if(isset($data['location_products_'.$i][$j]) && !empty($data['location_products_'.$i][$j]) && $data['shipment_type_'.$i][$j] == 'pickup'){
                        if(isset($pickup_quantity_sum[$data['location_products_'.$i][$j]]) && !empty($pickup_quantity_sum[$data['location_products_'.$i][$j]])){
                            $pickup_quantity_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['quantity_'.$i][$j]),2));
                            $pickup_weight_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['weight_'.$i][$j]),2));
                        }else{
                            $pickup_quantity_sum[$data['location_products_'.$i][$j]] = 0;
                            $pickup_quantity_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['quantity_'.$i][$j]),2));
                            $pickup_weight_sum[$data['location_products_'.$i][$j]] = 0;
                            $pickup_weight_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['weight_'.$i][$j]),2));
                        }                        
                    }
                    if(isset($data['location_products_'.$i][$j]) && !empty($data['location_products_'.$i][$j]) && $data['shipment_type_'.$i][$j] == 'delivery'){
                        if(isset($delivery_quantity_sum[$data['location_products_'.$i][$j]]) && !empty($delivery_quantity_sum[$data['location_products_'.$i][$j]])){
                            $delivery_quantity_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['quantity_'.$i][$j]),2));
                            $delivery_weight_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['weight_'.$i][$j]),2));
                        }else{
                            $delivery_quantity_sum[$data['location_products_'.$i][$j]] = 0;
                            $delivery_quantity_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['quantity_'.$i][$j]),2));
                            $delivery_weight_sum[$data['location_products_'.$i][$j]] = 0;
                            $delivery_weight_sum[$data['location_products_'.$i][$j]] += str_replace(',','',number_format(str_replace(',','.',$data['weight_'.$i][$j]),2));
                        }
                    }
                }
            }
        }
        if(isset($pickup_quantity_sum) && !empty($pickup_quantity_sum)){
            for($i=0;$i<count($pickup_quantity_sum);$i++){
                if(isset($pickup_quantity_sum[$data['old_product_1'][$i]]) && !empty($pickup_quantity_sum[$data['old_product_1'][$i]])){
                    if(str_replace(',','',number_format(str_replace(',','.',$data['old_quantity_1'][$i]))) != str_replace(',','',number_format(str_replace(',','.',$pickup_quantity_sum[$data['old_product_1'][$i]])))){
                        $error_msg = __('Please check products for pickup and delivery which is not match.');
                    }else{
                        $error_msg = "";
                    }
                    if(str_replace(',','',number_format(str_replace(',','.',$data['old_weight_1'][$i]))) != str_replace(',','',number_format(str_replace(',','.',$pickup_weight_sum[$data['old_product_1'][$i]])))){
                        $error_msg = __('Please check products for pickup and delivery which is not match.');
                    }else{
                        $error_msg = "";
                    }
                }else{
                    $error_msg = __('Please check products for pickup and delivery which is not match.');
                }
                
            }
        }
        if(array_diff($pickup_quantity_sum,$delivery_quantity_sum)){
            $error_msg = __('Please check products for pickup and delivery which is not match.');
        }
        if(array_diff($delivery_quantity_sum,$pickup_quantity_sum)){
            $error_msg = __('Please check products for pickup and delivery which is not match.');
        }
        echo json_encode($error_msg);
        
    }

    public function nearByDriver($booking,$drop_location='0') {

        // $current = Carbon::now()->startOfDay();
        $last_key = array_key_last($booking['trucks']['locations']);

        $matches = Availability::distinct()
            ->select("availability.*","users.name as driver_name",
                DB::raw("6371 * acos(cos(radians(start_lat))
                                            * cos(radians(".$booking['pickup_lat'].")) * cos(radians(".$booking['pickup_lng'].") - radians(start_lng))
                                            + sin(radians(start_lat)) * sin(radians(".$booking['pickup_lat']."))) AS pickup_distance"),

                DB::raw("6371 * acos(cos(radians(end_lat))
                                            * cos(radians(".$booking['trucks']['locations'][$last_key]['drop_lat'].")) * cos(radians(".$booking['trucks']['locations'][$last_key]['drop_lng'].") - radians(end_lng))
                                            + sin(radians(end_lat)) * sin(radians(".$booking['trucks']['locations'][$last_key]['drop_lat']."))) AS drop_distance")

            )
            // ->select($select)
            ->with('booking_request')
            ->leftJoin('users','users.id','=','availability.driver_id')
            ->where('availability.truck_type_id', $booking['trucks']['truck_type'])
            ->where('availability.date','>=',$booking['pickup_date'])
            ->where('availability.load_status','=','0')
            ->having('pickup_distance', '<', 50)
            // ->having($having)
            // ->having('drop_distance', '<', 50)
            ->orderBy('id', 'asc')
            ->get();


        return $matches;

    }

    public function removeProduct(Request $request,$id)
    {
        $data = $request->all();
        if(!empty($id) && isset($id) && isset($data['booking_id']) && !empty($data['booking_id'])){
            $booking_product = BookingProduct::where(['booking_id' => $data['booking_id'], 'product_id' => $id])->delete();
            return \Response::json('success', 200);
        }else{
            return \Response::json('error', 200);
        }
    }

    public function updateBookingData(Request $request)
    {
        $data = $request->all();
        if(isset($data) && !empty($data)){
            $check_is_existing_booking = Booking::where(['id' => $data['booking_id']])->exists();
            if($check_is_existing_booking){
                $update_data['amount'] = $data['amount'];
                $update_data['is_bid'] = '0';
                if(isset($data['allow_bidding']) && !empty($data['allow_bidding'])){
                    $update_data['is_bid'] = '1';
                }
                $update_data['status'] = 'pending';
                Booking::where('id', $data['booking_id'])->update($update_data);
                return \Response::json('success', 200);
            }else{
                return \Response::json('error', 200);
            }
        }
    }

    public function payment(Request $request,$booking_id)
    {
        if(isset($booking_id) && !empty($booking_id)){
            $validator = Validator::make($request->all(), [
                'amount' => 'required',
            ]);
            $data = $request->all();
            if ($validator->fails()) {
                return redirect()->back()->with('error',__('Amount field is required'));
            }else{
                $transaction_data['booking_id'] = $booking_id;
                $transaction_data['driver_id'] = $data['driver_id'];
                $transaction_data['amount'] = $data['amount'];
                $transaction_data['transaction_type'] = $data['payment_type'];
                $transaction_data['status'] = 'completed';
                $transaction_data['date'] = date('Y-m-d');
                $transaction_id = Transaction::create($transaction_data)->id;
                $update_booking['payment_status'] = 'completed';
                $update_booking['payment_type'] = $data['payment_type'];
                $update_booking['transaction_id'] = $transaction_id;
                Booking::where(['id' => $booking_id])->update($update_booking);
                return redirect('shipper/manage-shipment/'.$booking_id)->with('success',__('Payment payed successfully.'));
            }
        }else{
            return redirect(url('shipper/manage-shipment'))->with('error',__('Something went wrong, please try again later!'));
        }
    }

    public function test_notification()
    {
        echo '<pre>';print_r('Test');
        CommonHelper::send_push('ios','cTOtfRAIGUmOksJWUheWVQ:APA91bEN8pAf2RIhF0yygRED8_TpuWxK8fZjhcPQnqwersZl08fivT174SEVZZqOeE8LK4pMdxUY0fxK2PaFCFaLNIziSQTd30HcnCYkmWVxfnh8DkAmCbJlFWlmdNHXdiza2gi101GA','Test','Test','Test',[]);
    }


}
