<?php

namespace App\Http\Controllers\Shipper;

use App\Models\City;
use App\Models\State;
use App\Models\Booking;
use App\Models\User;
use App\Models\Country;
use App\Models\Product;
use App\Models\Facility;
use App\Models\TruckType;
use App\Models\TruckCategory;
use App\Models\TruckUnit;
use App\Models\ShipperPermission;
use Illuminate\Http\Request;
use App\Models\BookingProduct;
use App\Models\BookingLocations;
use App\Http\Controllers\Controller;

use App\Models\BookingLocation;
use App\Models\BookingProducts;
use App\Models\BookingTruck;
use App\Models\UserRole;
use App\Models\Role;
// use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Twilio\Tests\Request as TestsRequest;
use Yajra\DataTables\Facades\DataTables;
use PermissionHelper;

class ManagerManagementController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user =Auth::user();
            if($user->type == 'manager'){
                $permission = PermissionHelper::check_access('manager_management');
                if(empty($permission)){
                    return redirect()->back()->with('error',__("You can't access this module!"));
                }
            }
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        // $user =   Auth::user();
        // echo '<pre>';print_r($user);die();
        $countries =  Country::whereIn('name', ['Greece'])->get();
        if ($request->ajax()) {
            $user =   Auth::user();
            $data = User::where(['type' => 'manager','company_ref_id' => $user->id])->orderBy('id','desc');
            // $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('id','desc')->where('user_id',$user->id);
           
            // return DataTables::of($data)
            return DataTables::of($data)
                ->addIndexColumn()
                ->editColumn('action', function ($row) {
                    if($row['status'] == 1){
                        $btn = '
                        &nbsp;&nbsp;&nbsp;<a href="'.route('shipper.manager-management.edit',$row['id']).'" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" class="btn btn-xs text-default justify-content-end ">Edit</a>    
                        &nbsp;&nbsp;<a href="javascript:void(0)" data-id="'.$row['id'].'" onclick="approve_account_manager(this);return false;" data-status="0" data-popup="tooltip" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" class="btn btn-xs text-default justify-content-end ">Block</a>
                        ';
                    }else{
                        $btn = '
                        &nbsp;&nbsp;&nbsp;<a href="'.route('shipper.manager-management.edit',$row['id']).'" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" class="btn btn-xs text-default justify-content-end ">Edit</a>    
                        &nbsp;&nbsp;<a href="javascript:void(0)" data-id="'.$row['id'].'" onclick="approve_account_manager(this);return false;" data-status="1" data-popup="tooltip" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" class="btn btn-xs text-default justify-content-end ">Unblock</a>
                        ';
                    }
                    return $btn;

                })
                ->addColumn('mergeColumn', function($row){
                    return '(+30) '.$row->phone.'';
                })
                ->addColumn('no_of_shipment', function ($row) {
                    $no_of_shipment = '<span style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px;padding:0px 15px" class="btn">0</span>';
                    $booking_count = Booking::where(['user_id' => $row['id']])->count();
                    if(isset($booking_count) && !empty($booking_count)){
                        $no_of_shipment = '<span style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px;padding:0px 15px" class="btn">'.$booking_count.'</span>';
                    }
                    return $no_of_shipment;

                })
                ->rawColumns(['action','no_of_shipment'])
                ->make("*");

        } else {

            $columns = [
                            ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px'], 
                            ['data' => 'name', 'name' => "name",'title' => __("Name"),'width'=>'60px'],
                            ['data' => 'email', 'name' => "email",'title' => __("Work E-mail"),'width'=>'80px'],
                            ['data' => 'mergeColumn', 'name' => __("phone"),'title' => __("Phone No"),'width' => '15%'],
                            ['data' => 'no_of_shipment', 'name' => 'no_of_shipment','title' => __("No of Shipments"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                            ['data' => 'action', 'name' =>"Function",'title'=>__("Functions"), 'searchable' => false, 'orderable' => false ]
                       ];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('shipper.manager-management.index');
            $params['dateTableTitle'] = "Manager Management";
            $params['addUrl'] = "manager-management";
            $params['dataTableId'] = time();
            $params['countries'] = $countries;
            return view('shipper.pages.manager_management.index', $params);
        }
        // return view('shipper.pages.manage_shipment.index');
    }

    public function create()
    {
        $countries =  Country::whereIn('name', ['Greece'])->get();
        $params['dateTableTitle'] = "Add Manager";
        return view('shipper.pages.manager_management.create',$params,compact('countries'));
    }

    public function store(Request $request)
    {
        // echo '<pre>';print_r($request->all());die();
        $validator = \Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',           
        ],[
            'first_name.required' => 'First name field is required.',
            'last_name.required' => 'Last name field is required.',
            'email.required' => 'work e-mail field is required.',
            'phone.required' => 'phone no field is required.',          
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $input = $request->all();
        $user =   Auth::user();
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        // $input['password'] = substr($random, 0, 10);
        $input['password'] = '12345678';

        $mail_data['email'] = $input['email'];
        $mail_data['password'] = $input['password'];
        $mail_data['company_name'] = $user->company_name;


        #USER
        $manager = new User();
        $manager->name = $input['first_name'].' '.$input['last_name'];
        $manager->first_name = $input['first_name'];
        $manager->last_name = $input['last_name'];
        $manager->email = $input['email'];
        $manager->phone = $input['phone'];
        $manager->password = Hash::make('12345678');
        $manager->company_ref_id = $user->id;
        $manager->company_name = $user->company_name;
        $manager->email_verify = 1;
        $manager->phone_verify = 1;
        $manager->stage = 'accepted';
        $manager->status = 1;
        $manager->type = 'manager';
        $manager->save();

        #ROLE
        $role = Role::where('name','shipper')->first();
            
        #USER_ROLE
        $manager_role = UserRole::insert([
            'user_id' => $manager->id,
            'role_id' => $role['id'],
        ]);

        #Shipper Permission
        $shipper_permission = new ShipperPermission();
        $shipper_permission->user_id = $manager->id;
        $shipper_permission->create_shipment = 0;
        if(isset($input['create_shipment']) && !empty($input['create_shipment'])){
            $shipper_permission->create_shipment = 1;
        }

        $shipper_permission->view_shipment = 0;
        if(isset($input['view_shipment']) && !empty($input['view_shipment'])){
            $shipper_permission->view_shipment = 1;
        }

        $shipper_permission->cancel_shipment = 0;
        if(isset($input['cancel_shipment']) && !empty($input['cancel_shipment'])){
            $shipper_permission->cancel_shipment = 1;
        }

        $shipper_permission->view_products = 0;
        if(isset($input['view_products']) && !empty($input['view_products'])){
            $shipper_permission->view_products = 1;
        }

        $shipper_permission->view_address = 0;
        if(isset($input['view_address']) && !empty($input['view_address'])){
            $shipper_permission->view_address = 1;
        }

        $shipper_permission->search_driver = 0;
        if(isset($input['search_driver']) && !empty($input['search_driver'])){
            $shipper_permission->search_driver = 1;
        }

        $shipper_permission->manage_managers = 0;
        if(isset($input['manage_managers']) && !empty($input['manage_managers'])){
            $shipper_permission->manage_managers = 1;
        }

        $shipper_permission->manage_price = 0;
        if(isset($input['manage_price']) && !empty($input['manage_price'])){
            $shipper_permission->manage_price = 1;
        }

        $shipper_permission->save();

        dispatch(new \App\Jobs\ShipperManagerCredentialJob($mail_data));

        return redirect()->route('shipper.manager-management.index')->with('success', __('Shipper manager added successfully.'));
    }

    public function edit($id)
    {
        if(isset($id) && !empty($id))
        {
            $countries =  Country::whereIn('name', ['Greece'])->get();
            $check_is_exists = User::whereId($id)->where('type','manager')->exists();
            if($check_is_exists){
                $params['dateTableTitle'] = "Edit Manager";
                $params['data'] = User::where('users.id',$id)->where('type','manager')->leftJoin('shipper_permissions','shipper_permissions.user_id','=','users.id')->select(['users.*','shipper_permissions.*'])->first();
                return view('shipper.pages.manager_management.edit',$params,compact('countries'));
            }else{
                return redirect()->back()->with('error',__('User does not exists'));
            }
        }else{
            return redirect()->back()->with('error',__('Something went wrong! please try again later'));
        }
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',           
        ],[
            'full_name.required' => 'Full name field is required.',
            'phone.required' => 'phone field is required.',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $input = $request->all();
        $user = User::where(['id' => $id,'type' => 'manager'])->first();
        if(!empty($user) && isset($user)){
            $user->name = $input['first_name'].' '.$input['last_name'];
            $user->first_name = $input['first_name'];
            $user->last_name = $input['last_name'];
            $user->phone = $input['phone'];
            $user->update();
    
            $shipper_permission = ShipperPermission::where('user_id',$id)->first();
            if(!empty($shipper_permission)){
                $update_permission['create_shipment'] = 0;
                if(isset($input['create_shipment']) && !empty($input['create_shipment'])){
                    $update_permission['create_shipment'] = $input['create_shipment'];
                }
    
                $update_permission['view_shipment'] = 0;
                if(isset($input['view_shipment']) && !empty($input['view_shipment'])){
                    $update_permission['view_shipment'] = $input['view_shipment'];
                }
    
                $update_permission['cancel_shipment'] = 0;
                if(isset($input['cancel_shipment']) && !empty($input['cancel_shipment'])){
                    $update_permission['cancel_shipment'] = $input['cancel_shipment'];
                }
    
                $update_permission['view_products'] = 0;
                if(isset($input['view_products']) && !empty($input['view_products'])){
                    $update_permission['view_products'] = $input['view_products'];
                }
    
                $update_permission['view_address'] = 0;
                if(isset($input['view_address']) && !empty($input['view_address'])){
                    $update_permission['view_address'] = $input['view_address'];
                }
    
                $update_permission['search_driver'] = 0;
                if(isset($input['search_driver']) && !empty($input['search_driver'])){
                    $update_permission['search_driver'] = $input['search_driver'];
                }
    
                $update_permission['manage_managers'] = 0;
                if(isset($input['manage_managers']) && !empty($input['manage_managers'])){
                    $update_permission['manage_managers'] = $input['manage_managers'];
                }
    
                $update_permission['manage_price'] = 0;
                if(isset($input['manage_price']) && !empty($input['manage_price'])){
                    $update_permission['manage_price'] = $input['manage_price'];
                }
    
                ShipperPermission::where('user_id',$id)->update($update_permission);
            }
            return redirect()->route('shipper.manager-management.index')->with('success', __('Shipper manager updated successfully.'));
        }else{
            return redirect()->back()->with('error',__('Something went wrong! please try again later'));
        }

    }

    public function approveStatus($id, $status){
       
        $data = User::find($id);
        if($status == "1")
            $data->status = 1 ;
        else
            $data->status = 0 ;
        $data->save(); 
        // if($status == "1"){
        //     $data = User::find($id);
        //      dispatch(new \App\Jobs\ShipperAcceptProfileJob($data));
        // }
       
        return \Response::json($data, 200);
    }

}
