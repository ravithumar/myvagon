<?php

namespace App\Http\Controllers\Shipper;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\Facility;
use App\Models\Role;
use App\Models\State;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Booking;
use App\Models\BookingRequest;
use App\Models\BookingBid;
use App\Models\ShipperPermission;
use App\Models\BookingTruck;
use App\Models\BookingLocations;
use App\Models\Availability;
use Carbon\Carbon;
use App\Helpers;

// use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use PermissionHelper;
use Twilio\Tests\Request as TestsRequest;
use SocketHelper;

class AuthController extends Controller
{
  
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    public function create($userid = null){

        $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
        $state_pluck = State::whereIn('country_id', $country_pluck)->pluck('id');

        $countries =  Country::whereIn('name', ['Greece'])->get();
        $states = State::whereIn('country_id', $country_pluck)->get();
        $cities = City::orderBy('id','desc')->whereIn('state_id', $state_pluck)->get();
        
        $data = "step1";
        return view('shipper.auth.register', compact('cities', 'data', 'userid','states','countries'));
    }

    public function store(Request $request){
        // echo '<pre>';print_r($request->all());die();
       
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6'
           
        ],[
            'required' => 'This value is required.'
        ]);
        
        if ($validator->fails()) {
         
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        if($request->input('email_verify_register') == "0"){
            return redirect()->back()->withInput()->with('error', 'Please verify your email address before submit !');
        }

        if($request->input('phone_verify_register') == "0"){
            return redirect()->back()->withInput()->with('error', 'Please verify your phone number before submit !');
        }

        $input = $request->all();

        #USER
        $user = new User();
        $user->name = $input['first_name'].' '.$input['last_name'];
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->email = $input['email'];
        $user->phone = $input['phone'];
        $user->password = Hash::make($input['password']);
        $user->email_verify = 1;
        $user->phone_verify = 1;
        $user->stage = 'pending';
        $user->status = 1;
        $user->type = 'company';
        $user->save();
       
        #ROLE
        $role = Role::where('name','shipper')->first();
            
        #USER_ROLE
        $user_role = UserRole::insert([
            'user_id' => $user->id,
            'role_id' => $role['id'],
        ]);

       
        $data = "step2";
        $userid = urlencode($user->id);

        $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
        $state_pluck = State::whereIn('country_id', $country_pluck)->pluck('id');

        $countries =  Country::whereIn('name', ['Greece'])->get();
        $states = State::whereIn('country_id', $country_pluck)->get();
        $cities = City::orderBy('id','desc')->whereIn('state_id', $state_pluck)->get();
       
        return view('shipper.auth.register', compact('data', 'userid', 'cities','states','countries'));

        

    }

    public function storeCompanyProfile(Request $request, $userid){
        $id = urldecode($userid);
        $user = User::where('id', $id)->first();
        
        
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postcode' => 'required',
            'ssn' => 'required',
            'company_phone' => 'required'
           
        ],[
            'required' => 'This value is required.'
        ]);
        
        if ($validator->fails()) {
         
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

       
        $input = $request->all();

        #USER
        $user->company_name = $input['company_name'];
        $user->city = $input['city'];
        $user->state = $input['state'];
        $user->country = $input['country'];
        $user->postcode = $input['postcode'];
        $user->ssn = $input['ssn'];
        $user->company_phone = $input['company_phone'];
        $user->update();

        $data = "step3";
        $userid = urlencode($user->id);

        $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
        $state_pluck = State::whereIn('country_id', $country_pluck)->pluck('id');

        $countries =  Country::whereIn('name', ['Greece'])->get();
        $states = State::whereIn('country_id', $country_pluck)->get();
        $cities = City::orderBy('id','desc')->whereIn('state_id', $state_pluck)->get();

        return view('shipper.auth.register', compact('data', 'userid','cities', 'states', 'countries'));

    }

    public function storeFacilityAddress(Request $request, $userid){
        $id = urldecode($userid);
        $user = User::where('id', $id)->first();
        
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postcode2' => 'required',
            'phone_number' => 'required',
            'address_email' => 'required'
           
        ],[
            'required' => 'This value is required.'
        ]);
        
        if ($validator->fails()) {

            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

       
        $input = $request->all();

        #USER
        $facility = new Facility();
        $facility->user_id  = $user->id;
        $facility->name  = $input['name'];
        $facility->address  = $input['address'];
        $facility->address_lat  = $input['lat'];
        $facility->address_lng  = $input['lng'];
        $facility->city  = $input['city'];
        $facility->state  = $input['state'];
        $facility->country  = $input['country'];
        $facility->zipcode  = $input['postcode2'];
        $facility->phone  = $input['phone_number'];
        $facility->email  = $input['address_email'];
        if($request->input('rest_area_service')) {
            $facility->rest_area_service  =  1 ;
        }
        if($request->input('food_service')){
            $facility->food_service  =  1;
        }
        if($request->input('restroom_service')){
            $facility->restroom_service  = 1 ;
        }
       
        $facility->save();
        $user_data = $user->id;
        $this->shipperVerification($request,$user_data);
        return redirect()->route('shipper.login')->with('success', 'Your registration is successfully completed.');

    }

    public function shipperVerification(Request $request, $user_data){
      
        $user = User::where('id', $user_data)->first();
        dispatch(new \App\Jobs\ShipperVerificationJob($user, $user->id));
        return redirect()->route('shipper.login')->with('success', 'Your registration is successfully completed.');
    }

    public function shipperVerified($id){
        $user = User::where('id', $id)->first();
        $user->email_verified_at = Carbon::now();
        $user->update();
        $user_data = $user->id;
        return view('shipper.auth.verified', compact('user_data'));
    }


    public function createLogin(){
        return view('shipper.auth.login');
    }

    public function storeLogin(Request $request){
        
        $validator = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required',
        ],[
            'email.required' => 'Email or Phone is required !',
        ]);

        if (!$validator->fails()) {
           // $credentials = $request->only('email', 'password');

             $credentials = $request->only('email', 'password');

            if(is_numeric($request->get('email'))){
                $credentials =  [ 
                    'phone'=> $request->get('email'),
                    'password'=>$request->get('password')
                ];
               
            }else if (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
                $credentials = [
                    'email' => $request->get('email'), 
                    'password'=> $request->get('password'),
                ];
               
            }
            
            if (Auth::attempt($credentials)) {
                $user = Auth::user(); 

                if ($user->hasRole($user->id,2)) {

                    if($user->stage == "accepted"){

                        if($user->status == 1){
                             Session::put('runnint', 'running');
                             if($user->type == 'manager'){
                                $user_permission = ShipperPermission::where('user_id',$user->id)->first();
                                if(!empty($user_permission) && isset($user_permission)){
                                    Session::put('manager_permission', $user_permission);
                                }
                             }

                            return redirect()->route('shipper.dashboard');
                        }
                        Auth::logout();
                        return redirect()->back()->with('warning', 'Sorry! Your account has been inactive by the system');
                        
                    }
                    Auth::logout();
                    return redirect()->back()->with('warning', 'Your profile is under verification , please try after some time!');
                    
                }
                Auth::logout();
                return redirect()->back()->with('error', 'Can not authorized for shipper login !');
            
            }
            Auth::logout();
            return redirect()
                        ->route('shipper.login')
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => 'Entered email address/phone number or password doesn "t" match with the records !',
                        ]);
        }
        
    }

    public function shipperDashboard(Request $request,$id=0){
        //  App::setLocale('en');
        // SocketHelper::emit('accept_request',['shipper_id'=>393]);
        $user =   Auth::user();
        
        if($user->type == 'manager'){
            $manage_price = PermissionHelper::check_access('manage_price');
        }else{
            $manage_price = '1';
        }
        $booking_count_all = Booking::where('user_id',$user->id)->count();
        $booking_count_pending = Booking::where(['user_id'=>$user->id,'status' => 'pending'])->count();
        $booking_count_scheduled = Booking::where(['user_id'=>$user->id,'status' => 'scheduled'])->count();
        $booking_count_in_process = Booking::where(['user_id'=>$user->id,'status' => 'in-progress'])->count();
        $booking_count_completed = Booking::where(['user_id'=>$user->id,'status' => 'completed'])->count();
        // $bookings = Booking::with(['booking_location','booking_truck.driver_details'])->where('user_id',$user->id)->orderBy('id','desc')->get()->toArray();
        $bookings = Booking::with(['booking_location','booking_truck.driver_details'])->where('user_id',$user->id)->orderBy('id','desc')->paginate(10);
        if ($request->ajax()) {
    		$view = view('shipper.pages.dashboard_lists',compact('bookings'))->render();
            return response()->json(['html'=>$view]);
        }

        $booking_locations = array();
        if(isset($bookings[0]['id']) && !empty($bookings[0]['id'])){ 
            $booking_locations = BookingLocations::whereBookingId($bookings[0]['id'])->get();
        }
        $location_aray = array();
        if(isset($booking_locations) && !empty($booking_locations)){
            for($i=0;$i<count($booking_locations);$i++){
                array_push($location_aray,[$booking_locations[$i]['drop_location'],$booking_locations[$i]['drop_lat'],$booking_locations[$i]['drop_lng'],$i+1]);  
            } 
        }
        return view('shipper.pages.dashboard',compact('bookings','location_aray','manage_price','booking_count_all','booking_count_pending','booking_count_scheduled','booking_count_in_process','booking_count_completed'));
    }

    public function logout(Request $request) {
        Auth::logout();
        Session::forget('manager_permission');
        Session::forget('truck_data');
        Session::forget('product_data');
        Session::forget('direct_shipment_data');
        Session::forget('search_available_truck_detail');
        Session::forget('edit_truck_data');
        Session::forget('edit_product_data');
        
        return redirect()->route('shipper.login');
    }

    
    public function createForgotPassword(){
        return view('shipper.auth.forgot_password');
    }

    public function forgotPasswordPost(ForgotPasswordRequest $request){

        $users = User::where('email', $request->input('email'))->get();
      
        if(!isset($users) && $users->count() > 0){
            return redirect()->back()->withInput()->with('error', 'Email address is not registered');
        }
        $status = Password::sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
                    ? back()->withInput($request->input())->with(['success' => 'Reset password link sent on your email'])
                    : back()->withInput($request->input())->withErrors(['error' => 'Entered email is not registered !']);


    }

    public function resetPassword($token){
      
        return view('shipper.auth.reset_password', ['token' => $token]);
    }

    public function resetPasswordPost(Request $request){
        
        $user = Auth::user();
        $status = Password::reset(
            $request->only('email', 'password', 'confirmed', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
    
                $user->save();
    
            }
        );
    
        return $status === Password::PASSWORD_RESET
                    ? redirect()->route('shipper.login')->with('success', __($status))
                    : back()->withErrors(['error' => [__($status)]]);
    }

    public function shipperEmailVerify($email){
      
        $user = User::where('email', $email)->first();
       
        if(isset($user) && !empty($user)){
            $status = 1;
        }else{
            $status = 0;
        }
       return Response::json($status, 200);


    }
    public function shipperEmailVerifyManager($email){
      
        $user = User::where('email', $email)->first();
       
        if(isset($user) && !empty($user)){
            
        }else{
            echo 'true';
        }
    }

    public function shipperPhoneVerify($phone){
        $user = User::where('phone', $phone)->first();
       
        if(isset($user) && !empty($user)){
            $status = 1;
        }else{
            $status = 0;
        }

        return Response::json($status, 200);
    }

    public function shipperEmailProgress($email){
        dispatch(new \App\Jobs\ShipperEmailOTPJob($email));
    }

    public function my_profile()
    {
        $user = Auth::user();
        $countries =  Country::whereIn('name', ['Greece'])->get();
        $user_data = User::where('id',$user->id)->first();
        // print_r($user_data['id']);die();
        return view('shipper.pages.my_profile',compact('countries','user_data'));
    }

    public function update_profile(Request $request){
        $validator = Validator::make(request()->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'company_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'post_code' => 'required',
            'ssn_number' => 'required',
            'phone' => 'required',
            'profile' => 'file',
        ],[
            'first_name.required' => 'First name is required !',
            'last_name.required' => 'Last name is required !',
            'company_name.required' => 'Company name is required !',
            'country.required' => 'Country is required !',
            'state.required' => 'State is required !',
            'city.required' => 'City is required !',
            'post_code.required' => 'Postcode is required !',
            'ssn_number.required' => 'SSN number is required !',
            'phone.required' => 'Phone number is required !',
            'profile.required' => 'profile number is required !',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $input = $request->all();
        $user = Auth::user();
        $user_data = User::where('id',$user->id)->first();
        if($request->has('profile')){
            $img = Helpers\CommonHelper::uploadImagesS3Bucket($request->file('profile'),'shipper/');
            $user_data->profile = $img;
        }
        $user_data->first_name = $input['first_name'];
        $user_data->last_name = $input['last_name'];
        $user_data->name = $input['first_name'].' '.$input['last_name'];
        $user_data->company_name = $input['company_name'];
        $user_data->country = $input['country'];
        $user_data->state = $input['state'];
        $user_data->city = $input['city'];
        $user_data->postcode = $input['post_code'];
        $user_data->ssn = $input['ssn_number'];
        $user_data->company_phone = $input['phone'];
        $user_data->update();
        return redirect(url('shipper/my-profile'))->with('success', __('Profile update successfully.'));
    }

    public function change_password()
    {
        return view('shipper.pages.change_password');
    }

    public function update_password(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required',
        ],[
            'old_password.required' => 'Old password is required',
            'new_password.required' => 'New password is required',
            'confirm_password.required' => 'Confirm password is required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $input = $request->all();
        $user = Auth::user();
        $user_data = User::where('id',$user->id)->first();
        $check_old_password = Hash::check($input['old_password'], $user_data->password);
        if($check_old_password){
            $user_data->password = Hash::make($input['new_password']);
            $user_data->update();
            return redirect(url('shipper/change-password'))->with('success', __('Your password changed successfully.'));
        }else{
            return redirect(url('shipper/change-password'))->with('error', __('Your old password has been wrong, please try again.'));
        }
    }

    public function carrier_info($id)
    {
        $user = DB::table('users')->where('id',$id)->first();
        $user_role = DB::table('user_role')->where(['user_id' => $id,'role_id' => '3'])->first();
        if(isset($user) && !empty($user) && isset($user_role) && !empty($user_role)){
            $booking_truck= BookingTruck::where('driver_id',$id)->first();
            $user = DB::table('users')->where('id',$id)->first();
            return view('shipper.pages.carrier-info',compact('booking_truck','user'));
        }else{
            return redirect()->back()->with('error', __('Something went wrong, please try again later.'));
        }
    }

    public function changeLan($locat = 'en')
    {
        // echo '<pre>';print_r($locat);die();
        Session::put('lan',$locat);
         \App::setLocale($locat);
        return redirect()->back();
    }

    public function cron_past_availability()
    {
        Availability::where('date','<',date('Y-m-d'))->where(['load_status' => '0'])->update(['load_status' => '2']);
    }

    public function cron_past_booking()
    {
        $booking_data = Booking::where('pickup_date','<',date('Y-m-d'))->whereIn('status', ['pending','create'])->get();
        Booking::where('pickup_date','<',date('Y-m-d'))->whereIn('status', ['pending','create'])->update(['status' => 'cancelled']);
        if(isset($booking_data) && !empty($booking_data)){
            foreach($booking_data as $booking){
                $booking_request = BookingRequest::where('booking_id',$booking->id)->first();
                if(isset($booking_request) && !empty($booking_request)){
                    
                    SocketHelper::emit('cron_past_booking_request_cancel',$booking_request->toArray());
                    BookingRequest::where('booking_id',$booking->id)->update(['status' => 2]);
                }

                $booking_bid = BookingBid::where('booking_id',$booking->id)->first();
                if(isset($booking_bid) && !empty($booking_bid)){
                    
                    SocketHelper::emit('cron_past_booking_bid_cancel',$booking_bid->toArray());
                    BookingBid::where('booking_id',$booking->id)->update(['status' => 2]);
                }
            }
        }
    }

    public function cron_past_booking_scheduled()
    {
        $booking_data = Booking::where('pickup_date','<',date('Y-m-d'))->whereIn('status', ['scheduled','in-progress'])->get();
        Booking::where('pickup_date','<',date('Y-m-d'))->whereIn('status', ['scheduled','in-progress'])->update(['status' => 'completed','end_trip' => time()]);
        if(isset($booking_data) && !empty($booking_data)){
            foreach($booking_data as $booking){
                $booking_request = BookingRequest::where('booking_id',$booking->id)->first();
                if(isset($booking_request) && !empty($booking_request)){
                    
                    SocketHelper::emit('cron_past_booking_request_cancel',$booking_request->toArray());
                    BookingRequest::where('booking_id',$booking->id)->update(['status' => 2]);
                }

                $booking_bid = BookingBid::where('booking_id',$booking->id)->first();
                if(isset($booking_bid) && !empty($booking_bid)){
                    
                    SocketHelper::emit('cron_past_booking_bid_cancel',$booking_bid->toArray());
                    BookingBid::where('booking_id',$booking->id)->update(['status' => 2]);
                }
            }
        }
    }
    public function setting()
    {
        return view('shipper.pages.setting');
    }
}


