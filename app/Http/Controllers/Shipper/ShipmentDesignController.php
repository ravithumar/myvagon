<?php

namespace App\Http\Controllers\Shipper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\TruckUnit;
use App\Models\Country;
use App\Models\Facility;
use App;

class ShipmentDesignController extends Controller
{
    public function create(){
    	return view ('shipper.pages.shipment.index');
    }

    public function designPage(){

    	return view ('shipper.pages.shipment.desing');
    }

    public function locations_sim(Request $request)
    {
        App::setLocale($request->lang);
        session()->put('locale', $request->lang);
        // dd($request->lang);sss
        $user = Auth::user();
        $products = Product::where('user_id', $user->id)->orderBy('id','desc')->get()->toArray();
        $unitData = TruckUnit::where('deleted_at',NULL)->get()->toArray();
        $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
        $countries =  Country::whereIn('name', ['Greece'])->get();
        $addresses = Facility::where('user_id', $user->id)->select('*')->with('city_obj')->orderBy('id','desc')->get();
        $count = count($addresses);
        return view('shipper.pages.shipment.shipment_locations_sim', compact('products','unitData','countries','addresses','count'));
    }
}
