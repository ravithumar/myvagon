<?php

namespace App\Http\Controllers\Shipper;
use App\Models\Notification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
	public function index()
    {
        $user =Auth::user();

        Notification::where(['user_id' => $user->id])->update(['read_status' => '1']);
    	// $data = Notification::get();
    	// print_r($data);
    	// $columns = [
     //                    ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px'], 
     //                    ['data' => 'user_id', 'name' => "user_id",'title' => __("User ID"),'width'=>'60px'], 
     //                    ['data' => 'title', 'name' => "title",'title' => __("Title"),'width'=>'60px'], 
     //                    ['data' => 'message', 'name' => "message",'title' => __("Message"),'width'=>'60px'], 
                 
     //                ];
     //    $dateTableFields = $columns;
        return view('shipper.pages.notification.index');
    }
}

?>