<?php

namespace App\Http\Controllers\Shipper;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\Country;
use App\Models\Product;
use App\Models\State;
use Illuminate\Http\Request;
use App\Models\BookingProduct;
use App\Models\BookingTruck;
use App\Models\Booking;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables as FacadesDataTables;
use PermissionHelper;
use SocketHelper;

class ChatController extends Controller
{
    public function __construct()
    {
        // $this->middleware(function ($request, $next) {
        //     $user =Auth::user();
            
        //     return $next($request);
        // });
    }

    public function index(Request $request) {
        $user = Auth::user();
        $receiver_id = $request->input('receiver_id');
        $receiver =  Message::select('users.*')->where('receiver_id', '=', $receiver_id)
        			->orWhere('sender_id', '=', $user['id'])
                    ->join('users','users.id','=','message.receiver_id')
                	->orderBy('message.created_at','desc')
                	->groupBy('id')->get()->toArray();
                	$users = $receiver;
                    $sender_id = $user['id'];
                    $sender_name = $user['name'];
            $receiver_id = $receiver_id;
        return view('shipper.pages.chat.chat',compact('users','sender_id','sender_name','receiver_id'));
    }

    public function chat_demo(Request $request,$id)
    {
        $user = Auth::user();
        // echo '<pre>';print_r($user);die();
        $booking = Booking::where('id',$id)->first();
        // print_r($booking);die();
        $receiver_id = $request->input('receiver_id');
        if(isset($receiver_id) && !empty($receiver_id)){
            // $sender = Message::select('message.id as message_id','users.id','users.name','message.receiver_id as sid','message.created_at','message.message')->where('sender_id', '=', $user['id'])->join('users','users.id','=','message.receiver_id');
            // $users = Message::where(['receiver_id' => $user['id']])->orWhere(['sender_id' => $user['id']])
            // ->orderBy('created_at','desc')
            // ->get();
            // echo '<pre>';print_r($users);die();
            $users_receiver = Message::select('users.*','message.message')->where(['sender_id' => $user['id'], 'receiver_id' => $receiver_id])
            ->join('users','users.id','=','message.receiver_id')
            ->orderBy('message.created_at','desc')
            ->groupBy('users.id')
            ->get()->toArray();
            
            $existsting_ids = [];

            if(isset($users_receiver) && !empty($users_receiver)){
                foreach($users_receiver as $receiver){
                    $existsting_ids[] = $receiver['id'];
                }
            }

            $users_sender = Message::select('users.*','message.message')->where(['sender_id' => $receiver_id,'receiver_id' => $user['id']])->whereNotIn('sender_id',$existsting_ids)
            ->join('users','users.id','message.sender_id')
            ->orderBy('message.created_at','desc')
            ->groupBy('users.id')
            ->get()->toArray();
            
            $receiver = array_merge($users_receiver,$users_sender);
            // $receiver = Message::select('users.*')
            // ->where(['sender_id' => $user['id'], 'receiver_id' => $receiver_id])
            // // ->orWhere(function($query) use($user,$receiver_id){
            // //     $query->where(['sender_id' => $receiver_id]);
            // //     $query->where(['receiver_id' => $user['id']]);
            // // })
            //         ->join('users','users.id','=','message.receiver_id')
            //     ->orderBy('message.created_at','desc')
            //     ->groupBy('id')
            //     ->get();

                // echo '<pre>';print_r($receiver);die();
            
                $messages = Message::select('rc.id as receiver_id','rc.name as receiver_name','sd.id as sender_id','sd.name as sender_name','message.sender_id','message.created_at','message.message')
            ->join('users as rc','rc.id','message.receiver_id')
            ->join('users as sd','sd.id','message.sender_id')
            ->where(['sender_id' => $user['id'], 'receiver_id' => $receiver_id])
            ->orWhere(function($query) use($user,$receiver_id){
                $query->where(['sender_id' => $receiver_id]);
                $query->where(['receiver_id' => $user['id']]);
            })
            ->get();
            $users = $receiver;
             // echo '<pre>';print_r($users);die();
            $sender_id = $user['id'];
            $sender_name = $user['name'];
            $receiver_id = $receiver_id;
            return view('shipper.pages.chat.chat_demo',compact('users','sender_id','sender_name','receiver_id','messages','booking'));
        }else{
            return redirect()->back()->with('error', 'Something went wrong, please try again');
        }
    }

    public function getChat(Request $request)
    {
      $user = Auth::user();  
    
        $messages = Message::where(function($query) use($user,$request){
            $query->where(['sender_id' => $user['id']]);
            $query->where(['receiver_id' => $request->receiver_id]);
        })->orwhere(function($query) use($user,$request){
            $query->where(['sender_id' => $request->receiver_id]);
            $query->where(['receiver_id' => $user['id']]);
        })->orderBy('id','asc')->get();
        // echo '<pre>';print_r($messages);die();
    //   $messages = Message::select('rc.id as receiver_id','rc.name as receiver_name','sd.id as sender_id','sd.name as sender_name','message.sender_id','message.created_at','message.message')
    //   ->join('users as rc','rc.id','message.receiver_id')
    //   ->join('users as sd','sd.id','message.sender_id')
    //   ->where(['sender_id' => $user['id'], 'receiver_id' => $request->receiver_id])->orWhere(['sender_id' => $request->receiver_id, 'receiver_id' => $user['id']])->get()->toArray();
        $final_response = [];
        if(!empty($messages)){
            foreach($messages as $key=>$row){
                if($row['sender_id'] == $user['id']){
                    $receiver_data = User::where('id',$request->receiver_id)->first();
                    $sender_data = User::where('id',$user['id'])->first();
                    $final_response[$key]['receiver_id'] = $request->receiver_id;
                    $final_response[$key]['receiver_name'] = $receiver_data->name;
                    $final_response[$key]['receiver_image'] = $receiver_data->profile;
                    $final_response[$key]['sender_id'] = $user['id'];
                    $final_response[$key]['sender_name'] = $sender_data->name;
                    $final_response[$key]['sender_image'] = $sender_data->profile;
                    $final_response[$key]['message'] = $row['message'];
                    $final_response[$key]['created_at'] = date('H:i',strtotime($row['created_at']));
                }else{
                    $receiver_data = User::where('id',$request->receiver_id)->first();
                    $sender_data = User::where('id',$user['id'])->first();
                    $final_response[$key]['sender_id'] = $request->receiver_id;
                    $final_response[$key]['sender_name'] = $receiver_data->name;
                    $final_response[$key]['sender_image'] = $receiver_data->profile;
                    $final_response[$key]['receiver_id'] = $user['id'];
                    $final_response[$key]['receiver_name'] = $sender_data->name;
                    $final_response[$key]['receiver_image'] = $sender_data->profile;
                    $final_response[$key]['message'] = $row['message'];
                    $final_response[$key]['created_at'] = date('H:i',strtotime($row['created_at']));
                }
            }
        }

    //   echo '<pre>';print_r($final_response);die();
        return response()->json(['status' => true,'data' => $final_response]);
    //   echo  json_encode(['status' => true,'data' => $messages]);
    }

    public function get_transaction()
    {
        $rev = Transaction::groupBy('month')->get([
                            DB::raw('YEAR(date) as year'),
                            DB::raw('MONTHNAME(date) as month'),
                            DB::raw('SUM(amount) as total')
                        ]);

        echo '<pre>';print_r($rev);die();
    }

}
