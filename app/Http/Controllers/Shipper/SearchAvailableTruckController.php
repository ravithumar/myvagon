<?php

namespace App\Http\Controllers\Shipper;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRole;
use App\Models\TruckType;
use App\Models\Availability;
use App\Models\Booking;
use App\Models\BookingTruck;
use App\Models\BookingLocations;
use App\Models\BookingRequest;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Helpers;
use App\Models\Notification;
use Booking as GlobalBooking;
use Laravel\Passport\Token; 
use Illuminate\Support\Facades\Validator;
use App\Models\DriverVehicle;
use DataTables;
// use ElephantIO\Engine\SocketIO\Session;
use Illuminate\Support\Facades\Session;
use PermissionHelper;
use SocketHelper;

class SearchAvailableTruckController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user =Auth::user();
            if($user->type == 'manager'){
                $permission = PermissionHelper::check_access('search_driver');
                if(empty($permission)){
                    return redirect()->back()->with('error',__("You can't access this module!"));
                }
            }
            return $next($request);
        });
    }

    public function index_bkp(Request $request,$id=0)
    {
        $user =   Auth::user();
        $booking = Booking::where('pickup_date', '>=', date('Y-m-d'))
        ->where(function($query) use ($id){
            if($id > 0){
                $query->where('id',$id);
            }
        })
        ->where('user_id',$user->id)->whereIn('status',['pending','create'])->orderBy('id','asc')->get()->toArray();
        $available_trucks = [];
        if(isset($booking) && !empty($booking)){
            $i = 0;
            foreach($booking as $book){
                $truck_type = TruckType::where(['id'=>$book['trucks']['truck_type']])->limit(1)->get()->toArray();
                $availability = $this->nearByDriver($book);
                foreach ($availability->toArray() as $row) {
                    $row['trucks_type'] = $truck_type[0];
                    $row['booking'] = $book;
                    $available_trucks[] = $row;
                }
            }
        }
        $params['available_trucks'] = $available_trucks;
        $columns = [
                        ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px'], 
                        ['data' => 'truck_type', 'name' => "truck_type",'title' => __("Truck Type"),'width'=>'60px'], 
                        ['data' => 'start_date', 'name' => __("amount"),'title' => __("Start Date"),'width'=>'60px'],
                        ['data' => 'start_location', 'name' => __("status"),'title' => __("Start Location"),'width'=>'100px'],
                        ['data' => 'end_location', 'name' => __("pickup_date"),'title' => __("End Location"),'width'=>'100px'],
                        ['data' => 'carrier_info', 'name' => __("end_date"),'title' => __("Carrier Info"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                        ['data' => 'price', 'name' => __("drop_location"),'title' => __("Price"),'width'=>'80px'],
                        ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                    ];
        $params['dateTableFields'] = $columns;
        $params['dateTableTitle'] = "Search Available Trucks";
        $params['dataTableId'] = time();
        return view('shipper.pages.search_available_trucks.index', $params);
    }

    public function index(Request $request,$id=0)
    {
        Session::forget('search_available_truck_detail');
        $user =   Auth::user(); 
        $availability_data = Availability::with(['trucks_type','driver_info','booking_request' => function($query){$query->where('status', '0');}])->where('date', '>=', date('Y-m-d'))->where('load_status','0')->orderBy('id','desc')->get()->toArray();

        $available_trucks = [];
        foreach($availability_data as $key => $value){
            if(isset($value['end_lng']) && !empty($value['end_lng']) && $value['end_lng'] != '0.0'){
                $availability_booking = $this->nearByLocationCount($value,'1');
            }else{
                $availability_booking = $this->nearByLocationCount($value,'0');
            }        
            // echo '<pre>';print_r($availability_booking);
            $value['booking_match'] = $availability_booking;
            $booking_request_shippers = array_column($value['booking_request'],'shipper_id');
            if(empty($booking_request_shippers)){
                $available_trucks[] = $value;
            }else{
                if(!in_array($user['type'] == 'manager' ? $user['company_ref_id'] : $user['id'],$booking_request_shippers)){
                    $available_trucks[] = $value;
                }
            }
        }
        // }die();
        $params['available_trucks'] = $available_trucks;
        $columns = [
                        ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px','orderable' => false], 
                        ['data' => 'truck_type', 'name' => "truck_type",'title' => __("Truck Type"),'width'=>'60px'], 
                        ['data' => 'start_date', 'name' => __("amount"),'title' => __("Start Date"),'width'=>'60px'],
                        ['data' => 'start_location', 'name' => __("status"),'title' => __("Start Location"),'width'=>'100px'],
                        ['data' => 'end_location', 'name' => __("pickup_date"),'title' => __("End Location"),'width'=>'100px'],
                        ['data' => 'carrier_info', 'name' => __("end_date"),'title' => __("Carrier Info"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                        ['data' => 'price', 'name' => __("drop_location"),'title' => __("Price"),'width'=>'80px'],
                        ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                    ];
        $params['dateTableFields'] = $columns;
        $params['dateTableTitle'] = "Search Available Trucks";
        $params['dataTableId'] = time();
        $params['truckDetails'] = TruckType::all();
        $params['count'] = count($params['truckDetails']);
        return view('shipper.pages.search_available_trucks.index', $params);
    }

    public function nearByLocation($driver,$drop_location = '0') {

        $current = Carbon::now()->startOfDay();
        $user =   Auth::user();

        if($drop_location == '1'){
            $select = ["booking.*", 'booking_bid.driver_id as bid_driver', DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $driver['end_lat'] . "))
            * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $driver['end_lng'] . "))
            + sin(radians(" . $driver['end_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
        }else{
            $select = ["booking.*", "booking_bid.driver_id as bid_driver", DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
        }

        $matches = Booking::with('booking_location')->distinct()
            ->select($select)
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')
            ->leftJoin('booking_bid', 'booking.id', 'booking_bid.booking_id')
            ->where('booking_trucks.truck_type', $driver['truck_type_id'])
            ->where('booking.pickup_date','=',$driver['date'])
            ->whereIn('booking.status',['pending','create'])
            ->where('booking.is_bid',$driver['is_bid'])
            ->where('booking.user_id',$user->id)
            ->having('pickup_distance', '<', 50)
            ->groupBy('booking_trucks.booking_id')
            ->orderBy('pickup_date', 'asc');
            if($drop_location == '1')
            {
                $matches->having('drop_distance', '<', 50);
                $matches->having('drop_distance', '>=', 0);
            }
        return $matches->get();

    }

    public function nearByLocationCount($driver,$drop_location = '0') {
        $current = Carbon::now()->startOfDay();
        $user =   Auth::user();

        if($drop_location == '1'){
            $select = ["booking.*", 'booking_bid.driver_id as bid_driver', DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $driver['end_lat'] . "))
            * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $driver['end_lng'] . "))
            + sin(radians(" . $driver['end_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
        }else{
            $select = ["booking.*", "booking_bid.driver_id as bid_driver", DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
        }

        $matches = Booking::distinct()->with('booking_location')
            ->select($select)
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')
            ->leftJoin('booking_bid', 'booking.id', 'booking_bid.booking_id')
            ->where('booking_trucks.truck_type', $driver['truck_type_id'])
            ->where('booking.pickup_date','=',$driver['date'])
            ->whereIn('booking.status',['pending','create'])
            ->where('booking.is_bid',$driver['is_bid'])
            ->where('booking.user_id',$user->id)
            ->having('pickup_distance', '<', 50)
            ->groupBy('booking_trucks.booking_id')
            // ->having('pickup_distance', '>=', 0)
            ->orderBy('pickup_date', 'asc');
            if($drop_location == '1')
            {
                $matches->having('drop_distance', '<', 50);
                $matches->having('drop_distance', '>=', 0);
            }
            // echo '<pre>';print_r($matches->get());die();
        
        return $matches->count();

    }

    function change_key( $array, $old_key, $new_key ) {

        if( ! array_key_exists( $old_key, $array ) )
            return $array;
    
        $keys = array_keys( $array );
        $keys[ array_search( $old_key, $keys ) ] = $new_key;
    
        return array_combine( $keys, $array );
    }

    public function bindShipment($shipmnts) {
        $unique_array = [];
        $hash_value = [];
        foreach ($shipmnts as $element) {
            $hash = $element['booking']['id'];
            $data['pickup_lat'] = $element['booking']['pickup_lat'];
            $data['pickup_lng'] = $element['booking']['pickup_lng'];
            $data['start_lat'] = $element['start_lat'];
            $data['start_lng'] = $element['start_lng'];
            $data['bid_amount'] = $element['bid_amount'];
            $data['id'] = $element['id'];
            if(!in_array($data,$hash_value)){
                $hash_value[] = $data;
                $unique_array[$hash] = $element;
            }
        }
        $result = array_values($unique_array);
        return $result;
    }

    function group_by($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val[$key]]['date'] = $val[$key];
            $return[$val[$key]]['bids_data'][] = $val;
        }
        $final = [];
        foreach ($return as $key => $value) {
            $final[]=$value;
        }
        return $final;
    }

    public function nearByDriver($booking,$drop_location='0') {

        $current = Carbon::now()->startOfDay();
        $last_key = array_key_last($booking['trucks']['locations']);

        $matches = Availability::distinct()
            ->select("availability.*","users.name as driver_name",
                DB::raw("6371 * acos(cos(radians(start_lat))
                                            * cos(radians(".$booking['pickup_lat'].")) * cos(radians(".$booking['pickup_lng'].") - radians(start_lng))
                                            + sin(radians(start_lat)) * sin(radians(".$booking['pickup_lat']."))) AS pickup_distance"),

                DB::raw("6371 * acos(cos(radians(end_lat))
                                            * cos(radians(".$booking['trucks']['locations'][$last_key]['drop_lat'].")) * cos(radians(".$booking['trucks']['locations'][$last_key]['drop_lng'].") - radians(end_lng))
                                            + sin(radians(end_lat)) * sin(radians(".$booking['trucks']['locations'][$last_key]['drop_lat']."))) AS drop_distance")

            )
            // ->select($select)
            ->leftJoin('users','users.id','=','availability.driver_id')
            ->where('availability.truck_type_id', $booking['trucks']['truck_type'])
            ->where('availability.date','>=',$booking['pickup_date'])
            ->where('availability.load_status','=','0')
            ->where('availability.is_bid','=',$booking['is_bid'])
            ->having('pickup_distance', '<', 50)
            ->having('drop_distance', '<', 50)
            ->having('drop_distance', '>=', 0)
            // ->having($having)
            // ->having('drop_distance', '<', 50)
            ->orderBy('id', 'asc')
            ->get();


        return $matches;

    }

    public function book_now($booking_id=0,$availability_id=0)
    {
        $availability_data = Availability::whereId($availability_id)->where('load_status','0')->first();
        if(!empty($availability_data)){
            if($booking_id > 0 && $availability_id > 0){
                $booking= Booking::whereId($booking_id)->orderBy('id','desc')->limit(1)->get()->toArray();
                
                $driver_availablity = Availability::whereId($availability_id)->first();
                // echo '<pre>';print_r($driver_availablity);die();
                if(empty($booking))
                {
                    return redirect(url('shipper/manage-shipment'))->with('error',__('Shipment not found!'));
                }
                $booking_locations = BookingLocations::with('products')->whereBookingId($booking_id)->get()->toArray();
                $booking_truck = BookingTruck::with(['truck_type'])->whereBookingId($booking_id)->get()->toArray();
                $driver_truck = TruckType::whereId($driver_availablity['truck_type_id'])->first();
    
                $booking = $booking[0];
                $driver_details = User::find($driver_availablity['driver_id']);
                $driver_vehicle_details = DriverVehicle::where('user_id',$driver_availablity['driver_id'])->with('TruckSubCategory')->first();
                if(empty($booking) || empty($driver_details)){
                    return redirect()->back()->with('error',__('Shipment not found!'));
                }
            }else{
                return redirect()->back()->with('error',__('Shipment not found!'));
            }
            return view('shipper.pages.search_available_trucks.details',compact('booking','booking_locations','driver_truck','driver_vehicle_details','booking_truck','driver_details','availability_id'));
        }else{
            return redirect(url('shipper/search-available-trucks'))->with('error',__('Driver is not available because another shipment is assigned or another shipper requests, please try another driver.'));
        }
    }

    public function bid_now($booking_id=0,$availability_id=0)
    {
        // echo '<pre>';print_r($availability_id);die();
        $availability_data = Availability::whereId($availability_id)->where('load_status','0')->first(); 
        if(!empty($availability_data)){
            if($booking_id > 0 && $availability_id > 0){
                $booking= Booking::whereId($booking_id)->orderBy('id','desc')->limit(1)->get()->toArray();
                
                $driver_availablity = Availability::whereId($availability_id)->first();
                if(empty($booking))
                {
                    return redirect(url('shipper/manage-shipment'))->with('error',__('Shipment not found!'));
                }
                $booking_locations = BookingLocations::with('products')->whereBookingId($booking_id)->get()->toArray();
                $booking_truck = BookingTruck::with(['truck_type'])->whereBookingId($booking_id)->get()->toArray();
                $driver_truck = TruckType::whereId($driver_availablity['truck_type_id'])->first();
                $driver_vehicle_details = DriverVehicle::where('user_id',$driver_availablity['driver_id'])->with('TruckSubCategory')->first();
                $booking = $booking[0];
                $driver_details = User::find($driver_availablity['driver_id']);
                if(empty($booking) || empty($driver_details)){
                    return redirect()->back()->with('error',__('Shipment not found!'));
                }
            }else{
                return redirect()->back()->with('error',__('Shipment not found!'));
            }
            return view('shipper.pages.search_available_trucks.bid_details',compact('booking','booking_locations','driver_vehicle_details','driver_truck','booking_truck','driver_details','availability_id'));
        }else{
            return redirect(url('shipper/search-available-trucks'))->with('error',__('Driver is not available because another shipment is assigned or another shipper requests, please try another driver.'));
        }
    }

    public function confirm_booking(Request $request)
    {
        // echo '<pre>';print_r($request->all());die();
        $booking_id = $request->post('booking_id');
        $availability_id = $request->post('availability_id');
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'availability_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect(url('shipper/search-available-trucks'))->with('error',__('Something went wrong! Please try again later'));
        }else{
            $availability_data = Availability::whereId($availability_id)->where('load_status','0')->first(); 
            if(!empty($availability_data)){
                $check_booking_data = Booking::where('id',$booking_id)->first();
                $user = Auth::user();
                $availability_details = Availability::where('id',$availability_id)->first();
                $check_is_exists_booking_request = BookingRequest::where(['booking_id' => $booking_id,'driver_id' => $availability_details['driver_id'],'status' => '0'])->exists();
                if(isset($check_booking_data) && !empty($check_booking_data) && empty($check_is_exists_booking_request)){
    
                    if(isset($availability_details['is_bid']) && $availability_details['is_bid'] == '0'){
                        $check_request_for_book = BookingRequest::where(['booking_id' => $booking_id,'is_bid' => '0'])->orderBy('id','desc')->first();
                        if((!isset($check_request_for_book) && empty($check_request_for_book)) || (isset($check_request_for_book) && !empty($check_request_for_book) && $check_request_for_book['time_difference'] > 1800)){
                            $driver_vehicle = DriverVehicle::where(['user_id' => $availability_details['driver_id']])->first();
                                // if($check_booking_data->status == 'create'){
                                    // Booking::whereId($booking_id)->update(array('amount' => $availability_details->bid_amount));
                                // }
                                // $update_booking_truck = BookingTruck::where(['booking_id' => $booking_id,'truck_type' => $availability_details['truck_type_id']])->update(array('driver_id' => $availability_details['driver_id']));
                                // $update_booking = Booking::where(['id' => $booking_id])->update(array('status' => 'scheduled'));
                                // $update_availablity_details = Availability::where('id',$availability_id)->update(array('load_status'=>'1','booking_id' => $booking_id));
                                $booking_request_data['booking_id'] = $booking_id;
                                $booking_request_data['driver_id'] = $availability_details['driver_id'];
                                if($user['type'] == 'manager'){
                                    $booking_request_data['shipper_id'] = $user['company_ref_id'];
                                }else{
                                    $booking_request_data['shipper_id'] = $user['id'];
                                }
                                $booking_request_data['availability_id'] = $availability_id;
                                $booking_request_data['amount'] = $check_booking_data['amount'];
                                $booking_request_data['status'] = 0;
                                $booking_request_data['is_bid'] = $availability_details['is_bid'];
                                $booking_request_data['request_time'] = date('Y-m-d H:i:s');
                                
                                $booking_request = BookingRequest::create($booking_request_data);
                                $socket_data['booking_id'] = $booking_id;
                                $socket_data['driver_id'] = $availability_details['driver_id'];
                                $socket_data['truck_type'] = $driver_vehicle['truck_type'];
                                $socket_data['truck_category'] = $driver_vehicle['truck_sub_category'];
                                $socket_data['availability_id'] = $availability_id;
                                $socket_data['schedule_time'] = time();
                                SocketHelper::emit('accept_request',$socket_data);
        
                                // Notification::create(['user_id' => $check_booking_data['user_id'], 'title' => 'Book Now', 'message' => 'Your Shipment booked Successfully'])
                                // return redirect(url('shipper/manage-shipment?status=scheduled'))->with('success',__('Your booking confirm successfully!'));
                                return redirect(url('shipper/manage-shipment'))->with('success',__('Your booking request sent successfully!'));
                            
                        }else{
                            return redirect(url('shipper/search-available-trucks'))->with('error',__('Please wait for some time, another book request already exists.'));
                        }
                    }else{
                        // if($check_booking_data->status == 'create'){
                            // Booking::whereId($booking_id)->update(array('amount' => $availability_details->bid_amount));
                        // }
                        // $update_booking_truck = BookingTruck::where(['booking_id' => $booking_id,'truck_type' => $availability_details['truck_type_id']])->update(array('driver_id' => $availability_details['driver_id']));
                        // $update_booking = Booking::where(['id' => $booking_id])->update(array('status' => 'scheduled'));
                        // $update_availablity_details = Availability::where('id',$availability_id)->update(array('load_status'=>'1','booking_id' => $booking_id));
                        $booking_request_data['booking_id'] = $booking_id;
                        $booking_request_data['driver_id'] = $availability_details['driver_id'];
                        if($user['type'] == 'manager'){
                            $booking_request_data['shipper_id'] = $user['company_ref_id'];
                        }else{
                            $booking_request_data['shipper_id'] = $user['id'];
                        }
                        $booking_request_data['availability_id'] = $availability_id;
                        $booking_request_data['amount'] = $check_booking_data['amount'];
                        $booking_request_data['status'] = 0;
                        $booking_request_data['is_bid'] = $availability_details['is_bid'];
                        $booking_request_data['request_time'] = date('Y-m-d H:i:s');
                        
                        $booking_request = BookingRequest::create($booking_request_data);
                        // Notification::create(['user_id' => $check_booking_data['user_id'], 'title' => 'Book Now', 'message' => 'Your Shipment booked Successfully'])
                        // return redirect(url('shipper/manage-shipment?status=scheduled'))->with('success',__('Your booking confirm successfully!'));
                        return redirect(url('shipper/manage-shipment'))->with('success',__('Your bidding request sent successfully!'));
                    }
                    
                }else{
                    return redirect(url('shipper/search-available-trucks'))->with('error',__('Already added request or shipment not available.'));
                }
            }else{
                return redirect(url('shipper/search-available-trucks'))->with('error',__('Driver is not available because another shipment is assigned or another shipper requests, please try another driver.'));
            }
        }
    }

    public function demo_page(Request $request,$id=0)
    {
        $truckDetails = TruckType::all();
        $user =   Auth::user();
        $availability_data = Availability::with(['trucks_type','driver_info'])->where('date', '>=', date('Y-m-d'))->where('load_status','0')->get()->toArray();
        $available_trucks = [];
        foreach($availability_data as $key => $value){
            $availability_booking = $this->nearByLocationCount($value,'1');
            $value['booking_match'] = $availability_booking;
            $available_trucks[] = $value;
            // dd($value);
        }
        if($request->ajax()){
            $output= ' ';
            $availability_data=Availability::with(['trucks_type','driver_info'])
            ->where('date','LIKE','%'.$request->search."%")
            ->ORwhere('from_address','LIKE','%'.$request->search."%")
            ->ORwhere('to_address','LIKE','%'.$request->search."%")
            ->ORwhere('bid_amount','LIKE','%' .$request->search."%")->whereBetween('bid_amount', [100,2000])
            ->get();
           
            // dd($availability_data);
            if($availability_data ){
                foreach ($availability_data as $key => $value) {
                    $output.='<tr>'.
                    '<td>' .$value->id .'</td>'.
                    '<td>'.$value['trucks_type']['name'].'</td>'.
                    '<td>'.$value->date.'</td>'.
                    '<td>'.$value->from_address.'</td>'.
                    '<td>'.$value->to_address.'</td>'.
                    '<td>'.$value['driver_info']['name'].'</td>'.
                    '<td>'.$value->bid_amount.'</td>'.
                    '<td>'.$value->is_bid.'</td>'.
                    
                    '<td>'  .'</td>'.
                    '</tr>';
                }
               
            }
             return Response($output);
        }
        // echo '<pre>';print_r($available_trucks);die();
        // $booking = Booking::where('pickup_date', '>=', date('Y-m-d'))
        // ->where(function($query) use ($id){
        //     if($id > 0){
        //         $query->where('id',$id);
        //     }
        // })
        // ->where('user_id',$user->id)->whereIn('status',['pending','create'])->orderBy('id','asc')->get()->toArray();
        // $available_trucks = [];
        // if(isset($booking) && !empty($booking)){
        //     $i = 0;
        //     foreach($booking as $book){
        //         $truck_type = TruckType::where(['id'=>$book['trucks']['truck_type']])->limit(1)->get()->toArray();
        //         $availability = $this->nearByDriver($book);
        //         foreach ($availability->toArray() as $row) {
        //             $row['trucks_type'] = $truck_type[0];
        //             $row['booking'] = $book;
        //             $available_trucks[] = $row;
        //         }
        //     }
        // }
        $params['available_trucks'] = $available_trucks;
        $params['truckDetails'] = $truckDetails = TruckType::where('status',1)->with(['category' => function ($q) {
            $q->active();
        }])->get()->toArray();
        $params['count'] = count($truckDetails);
        $columns = [
                        ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px'], 
                        ['data' => 'truck_type', 'name' => "truck_type",'title' => __("Truck Type"),'width'=>'60px'], 
                        ['data' => 'start_date', 'name' => __("amount"),'title' => __("Start Date"),'width'=>'60px'],
                        ['data' => 'start_location', 'name' => __("status"),'title' => __("Start Location"),'width'=>'100px'],
                        ['data' => 'end_location', 'name' => __("pickup_date"),'title' => __("End Location"),'width'=>'100px'],
                        ['data' => 'carrier_info', 'name' => __("end_date"),'title' => __("Carrier Info"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                        ['data' => 'price', 'name' => __("drop_location"),'title' => __("Price"),'width'=>'80px'],
                        ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                    ];
        $params['dateTableFields'] = $columns;
        $params['dateTableTitle'] = "Search Available Trucks";
        $params['dataTableId'] = time();
        return view('shipper.pages.search_available_trucks.demopage', $params,compact('truckDetails'));
    }

    public function search_available_truck_bidding(Request $request){
        $data = $request->all();
        if(isset($data['bid_request']) && !empty($data['bid_request']) && isset($data['availability_id']) && !empty($data['availability_id'])){
            if($data['bid_request'] == 'show_existing'){
                // $availability_data = Availability::whereId($data['availability_id'])->where('load_status','0')->first();
                return redirect(url('shipper/search-available-trucks/matched-shipment',$data['availability_id']));
            }elseif($data['bid_request'] == 'create_new'){
                $availability_data = Availability::whereId($data['availability_id'])->where('load_status','0')->first();
                if($availability_data){
                    $session_data['driver_detail'] = User::whereId($availability_data['driver_id'])->first()->toArray();
                    $truck_detail = DriverVehicle::with(['TruckType','TruckSubCategory'])->where('user_id',$availability_data['driver_id']  )->first()->toArray();
                    $truck_type = [];
                    if(isset($truck_detail)){
                        $truck_type['truck_type'][] = $truck_detail['truck_type']['id'];
                        $truck_type['truck_categories'][$truck_detail['truck_type']['id']][] = $truck_detail['truck_sub_category']['id'];
                        $truck_type['shipment_note'] = '';
                        $truck_type['hydraulic'] = '0';
                    }
                    $session_data['truck_detail'] = $truck_type;
                    $session_data['availability_data'] = $availability_data;
                    $session_data['driver_vehicle_detail'] = $truck_detail;
                    if(empty(Session::get('truck_data'))){
                        Session::put('truck_data', $session_data);
                    }else{
                        Session::forget('truck_data');
                        Session::put('truck_data', $session_data);
                    }
                    return redirect(url('shipper/shipment/step2/create'));
                }else{
                    return redirect()->back()->with('error',__('Something went wrong!'));        
                }
            }
        }else{
            return redirect()->back()->with('error',__('Something went wrong!'));
        }
    }

    public function matched_shipment($id)
    {
        $availability_data = Availability::whereId($id)->where('load_status','0')->first(); 
        if(isset($availability_data) && !empty($availability_data)){
            $params['driver_detail'] = User::whereId($availability_data['driver_id'])->first()->toArray();
            $params['truck_detail'] = DriverVehicle::with(['TruckType','TruckSubCategory'])->where('user_id',$availability_data['driver_id'])->first()->toArray();
            $params['availability_data'] = Availability::whereId($id)->where('load_status','0')->first();
            $availability_data = Availability::with(['trucks_type','driver_info'])->whereId($id)->where('load_status','0')->first()->toArray();
            $available_trucks = [];
            if(isset($availability_data['end_lng']) && !empty($availability_data['end_lng']) && $availability_data['end_lng'] != 0.0){
                $availability_booking = $this->nearByLocation($availability_data,'1');
            }else{
                $availability_booking = $this->nearByLocation($availability_data,'0');
            }
            $availability_data['booking_data'] = $availability_booking;
            $available_trucks = $availability_data;
            $params['available_trucks'] = $available_trucks;
            $columns = [
                            ['data' => 'id', 'name' => "id",'title' => __("Order Id"),'width'=>'20px'], 
                            ['data' => 'truck_type', 'name' => "truck_type",'title' => __("Price"),'width'=>'60px'], 
                            ['data' => 'start_date', 'name' => "start_date",'title' => __("Status"),'width'=>'60px'],
                            ['data' => 'start_location', 'name' => "start_location",'title' => __("Start Date"),'width'=>'100px'],
                            ['data' => 'end_location', 'name' => "end_location",'title' => __("End Date"),'width'=>'100px'],
                            ['data' => 'carrier_info', 'name' => "carrier_info",'title' => __("Delivery Location"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                            ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                        ];
            $params['dateTableFields'] = $columns;
            $params['dateTableTitle'] = "Search Available Trucks";
            $params['dataTableId'] = time();
            return view('shipper.pages.search_available_trucks.matched_shipment', $params);
        }else{
            return redirect()->back()->with('error',__('Something went wrong!'));
        }
    }

    public function filter(Request $request)
    {
        $data = $request->all();
        // echo '<pre>';print_r($data);die();
        if(isset($data) && !empty($data)){
            $user =   Auth::user(); 
            $truck_type_array = [];
            if(isset($data['truck_type']) && !empty($data['truck_type'])){
                $truck_type_array = explode(',',$data['truck_type']);
            }
            $availability_data = Availability::with(['trucks_type','driver_info','booking_request'])
            ->where(function($query) use($data,$truck_type_array){
                if(isset($data['start_date']) && !empty($data['start_date'])){
                    $query->where('date','=',date('Y-m-d', strtotime($data['start_date'])));
                }else{
                    $query->where('date', '>=', date('Y-m-d'));
                }
                if(isset($data['min_amount']) && !empty($data['min_amount'])){
                    $query->where('bid_amount','>=',$data['min_amount']);
                }
                if(isset($data['max_amount']) && !empty($data['max_amount'])){
                    $query->where('bid_amount','<=',$data['max_amount']);
                }
                if(isset($truck_type_array) && !empty($truck_type_array)){
                    $query->whereIn('truck_type_id',$truck_type_array);
                }
                if(isset($data['start_location']) && !empty($data['start_location'])){
                    if(!empty($data['start_location'][0]) && !empty($data['start_location'][1])){
                        $query->where(['start_lat' => $data['start_location'][0], 'start_lng' => $data['start_location'][1]]);
                    }
                }
                if(isset($data['end_location']) && !empty($data['end_location'])){
                    if(!empty($data['end_location'][0]) && !empty($data['end_location'][1])){
                        $query->where(['end_lat' => $data['end_location'][0], 'end_lng' => $data['end_location'][1]]);
                    }
                }
                $query->where('load_status','0');
            })
            ->orderBy('id','desc')
            ->get()->toArray();
            // echo '<pre>';print_r($availability_data);die();

            $output = '';
            if(!empty($availability_data) && isset($availability_data)){
                foreach($availability_data as $key => $value){
                    $availability_booking = $this->nearByLocationCount($value,'1');
                    $value['booking_match'] = $availability_booking;
                    $booking_request_shippers = array_column($value['booking_request'],'shipper_id');
                    if(empty($booking_request_shippers)){
                        $available_trucks[] = $value;
                    }else{
                        if(!in_array($user['type'] == 'manager' ? $user['company_ref_id'] : $user['id'],$booking_request_shippers)){
                            $available_trucks[] = $value;
                        }
                    }
                    // $available_trucks[] = $value;
                }

                foreach ($available_trucks as $key => $value) {
                    $output .= '<tr>';
                    $output .= '<td>' .$value['id']. '</td>';
                    $output .= '<td>' .$value['trucks_type']['name']. '</td>';
                    $output .= '<td>' .date('F d, Y',strtotime($value['date'])). '</td>';
                    $output .= '<td>' .$value['from_address']. '</td>';
                    $output .= '<td>' .$value['to_address']. '</td>';
                    $output .= '<td>' .$value['driver_info']['name']. '</td>';
                    $output .= '<td>€ ' .$value['bid_amount']. '</td>';
                    if($value['is_bid'] == '1'){
                        $output .= '<td><a href="javascript:void(0)" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" data-id="' .$value['id']. '" data-booking-count="' .$value['booking_match'] .'" class=" btn btn-xs text-default justify-content-end bid_request_click"> Bid</a></td>';
                    }else{
                        $output .= '<td><a href="javascript:void(0)" style="color: #b27be7;font-weight:600;border: 1px solid #b27be7;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> Book</a></td>';
                    }
                    $output .= '</tr>';
                }
            }else{
                $output = '<tr class="text-center"><td colspan="8">No Match Found</td></tr>';
            }
            return json_encode($output,200);

        }
    }

}
