<?php

namespace App\Http\Controllers\Shipper;

use App\Helpers\CommonHelper as HelpersCommonHelper;
use App\Models\City;
use App\Models\State;
use App\Models\Booking;
use App\Models\Country;
use App\Models\Product;
use App\Models\Facility;
use App\Models\TruckType;
use App\Models\Device;
use App\Models\TruckCategory;
use App\Models\TruckUnit;
use App\Models\PackageType;
use Illuminate\Http\Request;
use App\Models\BookingProduct;
use App\Models\BookingLocations;
use App\Models\BookingBid;
use App\Models\DriverLocation;
use App\Models\SystemConfig;
use App\Http\Controllers\Controller;

use App\Models\BookingLocation;
use App\Models\BookingProducts;
use App\Models\BookingTruck;
use App\Models\DriverVehicle;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Twilio\Tests\Request as TestsRequest;
use Yajra\DataTables\Facades\DataTables;
use PermissionHelper;
use Twilio\Http\Response;
use Carbon\Carbon;
use App\Models\Availability;
use App\Models\BookingRequest;
use App\Models\CancelBooking;
use Booking as GlobalBooking;
use PDF;
use Image;
use Storage;
use CommonHelper;
use Mail;

// use Illuminate\Support\Facades\Storage;
class ManageShipmentController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user =Auth::user();
            if($user->type == 'manager'){
                $permission = PermissionHelper::check_access('view_shipment');
                if(empty($permission)){
                    return redirect()->back()->with('error',__("You can't access this module!"));
                }
            }
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        Session::forget('search_available_truck_detail');
        Session::forget('truck_data');
        Session::forget('edit_truck_data');
        Session::forget('edit_product_data');
        Session::forget('product_data');
        $url_param = $request->input('status');
        $search_param = $request->input('search');
    
        if ($request->ajax()) {
            $search_param = $request->post('searching_data');
            $user =   Auth::user();
            if($user->type == 'manager'){
                $cancel_shipment = PermissionHelper::check_access('cancel_shipment');
                $manage_price = PermissionHelper::check_access('manage_price');
            }else{
                $cancel_shipment = '1';
                $manage_price = '1';
            }

            if($request->sorting_date=='created_at'){
               
                if($request->zero_value==0){
                     $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('created_at','ASC')->where('user_id',$user->id)->get();
                 }else{
                    $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('created_at','DESC')->where('user_id',$user->id)->get();
                 }
               
            }
            elseif($request->sorting_date=='pickup_date'){

                if($request->zero_value==0){
                    $data = Booking::orderBy('pickup_date','asc')->where('user_id',$user->id)->get();
                }else{
                    $data = Booking::orderBy('pickup_date','desc')->where('user_id',$user->id)->get();
                }
            }
            elseif($request->sorting_date=='complete_date'){
                 if($request->zero_value==0){
                $data = Booking::with(['booking_location' => function ($query1) {
                        $query1->orderBy('delivered_at', 'asc')->latest();
                    }])->orderBy('created_at','asc')->where('user_id',$user->id)->get();
            }else {
                $data = Booking::with(['booking_location' => function ($query1) {
                        $query1->orderBy('delivered_at', 'desc')->latest();
                    }])->orderBy('created_at','desc')->where('user_id',$user->id)->get();
            }
            }
            else{
               $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])
            ->select('*')->orderBy('id','desc')
            ->where(function($query) use($user,$search_param){

                $query->where('user_id',$user->id);
                if(!empty($search_param) && isset($search_param)){
                    $query->where('pickup_address', 'LIKE', "%{$search_param}%");
                }
            });
            }
            /*$data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])
            ->select('*')->orderBy('id','desc')
            ->where(function($query) use($user,$search_param){

                $query->where('user_id',$user->id);
                if(!empty($search_param) && isset($search_param)){
                    $query->where('pickup_address', 'LIKE', "%{$search_param}%");
                }
            });*/
            return DataTables::of($data)
                ->filter(function ($query) use ($request,$url_param) {
                    if ($request->get('status') != '') {
                        if($request->get('status') == 'pending'){
                            $query->whereIn('status', [$request->get('status'),'create']);
                        }else{
                            $query->where('status', $request->get('status'));
                        }
                    }
                    // elseif(!empty($url_param)){
                    //     if($url_param == 'pending'){
                    //         $query->whereIn('status', [$url_param,'create']);
                    //     }else{
                    //         $query->where('status', $url_param);
                    //     }
                    // }
                })
                ->addIndexColumn()
                ->addColumn('shipment_type', function ($row){
                    if($row['is_public'] == '1'){
                        $shipment_type = '<b>Direct</b><br> Public';
                    }else{
                        $shipment_type = '<b>Direct</b><br> Private';
                    }
                    if($row['booking_type'] == 'multiple_shipment'){
                        if($row['is_public'] == '1'){
                            $shipment_type = '<b>Multiple</b><br> Public';
                        }else{
                            $shipment_type = '<b>Multiple</b><br> Private';
                        }
                    }
                    return $shipment_type;

                })
                ->editColumn('action', function ($row) use($cancel_shipment){
                    if(isset($row) && !empty($row)){
                        $btn = '';
                        if(isset($row['is_bid']) && !empty($row['is_bid']) && $row['is_bid'] == 1){
                            $get_bid_request = BookingBid::where(['booking_id' => $row['id'],'status' => '0'])->count();
                            if($get_bid_request > 0){
                                $btn .= '<a href="'.url('shipper/manage-shipment',$row['id']).'" style="color: #000000;text-decoration:underline;font-weight: bold;" class="font-14"> View Details</a><br><p style="color: #9B51E0;">'.$get_bid_request.' Bids</p>';
                            }else{
                                $btn .= '<a href="'.url('shipper/manage-shipment',$row['id']).'" style="color: #000000;text-decoration:underline;font-weight: bold;" class="font-14"> View Details</a></p>';
                            }
                        }else{
                            $btn .= '<a href="'.url('shipper/manage-shipment',$row['id']).'" style="color: #000000;text-decoration:underline;font-weight: bold;" class="font-14"> View Details</a>';
                        }
                    }
                    return $btn;
                })
                ->addColumn('start_date',function($row){
                    $start_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $start_date = date('d/m/Y',strtotime($row['pickup_date'])) .'<br> TO <br>'. date('d/m/Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $start_date;
                })
                ->addColumn('drop_location',function($row){
                    $drop_location = '-';
                    if(isset($row) && !empty($row)){
                        $company_name = isset($row['booking_location'][0]['company_name']) ? $row['booking_location'][0]['company_name'] : "-";
                        $drop_location = isset($row['booking_location'][0]['drop_location'])?$row['booking_location'][0]['drop_location']:"-";
                        $drop_location = '<b>'.$company_name.'</b><br>'.$drop_location;
                    }
                    return $drop_location;
                })
                ->addColumn('pickup_address',function($row){
                    $pickup_address = '-';
                    if(isset($row) && !empty($row)){
                        $last_key = array_key_last($row['booking_location']->toArray());
                        $company_name = isset($row['booking_location'][$last_key]['company_name']) ? $row['booking_location'][$last_key]['company_name'] : "-";
                        $pickup_address = isset($row['booking_location'][$last_key]['drop_location'])?$row['booking_location'][$last_key]['drop_location']:"-";
                        $pickup_address = '<b>'.$company_name.'</b><br>'.$pickup_address;
                    }
                    return $pickup_address;
                })
                ->editColumn('status',function($row){
                    $status = '';
                    if($row['status'] == 'create'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        $status .= '<br><small class="text-danger">'.__("Waiting for truck").'</small>';
                    }elseif($row['status'] == 'scheduled'){
                        $status = '<label class="scheduled-status" style="padding: 5px 10px;">'.__("Scheduled").'</label>';
                        $status .= '<br><small style="color:#c9d569">'.\Carbon\Carbon::parse($row['pickup_date'])->diffForHumans().'</small>';
                    }elseif($row['status'] == 'pending'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        if($row['is_bid'] == '1'){
                            $status .= '<br><small class="text-danger">'.__("Waiting for bid").'</small>';
                        }else{
                            $status .= '<br><small class="text-danger">'.__("Waiting for book").'</small>';
                        }
                    }elseif($row['status'] == 'in-progress'){
                        $status = '<label class="in-process-status" style="padding: 7px 10px;width: 109%;">'.__("In-progress").'</label>';
                    }elseif($row['status'] == 'completed'){
                        $status = '<label class="completed-status" style="padding: 5px 10px;">'.__("Completed").'</label>';
                        if(!empty($row['pod_image']) && isset($row['pod_image'])){
                            if($row['payment_status'] == 'pending'){
                                $status .= '<br><small style="color:#7fd569">'.__("Payment Pending").'</small>';
                            }else{
                                $status .= '<br><small style="color:#7fd569">'.__("Payment Successful").'</small>';
                            }   
                        }else{
                            $status .= '<br><small style="color:#7fd569">'.__("Missing POD").'</small>';
                        }
                    }else{
                        $status = '<label class="cancelled-status" style="padding: 5px 10px;">'.__("Cancelled").'</label>';
                        $status .= '<br><small style="color:#252a38">'.__("Cancellation fee*").'</small>';
                    }
                    return $status;
                })
                ->editColumn('amount',function($row) use($manage_price){
                    $amount = '';
                    if(!empty($manage_price)){
                        $amount = '€'.CommonHelper::price_format($row['amount'],'GR');
                    }else{
                        $amount = '-';
                    }
                    return $amount;
                })

                ->rawColumns(['shipment_type','action','status','drop_location','pickup_address','start_date' ])
                ->make("*");
        } else {
            $columns = [
                        ['data' => 'shipment_type', 'name' => "shipment_type",'title' => __("Type"),'width'=>'10%','searchable' => false, 'orderable' => false], 
                        ['data' => 'id', 'name' => "id",'title' => __("Order Id"),'width'=>'8%'], 
                        ['data' => 'amount', 'name' => __("amount"),'title' => __("Price"),'width'=>'10%'],
                        ['data' => 'status', 'name' => __("status"),'title' => __("Status"),'width'=>'12%'],
                        ['data' => 'start_date', 'name' => __("pickup_date"),'title' => __("Date"),'width'=>'15%','searchable' => false, 'orderable' => false],
                        ['data' => 'pickup_address', 'name' => __("pickup_address"),'title' => __("Pickup Location"),'width'=>'17.5%', 'searchable' => false, 'orderable' => false],
                        ['data' => 'drop_location', 'name' => __("drop_location"),'title' => __("Delivery Location"),'width'=>'17.5%','searchable' => false, 'orderable' => false],
                        ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"),'width'=>'10%', 'searchable' => false, 'orderable' => false ]
                    ];
            $params['dateTableFields'] = $columns;
            // $params['dateTableUrl'] = route('manage-shipment.index');
            $params['dateTableUrl'] = route('shipper.manage-shipment.index');
            $params['dateTableTitle'] = "Manage Shipment";
            $params['addUrl'] = "manage-shipment";
            $params['dataTableId'] = time();
            $params['url_param'] = $url_param;
            $params['search_param'] = $search_param;
            // $params['cities'] = $cities;
            // $params['states'] = $states;
            // $params['countries'] = $countries;
            return view('shipper.pages.manage_shipment.index', $params);
        }
        // return view('shipper.pages.manage_shipment.index');
    }

    public function change_status(Request $request)
    {   
        $booking_id = $request->post('id');
        $status = $request->post('status');
        if(!empty($booking_id) && !empty($status)){
            Booking::where('id',$booking_id)->update(array('status'=>$status));
            $flag = 1;
        }
        $flag = 0;
        return \Response::json($flag, 200);
    }

    public function details($id)
    {
        $user =  BookingTruck::with(['truck_type','driver_details'])->whereBookingId($id)->get()->toArray();
        
        $booking_locations = BookingLocations::with('products')->whereBookingId($id)->get()->toArray();
        // dd($booking_locations);
        if(isset($id) && !empty($id)){
            $user = Auth::user();
            if($user['type'] == 'manager'){
                // $booking = Booking::whereId($id)->where('user_id',$user['company_ref_id'])->orderBy('booking.id','desc')->limit(1)->first();
                $booking= Booking::whereId($id)->where('user_id',$user['company_ref_id'])->orderBy('id','desc')->limit(1)->get()->toArray();
            }else{
                $booking= Booking::whereId($id)->where('user_id',$user['id'])->orderBy('id','desc')->limit(1)->get()->toArray();
                // $booking = Booking::whereId($id)->where('user_id',$user['id'])->orderBy('booking.id','desc')->limit(1)->first();
            }
            // dd($booking);die();
            if(empty($booking))
            {
                return redirect(url('shipper/manage-shipment'))->with('error',__('Shipment not found!'));
            }
            $booking_locations = BookingLocations::with('products')->whereBookingId($id)->get()->toArray();
            $booking_truck = BookingTruck::with(['truck_type','driver_details'])->whereBookingId($id)->get()->toArray();
            $booking = $booking[0];
            if(empty($booking)){
                return redirect()->back()->with('error',__('Shipment not found!'));
            }    
        }else{
            return redirect()->back()->with('error',__('Shipment not found!'));
        }
        // $calculation = $this->calculate_distance($booking['pickup_address'],$booking['booking_location']['drop_location']);
        if($booking['status'] == 'create'){
            return redirect(url('shipper/shipment/search',$booking['id']));
        }
        
        if($user->type == 'manager'){
            $cancel_shipment = PermissionHelper::check_access('cancel_shipment');
            $manage_price = PermissionHelper::check_access('manage_price');
            $search_driver = PermissionHelper::check_access('search_driver');
        }else{
            $cancel_shipment = 1;
            $manage_price = 1;
            $search_driver = 1;
        }
        $booking_bid = BookingBid::with(['driver','driver_vehicle.TruckType'])->where(['booking_id' => $id,'status' => '0'])->orderBy('id','asc')->get()->toArray();
        $bookingss = Booking::where('pickup_date', '>=', date('Y-m-d'))
        ->where(function($query) use ($id){
            if($id > 0){
                $query->where('id',$id);
            }
        })
        ->where('user_id',$user->id)->whereIn('status',['pending','create'])->orderBy('id','asc')->get()->toArray();
        $available_trucks = [];
        $user =   Auth::user();
        
            
            

       if($booking['status'] == 'cancelled')
       {
        foreach ($booking as $key => $cancellation_charge) {
           
           $cancellation_charge = SystemConfig::where(['path' => 'shipper_cancellation_charge'])->orderBy('id','asc')->value('value');
        }
        $data = Booking::where(['status' => 'cancelled'])->get(['cancellation_charge']);
        // print_r($data);die();
       }


        
        if(isset($bookingss) && !empty($bookingss)){
            $i = 0;
            foreach($bookingss as $book){
                
                $truck_type = TruckType::where(['id'=>$book['trucks']['truck_type']])->limit(1)->get()->toArray();
                $availability = $this->nearByDriver($book);
                foreach ($availability->toArray() as $row) {
                    // echo '<pre>';print_r($row);die();
                    $row['trucks_type'] = $truck_type[0];
                    $row['booking'] = $book;
                    if(empty($row['booking_request'])){
                        $available_trucks[] = $row;
                    }else{
                        if(isset($row['booking_request']) && !empty($row['booking_request'])){
                            foreach($row['booking_request'] as $booking_request){
                                if($booking_request['shipper_id'] != ($user['type'] == 'manager' ? $user['company_ref_id'] : $user['id'])){
                                    $available_trucks[] = $row;
                                }
                            }
                        }
                    }
                    // $available_trucks[] = $row;
                }
            }
        }
        $available_trucks = $available_trucks;
        $columns = [
                        ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px'], 
                        ['data' => 'truck_type', 'name' => "truck_type",'title' => __("Truck Type"),'width'=>'60px'], 
                        ['data' => 'start_date', 'name' => __("start_date"),'title' => __("Start Date"),'width'=>'60px'],
                        ['data' => 'start_location', 'name' => __("status"),'title' => __("Start Location"),'width'=>'100px'],
                        ['data' => 'end_location', 'name' => __("end_location"),'title' => __("End Location"),'width'=>'100px'],
                        ['data' => 'carrier_info', 'name' => __("carrier_info"),'title' => __("Carrier Info"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                        ['data' => 'price', 'name' => __("price"),'title' => __("Price"),'width'=>'80px'],
                        ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                    ];
        $dateTableFields = $columns;
        $dataTableId = time();
        $booking_requests = BookingRequest::where(['booking_id' => $id,'status' => '0'])->with(['driver','availability'])->get()->toArray();
        $driver_location = DriverLocation::whereDriverId($booking['trucks']['driver_id'])->first();
        $cancel_request = CancelBooking::where(['booking_id' => $id,'status' => 0])->orderBy('id','desc')->first();
        // echo '<pre>';print_r($booking_locations);die();
        return view('shipper.pages.manage_shipment.details',compact('booking','booking_locations','booking_truck','booking_bid','cancel_shipment','manage_price','search_driver','dataTableId','dateTableFields','available_trucks','booking_requests','driver_location','user','cancel_request'));
    }

    public function nearByDriver($booking,$drop_location='0') {

        $current = Carbon::now()->startOfDay();
        $last_key = array_key_last($booking['trucks']['locations']);

        $matches = Availability::distinct()
            ->select("availability.*","users.name as driver_name",
                DB::raw("6371 * acos(cos(radians(start_lat))
                                            * cos(radians(".$booking['pickup_lat'].")) * cos(radians(".$booking['pickup_lng'].") - radians(start_lng))
                                            + sin(radians(start_lat)) * sin(radians(".$booking['pickup_lat']."))) AS pickup_distance"),

                DB::raw("6371 * acos(cos(radians(end_lat))
                                            * cos(radians(".$booking['trucks']['locations'][$last_key]['drop_lat'].")) * cos(radians(".$booking['trucks']['locations'][$last_key]['drop_lng'].") - radians(end_lng))
                                            + sin(radians(end_lat)) * sin(radians(".$booking['trucks']['locations'][$last_key]['drop_lat']."))) AS drop_distance")

            )
            // ->select($select)
            ->with(['booking_request' => function($query) {$query->where('status', '0');}])
            ->leftJoin('users','users.id','=','availability.driver_id')
            ->where('availability.truck_type_id', $booking['trucks']['truck_type'])
            ->where('availability.date','=',$booking['pickup_date'])
            ->where('availability.load_status','=','0')
            ->where('availability.is_bid',$booking['is_bid'])
            ->having('pickup_distance', '<', 50)
            ->having('drop_distance', '<', 50)
            ->having('drop_distance', '>=', 0)
            ->orderBy('id', 'asc')
            ->get();


        return $matches;

    }

    public function calculate_distance($pick_location,$drop_location)
    {
        if ($pick_location != '' && $drop_location != '') {
            $distance_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($pick_location) . "&destinations=" . urlencode($drop_location) . "&mode=driving&key=" . env('MAP_KEY');

            $distance = (array) json_decode(file_get_contents($distance_url));
            $km = (Float) $distance['rows'][0]->elements[0]->distance->value;
            $km = $km / 1000;
            $duration = (Float) $distance['rows'][0]->elements[0]->duration->value;
            $duration = $duration / 60;
            $duration = $duration / 60;
            $data['miles'] = round($km * 0.621371); // kilometer to miles conversion
            $data['journey'] = round($duration);
        } else {
            $data['miles'] = 0;
            $data['journey'] = 0;
        }
        return $data;
    }

    public function destroy(Request $request, $id = null) {
      
        $booking = Booking::where('id',$id)->delete();
        // $booking->delete();
        // $booking_product = BookingProduct::where('booking_id',$id)->delete();
        
        // $booking_location = BookingLocations::where('booking_id',$id)->delete();
        // $booking_truck = BookingTruck::where('booking_id',$id)->delete();
        // $booking_location->delete();
        return \Response::json('success', 200);
       
    }

    public function design()
    {
        return view('shipper.pages.manage_shipment.design');
    }

    public function accept_bid_request(Request $request)
    {
        $id = $request->post('id');
        $driver_id = $request->post('driver_id');
        $booking_id = $request->post('booking_id');
        $availability_id = $request->post('availability_id');
        if(!empty($id) && !empty($booking_id) && !empty($driver_id)){
            $bid_data = BookingBid::where('id',$id)->first();
            if(isset($bid_data) && !empty($bid_data)){
                $driver_details = DriverVehicle::where('user_id',$driver_id)->first();
                if(!empty($driver_details)){
                    $booking_data = Booking::with('booking_location')->where(['id' => $booking_id])->first()->toArray();
                    $first_key = array_key_first($booking_data['booking_location']);
                    $last_key = array_key_last($booking_data['booking_location']);

                    $check_driver_is_free = BookingTruck::where(['driver_id' => $driver_id])
                    ->select(['booking_trucks.*','booking.id as booking_id','booking.status as booking_status','booking.pickup_date as booking_pickup_date'])
                    ->join('booking','booking.id','=','booking_trucks.booking_id')
                    ->join('booking_locations','booking_locations.booking_id','=','booking_trucks.booking_id')
                    ->whereIn('booking_locations.delivered_at',[$booking_data['booking_location'][$first_key]['delivered_at'],$booking_data['booking_location'][$last_key]['delivered_at']])
                    ->whereIn('booking.status',['scheduled','in-progress'])
                    ->get()->toArray();

                    if(isset($check_driver_is_free) && !empty($check_driver_is_free)){
                        return \Response::json(['error' => true,'message' => 'You have already scheduled a shipment for applied dates and time.'], 200);
                    }else{
                        $update_booking_truck = BookingTruck::where(['booking_id' => $booking_id,'truck_type' => $driver_details->truck_type])->update(['driver_id' => $driver_id]);
                        if($update_booking_truck){
                            BookingTruck::where(['booking_id' => $booking_id])->whereNotIn('truck_type', [$driver_details->truck_type])->delete();
                            BookingTruck::where(['booking_id' => $booking_id])->update(['truck_type_category' => $driver_details['truck_sub_category']]);
                        }
                        $update_booking = Booking::where(['id' => $booking_id])->update(['status' => 'scheduled','amount' => number_format($bid_data['amount'],2),'schedule_time' => time()]);
                        $update_booking_bid_all = BookingBid::where('booking_id',$booking_id)->update(['status' => 2]);
                        $update_booking_bid = BookingBid::where('id',$id)->update(['status' => 1]);
                        if(isset($availability_id) && !empty($availability_id)){
                            $update_availability = Availability::where('id',$availability_id)->update(['booking_id' => $booking_id,'load_status' => '1']);
                        }
                        $driver_device_data = Device::where('user_id',$driver_id)->orderBy('id','desc')->first();
                        if(isset($driver_device_data) && !empty($driver_device_data)){
                            $user_setting = CommonHelper::users_setting($driver_id);
                            if($user_setting['notification'] == 1 && $user_setting['bid_accepted'] == 1){
                                CommonHelper::send_push($driver_device_data->type,$driver_device_data->token,'Bid Request','Shipper has accepted your booking #'.$booking_id.' bid request','bid_request',[],$driver_id);                            
                            }
                        }
                        return \Response::json(['success' => true,'message' => 'Shipment assign successfully'], 200);
                    }
                }
            }
        }
    }

    public function decline_bid_request(Request $request)
    {
        $id = $request->post('id');
        $driver_id = $request->post('driver_id');
        $booking_id = $request->post('booking_id');
        $availability_id = $request->post('availability_id');
        if(!empty($id) && isset($id)){
            $check_bid_exists = BookingBid::where('id',$id)->first();
            if(!empty($check_bid_exists)){
                if(isset($check_bid_exists->availability_id) && !empty($check_bid_exists->availability_id) && $check_bid_exists->availability_id > 0){
                    Availability::where('id',$check_bid_exists->availability_id)->update(['load_status' => '0']);
                }
                $update_booking_bid = BookingBid::where('id',$id)->update(['status' => 2]);
                $driver_device_data = Device::where('user_id',$check_bid_exists->driver_id)->orderBy('id','desc')->first();
                if(isset($driver_device_data) && !empty($driver_device_data)){
                    CommonHelper::send_push($driver_device_data->type,$driver_device_data->token,'Bid Request','Shipper has decline your booking #'.$booking_id.' bid request','bid_request',[],$check_bid_exists->driver_id);
                }
                return \Response::json($update_booking_bid, 200);
            }
        }
    }

    public function map_loading(Request $request)
    {
        $booking_id = $request->post('id');
        if(!empty($booking_id) && isset($booking_id)){
            $check_is_exists = BookingLocations::where('booking_id',$booking_id)->exists();
            if($check_is_exists){
                $booking_location = BookingLocations::where('booking_id',$booking_id)->get();
                $location_array = array();
                if(isset($booking_location) && !empty($booking_location)){
                    for($i=0;$i<count($booking_location);$i++){
                        array_push($location_array,[$booking_location[$i]['drop_location'],$booking_location[$i]['drop_lat'],$booking_location[$i]['drop_lng'],$i+1]);  
                    } 
                }
                return \Response::json($location_array, 200);                
            }
        }
    }

    public function carrier_info($id){
        $booking_truck= BookingTruck::where('driver_id',$id)->first();

        $user = DB::table('users')->where('id',$id)->first();
        
            
        return view('shipper.pages.manage_shipment.carrier-info',compact('booking_truck','user'));
    }

    public function test_time()
    {
        // echo '<pre>';print_r(money_format(15200));die();
        $number = '111.459,87';
        $number = str_replace(',','.',str_replace('.','',$number));
        echo number_format($number, 2, ',', '.');
    }

    public function cancelRequest(Request $request)
    {
        $id = $request->post('cancel_id');
        if(!empty($id) && isset($id)){
            $cancel_request = CancelBooking::where('id',$id)->first();
            if(isset($cancel_request) && !empty($cancel_request)){
                $update_booking_truck = BookingTruck::where(['booking_id' => $cancel_request->booking_id,'driver_id' => $cancel_request->driver_id])->update(['driver_id' => 0]);
                $update_booking = Booking::where(['id' => $cancel_request->booking_id])->update(['status' => 'pending']);
                $update_cancel_request = CancelBooking::where('id',$id)->update(['status' => 1]);
                Availability::where(['booking_id' => $cancel_request->booking_id,'load_status' => '2'])->update(['load_status' => '0']);
                $device_tokens = Device::where('user_id',$cancel_request['driver_id'])->first();
                if(isset($device_tokens) && !empty($device_tokens)){
                    CommonHelper::send_push($device_tokens['type'],$device_tokens['token'],'Cancel Shipment','Shipper cancel booking #'.$cancel_request->booking_id.', which is assign to you.','cancel_shipment',[],$device_tokens['user_id']);
                }
                return \Response::json($update_cancel_request, 200);
            }
        }
    }

    public function cancelShipment(Request $request)
    {
        $booking_id = $request->post('booking_id');
        if(isset($booking_id) && !empty($booking_id)){
            $booking_data = Booking::where(['id' => $booking_id])->first();
            if(isset($booking_data) && !empty($booking_data)){
                if($booking_data['status'] == 'scheduled'){
                    $cancellation_charge = SystemConfig::where(['path' => 'shipper_cancellation_carge'])->value('value');
                    $insert_data['booking_id'] = $booking_id;
                    $insert_data['driver_id'] = 0;
                    $insert_data['shipper_id'] = $booking_data['user_id'];
                    $insert_data['request_time'] = date('Y-m-d H:i:s');
                    $insert_data['status'] = 1;
                    $insert_data['cancellation_charge'] = !empty($cancellation_charge) && isset($cancellation_charge) ? $cancellation_charge : 0;
                    CancelBooking::insert($insert_data);

                    $update_booking_data = Booking::where(['id'=>$booking_id])->update(['status' => 'cancelled']);
                    // $update_booking_truck = BookingTruck::where(['booking_id'=>$booking_id])->update(['driver_id' => 0]);
                    return \Response::json($update_booking_data, 200);
                    // echo '<pre>';print_r($cancellation_charge);die();
                }else{
                    $update_booking = Booking::where('id',$booking_id)->update(['status' => 'cancelled']);
                    return \Response::json($update_booking, 200);
                }
            }
        }
    }

    public function cancelDriver(Request $request)
    {
        $booking_id = $request->post('booking_id');
        $driver_id = $request->post('driver_id');
        if(isset($driver_id) && !empty($driver_id) && isset($booking_id) && !empty($booking_id)){
            $booking_data = Booking::where(['id' => $booking_id])->first();
            if(isset($booking_data) && !empty($booking_data)){
                if($booking_data['status'] == 'scheduled' || $booking_data['status'] == 'in-progress'){
                    $update_booking_data = Booking::where(['id'=>$booking_id])->update(['status' => 'pending']);
                    $device_tokens = Device::where('user_id',$driver_id)->first();
                    if(isset($device_tokens) && !empty($device_tokens)){
                        CommonHelper::send_push($device_tokens['type'],$device_tokens['token'],'Cancel your trip','Shipper reject you for booking #'.$booking_id.'.','cancel_driver',[],$device_tokens['user_id']);
                    }
                    $update_booking_truck = BookingTruck::where(['booking_id'=>$booking_id])->update(['driver_id' => 0]);
                    BookingLocations::where(['booking_id' => $booking_id])->update(['arrived_at' => '','start_loading' => '','start_journey' => '']);

                    return \Response::json(['success' => true, 'message' => 'Driver cancelled successfully'], 200);
                }
                else{
                    return \Response::json(['error' => true, 'message' => 'Driver not cancelled'], 200);
                }
            }
        }
    }
    /*public function share_email(Request $request)
    {
        $booking=Booking::where('id',$request->id)->first();
        if($booking->email_or_number==NULL){
            $booking->email_or_number=$request->email;
            $booking->save();
        }else{
            $booking->email_or_number=$booking->email_or_number.",".$request->email;
            $booking->save();
        }
        
        return redirect()->back()->with('success',__('Email has been shared successfully!'));
    }*/
    public function share_email(Request $request)
    {
        $data=$request->all();
        $booking=Booking::where('id',$request->id)->first();
        if($booking->email_or_number=='')
        {
            $booking->email_or_number=$request->email;
            $booking->save();
            $data['email_or_number']=$request->email;
            $data['booking_id']=$request->id;
            dispatch(new \App\Jobs\ShipperSharEmailJobs($data));
        }
        else
        {
            $booking->email_or_number=$booking->email_or_number.",".$request->email;
            $booking->save();
            $data['email_or_number']=$request->email;
            $data['booking_id']=$request->id;
            dispatch(new \App\Jobs\ShipperSharEmailJobs($data));
        }
        
        return redirect()->back()->with('success',__('Email has been shared successfully!'));
    }
    public function share_link(Request $request,$id)
    {
        
        
        $booking=Booking::where('id',$id)->first();
        $booking_status=$booking->status;
        
        if($booking_status=='completed'){
            return view('shipper.pages.manage_shipment.expiry');
        }else{
        $booking_id = $id;
        if(!empty($booking_id) && isset($booking_id)){
            $check_is_exists = BookingLocations::where('booking_id',$booking_id)->exists();
            if($check_is_exists){
                $booking_location = BookingLocations::where('booking_id',$booking_id)->get();
                $location_array = array();
                if(isset($booking_location) && !empty($booking_location)){
                    for($i=0;$i<count($booking_location);$i++){
                        array_push($location_array,[$booking_location[$i]['drop_location'],$booking_location[$i]['drop_lat'],$booking_location[$i]['drop_lng'],$i+1]);  
                    } 
                }
                  
                return view('shipper.pages.manage_shipment.createmap',compact('location_array'));            
            }
        }    
        }
        
    }
    public function generatePDF($id)
    {
        
        $booking= Booking::where('id',$id)->first();
          // dd($booking);die();
        $pdf = PDF::loadView('myPDF', compact('booking'));

    
        return $pdf->download('myvagon.pdf');
    }

    public function generateImage($id)
    {
        
        $booking= Booking::where('id',$id)->first();
        $filePath = env('S3_IMAGE_URL').'POD'.'/'.$booking->pod_image;
        $headers = [
            'Content-Type'        => 'application/jpeg',
            'Content-Disposition' => 'attachment; filename="'. $booking->pod_image .'"',
        ];
        return \Response::make(Storage::disk('s3')->get('POD'.'/'.$booking->pod_image), 200, $headers);  
    }

}
