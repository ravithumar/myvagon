<?php

namespace App\Http\Controllers\Shipper;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Country;
use App\Models\Facility;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Twilio\Http\Response;
use Twilio\Tests\Request as TestsRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Helpers;
use PermissionHelper;

class AddressController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user =Auth::user();
            if($user->type == 'manager'){
                $permission = PermissionHelper::check_access('view_address');
                if(empty($permission)){
                    return redirect()->back()->with('error',__("You can't access this module!"));
                }
            }
            return $next($request);
        });
    }

     public function index(Request $request, $id = null) {
        //  echo '<pre>';print_r(date('Y-m-d H:i:s'));die();
        $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
        $state_pluck = State::whereIn('country_id', $country_pluck)->pluck('id');

        $countries =  Country::whereIn('name', ['Greece'])->get();
        // $states = State::whereIn('country_id', $country_pluck)->get();
        // $cities = City::select('*')->orderBy('id','desc')->whereIn('state_id', $state_pluck)->get();
       
        if ($request->ajax()) {
            $user =   Auth::user();
            if($user['type'] == 'manager'){
                $data = Facility::whereIn('user_id', [$user['id'],$user['company_ref_id']])->select('*')->orderBy('id','desc');
            }else{
                $data = Facility::where('user_id', $user->id)->select('*')->orderBy('id','desc');
            }

           
            return DataTables::of($data)
                ->addIndexColumn()
                ->editColumn('action', function ($row) {
                     
                    $id = "'" . $row['id'] . "'";
                    $name = "'" . $row['name'] . "'";
                    $address = "'" . $row['address'] . "'";
                    $lat = "'" . $row['address_lat'] . "'";
                    $lng = "'" . $row['address_lng'] . "'";
                    $postcode = "'" . $row['zipcode'] . "'";
                    $phone = "'" . $row['phone'] . "'";
                    $email = "'" . $row['email'] . "'";
                    $city = '';
                    $state = '';
                    // $city = "'" . $row->city_obj->name . "'";
                    // $state = "'" . $row->state_obj->name . "'";
                    // $country = "'" . $row->country_obj->name . "'";
                    $serv1 = "'" . $row['restroom_service'] . "'";
                    $serv2 = "'" . $row['food_service'] . "'";
                    $serv3 = "'" . $row['rest_area_service'] . "'";
                    $updateLink = route('shipper.address.update', $row['id']);
                    $city_id = "'" . $row['city'] . "'";
                    $state_id = "'" . $row['state'] . "'";
                    $country_id = "'" . $row['country'] . "'";
                    $option = '';

                    $updateLink = "'" . $updateLink . "'";

                    // $city_name = City::first('name');
                   
                  

                    //  $btn = '
                    // &nbsp;&nbsp;<a href="'.route('shipper.address.show',$row['id']).'" style="  float: left; background-color: white;color: black;" class="btn btn-white btn-xs text-default add-product justify-content-end "> <i class="fa fa-eye"></i></a>
                    // &nbsp;&nbsp;&nbsp;<a href="'.route('shipper.address.edit',$row['id']).'" style="  float: left; background-color: white;color: black;margin-left: 16px;" class="btn btn-white btn-xs text-default add-product justify-content-end "> <i class="fa fa-pencil-square-o"></i></a>
                    // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delete_address(this);return false;" data-newurl="address/delete" data-url="/delete" data-token="'.csrf_token().'" style="  float: left; background-color: white;color: black;margin-left: 16px;" data-id="'.$row['id'].'" class="btn btn-white btn-xs text-default add-product justify-content-end "> <i class="fa fa-trash"></i></a>
                    // ';
                    $btn = '<a href="'.route('shipper.address.show',$row['id']).'" style="color: #9B51E0;text-decoration:underline;font-weight: bold;" class="font-14"> View Details</a>';
                    return $btn;

                })
                ->addColumn('mergeColumn', function($row){
                    return '(+30) '.$row->phone.'';
                })

                ->rawColumns(['action','mergeColumn' ])
                ->make(true);
        } else {

           
            $columns = [
                            ['data' => 'id', 'name' => "Id",'title' => __("S.No."),'width' => '2%'], 
                            ['data' => 'name', 'name' => __("Name"),'title' => __("Name"),'width' => '8%'],
                            ['data' => 'address', 'name' => __("Address"),'title' => __("Address"),'width' => '60%'],
                            ['data' => 'mergeColumn', 'name' => __("phone"),'title' => __("Phone No"),'width' => '15%'],
                            ['data' => 'city', 'name' => __("City"),'title' => __("City"),'width' => '5%'],
                            ['data' => 'action', 'name' =>__("Action"),'title'=>__("Action"), 'searchable' => false, 'orderable' => false,'width' => '10%' ]
                       ];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('shipper.address.index');
            $params['dateTableTitle'] = "Address";
            $params['addUrl'] = "address";
            $params['dataTableId'] = time();
            // $params['cities'] = $cities;
            // $params['states'] = $states;
            $params['countries'] = $countries;
            return view('shipper.pages.address.index', $params);
        }
    }

    public function getCityStateCountry($cityid, $stateid){
        $city = City::where('id', $cityid)->value('name');
        $state = State::where('id', $stateid)->value('name');
        dd($cityid, $stateid);
        $data = array($city, $state);
        return response($data , 200);
    }

    public function create()
    {
        // echo 'Test';die();
        $country_pluck = Country::whereIn('name', ['Greece'])->pluck('id');
        $countries =  Country::whereIn('name', ['Greece'])->get();
       
        return view('shipper.pages.address.create',compact('countries','country_pluck'));
    }

    public function store(Request $request) {
       
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'phone' => 'required',
            // 'city' => 'required',
            'email' => 'required|email',
            
        ],[
            'name.required' => 'please enter name',
            'address.required' => 'please enter address',
            'lat.required' => 'please select address using google suggestion',
            'lng.required' => 'please select address using google suggestion',
            'phone.required' => 'please enter phone',
            'email.required' => 'please enter email',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }
        $user = Auth::user();
        $data = $request->all();
        $address = new Facility();
        if($user['type'] == 'manager'){
            $address->user_id = $user->company_ref_id;
            $address->insert_by = $user->id;
        }else{
            $address->user_id = $user->id;
            $address->insert_by = $user->id;
        }
        $address->name = $data['name'];
        $address->address = $data['address'];
        $address->city = '';
        if(isset($data['city']) && !empty($data['city'])){
            $address->city = $data['city'];
        }
        $address->address_lat = $data['lat'];
        $address->address_lng = $data['lng'];
        // $address->city = $data['city'];
        // $address->state = $data['state'];
        $address->country = '85';
        $address->zipcode = $data['post_code'];
        $address->phone = $data['phone'];
        $address->email = $data['email'];
       
        if($request->get('restroom_service')){
            $address->restroom_service = "1";
        }else{
            $address->restroom_service = "0";
        }
        if($request->get('food_service')){
            $address->food_service = "1";
        }else{
             $address->food_service = "0";
        }
        if($request->get('rest_area_service')){
            $address->rest_area_service = "1";
        }else{
             $address->rest_area_service = "0";
        }
       
        
        $address->save();
       
        return redirect()->route('shipper.address.index')->with('success',__('Address added Successfully'));
    }

    public function edit($id)
    {
        
        $countries =  Country::whereIn('name', ['Greece'])->get();

        $user = Auth::user();
        $address = Facility::where(['id' => $id])->first();
        if(!empty($address)){
            return view('shipper.pages.address.edit',compact('address','countries'));
        }else{
            return redirect()->route('shipper.address.index')->with('error',__('Something goes to wrong, please try again'));
        }
    }

    public function update(Request $request, $id){
        $address = Facility::where('id', $id)->first();
        if(!empty($address)){
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'address' => 'required',
                'lat' => 'required',
                'lng' => 'required',
                // 'city' => 'required',
                'phone' => 'required',
                'email' => 'required|email',
                
            ],[
                'name.required' => 'please enter name',
                'address.required' => 'please enter address',
                'lat.required' => 'please select address using google suggestion',
                'lng.required' => 'please select address using google suggestion',
                'phone.required' => 'please enter phone',
                'email.required' => 'please enter email',
                'city.required' => 'please enter city',
            ]);
            
            if ($validator->fails()) {
                return redirect()->back()
                            ->withErrors($validator->errors())
                            ->withInput();
            }
          
            $data = $request->all();

            $address->name = $data['name'];
            $address->address = $data['address'];
            $address->address_lat = $data['lat'];
            $address->address_lng = $data['lng'];
            $address->city = '';
            if(isset($data['city']) && !empty($data['city'])){
                $address->city = $data['city'];
            }
            // $address->state = $data['state'];
            $address->country = '85';
            $address->zipcode = $data['post_code'];
            $address->phone = $data['phone'];
            $address->email = $data['email'];
           if(isset($data['restroom_service']) && !empty($data['restroom_service']) && $data['restroom_service'] == '1'){
                $address->restroom_service = "1";
            }else{
                $address->restroom_service = "0";
            }
            if(isset($data['food_service']) && !empty($data['food_service']) && $data['food_service'] == '1'){
                $address->food_service = "1";
            }else{
                $address->food_service = "0";
            }
            if(isset($data['rest_area_service']) && !empty($data['rest_area_service']) && $data['rest_area_service'] == '1'){
                $address->rest_area_service = "1";
            }else{
                $address->rest_area_service = "0";
            }
            $address->update();
           
            return redirect()->route('shipper.address.index')->with('success',__('Address data update Successfully'));
        }else{
            return redirect()->route('shipper.address.index')->with('error',__('Something goes wrong, please try again'));
        }
        
    }

    public function show($id)
    {
         $countries =  Country::whereIn('name', ['Greece'])->get();
        $user = Auth::user();
        $address = Facility::where(['id' => $id])->first();
        if(!empty($address)){
            return view('shipper.pages.address.show',compact('address','countries'));
        }else{
            return redirect()->route('shipper.address.index')->with('error',__('Something goes to wrong, please try again'));
        }
    }


    public function destroy(Request $request, $id = null) {
      
        $facility = Facility::where('id',$id)->first();
        $facility->delete();
        return \Response::json('success', 200);
       
    }

    public function get_state(Request $request)
    {
        $country_id = $request->post('country_id');
        if(isset($country_id) && !empty($country_id)){
            $states = State::where('country_id', $country_id)->orderBy('name','ASC')->get()->toArray();
            $response['status'] = true;
            $response['message'] = 'State list';
            $response['data'] = $states;
        }else{
            $response['status'] = false;
            $response['message'] = 'Please select country';
        }
        return response()->json($response,200);
    }
    
    public function get_city(Request $request)
    {
        $state_id = $request->post('state_id');
        if(isset($state_id) && !empty($state_id)){
            $cities = City::where('state_id', $state_id)->orderBy('name','ASC')->get()->toArray();
            $response['status'] = true;
            $response['message'] = 'State list';
            $response['data'] = $cities;
        }else{
            $response['status'] = false;
            $response['message'] = 'Please select country';
        }
        return response()->json($response,200);
    }
}
