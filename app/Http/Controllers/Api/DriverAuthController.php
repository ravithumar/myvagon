<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\Notification;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Helpers;
use App\Helpers\CommonHelper as HelpersCommonHelper;
use App\Models\DriverVehicle;
use App\Models\DriverPermission;
use App\Models\UserSettings;
use App\Models\BankDetails;
use App\Models\TruckType;
use App\Models\Device;
use App\Models\Booking;
use App\Models\BookingTruck;
use App\Models\BookingBid;
use App\Models\BookingRequest;
use App\Models\VehicleCapacity;
use App\Models\Message;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\LoginDriverResource;
use App\Http\Resources\ChatUserResource;
use CommonHelper;
use Twilio\Rest\Api\V2010\Account\Call\NotificationList;
use Twilio\Tests\Request as TestsRequest;

class DriverAuthController extends Controller
{
    use ApiResponser;

    public function register(Request $request){
        // echo '<pre>';print_r($request->all());die();
        $validator = Validator::make($request->all(), [
            'device_token' => 'required',
            'device_type' => 'required',
            'device_name' => 'required',
            'app_version'  => 'required',
            'fullname' =>  'required',
            'country_code' => 'required',
            'mobile_number' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'min:8',
            'truck_type' =>  'required',
            'truck_sub_category' =>  'required',
            // 'truck_features' =>  'required',
            'truck_weight' =>  'required',
            'weight_unit' =>  'required',
            'truck_capacity' =>  'required',
            'capacity_unit' =>  'required',
            'brand' =>  'required',
            // 'pallets' =>  'required',
            'load_capacity' => 'sometimes|nullable',
            'hydraulic_door' => 'sometimes|nullable',
            'cooling' => 'sometimes|nullable',
            'fuel_type' => 'required',
            'plate_number_truck' =>  'required',
           // 'vehicle_images' =>  'required',
            'id_proof' =>  'required',
            'license' =>  'required',
        ],
        [
         'device_token.required' => __('The device token field is required.'),
         'device_type.required' => __('The device type field is required.'),
         'device_name.required' => __('The device name field is required.'),
         'app_version.required' => __('The app version field is required.'),
         'fullname.required' => __('The fullname field is required.'),
         'country_code.required' => __('The country code field is required.'),
         'mobile_number.required' => __('The mobile number field is required.'),
         'email.required' => __('The email field is required.'),
         'password.required' => __('The password field is required.'),
         'truck_type.required' => __('The truck type field is required.'),
         'truck_sub_category.required' => __('The truck sub category field is required.'),
         'truck_weight.required' => __('The truck weight field is required.'),
         'weight_unit.required' => __('The weight unit field is required.'),
         'truck_capacity.required' => __('The truck capacity field is required.'),
         'capacity_unit.required' => __('The capacity unit field is required.'),
         'brand.required' => __('The brand field is required.'),
         'fuel_type.required.' => __('The fuel type field is required.'),
         'plate_number_truck.required' => __('The plate number truck field is required.'),
         'id_proof.required' => __('The id proof field is required.'),
         'license.required' => __('The license field is required.'),
         
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $data = $request->all();
        $phone = $data['mobile_number'];
       
        // DB::beginTransaction();
        // try {

            #USER
            $user = new User();
            $user->phone = $data['mobile_number'];
            $user->country_code = $data['country_code'];
            $user->name = $data['fullname'] ? $data['fullname'] : '';
            $user->email = $data['email'];
            $user->type = 'freelancer';
            $user->password = Hash::make($data['password']);

            if(isset($data['license_number']) && !empty($data['license_number'])){
                $user->licence_number = $data['license_number'];
            }
            if(isset($data['license_expiry_date']) && !empty($data['license_expiry_date'])){
                $user->licence_expiry_date = date('Y-m-d',strtotime($data['license_expiry_date']));
            }
            // $user->password = 1;
            $user->email_verify = 1;
            $user->phone_verify = 1;
            $user->status = 1;
            $user->block_status = '0';
            $user->save();

            $mail_data['email'] = $data['email'];
            $mail_data['id'] = $user->id;
            $mail_data['first_name'] = $data['fullname'] ? $data['fullname'] : '';

            #DEVICE
            $device = Device::insert([
                'user_id' => $user->id,
                'name' => $data['device_name'],
                'token' => $data['device_token'],
                'type' => $data['device_type'],
                'app_type' => $data['app_version'],
                'updated_at' => Carbon::now(),
            ]);

            if(isset($data['load_capacity'])){

                $load_capacity = $data['load_capacity'];
            }else{
                $load_capacity = null;
            }


            if(isset($data['vehicle_images'])){

                if(isset($data['vehicle_images']) && !empty($data['vehicle_images'])){
                    $vehicle_images = explode(',',$data['vehicle_images']);
                    foreach($vehicle_images as $image){
                        if(!empty($image)){
                            CommonHelper::moveImagesS3Bucket($image,'driver/');        
                        }
                    }
                }
                $v_image = $data['vehicle_images'];
            }else{
                $v_image = null;
            }

            if($data['id_proof'] != "")
            {
                CommonHelper::moveImagesS3Bucket($data['id_proof'],'driver/');
            }
            if($data['license'] != "")
            {
                CommonHelper::moveImagesS3Bucket($data['license'],'driver/');
            }

            $plate_number_trailer = "";
            if(isset($data['plate_number_trailer']) && !empty($data['plate_number_trailer'])){
                $plate_number_trailer = $data['plate_number_trailer'];
            }

            $truck_features = "";
            if(isset($data['truck_features']) && !empty($data['truck_features'])){
                $truck_features = $data['truck_features'];
            }
            #DRIVER_VEHICLE
            $vehicle = DriverVehicle::insertGetId([
                'user_id' => $user->id,
                'truck_type' => $data['truck_type'],
                'truck_sub_category' => $data['truck_sub_category'],
                'weight' => $data['truck_weight'],
                'weight_unit' => $data['weight_unit'],
                'load_capacity' => $data['truck_capacity'],
                // 'truck_capacity_unit' => $data['capacity_unit'],
                // 'load_capacity' => $load_capacity,
                'load_capacity_unit' => $data['capacity_unit'],
                'brand' => $data['brand'],
                // 'capacity_pallets' => $data['pallets'],
                // 'pallets' => $data['pallets'],
                'fuel_type' => $data['fuel_type'],
                'truck_features' => $truck_features, 
                'registration_no' => $data['plate_number_truck'],
                'trailer_registration_no' => $plate_number_trailer,
                'images' => $v_image,
                'id_proof' => $data['id_proof'],
                'license' => $data['license']
            ]);

            #VEHICLE_CAPACITY
            if(isset($data['pallets']) && !empty($data['pallets'])){
                $package_data = json_decode($data['pallets']);
                foreach($package_data as $capacity){
                    $vehicle_capacity = VehicleCapacity::insert([
                        'driver_vehicle_id' => $vehicle,
                        'package_type_id' => $capacity->id,
                        'value' => $capacity->value,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                } 
            }
           
            #ROLE_CHECK
            $role = Role::where('name','carrier')->first();
                

            #USER_ROLE
            $user_role = UserRole::insert([
                'user_id' => $user->id,
                'role_id' => $role['id'],
            ]);

            #SETTING
            $setting = new UserSettings;
            $setting->user_id = $user->id;
            $setting->notification = 1;
            $setting->message = 1;
            $setting->bid_received = 1;
            $setting->bid_accepted = 1;
            $setting->load_assign_by_dispatcher = 1;
            $setting->start_trip_reminder = 1;
            $setting->complete_trip_reminder = 1;
            $setting->pdo_remider = 1;
            $setting->match_shippment_near_you = 1;
            $setting->match_shippment_near_delivery = 1;
            $setting->rate_shipper = 1;
            $setting->save();


            $permissions = [
                'user_id' => $user->id,
                'search_loads' =>1,
                'my_loads' =>1,
                'my_profile' =>1,
                'setting' =>1,
                'statistics' =>1,
                'change_password' =>1,
                'allow_bid' =>1,
                'view_price' =>1,
                'post_availibility' =>1,
                // 'created_at' =>date('Y-m-d H:i:s')
            ];
            DriverPermission::insert($permissions);
            $accessToken = $user->createToken('authToken')->accessToken;
            $user->token = $accessToken;
         
            $res_data = new LoginDriverResource($user);
            $data = $res_data->toArray($user);
            $data['token'] = $accessToken;
            DB::commit();

            dispatch(new \App\Jobs\DriverVerificationJob($mail_data));

            return $this->successResponse($data, __('Register successfully'), true);
      
        // } catch (Exception $e) {
        //     DB::rollback();
        //     return $this->errorResponse($e->getMessage(), true);

        // }

    }

    public function register_new(Request $request){
        // echo '<pre>';print_r($request->all());die();
        $validator = Validator::make($request->all(), [
            'device_token' => 'required',
            'device_type' => 'required',
            'device_name' => 'required',
            'app_version'  => 'required',
            'fullname' =>  'required',
            'country_code' => 'required',
            'mobile_number' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'min:8',
            'license_number' =>  'required',
            'license_expiry_date' =>  'required',
            'license' =>  'required',
            'eu_registration_document' =>  'required',
            'tractor_brand' =>  'required',
            'tractor_plate_number' =>  'required',
            'truck_details' =>  'required',
            'payment_type' =>  'required',

            // 'truck_type' =>  'required',
            // 'truck_sub_category' =>  'required',
            // 'truck_features' =>  'required',
            // 'truck_weight' =>  'required',
            // 'weight_unit' =>  'required',
            // 'truck_capacity' =>  'required',
            // 'capacity_unit' =>  'required',
            // 'brand' =>  'required',
            // 'pallets' =>  'required',
            // 'load_capacity' => 'sometimes|nullable',
            // 'hydraulic_door' => 'sometimes|nullable',
            // 'cooling' => 'sometimes|nullable',
            // 'fuel_type' => 'required',
            // 'plate_number_truck' =>  'required',
           // 'vehicle_images' =>  'required',
            // 'id_proof' =>  'required',
            
        ],
        [
         'device_token.required' => __('The device token field is required.'),
         'device_type.required' => __('The device type field is required.'),
         'device_name.required' => __('The device name field is required.'),
         'app_version.required' => __('The app version field is required.'),
         'fullname.required' => __('The fullname field is required.'),
         'country_code.required' => __('The country code field is required.'),
         'mobile_number.required' => __('The mobile number field is required.'),
         'email.required' => __('The email field is required.'),
         'password.required' => __('The password field is required.'),
         'license_number.required' => __('The license number field is required'),
         'license_expiry_date.required' => __('The license expiry date field is required'),
         'license.required' => __('The license field is required.'),
         'eu_registration_document.required' => __('The EU regisrtation document field is required.'),
         'tractor_brand.required' => __('The tractor brand field is required.'),
         'tractor_plate_number.required' => __('The tractor plate number field is required.'),
         'truck_details.required' => __('The tractor details fields are required.'),
         'payment_type.required' => __('The payment type field is required.'),

        //  'truck_type.required' => __('The truck type field is required.'),
        //  'truck_sub_category.required' => __('The truck sub category field is required.'),
        //  'truck_weight.required' => __('The truck weight field is required.'),
        //  'weight_unit.required' => __('The weight unit field is required.'),
        //  'truck_capacity.required' => __('The truck capacity field is required.'),
        //  'capacity_unit.required' => __('The capacity unit field is required.'),
        //  'brand.required' => __('The brand field is required.'),
        //  'fuel_type.required.' => __('The fuel type field is required.'),
        //  'plate_number_truck.required' => __('The plate number truck field is required.'),
        //  'id_proof.required' => __('The id proof field is required.'),
         
         
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        // echo '<pre>';print_r($truck_details);die();
        #USER
        $user = new User();
        $user->phone = $data['mobile_number'];
        $user->country_code = $data['country_code'];
        $user->name = $data['fullname'] ? $data['fullname'] : '';
        $user->email = $data['email'];
        $user->type = 'freelancer';
        $user->password = Hash::make($data['password']);

        if(isset($data['license_number']) && !empty($data['license_number'])){
            $user->licence_number = $data['license_number'];
        }
        if(isset($data['license_expiry_date']) && !empty($data['license_expiry_date'])){
            $user->licence_expiry_date = date('Y-m-d',strtotime($data['license_expiry_date']));
        }
        // $user->password = 1;
        $user->email_verify = 1;
        $user->phone_verify = 1;
        $user->status = 1;
        $user->block_status = '0';
        $user->payment_type = $data['payment_type'];
        $user->save();

        $mail_data['email'] = $data['email'];
        $mail_data['id'] = $user->id;
        $mail_data['first_name'] = $data['fullname'] ? $data['fullname'] : '';

        #DEVICE
        $device = Device::insert([
            'user_id' => $user->id,
            'name' => $data['device_name'],
            'token' => $data['device_token'],
            'type' => $data['device_type'],
            'app_type' => $data['app_version'],
            'updated_at' => Carbon::now(),
        ]);

        // if(isset($data['load_capacity'])){
        //     $load_capacity = $data['load_capacity'];
        // }else{
        //     $load_capacity = null;
        // }


        if(isset($data['tractor_images'])){

            if(isset($data['tractor_images']) && !empty($data['tractor_images'])){
                $vehicle_images = explode(',',$data['tractor_images']);
                foreach($vehicle_images as $image){
                    if(!empty($image)){
                        CommonHelper::moveImagesS3Bucket($image,'driver/');        
                    }
                }
            }
            $v_image = $data['tractor_images'];
        }else{
            $v_image = null;
        }

        if($data['eu_registration_document'] != "")
        {
            CommonHelper::moveImagesS3Bucket($data['eu_registration_document'],'driver/');
        }
        if($data['license'] != "")
        {
            CommonHelper::moveImagesS3Bucket($data['license'],'driver/');
        }

        // $plate_number_trailer = "";
        // if(isset($data['plate_number_trailer']) && !empty($data['plate_number_trailer'])){
        //     $plate_number_trailer = $data['plate_number_trailer'];
        // }

        $truck_features = "";
        if(isset($data['truck_features']) && !empty($data['truck_features'])){
            $truck_features = $data['truck_features'];
        }
        $truck_details = [];
        if(isset($data['truck_details']) && !empty($data['truck_details'])){
            $truck_details = json_decode($data['truck_details']);
        }
        if(isset($truck_details) && !empty($truck_details)){
            foreach($truck_details as $key => $value){
                if($key == 0){
                    #DRIVER_VEHICLE
                    $vehicle = DriverVehicle::insertGetId([
                        'user_id' => $user->id,
                        'truck_type' => $value['truck_type'],
                        'truck_sub_category' => $value['truck_sub_category'],
                        'weight' => $value['weight'],
                        'weight_unit' => $value['weight_unit'],
                        'load_capacity' => $value['capacity'],
                        // 'truck_capacity_unit' => $data['capacity_unit'],
                        // 'load_capacity' => $load_capacity,
                        'load_capacity_unit' => $value['capacity_unit'],
                        'brand' => $data['tractor_brand'],
                        // 'capacity_pallets' => $data['pallets'],
                        // 'pallets' => $data['pallets'],
                        'fuel_type' => $data['tractor_fual_type'],
                        'truck_features' => $truck_features, 
                        'registration_no' => $data['tractor_plate_number'],
                        'trailer_registration_no' => $value['plate_number'],
                        'images' => $v_image,
                        'id_proof' => $data['eu_registration_document'],
                        'license' => $data['license']
                    ]);                    
                }
                if(isset($value['images'])){

                    if(isset($value['images']) && !empty($value['images'])){
                        $images = explode(',',$value['images']);
                        foreach($images as $image){
                            if(!empty($image)){
                                CommonHelper::moveImagesS3Bucket($image,'driver/');        
                            }
                        }
                    }
                    $vehicle_images = $value['images'];
                }else{
                    $vehicle_images = null;
                }

                $truck_detail_id = DB::table('driver_truck_details')->insertGetId([
                    'truck_type' => $value['truck_type'],
                    'truck_sub_category' => $value['truck_sub_category'],
                    'weight' => $value['weight'],
                    'weight_unit' => $value['weight_unit'],
                    'load_capacity' => $value['capacity'],
                    'load_capacity_unit' => $value['capacity_unit'],
                    'plate_number' => $value['plate_number'],
                    'images' => $vehicle_images,
                    'default_truck' => $key == 0 ? 1 : 0,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

                #VEHICLE_CAPACITY
                if(isset($value['pallets']) && !empty($value['pallets'])){
                    $package_data = $value['pallets'];
                    foreach($package_data as $capacity){
                        $vehicle_capacity = VehicleCapacity::insert([
                            'driver_vehicle_id' => $vehicle,
                            'driver_truck_details_id' => $truck_detail_id,
                            'package_type_id' => $capacity->id,
                            'value' => $capacity->value,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]);
                    } 
                }
            }
        }
       
        #ROLE_CHECK
        $role = Role::where('name','carrier')->first();
            

        #USER_ROLE
        $user_role = UserRole::insert([
            'user_id' => $user->id,
            'role_id' => $role['id'],
        ]);

        #SETTING
        $setting = new UserSettings;
        $setting->user_id = $user->id;
        $setting->notification = 1;
        $setting->message = 1;
        $setting->bid_received = 1;
        $setting->bid_accepted = 1;
        $setting->load_assign_by_dispatcher = 1;
        $setting->start_trip_reminder = 1;
        $setting->complete_trip_reminder = 1;
        $setting->pdo_remider = 1;
        $setting->match_shippment_near_you = 1;
        $setting->match_shippment_near_delivery = 1;
        $setting->rate_shipper = 1;
        $setting->save();

        if(isset($data['payment_type']) && !empty($data['payment_type']) && $data['payment_type'] == 1){
            BankDetails::create([
                'user_id' => $user->id,
                'iban' => $data['iban'],
                'account_number' => $data['account_number'],
                'bank_name' => $data['bank_name'],
                'country' => $data['country'],
            ]);
        }


        $permissions = [
            'user_id' => $user->id,
            'search_loads' =>1,
            'my_loads' =>1,
            'my_profile' =>1,
            'setting' =>1,
            'statistics' =>1,
            'change_password' =>1,
            'allow_bid' =>1,
            'view_price' =>1,
            'post_availibility' =>1,
            // 'created_at' =>date('Y-m-d H:i:s')
        ];
        DriverPermission::insert($permissions);
        $accessToken = $user->createToken('authToken')->accessToken;
        $user->token = $accessToken;
     
        $res_data = new LoginDriverResource($user);
        $data = $res_data->toArray($user);
        $data['token'] = $accessToken;
        DB::commit();

        dispatch(new \App\Jobs\DriverVerificationJob($mail_data));

        return $this->successResponse($data, __('Register successfully'), true);
    }

     public function emailVerify(Request $request){
        $validator = Validator::make(request()->all(), [
            'email' => 'required',
        ],['email.required' => __('The email field is required.'), ]);
        if (!$validator->fails()) {

            $email = $request->input('email');

            $check = User::where('email', $email)->first();
         	
            if(isset($check)){
            	return $this->errorResponse(__('This email already exists. Please sign in using this email or sign up with a new one.'), true);
            }

            dispatch(new \App\Jobs\DriverEmailOTPJob($email));
            $otp = 456789;
            $data['OTP'] = $otp;
            return $this->successResponse($data,__('Please type the verification code sent via Email'),  true);
        }
        	
       	
        return $this->errorResponse($validator->errors()->first(), true);

    }

    // public function emailTest(Request $request){
    //     $validator = Validator::make(request()->all(), [
    //         'email' => 'required',
    //     ]);
    //     if (!$validator->fails()) {

    //         $data['email'] = $request->input('email');
    //         $data['first_name'] = 'KD';
    //         $data['id'] = '1';

    //         // $check = User::where('email', $email)->first();
         	
    //         // if(isset($check)){
    //         // 	return $this->errorResponse(__('This email already exists. Please sign in using this email or sign up with a new one.'), true);
    //         // }

    //         // dispatch(new \App\Jobs\DriverAcceptProfileJob($data));
    //         dispatch(new \App\Jobs\ShipperVerificationJob($data, $data['id']));
    //         $otp = 456789;
    //         $data['OTP'] = $otp;
    //         return $this->successResponse($data,__('Please type the verification code sent via Email'),  true);
    //     }
        	
       	
    //     return $this->errorResponse($validator->errors()->first(), true);

    // }
    
    
    public function phoneVerify(Request $request){

        $validator = Validator::make(request()->all(), [
            'mobile_number' => 'required',
        ],['mobile_number.required' => __('The mobile number field is required.'), ]);
        if (!$validator->fails()) {

            $phone = $request->input('mobile_number');

            $check = User::where('phone', $phone)->first();
         
            if(isset($check)){
                return $this->errorResponse( __('This phone number is already exist ! Please use another phone number'),  true);
            }

            $otp = 456789;
            $data['OTP'] = $otp;
            return $this->successResponse($data, __('Please type the verification code sent via SMS'));
        }
        return $this->errorResponse($validator->errors()->first(), true);
        
    }
    

    public function imageUpload(Request $request){
            // print_r($_FILES[''])
            if($request->hasfile('images')){
                $images = $request->file('images');
                foreach($images as $image){
                    $single[] =  Helpers\CommonHelper::multipleImageUpload($image,  'temp/');
                }
                $data['images'] = $single;
                return $this->successResponse($data, __('Image Upload successfully'), true);
            }
            else{
                return $this->errorResponse(__('Something want wrong!'), true);
            }
    }


    

    public function login(Request $request) {
        $validator = Validator::make(request()->all(), [
            'device_token' => 'required',
            'device_type' => 'required',
            'device_name' => 'required',
            'app_version'  => 'required',
            'email' => 'email|required',
            'password' => 'required',
        ],[
            'device_token.required' => __('The device token field is required.'),
            'device_type.required' => __('The device type field is required.'), 
            'device_name.required' => __('The device name field is required.'),
            'app_version.required' => __('The app version field is required.'),
            'email.required' => __('The email field is required.'),
            'password.required' => __('The password field is required.'),
        ]);
        if (!$validator->fails()) {

            $stora = $request->all();
            $email = $stora['email'];
            $password = $stora['password'];

            if (auth()->attempt(['email' => $email, 'password' => $password])) {
                $user = auth()->user();
                $user_data = User::find($user->id);

                if ($user->hasRole($user->id,3)) {

                    if($user->block_status == 1){

                        if($user->status == 1){
                            $chk = Device::where('user_id', $user->id)->first();
                            // echo '<pre>';print_r($chk);die();
                            // $vehicle = DriverVehicle::where('user_id', $user->id)->with('brands:id,name')->first();

                            if(isset($chk)){
                                if($chk->token != $stora['device_token']){
                                    CommonHelper::send_push($chk->type,$chk->token,'Session Expire','Session Expire','sessionTimeout',[]);
                                }
                                Device::where('user_id', $user->id)
                                    ->update([
                                    'name' => $stora['device_name'],
                                    'token' => $stora['device_token'],
                                    'type' => $stora['device_type'],
                                    'app_type' => $stora['app_version'],
                                    'updated_at' => Carbon::now(),
                                    ]);
                            }else{
                            Device::insert([
                                    'user_id' => $user->id,
                                    'name' => $stora['device_name'],
                                    'token' => $stora['device_token'],
                                    'type' => $stora['device_type'],
                                    'app_type' => $stora['app_version'],
                                    'updated_at' => Carbon::now(),
                                ]);
                            }

                            $resData = new LoginDriverResource($user_data);
                            $data = $resData->toArray($user_data);
                            
                            $data['token'] = auth()->user()->createToken('authToken')->accessToken;
                            return $this->successResponse($data,__('Login successfully'), true);

                            
                        }
                        Auth::logout();
                        return $this->errorResponse(__('Your driverProfile is Inactive , please try after some time!'),  true);   
                    }
                    Auth::logout();
                    return $this->errorResponse(__('Your profile is under verification , please try after some time!'),  true);   
                   
                }
                Auth::logout();
                return $this->errorResponse(__('Can not authorized for driver login'),  true);        
            }
            Auth::logout();
            return $this->errorResponse(__('Invalide email or password'),  true);

        }
                    
        return $this->errorResponse($validator->errors()->first() , true);

    }

    public function userGetSetting(Request $request){
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required',
        ],[
            'user_id.required' => __('The user id field is required.'),
            
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $data = $request->all();
        $user_id = $data['user_id'];

        $user = User::where('id', $user_id)->first();

        if(!isset($user)){
            return $this->errorResponse(__('User Not Found'), true);
        }

        $setting = UserSettings::where('user_id', $user->id)->first();

        if(isset($setting)){
            return $this->successResponse($setting,__('Data get successfully'), true);
        }else{
            return $this->errorResponse(__('User Data Not Found'), true);
        }
    }

    public function userSetting(Request $request){

        $validator = \Validator::make($request->all(), [
            'user_id' => 'required',
        ],[
            'user_id.required' => __('The user id field is required.'),
            
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $data = $request->all();
        $user_id = $data['user_id'];

        $user = User::where('id', $user_id)->first();

        if(!isset($user)){
            return $this->errorResponse(__('User Not Found'), true);
        }

        if(!isset($user)){
			return $this->errorResponse(__('User Not Found'), true);
        }
        
        $setting = UserSettings::where('user_id', $user->id)->first();

        if(isset($setting)){
            $setting->fill($data)->save();
            return $this->successResponse($data,__('Data Update successfully'), true);
        }else{
          
            return $this->errorResponse(__('User Data Not Found'), true);
        }

    }

    public function changePassword(Request $request){
        $data = $request->all();
        //$user = \Auth::user();

        $validator = \Validator::make($request->all(), [
        	'user_id' => 'required',
            'old_password' => 'required',
            'new_password' => 'required|min:6',
           // 'confirm_password' => 'required|same:new_password',

        ],[
            'user_id.required' => __('The user id field is required.'),
            'old_password.required' => __('The old password field is required.'),
            'new_password.required' => __('The new password field is required.'),
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $user_id = $data['user_id'];
        $user = User::where('id', $user_id)->first();
       
       
        if ((\Hash::check($request->input('old_password'), $user->password)) == false) {
            return $this->errorResponse(__('Please check your old password'), true);

        } else if ((\Hash::check($request->input('new_password'), $user->password)) == true) {
            return $this->errorResponse( __('Please enter a password which is not similar then current password.'), true);

        } else {
            User::where('id', $user->id)->update(['password' => \Hash::make($data['new_password'])]);
            return $this->successResponse($data,  __('Password updated successfully.'), true);

        }
        
    }

    public function driverProfile(Request $request){
       $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'mobile_number' => 'required',
            'truck_type' => 'required',
            'truck_weight'  => 'required',
            'weight_unit' =>  'required',
            'truck_capacity' => 'required',
            'capacity_unit' => 'required',
            'brand' =>  'required',
            'pallets' =>  'required',
            // 'hydraulic_door' =>  'required',
            'fuel_type' =>  'required',
            'plate_number_truck' =>  'required',
            'license_number' =>  'required',
            'license_expiry_date' =>  'required',
        ],[
            'fullname.required' => __('The fullname field is required.'),
            'mobile_number.required' => __('The mobile number field is required.'),
            'truck_type.required' => __('The truck type field is required.'),
            'truck_weight.required' => __('The truck weight field is required.'),
            'weight_unit.required' => __('The weight unit field is required.'),
            'truck_capacity.required' => __('The truck capacity field is required.'),
            'capacity_unit.required' => __('The capacity unit field is required.'),
            'brand.required' => __('The brand field is required.'),
            'pallets.required' => __('The pallets field is required.'),
            'fuel_type.required' => __('The fuel type field is required.'),
            'license_number.required' => __('The license number field is required.'),
            'plate_number_truck.required' => __('The plate number truck field is required.'),
            'license_expiry_date.required' => __('The license expiry date field is required.'),
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $user = \Auth::user();
        if(!isset($user)){
        	 return $this->errorResponse(__('User Not Found'), true);
        }
        
        $booking_truck = BookingTruck::where('driver_id',$user->id)->pluck('booking_id')->toArray();
        $booking_bid = BookingBid::where('driver_id', $user['id'])->where('status', '0')->get()->toArray();
        $booking_request = BookingRequest::where('driver_id',$user['id'])->where('status','0')->get()->toArray();
        if(isset($booking_bid) && !empty($booking_bid) || isset($booking_request) && !empty($booking_request)){
            return $this->errorResponse(__('User Profile can not Update'), true);
        }
        if(isset($booking_truck) && !empty($booking_truck)) {
            $booking = Booking::whereIn('id',$booking_truck)->whereIn('status',['scheduled','in-progress'])->get()->toArray();
            if(isset($booking) && !empty($booking)){
                return $this->errorResponse(__('User Profile can not Update'), true);
            }
        }

        $vehicle = DriverVehicle::where('user_id', $user->id)->first();
        $data = $request->all();
        if(isset($data['vehicle_images']) && !empty($data['vehicle_images'])){
            if(isset($data['vehicle_images']) && !empty($data['vehicle_images'])){
                $vehicle_images = explode(',',$data['vehicle_images']);
                foreach($vehicle_images as $image){
                    if(!empty($image)){
                        CommonHelper::moveImagesS3Bucket($image,'driver/');        
                    }
                }
            }
            if(!empty($vehicle['images']) && isset($vehicle['images'])){
                // echo '<pre>';print_r($vehicle['images']);die();
                $vehicle_images_old = $vehicle['images'];
                foreach($vehicle_images_old as $image_old){
                    if(!empty($image_old)){
                        CommonHelper::removeImage('driver/'.$image_old);    
                    }
                }
            }
            $v_image = $data['vehicle_images'];
        }
        if(isset($data['id_proof']) && !empty($data['id_proof']))
        {
            CommonHelper::moveImagesS3Bucket($data['id_proof'],'driver/');
            if(!empty($vehicle['id_proof']) && isset($vehicle['id_proof'])){
                CommonHelper::removeImage('driver/'.$vehicle['id_proof']);
            }
        }
        if(isset($data['license']) && !empty($data['license']))
        {
            CommonHelper::moveImagesS3Bucket($data['license'],'driver/');
            if(!empty($vehicle['license']) && isset($vehicle['license'])){
                CommonHelper::removeImage('driver/'.$vehicle['license']);
            }
        }
        if(isset($data['profile_image']) && !empty($data['profile_image']))
        {
            CommonHelper::moveImagesS3Bucket($data['profile_image'],'driver/');
            if(!empty($user['profile']) && isset($user['profile'])){
                CommonHelper::removeImage('driver/'.$user['profile']);
            }
        }

        
        $user->name = $request->input('fullname');
        if(isset($data['profile_image']) && !empty($data['profile_image'])){
            $user->profile = $request->input('profile_image');
        }
        $user->phone = $request->input('mobile_number');
        $user->licence_number = $request->input('license_number');
        $user->licence_expiry_date = date('Y-m-d',strtotime($request->input('license_expiry_date')));
        $user->update();

       
        $vehicle->truck_type = $request->input('truck_type');
        $vehicle->truck_sub_category = $request->input('truck_sub_category');
        $vehicle->weight = $request->input('truck_weight');
        $vehicle->weight_unit = $request->input('weight_unit');
        $vehicle->load_capacity = $request->input('truck_capacity');
        $vehicle->load_capacity_unit = $request->input('capacity_unit');
        $vehicle->brand = $request->input('brand');
        // $vehicle->capacity_pallets = $request->input('pallets');
        // $vehicle->pallets = $request->input('pallets');
        // if(isset($request->input('hydraulic_door')) && !empty($request->input('hydraulic_door'))){
        //     $vehicle->hydraulic_door = $request->input('hydraulic_door');
        // }
        // $vehicle->cooling = $request->input('cooling');
        $vehicle->fuel_type = $request->input('fuel_type');
        $vehicle->registration_no = $request->input('plate_number_truck');
        if(isset($data['plate_number_trailer']) && !empty($data['plate_number_trailer'])){
            $vehicle->trailer_registration_no = $data['plate_number_trailer'];
        }
        if(isset($data['vehicle_images']) && !empty($data['vehicle_images'])){
            $vehicle->images = $request->input('vehicle_images');
        }
        if(isset($data['id_proof']) && !empty($data['id_proof'])){
            $vehicle->id_proof = $request->input('id_proof');
        }
        if(isset($data['license']) && !empty($data['license'])){
            $vehicle->license = $request->input('license');
        }
        $vehicle->update();

        #VEHICLE_CAPACITY
        if(isset($data['pallets']) && !empty($data['pallets'])){
            VehicleCapacity::where('driver_vehicle_id',$vehicle['id'])->delete();
            $package_data = json_decode($data['pallets']);
            foreach($package_data as $capacity){
                $vehicle_capacity = VehicleCapacity::insert([
                    'driver_vehicle_id' => $vehicle['id'],
                    'package_type_id' => $capacity->id,
                    'value' => $capacity->value,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            } 
        }
        $resData = new LoginDriverResource($user);
        $data = $resData->toArray($user);
        // echo '<pre>';print_r($data);die();
        return $this->successResponse($data,__('User Profile Update successfully'), true); 

    }

    public function driverProfileBKP(Request $request){
           // echo '<pre>';print_r($request->all());
        //    echo '<pre>';print_r($_FILES);
        //    die();
           $validator = Validator::make($request->all(), [
                'fullname' => 'required',
                'mobile_number' => 'required',
                'truck_type' => 'required',
                'truck_weight'  => 'required',
                'weight_unit' =>  'required',
                'truck_capacity' => 'required',
                'capacity_unit' => 'required',
                'brand' =>  'required',
                'pallets' =>  'required',
                // 'hydraulic_door' =>  'required',
                'fuel_type' =>  'required',
                'plate_number_truck' =>  'required',
                'license_number' =>  'required',
                'license_expiry_date' =>  'required',
            ],[
            'fullname.required' => __('The fullname field is required.'),
            'mobile_number.required' => __('The mobile number field is required.'),
            'truck_type.required' => __('The truck type field is required.'),
            'truck_weight.required' => __('The truck weight field is required.'),
            'weight_unit.required' => __('The weight unit field is required.'),
            'truck_capacity.required' => __('The truck capacity field is required.'),
            'capacity_unit.required' => __('The capacity unit field is required.'),
            'brand.required' => __('The brand field is required.'),
            'pallets.required' => __('The pallets field is required.'),
            'fuel_type.required' => __('The fuel type field is required.'),
            'license_number.required' => __('The license number field is required.'),
            'plate_number_truck.required' => __('The plate number truck field is required.'),
            'license_expiry_date.required' => __('The license expiry date field is required.'),
        ]);
    
            if ($validator->fails()) {
                return $this->errorResponse($validator->errors()->first(), true);
            }
    
            $user = \Auth::user();
            if(!isset($user)){
                 return $this->errorResponse(__('User Not Found'), true);
            }

            $booking_truck = BookingTruck::where('driver_id',$user->id)->pluck('booking_id')->toArray();
            $booking_bid = BookingBid::where('driver_id', $user['id'])->where('status', '0')->get()->toArray();
            $booking_request = BookingRequest::where('driver_id',$user['id'])->where('status','0')->get()->toArray();
            if(isset($booking_bid) && !empty($booking_bid) || isset($booking_request) && !empty($booking_request)){
                return $this->errorResponse(__('User Profile can not Update'), true);
            }
            if(isset($booking_truck) && !empty($booking_truck)) {
                $booking = Booking::whereIn('id',$booking_truck)->whereIn('status',['scheduled','in-progress'])->get()->toArray();
                if(isset($booking) && !empty($booking)){
                    return $this->errorResponse(__('User Profile can not Update'), true);
                }
            }

                        
            $vehicle = DriverVehicle::where('user_id', $user->id)->first();
            $data = $request->all();
            if(isset($data['vehicle_images']) && !empty($data['vehicle_images'])){
                if(isset($data['vehicle_images']) && !empty($data['vehicle_images'])){
                    $vehicle_images = explode(',',$data['vehicle_images']);
                    foreach($vehicle_images as $image){
                        if(!empty($image)){
                            CommonHelper::moveImagesS3Bucket($image,'driver/');        
                        }
                    }
                }
                if(!empty($vehicle['images']) && isset($vehicle['images'])){
                    // echo '<pre>';print_r($vehicle['images']);die();
                    $vehicle_images_old = $vehicle['images'];
                    foreach($vehicle_images_old as $image_old){
                        if(!empty($image_old)){
                            CommonHelper::removeImage('driver/'.$image_old);    
                        }
                    }
                }
                $v_image = $data['vehicle_images'];
            }
            if(isset($data['id_proof']) && !empty($data['id_proof']))
            {
                CommonHelper::moveImagesS3Bucket($data['id_proof'],'driver/');
                if(!empty($vehicle['id_proof']) && isset($vehicle['id_proof'])){
                    CommonHelper::removeImage('driver/'.$vehicle['id_proof']);
                }
            }
            if(isset($data['license']) && !empty($data['license']))
            {
                CommonHelper::moveImagesS3Bucket($data['license'],'driver/');
                if(!empty($vehicle['license']) && isset($vehicle['license'])){
                    CommonHelper::removeImage('driver/'.$vehicle['license']);
                }
            }
            if(isset($data['profile_image']) && !empty($data['profile_image']))
            {
                CommonHelper::moveImagesS3Bucket($data['profile_image'],'driver/');
                if(!empty($user['profile']) && isset($user['profile'])){
                    CommonHelper::removeImage('driver/'.$user['profile']);
                }
            }
    
            
            $user->name = $request->input('fullname');
            if(isset($data['profile_image']) && !empty($data['profile_image'])){
                $user->profile = $request->input('profile_image');
            }
            $user->phone = $request->input('mobile_number');
            $user->licence_number = $request->input('license_number');
            $user->licence_expiry_date = date('Y-m-d',strtotime($request->input('license_expiry_date')));
            $user->block_status = ($user['block_status'] < 1) ? $user['block_status'] : '0';
            $user->update();
    
           
            $vehicle->truck_type = $request->input('truck_type');
            $vehicle->truck_sub_category = $request->input('truck_sub_category');
            $vehicle->weight = $request->input('truck_weight');
            $vehicle->weight_unit = $request->input('weight_unit');
            $vehicle->load_capacity = $request->input('truck_capacity');
            $vehicle->load_capacity_unit = $request->input('capacity_unit');
            $vehicle->brand = $request->input('brand');
            // $vehicle->capacity_pallets = $request->input('pallets');
            // $vehicle->pallets = $request->input('pallets');
            // if(isset($request->input('hydraulic_door')) && !empty($request->input('hydraulic_door'))){
            //     $vehicle->hydraulic_door = $request->input('hydraulic_door');
            // }
            // $vehicle->cooling = $request->input('cooling');
            $vehicle->fuel_type = $request->input('fuel_type');
            $vehicle->registration_no = $request->input('plate_number_truck');
            if(isset($data['plate_number_trailer']) && !empty($data['plate_number_trailer'])){
                $vehicle->trailer_registration_no = $data['plate_number_trailer'];
            }
            if(isset($data['vehicle_images']) && !empty($data['vehicle_images'])){
                $vehicle->images = $request->input('vehicle_images');
            }
            if(isset($data['id_proof']) && !empty($data['id_proof'])){
                $vehicle->id_proof = $request->input('id_proof');
            }
            if(isset($data['license']) && !empty($data['license'])){
                $vehicle->license = $request->input('license');
            }
            $vehicle->update();
    
            #VEHICLE_CAPACITY
            if(isset($data['pallets']) && !empty($data['pallets'])){
                VehicleCapacity::where('driver_vehicle_id',$vehicle['id'])->delete();
                $package_data = json_decode($data['pallets']);
                foreach($package_data as $capacity){
                    $vehicle_capacity = VehicleCapacity::insert([
                        'driver_vehicle_id' => $vehicle['id'],
                        'package_type_id' => $capacity->id,
                        'value' => $capacity->value,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                } 
            }
            $resData = new LoginDriverResource($user);
            $data = $resData->toArray($user);
            
                return $this->successResponse($data,__('User Profile Update successfully'), true); 
            
    
        }

    public function bid_list()
    {
        $response_data = DriverPermission::where(['id'=>'18'])->first()->toArray();
        echo "<pre>";print_r($response_data);echo "</pre>";die;
        //         $data_array['date'] = date("d F'y",strtotime($bid_list['pickup_date']))
        //         echo '<pre>';print_r($bid_list);die();
        //         $data_array['value'] = 
        //     }
        // }
        return $this->successResponse($bid_lists, __("Data Get Successfully"), true);
    }

    public function edit_driver_permission(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ],['user_id.required' => __('The posted user id field is required.'),
           
           
          ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $data = $request->all();
        $driver = User::where(['id' => $data['user_id'], 'type' => 'driver'])->first();
        if(isset($driver) && !empty($driver)){
            if($driver->hasRole($driver->id,3)){
                $permission = DriverPermission::where('user_id', $data['user_id'])->first();
                if(isset($permission)){
                    $permission->fill($data)->save();
                }
                $response = DriverPermission::where('user_id', $data['user_id'])->first();
                return $this->successResponse($response,__('Driver Permission changed successfully'), true);
            }else{
                return $this->errorResponse(__('User Not Found'),true);
            }
        }else{
            return $this->errorResponse(__('User Not Found'),true);
        }
        
    }

    public function manage_driver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ],['user_id.required' => __('The posted user id field is required.'),
           
           
          ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $data = $request->all();
       
        $driver = User::where(['company_ref_id' => $data['user_id'], 'type' => 'driver'])->get()->toArray();
        $response = [];
        if(isset($driver) && !empty($driver)){
            foreach($driver as $row){
                $vehicle = DriverVehicle::where('user_id', $row['id'])->with('TruckType:id,name', 'TruckSubCategory:id,name', 'brands:id,name', 'VehicleCapacity')->first();
                $vehicle->truck_features = $vehicle->feature; 
                $permissions = DriverPermission::whereUserId($row['id'])->first();
                $row['vehicle'] = $vehicle;
                $row['permission'] = $permissions;
                $response[] = $row;
            }
        }
        return $this->successResponse($response, __('Driver list'), true);
    }

    public function notificationList($driver_id)
    {
        $user = User::where('id', $driver_id)->exists();
        $user_role = UserRole::where(['user_id' => $driver_id,'role_id' => '3'])->exists();
        if(!empty($user) && !empty($user_role)){
            $response = Notification::where('user_id',$driver_id)->orderBy('id','desc')->get();
            Notification::where('user_id',$driver_id)->update(['read_status' => '1']);
            return $this->successResponse($response, __('Notification list'), true);
        }else{
            return $this->errorResponse(__('User Not Found'),true);
        }
    }

    public function chatUsers(Request $request){
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
        ],['driver_id.required' => __('The driver id field is required.'),           
          ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_driver = UserRole::where(['user_id' => $data['driver_id'],'role_id' => '3'])->exists();
        if($check_is_driver){
            $users_receiver = Message::select('sd.*','message.message')->where('receiver_id', '=', $data['driver_id'])
            ->join('users as sd','sd.id','message.sender_id')
            ->orderBy('message.created_at','desc')
            ->groupBy('sd.id')
            ->get()->toArray();
            $existsting_ids = [];
            if(isset($users_receiver) && !empty($users_receiver)){
                foreach($users_receiver as $receiver){
                    $existsting_ids[] = $receiver['id'];
                }
            }

            $users_sender = Message::select('rc.*','message.message')->where('sender_id', '=', $data['driver_id'])->whereNotIn('receiver_id',$existsting_ids)
            ->join('users as rc','rc.id','message.receiver_id')
            ->orderBy('message.created_at','desc')
            ->groupBy('rc.id')
            ->get()->toArray();

            $final_users = array_merge($users_receiver,$users_sender);
            $users_list = [];
            foreach($final_users as $user){
                $message = Message::where(['sender_id' => $data['driver_id'], 'receiver_id' => $user['id']])
                ->orWhere(function($query) use($data,$user){
                    $query->where(['sender_id' => $user['id']]);
                    $query->where(['receiver_id' => $data['driver_id']]);
                })->orderBy('id','desc')->limit(1)->get()->toArray();
                
                if(isset($message) && !empty($message)){
                    $user['message'] = $message[0]['message'];
                    $user['message_time'] = date('Y-m-d h:i A',strtotime($message[0]['created_at']));
                }
                $users_list[] = $user;
            }
            // echo '<pre>';print_r($users_list);die();
            // return $this->successResponse($final_users, __('Users list'), true);
            // return ChatUserResource::collection($final_users);
            return (ChatUserResource::collection($users_list))->additional([
                'status' => true,
                'message' => __('Users list')    
            ]);
        }else{
            return $this->errorResponse(__('User Not Found'),true);
        }
    }

    public function chatMessages(Request $request)
    {
        // echo '<pre>';print_r($request->all());die();
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'shipper_id' => 'required'
        ],['driver_id.required' => __('The posted driver id field is required.'),
           'shipper_id.required' => __('The posted shipper id field is required.'),
           
          ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        
        $messages = Message::select('rc.id as receiver_id','rc.name as receiver_name','sd.id as sender_id','sd.name as sender_name','message.sender_id','message.created_at','message.message')
        ->join('users as rc','rc.id','message.receiver_id')
        ->join('users as sd','sd.id','message.sender_id')
        ->where(['sender_id' => $data['driver_id'], 'receiver_id' => $data['shipper_id']])
        ->orWhere(function($query) use($data){
            $query->where(['sender_id' => $data['shipper_id']]);
            $query->where(['receiver_id' => $data['driver_id']]);
        })
        ->get()->toArray();

        if(!empty($messages)){
            foreach($messages as $key=>$row){
                    $messages[$key]['created_at'] = date('Y-m-d H:i:s',strtotime($row['created_at']));
            }
        }
        return $this->successResponse($messages, __('Message list'), true);
    }

    public function user_setting($id)
    {
        $userSetting=UserSettings::find($id);
        if(isset($userSetting))
        {
             return $this->successResponse($userSetting, __('User Notification'), true); 
        }else
            {
                return $this->errorResponse(__('User Notification not Found'),true);
            }
    }
    public function userSettingUpdate(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'id'=>'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $id=$request->id;
        $userSetting=UserSettings::find($id);
        $userSetting->user_id=$request->user_id;
        $userSetting->notification=$request->notification;
        $userSetting->message=$request->message;
        $userSetting->bid_received=$request->bid_received;
        $userSetting->bid_accepted=$request->bid_accepted;
        $userSetting->load_assign_by_dispatcher=$request->load_assign_by_dispatcher;
        $userSetting->start_trip_reminder=$request->start_trip_reminder;
        $userSetting->complete_trip_reminder=$request->complete_trip_reminder;
        $userSetting->pdo_remider=$request->pdo_remider;
        $userSetting->match_shippment_near_you=$request->match_shippment_near_you;
        $userSetting->match_shippment_near_delivery=$request->match_shippment_near_delivery;
        $userSetting->rate_shipper=$request->rate_shipper;
        $userSetting->save();
        if(isset($userSetting)){
            return $this->successResponse(__('User Notification updated successfully'),true);
        }else{
             return $this->errorResponse(__('something has wrong, please try again later'),true);
        }

    }

}
