<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Facility;
use App\Models\UserRole;
use App\Models\Role;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Booking;
use App\Models\Device;
use App\Models\DriverVehicle;
use App\Models\BookingTruck;
use Carbon\Carbon;
use App\Helpers; 

class AuthController extends Controller {
    use ApiResponser;

    public function init($type="",$version="",$driver_id=0){
        $data['booking'] = [];
        $data['update_location_interval'] = 20;
        if($driver_id > 0){
            $booking_data = Booking::whereHas('booking_truck', function($query) use($driver_id) {$query->where('driver_id',$driver_id); })->where(['status' => 'in-progress'])->first();
            $driver_vehicle_details = DriverVehicle::where('user_id',$driver_id)->first();
            if(isset($booking_data) && !empty($booking_data)){
                $booking_data = $booking_data->toArray();
                $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($booking_data['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                $booking_data['trucks'] = $trucks->toArray();
            }
            $data['booking'] = $booking_data;
            $data['terms_and_condition'] = url('terms-condition');
            $data['privacy_policy'] = url('privacy-policy');
            $data['about_us'] = url('about-us');
            $data['radius'] = 500;
            $data['localization_language'] = [
                [
                    'name' => 'Greek',
                    'localization_code' => 'gr'
                ],
                [
                    'name' => 'English',
                    'localization_code' => 'en'
                ]
            ];
        }
        return $this->successResponse($data);
    }

    public function login(Request $request,$driver_id=0) {
        $validator = Validator::make(request()->all(), [
            'email' => 'email|required',
            'password' => 'required',
        ],
        ['required' => __('The email field is required.'),
         'required' => __('The password field is required.'),
    	]);
        if (!$validator->fails()) {
            
            if (!auth()->attempt($request->all())) {
                $data['status'] = false;
                $data['error'] = __('Invalid email or password');
                return $this->errorResponse($data);
            }
            $user = \Auth::user();
            print_r(Device::where(['user_id' => $user->id])->first());die();
            $booking_data = Booking::where('user_id',$user->id)->select("booking.*")->where(['status' => 'in-progress'])->first();
            // print_r($booking_data);die();
            $driver_vehicle_details = DriverVehicle::where('user_id',$driver_id)->first();
            if(isset($booking_data) && !empty($booking_data)){
                $booking_data = $booking_data->toArray();
                $trucks = BookingTruck::whereBookingId($booking_data['id'])->first();
                $booking_data['trucks'] = $trucks->toArray();
            }
            $accessToken = auth('api')->user()->createToken('authToken')->accessToken;
            $user = auth('api')->user();
            $user->token = $accessToken;
            $data['status'] = true;
            $data['data'] = $user;
            $data['booking_data'] = $booking_data;
            return $this->successResponse($data,__('login Successfully'));
        }
        $data['status'] = false;
        $data['error'] = $validator->messages();
        return $this->errorResponse($data, true);

    }

    public function register(Request $request){
       
        $validator = \Validator::make($request->all(), [
            'name' =>  'sometimes|nullable',
            'first_name'=> 'required',
            'last_name' => 'required',
            'email' => 'email|required',
            'profile' => 'required|file|max:8192',
            'facebook_link' => 'sometimes|nullable|url',
            'twitter_link' => 'sometimes|nullable|url',
            'linkedin' => 'sometimes|nullable|url',
            'phone_number' => 'required|numeric',
            'type' => 'required|in:company,freelancer',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6',
            'company_name' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'post_code' => 'required|digits:5|integer',
            'ssn' => 'required',
            'facility_nickname' => 'required',
            'facility_address' => 'required',
            'facility_city' => 'required',
            'facility_state' => 'required',
            'facility_country' => 'required',
            'facility_post_code' => 'required|digits:5|integer',
            'facility_phone_number' => 'required|numeric',
            'facility_email' => 'sometimes|nullable',
            // multiple services & multiple product for shiper ----- HOLD

        ],
    	[
         'first_name.required' => __('The first name field is required.'),
         'last_name.required' => __('The last name field is required.'),
         'email.required' => __('The email field is required.'),
         'profile.required' => __('The profile field is required.'),
         'phone_number.required' => __('The phone number field is required.'),
         'type.required' => __('The type field is required.'),
         'password.required' => __('The password field is required.'),
         'password_confirmation.required' => __('The password confirmation field is required.'),
         'company_name.required' => __('The company name field is required.'),
         'city.required' => __('The city field is required.'),
         'state.required' => __('The state field is required.'),
         'country.required' => __('The country field is required.'),
         'post_code.required' => __('The post code field is required.'),
         'ssn.required' => __('The ssn field is required.'),
         'facility_nickname.required' => __('The facility nickname field is required.'),
         'facility_address.required' => __('The facility address field is required.'),
         'facility_city.required.' => __('The facility city field is required.'),
         'facility_state.required' => __('The facility state field is required.'),
         'facility_country.required' => __('The facility country field is required.'),
         'facility_post_code.required' => __('The facility post code field is required.'),
         'facility_phone_number.required' => __('The facility phone number field is required.'),
         'facility_email.required' => __('The facility email field is required.'),
    	]);
        
        if (!$validator->fails()) {
            $data = $request->all();
            \DB::beginTransaction();
            try {
                    
                    $img = Helpers\CommonHelper::imageUpload($data['profile'], 'images/shipper/');
                   
                    #USER
                    $user = User::create([
                        'name' => $data['name'] ? $data['name'] : '',
                        'first_name' => $data['first_name'],
                        'last_name' => $data['last_name'],
                        'email' => $data['email'],
                        'profile' => $img,
                        'self_des' => $data['self_des'] ? $data['self_des'] : '',
                        'facebook_link' => $data['facebook_link'] ,
                        'twitter_link' => $data['twitter_link'],
                        'linkedin' => $data['linkedin'],
                        'phone' => $data['phone_number'],
                        'type' => $data['type'],
                        'password' => Hash::make($data['password']),
                        'company_name' => $data['company_name'],
                        'city' => $data['city'],
                        'state' => $data['state'],
                        'country' => $data['country'],
                        'ssn' => $data['ssn'],
                    ]);
                  
                    #FACILITY
                    $facilities = Facility::insert([
                        'user_id' => $user->id,
                        'name' => $data['facility_nickname'],
                        'address' => $data['facility_address'],
                        'city' => $data['facility_city'],
                        'state' => $data['facility_state'],
                        'country' => $data['facility_country'],
                        'zipcode' => $data['facility_post_code'],
                        'phone' => $data['facility_phone_number'],
                        'email' => $data['facility_email'],
                        'updated_at' => Carbon::now(),

                    ]);

                    #ROLE_CHECK
                    $role = Role::where('name','shipper')->first();
                   

                    #USER_ROLE
                    $user_role = UserRole::insert([
                        'user_id' => $user->id,
                        'role_id' => $role['id'],
                    ]);
                    
                    $accessToken = $user->createToken('authToken')->accessToken;
                    $user->token = $accessToken;
                    $data['token'] = $accessToken;
                    $data['status'] = true;

                    \DB::commit();

                    dispatch(new \App\Jobs\ShipperSendEmailJob($data));

                    return $this->successResponse($data,__('User Register Successfully'));

                    #SHIPER REGISTER MAIL


                    
                   

            } catch (\Exception $e) {
                \DB::rollback();
                $data['status'] = false;
                $data['error'] = $e->getMessage();
                return $this->errorResponse($data, true);

            }
                 
        }else{
            $data['status'] = false;
            $data['error'] = $validator->messages();
            return $this->errorResponse($data, true);
        }

    }

    public function system_date_time()
    {
        // $data['status'] = true;
        // $data['date'] = date('Y-m-d H:i:s');
        return $this->successResponse(date('Y-m-d h:i A'),__("success"),true);
        // return date('Y-m-d H:i:s');
    }

    public function logout()
    {
        $user_id = auth('api')->user()->id;
        Device::where('user_id',$user_id)->delete();
        $user = auth('api')->user()->token();
        $user->revoke();
        return $this->successResponse([],__('Logout Successfully'));
    }
}
