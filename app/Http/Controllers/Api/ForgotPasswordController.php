<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class ForgotPasswordController extends Controller
{
    use ApiResponser;

    public function forgot(Request $request) {
        $validator = Validator::make($request->all(), [
            'phone' => 'required'
        ],['phone.required' => __('The phone field is required.'),  
          ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $user = User::wherePhone($request->phone)->first();
        if(!empty($user))
        {
            $otp = 456789;
            $data['OTP'] = $otp;
            return $this->successResponse($data,__('Please type the verification code sent via SMS'),  true);
        }
       return $this->errorResponse(__('This phone number is not registered!'), false);
    }

    public function reset(Request $request) {

        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'password' => 'required'
        ],['phone.required' => __('The phone field is required.'), 
            'password.required' => __('The password field is required.'),  
          ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $user = User::wherePhone($request->phone)->first();
        $data['id'] = $user->id;
        if(!empty($user))
        {
            $password = Hash::make($request->password);
            User::wherePhone($request->phone)->update(['password'=>$password]);
            return $this->successResponse($data, __('Password reset successfully.'),  true);
        }

        return $this->errorResponse(__('This phone number is not registered!'), false);
    }

}
