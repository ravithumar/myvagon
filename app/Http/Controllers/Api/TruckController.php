<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Models\TruckCategory;
use App\Models\TruckType;
use App\Models\PackageType;
use App\Models\TruckBrand;
use App\Models\TruckFeatures;
use App\Models\TruckUnit;

class TruckController extends Controller
{
    use ApiResponser;

    public function categoryListing(Request $request){
        $type = $request->input('type');
       
        if(isset($type))
            $categories = TruckCategory::orderBy('id','desc')->where('truck_type_id', $type)->where('status', 1)->get();
        else
            $categories = TruckCategory::orderBy('id','desc')->where('status', 1)->get();
        
        return $this->successResponse($categories, __("Data Get Successfully"));
    }
    public function truckTypeListing(){
        $type = TruckType::orderBy('id','desc')
                        ->where('status', 1)
                        ->with(['category' => function($query) {$query->where('status', 1);}])
                        ->get();
                        
        return $this->successResponse($type, __("Data Get Successfully"), true);
    }
    public function packageListing(){
        $package = PackageType::select('id','name')->orderBy('id', 'asc')->where('status',1)->get();
        return $this->successResponse($package, __("Data Get Successfully"), true);
    }

    public function unitListing(){
        $units = TruckUnit::select('id','name' )->orderBy('id','desc')->get();
        return $this->successResponse($units, __("Data Get Successfully"));
    }

    public function truckFeatures(){
        $features = TruckFeatures::select('id','name')->orderBy('id','desc')->get();
        return $this->successResponse($features, __("Data Get Successfully"));
    }

    public function truckBrands(){
        
        $brands = TruckBrand::select('id','name')->orderBy('id','desc')->get();
        return $this->successResponse($brands, __("Data Get Successfully"));
    }

    public function lang($locat = 'en'){
        // echo '<pre>';print_r($locat);
        $data = \App::getLocale('lang',$locat);
        return $this->successResponse($data,__("Name"));
    }

}
