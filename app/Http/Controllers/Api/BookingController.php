<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommonHelper as HelpersCommonHelper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Validator;
use App\Models\Booking;
use App\Models\User;
use App\Models\BookingBid;
use App\Models\BookingTruck;
use App\Models\DriverVehicle;
use App\Models\Availability;
use App\Models\BookingRequest;
use App\Models\BookingLocations;
use App\Models\BookingProduct;
use App\Models\ReviewRating;
use App\Models\Notification;
use App\Models\CancelBooking;
use App\Models\Transaction;
use Booking as GlobalBooking;
use Illuminate\Support\Facades\DB;
use CommonHelper;
use Twilio\Tests\Request as TestsRequest;
use Carbon\Carbon;
use LDAP\Result;
use SocketHelper;

class BookingController extends Controller
{
    use ApiResponser;

    public function booking_details(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'driver_id' => 'required',
        ],['booking_id.required' => __('The booking id field is required.'),
        'driver_id.required' => __('The driver id field is required.'),
        ]);
        $data = $request->all();
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $booking = Booking::select("booking.*")
            ->where('booking.id',$request->booking_id)
            ->first();
            $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
        if(!empty($booking))
        {
            $booking = $booking->toArray();
            $truck = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($booking['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
            // echo '<pre>';print_r($truck);die();
            $booking['amount'] = CommonHelper::price_format($booking['amount'],'GR');
            $sum = 0;
            if(!empty($truck) && isset($truck)){
                $booking['trucks'] = $truck->toArray();
                $sum = $this->weight_sum($truck->toArray()['locations']);
            }

            // $sum = array_sum(array_column($truck->toArray()['locations'][0]['products'], 'weight'));
            $booking['total_weight'] = $sum;
            
            $check_bid = BookingBid::whereBookingId($request->booking_id)->whereDriverId($request->driver_id)->first();
            $booking['driver_bid'] = 0;
            if(!empty($check_bid))
            {
                $booking['driver_bid'] = 1;
            }
            return $this->successResponse($booking, __("Data Get Successfully"), true);
        }
         return $this->errorResponse(__('Shipment not found!'),true);
    }

    public function my_loads_bkp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'date' => 'required',
            'driver_id' => 'required',
            'status' => 'required',
        ],['status.required' => __('The status field is required.'),
        'driver_id.required' => __('The driver id field is required.'),
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), true);
        }
        $data = $request->all();

        $driver_details = User::where('id',$data['driver_id'])->first();
        
        if(!empty($driver_details)){
            if($data['status'] == 'all' || $data['status'] == 'pending'){
                $my_loads = Booking::select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->get();
            }else{
                $my_loads = Booking::select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->where('booking.status',$data['status'])->get();
            }
            $loads_final = [];
            if(!empty($my_loads)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($my_loads->toArray() as $row){   
                    $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
                    if(isset($loads[$i]['trucks']) && !empty($loads[$i]['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $loads[$i]['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($loads[$i]['trucks']['locations']);
                            // $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                        }
                        $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $loads[$i]['total_weight'] = $sum." ".$unit;
                    }
                    $i++;
                }
                if(!empty($loads)){
                    $loads_final = $this->group_by($loads,'pickup_date');
                }
            }
            return $this->successResponse($loads_final, __("success"), true);
        }else{
            return $this->errorResponse(__('User Not Found'), true);
        }
    }

    public function my_loads_new(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'status' => 'required',
            'page_num' => 'required',
            'type' => 'required',
        ],['status.required' => __('The status field is required.'),
        'driver_id.required' => __('The driver id field is required.'),
        'page_num.required' => __('The page num field is required.'),
        'type.required' => __('The type field is required.'),
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), true);
        }
        $data = $request->all();
        // echo '<pre>';print_r($data['type']);die();
        $driver_details = User::where('id',$data['driver_id'])->first();
        
        if(!empty($driver_details)){
            $limit = 10;
            $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
            $skip = ($page - 1) * $limit;
            $my_loads = array();
            $posted_trucks = array();
            $posted_trucks = $this->get_posted_truck($data,$skip,$limit);

            $posted_truck_final = [];
            $match_booking_id = [];
            if(!empty($posted_trucks)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                $posted_trucks = $posted_trucks->toArray();
                foreach($posted_trucks as $row){   
                    if(isset($posted_trucks[$i]['booking_info']['trucks']) && !empty($posted_trucks[$i]['booking_info']['trucks'])){
                        $match_booking_id[] = $posted_trucks[$i]['booking_info']['id'];
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['booking_info']['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $posted_trucks[$i]['booking_info']['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($posted_trucks[$i]['booking_info']['trucks']['locations']);
                        }
                        $unit = isset($posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $posted_trucks[$i]['booking_info']['total_weight'] = $sum." ".$unit;
                    }
                    // $posted_trucks[$i]['is_bid']=2;
                    // echo '<pre>';print_r($posted_trucks);die();
                    $matches = $this->nearByLocation($posted_trucks[$i],'1');
                    $count_matches = count($matches);
                    $posted_trucks[$i]['matches_found'] = $count_matches;
                    $posted_trucks[$i]['posted_truck']=1;
                    $i++;
                }
                if(!empty($posted_trucks)){
                    // $posted_truck_final = $posted_trucks->toArray();
                    $posted_truck_final = $posted_trucks;
                }
            }

            // echo '<pre>';print_r($match_booking_id);die();
            $my_loads = $this->get_loads($data,$skip,$limit,$match_booking_id);
            
            $loads_final = [];
            
            if(!empty($my_loads)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($my_loads->toArray() as $row){   
                    $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
                    $loads[$i] = $this->change_key($row, 'pickup_date', 'date');
                    if(isset($loads[$i]['trucks']) && !empty($loads[$i]['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $loads[$i]['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($loads[$i]['trucks']['locations']);
                            // $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                        }
                        $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $loads[$i]['total_weight'] = $sum." ".$unit;
                    }
                    $i++;
                }
                if(!empty($loads)){
                    // $loads_final = $this->group_by_request($loads,'id');
                    $loads_final = $loads;
                }
            }
            // echo '<pre>';print_r($loads_final);die();

            
            // print_r($posted_truck_final);exit;
            $load_data = array_merge($loads_final,$posted_truck_final);
            $final_array = array();
            if(!empty($load_data)){
                $final_array = $this->group_by_request($load_data,'id');
            }

            $date = array();
            foreach ($final_array as $key => $row)
            {
                $date[$key] = strtotime($row['date']);
            }
            array_multisort($date, SORT_DESC, $final_array);


            // return $this->successResponse($loads_final, __("success"), true);
            return $this->successResponse($final_array, __("Data Get Successfully"), true);
        }else{
            return $this->errorResponse(__('User Not Found'), true);
        }
    }

    public function my_loads_new2(Request $request){
        $validator = Validator::make($request->all(), [
            // 'date' => 'required',
            'driver_id' => 'required',
            'status' => 'required',
            'page_num' => 'required',
        ],['status.required' => __('The status field is required.'),
        'driver_id.required' => __('The driver id field is required.'),
        'page_num.required' => __('The page num field is required.'),
        
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), true);
        }
        $data = $request->all();
        // echo '<pre>';print_r($data['type']);die();
        $driver_details = User::where('id',$data['driver_id'])->first();
        
        if(!empty($driver_details)){
            $limit = 10;
            $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
            $skip = ($page - 1) * $limit;

            if($data['status'] == 'all'){
                $my_loads = Booking::select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->get();
            }elseif($data['status'] == 'pending'){
                $my_loads = Booking::select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->get();
            }else{
                $my_loads = Booking::select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->get();
            }
            // echo '<pre>';print_r($my_loads);
            // die();
            $loads_final = [];
            if(!empty($my_loads)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($my_loads->toArray() as $row){   
                    $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
                    $loads[$i] = $this->change_key($row, 'pickup_date', 'date');
                    if(isset($loads[$i]['trucks']) && !empty($loads[$i]['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $loads[$i]['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($loads[$i]['trucks']['locations']);
                            // $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                        }
                        $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $loads[$i]['total_weight'] = $sum." ".$unit;
                    }
                    $i++;
                }


                if(!empty($loads)){
                    $loads_final = $this->group_by_request($loads,'id');
                }
            }
            return $this->successResponse($loads_final, __("success"), true);
        }else{
            return $this->errorResponse(__('User Not Found'), true);
        }
    }

    public function my_loads_1(Request $request){

        $validator = Validator::make($request->all(), [
            // 'date' => 'required',
            'driver_id' => 'required',
            'status' => 'required',
            'page_num' => 'required',
            'type' => 'required',
        ],['status.required' => __('The status field is required.'),
        'driver_id.required' => __('The driver id field is required.'),
        'page_num.required' => __('The page num field is required.'),
        'type.required' => __('The type field is required.'),
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), true);
        }
        $data = $request->all();
       
        $driver_details = User::where('id',$data['driver_id'])->first();
        
        if(!empty($driver_details)){
            $limit = 10;
            $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
            $skip = ($page - 1) * $limit;
            $my_loads = array();
            $posted_trucks = array();
            if($data['status'] == 'all'){

                if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                    $my_loads = Booking::select('booking.*')
                    ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                    ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                    ->where('booking_trucks.driver_id',$data['driver_id'])
                    ->where(function($query) use($data) {
                        if(isset($data['type']) && !empty($data['type'])){
                            if($data['type'] == 'bid'){
                                $query->where('booking.is_bid',1);
                            }elseif($data['type'] == 'book'){
                                $query->where('booking.is_bid',0);
                            }
                        }
                    })
                    ->orWhere(function($query) use($data) {
                        $query->where('booking_bid.driver_id', $data['driver_id'])
                            ->where('booking_bid.status', 0);
                    })
                    ->where(function($query) use($data) {
                        if(!empty($data['date'])){
                            $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                        }
                    })
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                    $my_loads = Booking::select('booking.*')
                    ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                    ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                    ->where(function($query) use($data) {
                        $query->where('booking_trucks.driver_id',$data['driver_id']);  
                        if(isset($data['type']) && !empty($data['type'])){
                            if($data['type'] == 'bid'){
                                $query->where('booking.is_bid',1);
                            }elseif($data['type'] == 'book'){
                                $query->where('booking.is_bid',0);
                            }
                        }
                    })
                    ->orWhere(function($query) use($data) {
                        $query->where('booking_bid.driver_id', $data['driver_id'])
                            ->where('booking_bid.status', 0);
                        if(isset($data['type']) && !empty($data['type'])){
                            if($data['type'] == 'bid'){
                                $query->where('booking.is_bid',1);
                            }elseif($data['type'] == 'book'){
                                $query->where('booking.is_bid',0);
                            }
                        }
                    })
                    ->where(function($query) use($data) {
                        if(!empty($data['date'])){
                            $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                        }
                    })
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }

                if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                    $posted_trucks = Availability::with(['booking_info'])->where(['availability.driver_id' => $data['driver_id']])
                    ->where('load_status','0')
                    ->skip($skip)
                    ->limit($limit)
                    ->get();
                   
                }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                    $posted_trucks = Availability::where(['driver_id' => $data['driver_id']])
                    ->with('booking_info')
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }
            }elseif($data['status'] == 'pending'){

                if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                    $my_loads = Booking::select('booking.*')
                    ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                    ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                    ->where('booking_trucks.driver_id',$data['driver_id'])
                    ->where('booking.status',$data['status'])
                    ->where(function($query) use($data) {
                        if(isset($data['type']) && !empty($data['type'])){
                            if($data['type'] == 'bid'){
                                $query->where('booking.is_bid',1);
                            }elseif($data['type'] == 'book'){
                                $query->where('booking.is_bid',0);
                            }
                        }
                    })
                    ->orWhere(function($query) use($data) {
                        $query->where('booking_bid.driver_id', $data['driver_id'])
                            ->where('booking_bid.status', 0);
                        if(isset($data['type']) && !empty($data['type'])){
                            if($data['type'] == 'bid'){
                                $query->where('booking.is_bid',1);
                            }elseif($data['type'] == 'book'){
                                $query->where('booking.is_bid',0);
                            }
                        }
                    })
                    ->where(function($query) use($data) {
                        if(!empty($data['date'])){
                            $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                        }
                    })
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                    $my_loads = Booking::select('booking.*')
                    ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                    ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                    ->where('booking_trucks.driver_id',$data['driver_id'])
                    ->where('booking.status',$data['status'])
                    ->where(function($query) use($data) {
                        if(isset($data['type']) && !empty($data['type'])){
                            if($data['type'] == 'bid'){
                                $query->where('booking.is_bid',1);
                            }elseif($data['type'] == 'book'){
                                $query->where('booking.is_bid',0);
                            }
                        }
                    })
                    ->orWhere(function($query) use($data) {
                        $query->where('booking_bid.driver_id', $data['driver_id'])
                            ->where('booking_bid.status', 0);
                    })
                    ->where(function($query) use($data) {
                        if(!empty($data['date'])){
                            $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                        }
                    })
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }

                if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                    $posted_trucks = Availability::where(['driver_id' => $data['driver_id']])
                    // ->leftJoin('booking_request','booking_request.driver_id','availability.driver_id')
                    ->where('load_status','0')
                    // ->where('booking_request.status','0')
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                    $posted_trucks = Availability::where(['driver_id' => $data['driver_id']])->with('booking_info')
                    ->where('load_status','0')
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }

            }else{
                if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                    $my_loads = Booking::select('booking.*')
                    ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                    ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                    ->where('booking_trucks.driver_id',$data['driver_id'])
                    ->where('booking.status',$data['status'])
                    ->where(function($query) use($data) {
                        if(isset($data['type']) && !empty($data['type'])){
                            if($data['type'] == 'bid'){
                                $query->where('booking.is_bid',1);
                            }elseif($data['type'] == 'book'){
                                $query->where('booking.is_bid',0);
                            }
                        }
                    })
                    ->where(function($query) use($data) {
                        if(!empty($data['date'])){
                            $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                        }
                    })
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                    $my_loads = Booking::select('booking.*')
                    ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                    ->join('booking_trucks','booking.id','=','booking_trucks.booking_id')
                    ->where('booking_trucks.driver_id',$data['driver_id'])
                    ->where('booking.status',$data['status'])
                    ->where(function($query) use($data) {
                        if(isset($data['type']) && !empty($data['type'])){
                            if($data['type'] == 'bid'){
                                $query->where('booking.is_bid',1);
                            }elseif($data['type'] == 'book'){
                                $query->where('booking.is_bid',0);
                            }
                        }
                    })
                    ->where(function($query) use($data) {
                        if(!empty($data['date'])){
                            $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                        }
                    })
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }

                if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                     $posted_trucks = Availability::with(['booking_info'])
                    //  ->select('availability.*','booking_request.booking_id as bkid')
                    // ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                    ->where('availability.load_status','1')
                    ->where(['availability.driver_id' => $data['driver_id']])
                    ->whereHas('booking_info',function($q) use($data)
                    {
                        $q->where('status',$data['status']);
                    
                    })
                    ->skip($skip)
                    ->limit($limit)
                    ->get();
                }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                    $posted_trucks = Availability::with(['booking_info'])
                    ->where(['driver_id' => $data['driver_id']])
                    ->where('load_status','1')
                    ->whereHas('booking_info',function($q) use($data)
                    {
                        $q->where('status',$data['status']);
                    
                    })
                    // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                    ->skip($skip)
                    ->limit($limit)
                    ->orderBy('id','desc')
                    ->get();
                }
            }
            // echo '<pre>';print_r($posted_trucks->toArray());
            // die();
            $loads_final = [];
            if(!empty($my_loads)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($my_loads->toArray() as $row){   
                    $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
                    $loads[$i] = $this->change_key($row, 'pickup_date', 'date');
                    if(isset($loads[$i]['trucks']) && !empty($loads[$i]['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $loads[$i]['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($loads[$i]['trucks']['locations']);
                            // $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                        }
                        $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $loads[$i]['total_weight'] = $sum." ".$unit;
                    }
                    $i++;
                }
                if(!empty($loads)){
                    // $loads_final = $this->group_by_request($loads,'id');
                    $loads_final = $loads;
                }
            }

            $posted_truck_final = [];
            if(!empty($posted_trucks)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($posted_trucks->toArray() as $row){   
                    
                    if(isset($posted_trucks[$i]['booking_info']['trucks']) && !empty($posted_trucks[$i]['booking_info']['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['booking_info']['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        // echo '<pre>';print_r($trucks->toArray());die();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $posted_trucks[$i]['booking_info']['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($posted_trucks[$i]['booking_info']['trucks']['locations']);
                        }
                        $unit = isset($posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $posted_trucks[$i]['booking_info']['total_weight'] = $sum." ".$unit;
                    }
                    // $posted_trucks[$i]['is_bid']=2;
                    $posted_trucks[$i]['posted_truck']=1;
                    $i++;
                }
                if(!empty($posted_trucks)){
                    $posted_truck_final = $posted_trucks->toArray();
                }
            }
            $load_data = array_merge($loads_final,$posted_truck_final);
            // print_r($posted_truck_final);exit;
            $final_array = array();
            if(!empty($load_data)){
                $final_array = $this->group_by_request($load_data,'id');
            }

            $date = array();
            foreach ($final_array as $key => $row)
            {
                $date[$key] = strtotime($row['date']);
            }
            array_multisort($date, SORT_DESC, $final_array);


            // return $this->successResponse($loads_final, __("success"), true);
            return $this->successResponse($final_array, __("success"), true);
        }else{
            return $this->errorResponse(__('User Not Found'), true);
        }
    }

    public function my_loads(Request $request){
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'status' => 'required',
            'page_num' => 'required',
            'type' => 'required',
        ],['status.required' => __('The status field is required.'),
        'driver_id.required' => __('The driver id field is required.'),
        'page_num.required' => __('The page num field is required.'),
        'type.required' => __('The type field is required.'),
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), true);
        }
        $data = $request->all();
        // echo '<pre>';print_r($data['type']);die();
        $driver_details = User::where('id',$data['driver_id'])->first();
        
        if(!empty($driver_details)){
            $limit = 10;
            $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
            $skip = ($page - 1) * $limit;
            $my_loads = array();
            $posted_trucks = array();
            $posted_trucks = $this->get_posted_truck($data,$skip,$limit);

            $posted_truck_final = [];
            $match_booking_id = [];
            if(!empty($posted_trucks)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                $posted_trucks = $posted_trucks->toArray();
                foreach($posted_trucks as $row){   
                    if(isset($posted_trucks[$i]['booking_info']['trucks']) && !empty($posted_trucks[$i]['booking_info']['trucks'])){
                        $match_booking_id[] = $posted_trucks[$i]['booking_info']['id'];
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['booking_info']['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $posted_trucks[$i]['booking_info']['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($posted_trucks[$i]['booking_info']['trucks']['locations']);
                        }
                       
                        $unit = isset($posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $posted_trucks[$i]['booking_info']['total_weight'] = $sum." ".$unit;
                        $posted_trucks[$i]['booking_info']['amount'] = CommonHelper::price_format($posted_trucks[$i]['booking_info']['amount'],'GR');
                    }
                    // $posted_trucks[$i]['is_bid']=2;
                    // if(isset($posted_trucks[$i]['']))
                
                    if(isset($posted_trucks[$i]['end_lng']) && !empty($posted_trucks[$i]['end_lng']) && $posted_trucks[$i]['end_lng'] != 0.0){
                        $matches = $this->nearByLocationBidBook($posted_trucks[$i],'1');
                    }else{
                        $matches = $this->nearByLocationBidBook($posted_trucks[$i],'0');
                    }
                    $final_matches = [];
                    if(isset($matches) && !empty($matches)){
                        foreach ($matches->toArray() as $row1) {
                            if(($row1['bid_driver'] != $data['driver_id']) ||  $row1['bid_driver']== null)
                            {
                                $final_matches[] = $row1; 
                            }
                        }
                    }
                    // echo '<pre>';print_r($matches);die();
                    $count_matches = count($final_matches);
                    $posted_trucks[$i]['matches_found'] = $count_matches;
                    $posted_trucks[$i]['posted_truck']=1;
                    $i++;
                }
                if(!empty($posted_trucks)){
                    // $posted_truck_final = $posted_trucks->toArray();
                    $posted_truck_final = $posted_trucks;
                }
            }

            // echo '<pre>';print_r($match_booking_id);die();
            $my_loads = $this->get_loads($data,$skip,$limit,$match_booking_id);
            // echo '<pre>';print_r($my_loads);die();
            $loads_final = [];
            
            if(!empty($my_loads)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($my_loads->toArray() as $row){   
                    $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
                    $loads[$i] = $this->change_key($row, 'pickup_date', 'date');
                    if(isset($loads[$i]['trucks']) && !empty($loads[$i]['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $loads[$i]['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($loads[$i]['trucks']['locations']);
                            // $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                        }
                        // if(isset($row['booking_bid_amount']) && !empty($row['booking_bid_amount'])){
                        //     $loads[$i]['booking_info']['amount'] = $row['booking_bid_amount'];   
                        // }
                        $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $loads[$i]['total_weight'] = $sum." ".$unit;
                        $loads[$i]['amount'] = CommonHelper::price_format($loads[$i]['amount'],'GR');
                    }
                    $i++;
                }
                if(!empty($loads)){
                    // $loads_final = $this->group_by_request($loads,'id');
                    $loads_final = $loads;
                }
            }
            // echo '<pre>';print_r($loads_final);die();

            
            // print_r($posted_truck_final);exit;
            $load_data = array_merge($loads_final,$posted_truck_final);
            $final_array = array();
            if(!empty($load_data)){
                $final_array = $this->group_by_request($load_data,'id');
            }

            $date = array();
            foreach ($final_array as $key => $row)
            {
                $date[$key] = strtotime($row['date']);
            }
            if($data['status'] == 'all' || $data['status'] == 'pending' || $data['status'] == 'scheduled'){
                array_multisort($date, SORT_ASC, $final_array);
            }else{

                array_multisort($date, SORT_DESC, $final_array);
            }


            // return $this->successResponse($loads_final, __("success"), true);
            return $this->successResponse($final_array, __("Data Get Successfully"), true);
        }else{
            return $this->errorResponse(__('User Not Found'), true);
        }
    }

    // public function bid_requests(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'availability_id' => 'required',
    //     ]);
    //     if ($validator->fails()) {
    //         return $this->errorResponse($validator->errors(), true);
    //     }
    //     $data = $request->all();
    //     $check_is_exists = Availability::where('id',$data['availability_id'])->exists();
    //     if($check_is_exists){
    //         $booking_request_data = Availability::where(['id' => $data['availability_id']])->with(['booking_request'])->first();
    //         // echo '<pre>';print_r($booking_request_data);die();
    //         if(isset($booking_request_data) && !empty($booking_request_data) && isset($booking_request_data['booking_request']) && !empty($booking_request_data['booking_request'])){
    //             foreach($booking_request_data['booking_request'] as $booking_request){
    //                 $booking_request['booking_info'] = Booking::whereId($booking_request['booking_id'])->orderBy('id','desc')->first();
    //                 // echo '<pre>';print_r($booking);die();
    //             }
    //         }
    //         // echo '<pre>';print_r($booking_request_data['booking_request']);die();
    //         return $this->successResponse($booking_request_data, __("success"), true);
    //     }else{
    //         return $this->errorResponse(__('Post availability not found'), true);
    //     }
    // }

    // public function bid_requests(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'availability_id' => 'required',
    //     ]);
    //     if ($validator->fails()) {
    //         return $this->errorResponse($validator->errors(), true);
    //     }
    //     $data = $request->all();
    //     $check_is_exists = Availability::where('id',$data['availability_id'])->exists();
    //     if($check_is_exists){
    //         $booking_request_data = Availability::where(['id' => $data['availability_id']])->with(['booking_request'])->first();
    //         $requests = $booking_request_data;
    //         if(isset($booking_request_data) && !empty($booking_request_data) && isset($booking_request_data['booking_request']) && !empty($booking_request_data['booking_request'])){
    //             $i = 0;
    //             foreach($booking_request_data['booking_request'] as $booking_request){
    //                 $requests[$i] = $booking_request;
    //                 $booking_info = Booking::whereId($booking_request['booking_id'])->orderBy('id','desc')->first();
    //                 $requests[$i]['booking_info'] = $booking_info;
    //                 if(isset($requests[$i]['booking_info']['trucks']) && !empty($requests[$i]['booking_info']['trucks'])){
    //                     $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($booking_request['booking_id'])->whereTruckType($booking_request_data->truck_type_id)->first();
    //                     $sum = 0;
    //                     if(isset($trucks) && !empty($trucks)){
    //                         $requests[$i]['booking_info']['trucks'] = $trucks->toArray();
    //                         $sum = $this->weight_sum($requests[$i]['booking_info']['trucks']['locations']);
    //                     }
    //                     $unit = isset($requests[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $requests[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
    //                     $requests[$i]['booking_info']['total_weight'] = $sum." ".$unit;
    //                 }
    //                 $i++;
    //             }
    //         }
    //         return $this->successResponse($requests, __("success"), true);
    //     }else{
    //         return $this->errorResponse(__('Post availability not found'), true);
    //     }
    // }

    public function bid_requests(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'availability_id' => 'required',
        ],['availability_id.required' => __('The availability id field is required.'),
        
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), true);
        }
        $data = $request->all();
        $check_is_exists = Availability::where('id',$data['availability_id'])->exists();
        if($check_is_exists){
            $booking_request_data = Availability::where(['id' => $data['availability_id']])->with(['booking_request' => function($query) {$query->where('status',0);}])->first();
            $requests = array();
            
            if(isset($booking_request_data) && !empty($booking_request_data) && isset($booking_request_data['booking_request']) && !empty($booking_request_data['booking_request'])){
                $i = 0;
                foreach($booking_request_data['booking_request'] as $booking_request){
                    $requests = $booking_request_data->toArray();
                    $requests['booking_request'][$i] = $booking_request;
                    // $booking_request_data['booking_request'][$i] = $booking_request;
                    $booking_info = Booking::with(['booking_truck','booking_truck.truck_type'])->whereId($booking_request['booking_id'])->orderBy('id','desc')->first();
                    if(!isset($booking_info) && empty($booking_info)){
                        return $this->errorResponse(__('Booking not found'), true);
                    }   
                    $booking_info = $booking_info->toArray();                 
                    $booking_info['trucks']['truck_type'] = $booking_info['booking_truck'][0]['truck_type']; 
                    $booking_info['total_weight']="0"; 
                    unset($booking_info['booking_truck']);
                    $requests['booking_request'][$i]['booking_info'] = $booking_info;
                    $requests['booking_request'][$i]['offer_price'] = "0";
                    // echo '<pre>';print_r($booking_request_data['bid_amount']);
                    // echo '<pre>';print_r($booking_request['amount']);die();
                    if(isset($booking_request_data['bid_amount']) && !empty($booking_request_data['bid_amount']) && isset($booking_request['amount']) && !empty($booking_request['amount'])){
                        if($booking_request_data['bid_amount'] != 0 && isset($booking_request['amount']) && !empty($booking_request['amount']) && $booking_request['amount'] != 0){
                            $requests['booking_request'][$i]['offer_price'] = strval((($booking_request['amount'] - $booking_request_data['bid_amount']) / $booking_request_data['bid_amount'])*100);
                        }else{
                            $requests['booking_request'][$i]['offer_price'] = "100";
                        }
                    }
                    $requests['booking_request'][$i]['offer_price'] = CommonHelper::price_format($requests['booking_request'][$i]['offer_price'],'GR');
                    // if(isset($requests['booking_request'][$i]['booking_info']['trucks']) && !empty($requests['booking_request'][$i]['booking_info']['trucks'])){
                    //     $sum = 0;
                    //     if(isset($trucks) && !empty($trucks)){
                    //         $sum = $this->weight_sum($requests['booking_request'][$i]['booking_info']['trucks']['locations']);
                    //     }
                    //     $unit = isset($requests['booking_request'][$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $requests['booking_request'][$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                    // }
                    $i++;
                }
            }
            if(empty($requests)){
                $requests = (object) $requests;
            }
            return $this->successResponse($requests, __("Data Get Successfully"), true);
        }else{
            return $this->errorResponse(__('Post availability not found'), true);
        }
    }



    public function get_loads($data,$skip,$limit,$match_booking_id)
    {
        $my_loads = array();
        if($data['status'] == 'all'){

            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $my_loads = Booking::distinct()->select(['booking.*','booking_bid.amount as booking_bid_amount'])
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->whereIn('booking.status',['pending','in-progress','scheduled'])
                // ->where(function($query) use($data) {
                //     if(isset($data['type']) && !empty($data['type'])){
                //         if($data['type'] == 'bid'){
                //             $query->where('booking.is_bid',1);
                //         }elseif($data['type'] == 'book'){
                //             $query->where('booking.is_bid',0);
                //         }
                //     }
                // })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('pickup_date','asc')
                ->get();
                // echo '<prE>';print_r($my_loads);die();
            }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                $my_loads = Booking::distinct()->select(['booking.*','booking_bid.amount as booking_bid_amount'])
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->whereIn('booking.status',['pending','in-progress','scheduled'])
                ->where(function($query) use($data) {
                    $query->where('booking_trucks.driver_id',$data['driver_id']);  
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','asc')
                ->get();
            }
        }elseif($data['status'] == 'pending'){

            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $my_loads = Booking::distinct()->select(['booking.*','booking_bid.amount as booking_bid_amount'])
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                $my_loads = Booking::distinct()->select(['booking.*','booking_bid.amount as booking_bid_amount'])
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }

        }elseif(isset($data['status']) && !empty($data['status']) && $data['status'] == 'in-progress'){
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                // ->whereNotIn('availability.booking_id',[])
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->whereNotIn('booking.id',$match_booking_id)
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->whereNotIn('booking.id',$match_booking_id)
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }
        }elseif(isset($data['status']) && !empty($data['status']) && $data['status'] == 'past'){
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                // ->whereNotIn('availability.booking_id',[])
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->whereNotIn('booking.id',$match_booking_id)
                ->whereIn('booking.status',['completed','cancelled'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->whereNotIn('booking.id',$match_booking_id)
                ->whereIn('booking.status',['completed','cancelled'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }
        }else{
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                // ->whereNotIn('availability.booking_id',[])
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->whereNotIn('booking.id',$match_booking_id)
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->whereNotIn('booking.id',$match_booking_id)
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }
        }
        return $my_loads;
    }

    public function get_posted_truck($data,$skip,$limit)
    {
        $posted_trucks = array();
        if($data['status'] == 'all'){
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])->select('availability.*',DB::raw('COUNT(booking_request.id) as booking_request_count'))->where(['availability.driver_id' => $data['driver_id']])
                // ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                ->leftJoin('booking_request',function($join){
                    $join->on('booking_request.availability_id','=','availability.id')->where('booking_request.status',0);
                })
                // ->where('booking_request.status',0)
                ->where('load_status','0')
                ->groupBy('availability.id')
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                ->get();

                // $posted_trucks = $this->get_posted_truck($data,$skip,$limit);
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $posted_trucks = Availability::distinct()->with('booking_info')->select('availability.*',DB::raw('COUNT(booking_request.id) as booking_request_count'))->where(['availability.driver_id' => $data['driver_id']])
                // ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                ->leftJoin('booking_request',function($join){
                    $join->on('booking_request.availability_id','=','availability.id')->where('booking_request.status',0);
                })
                // ->where('booking_request.status',0)
                ->groupBy('availability.id')    
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                ->get();
                // $posted_trucks = $this->get_posted_truck($data,$skip,$limit);
            }
        }elseif($data['status'] == 'pending'){

            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $posted_trucks = Availability::distinct()->select('availability.*',DB::raw('COUNT(booking_request.id) as booking_request_count'))->where(['availability.driver_id' => $data['driver_id']])
                // ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                ->leftJoin('booking_request',function($join){
                    $join->on('booking_request.availability_id','=','availability.id')->where('booking_request.status',0);
                })
                ->where('load_status','0')
                // ->where('booking_request.status',0)
                ->skip($skip)
                ->limit($limit)
                ->groupBy('availability.id')
                ->orderBy('availability.date','desc')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $posted_trucks = Availability::distinct()->select('availability.*',DB::raw('COUNT(booking_request.id) as booking_request_count'))->where(['availability.driver_id' => $data['driver_id']])->with('booking_info')
                // ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                ->leftJoin('booking_request',function($join){
                    $join->on('booking_request.availability_id','=','availability.id')->where('booking_request.status',0);
                })
                ->where('load_status','0')
                // ->where('booking_request.status',0)
                ->skip($skip)
                ->limit($limit)
                ->groupBy('availability.id')
                ->orderBy('availability.date','desc')
                ->get();
            }

        }elseif(isset($data['status']) && !empty($data['status']) && $data['status'] == 'in-progress'){
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])
                ->where(['availability.driver_id' => $data['driver_id']])
                ->where('availability.load_status','1')
                ->whereHas('booking_info',function($q) use($data)
                {
                    $q->where('status',$data['status']);
                })
                // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                // ->groupBy('availability.id')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])
                ->where(['availability.driver_id' => $data['driver_id']])
                ->where('availability.load_status','1')
                ->whereHas('booking_info',function($q) use($data)
                {
                    $q->where('status',$data['status']);
                })
                // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                // ->groupBy('availability.id')
                ->get();
            }
        }elseif(isset($data['status']) && !empty($data['status']) && $data['status'] == 'past'){
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])
                ->where(['availability.driver_id' => $data['driver_id']])
                ->where('availability.load_status','1')
                ->whereHas('booking_info',function($q) use($data)
                {
                    $q->whereIn('status',['completed','cancelled']);
                })
                // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                // ->groupBy('availability.id')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])
                ->where(['availability.driver_id' => $data['driver_id']])
                ->where('availability.load_status','1')
                ->whereHas('booking_info',function($q) use($data)
                {
                    $q->whereIn('status',['completed','cancelled']);
                })
                // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                // ->groupBy('availability.id')
                ->get();
            }
        }else{
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])
                ->where(['availability.driver_id' => $data['driver_id']])
                ->where('availability.load_status','1')
                ->whereHas('booking_info',function($q) use($data)
                {
                    $q->where('status',$data['status']);
                })
                // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                // ->groupBy('availability.id')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])
                ->where(['availability.driver_id' => $data['driver_id']])
                ->where('availability.load_status','1')
                ->whereHas('booking_info',function($q) use($data)
                {
                    $q->where('status',$data['status']);
                })
                // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                // ->groupBy('availability.id')
                ->get();
            }
        }
        return $posted_trucks;
    }

    public function posted_truck(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'date' => 'required',
            'driver_id' => 'required',
            'status' => 'required',
        ],['status.required' => __('The status field is required.'),
            'driver_id.required' => __('The driver id field is required.'),

        
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), true);
        }
        $data = $request->all();
        // echo '<pre>';print_r($data);die();
        $driver_details = User::where('id',$data['driver_id'])->first();
        
        if(!empty($driver_details)){
            $limit = 10;
            $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
            $skip = ($page - 1) * $limit;
            $status = $data['status'];

            if($status == 'all'){
                $posted_trucks = Availability::where(['driver_id' => $data['driver_id']])
                    ->with('booking_info')
                    ->skip($skip)
                    ->limit($limit)
                    ->get();
            }elseif($status == 'pending'){
                $posted_trucks = Availability::where(['driver_id' => $data['driver_id']])
                ->where('load_status','0')
                ->skip($skip)
                ->limit($limit)
                ->get();
            }else{
                $posted_trucks = Availability::where(['driver_id' => $data['driver_id']])
                    ->where('load_status','1')
                    ->with(['booking_info' => function($query) use($status) { $query->where('status',$status); }])
                    ->skip($skip)
                    ->limit($limit)
                    ->get();
            }
            $posted_truck_final = [];
            if(!empty($posted_trucks)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($posted_trucks->toArray() as $row){   
                    if(isset($posted_trucks[$i]['booking_info']['trucks']) && !empty($posted_trucks[$i]['booking_info']['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['booking_info']['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        // echo '<pre>';print_r($trucks->toArray());die();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $posted_trucks[$i]['booking_info']['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($posted_trucks[$i]['booking_info']['trucks']['locations']);
                        }
                        $unit = isset($posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $posted_trucks[$i]['booking_info']['total_weight'] = $sum." ".$unit;
                    }
                    $i++;
                }
                if(!empty($posted_trucks)){
                    $posted_truck_final = $posted_trucks;
                }
            }
            return $this->successResponse($posted_truck_final, __("success"), true);
        }else{
            return $this->errorResponse(__('User Not Found'), true);
        }
    }

    function weight_sum($array)
    {
        $sum = 0;
        foreach($array as $key => $value){
            // echo '<pre>';print_r($value);
            if(isset($value) && !empty($value)){
                foreach($value['products'] as $k => $product){
                    if($product['is_pickup'] == '1'){
                        $sum += round(str_replace(',','.',$product['weight']),2);
                    }
                }
            }
        }
        // echo '<pre>';print_r($sum);
        // die();
        return $sum;
    }

    function group_by($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val[$key]]['date'] = $val[$key];
            $return[$val[$key]]['loads_data'][] = $val;
        }
        $final = [];
        foreach ($return as $key => $value) {
            $final[]=$value;
        }
        return $final;
    }

    function group_by_request($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $empty_array = (object)[];
            $type = '';
            if(isset($val['posted_truck']) && !empty($val['posted_truck']) && $val['posted_truck'] == 1)
            {
                $type = 'posted_truck';
            }else{
                if($val['is_bid'] == 1){
                    $type = 'bid';
                }elseif($val['is_bid'] == 0){
                    $type = 'book';
                }else{
                    $type = 'posted_truck';
                }
            }
            $return[$val['created_at']]['type'] = $type;
            $return[$val['created_at']]['date'] = $val['date'];

            if(isset($val['posted_truck']) && !empty($val['posted_truck']) && $val['posted_truck'] == 1){
                // $val['matches_found'] = 0;
                //     if($val['date'] >= date('Y-m-d')){
                //         $matches = $this->nearByLocation($val,'1');
                //         if(isset($matches)){
                //             $val['matches_found'] = count($matches);
                //         }
                //     }
                    $return[$val['created_at']]['posted_truck'] = $val;
                    $return[$val['created_at']]['bid'] = $empty_array;
                    $return[$val['created_at']]['book'] = $empty_array;
            }else{
                if($val['is_bid'] == 1){
                    $return[$val['created_at']][$type] = $val;
                    $return[$val['created_at']]['book'] = $empty_array;
                    $return[$val['created_at']]['posted_truck'] = $empty_array;
                }elseif($val['is_bid'] == 0){
                    $return[$val['created_at']][$type] = $val;
                    $return[$val['created_at']]['bid'] = $empty_array;
                    $return[$val['created_at']]['posted_truck'] = $empty_array;
                }else{
                    // $val['matches_found'] = 0;
                    // if($val['date'] >= date('Y-m-d')){
                    //     $matches = $this->nearByLocation($val,'1');
                    //     if(isset($matches)){
                    //         $val['matches_found'] = count($matches);
                    //     }
                    // }
                    $return[$val['created_at']]['posted_truck'] = $val;
                    $return[$val['created_at']]['bid'] = $empty_array;
                    $return[$val['created_at']]['book'] = $empty_array;
                }
            }

                        
        }
        $final = [];
        foreach ($return as $key => $value) {
            $final[]=$value;
        }
        return $final;
    }

    // public function nearByLocation($driver,$drop_location = '0') {

    //     if($drop_location == '1'){
    //         $select = ["booking.*", 'booking_bid.driver_id as bid_driver', DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
    //         * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
    //         + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $driver['end_lat'] . "))
    //         * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $driver['end_lng'] . "))
    //         + sin(radians(" . $driver['end_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
    //     }else{
    //         $select = ["booking.*", "booking_bid.driver_id as bid_driver", DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
    //         * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
    //         + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
    //     }

    //     $matches = Booking::distinct()
    //         ->select($select)
    //         ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
    //         ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')
    //         ->leftJoin('booking_bid', 'booking.id', 'booking_bid.booking_id')
    //         // ->where('booking_locations.delivered_at', '<' , $current)
    //         ->where('booking_trucks.truck_type', $driver['truck_type_id'])
    //         // ->where('booking_bid.driver_id','!=', $driver['driver_id'])
    //         ->where('booking.pickup_date','>=',$driver['date'])
    //         ->where('booking.status','pending')
    //         ->having('pickup_distance', '<', 50)
    //         ->orderBy('pickup_date', 'asc');

    //     if($drop_location == '1')
    //     {
    //         $matches->having('drop_distance', '<', 50);
    //         $matches->having('drop_distance', '>=', 0);
    //     }
    //     return $matches->get();

    // }

    public function nearByLocation($driver,$drop_location = '0') {
        // $current = Carbon::now()->startOfDay();

        if($drop_location == '1'){
            $select = ["booking.*", 'booking_bid.driver_id as bid_driver', DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $driver['end_lat'] . "))
            * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $driver['end_lng'] . "))
            + sin(radians(" . $driver['end_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
        }else{
            $select = ["booking.*", "booking_bid.driver_id as bid_driver", DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
        }

        $matches = Booking::distinct('booking.id')
            ->select($select)
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')
            // ->leftJoin('booking_bid', 'booking.id', 'booking_bid.booking_id')
            ->leftJoin('booking_bid', function ($join) {
                $join->on('booking.id', '=', 'booking_bid.booking_id');
                $join->where(['booking_bid.status' => 0]);
            })
            // ->where('booking_locations.delivered_at', '<' , $current)
            ->where('booking_trucks.truck_type', $driver['truck_type_id'])
            // ->where('booking_bid.driver_id','!=', $driver['driver_id'])
            ->where('booking.pickup_date','>=',$driver['date'])
            ->where('booking.is_bid',$driver['is_bid'])
            ->where('booking.status','pending')
            ->where('booking.is_public','1')
            ->having('pickup_distance', '<', 50)
            ->groupBy('booking_locations.booking_id')
            ->orderBy('pickup_date', 'asc');

            if($drop_location == '1')
            {
                $matches->having('drop_distance', '>=', 0);
                $matches->having('drop_distance', '<', 50);
            }


        return $matches->get();

    }

    public function nearByLocationBidBook($driver,$drop_location = '0') {
        // $current = Carbon::now()->startOfDay();

        if($drop_location == '1'){
            $select = ["booking.*", 'booking_bid.driver_id as bid_driver', DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $driver['end_lat'] . "))
            * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $driver['end_lng'] . "))
            + sin(radians(" . $driver['end_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
        }else{
            $select = ["booking.*", "booking_bid.driver_id as bid_driver", DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
        }

        $matches = Booking::distinct('booking.id')
            ->select($select)
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')
            // ->leftJoin('booking_bid', 'booking.id', 'booking_bid.booking_id')
            ->leftJoin('booking_bid', function ($join) {
                $join->on('booking.id', '=', 'booking_bid.booking_id');
                $join->where(['booking_bid.status' => 0]);
            })
            // ->where('booking_locations.delivered_at', '<' , $current)
            ->where('booking_trucks.truck_type', $driver['truck_type_id'])
            // ->where('booking_bid.driver_id','!=', $driver['driver_id'])
            ->where('booking.pickup_date','>=',$driver['date'])
            // ->where('booking.is_bid',$driver['is_bid'])
            ->where('booking.status','pending')
            ->where('booking.is_public','1')
            ->having('pickup_distance', '<', 50)
            ->groupBy('booking_locations.booking_id')
            ->orderBy('pickup_date', 'asc');

            if($drop_location == '1')
            {
                $matches->having('drop_distance', '>=', 0);
                $matches->having('drop_distance', '<', 50);
            }


        return $matches->get();

    }

    function change_key( $array, $old_key, $new_key ) {

        if( ! array_key_exists( $old_key, $array ) )
            return $array;
    
        $keys = array_keys( $array );
        $keys[ array_search( $old_key, $keys ) ] = $new_key;
    
        return array_combine( $keys, $array );
    }

    public function acceptBookingRequest(Request $request)
    {
        echo '<pre>';print_r($request->all());die();
    }

    public function acceptBiddingRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_request_id' => 'required',
            'driver_id' => 'required',
        ],['booking_request_id.required' => __('The booking request id field is required.'),
            'driver_id.required' => __('The driver id field is required.'), ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), true);
        }
        $booking_request_id = $request->post('booking_request_id');
        $driver_id = $request->post('driver_id');
        $check_is_exists = BookingRequest::where(['id' => $booking_request_id,'driver_id' => $driver_id,'is_bid' => '1'])->exists();
        if($check_is_exists){
            $booking_request_data = BookingRequest::where(['id' => $booking_request_id])->first();
            $get_availability_data = Availability::where(['id' => $booking_request_data['availability_id'],'load_status' => '0'])->first();
            if(!empty($get_availability_data) && isset($get_availability_data)){
                $booking_data = Booking::with('booking_location')->where(['id' => $booking_request_data['booking_id']])->first();
                
                // die();
                $driver_vehicle_data = DriverVehicle::where(['user_id' => $driver_id])->first();
                if(isset($driver_vehicle_data) && !empty($driver_vehicle_data)){
                    // Asign Driver To Booking
                    $update_booking_truck = BookingTruck::where(['booking_id' => $booking_request_data['booking_id'],'truck_type' => $driver_vehicle_data['truck_type']])->update(['driver_id' => $driver_id]);
                    if($update_booking_truck){
                        BookingTruck::where(['booking_id' => $booking_request_data['booking_id']])->whereNotIn('truck_type', [$driver_vehicle_data['truck_type']])->delete();
                    }
                    Booking::where(['id' => $booking_request_data['booking_id']])->update(['status' => 'scheduled','amount' => number_format($booking_request_data['amount'],2)]);

                    //Change status of another booking requests
                    BookingRequest::where('availability_id',$booking_request_data['availability_id'])->update(['status' => 2]);
                    BookingRequest::where(['id' => $booking_request_id])->update(['status' => 1]);

                    //Change Availability Status 
                    if(isset($booking_data['booking_location']) && !empty($booking_data['booking_location'])){
                        $first_key = array_key_first($booking_data['booking_location']->toArray());
                        $last_key = array_key_last($booking_data['booking_location']->toArray());
    
                        $get_availability_ids = Availability::whereBetween('date',[$booking_data['booking_location'][$first_key]['delivered_at'], $booking_data['booking_location'][$last_key]['delivered_at']])->pluck('id');
    
                        // Update all the availability load status
                        Availability::whereIn('id',$get_availability_ids)->update(['load_status' => 2]);
                        return $this->successResponse([], __("Bid placed successfully"), true);
                    }
                    Availability::where('id',$booking_request_data['availability_id'])->update(['booking_id' => $booking_request_data['booking_id'],'load_status' => '1']);

                    

                }else{
                    return $this->errorResponse(__('User not exists.'), true);
                }
            }else{
                return $this->errorResponse(__('Availability not exists.'), true);    
            }
        }else{
            return $this->errorResponse(__('Booking request not exists.'), true);
        }
    }

    public function declineBiddingRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_request_id' => 'required',
            'driver_id' => 'required',
        ],['booking_request_id.required' => __('The booking request id field is required.'),
            'driver_id.required' => __('The driver id field is required.'),

        
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $booking_request_id = $request->post('booking_request_id');
        $driver_id = $request->post('driver_id');
        $check_is_exists = BookingRequest::where(['id' => $booking_request_id,'driver_id' => $driver_id,'is_bid' => '1','status' => 0])->exists();
        if($check_is_exists){
            BookingRequest::where(['id' => $booking_request_id])->update(['status' => 2]);
            return $this->successResponse([], __("Bid decline successfully"), true);
        }else{
            return $this->errorResponse(__('Booking request not exists.'), true);
        }
    }

    public function acceptdeclineBiddingRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_request_id' => 'required',
            'driver_id' => 'required',
            'status' => 'required',
        ],[ 'booking_request_id.required' => __('The booking request id field is required.'),
            'driver_id.required' => __('The driver id field is required.'),
            'status.required' => __('The status field is required.'),
        ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        // return $this->errorResponse($request->post('status'), true);
        // return $this->successResponse([], $request->post('status'), true);
        $booking_request_id = $request->post('booking_request_id');
        $driver_id = $request->post('driver_id');
        $check_is_exists = BookingRequest::where(['id' => $booking_request_id,'driver_id' => $driver_id,'is_bid' => '1'])->exists();
        if($check_is_exists){
            if($request->post('status') == 'accept'){
                $booking_request_data = BookingRequest::where(['id' => $booking_request_id])->first();
                $get_availability_data = Availability::where(['id' => $booking_request_data['availability_id'],'load_status' => '0'])->first();
                if(!empty($get_availability_data) && isset($get_availability_data)){
                    $booking_data = Booking::with('booking_location')->where(['id' => $booking_request_data['booking_id']])->first()->toArray();
                    
                    $driver_vehicle_data = DriverVehicle::where(['user_id' => $driver_id])->first();
                    if(isset($driver_vehicle_data) && !empty($driver_vehicle_data)){

                        $first_key = array_key_first($booking_data['booking_location']);
                        $last_key = array_key_last($booking_data['booking_location']);

                        // $check_driver_is_free = BookingTruck::where(['driver_id' => $driver_id])
                        // ->select(['booking_trucks.*','booking.id as booking_id','booking.status as booking_status','booking.pickup_date as booking_pickup_date'])
                        // ->join('booking','booking.id','=','booking_trucks.booking_id')
                        // ->join('booking_locations','booking_locations.booking_id','=','booking_trucks.booking_id')
                        // ->whereIn('booking_locations.delivered_at',[$booking_data['booking_location'][$first_key]['delivered_at'],$booking_data['booking_location'][$last_key]['delivered_at']])
                        // ->whereIn('booking.status',['scheduled','in-progress'])
                        // ->get()->toArray();

                        $check_driver_is_free = BookingTruck::where(['driver_id' => $driver_id])
                        ->select(['booking_trucks.*','booking.id as booking_id','booking.status as booking_status','booking.pickup_date as booking_pickup_date',DB::raw("CONCAT(booking_locations.delivered_at,' ',booking_locations.delivery_time_to) AS full_date")])
                        ->join('booking','booking.id','=','booking_trucks.booking_id')
                        ->join('booking_locations','booking_locations.booking_id','=','booking_trucks.booking_id')
                        ->whereIn('booking.status',['scheduled','in-progress'])
                        ->get()->toArray();
                        if(isset($check_driver_is_free) && !empty($check_driver_is_free)){
                            foreach($check_driver_is_free as $driver){
                                $new_first_key = array_key_first($driver['locations']);
                                $new_last_key = array_key_last($driver['locations']);
                                $start_date = date('Y-m-d H:i',strtotime($driver['locations'][$new_first_key]['delivered_at'].' '.$driver['locations'][$new_first_key]['delivery_time_to']));
                                $end_date = date('Y-m-d H:i',strtotime($driver['locations'][$new_last_key]['delivered_at'].' '.$driver['locations'][$new_last_key]['delivery_time_to']));
                                $exist_start = date('Y-m-d H:i',strtotime($booking_data['booking_location'][$first_key]['delivered_at'].' '.$booking_data['booking_location'][$first_key]['delivery_time_to']));
                                $exist_end = date('Y-m-d H:i',strtotime($booking_data['booking_location'][$last_key]['delivered_at'].' '.$booking_data['booking_location'][$last_key]['delivery_time_to']));
                                if(($start_date < $exist_start && $end_date > $exist_start) || ($start_date < $exist_end && $end_date > $exist_end)){
                                    return $this->errorResponse(__("You have already scheduled a shipment for applied dates and time."), true);
                                }
                                $update_booking_truck = BookingTruck::where(['booking_id' => $booking_request_data['booking_id'],'truck_type' => $driver_vehicle_data['truck_type']])->update(['driver_id' => $driver_id]);
                                if($update_booking_truck){
                                    BookingTruck::where(['booking_id' => $booking_request_data['booking_id']])->whereNotIn('truck_type', [$driver_vehicle_data->truck_type])->delete();
                                    BookingTruck::where(['booking_id' => $booking_request_data['booking_id']])->update(['truck_type_category' => $driver_vehicle_data['truck_sub_category']]);
                                }
                                Booking::where(['id' => $booking_request_data['booking_id']])->update(['status' => 'scheduled','amount' => number_format($booking_request_data['amount'],2),'schedule_time' => time()]);

                                //Change status of another booking requests
                                BookingRequest::where('availability_id',$booking_request_data['availability_id'])->update(['status' => 2]);
                                BookingRequest::where(['id' => $booking_request_id])->update(['status' => 1]);
                                BookingBid::where(['booking_id' => $booking_request_data['booking_id']])->update(['status' => 2]);

                                //Change Availability Status 
                                if(isset($booking_data['booking_location']) && !empty($booking_data['booking_location'])){
                                    $first_key = array_key_first($booking_data['booking_location']);
                                    $last_key = array_key_last($booking_data['booking_location']);

                                    $get_availability_ids = Availability::whereBetween('date',[$booking_data['booking_location'][$first_key]['delivered_at'], $booking_data['booking_location'][$last_key]['delivered_at']])->pluck('id');

                                    // Update all the availability load status
                                    Availability::whereIn('id',$get_availability_ids)->update(['load_status' => 2]);
                                    SocketHelper::emit('bid_accept_request_push',['driver_id' => $driver_id, 'booking_id' => $booking_data['id'], 'shipper_id' => $booking_data['user_id']]);
                                    return $this->successResponse([], __("Bid placed successfully"), true);
                                }
                            }
                            // return $this->errorResponse(__("You have already scheduled a shipment for applied dates and time."), true);
                        }else{
                            $booking_data = Booking::with('booking_location')->where(['id' => $booking_request_data['booking_id']])->first()->toArray();
                            // Asign Driver To Booking
                            $update_booking_truck = BookingTruck::where(['booking_id' => $booking_request_data['booking_id'],'truck_type' => $driver_vehicle_data['truck_type']])->update(['driver_id' => $driver_id]);
                            if($update_booking_truck){
                                BookingTruck::where(['booking_id' => $booking_request_data['booking_id']])->whereNotIn('truck_type', [$driver_vehicle_data->truck_type])->delete();
                                BookingTruck::where(['booking_id' => $booking_request_data['booking_id']])->update(['truck_type_category' => $driver_vehicle_data['truck_sub_category']]);
                            }
                            Booking::where(['id' => $booking_request_data['booking_id']])->update(['status' => 'scheduled','amount' => number_format($booking_request_data['amount'],2)]);

                            //Change status of another booking requests
                            BookingRequest::where('availability_id',$booking_request_data['availability_id'])->update(['status' => 2]);
                            BookingRequest::where(['id' => $booking_request_id])->update(['status' => 1]);
                            BookingBid::where(['booking_id' => $booking_request_data['booking_id']])->update(['status' => 2]);

                            //Change Availability Status 
                            if(isset($booking_data['booking_location']) && !empty($booking_data['booking_location'])){
                                $first_key = array_key_first($booking_data['booking_location']);
                                $last_key = array_key_last($booking_data['booking_location']);

                                $get_availability_ids = Availability::whereBetween('date',[$booking_data['booking_location'][$first_key]['delivered_at'], $booking_data['booking_location'][$last_key]['delivered_at']])->pluck('id');

                                // Update all the availability load status
                                Availability::whereIn('id',$get_availability_ids)->update(['load_status' => 2]);
                                SocketHelper::emit('bid_accept_request_push',['driver_id' => $driver_id, 'booking_id' => $booking_data['id'], 'shipper_id' => $booking_data['user_id']]);
                                return $this->successResponse([], __("Bid placed successfully"), true);
                            }
                            // Availability::where('id',$booking_request_data['availability_id'])->update(['booking_id' => $booking_request_data['booking_id'],'load_status' => '1']);
                        }    
                    }else{
                        return $this->errorResponse(__('User not exists.'), true);
                    }
                }else{
                    return $this->errorResponse(__('Availability not exists.'), true);    
                }
            }elseif($request->post('status') == 'reject'){
                $validator = Validator::make($request->all(), [
                    'booking_request_id' => 'required',
                    'driver_id' => 'required',
                ]);
                if ($validator->fails()) {
                    return $this->errorResponse($validator->errors(), true);
                }

                $booking_request_id = $request->post('booking_request_id');
                $driver_id = $request->post('driver_id');
                $check_is_exists = BookingRequest::where(['id' => $booking_request_id,'driver_id' => $driver_id,'is_bid' => '1','status' => 0])->first();
                if(!empty($check_is_exists)){
                    $booking_request_data = BookingRequest::where(['id' => $booking_request_id])->first();
                    $booking_data = Booking::with('booking_location')->where(['id' => $booking_request_data['booking_id']])->first()->toArray();
                    if(isset($check_is_exists->availability_id) && !empty($check_is_exists->availability_id) && $check_is_exists->availability_id){
                        Availability::where(['id' => $check_is_exists->availability_id])->update(['load_status' => '0']);
                    }
                    BookingRequest::where(['id' => $booking_request_id])->update(['status' => 2]);
                    SocketHelper::emit('bid_decline_request_push',['driver_id' => $driver_id, 'booking_id' => $booking_data['id'], 'shipper_id' => $booking_data['user_id']]);
                    return $this->successResponse([], __("Bid decline successfully"), true);
                }else{
                    return $this->errorResponse(__('Booking request not exists.'), true);
                }
            }else{
                return $this->errorResponse(__('Invalid status.'), true);
            }
            
        }else{
            return $this->errorResponse(__('Booking request not exists.'), true);
        }
    }

    public function arrivedAtPickupLocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'location_id' => 'required',
            'driver_id' => 'required',
            'booking_id' => 'required',
        ],['location_id.required' => __('The location id field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
           'booking_id.required' => __('The booking id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exists_booking = Booking::where(['id' => $data['booking_id']])->exists();
        if($check_is_exists_booking){
            $check_is_exists_booking_truck = BookingTruck::where(['booking_id' => $data['booking_id'],'driver_id' => $data['driver_id']])->exists();
            if($check_is_exists_booking_truck){
                BookingLocations::where(['id' => $data['location_id']])->update(['arrived_at' => time()]);
                $booking_data = Booking::where('id',$data['booking_id'])->first();
                $data['shipper_id'] = $booking_data['user_id'];
                SocketHelper::emit('arrived_at_location',$data);
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                if(!empty($booking_data)){
                    $booking_data = $booking_data->toArray();
                    $truck = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($booking_data['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();

                    $booking_data['trucks'] = $truck->toArray();

                    // $sum = array_sum(array_column($truck->toArray()['locations'][0]['products'], 'weight'));
                    $sum = $this->weight_sum($truck->toArray()['locations']);
                    $booking_data['total_weight'] = $sum;
                    
                    $check_bid = BookingBid::whereBookingId($request->booking_id)->whereDriverId($request->driver_id)->first();
                    $booking_data['driver_bid'] = 0;
                    if(!empty($check_bid))
                    {
                        $booking_data['driver_bid'] = 1;
                    }
                }
                // $notification_data['user_id'] = $booking_data['user_id'];
                // $notification_data['title'] = 'Driver Arrived';
                // $notification_data['message'] = "Driver has Booking ID#".$data['booking_id']." arrived at location";
                // $notification_data['created_at'] = date('Y-m-d h:i:s');
                // Notification::insert($notification_data);
                return $this->successResponse($booking_data, __("Arrived at pickup location"), true);
            }else{
                return $this->errorResponse(__('Driver does not assign this booking, please try again.'), true);
            }
        }else{
            return $this->errorResponse(__('Booking not exists.'), true);
        }
    }

    public function startLoading(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'location_id' => 'required',
            'driver_id' => 'required',
            'booking_id' => 'required',
        ],['location_id.required' => __('The location id field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
           'booking_id.required' => __('The booking id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exists_booking = Booking::where(['id' => $data['booking_id']])->exists();
        if($check_is_exists_booking){
            $check_is_exists_booking_truck = BookingTruck::where(['booking_id' => $data['booking_id'],'driver_id' => $data['driver_id']])->exists();
            if($check_is_exists_booking_truck){
                BookingLocations::where(['id' => $data['location_id']])->update(['start_loading' => time()]);
                $booking_data = Booking::where('id',$data['booking_id'])->first();
                $data['shipper_id'] = $booking_data['user_id'];
                SocketHelper::emit('start_loading',$data);
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                if(!empty($booking_data)){
                    $booking_data = $booking_data->toArray();
                    $truck = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($booking_data['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();

                    $booking_data['trucks'] = $truck->toArray();

                    // $sum = array_sum(array_column($truck->toArray()['locations'][0]['products'], 'weight'));
                    $sum = $this->weight_sum($truck->toArray()['locations']);
                    $booking_data['total_weight'] = $sum;
                    
                    $check_bid = BookingBid::whereBookingId($request->booking_id)->whereDriverId($request->driver_id)->first();
                    $booking_data['driver_bid'] = 0;
                    if(!empty($check_bid))
                    {
                        $booking_data['driver_bid'] = 1;
                    }
                    $booking_data['amount'] = CommonHelper::price_format($booking_data['amount'],'GR');
                }
                $notification_data['user_id'] = $booking_data['user_id'];
                $notification_data['title'] = 'Start Loading';
                $notification_data['message'] = "Driver has Booking ID#".$data['booking_id']." started loading";
                $notification_data['created_at'] = date('Y-m-d h:i:s');
                Notification::insert($notification_data);
                return $this->successResponse($booking_data, __("Start loading truck"), true);
            }else{
                return $this->errorResponse(__('Driver does not assign this booking, please try again.'), true);
            }
        }else{
            return $this->errorResponse(__('Booking not exists.'), true);
        }
    }

    public function startJourney(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'location_id' => 'required',
            'driver_id' => 'required',
            'booking_id' => 'required',
        ],['location_id.required' => __('The location id field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
           'booking_id.required' => __('The booking id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exists_booking = Booking::where(['id' => $data['booking_id']])->exists();
        if($check_is_exists_booking){
            $check_is_exists_booking_truck = BookingTruck::where(['booking_id' => $data['booking_id'],'driver_id' => $data['driver_id']])->exists();
            if($check_is_exists_booking_truck){
                $booking_location_data = BookingProduct::where(['booking_id' => $data['booking_id']])->first();
                BookingLocations::where(['id' => $data['location_id']])->update(['start_journey' => time(),'start_loading' => time()]);
                $booking_data = Booking::where('id',$data['booking_id'])->first();
                $data['shipper_id'] = $booking_data['user_id'];
                $data['is_pickup'] = $booking_location_data->is_pickup;
                SocketHelper::emit('start_journy',$data);
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                if(!empty($booking_data)){
                    $booking_data = $booking_data->toArray();
                    $truck = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($booking_data['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();

                    $booking_data['trucks'] = $truck->toArray();

                    // $sum = array_sum(array_column($truck->toArray()['locations'][0]['products'], 'weight'));
                    $sum = $this->weight_sum($truck->toArray()['locations']);
                    $booking_data['total_weight'] = $sum;
                    
                    $check_bid = BookingBid::whereBookingId($request->booking_id)->whereDriverId($request->driver_id)->first();
                    $booking_data['driver_bid'] = 0;
                    if(!empty($check_bid))
                    {
                        $booking_data['driver_bid'] = 1;
                    }
                    $booking_data['amount'] = CommonHelper::price_format($booking_data['amount'],'GR');
                }
                
                $notification_data['user_id'] = $booking_data['user_id'];
                $notification_data['title'] = 'Start Trip';
                $notification_data['message'] = "Driver has Booking ID#".$data['booking_id']." completed pickup";
                if($booking_location_data->is_pickup == '1'){
                    $notification_data['title'] = 'Complete Pickup';
                    $notification_data['message'] = "Driver has Booking ID#".$data['booking_id']." completed pickup";
                }else{
                    $notification_data['title'] = 'Complete Drop-off';
                    $notification_data['message'] = "Driver has Booking ID#".$data['booking_id']." completed drop-off";
                }
                $notification_data['created_at'] = date('Y-m-d h:i:s');
                Notification::insert($notification_data);
                return $this->successResponse($booking_data, __("Start Journey"), true);
            }else{
                return $this->errorResponse(__('Driver does not assign this booking, please try again.'), true);
            }
        }else{
            return $this->errorResponse(__('Booking not exists.'), true);
        }
    }

    public function completeTrip(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'location_id' => 'required',
            'driver_id' => 'required',
            'booking_id' => 'required',
        ],['location_id.required' => __('The location id field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
           'booking_id.required' => __('The booking id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exists_booking = Booking::where(['id' => $data['booking_id']])->exists();
        if($check_is_exists_booking){
            $check_is_exists_booking_truck = BookingTruck::where(['booking_id' => $data['booking_id'],'driver_id' => $data['driver_id']])->exists();
            if($check_is_exists_booking_truck){
                if(isset($data['pod_image']) && !empty($data['pod_image'])){
                    CommonHelper::moveImagesS3Bucket($data['pod_image'],'POD/');        
                }
                $booking_update_data['status'] = 'completed';
                $booking_update_data['end_trip'] = time();
                if(isset($data['pod_image']) && !empty($data['pod_image'])){
                    $booking_update_data['pod_image'] = $data['pod_image'];
                }
                Booking::where(['id' => $data['booking_id']])->update($booking_update_data);
                SocketHelper::emit('complete_booking',$data);
                $booking_data = Booking::where('id',$data['booking_id'])->first();
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                if(!empty($booking_data)){
                    $booking_data = $booking_data->toArray();
                    $truck = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($booking_data['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();

                    $booking_data['trucks'] = $truck->toArray();

                    // $sum = array_sum(array_column($truck->toArray()['locations'][0]['products'], 'weight'));
                    $sum = $this->weight_sum($truck->toArray()['locations']);
                    $booking_data['total_weight'] = $sum;
                    
                    $check_bid = BookingBid::whereBookingId($request->booking_id)->whereDriverId($request->driver_id)->first();
                    $booking_data['driver_bid'] = 0;
                    if(!empty($check_bid))
                    {
                        $booking_data['driver_bid'] = 1;
                    }
                    $booking_data['amount'] = CommonHelper::price_format($booking_data['amount'],'GR');
                }
                $notification_data['user_id'] = $booking_data['user_id'];
                $notification_data['title'] = 'Complete Trip';
                $notification_data['message'] = "Driver has Booking ID#".$data['booking_id']." completed trip";
                $notification_data['created_at'] = date('Y-m-d h:i:s');
                Notification::insert($notification_data);
                return $this->successResponse($booking_data, __("Booking Completed"), true);
            }else{
                return $this->errorResponse(__('Driver does not assign this booking, please try again.'), true);
            }
        }else{
            return $this->errorResponse(__('Booking not exists.'), true);
        }
    }

    public function uploadPOD(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pod_image' => 'required',
        ],['pod_image.required' => __('The pod image field is required.'),
           
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exists_booking = Booking::where(['id' => $data['booking_id']])->exists();
        if($check_is_exists_booking){
            if(isset($data['pod_image']) && !empty($data['pod_image'])){
                CommonHelper::moveImagesS3Bucket($data['pod_image'],'POD/');        
            }
            if(isset($data['pod_image']) && !empty($data['pod_image'])){
                $booking_update_data['pod_image'] = $data['pod_image'];
                Booking::where(['id' => $data['booking_id']])->update($booking_update_data);
            }
            $booking_data = Booking::where('id',$data['booking_id'])->first();
            $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
            if(!empty($booking_data)){
                $booking_data = $booking_data->toArray();
                $truck = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($booking_data['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();

                $booking_data['trucks'] = $truck->toArray();

                // $sum = array_sum(array_column($truck->toArray()['locations'][0]['products'], 'weight'));
                $sum = $this->weight_sum($truck->toArray()['locations']);
                $booking_data['total_weight'] = $sum;
                
                $check_bid = BookingBid::whereBookingId($request->booking_id)->whereDriverId($request->driver_id)->first();
                $booking_data['driver_bid'] = 0;
                if(!empty($check_bid))
                {
                    $booking_data['driver_bid'] = 1;
                }
                $booking_data['amount'] = CommonHelper::price_format($booking_data['amount'],'GR');
            }
            return $this->successResponse($booking_data, __("POD uploaded successfully"), true);
        }else{
            return $this->errorResponse(__('Booking not exists.'), true);
        }
    }

    public function reviewRating(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'driver_id' => 'required',
            'rating' => 'required',
        ],['booking_id.required' => __('The booking id field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
           'rating.required' => __('The rating field is required.'),
           
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exists_booking = Booking::where(['id' => $data['booking_id']])->exists();
        if($check_is_exists_booking){
            $check_booking_completed = Booking::where(['id' => $data['booking_id'],'status' => 'completed'])->exists();
            if($check_booking_completed){
                $check_driver_assigned = BookingTruck::where(['booking_id' => $data['booking_id'],'driver_id' => $data['driver_id']])->exists();
                if($check_driver_assigned){
                    $booking_data = Booking::where('id',$data['booking_id'])->first();
                    Booking::where(['id' => $data['booking_id']])->update(['rate_shipper' => '1']);
                    $rating_data_insert['booking_id'] = $data['booking_id'];
                    $rating_data_insert['from_user_id'] = $data['driver_id'];
                    $rating_data_insert['to_user_id'] = $booking_data['user_id'];
                    $rating_data_insert['user_type'] = 'shipper';
                    $rating_data_insert['rating'] = $data['rating'];
                    if(isset($data['review']) && !empty($data['review'])){
                        $rating_data_insert['review'] = $data['review'];
                    }
                    $review_rating_id = ReviewRating::create($rating_data_insert)->id;
                    $review_data = ReviewRating::where('id',$review_rating_id)->first();
                    return $this->successResponse($review_data, __("Review rating added successfully."), true);
                }else{
                    return $this->errorResponse(__('No valid driver for this shipment.'), true);
                }
            }else{
                return $this->errorResponse(__('Please complete a trip.'), true);
            }
        }else{
            return $this->errorResponse(__('Booking not exists.'), true);
        }
    }

    public function cancelRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'driver_id' => 'required',
            'shipper_id' => 'required',
        ],['booking_id.required' => __('The booking id field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
           'shipper_id.required' => __('The shipper id field is required.'),
           
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exists_booking = Booking::where(['id' => $data['booking_id']])->whereIn('status', ['scheduled'])->exists();
        if($check_is_exists_booking){
            $check_driver_exists = BookingTruck::where(['booking_id' => $data['booking_id'], 'driver_id' => $data['driver_id']])->exists();
            if($check_driver_exists){
                $count = CancelBooking::where(['driver_id' => $data['driver_id']])->whereMonth('request_time', date('m'))->whereYear('request_time',date('Y'))->count();
                if($count < 5){
                    SocketHelper::emit('cancel_request',$data);
                    
                    $cancel_request['booking_id'] = $data['booking_id'];
                    $cancel_request['driver_id'] = $data['driver_id'];
                    $cancel_request['shipper_id'] = $data['shipper_id'];
                    $cancel_request['request_time'] = date('Y-m-d h:i:s');
                    $cancel_request['status'] = 0;
                    $cancel_request['cancellation_charge'] = '0';
                    CancelBooking::insert($cancel_request);
                
                    return $this->successResponse([], __("Cancel request sent successfully, please wait for cancelled request accept by shipper."), true);
                }else{
                    return $this->errorResponse(__('Your mothly 5 request limit over.'), true);    
                }
            }else{
                return $this->errorResponse(__('Sorry you can not add cancel request because shipment is not assigned to you.'), true);    
            }
        }else{
            return $this->errorResponse(__('Shipment not exists or it is not in scheduled, so please try another shipment.'), true);
        }
    }

    public function TrashPostedTruck(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'posted_truck_id' => 'required',
        ],['posted_truck_id.required' => __('The posted truck id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exist_post_availability = Availability::where('id',$data['posted_truck_id'])->exists();
        if($check_is_exist_post_availability){
            BookingBid::where('availability_id',$data['posted_truck_id'])->update(['status' => 2]);
            BookingRequest::where('availability_id',$data['posted_truck_id'])->update(['status' => 2]);
            Availability::where('id',$data['posted_truck_id'])->update(['load_status' => 2]);
            return $this->successResponse([], __("Posted truck trashed successfully."), true);
        }else{
            return $this->errorResponse(__('Posted truck not exists, so please try another posted truck.'), true);
        }
    }

    public function cancelBidRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'driver_id' => 'required',
            'shipper_id' => 'required',
        ],['booking_id.required' => __('The booking id field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
           'shipper_id.required' => __('The shipper id field is required.'),
           
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_exists_booking = Booking::where(['id' => $data['booking_id']])->whereIn('status', ['create','pending'])->exists();
        if($check_is_exists_booking){
            $check_is_exists_bid_request = BookingBid::where(['booking_id' => $data['booking_id'],'driver_id' => $data['driver_id']])->exists();
            // $check_driver_exists = BookingTruck::where(['booking_id' => $data['booking_id'], 'driver_id' => $data['driver_id']])->exists();
            if($check_is_exists_bid_request){
                BookingBid::where(['booking_id' => $data['booking_id'],'driver_id' => $data['driver_id']])->update(['status' => 2]);
                return $this->successResponse([], __("Your bid cancellation request sent successfully"), true);
            }else{
                return $this->errorResponse(__('Sorry you can not cancel bid request.'), true);    
            }
        }else{
            return $this->errorResponse(__('Shipment not exists or it is not in pending, so please try another shipment.'), true);
        }
    }

    public function transactionHistory(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
        ],[
           'driver_id.required' => __('The driver id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_driver_exists = DriverVehicle::where('user_id',$data['driver_id'])->exists();
        if($check_is_driver_exists){
            $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
            $transaction_history = Transaction::with(['booking_data'])->where('driver_id',$data['driver_id'])->orderBy('date','desc')->get();
            $transaction_history_response = [];
            if(isset($transaction_history) && !empty($transaction_history)){
                foreach($transaction_history as $key => $transaction){
                    $transaction_history_response[$key] = $transaction->toArray();
                    $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($transaction['booking_data']['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                    $sum = 0;
                    if(isset($trucks) && !empty($trucks)){
                        $transaction_history_response[$key]['booking_data']['trucks'] = $trucks->toArray();
                        $sum = $this->weight_sum($transaction_history_response[$key]['booking_data']['trucks']['locations']);
                    }
                    $unit = isset($transaction_history_response[$key]['booking_data']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $transaction_history_response[$key]['booking_data']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                    $transaction_history_response[$key]['booking_data']['total_weight'] = $sum." ".$unit;
                    $transaction_history_response[$key]['booking_data']['amount'] = CommonHelper::price_format($transaction_history_response[$key]['booking_data']['amount'],'GR');
                }
            }
            // echo '<pre>';print_r($transaction_history_response);die();
            return $this->successResponse($transaction_history_response, __("Transaction List"), true);
        }else{
            return $this->errorResponse(__('User not exists.'), true);
        }
    }

    public function acceptPayment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'driver_id' => 'required',
        ],[
           'booking_id.required' => __('The booking id field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_booking_is_exists = Booking::where(['id' => $data['booking_id'],'status' => 'completed'])->first();
        if(isset($check_booking_is_exists) && !empty($check_booking_is_exists)){
            $check_driver_assigned_for_booking = BookingTruck::where(['booking_id' => $data['booking_id'],'driver_id' => $data['driver_id']])->first();
            if(isset($check_driver_assigned_for_booking) && !empty($check_driver_assigned_for_booking)){
                if($check_booking_is_exists->payment_status != 'completed'){
                    $transaction_data['booking_id'] = $data['booking_id'];
                    $transaction_data['driver_id'] = $data['driver_id'];
                    $transaction_data['amount'] = CommonHelper::price_format($check_booking_is_exists->amount,'GR');
                    $transaction_data['transaction_type'] = 'cash';
                    $transaction_data['status'] = 'completed';
                    $transaction_data['date'] = date('Y-m-d');
                    $transaction_id = Transaction::create($transaction_data)->id;
                    $update_booking['payment_status'] = 'completed';
                    $update_booking['payment_type'] = 'cash';
                    $update_booking['transaction_id'] = $transaction_id;
                    Booking::where(['id' => $data['booking_id']])->update($update_booking);
                    return $this->successResponse([], __("Payment completed successfully"), true);
                }else{
                    return $this->errorResponse(__('Shipment payment already completed.'), true);        
                }
            }else{
                return $this->errorResponse(__('Shipment not assigned for you, please try another shipment.'), true);    
            }
        }else{
            return $this->errorResponse(__('Shipment not exists or shipment not completed.'), true);
        }
    }
    public function myloadcount(Request $request)
    {
         $driver_id=$request->driver_id;

        $booking=BookingTruck::where('driver_id',$driver_id)
                            ->where('pickup_date', '>', Carbon::now()->startOfWeek())
                            ->where('pickup_date', '<', Carbon::now()->endOfWeek())
                            ->join('booking','booking_trucks.booking_id','=','booking.id')
                            ->selectRaw('WEEK(pickup_date) as week,count(booking_id) as bookingData,sum(distance) as distancesum,sum(pickup_lat) as pickupdata,sum(deadhead) as deadheaddata')
                            ->groupBy('week')
                            ->get();
            
                        foreach($booking->toArray() as $row)
                        {   
                            $output[] = array(
                            'LOADS/ WEEK'=>$row['bookingData'],
                            'KM/ WEEK'=>$row['bookingData']*1000/$row['distancesum'],
                            'DEADHEAD'=>abs($row['pickupdata'] - $row['deadheaddata'])
                            );
                        }
                if(isset($output)){
                return $this->successResponse($output,__('My Loads Count'),true);
            }else{
             return $this->errorResponse(__('something has wrong, please try again later'),true);
        }
         
        
    }

    public function statistic(Request $request){
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
        ],[
           'driver_id.required' => __('The driver id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_driver_exists = DriverVehicle::where('user_id',$data['driver_id'])->exists();
        if($check_is_driver_exists){
            $booking = BookingTruck::where('driver_id',$data['driver_id'])
            ->where('pickup_date', '>', Carbon::now()->startOfWeek())
            ->where('pickup_date', '<', Carbon::now()->endOfWeek())
            ->join('booking','booking_trucks.booking_id','=','booking.id')
            ->selectRaw('WEEK(pickup_date) as week,count(booking_id) as bookingData,sum(distance) as distancesum,sum(amount) as totalearning,sum(deadhead) as deadheaddata')
            ->groupBy('week')
            ->first();
            // echo '<pre>';print_r($booking);die();

            $booking_location=BookingLocations::join('booking','booking_locations.booking_id','=','booking.id')
                                                ->where('user_id',$data['driver_id'])
                                                ->pluck('booking_id')
                                                ->toArray();
                foreach($booking_location as $key=>$value)
                    {
                        $book_location_lat=BookingLocations::all();
                        foreach($book_location_lat as $key=>$value)
                        {
                        $lat=$value->drop_lat;
                        $lng=$value->drop_lng;
                        $nearest_lat=DB::table("booking_locations")
                        ->select("booking_locations.id","booking_locations.booking_id"
                        ,DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
                        * cos(radians(booking_locations.drop_lat)) 
                        * cos(radians(booking_locations.drop_lng) - radians(" . $lng . ")) 
                        + sin(radians(" .$lat. ")) 
                        * sin(radians(booking_locations.drop_lat))) AS distanceData"))
                        ->groupBy("booking_locations.id")
                        ->whereIn('booking_id',$booking_location)
                        ->orderBy('id','DESC')
                        ->first();

                        $dataamount=Booking::where('id',$nearest_lat->booking_id)->where('status','completed')->first();
                
                            if($dataamount===null)
                                {
                                    $amount=0;
                                }
                                else
                                {
                                    $amount=$dataamount->amount;  
                                }
                
                        }
    
                    }
            $transaction_history = Transaction::where('driver_id',$data['driver_id'])->orderBy('date','desc')->get();
            $total_earning = 0;
            if(isset($transaction_history) && !empty($transaction_history)){
                foreach($transaction_history as $transaction){
                    $total_earning += str_replace(',','',$transaction['amount']);
                }
            }
            $response['name'] = 'EARNING'; 
            $response['value'] = '€'.round($total_earning,2); 
            $response['color'] = '579F2B'; 
            $response['statistic_type'] = '0'; 
            $final_response[] = $response;
            $response['name'] = 'LOADS/WEEK'; 
            $response['value'] = isset($booking->bookingData) && !empty($booking->bookingData) ? "".$booking->bookingData : '0.00'; 
            $response['color'] = '9C4AE3'; 
            $response['statistic_type'] = '1';
            $final_response[] = $response; 
            $km_per_week = '0';
            if(isset($booking->distancesum) && !empty($booking->distancesum) && $booking->distancesum != 0){
                $km_per_week = "".$booking->bookingData * 1000 / $booking->distancesum;
            }

            $response['name'] = '/KM/WEEK';
            $response['value'] = "".round($km_per_week,2);
            $response['color'] = 'DCA62A';
            $response['statistic_type'] = '2';
            $final_response[] = $response;

            $response['name'] = 'DEADHEAD';
            $response['value'] = isset($booking->deadheaddata) && !empty($booking->deadheaddata) ? "".round($booking->deadheaddata,2) : "0.00";
            $response['color'] = 'D76867';
            $response['statistic_type'] = '3';
            $final_response[] = $response;

             $response['name'] = 'EUR CARBON SAVED';
             $response['value'] =isset($amount) && !empty($amount) ? "".round($amount,2) : "0.00";;
             $response['color'] = '5194e0';
             $response['statistic_type'] = '4';
             $final_response[] = $response;
            return $this->successResponse($final_response, __("Statisctics List"), true);
        }else{
            return $this->errorResponse(__('User not exists.'), true);
        }
    }

    public function statisticDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'statistic_type' => 'required',
            'driver_id' => 'required',
        ],[
           'statistic_type.required' => __('The statistics type field is required.'),
           'driver_id.required' => __('The driver id field is required.'),
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_is_driver_exists = DriverVehicle::where('user_id',$data['driver_id'])->exists();
        if($check_is_driver_exists){
            if($data['statistic_type'] == 0){
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                $transaction_history = Transaction::with(['booking_data'])->where('driver_id',$data['driver_id'])->orderBy('date','desc')->get();
                $transaction_history_response = [];
                if(isset($transaction_history) && !empty($transaction_history)){
                    foreach($transaction_history as $key => $transaction){
                        $converted_to_array = $transaction->toArray();
                        $transaction_history_response[$key] = $converted_to_array['booking_data'];
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($transaction['booking_data']['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $transaction_history_response[$key]['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($transaction_history_response[$key]['trucks']['locations']);
                        }
                        $unit = isset($transaction_history_response[$key]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $transaction_history_response[$key]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $transaction_history_response[$key]['total_weight'] = $sum." ".$unit;
                    }
                }
                // echo '<pre>';print_r($transaction_history_response);die();
                return $this->successResponse($transaction_history_response, __("Transaction List"), true);
            }else{
                $booking_ids=BookingTruck::where('driver_id',$data['driver_id'])
                ->where('pickup_date', '>', Carbon::now()->startOfWeek())
                ->where('pickup_date', '<', Carbon::now()->endOfWeek())
                ->join('booking','booking_trucks.booking_id','=','booking.id')
                ->pluck('booking.id')->toArray();

                $booking_data = [];
                if(isset($booking_ids) && !empty($booking_ids)){
                    $booking_data = Booking::whereIn('id',$booking_ids)->get()->toArray();
                    $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                    if(isset($booking_data) && !empty($booking_data)){
                        foreach($booking_data as $key => $value){
                            $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($value['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                            $sum = 0;
                            if(isset($trucks) && !empty($trucks)){
                                $booking_data[$key]['trucks'] = $trucks->toArray();
                                $sum = $this->weight_sum($booking_data[$key]['trucks']['locations']);
                            }
                            $unit = isset($booking_data[$key]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $booking_data[$key]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                            $booking_data[$key]['total_weight'] = $sum." ".$unit;
                        }
                    }
                }
                return $this->successResponse($booking_data,__('Statistics Details'),true);
            }
        }else{
            return $this->errorResponse(__('User not exists.'), true);
        }
    }

    public function contactUs()
    {
        $response['call'] = '+301234567890';
        $user_data = User::where('email','admin@admin.com')->first();
        $chat_data['id'] = $user_data['id'];
        $chat_data['name'] = $user_data['name'];
        $chat_data['profile'] = $user_data['profile'];
        $response['chat'] = $chat_data;
        return $this->successResponse($response,__('Contact Detail'),true);
    }
}