<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CommonHelper as HelpersCommonHelper;
use App\Http\Controllers\Controller;
use App\Models\Availability;
use App\Models\Booking;
use App\Models\BookingBid;
use App\Models\BookingTruck;
use App\Models\DriverVehicle;
use App\Models\BookingLocations;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Twilio\Tests\Request as TestsRequest;
use App\Models\User;
use App\Models\UserRole;
use Booking as GlobalBooking;
use NumberFormatter;
use SocketHelper;
use CommonHelper;

class PostTruckController extends Controller {
    use ApiResponser;

    public function postTruck(Request $request) {
        $validator = Validator::make($request->all(), [
            'truck_type_id' => 'required',
            'driver_id' => 'required',
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|date_format:H:i:s',
            'start_lat' => 'required',
            'start_lng' => 'required',
            'end_lat' => 'nullable',
            'end_lng' => 'nullable',
            'is_bid' => 'required|in:1,0',
            'bid_amount' => 'nullable',

        ],['truck_type_id.required' => __('The truck type id field is required.'), 
            'driver_id.required' => __('The driver id field is required.'),  
            'date.required' => __('The date field is required.'),  
            'time.required' => __('The time field is required.'),  
            'start_lat.required' => __('The start lat field is required.'),  
            'start_lng.required' => __('The start lng field is required.'),  
            'end_lat.required' => __('The end lat field is required.'),  
            'end_lng.required' => __('The end lng field is required.'),  
            'is_bid.required' => __('The is bid field is required.'),  
            'bid_amount.required' => __('The bid amount field is required.'),  
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        
        $booking_already_scheduled = BookingTruck::where(['booking_trucks.driver_id' => $data['driver_id']])
            // ->where(DB::raw('CONCAT(booking_locations.delivered_at," ",booking_locations.delivery_time_to)'),'>=',date('Y-m-d H:i A',strtotime($data['date'].' '.$data['time'])))
            ->whereNotIn('booking.status',['cancelled','pending','completed'])
            ->join('booking','booking.id','=','booking_trucks.booking_id')
            ->join('booking_locations','booking_locations.booking_id','=','booking_trucks.booking_id')
            ->get()->toArray();
        if(isset($booking_already_scheduled) && !empty($booking_already_scheduled)){
            foreach($booking_already_scheduled as $booking){
                if(isset($booking['locations']) && !empty($booking['locations'])){
                    foreach($booking['locations'] as $location){
                        if($location['is_pickup'] == '1' && $location['delivered_at'].' '.$location['delivery_time_from'] == date('Y-m-d H:i A',strtotime($data['date'].' '.$data['time'])) ){
                            return $this->errorResponse(__("You could not add post availability because, you are already added same post."), true);
                        }
                    }
                }
            }
        }

        if(isset($data['start_lng']) && !empty($data['start_lng']) && isset($data['end_lat']) && !empty($data['end_lng'])  && $data['end_lng'] != 0.0){
            $select = [DB::raw("6371 * acos(cos(radians(" . $data['start_lat'] . "))
            * cos(radians(start_lat)) * cos(radians(start_lng) - radians(" . $data['start_lng'] . "))
            + sin(radians(" . $data['start_lat'] . ")) * sin(radians(start_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $data['end_lat'] . "))
            * cos(radians(end_lat)) * cos(radians(end_lng) - radians(" . $data['end_lng'] . "))
            + sin(radians(" . $data['end_lat'] . ")) * sin(radians(end_lat))) AS drop_distance")];
        }else{
            $select = [DB::raw("6371 * acos(cos(radians(" . $data['start_lat'] . "))
            * cos(radians(start_lat)) * cos(radians(start_lng) - radians(" . $data['start_lng'] . "))
            + sin(radians(" . $data['start_lat'] . ")) * sin(radians(start_lat))) AS pickup_distance")];
        }

        $is_already_set_availability1 = Availability::select($select)->where(['truck_type_id' => $data['truck_type_id'],'driver_id' => $data['driver_id'],'date' => $data['date'],'load_status' => '0'])->having('pickup_distance','<','50');
        if(isset($data['end_lng']) && !empty($data['end_lng'])  && $data['end_lng'] != 0.0){
            $is_already_set_availability1->having('drop_distance','<','50');
        }
        $is_already_set_availability = $is_already_set_availability1->get()->toArray();
        if(isset($is_already_set_availability) && !empty($is_already_set_availability) && count($is_already_set_availability) > 0){
            return $this->errorResponse(__("You already added post availability for same locations, please try again with different locations."), true);
        }

        $check_already_exists = Availability::where(['date' => $data['date'],'load_status' => '0','time' => $data['time'],'start_lat' => $data['start_lat'],'start_lng' => $data['start_lng'],'end_lat' => !empty($data['end_lat']) && isset($data['end_lat']) ? $data['end_lat'] : '','end_lng' => !empty($data['end_lng']) && isset($data['end_lng']) ? $data['end_lng'] : ''])->exists();
            
        if(!$check_already_exists){
            SocketHelper::emit('post_availability_push',$data);
            $availability = new Availability();
            $availability->truck_type_id = $data['truck_type_id'];
            $availability->driver_id = $data['driver_id'];
            $availability->date = $data['date'];
            $availability->time = $data['time'];
            $availability->start_lat = $data['start_lat'];
            $availability->start_lng = $data['start_lng'];
            if(isset($data['start_address']) && !empty($data['start_address'])){
                $availability->from_address = $data['start_address'];
            }
            if(isset($data['end_address']) && !empty($data['end_address'])){
                $availability->to_address = $data['end_address'];
            }
            if(isset($data['end_lat']) && !empty($data['end_lat'])){
                $availability->end_lat = $data['end_lat'];
            }
            if(isset($data['end_lng']) && !empty($data['end_lng'])){
                $availability->end_lng = $data['end_lng'];
            }
            $availability->is_bid = $data['is_bid'];
            $amount = 0;
            if(isset($data['bid_amount']) && !empty($data['bid_amount'])){
                $amount = $data['bid_amount'];
            }
            $availability->bid_amount = $amount;
            $availability->save();

            $drop_location = '0';
            if(isset($data['end_lng']) && !empty($data['end_lng']) && $data['end_lng'] != 0.0){
                $drop_location = '1';
            }
            // echo '<pre>';print_r($drop_location);die();
            #FIND_NEARBY_LOCATION
            $matches = $this->nearByLocationBidBook($availability,$drop_location);

            $final_matches = [];
            if(isset($matches) && !empty($matches)){
                foreach ($matches->toArray() as $row1) {
                    // if(($row1['bid_driver'] != $data['driver_id']) ||  $row1['bid_driver']== null)
                    // {
                    //     $final_matches[] = $row1; 
                    // }
                    $check_bid = BookingBid::whereBookingId($row1['id'])->whereDriverId($data['driver_id'])->where(['status' => '0'])->first();
                    if(!$check_bid){
                        $final_matches[] = $row1;
                    }
                }
            }
    
            $count_matches = count($final_matches);
            $availability['count'] = $count_matches;
            $availability['booking_request_count'] = $count_matches;
            $availability['matches'] = $count_matches . ' matches have been found, would you like to view them ?';
            
            

            return $this->successResponse($availability, __("Availability Post Successfully"), true);
            // }else{
            //     return $this->errorResponse(__("You could not add post availability because, you are already added same post."), true);    
            // }
        }else{
            return $this->errorResponse(__("You can not add post availability because, you already scheduled a shipment, so please complete your trip and try again later."), true);
        }

    }

    public function get_distance($lat1,$lng1,$lat2,$lng2)
    {
        echo '<pre>';print_r($lat1);
        echo '<pre>';print_r($lng1);
        echo '<pre>';print_r($lat2);
        echo '<pre>';print_r($lng2);die();
        $theta = $lng1 - $lng2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        return $miles;
    }

    public function nearByLocation($driver,$drop_location = '0') {

        $current = Carbon::now()->startOfDay();

        if($drop_location == '1'){
            $select = ["booking.*", 'booking_bid.driver_id as bid_driver', DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $driver['end_lat'] . "))
            * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $driver['end_lng'] . "))
            + sin(radians(" . $driver['end_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
        }else{
            $select = ["booking.*", "booking_bid.driver_id as bid_driver", DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
        }

        $matches = Booking::distinct()
            ->select($select)
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')
            ->leftJoin('booking_bid', function ($join) {
                $join->on('booking.id', '=', 'booking_bid.booking_id');
                $join->where(['booking_bid.status' => 0]);
            })
            // ->leftJoin('booking_bid', 'booking.id', 'booking_bid.booking_id')
            // ->where('booking_locations.delivered_at', '<' , $current)
            ->where('booking_trucks.truck_type', $driver['truck_type_id'])
            // ->where('booking_bid.driver_id','!=', $driver['driver_id'])
            ->where('booking.pickup_date','=',$driver['date'])
            ->where('booking.is_bid',$driver['is_bid'])
            ->where('booking.status','pending')
            ->where('booking.is_public','1')
            ->having('pickup_distance', '<', 50)
            ->orderBy('pickup_date', 'asc');

            if($drop_location == '1')
            {
                $matches->having('drop_distance', '<', 50);
                $matches->having('drop_distance', '>=', 0);
            }


        return $matches->get();

    }
    
    public function nearByLocationBidBook($driver,$drop_location = '0') {

        $current = Carbon::now()->startOfDay();

        if($drop_location == '1'){
            $select = ["booking.*", 'booking_bid.driver_id as bid_driver', DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $driver['end_lat'] . "))
            * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $driver['end_lng'] . "))
            + sin(radians(" . $driver['end_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
        }else{
            $select = ["booking.*", "booking_bid.driver_id as bid_driver", DB::raw("6371 * acos(cos(radians(" . $driver['start_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $driver['start_lng'] . "))
            + sin(radians(" . $driver['start_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
        }

        $matches = Booking::distinct()
            ->select($select)
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')
            ->leftJoin('booking_bid', function ($join) {
                $join->on('booking.id', '=', 'booking_bid.booking_id');
                $join->where(['booking_bid.status' => 0]);
            })
            // ->leftJoin('booking_bid', 'booking.id', 'booking_bid.booking_id')
            // ->where('booking_locations.delivered_at', '<' , $current)
            ->where('booking_trucks.truck_type', $driver['truck_type_id'])
            // ->where('booking_bid.driver_id','!=', $driver['driver_id'])
            ->where('booking.pickup_date','=',$driver['date'])
            // ->where('booking.is_bid',$driver['is_bid'])
            ->where('booking.status','pending')
            ->where('booking.is_public','1')
            ->having('pickup_distance', '<', 50)
            ->orderBy('pickup_date', 'asc');

            if($drop_location == '1')
            {
                $matches->having('drop_distance', '<', 50);
                $matches->having('drop_distance', '>=', 0);
            }


        return $matches->get();

    }
    
    public function nearByLocationSearch($data,$drop_location = '0') {
        // echo '<pre>';print_r($data['price_max']);die();
        $current = Carbon::now()->startOfDay();

        if($drop_location == '1' && !empty($data['pickup_lat']) && !empty($data['pickup_lng']) && !empty($data['delivery_lat']) && !empty($data['delivery_lng'])){
            $select = ["booking.*", DB::raw("6371 * acos(cos(radians(" . $data['pickup_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $data['pickup_lng'] . "))
            + sin(radians(" . $data['pickup_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $data['delivery_lat'] . "))
            * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $data['delivery_lng'] . "))
            + sin(radians(" . $data['delivery_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
        }elseif(!empty($data['pickup_lat']) && !empty($data['pickup_lng'])){
            $select = ["booking.*", DB::raw("6371 * acos(cos(radians(" . $data['pickup_lat'] . "))
            * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $data['pickup_lng'] . "))
            + sin(radians(" . $data['pickup_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
        }else{
            $select = ["booking.*"];
        }

        $matches = Booking::distinct()
            ->select($select)
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')            
            ->where(function($query) use ($data)  {
                $date = date('Y-m-d',strtotime($data['date']));
                $query->where('booking.status','pending');
                $query->where('booking.is_public','1');
                $query->where('booking_trucks.truck_type', $data['truck_type_id']);
                
                if(isset($data['price_min']) && !empty($data['price_min'])) {
                    $query->whereBetween('booking.amount',[$data['price_min'],$data['price_max']]);
                }
                $query->whereDate('pickup_date',$date);
             });
            if(isset($data['pickup_lat']) && !empty($data['pickup_lat']) && isset($data['pickup_lng']) && !empty($data['pickup_lng'])){
                $matches = $matches->having('pickup_distance', '<', 50);
            }
            if(isset($data['delivery_lat']) && !empty($data['delivery_lat']) && isset($data['delivery_lng']) && !empty($data['delivery_lng'])){
                $matches = $matches->having('drop_distance', '<', 50);
            }
            $matches = $matches->orderBy('pickup_date', 'asc');
            $matches = $matches->get();
        return $matches;

    }

    public function driverFind($truck_type, $date) {

        $current = Carbon::now()->startOfDay();
        $drivers = Availability::select('*')
            ->where('truck_type_id', $truck_type)
            ->where(DB::raw('CONCAT(date, " ", time)'), '<', $current)
            ->get();

        return $drivers;

    }

    public function AvailabilityResult(Request $request) {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'availability_id' => 'required',
        ],['driver_id.required' => __('The driver id field is required.'), 
            'availability_id.required' => __('The availability id field is required.'),  
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $availability = Availability::whereDriverId($request->driver_id)->whereId($request->availability_id)->where('date', '>=', date('Y-m-d'))->get();
        $loads = [];
        if (!empty($availability)) {
            $i = 0;
            foreach ($availability as $value) {
                if(isset($value['end_lng']) && !empty($value['end_lng']) && $value['end_lng'] != '0.0'){
                    $mathch = $this->nearByLocationBidBook($value,'1');
                }else{
                    $mathch = $this->nearByLocationBidBook($value,'0');
                }
                foreach ($mathch->toArray() as $row) {
                    if(($row['bid_driver'] != $value->driver_id) ||  $row['bid_driver']== null)
                    {
                        $row['availability_id'] = $value['id'];
                        $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
                        $loads[$i] = $this->change_key($row, 'pickup_date', 'date');

                        $loads[$i]['trucks'] = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($value->truck_type_id)->first()->toArray();

                        $check_bid = BookingBid::whereBookingId($row['id'])->whereDriverId($request->driver_id)->where(['status' => '0'])->first();
                        $loads[$i]['driver_bid'] = 0;
                        if(!empty($check_bid))
                        {
                            $loads[$i]['driver_bid'] = 1;
                        }
                        $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                        $loads[$i]['total_weight'] = $sum." ".$loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'];
                        $loads[$i]['amount'] = CommonHelper::price_format($loads[$i]['amount'],'GR');
                        $i++;
                    }
                }
            }
            $loads = $this->bindShipment($loads);

            // $loads_final = $this->group_by($loads,'pickup_date');
            return $this->successResponse($loads, __("Data Get Successfully"), true);
        }else{
            return $this->errorResponse(__("No data found."), true);
        }
    }

    public function searchLoadsBKP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'page_num' => 'required',
        ],['driver_id.required' => __('The driver id field is required.'), 
            'page_num.required' => __('The page num field is required.'),  
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $is_exists = User::whereId($data['driver_id'])->exists();
        $is_exists_driver_role = UserRole::where(['user_id' => $data['driver_id'],'role_id' => '3'])->exists();
        if($is_exists && $is_exists_driver_role){
            $driver_details = User::whereId($data['driver_id'])->first()->toArray();
            $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first()->toArray();
            $limit = 10;
            $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
            $skip = ($page - 1) * $limit;
            $matches = Booking::distinct()
            ->select("booking.*")
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')            
            ->where(function($query) use ($driver_vehicle_details)  {
                $query->where('booking.status','pending');
                $query->where('booking_trucks.truck_type', $driver_vehicle_details['truck_type']);
             });
            
            $matches = $matches->orderBy('pickup_date', 'desc');
            // $matches = $matches->skip($skip);
            // $matches = $matches->limit($limit);
            $matches = $matches->get()->toArray();
            $i = 0;
            $loads = [];
            foreach ($matches as $row) {
                $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');

                $loads[$i]['trucks'] = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details['truck_type'])->first()->toArray();

                $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'])?$loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']:"";
                $loads[$i]['total_weight'] = $sum." ".$unit;
                $i++;
            }
            $loads = $this->bindShipment($loads);
            $loads_final = $this->group_by($loads,'pickup_date');
            return $this->successResponse($loads_final, __("success"), true);

        }else{
            return $this->errorResponse(__("No carrier found."), true);
        }

    }

    public function searchLoads(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'page_num' => 'required',
            // 'journey_type' => 'required',
        ],['driver_id.required' => __('The driver id field is required.'), 
            'page_num.required' => __('The page num field is required.'),  
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $is_exists = User::whereId($data['driver_id'])->exists();
        $is_exists_driver_role = UserRole::where(['user_id' => $data['driver_id'],'role_id' => '3'])->exists();
        if($is_exists && $is_exists_driver_role){
            $driver_details = User::whereId($data['driver_id'])->first()->toArray();
            $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first()->toArray();
            if($data['page_num'] == '1' || $data['page_num'] == '0'){
                $limit = 11;
            }else{
                $limit = 10;
            }
            
            $status = "pending";
            $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
            $skip = ($page - 1) * $limit;
            
            $select = ["booking.*"];
            if(isset($data['pickup_lat']) && !empty($data['pickup_lat']) && isset($data['pickup_lng']) && !empty($data['pickup_lng']) && isset($data['dropoff_lat']) && !empty($data['dropoff_lat']) && isset($data['dropoff_lng']) && !empty($data['dropoff_lng'])){
                $select = ["booking.*",DB::raw("6371 * acos(cos(radians(" . $data['pickup_lat'] . "))
                * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $data['pickup_lng'] . "))
                + sin(radians(" . $data['pickup_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance"), DB::raw("6371 * acos(cos(radians(" . $data['dropoff_lat'] . "))
                * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $data['dropoff_lng'] . "))
                + sin(radians(" . $data['dropoff_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
            }elseif(isset($data['pickup_lat']) && !empty($data['pickup_lat']) && isset($data['pickup_lng']) && !empty($data['pickup_lng'])){
                $select = ["booking.*",DB::raw("6371 * acos(cos(radians(" . $data['pickup_lat'] . "))
                * cos(radians(pickup_lat)) * cos(radians(pickup_lng) - radians(" . $data['pickup_lng'] . "))
                + sin(radians(" . $data['pickup_lat'] . ")) * sin(radians(pickup_lat))) AS pickup_distance")];
            }elseif(isset($data['dropoff_lat']) && !empty($data['dropoff_lat']) && isset($data['dropoff_lng']) && !empty($data['dropoff_lng'])){
                $select = ["booking.*",DB::raw("6371 * acos(cos(radians(" . $data['dropoff_lat'] . "))
                * cos(radians(drop_lat)) * cos(radians(drop_lng) - radians(" . $data['dropoff_lng'] . "))
                + sin(radians(" . $data['dropoff_lat'] . ")) * sin(radians(drop_lat))) AS drop_distance")];
            }
            if(isset($data['journey_type']) && !empty($data['journey_type'])){
                if($data['journey_type'] == 2){
                    $data['journey_type'] = 'Long haul shipment';
                }elseif($data['journey_type'] == 1){
                    $data['journey_type'] = 'Medium haul shipment';
                }else{
                    $data['journey_type'] = 'Short haul shipment';
                }

            }
            // echo '<pre>';print_r($data);die();
            // echo '<pre>';print_r($skip);
            // echo '<pre>';print_r($limit);
            $matches = Booking::distinct()
            ->select($select)
            ->join('booking_locations', 'booking.id', 'booking_locations.booking_id')
            ->join('booking_trucks', 'booking.id', 'booking_trucks.booking_id')
                     
            ->where(function($query) use ($driver_vehicle_details,$data)  {
                // $query->where('booking.status',"pending");
                $query->where('booking_trucks.truck_type', $driver_vehicle_details['truck_type']);
                $query->where('booking.pickup_date','>=', date('Y-m-d'));
                if(isset($data['pickup_date']) && !empty($data['pickup_date'])){
                    $query->where('booking.pickup_date',$data['pickup_date']);
                }
                if(isset($data['min_price']) && !empty($data['min_price'])){
                    $query->where('booking.amount','>=',$data['min_price']);
                }
                if(isset($data['max_price']) && !empty($data['max_price'])){
                    $query->where('booking.amount','<=',$data['max_price']);
                }
                if(isset($data['journey_type']) && !empty($data['journey_type'])){
                    $query->where('booking.journey_type','=',$data['journey_type']);
                }
             })->where('booking.status','=',"$status")->where('booking.is_public','1');
            if(isset($data['pickup_lat']) && !empty($data['pickup_lat']) && isset($data['pickup_lng']) && !empty($data['pickup_lng']) && isset($data['dropoff_lat']) && !empty($data['dropoff_lat']) && isset($data['dropoff_lng']) && !empty($data['dropoff_lng'])){
                $matches = $matches->having('pickup_distance', '<', 50);
                $matches = $matches->having('drop_distance', '>=', 0);
                $matches = $matches->having('drop_distance', '<', 50);
            }elseif(isset($data['pickup_lat']) && !empty($data['pickup_lat']) && isset($data['pickup_lng']) && !empty($data['pickup_lng'])){
                $matches = $matches->having('pickup_distance', '<', 50);
            }elseif(isset($data['dropoff_lat']) && !empty($data['dropoff_lat']) && isset($data['dropoff_lng']) && !empty($data['dropoff_lng'])){
                $matches = $matches->having('drop_distance', '>=', 0);
                $matches = $matches->having('drop_distance', '<', 50);
            }
            if(isset($data['price_sort']) && !empty($data['price_sort'])){
                $matches = $matches->orderBy('amount',$data['price_sort']);
            }
            if(isset($data['total_distance_sort']) && !empty($data['total_distance_sort'])){
                $matches = $matches->orderBy('distance',$data['total_distance_sort']);
            }
            $matches = $matches->orderBy('pickup_date', 'asc');
            $matches = $matches->skip($skip);
            $matches = $matches->take($limit);
            $matches = $matches->get()->toArray();
            $i = 0;
            $loads = [];
            foreach ($matches as $row) {
                $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
                $loads[$i] = $this->change_key($row, 'pickup_date', 'date');
                $loads[$i]['amount'] = CommonHelper::price_format($row['amount'],'GR');

                $loads[$i]['trucks'] = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details['truck_type'])->first()->toArray();

                $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'])?$loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']:"";
                $loads[$i]['total_weight'] = $sum." ".$unit;
                
                $check_bid = BookingBid::whereBookingId($row['id'])->whereDriverId($data['driver_id'])->where('status',0)->first();
                if(!empty($check_bid))
                {
                    unset($loads[$i]);
                }
                $i++;
            }
            $loads_final = $this->bindShipment($loads);
            if(isset($data['rating_sort']) && !empty($data['rating_sort'])){
                $rating = array();
                foreach ($loads_final as $key => $row)
                {
                    $rating[$key] = $row['shipper_details']['shipper_rating'];
                }
                if($data['rating_sort'] == 'asc'){
                    array_multisort($rating,SORT_ASC, $loads_final);
                }else{
                    array_multisort($rating,SORT_DESC, $loads_final);
                }
            }
            return $this->successResponse($loads_final, __("Data Get Successfully"), true);

        }else{
            return $this->errorResponse(__("No carrier found."), true);
        }

    }

    public function searchLoads_bkp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'truck_type_id' => 'required',
        ],['date.required' => __('The date field is required.'), 
            'truck_type_id.required' => __('The truck type id field is required.'),  
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $drop_location = '0';
        if(!empty($data['delivery_lat']) && !empty($data['delivery_lng'])){
            $drop_location = '1';
        }
        $mathch = $this->nearByLocationSearch($request,$drop_location);
        // echo '<pre>';print_r($mathch);die();
        $i = 0;
        $loads = [];
        foreach ($mathch->toArray() as $row) {
            $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');

            $loads[$i]['trucks'] = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($data['truck_type_id'])->first()->toArray();

            $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
            $loads[$i]['total_weight'] = $sum." ".$loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'];
            if(isset($data['weight_min']) && !empty($data['weight_min']) && isset($data['weight_max']) && !empty($data['weight_max'])){
                if($sum >= $data['weight_min'] && $sum <= $data['weight_max']){
                }else{
                    unset($loads[$i]);
                }
            }
            $i++;
        }
        $loads = $this->bindShipment($loads);
        $loads_final = $this->group_by($loads,'pickup_date');
        return $this->successResponse($loads_final, __("success"), true);

    }

    function change_key( $array, $old_key, $new_key ) {

        if( ! array_key_exists( $old_key, $array ) )
            return $array;
    
        $keys = array_keys( $array );
        $keys[ array_search( $old_key, $keys ) ] = $new_key;
    
        return array_combine( $keys, $array );
    }

    public function bindShipment($shipmnts) {
        $unique_array = [];
        foreach ($shipmnts as $element) {
            $hash = $element['id'];
            $unique_array[$hash] = $element;
        }
        $result = array_values($unique_array);
        return $result;
    }

    function group_by($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val[$key]]['date'] = $val[$key];
            $return[$val[$key]]['bids_data'][] = $val;
        }
        $final = [];
        foreach ($return as $key => $value) {
            $final[]=$value;
        }
        return $final;
    }

    function group_by_new($array, $key) {
        $return = array();
        foreach ($array as $k => $val) {
            // $return[$val[$key]]['date'] = $val[$key];
            $val['date'] = $val[$key];
            $return[$val[$key]]['bids_data'][] = $val;
            // $return[] = $val;
        }
        $final = [];
        foreach ($return as $key => $value) {
            $final[]=$value;
        }
        return $final;
    }

    public function calculate_distance($pick_location,$drop_location)
    {
        if ($pick_location != '' && $drop_location != '') {
            $distance_url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . urlencode($pick_location) . "&destinations=" . urlencode($drop_location) . "&mode=driving&key=" . env('MAP_KEY');

            $distance = (array) json_decode(file_get_contents($distance_url));
            $km = (Float) $distance['rows'][0]->elements[0]->distance->value;
            $km = $km / 1000;
            $duration = (Float) $distance['rows'][0]->elements[0]->duration->value;
            $duration = $duration / 60;
            $duration = $duration / 60;
            $data['miles'] = round($km * 0.621371); // kilometer to miles conversion
            $data['journey'] = round($duration);
        } else {
            $data['miles'] = 0;
            $data['journey'] = 0;
        }
        return $data;
    }

    public function post_bid(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'driver_id' => 'required',
            'amount' => 'required',
        ],['booking_id.required' => __('The booking id field is required.'), 
            'driver_id.required' => __('The driver id field is required.'),  
            'amount.required' => __('The amount field is required.'),  
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }
        $data = $request->all();
        $check_booking_is_not_assign = Booking::where(['id' => $data['booking_id'], 'status' => 'pending'])->exists();
        if($check_booking_is_not_assign){
            $booking_data = Booking::with(['booking_location'])->where(['id' => $data['booking_id']])->first()->toArray();
            $data['shipper_id'] = $booking_data['user_id'];
            // echo '<pre>';print_r($data);die();
            if(isset($booking_data) && !empty($booking_data)){
                $first_key = array_key_first($booking_data['booking_location']);
                $last_key = array_key_last($booking_data['booking_location']);
                // echo '<pre>';print_r($booking_data);die();
                $check_driver_is_free = BookingTruck::where(['driver_id' => $data['driver_id']])
                ->select(['booking_trucks.*','booking.id as booking_id','booking.status as booking_status','booking.pickup_date as booking_pickup_date',DB::raw("CONCAT(booking_locations.delivered_at,' ',booking_locations.delivery_time_to) AS full_date")])
                ->join('booking','booking.id','=','booking_trucks.booking_id')
                ->join('booking_locations','booking_locations.booking_id','=','booking_trucks.booking_id')
                ->whereIn('booking.status',['scheduled','in-progress'])
                ->get()->toArray();
                if(isset($check_driver_is_free) && !empty($check_driver_is_free)){
                    foreach($check_driver_is_free as $driver){
                        // echo '<pre>';print_r($driver);
                        $new_first_key = array_key_first($driver['locations']);
                        $new_last_key = array_key_last($driver['locations']);
                        $start_date = date('Y-m-d H:i',strtotime($driver['locations'][$new_first_key]['delivered_at'].' '.$driver['locations'][$new_first_key]['delivery_time_to']));
                        $end_date = date('Y-m-d H:i',strtotime($driver['locations'][$new_last_key]['delivered_at'].' '.$driver['locations'][$new_last_key]['delivery_time_to']));
                        $exist_start = date('Y-m-d H:i',strtotime($booking_data['booking_location'][$first_key]['delivered_at'].' '.$booking_data['booking_location'][$first_key]['delivery_time_to']));
                        $exist_end = date('Y-m-d H:i',strtotime($booking_data['booking_location'][$last_key]['delivered_at'].' '.$booking_data['booking_location'][$last_key]['delivery_time_to']));
                        // echo '<pre>';print_r($booking_data['booking_location'][$first_key]['delivered_at']);
                        // echo '<pre>';print_r($driver['locations'][$new_first_key]['delivered_at']);
                        if(($booking_data['booking_location'][$first_key]['delivered_at'] <=  $driver['locations'][$new_first_key]['delivered_at']) && ($booking_data['booking_location'][$last_key]['delivered_at'] <= $driver['locations'][$new_last_key]['delivered_at'])){
                            return $this->errorResponse(__("You have already scheduled a shipment for applied dates and time1."), true);
                        }
                        if(($start_date < $exist_start && $end_date > $exist_start) || ($start_date < $exist_end && $end_date > $exist_end)){
                            return $this->errorResponse(__("You have already scheduled a shipment for applied dates and time2."), true);
                        }
                    }
                    SocketHelper::emit('bid_request_push',$data);
                    $bid_data['booking_id'] = $data['booking_id'];
                    $bid_data['driver_id'] = $data['driver_id'];
                    $bid_data['amount'] = $data['amount'];
                    $bid_data['availability_id'] = "";
                    $insert_id = BookingBid::insertGetId($data);
                    if(isset($data['availability_id']) && !empty($data['availability_id'])){
                        Availability::where('id',$data['availability_id'])->update(['load_status' => '2']);
                    }
                    $get_data = BookingBid::where('id',$insert_id)->first();
                    return $this->successResponse($get_data, __("Bid placed successfully"), true);
                }else{
                    SocketHelper::emit('bid_request_push',$data);
                    $bid_data['booking_id'] = $data['booking_id'];
                    $bid_data['driver_id'] = $data['driver_id'];
                    $bid_data['amount'] = $data['amount'];
                    $bid_data['availability_id'] = "";
                    $insert_id = BookingBid::insertGetId($data);
                    if(isset($data['availability_id']) && !empty($data['availability_id'])){
                        Availability::where('id',$data['availability_id'])->update(['load_status' => '2']);
                    }
                    $get_data = BookingBid::where('id',$insert_id)->first();
                    $get_data['amount'] = CommonHelper::price_format($get_data['amount'],'GR');

                    return $this->successResponse($get_data, __("Bid placed successfully"), true);
                }
            }
        }else{
            return $this->errorResponse(__("Shipment already booked or shipment not available, please try another shipment."), true);
        }
    }

    public function post_book(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'required',
            'driver_id' => 'required',
        ],['booking_id.required' => __('The booking id field is required.'), 
            'driver_id.required' => __('The driver id field is required.'),  
            
          ]);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), true);
        }

        $data = $request->all();
        $check_is_exists = Booking::where(['id' => $data['booking_id']])->whereIn('status', ['pending','create'])->exists();
        if($check_is_exists){
            $driver_detail = DriverVehicle::where(['user_id' => $data['driver_id']])->first();
            if(!empty($driver_detail)){
                $booking_data = Booking::with(['booking_location'])->where(['id' => $data['booking_id']])->first()->toArray();
                $data['shipper_id'] = $booking_data['user_id'];
                if(isset($booking_data) && !empty($booking_data)){
                    $first_key = array_key_first($booking_data['booking_location']);
                    $last_key = array_key_last($booking_data['booking_location']);

                    $check_driver_is_free = BookingTruck::where(['driver_id' => $data['driver_id']])
                    ->select(['booking_trucks.*','booking.id as booking_id','booking.status as booking_status','booking.pickup_date as booking_pickup_date',DB::raw("CONCAT(booking_locations.delivered_at,' ',booking_locations.delivery_time_to) AS full_date")])
                    ->join('booking','booking.id','=','booking_trucks.booking_id')
                    ->join('booking_locations','booking_locations.booking_id','=','booking_trucks.booking_id')
                    ->whereIn('booking.status',['scheduled','in-progress'])
                    ->get()->toArray();
                    if(isset($check_driver_is_free) && !empty($check_driver_is_free)){
                        // return $this->errorResponse(__("You have already scheduled a shipment for applied dates and time."), true);
                        foreach($check_driver_is_free as $driver){
                            $new_first_key = array_key_first($driver['locations']);
                            $new_last_key = array_key_last($driver['locations']);
                            $start_date = date('Y-m-d H:i',strtotime($driver['locations'][$new_first_key]['delivered_at'].' '.$driver['locations'][$new_first_key]['delivery_time_to']));
                            $end_date = date('Y-m-d H:i',strtotime($driver['locations'][$new_last_key]['delivered_at'].' '.$driver['locations'][$new_last_key]['delivery_time_to']));
                            $exist_start = date('Y-m-d H:i',strtotime($booking_data['booking_location'][$first_key]['delivered_at'].' '.$booking_data['booking_location'][$first_key]['delivery_time_to']));
                            $exist_end = date('Y-m-d H:i',strtotime($booking_data['booking_location'][$last_key]['delivered_at'].' '.$booking_data['booking_location'][$last_key]['delivery_time_to']));
                            if(($booking_data['booking_location'][$first_key]['delivered_at'] <=  $driver['locations'][$new_first_key]['delivered_at']) && ($booking_data['booking_location'][$last_key]['delivered_at'] <= $driver['locations'][$new_last_key]['delivered_at'])){
                                return $this->errorResponse(__("You have already scheduled a shipment for applied dates and time."), true);
                            }
                            if(($start_date < $exist_start && $end_date > $exist_start) || ($start_date < $exist_end && $end_date > $exist_end)){
                                return $this->errorResponse(__("You have already scheduled a shipment for applied dates and time."), true);
                            }
                        }
                        Booking::where(['id' => $data['booking_id']])->update(['status' => 'scheduled','schedule_time' => time()]);
                        $update_booking_truck = BookingTruck::where(['booking_id' => $data['booking_id'],'truck_type' => $driver_detail->truck_type])->update(['driver_id' => $data['driver_id']]);
                        if($update_booking_truck){
                            BookingTruck::where(['booking_id' => $data['booking_id']])->whereNotIn('truck_type', [$driver_detail->truck_type])->delete();
                            BookingTruck::where(['booking_id' => $data['booking_id']])->update(['truck_type_category' => $driver_detail['truck_sub_category']]);
                        }
                        if(isset($data['availability_id']) && !empty($data['availability_id'])){
                            Availability::where(['id' => $data['availability_id']])->update(['load_status' => '2', 'booking_id' => $data['booking_id']]);
                        }
                        SocketHelper::emit('post_book_push',$data);
                        return $this->successResponse([], __("Shipment book successfully"), true);
                    }else{
                        Booking::where(['id' => $data['booking_id']])->update(['status' => 'scheduled','schedule_time' => time()]);
                        $update_booking_truck = BookingTruck::where(['booking_id' => $data['booking_id'],'truck_type' => $driver_detail->truck_type])->update(['driver_id' => $data['driver_id']]);
                        if($update_booking_truck){
                            BookingTruck::where(['booking_id' => $data['booking_id']])->whereNotIn('truck_type', [$driver_detail->truck_type])->delete();
                            BookingTruck::where(['booking_id' => $data['booking_id']])->update(['truck_type_category' => $driver_detail['truck_sub_category']]);
                        }
                        if(isset($data['availability_id']) && !empty($data['availability_id'])){
                            Availability::where(['id' => $data['availability_id']])->update(['load_status' => '2', 'booking_id' => $data['booking_id']]);
                        }
                        SocketHelper::emit('post_book_push',$data);
                        return $this->successResponse([], __("Shipment book successfully"), true);
                    }
                }
            }else{
                return $this->errorResponse(__("Carrier not found"), true);
            }
        }
        return $this->errorResponse(__("Shipment already booked or shipment not available, please try another shipment."), true);
    }
}
