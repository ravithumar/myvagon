<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class WebsiteController extends Controller
{
	public function changeLang($locat = 'gr')
    {
    	// echo '<pre>';print_r($locat);die();
        Session::put('lang',$locat);
         \App::setLocale($locat);
        return redirect()->back();
    }
}

?>
