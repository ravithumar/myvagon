<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Models\TruckCategory;
use App\Models\TruckType;
use App\Models\TruckUnit;
use App\Helpers; 
use \Validator;

class TruckTypeController extends Controller
{
    // public function index(Request $request, $id = null) {
     
    //         $base = env('APP_URL') . 'images/type/';
           
    //         if ($request->ajax()) {
            
    //             $data = TruckType::select('*');
                
    //             return Datatables::of($data)
    //                 ->addIndexColumn()

    //                 ->editColumn('status', function ($row) {
                       
    //                     if($row['status'] == 1)
    //                         $btn = '<a  class=" text-success grab" data-url="status/change" data-table="truck_type" data-id="' . $row["id"] . '"  data-status="' . $row["status"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-success">Active</span></a>';
    //                     else
    //                         $btn = '<a  class=" text-danger grab" data-url="status/change" data-table="truck_type" data-id="' . $row["id"] . '" data-status="' . $row["status"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-danger">Inactive</span></a>';
                        
    //                     return $btn;
    //                 })



    //                 ->editColumn('action', function ($row) {
    //                     $btn = 
    //                     '<a class="text-danger grab btn btn-outline-danger btn-xs"  data-newurl="truck/type/delete" data-url="type/delete" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="' . csrf_token() . '" ><i class="fa fa-trash"></i></a>
    //                      &nbsp;&nbsp;
    //                     <a href="' . route('admin.truck.type.index', $row['id']) . '" class="text-info grab btn btn-outline-info btn-xs"><i class="fa fa-pencil"></i></a>
    //                     ';
    //                     return $btn;
    //                 })
    //                 ->rawColumns(['image','status', 'action'])
    //                 ->make(true);
    //         } else {
                
    //             if(isset($id) && $id != null){
    //                 $params['edit_param'] = TruckType::find($id);
    //             }
                
    //             $columns = [
    //                 ['data' => 'id', 'name' => "Id", 'title' => __("Id")],
    //                 ['data' => 'name', 'name' => __("Name"), 'title' => __("Name")],
    //                 ['data' => 'truck_unit.name', 'name' => __("unit_id"), 'title' => __("Unit")],
    //                 ['data' => 'capacity', 'name' => __("capacity"), 'title' => __("capacity")],
    //                 ['data' => 'status', 'name' => __("Status"), 'title' => __("Status"), 'searchable' => true, 'orderable' => true],
    //                 ['data' => 'action', 'name' => __("Action"), 'title' => __("Action"), 'searchable' => false, 'orderable' => false],
                  
                    
                
    //             ];
    //             $params['dateTableFields'] = $columns;
    //             $params['dateTableUrl'] = route('admin.truck.type.index');
    //             $params['dateTableTitle'] = "Truck Type";
    //             $params['addUrl'] = "truck";
    //             $params['dataTableId'] = time();
    //             $params['base'] = $base;
    //             $params['truck_unit'] = TruckUnit::get();
               

    //             return view('admin.pages.truck_type.index', $params);
    //         }
      
    // }

    public function index(Request $request, $id = null) {

            $base = env('APP_URL') . 'images/type/';

            if ($request->ajax()) {

                $data = TruckType::select('*')->with(['TruckUnit'])->get();
                // echo '<pre>'; print_r($data);
                return Datatables::of($data)
                    ->addIndexColumn()

                    ->editColumn('status', function ($row) {

                        if($row['status'] == 1)
                            $btn = '<a  class=" text-success grab" data-url="status/change" data-table="truck_type" data-id="' . $row["id"] . '"  data-status="' . $row["status"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-success">Active</span></a>';
                        else
                            $btn = '<a  class=" text-danger grab" data-url="status/change" data-table="truck_type" data-id="' . $row["id"] . '" data-status="' . $row["status"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-danger">Inactive</span></a>';

                        return $btn;
                    })
                    ->editColumn('action', function ($row) {
                        $btn =
                        '<a class="text-danger grab btn btn-outline-danger"  data-newurl="truck/type/delete" data-url="type/delete" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="' . csrf_token() . '" ><i class="fa fa-trash"></i></a>
                         &nbsp;&nbsp;
                        <a href="' . route('admin.truck.type.index', $row['id']) . '" class="text-info grab btn btn-outline-info"><i class="fa fa-pencil"></i></a>
                        ';
                        return $btn;
                    })
                    ->rawColumns(['image','status', 'action'])
                    ->make(true);
            } else {

                if(isset($id) && $id != null){
                    $params['edit_param'] = TruckType::find($id);
                }

                $columns = [
                    ['data' => 'id', 'name' => "Id", 'title' => __("Id")],
                    ['data' => 'name', 'name' => __("name"), 'title' => __("Name"), 'searchable' => true],
                    ['data' => 'truck_unit.name', 'name' => __("unit_id"), 'title' => __("Unit")],
                    ['data' => 'capacity', 'name' => __("capacity"), 'title' => __("capacity")],
                    ['data' => 'status', 'name' => __("Status"), 'title' => __("Status"), 'searchable' => true, 'orderable' => true],
                    ['data' => 'action', 'name' => __("Action"), 'title' => __("Action"), 'searchable' => false, 'orderable' => false],
                ];
                $params['dateTableFields'] = $columns;
                $params['dateTableUrl'] = route('admin.truck.type.index');
                $params['dateTableTitle'] = "Truck Type";
                $params['addUrl'] = "truck";
                $params['dataTableId'] = time();
                $params['base'] = $base;
                $params['truck_unit'] = TruckUnit::get();

                return view('admin.pages.truck_type.index', $params);
            }

    }



    public function store(Request $request) {
       
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255',
            'image' => 'required',
            'truck_unit' => 'required',
            'capacity' => 'required',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $img = Helpers\CommonHelper::imageUpload($request->image, 'images/type');
        $is_trailer = $request->input('is_trailer');
        $type = new TruckType();
        $type->name = $request->input('name');
        $type->icon = $img;
        $type->unit_id = $request->input('truck_unit');
        $type->capacity = $request->input('capacity');
        if(isset($is_trailer) && !empty($is_trailer)){
            $type->is_trailer = '1';
        }else{
            $type->is_trailer = '0';
        }
        $type->save();

        return redirect()->route('admin.truck.type.index')->with('success', __('Truck type created successfully.'));
      
    }

    public function update(Request $request, TruckType $type, $id) {
        
            $validator = \Validator::make($request->all(), [
                'name' => 'required|max:255',
                'truck_unit' => 'required',
                'capacity' => 'required',
            ]);
            
            if ($validator->fails()) {
                return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
            }

            $type = TruckType::whereId($id)->first();
            
            if($request->has('image')){
                $img = Helpers\CommonHelper::imageUpload($request->image, 'images/type');
            }else{
                $img = $type->icon;
            }
                
            $is_trailer = $request->input('is_trailer');
            $type->name = $request->input('name');
            $type->icon = $img;
            $type->unit_id = $request->input('truck_unit');
            $type->capacity = $request->input('capacity');
            if(isset($is_trailer) && !empty($is_trailer)){
                $type->is_trailer = '1';
            }else{
                $type->is_trailer = '0';
            }
            $type->update();

            return redirect()->route('admin.truck.type.index')->with('success', __('Truck type update successfully.'));
        
    }

    public function destroy($id) {
        TruckType::whereId($id)->delete();
        return \Response::json('success', 200);

    }

}
