<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Booking;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\DriverVehicle;
use App\Models\VehicleCapacity;
use App\Models\DriverPermission;
use Illuminate\Support\Facades\Validator;
use App\Helpers;
use App\Models\Invoice;
use Mail;
use Illuminate\Support\Str;
use App\Jobs\DriverCommisionInvoiceJob;
use Laravel\Passport\Token; 
use App\Models\BookingTruck;
use App\Models\Availability;
use Illuminate\Support\Facades\DB;
Use App\Models\BookingLocations;
use DataTables;

class DriverController extends Controller
{
    public function index(Request $request) {

      
        #driver role identify
        $role = Role::select('*')->where('name','carrier')->first();
        $user_role = UserRole::select('user_id')->where('role_id', $role->id)->pluck('user_id');
        $data = User::select('*')->whereIn('id', $user_role)->orderBy('id','desc');

        if ($request->ajax()) {
           
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                       
                    if($row['status'] == 1)
                        $btn = '<a  class="text-success grab"  data-url="status/change"  data-status="'. $row['status'] .'" data-table="users" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-success">Active</span></a>';
                    else
                        $btn = '<a  class="text-danger grab" data-url="status/change" data-status="'. $row['status'] .'" data-table="users" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-danger">Inactive</span></a>';
                    
                    return $btn;
                })

                ->editColumn('action', function ($row) {
                    /* $btn = '
                     <a  href="' . route('admin.driver.view', $row['id']) . '" class="text-primary grab btn btn-outline-primary btn-xs"><i class="fas fa-search"></i></a> &nbsp;&nbsp;
                     <a class="text-danger grab btn btn-outline-danger btn-xs"  data-newurl="truck/type/delete" data-url="type/delete" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="' . csrf_token() . '" ><i class="fa fa-trash"></i></a> &nbsp;&nbsp;
                     <a  href="' . url('admin/driver/view_load', $row['id']) . '" class="text-dark">View</a>
                    '; */

                    $btn = '
                     <a class="text-danger grab btn btn-outline-danger btn-xs"  data-newurl="truck/type/delete" data-url="type/delete" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="' . csrf_token() . '" ><i class="fa fa-trash"></i></a> &nbsp;&nbsp;
                     <a  href="' . url('admin/driver/view_load', $row['id']) . '" class="text-dark">View Details</a>
                    ';
                    return $btn;
                })
                ->editColumn('created_at', function($row){
                    return date('d/m/Y',strtotime($row['created_at']));
                })
                ->rawColumns(['status', 'action'])

                ->make(true);
        } else {
            $columns = [
                            ['data' => 'id', 'name' => "Id",'title' => __("Id")], 
                            ['data' => 'name', 'name' => __("Name"),'title' => __("Name")],
                            ['data' => 'type', 'name' => __("Type"),'title' => __("Type")],
                            // ['data' => 'email', 'name' => __("Email"),'title' => __("Email")],
                             ['data' => 'status', 'name' => __("Status"), 'title' => __("Status"), 'searchable' => true, 'orderable' => true],
                             ['data' => 'created_at', 'name' => __("created_at"), 'title' => __("Date Joined"), 'searchable' => true, 'orderable' => true],
                            // ['data' => 'phone', 'name' => __("Phone"),'title' => __("Phone")],
                            
                           
                            ['data' => 'action', 'name' =>__("Action"),'title'=>__("Action"), 'searchable' => false, 'orderable' => false]
                       ];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('admin.driver.index');
            $params['dateTableTitle'] = "Driver";
            $params['addUrl'] = "driver";
            $params['dataTableId'] = time();
            return view('admin.pages.drivers.index', $params);
        }
    }
    // public function my_loads_demo(Request $request,$id){
        
    //    $data = User::where('id',$id)->first();
       
    //     if(!empty($data)){
    //         $limit = 10;
    //         $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
    //         $skip = ($page - 1) * $limit;
    //         $my_loads = array();
    //         $posted_trucks = array();
    //         $my_loads = $this->get_loads($data,$skip,$limit);
    //         $posted_trucks = $this->get_posted_truck($data,$skip,$limit);
            
    //         $loads_final = [];
            
    //         if(!empty($my_loads)){
    //             $i = 0;
    //             $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
    //             foreach($my_loads->toArray() as $row){   
    //                 // $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
    //                 // $loads[$i] = $this->change_key($row, 'pickup_date', 'date');
    //                 if(isset($loads[$i]['trucks']) && !empty($loads[$i]['trucks'])){
    //                     $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
    //                     $sum = 0;
    //                     if(isset($trucks) && !empty($trucks)){
    //                         $loads[$i]['trucks'] = $trucks->toArray();
    //                         $sum = $this->weight_sum($loads[$i]['trucks']['locations']);
    //                         // $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
    //                     }
    //                     $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
    //                     $loads[$i]['total_weight'] = $sum." ".$unit;
    //                 }
    //                 $i++;
    //             }
    //             if(!empty($loads)){
    //                 // $loads_final = $this->group_by_request($loads,'id');
    //                 $loads_final = $loads;
    //             }
    //         }
    //         // echo '<pre>';print_r($loads_final);die();

    //         $posted_truck_final = [];
    //         if(!empty($posted_trucks)){
    //             $i = 0;
    //             $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();

    //             foreach($posted_trucks->toArray() as $row){   
                    
    //                 if(isset($posted_trucks[$i]['booking_info']['trucks']) && !empty($posted_trucks[$i]['booking_info']['trucks'])){
    //                     $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['booking_info']['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
    //                     // echo '<pre>';print_r($trucks->toArray());die();
    //                     $sum = 0;
    //                     if(isset($trucks) && !empty($trucks)){
    //                         $posted_trucks[$i]['booking_info']['trucks'] = $trucks->toArray();
    //                         $sum = $this->weight_sum($posted_trucks[$i]['booking_info']['trucks']['locations']);
    //                     }
    //                     $unit = isset($posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
    //                     $posted_trucks[$i]['booking_info']['total_weight'] = $sum." ".$unit;
    //                 }
    //                 // $posted_trucks[$i]['is_bid']=2;
    //                 $posted_trucks[$i]['posted_truck']=1;
    //                 $i++;
    //             }
    //             if(!empty($posted_trucks)){
    //                 $posted_truck_final = $posted_trucks->toArray();
    //             }

    //         }
    //         // print_r($posted_truck_final);exit;
    //         $load_data = array_merge($loads_final,$posted_truck_final);
    //         $final_array = array();
    //         if(!empty($load_data)){
    //             $final_array = $this->group_by_request($load_data,'id');
    //         }

    //         $date = array();
    //         foreach ($final_array as $key => $row)
    //         {
    //             $date[$key] = strtotime($row['date']);
    //         }
    //         array_multisort($date, SORT_DESC, $final_array);

    //         // print_r($unit);exit;
    //         // return $this->successResponse($loads_final, __("success"), true);
    //     }

    //     $params['dateTableTitle'] = "view load scheduled";
    //     $params['dataTableId'] = time();
        
    //     return view('admin.pages.drivers.view_load',$params,compact('data','posted_trucks'));
    // }
    public function get_loads($data,$skip,$limit)
    {
        $my_loads = array();
        if($data['status'] == 'all'){

            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                // ->where(function($query) use($data) {
                //     if(isset($data['type']) && !empty($data['type'])){
                //         if($data['type'] == 'bid'){
                //             $query->where('booking.is_bid',1);
                //         }elseif($data['type'] == 'book'){
                //             $query->where('booking.is_bid',0);
                //         }
                //     }
                // })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('pickup_date','desc')
                ->get();

                // echo '<prE>';print_r($my_loads);die();
            }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where(function($query) use($data) {
                    $query->where('booking_trucks.driver_id',$data['driver_id']);  
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }
            dd($data['type']);
        }elseif($data['status'] == 'pending'){

            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->orWhere(function($query) use($data) {
                    $query->where('booking_bid.driver_id', $data['driver_id'])
                        ->where('booking_bid.status', 0);
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }

        }else{
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book' || $data['type'] == 'bid')){
                $my_loads = Booking::distinct()->select('booking.*')
                ->leftJoin('booking_bid','booking_bid.booking_id','=','booking.id')
                ->leftJoin('booking_trucks','booking.id','=','booking_trucks.booking_id')
                ->where('booking_trucks.driver_id',$data['driver_id'])
                ->where('booking.status',$data['status'])
                ->where(function($query) use($data) {
                    if(isset($data['type']) && !empty($data['type'])){
                        if($data['type'] == 'bid'){
                            $query->where('booking.is_bid',1);
                        }elseif($data['type'] == 'book'){
                            $query->where('booking.is_bid',0);
                        }
                    }
                })
                ->where(function($query) use($data) {
                    if(!empty($data['date'])){
                        $query->where('booking.pickup_date',date('Y-m-d',strtotime($data['date'])));
                    }
                })
                ->skip($skip)
                ->limit($limit)
                ->orderBy('id','desc')
                ->get();
            }
            
        }
        return $my_loads;
    }
    
    public function get_posted_truck($data,$skip,$limit)
    {
        $posted_trucks = array();
        if($data['status'] == 'all'){
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])->select('availability.*',DB::raw('COUNT(booking_request.id) as booking_request_count'))->where(['availability.driver_id' => $data['driver_id']])
                ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                // ->where('booking_request.status',0)
                ->where('load_status','0')
                ->groupBy('availability.id')
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                ->get();

                // $posted_trucks = $this->get_posted_truck($data,$skip,$limit);
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $posted_trucks = Availability::distinct()->with('booking_info')->select('availability.*',DB::raw('COUNT(booking_request.id) as booking_request_count'))->where(['availability.driver_id' => $data['driver_id']])
                ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                // ->where('booking_request.status',0)
                ->groupBy('availability.id')    
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                ->get();
                // $posted_trucks = $this->get_posted_truck($data,$skip,$limit);
            }
        }elseif($data['status'] == 'pending'){

            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $posted_trucks = Availability::distinct()->select('availability.*',DB::raw('COUNT(booking_request.id) as booking_request_count'))->where(['availability.driver_id' => $data['driver_id']])
                ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                ->where('load_status','0')
                // ->where('booking_request.status',0)
                ->skip($skip)
                ->limit($limit)
                ->groupBy('availability.id')
                ->orderBy('availability.date','desc')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $posted_trucks = Availability::distinct()->select('availability.*',DB::raw('COUNT(booking_request.id) as booking_request_count'))->where(['availability.driver_id' => $data['driver_id']])->with('booking_info')
                ->leftJoin('booking_request','booking_request.availability_id','availability.id')
                ->where('load_status','0')
                // ->where('booking_request.status',0)
                ->skip($skip)
                ->limit($limit)
                ->groupBy('availability.id')
                ->orderBy('availability.date','desc')
                ->get();
            }

        }else{
            if(isset($data['type']) && !empty($data['type']) && $data['type'] == 'all'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])
                ->where(['availability.driver_id' => $data['driver_id']])
                ->where('availability.load_status','1')
                ->whereHas('booking_info',function($q) use($data)
                {
                    $q->where('status',$data['status']);
                })
                // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                // ->groupBy('availability.id')
                ->get();
            }elseif(isset($data['type']) && !empty($data['type']) && $data['type'] == 'posted_truck'){
                $posted_trucks = Availability::distinct()->with(['booking_info'])
                ->where(['availability.driver_id' => $data['driver_id']])
                ->where('availability.load_status','1')
                ->whereHas('booking_info',function($q) use($data)
                {
                    $q->where('status',$data['status']);
                })
                // ->with(['booking_info' => function($query) use($data) { $query->where('booking.status',$data['status']); }])
                ->skip($skip)
                ->limit($limit)
                ->orderBy('availability.date','desc')
                // ->groupBy('availability.id')
                ->get();
            }
        }
        return $posted_trucks;
    }
    public function view_load(Request $request,$driver_id){
        
        $data = User::select('*')->where('id', $driver_id)->first();
        $vehicle = DriverVehicle::with(['TruckType','TruckSubCategory','brands'])->where('user_id', $data->id)->first();
        $vehicle_capacity = VehicleCapacity::where('driver_vehicle_id', $vehicle->id)->get()->toArray();
        $driver_permissions = DriverPermission::where('user_id',$driver_id)->first();      
        $booking = Booking::orderBy('id','desc')->get()->first();
        
        if(!empty($data)){
            $limit = 10;
            $page = $data['page_num'] && $data['page_num'] > 0 ? $data['page_num'] : 1;
            $skip = ($page - 1) * $limit;
            $my_loads = array();
            $posted_trucks = array();
            $my_loads = $this->get_loads($data,$skip,$limit);
            $posted_trucks = $this->get_posted_truck($data,$skip,$limit);
            
            $loads_final = [];
            
            if(!empty($my_loads)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($my_loads->toArray() as $row){   
                    // $loads[$i] = $this->change_key($row, 'user_id', 'shipper_details');
                    // $loads[$i] = $this->change_key($row, 'pickup_date', 'date');
                    if(isset($loads[$i]['trucks']) && !empty($loads[$i]['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $loads[$i]['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($loads[$i]['trucks']['locations']);
                            // $sum = array_sum(array_column($loads[$i]['trucks']['locations'][0]['products'], 'weight'));
                        }
                        $unit = isset($loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name']) ? $loads[$i]['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $loads[$i]['total_weight'] = $sum." ".$unit;
                    }
                    $i++;
                }
                if(!empty($loads)){
                    // $loads_final = $this->group_by_request($loads,'id');
                    $loads_final = $loads;
                }
            }
            // echo '<pre>';print_r($loads_final);die();

            $posted_truck_final = [];

            if(!empty($posted_trucks)){
                $i = 0;
                $driver_vehicle_details = DriverVehicle::where('user_id',$data['driver_id'])->first();
                foreach($posted_trucks->toArray() as $row){   
                    
                    if(isset($posted_trucks[$i]['booking_info']['trucks']) && !empty($posted_trucks[$i]['booking_info']['trucks'])){
                        $trucks = BookingTruck::with(['truck_type'=>function($query){$query->select('id','name');}])->whereBookingId($row['booking_info']['id'])->whereTruckType($driver_vehicle_details->truck_type)->first();
                        // echo '<pre>';print_r($trucks->toArray());die();
                        $sum = 0;
                        if(isset($trucks) && !empty($trucks)){
                            $posted_trucks[$i]['booking_info']['trucks'] = $trucks->toArray();
                            $sum = $this->weight_sum($posted_trucks[$i]['booking_info']['trucks']['locations']);
                        }
                        $unit = isset($posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name']) ? $posted_trucks[$i]['booking_info']['trucks']['locations'][0]['products'][0]['unit']['name'] : '';
                        $posted_trucks[$i]['booking_info']['total_weight'] = $sum." ".$unit;
                    }
                    // $posted_trucks[$i]['is_bid']=2;
                    $posted_trucks[$i]['posted_truck']=1;
                    $i++;
                }
                if(!empty($posted_trucks)){
                    $posted_truck_final = $posted_trucks->toArray();
                }

            }
            // print_r($posted_truck_final);exit;
            $load_data = array_merge($loads_final,$posted_truck_final);
            $final_array = array();
            if(!empty($load_data)){
                $final_array = $this->group_by_request($load_data,'id');
            }

            $date = array();
            foreach ($final_array as $key => $row)
            {
                $date[$key] = strtotime($row['date']);
            }
            array_multisort($date, SORT_DESC, $final_array);

            // print_r($posted_truck_final);exit;
        }

        $params['dateTableTitle'] = "View Load scheduled";
        $params['paymentTableTitle'] = "Payment Section ";
        $params['driverTableTitle'] = "Driver Profile ";
        $params['truckdetailsTableTitle'] = "Driver Truck Details ";
        $params['ActiveheadingTitle'] = "Active/Deactive ";
        $params['chatheadingTitle'] = "Chat & Support ";
        $params['TrailerTableTitle'] = "Driver Trailer Details ";
        $params['data'] = $data;
        $params['vehicle'] = $vehicle->toArray();
        $params['vehicle_capacity'] = $vehicle_capacity;
        $params['driver_permissions'] = $driver_permissions;
        $params['addUrl'] = "driver";
        $params['dataTableId'] = time();
        return view('admin.pages.drivers.view_load',$params,compact('data','vehicle','vehicle_capacity','driver_permissions','booking'));
    }
    public function view_profile(Request $request, $id){
        $params['dateTableTitle'] = "Driver Profile";
        $data = User::select('*')->where('id', $id)->first();
        $vehicle = DriverVehicle::with(['TruckType','TruckSubCategory','brands'])->where('user_id', $data->id)->first();
        $vehicle_capacity = VehicleCapacity::where('driver_vehicle_id', $vehicle->id)->get()->toArray();
        $driver_permissions = DriverPermission::where('user_id',$id)->first();   
        return view('admin.pages.drivers.view_profile',$params,compact('data','vehicle','vehicle_capacity','driver_permissions'));
    }
    public function view(Request $request, $id){

        $data = User::select('*')->where('id', $id)->first();
        $vehicle = DriverVehicle::with(['TruckType','TruckSubCategory','brands'])->where('user_id', $data->id)->first();
        $vehicle_capacity = VehicleCapacity::where('driver_vehicle_id', $vehicle->id)->get()->toArray();
        $driver_permissions = DriverPermission::where('user_id',$id)->first();        
        $columns = [
                        ['data' => 'id', 'name' => "Order Id",'title' => __("Order Id")], 
                        ['data' => 'name', 'name' => __("Name"),'title' => __("Name")],
                        ['data' => 'email', 'name' => __("Email"),'title' => __("Email")],
                        ['data' => 'type', 'name' => __("Type"),'title' => __("Type")],
                        
        ];
        $params['dateTableFields'] = $columns;
        $params['dateTableUrl'] = route('admin.driver.view', $data->id);
        $params['dateTableTitle'] = "driver";
        $params['data'] = $data;
        $params['vehicle'] = $vehicle->toArray();
        $params['vehicle_capacity'] = $vehicle_capacity;
        $params['driver_permissions'] = $driver_permissions;
        $params['addUrl'] = "driver";
        $params['dataTableId'] = time();
        return view('admin.pages.drivers.view', $params);
    
    }

    public function update($id, Request $request, User $user){
        $validator = \Validator::make($request->all(), [
            'full_name' => 'required',
            // 'last_name' => 'required',
            // 'self_des' => 'required',
            'profile' => 'file',
            // 'company_name' => 'required',
            // 'company_contact' => 'required',
           
        ],[
            'full_name.required' => 'Full name field is required.',
            // 'last_name.required' => 'This value is required.',
            // 'self_des.required' => 'This value is required.',
            'profile.required' => 'profile field is required.',
            // 'company_name.required' => 'This value is required.',
            // 'company_contact.required' => 'This value is required.',
          
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $request->all();
        $user = User::where('id',$id)->first();


        if($request->has('profile')){
            $img = Helpers\CommonHelper::uploadImagesS3Bucket($request->file('profile'),'driver/');
            // $img = Helpers\CommonHelper::imageUpload($request->profile, 'images/driver');
            $user->profile = $img;

        }

        $user->name = $request->input('full_name');
        // $user->self_des = $request->input('self_des');
        // $user->company_name = $request->input('company_name');
        // $user->company_phone = $request->input('company_contact');
       
        $user->update();
        return redirect()->route('admin.driver.view', $user->id)->with('success', __('Driver content update successfully.'));
    }

    public function accepted($id){
        $user = User::where('id', $id)->first();
        $user->stage = "accepted";
        $user->update();
        return \Response::json($user, 200);

    }

    public function rejected($id){
        $user = User::where('id', $id)->first();
        $user->stage = "rejected";
        $user->update();
        return \Response::json($user, 200);

    }

    public function blockStatus($id, $status){
       
        $data = User::find($id);
        $data->block_status = $status;
        $data->save(); 
       
       if($status == "0"){
            $userTokens = $data->tokens;
           
            foreach($userTokens as $token) {
                $token->revoke();  
                $token->delete();   
                
                \DB::table('oauth_access_tokens')
                ->where('user_id', $id)
                ->update([
                    'revoked' => false
                ]);

            }
       }
       if($status == "1"){
           $data = User::find($id);
            dispatch(new \App\Jobs\DriverAcceptProfileJob($data));
       }
        return \Response::json($data, 200);
    }

    public function change_permission(Request $request,$id)
    {
        $data = $request->all();
        if(isset($id) && !empty($id)){
            $permission['search_loads'] = 0;
            if(isset($data['search_loads']) && !empty($data['search_loads'])){
                $permission['search_loads'] = 1;
            }

            $permission['my_loads'] = 0;
            if(isset($data['my_loads']) && !empty($data['my_loads'])){
                $permission['my_loads'] = 1;
            }

            $permission['my_profile'] = 0;
            if(isset($data['my_profile']) && !empty($data['my_profile'])){
                $permission['my_profile'] = 1;
            }

            $permission['statistics'] = 0;
            if(isset($data['statistics']) && !empty($data['statistics'])){
                $permission['statistics'] = 1;
            }

            $permission['settings'] = 0;
            if(isset($data['setting']) && !empty($data['setting'])){
                $permission['settings'] = 1;
            }

            $permission['change_password'] = 0;
            if(isset($data['change_password']) && !empty($data['change_password'])){
                $permission['change_password'] = 1;
            }

            $permission['allow_bid'] = 0;
            if(isset($data['allow_bid']) && !empty($data['allow_bid'])){
                $permission['allow_bid'] = 1;
            }

            $permission['view_price'] = 0;
            if(isset($data['view_price']) && !empty($data['view_price'])){
                $permission['view_price'] = 1;
            }

            $permission['post_availibility'] = 0;
            if(isset($data['post_availibility']) && !empty($data['post_availibility'])){
                $permission['post_availibility'] = 1;
            }

            $check_is_exists = DriverPermission::where('user_id',$id)->exists();
            if($check_is_exists){
                DriverPermission::where('user_id',$id)->update($permission);
            }else{
                $permission['user_id'] = $id;
                DriverPermission::create($permission);
            }
            return redirect()->route('admin.driver.view', $id)->with('success', __('Driver permissions updated successfully.'));
        }
        return redirect()->route('admin.driver')->with('error', __('Something went wrong! please try again later'));
    }
     public function get_invoice(Request $request,$driver_id){
                // echo '<pre>';print_r($driver_id);die();

        $data_user = BookingTruck::select('*')->where('driver_id', $driver_id)->get();
        $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])
                ->Orwhere('status','completed' || 'status','cancelled' || 'payment_status' , 'completed')
                ->get();
        // $booking_id = $data_user['booking_id'];
               
        $url_param = $request->input('status');
        $search_param = $request->input('search');
        if ($request->ajax()) {
            $search_param = $request->post('searching_data');
            $start_date = '';
            $end_date = '';
            $pickup_date = $request->post('pickup_date');
            if(isset($pickup_date) && !empty($pickup_date)){
                $start_date = date("Y-m-d", mktime(0, 0, 0, $request->post('pickup_date'), 01));
                $end_date = date("Y-m-t", mktime(0, 0, 0, $request->post('pickup_date')));
            }
            // print_r($pickup_date);die();
            $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->where(function($query) use($start_date,$end_date){
                if(isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date)){
                    // $query->where('pickup_date','>=',$start_date);   
                    // $query->where('pickup_date','<=',$end_date);   
                    $query->whereBetween('pickup_date',[$start_date,$end_date]);

                }
            })->where(function($query){
                $query->where(['status' => 'cancelled']);
                $query->orWhere(['status' => 'completed','payment_status' => 'completed']);
            })->orderBy('id','desc')->get();
           
            
            
             
                
            return DataTables::of($data)
                ->filter(function ($query) use ($request) {
                    if ($request->get('status') != '') {
                        $query->where('status', $request->get('status'));
                    }
                    
                })
                ->addIndexColumn()
                ->editColumn('action', function ($row) {
                    $pickup_address = "'" . $row['pickup_address'] . "'";
                    $pickup_lat = "'" . $row['pickup_lat'] . "'";
                    $pickup_lng = "'" . $row['pickup_lng'] . "'";
                    $drop_location = "'" . $row['booking_location'][0]['drop_location'] . "'";
                    $drop_lat = 0;
                    $drop_lng = 0;
                    if(isset($row['booking_location'][0]['drop_lat']) && !empty($row['booking_location'][0]['drop_lat'])){
                        $drop_lat = "'" . $row['booking_location'][0]['drop_lat'] . "'";
                    }
                    if(isset($row['booking_location'][0]['drop_lng']) && !empty($row['booking_location'][0]['drop_lng'])){
                        $drop_lng = "'" . $row['booking_location'][0]['drop_lng'] . "'";
                    }

                    if($row['status'] != 'pending' || $row['status'] != 'created'){
                        // $btn = '
                        // &nbsp;&nbsp;<a href="javascript:void(0)"  onclick="delete_shipment(this);return false;" data-newurl="manage-shipment/delete" data-id="'.$row['id'].'" data-token="'. csrf_token() .'" style="background-color: white;color: black;" class="text-danger btn btn-outline-danger btn-xs text-default justify-content-end "> <i class="fa fa-trash"></i></a>
                        // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                        
                        $btn = '<a href="'.url('admin/shipper/shipment-detail',$row['id'] ).'" style="color:#9B51E0;text-decoration:underline;font-weight: bold;"><b>'.'View Details'.'</b></a>';
                    }else{
                        // $btn = '
                        // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                    }
                    return $btn;

                })
                ->addColumn('start_date',function($row){
                    $start_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $start_date = date('d/m/Y',strtotime($row['pickup_date'])) .'<br> TO <br>'. date('d/m/Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $start_date;
                })
                ->addColumn('end_date',function($row){
                    $end_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $end_date = date('F d, Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $end_date;
                })
                ->addColumn('shipment_type', function ($row){
                    if($row['is_public'] == '1'){
                        $shipment_type = ' Direct    Public ';
                    }else{
                        $shipment_type = 'Direct    Private';
                    }
                    if($row['booking_type'] == 'multiple_shipment'){
                        if($row['is_public'] == '1'){
                            $shipment_type = 'Multiple   Public';
                        }else{
                            $shipment_type = ' Multiple    Private';
                        }
                    }
                    return $shipment_type;

                })
                ->addColumn('drop_location',function($row){
                    $drop_location = '-';
                    if(isset($row) && !empty($row)){
                        $company_name = isset($row['booking_location'][0]['company_name']) ? $row['booking_location'][0]['company_name'] : "-";
                        $drop_location = isset($row['booking_location'][0]['drop_location'])?$row['booking_location'][0]['drop_location']:"-";
                        $drop_location = '<b>'.$company_name.'</b><br>'.$drop_location;
                    }
                    return $drop_location;
                })
                ->editColumn('amount',function($row){
                    return '€ '.$row['amount'];
                })
                ->editColumn('status',function($row){
                    $status = '';
                    if($row['status'] == 'create'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        $status .= '<br><small class="text-danger">'.__("Waiting for truck").'</small>';
                    }elseif($row['status'] == 'scheduled'){
                        $status = '<label class="scheduled-status" style="padding: 5px 10px;">'.__("Scheduled").'</label>';
                    }elseif($row['status'] == 'pending'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        if($row['is_bid'] == '1'){
                            $status .= '<br><small class="text-danger">'.__("Waiting for bid").'</small>';
                        }else{
                            $status .= '<br><small class="text-danger">'.__("Waiting for book").'</small>';
                        }
                    }elseif($row['status'] == 'in-process'){
                        $status = '<label class="in-process-status" style="padding: 5px 10px;">'.__("In-process").'</label>';
                    }elseif($row['status'] == 'completed'){
                        $status = '<label class="completed-status" style="padding: 5px 10px;">'.__("Completed").'</label>';
                    }else{
                        $status = '<label class="cancelled-status" style="padding: 5px 10px;">'.__("Cancelled").'</label>';
                    }
                    return $status;
                })

                ->rawColumns(['action','status','drop_location','pickup_address','start_date' ,'action','status','drop_location' ,'pickup_date'])
                ->make("*");

        }  else {
            $columns = [
                            ['data' => 'shipment_type', 'name' => "shipment_type",'title' => __("Type"),'width'=>'10%','searchable' => false, 'orderable' => false], 
                            ['data' => 'id', 'name' => "id",'title' => __("Order Id"),'width'=>'60px'], 
                            ['data' => 'amount', 'name' => __("amount"),'title' => __("Price"),'width'=>'60px'],
                            ['data' => 'status', 'name' => __("status"),'title' => __("Status"),'width'=>'80px'],
                            ['data' => 'start_date', 'name' => __("pickup_date"),'title' => __("Date"),'width'=>'15%','searchable' => false, 'orderable' => false],
                            // ['data' => 'end_date', 'name' => __("end_date"),'title' => __("End Date"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                            ['data' => 'pickup_address', 'name' => __("pickup_address"),'title' => __("Pickup Location"),'width'=>'17.5%', 'searchable' => false, 'orderable' => false],
                            ['data' => 'drop_location', 'name' => __("drop_location"),'title' => __("Delivery Location"),'width'=>'17.5%','searchable' => false, 'orderable' => false],
                            ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                       ];
                       // dd($columns);die();
                       $driver_id = $driver_id;
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = url('/admin/driver/generate_invoice',$driver_id);
            // dd($params['dateTableUrl'] );die();
            $params['dateTableTitle'] = "View Driver";
            // $params['addUrl'] = "manage-shipment";
            $params['data'] = $data;
            // dd($params['data']);die();
            $params['dataTableId'] = time();
           // dd($params['dataTableId']);die();
            // $params['multiple_data'] = $multiple_data;
            $params['search_param'] = $search_param;
            return view('admin.pages.drivers.get_invoice', $params,compact('data_user','driver_id'));
        }
       
    }
    public function driver_send_mail(Request $request,$driver_id){
        // echo '<pre>';print_r($driver_id);die();
        $validator = Validator::make($request->all(), [
            'invoice_number' => '',
            'user_id' => '',
            'start_date' => '',
            'end_date' => '',
            'email' => '',
            'total_amount' => '',
            'status' => '',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }
        $invoice = new Invoice();
        $pickup_date = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->orderBy('pickup_date')->get();
        $start_date = '';
        $end_date = '';
        $pickup_date = $request->post('pickup_date');
        
        if(isset($pickup_date) && !empty($pickup_date)){
            $start_date = date("Y-m-d", mktime(0, 0, 0, $request->post('pickup_date'), 01));
            $end_date = date("Y-m-t", mktime(0, 0, 0, $request->post('pickup_date')));
        }
            
               
        // dd($start_date);die();
        $data_user = User::select('*')->where('id', $driver_id)->first();
        // dd($data_user->id);die();
        $check_is_exists = Invoice::where('user_id',$request->user_id)->where('start_date',$request->start_date)->where('end_date',$request->end_date)->exists();
        if($check_is_exists){
            $invoice->invoice_number = Str::random(8);
            $invoice->user_id = $data_user->id;
            $invoice->start_date = $start_date;
            $invoice->end_date = $end_date;
            $invoice->email = $request->input('email');
            $invoice->total_amount = 0;
            $invoice->status =  ($data_user->status == 1) ? '1' : '0';
            $invoice->save();
            $invoice['user_id'] = $data_user->id;
            $invoice = $invoice->id;
            $encrypted = base64_encode($data_user->id.".".$start_date.".".$end_date.".".$invoice);
            $link = url('driver-comission', $encrypted);;
            // Mail::send(new \App\Mail\SendMail( $link));
            $request->email=$request->email;
            $response_data['email'] = $request->email;
            $response_data['invoice_data'] = $invoice;
            $response_data['link'] = $link;
            dispatch(new \App\Jobs\DriverCommisionInvoiceJob($response_data));
        }else{
            $data_user = User::select('*')->where('id', $driver_id)->first();
            $invoice['user_id'] = $data_user->id;
            $invoice = $invoice->id;
            $encrypted = base64_encode($data_user->id.".".$start_date.".".$end_date.".".$invoice);
            $link = url('driver-comission', $encrypted);;
            // Mail::send(new \App\Mail\SendMail( $link));
            $request->email=$request->email;
            $response_data['email'] = $request->email;
            $response_data['invoice_data'] = $invoice;
            $response_data['link'] = $link;
            dispatch(new \App\Jobs\DriverCommisionInvoiceJob($response_data));
        }
        return redirect()->back()->with('success','Your Mail Send Successfully.',compact('pickup_date'));
    }

    



}
