<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Models\TruckCategory;
use App\Models\TruckType;

class TruckController extends Controller
{
    public function index(Request $request, $id = null) {
     
       
            
        if ($request->ajax()) {
        
            $data = TruckCategory::join('truck_type', 'truck_type_category.truck_type_id', '=', 'truck_type.id')->select('truck_type_category.*', 'truck_type.name as type');
            
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                    
                    if($row['status'] == 1)
                        $btn = '<a  class="text-success grab"  data-url="status/change" data-status="'. $row['status'] .'" data-table="truck_type_category" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-success">Active</span></a>';
                    else
                        $btn = '<a  class="text-danger grab"  data-url="status/change" data-status="'. $row['status'] .'" data-table="truck_type_category" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-danger">Inactive</span></a>';
                    
                    return $btn;
                })



                ->editColumn('action', function ($row) {
                    $btn = 
                    '<a class="text-danger grab btn btn-outline-danger btn-xs"  data-newurl="truck/category/delete"  data-url="type/delete" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="' . csrf_token() . '" ><i class="fa fa-trash"></i></a>
                        &nbsp;&nbsp;
                    <a href="' . route('admin.truck.category.index', $row['id']) . '" class="text-info grab btn btn-outline-info btn-xs"><i class="fa fa-pencil"></i></a>
                    ';
                    return $btn;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        } else {
            $type = TruckType::orderBy('id','desc')->where('status',1)->get();
            
            if(isset($id) && $id != null){
                $params['edit_param'] = TruckCategory::find($id);
            }
            
            $columns = [
                ['data' => 'id', 'name' => "Id", 'title' => __("Id")],
                ['data' => 'name', 'name' => "name", 'title' => __("Name")],
                ['data' => 'type', 'name' => "truck_type.name", 'title' => __("Truck Type")],
                ['data' => 'status', 'name' => "status", 'title' => __("Status"), 'searchable' => true, 'orderable' => true],
                ['data' => 'action', 'name' => "action", 'title' => __("Action"), 'searchable' => false, 'orderable' => false],
            ];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('admin.truck.category.index');
            $params['dateTableTitle'] = "Truck Category";
            $params['addUrl'] = "truck";
            $params['dataTableId'] = time();
            $params['type'] = $type;


            return view('admin.pages.truck_category.index', $params);
        }
       
    }

    public function store(Request $request) {
      
        $validator = \Validator::make($request->all(), [
            'truck_type_id' => 'required',
            'name' => 'required',
        ],[
            'truck_type_id.required' => 'This value is required.'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        TruckCategory::create($request->all());
        return redirect()->route('admin.truck.category.index')->with('success', __('Truck category created successfully.'));
       
    }

    public function update(Request $request, TruckCategory $category, $id) {

        $validator = \Validator::make($request->all(), [
            'truck_type_id' => 'required',
            'name' => 'required',
        ],[
            'truck_type_id.required' => 'This value is required.'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $request->all();
        $category = TruckCategory::whereId($id)->first();
        $category->fill($input)->save();
        return redirect()->route('admin.truck.category.index')->with('success', __('Truck category update successfully.'));
       
    }

    public function destroy($id) {
      
        TruckCategory::whereId($id)->delete();
        return \Response::json('success', 200);

    }

}
