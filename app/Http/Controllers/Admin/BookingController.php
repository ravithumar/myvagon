<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Booking;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\DriverVehicle;
use App\Models\VehicleCapacity;
use App\Helpers;
// use Booking;
use Laravel\Passport\Token; 
use DataTables;

class BookingController extends Controller
{
    public function index(Request $request) {
        // echo '<pre>';print_r('Test');die();

      
        #driver role identify
        // $role = Role::select('*')->where('name','carrier')->first();
        // $user_role = UserRole::select('user_id')->where('role_id', $role->id)->pluck('user_id');

        $data = Booking::with(['user','booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('id','desc');
        // echo '<pre>';print_r($data);die();

        if ($request->ajax()) {
           
            return Datatables::of($data)
                ->filter(function ($query) use ($request) {
                    if ($request->get('status') != '') {
                        $query->where('status', $request->get('status'));
                    }
                })
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                    $status = '-';   
                    if($row['status'] == 'create'){
                        $status = '<label class="created-status" style="padding: 5px 10px;">'.__("Created").'</label>';
                    }elseif($row['status'] == 'scheduled'){
                        $status = '<label class="scheduled-status" style="padding: 5px 10px;">'.__("Scheduled").'</label>';
                    }elseif($row['status'] == 'pending'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;">'.__("Pending").'</label>';
                    }elseif($row['status'] == 'in-process'){
                        $status = '<label class="in-process-status" style="padding: 5px 10px;">'.__("In-process").'</label>';
                    }elseif($row['status'] == 'completed'){
                        $status = '<label class="completed-status" style="padding: 5px 10px;">'.__("Completed").'</label>';
                    }else{
                        $status = '<label class="cancelled-status" style="padding: 5px 10px;">'.__("Cancelled").'</label>';
                    }
                    return $status;
                })
                // ->editColumn('pickup_date', function($row){
                //     $pickup_date = date('F d, Y',strtotime($row['pickup_date'])) .'<br>'.($row['pickup_time_from'] == $row['pickup_time_to'] ? $row['pickup_time_from'] : $row['pickup_time_from'].' - '.$row['pickup_time_to']);
                //     return $pickup_date;
                // })
                // ->addColumn('end_date',function($row){
                //     $end_date = '-';
                //     if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                //         $end_date = date('d/m/Y',strtotime($row['booking_location'][0]['delivered_at'])).'<br>'.($row['pickup_time_from'] == $row['pickup_time_to'] ? $row['pickup_time_from'] : $row['pickup_time_from'].' - '.$row['pickup_time_to']);
                //     }
                //     return $end_date;
                // })
                ->addColumn('start_date',function($row){
                    $start_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $start_date = date('d/m/Y',strtotime($row['pickup_date'])) .'<br> TO <br>'. date('d/m/Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $start_date;
                })
                ->addColumn('drop_location',function($row){
                    $drop_location = '-';
                    if(isset($row) && !empty($row)){
                        $company_name = isset($row['booking_location'][0]['company_name']) ? $row['booking_location'][0]['company_name'] : "-";
                        $drop_location = isset($row['booking_location'][0]['drop_location'])?$row['booking_location'][0]['drop_location']:"-";
                        $drop_location = '<a href="'.url('admin/shipper/shipment-detail',$row['id']).'" style="color:#6c757d"><b>'.$company_name.'</b><br>'.$drop_location.'</a>';
                    }
                    return $drop_location;
                })
                ->editColumn('action', function ($row) {
                    $pickup_address = "'" . $row['pickup_address'] . "'";
                    $pickup_lat = "'" . $row['pickup_lat'] . "'";
                    $pickup_lng = "'" . $row['pickup_lng'] . "'";
                    $drop_location = "'" . $row['booking_location'][0]['drop_location'] . "'";
                    $drop_lat = 0;
                    $drop_lng = 0;
                    if(isset($row['booking_location'][0]['drop_lat']) && !empty($row['booking_location'][0]['drop_lat'])){
                        $drop_lat = "'" . $row['booking_location'][0]['drop_lat'] . "'";
                    }
                    if(isset($row['booking_location'][0]['drop_lng']) && !empty($row['booking_location'][0]['drop_lng'])){
                        $drop_lng = "'" . $row['booking_location'][0]['drop_lng'] . "'";
                    }

                    if($row['status'] != 'pending' || $row['status'] != 'created'){
                        $btn = '
                        &nbsp;&nbsp;<a href="javascript:void(0)"  onclick="delete_notiflix(this);return false;" data-newurl="booking/delete" data-id="'.$row['id'].'" data-token="'. csrf_token() .'" style="background-color: white;color: black;" class="text-danger btn btn-outline-danger btn-xs text-default justify-content-end "> <i class="fa fa-trash"></i></a>
                        &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                    }else{
                        $btn = '
                        &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                    }
                    return $btn;
                })
                ->editColumn('name', function($row){
                    $name = '-';
                    if(isset($row['user']['name']) && !empty($row['user']['name'])){
                        $name = '<a href="'.route('admin.shipper.view',$row['user_id']).'">'.$row['user']['name'].'</a>';
                    }
                    return $name;
                })
                ->rawColumns(['status', 'action', 'pickup_date','start_date','drop_location','name'])

                ->make(true);
        } else {
            $columns = [
                            ['data' => 'id', 'name' => "id",'title' => __("Order Id"),'width'=>'60px'], 
                            ['data' => 'name', 'name' => "name",'title' => __("Shipper Name"),'width'=>'100px'],
                            // ['data' => 'user.email', 'name' => __("Email"),'title' => __("Email")],
                            // ['data' => 'user.phone', 'name' => __("Phone"),'title' => __("Phone")],
                            // ['data' => 'pickup_date', 'name' => "pickup_date",'title' => __("Start Date"),'width'=>'100px'],
                            ['data' => 'start_date', 'name' => "start_date",'title' => __("Date"),'width'=>'100px'],
                            ['data' => 'drop_location', 'name' => "drop_location",'title' => __("Delivery Address"),'width'=>'150px'],
                            ['data' => 'status', 'name' => __("Status"), 'title' => __("Status"),'width'=>'80px', 'searchable' => true, 'orderable' => true],
                            ['data' => 'action', 'name' =>__("Action"),'title'=>__("Action"), 'searchable' => false, 'orderable' => false]
                       ];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('admin.booking.index');
            $params['dateTableTitle'] = "Booking";
            $params['addUrl'] = "booking";
            $params['dataTableId'] = time();
            return view('admin.pages.booking.index', $params);
        }
    }

     public function destroy($id) {
        Booking::whereId($id)->delete();
        return \Response::json('success', 200);

    }

}
