<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SystemConfig;
use App\Models\ApplicationConfig;
use Illuminate\Http\Request;
use App\Models\TruckCategory;
use App\Models\TruckType;
use App\Models\TruckUnit;
use App\Models\PackageType;
use CommonHelper;

class SystemConfigController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['title'] = __('System Configuration');
        return view('admin.pages.system_config', $data);
    }

    public function app() {
        $data['title'] = __('Application Configuration');
        $data['app'] =ApplicationConfig::all();
        // echo $data['app'][0]->force_update;exit;
        return view('admin.pages.app_config', $data);
    }

    public function submit_app(Request $request)
    {
        // $request = $request;
        $android_customer['version']=$request['android_customer_version'];
        $android_customer['force_update']=isset($request['android_customer_update'])?1:0;
        $android_customer['maintenance']=isset($request['customer_maintenance'])?1:0;
        // _pre($android_customer);
        ApplicationConfig::where('type','android_customer')->update($android_customer);
        $ios_customer['version']=$request['ios_customer_version'];
        $ios_customer['force_update']=isset($request['ios_customer_update'])?1:0;
        $ios_customer['maintenance']=isset($request['customer_maintenance'])?1:0;
        ApplicationConfig::where('type','ios_customer')->update($ios_customer);
        $android_driver['version']=$request['android_driver_version'];
        $android_driver['force_update']=isset($request['android_driver_update'])?1:0;
        $android_driver['maintenance']=isset($request['driver_maintenance'])?1:0;
        ApplicationConfig::where('type','android_driver')->update($android_driver);
        $ios_driver['version']=$request['ios_driver_version'];
        $ios_driver['force_update']=isset($request['ios_driver_update'])?1:0;
        $ios_driver['maintenance']=isset($request['driver_maintenance'])?1:0;
        ApplicationConfig::where('type','ios_driver')->update($ios_driver);
        return redirect()->route('admin.app.config')->with('success', __('App Configuration details updated successfully.'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SystemConfig  $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function show(SystemConfig $systemConfig) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SystemConfig  $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(SystemConfig $systemConfig) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SystemConfig  $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $params = $request->all();  
        unset($params['_token']);
        if(!empty($params))
        {
            foreach ($params as $key => $value) {
                $key_array[$key] = $value;
            }
            CommonHelper::ConfigSet($key_array);
        }
        return redirect()->route('admin.system.config')->with('success','System Configuration successfully saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SystemConfig  $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(SystemConfig $systemConfig) {
        //
    }
    public function index_system(Request $request){
        $type = TruckType::first()->all();
        $base = env('APP_URL') . 'images/type/';
         $package = PackageType::select('*')->get();
         // dd($package);
         $columns = [
                            ['data' => 'id', 'name' => "id",'title' => __("Id")], 
                            ['data' => 'name', 'name' => 'company_name','title' => __("Name")],
                           ['data' => 'Image', 'name' => __("image"),'title' => __("Image")],
                            
                       ];
        $data = TruckCategory::join('truck_type', 'truck_type_category.truck_type_id', '=', 'truck_type.id')->select('truck_type_category.*', 'truck_type.name as type')->get()->all();
         
                $params['dateTableTitle'] = "System Section";
                $params['dateTableFields'] = $columns;
                $params['dataTableId'] = time();
                $params['base'] = $base;
                $params['truck_unit'] = TruckUnit::get();
                $params['dateTableUrl'] = route('admin.system.config');
                return view('admin.pages.system.index',$params,compact('type','data','package'));
            
        
        
    }
}
