<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Role;
use App\Helpers; 
use App\Models\Facility;
use App\Models\Booking;
use App\Models\BookingLocations;
use App\Models\BookingTruck;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use App\Models\Invoice;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DataTables;
use App\Mail\SendMail;
use Mail;
use Illuminate\Support\Facades\Crypt;
use Twilio\Tests\Request as TestsRequest;

class ShipperController extends Controller
{
    public function index(Request $request) {

      
        // shipper role identify
        $role = Role::select('*')->where('name','shipper')->first();
        $user_role = UserRole::select('user_id')->where('role_id', $role->id)->pluck('user_id');
        $data = User::select('*')->whereIn('id', $user_role)->orderBy('id','desc');

        if ($request->ajax()) {
           
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                       
                    if($row['status'] == 1)
                        $btn = '<a  class="text-success grab"  data-url="status/change"  data-table="users" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-success">Active</span></a>';
                    else
                        $btn = '<a  class="text-danger grab" data-url="status/change" data-table="users" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-danger">Inactive</span></a>';
                    
                    return $btn;
                })

                ->editColumn('action', function ($row) {
                    /* $btn = '
                     <a  href="' . route('admin.shipper.view', $row['id']) . '" class="text-primary grab btn btn-outline-primary btn-xs"><i class="fas fa-search"></i></a>
                    &nbsp;&nbsp;&nbsp;<a  href="' . url('admin/shipper/shipment', $row['id']) . '" class="text-dark">View Shipment</a>
                    '; */
                    $btn = '
                        <a  href="' . url('admin/shipper/shipment', $row['id']) . '" class="text-dark">View Details</a>
                    ';
                    return $btn;
                })
                ->editColumn('created_at',function($row){
                    return date('d/m/Y',strtotime($row['created_at']));
                })
                ->addColumn('no_of_shipment',function($row){
                    $number_of_shipment = Booking::where(['user_id' => $row['id']])->count();
                    $response = '<span style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px;padding:0px 15px" class="btn">0</span>';
                    
                    if(isset($number_of_shipment) && !empty($number_of_shipment)){
                        // $response = $number_of_shipment;
                        $response = '<span style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px;padding:0px 15px" class="btn">'.$number_of_shipment.'</span>';
                    }
                    return $response;
                })
                ->rawColumns(['status', 'action', 'no_of_shipment'])

                ->make(true);
        } else {
            $columns = [
                        ['data' => 'id', 'name' => "id",'title' => __("Id")], 
                        ['data' => 'company_name', 'name' => 'company_name','title' => __("Company Name")],
                        //    ['data' => 'name', 'name' => __("Name"),'title' => __("Name")],
                        ['data' => 'created_at', 'name' => 'created_at','title' => __("Date Joined")],
                        ['data' => 'status', 'name' => 'status', 'title' => __("Status"), 'searchable' => true, 'orderable' => false],
                        ['data' => 'no_of_shipment', 'name' => 'no_of_shipment', 'title' => __("Shipments"), 'searchable' => false, 'orderable' => false],
                        ['data' => 'action', 'name' => 'action','title'=>__("Action"), 'searchable' => false, 'orderable' => false]
                       ];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('admin.shipper.index');
            $params['dateTableTitle'] = "Shipper";
            $params['addUrl'] = "shipper";
            $params['dataTableId'] = time();
            return view('admin.pages.shipper.index', $params);
        }
    }

    public function approveStatus($id, $status){
       
        $data = User::find($id);
        if($status == "1")
            $data->stage = "accepted" ;
        else
            $data->stage = "rejected" ;
        $data->save(); 
        if($status == "1"){
            $data = User::find($id);
             dispatch(new \App\Jobs\ShipperAcceptProfileJob($data));
        }
       
        return \Response::json($data, 200);
    }
    public function view_profile(Request $request, $id){
        $data_user = User::with(['country_name','state_name','city_name'])->select('*')->where('id', $id)->first();
        // dd($data_user);
        if(isset($data_user) && !empty($data_user)){
            $facility = Facility::where('user_id', $data_user->id)->first();
            $multiple_data = Facility::where('user_id', $data_user->id)->with('city_obj')->get();
            
                
                $params['dateTableUrl'] = route('admin.shipper.view', $data_user->id);
                $params['dateTableTitle'] = "Company Profile";
                $params['data'] = $data_user;
                $params['facility'] = $facility;
                $params['addUrl'] = "shipper";
                $params['multiple_data'] = $multiple_data;
                $params['dataTableId'] = time();
            
                return view('admin.pages.shipper.view_profile', $params);
        }else{
            return redirect()->back()->with("error",'Shipper details not available. please try again later.');
        }
    }

    public function view(Request $request, $id){
        $data_user = User::with(['country_name','state_name','city_name'])->select('*')->where('id', $id)->first();
        // dd($data_user);
        if(isset($data_user) && !empty($data_user)){
            $facility = Facility::where('user_id', $data_user->id)->first();
            $multiple_data = Facility::where('user_id', $data_user->id)->with('city_obj')->get();
            
                $columns = [
                                ['data' => 'id', 'name' => "Id",'title' => __("Id")], 
                                ['data' => 'name', 'name' => __("Name"),'title' => __("Name")],
                                ['data' => 'address', 'name' => __("Address"),'title' => __("Address")],
                                
                ];
                $params['dateTableFields'] = $columns;
                $params['dateTableUrl'] = route('admin.shipper.view', $data_user->id);
                $params['dateTableTitle'] = "shipper";
                $params['data'] = $data_user;
                $params['facility'] = $facility;
                $params['addUrl'] = "shipper";
                $params['multiple_data'] = $multiple_data;
                $params['dataTableId'] = time();
            
                return view('admin.pages.shipper.view', $params);
        }else{
            return redirect()->back()->with("error",'Shipper details not available. please try again later.');
        }
    }

    public function update($id, Request $request, User $user){

        $validator = \Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'commission_fee' => '',
            'phone' => '',
            'profile' => 'file'
        ],[
            'first_name.required' => 'Please enter first name.',
            'last_name.required' => 'Please enter last name.',
            'profile.required' => 'Please select profile image.',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $request->all();
        $user = User::where('id',$id)->first();
       

        if($request->has('profile')){
            $img = Helpers\CommonHelper::uploadImagesS3Bucket($request->file('profile'),'shipper/');
            // $img = Helpers\CommonHelper::imageUpload($request->profile, 'images/shipper');
            $user->profile = $img;
        }

        $user->name = $request->input('first_name').' '.$request->input('last_name');
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->commission_fee = $request->input('commission_fee');
        $user->phone = $request->input('phone');
        $user->update();
        return redirect()->route('admin.shipper.view', $user->id)->with('success', __('Shipper content update successfully.'));
    }

    public function shipment(Request $request,$shipper_id)
    {
          /*dd($request->all());*/
       //   $validator = Validator::make($request->all(), [
       //      'commission_fee' => 'required',
       //      ]);
         
       //  $data = $request->commission_fee;
       //  // dd($request->commission_fee);
       //  $role = Role::select('*')->where('name','shipper')->first();
       //  $role_user = UserRole::where('role_id',$role->id)->get(); 
       // // if(!empty($role_user))
       // // {
       // //  foreach ($role_user as $key => $value) {
       // //     $user_fee = User::where('id',$value['user_id'])->update(['commission_fee' => '45']);
       // //  }
       // // }
       // $user_fee = User::where('id',$shipper_id)->first();
       // // dd($user_fee);
       // $user_fee->commission_fee = $data;
       // $user_fee->save(); 
       // \Session::flash('flash_message','successfully saved.');
       // echo '<pre>';print_r($user_fee);die();
          // print_r($shipper_id);
        $user =   Auth::user();
        
        $data_user = User::with(['country_name','state_name','city_name'])->select('*')->where('id', $shipper_id)->first();
        $data_manager = User::where(['type' => 'manager','company_ref_id' => $data_user->id])->orderBy('id','desc')->get();
        // dd($data_manager);
        $invoice = Invoice::get()->all();
        $products = Product::where('user_id',$data_user->id)->get();
        $data = User::where('user_id', $data_user->id)->orderBy('id','desc');
        
        if(isset($data_user) && !empty($data_user)){
            $facility = Facility::where('user_id', $data_user->id)->first();
            $multiple_data = Facility::where('user_id', $data_user->id)->with('city_obj')->get();
        }else{
            return redirect()->back()->with("error",'Shipper details not available. please try again later.');
        }
        $url_param = $request->input('status');
        $search_param = $request->input('search');
        if ($request->ajax()) {
            $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('id','desc')->where('user_id',$shipper_id);
            $user =   Auth::user();
            $search_param = $request->post('searching_data');
            // return DataTables::of($data)
            if($request->sorting_date=='created_at'){
                if($request->zero_value==0){
                    $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('created_at','ASC')->where('user_id',$shipper_id);
                }else{
                    $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','asc');}])->select('*')->orderBy('created_at','DESC')->where('user_id',$shipper_id);
                }
            }
                if($request->sorting_date=='created_at'){
                    if($request->zero_value==0){
                         $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('created_at','ASC')->where('user_id',$shipper_id)->get();
                     }else{
                        $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('created_at','DESC')->where('user_id',$shipper_id)->get();
                     }
                }
                elseif($request->sorting_date=='pickup_date'){

                    if($request->zero_value==0){
                        $data = Booking::orderBy('pickup_date','asc')->where('user_id',$shipper_id)->get();
                    }else{
                        $data = Booking::orderBy('pickup_date','desc')->where('user_id',$shipper_id)->get();
                    }
                }
                elseif($request->sorting_date=='complete_date'){
                     if($request->zero_value==0){
                        $data = Booking::with(['booking_location' => function ($query1) {
                            $query1->orderBy('delivered_at', 'asc')->latest();
                        }])->orderBy('created_at','asc')->where('user_id',$shipper_id)->get();
                    }else {
                            $data = Booking::with(['booking_location' => function ($query1) {
                            $query1->orderBy('delivered_at', 'desc')->latest();
                        }])->orderBy('created_at','desc')->where('user_id',$shipper_id)->get();
                    }
                }
            else{
               $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])
                ->select('*')->orderBy('id','desc')
                ->where(function($query) use($shipper_id,$search_param){

                    $query->where('user_id',$shipper_id);
                    if(!empty($search_param) && isset($search_param)){
                        $query->where('pickup_address', 'LIKE', "%{$search_param}%");
                    }
                });
            }
            return DataTables::of($data)
                ->filter(function ($query) use ($request) {
                    if ($request->get('status') != '') {
                        $query->where('status', $request->get('status'));
                    }
                })
                ->addIndexColumn()
                ->editColumn('action', function ($row) {
                    $pickup_address = "'" . $row['pickup_address'] . "'";
                    $pickup_lat = "'" . $row['pickup_lat'] . "'";
                    $pickup_lng = "'" . $row['pickup_lng'] . "'";
                    $drop_location = "'" . $row['booking_location'][0]['drop_location'] . "'";
                    $drop_lat = 0;
                    $drop_lng = 0;
                    if(isset($row['booking_location'][0]['drop_lat']) && !empty($row['booking_location'][0]['drop_lat'])){
                        $drop_lat = "'" . $row['booking_location'][0]['drop_lat'] . "'";
                    }
                    if(isset($row['booking_location'][0]['drop_lng']) && !empty($row['booking_location'][0]['drop_lng'])){
                        $drop_lng = "'" . $row['booking_location'][0]['drop_lng'] . "'";
                    }

                    if($row['status'] != 'pending' || $row['status'] != 'created'){
                        // $btn = '
                        // &nbsp;&nbsp;<a href="javascript:void(0)"  onclick="delete_shipment(this);return false;" data-newurl="manage-shipment/delete" data-id="'.$row['id'].'" data-token="'. csrf_token() .'" style="background-color: white;color: black;" class="text-danger btn btn-outline-danger btn-xs text-default justify-content-end "> <i class="fa fa-trash"></i></a>
                        // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                        
                        $btn = '<a href="'.url('admin/shipper/shipment-detail',$row['id'] ).'" style="color:#9B51E0;text-decoration:underline;font-weight: bold;"><b>'.'View Details'.'</b></a>';
                    }else{
                        // $btn = '
                        // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                    }
                    return $btn;

                })
                ->addColumn('shipment_type', function ($row){
                    if($row['is_public'] == '1'){
                        $shipment_type = 'Direct     Public';
                    }else{
                        $shipment_type = 'Direct      Private';
                    }
                    if($row['booking_type'] == 'multiple_shipment'){
                        if($row['is_public'] == '1'){
                            $shipment_type = 'Multiple      Public';
                        }else{
                            $shipment_type = 'Multiple     Private';
                        }
                    }
                    return $shipment_type;

                })
                 ->addColumn('start_date',function($row){
                    $start_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $start_date = date('d/m/Y',strtotime($row['pickup_date'])) . ' TO ' . date('d/m/Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $start_date;
                })
                ->addColumn('end_date',function($row){
                    $end_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $end_date = date('F d, Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $end_date;
                })
                ->addColumn('drop_location',function($row){
                    $drop_location = '-';
                    if(isset($row) && !empty($row)){
                        $company_name = isset($row['booking_location'][0]['company_name']) ? $row['booking_location'][0]['company_name'] : "-";
                        $drop_location = isset($row['booking_location'][0]['drop_location'])?$row['booking_location'][0]['drop_location']:"-";
                        // $drop_location = '<a href="'.url('admin/shipper/shipment-detail',$row['id']).'" style="color:#6c757d"><b>'.$company_name.'</b><br>'.$drop_location.'</a>';
                    }
                    return $drop_location;
                })
                ->editColumn('amount',function($row){
                    return '€ '.$row['amount'];
                })
                ->editColumn('status',function($row){
                    $status = '';
                    if($row['status'] == 'create'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        $status .= '<br><small class="text-danger">'.__("Waiting for truck").'</small>';
                    }elseif($row['status'] == 'scheduled'){
                        $status = '<label class="scheduled-status" style="padding: 5px 10px;">'.__("Scheduled").'</label>';
                    }elseif($row['status'] == 'pending'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        if($row['is_bid'] == '1'){
                            $status .= '<br><small class="text-danger">'.__("Waiting for bid").'</small>';
                        }else{
                            $status .= '<br><small class="text-danger">'.__("Waiting for book").'</small>';
                        }
                    }elseif($row['status'] == 'in-process'){
                        $status = '<label class="in-process-status" style="padding: 5px 10px;">'.__("In-process").'</label>';
                    }elseif($row['status'] == 'completed'){
                        $status = '<label class="completed-status" style="padding: 5px 10px;">'.__("Completed").'</label>';
                    }else{
                        $status = '<label class="cancelled-status" style="padding: 5px 10px;">'.__("Cancelled").'</label>';
                    }
                    return $status;
                })

                ->rawColumns(['action','status','drop_location' ])
                ->make("*");
        } else {
            $columns = [
                            ['data' => 'shipment_type', 'name' => "shipment_type",'title' => __("Type"),'width'=>'10%','searchable' => false, 'orderable' => false], 
                            ['data' => 'id', 'name' => "id",'title' => __("Order Id"),'width'=>'60px'], 
                            ['data' => 'amount', 'name' => __("amount"),'title' => __("Price"),'width'=>'60px'],
                            ['data' => 'status', 'name' => __("status"),'title' => __("Status"),'width'=>'80px'],
                            ['data' => 'start_date', 'name' => __("pickup_date"),'title' => __("Date"),'width'=>'15%','searchable' => false, 'orderable' => false],
                            // ['data' => 'end_date', 'name' => __("end_date"),'title' => __("End Date"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                            ['data' => 'pickup_address', 'name' => __("pickup_address"),'title' => __("Pickup Location"),'width'=>'17.5%', 'searchable' => false, 'orderable' => false],
                            ['data' => 'drop_location', 'name' => __("drop_location"),'title' => __("Delivery Location"),'width'=>'17.5%','searchable' => false, 'orderable' => false],
                            ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                       ];
            }
            
            $shipper_id = $shipper_id;
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = url('admin/shipper/shipment',$shipper_id);
            $params['dateTableTitle'] = "View Shipments";
            $params['addUrl'] = "manage-shipment";
            $params['ChatheadingTitle'] = "Chat/Support";
            $params['ActiveheadingTitle'] = "Active/Deactive";
            $params['data'] = $data_user;
            $params['dataTableId'] = time();
            $params['headingTitle'] = "Company Profile  ";
            $params['ManagerheadingTitle'] = "Company Manager";
            $params['PaymentheadingTitle'] = "Payment Section";
            $params['FacilitiesheadingTitle'] = "Company Facilities";
            $params['ProductheadingTitle'] = "Company Product Master";
            $params['multiple_data'] = $multiple_data;

            return view('admin.pages.shipper.shipment_list', $params,compact('data_user','products','data_manager','user','shipper_id','invoice'))->with('success',__('Commission added Successfully'));

    }

    public function commission_fee(Request $request,$shipper_id){
        $validator = Validator::make($request->all(), [
            'commission_fee' => 'required',
            ]);
         
        $data = $request->commission_fee;
        // dd($request->commission_fee);
        $role = Role::select('*')->where('name','shipper')->first();
        $role_user = UserRole::where('role_id',$role->id)->get(); 
       // if(!empty($role_user))
       // {
       //  foreach ($role_user as $key => $value) {
       //     $user_fee = User::where('id',$value['user_id'])->update(['commission_fee' => '45']);
       //  }
       // }
       $user_fee = User::where('id',$shipper_id)->first();
       // dd($user_fee);
       $user_fee->commission_fee = $data;
       $user_fee->save(); 
       
       return redirect()->back()->with('success', 'Commission Fee Added Successfully.!'); 
    }

    public function demo_shipment(Request $request,$shipper_id)
    {
        /*dd($request->all());*/
       //   $validator = Validator::make($request->all(), [
       //      'commission_fee' => 'required',
       //      ]);
         
       //  $data = $request->commission_fee;
       //  // dd($request->commission_fee);
       //  $role = Role::select('*')->where('name','shipper')->first();
       //  $role_user = UserRole::where('role_id',$role->id)->get(); 
       // // if(!empty($role_user))
       // // {
       // //  foreach ($role_user as $key => $value) {
       // //     $user_fee = User::where('id',$value['user_id'])->update(['commission_fee' => '45']);
       // //  }
       // // }
       // $user_fee = User::where('id',$shipper_id)->first();
       // // dd($user_fee);
       // $user_fee->commission_fee = $data;
       // $user_fee->save(); 
       // \Session::flash('flash_message','successfully saved.');
       // echo '<pre>';print_r($user_fee);die();
        $user =   Auth::user();
       
        $data_user = User::with(['country_name','state_name','city_name'])->select('*')->where('id', $shipper_id)->first();
        $data_manager = User::where(['type' => 'manager','company_ref_id' => $data_user->id])->orderBy('id','desc')->get();
        // dd($data_manager);
        $products = Product::where('user_id',$data_user->id)->get();
        $data = User::where('user_id', $data_user->id)->orderBy('id','desc');
        
        if(isset($data_user) && !empty($data_user)){
            $facility = Facility::where('user_id', $data_user->id)->first();
            $multiple_data = Facility::where('user_id', $data_user->id)->with('city_obj')->get();
        }else{
            return redirect()->back()->with("error",'Shipper details not available. please try again later.');
        }
        if ($request->ajax()) {
            $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('id','desc')->where('user_id',$shipper_id);
            $user =   Auth::user();
            if($request->sorting_date=='created_at'){
                if($request->zero_value==0){
                    $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('created_at','ASC')->where('user_id',$shipper_id);
                }else{
                    $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','asc');}])->select('*')->orderBy('created_at','DESC')->where('user_id',$shipper_id);
                }
            }
                if($request->sorting_date=='created_at'){
                    if($request->zero_value==0){
                         $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('created_at','ASC')->where('user_id',$shipper_id)->get();
                     }else{
                        $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->select('*')->orderBy('created_at','DESC')->where('user_id',$shipper_id)->get();
                     }
                }
                elseif($request->sorting_date=='pickup_date'){

                    if($request->zero_value==0){
                        $data = Booking::orderBy('pickup_date','asc')->where('user_id',$shipper_id)->get();
                    }else{
                        $data = Booking::orderBy('pickup_date','desc')->where('user_id',$shipper_id)->get();
                    }
                }
                elseif($request->sorting_date=='complete_date'){
                     if($request->zero_value==0){
                        $data = Booking::with(['booking_location' => function ($query1) {
                            $query1->orderBy('delivered_at', 'asc')->latest();
                        }])->orderBy('created_at','asc')->where('user_id',$shipper_id)->get();
                    }else {
                            $data = Booking::with(['booking_location' => function ($query1) {
                            $query1->orderBy('delivered_at', 'desc')->latest();
                        }])->orderBy('created_at','desc')->where('user_id',$shipper_id)->get();
                    }
                }
            else{
               $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])
                ->select('*')->orderBy('id','desc')
                ->where(function($query) use($shipper_id,$search_param){

                    $query->where('user_id',$shipper_id);
                    if(!empty($search_param) && isset($search_param)){
                        $query->where('pickup_address', 'LIKE', "%{$search_param}%");
                    }
                });
            }
            // return DataTables::of($data)
            return DataTables::of($data)
                ->filter(function ($query) use ($request) {
                    if ($request->get('status') != '') {
                        $query->where('status', $request->get('status'));
                    }
                })
                ->addIndexColumn()
                ->editColumn('action', function ($row) {
                    $pickup_address = "'" . $row['pickup_address'] . "'";
                    $pickup_lat = "'" . $row['pickup_lat'] . "'";
                    $pickup_lng = "'" . $row['pickup_lng'] . "'";
                    $drop_location = "'" . $row['booking_location'][0]['drop_location'] . "'";
                    $drop_lat = 0;
                    $drop_lng = 0;
                    if(isset($row['booking_location'][0]['drop_lat']) && !empty($row['booking_location'][0]['drop_lat'])){
                        $drop_lat = "'" . $row['booking_location'][0]['drop_lat'] . "'";
                    }
                    if(isset($row['booking_location'][0]['drop_lng']) && !empty($row['booking_location'][0]['drop_lng'])){
                        $drop_lng = "'" . $row['booking_location'][0]['drop_lng'] . "'";
                    }

                    if($row['status'] != 'pending' || $row['status'] != 'created'){
                        // $btn = '
                        // &nbsp;&nbsp;<a href="javascript:void(0)"  onclick="delete_shipment(this);return false;" data-newurl="manage-shipment/delete" data-id="'.$row['id'].'" data-token="'. csrf_token() .'" style="background-color: white;color: black;" class="text-danger btn btn-outline-danger btn-xs text-default justify-content-end "> <i class="fa fa-trash"></i></a>
                        // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                        
                        $btn = '<a href="'.url('admin/shipper/shipment-detail',$row['id'] ).'" style="color:#9B51E0;text-decoration:underline;font-weight: bold;"><b>'.'View Details'.'</b></a>';
                    }else{
                        // $btn = '
                        // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                    }
                    return $btn;

                })
                ->addColumn('start_date',function($row){
                    $start_date = '-';
                    if(isset($row) && !empty($row)){
                        $start_date = date('F d, Y',strtotime($row['pickup_date']));
                    }
                    return $start_date;
                })
                ->addColumn('end_date',function($row){
                    $end_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $end_date = date('F d, Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $end_date;
                })
                ->addColumn('drop_location',function($row){
                    $drop_location = '-';
                    if(isset($row) && !empty($row)){
                        $company_name = isset($row['booking_location'][0]['company_name']) ? $row['booking_location'][0]['company_name'] : "-";
                        $drop_location = isset($row['booking_location'][0]['drop_location'])?$row['booking_location'][0]['drop_location']:"-";
                        // $drop_location = '<a href="'.url('admin/shipper/shipment-detail',$row['id']).'" style="color:#6c757d"><b>'.$company_name.'</b><br>'.$drop_location.'</a>';
                    }
                    return $drop_location;
                })
                ->editColumn('amount',function($row){
                    return '€ '.$row['amount'];
                })
                ->editColumn('status',function($row){
                    $status = '';
                    if($row['status'] == 'create'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        $status .= '<br><small class="text-danger">'.__("Waiting for truck").'</small>';
                    }elseif($row['status'] == 'scheduled'){
                        $status = '<label class="scheduled-status" style="padding: 5px 10px;">'.__("Scheduled").'</label>';
                    }elseif($row['status'] == 'pending'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        if($row['is_bid'] == '1'){
                            $status .= '<br><small class="text-danger">'.__("Waiting for bid").'</small>';
                        }else{
                            $status .= '<br><small class="text-danger">'.__("Waiting for book").'</small>';
                        }
                    }elseif($row['status'] == 'in-process'){
                        $status = '<label class="in-process-status" style="padding: 5px 10px;">'.__("In-process").'</label>';
                    }elseif($row['status'] == 'completed'){
                        $status = '<label class="completed-status" style="padding: 5px 10px;">'.__("Completed").'</label>';
                    }else{
                        $status = '<label class="cancelled-status" style="padding: 5px 10px;">'.__("Cancelled").'</label>';
                    }
                    return $status;
                })

                ->rawColumns(['action','status','drop_location' ])
                ->make("*");
        } else {
            $columns = [
                            ['data' => 'id', 'name' => "id",'title' => __("S.No."),'width'=>'20px'], 
                            ['data' => 'id', 'name' => "id",'title' => __("Order Id"),'width'=>'60px'], 
                            ['data' => 'amount', 'name' => __("amount"),'title' => __("Price"),'width'=>'60px'],
                            ['data' => 'status', 'name' => __("status"),'title' => __("Status"),'width'=>'80px'],
                            ['data' => 'start_date', 'name' => __("pickup_date"),'title' => __("Start Date"),'width'=>'100px'],
                            ['data' => 'end_date', 'name' => __("end_date"),'title' => __("End Date"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                            ['data' => 'drop_location', 'name' => __("drop_location"),'title' => __("Delivery Location"),'width'=>'150px'],
                            ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ],
                       ];
            }
            
            $shipper_id = $shipper_id;
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = url('admin/shipper/shipment',$shipper_id);
            $params['dateTableTitle'] = "View Shipments";
            $params['addUrl'] = "manage-shipment";
            $params['ChatheadingTitle'] = "Chat/Support";
            $params['ActiveheadingTitle'] = "Active/Deactive";
            $params['data'] = $data_user;
            $params['dataTableId'] = time();
            $params['headingTitle'] = "Company Profile : ";
            $params['ManagerheadingTitle'] = "Company Manager";
            $params['PaymentheadingTitle'] = "Payment Section";
            $params['FacilitiesheadingTitle'] = "Company Facilities";
            $params['ProductheadingTitle'] = "Company Product Master";
            $params['multiple_data'] = $multiple_data;

            return view('admin.pages.shipper.demo_shipment_list', $params,compact('data_user','products','data_manager','user','shipper_id'))->with('success',__('Commission added Successfully'));

    }
    public function generate_invoice(Request $request){
        $role = Role::select('*')->where('name','shipper')->first();
        $user_role = UserRole::select('user_id')->where('role_id', $role->id)->pluck('user_id');
        $data = User::select('*')->whereIn('id', $user_role)->orderBy('id','desc');

        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                    if($row['status'] == 1)
                        $btn = '<a  class="text-success grab"  data-url="status/change"  data-table="users" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-success">Active</span></a>';
                    else
                        $btn = '<a  class="text-danger grab" data-url="status/change" data-table="users" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-danger">Inactive</span></a>';
                    
                    return $btn;
                })
                ->editColumn('action', function ($row) {
                    $btn = '
                     
                    <a  href="' . url('admin/shipper/generate_invoice/get_invoice',$row['id']) . '" class="text-dark">Get Invoice</a>
                    ';
                    return $btn;
                })
                ->editColumn('created_at', function($row) {
                    return date('d-m-Y H:i:s',strtotime($row['created_at']));
                })
                ->rawColumns(['status', 'action'])

                ->make(true);
        } else {
            $columns = [
                            ['data' => 'id', 'name' => "id",'title' => __("Id")], 
                            ['data' => 'company_name', 'name' => 'company_name','title' => __("Company Name")],
                           ['data' => 'name', 'name' => __("Name"),'title' => __("Name")],
                            ['data' => 'status', 'name' => 'status', 'title' => __("Status"), 'searchable' => true, 'orderable' => false],
                            ['data' => 'created_at', 'name' => 'created_at','title' => __("Date Joined")],
                            ['data' => 'action', 'name' => 'action','title'=>__("Action"), 'searchable' => false, 'orderable' => false]
                       ];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = url('admin/shipper/generate_invoice');
            $params['invoiceTitle'] = "Generate invoice";
            $params['addUrl'] = "shipper";
            $params['dataTableId'] = time();
            return view('admin.pages.shipper.generate_invoice',$params);
        }
    }
    public function get_invoice(Request $request,$shipper_id){
        $data_user = User::with(['country_name','state_name','city_name'])->select('*')->where('id', $shipper_id)->first();
        $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->where('user_id',$shipper_id)->Orwhere('status','completed' || 'status','cancelled' || 'payment_status' , 'completed')->where('status' , '!==' , 'scheduled')->get();
        if(isset($data_user) && !empty($data_user)){
            $facility = Facility::where('user_id', $data_user->id)->first();
            $multiple_data = Facility::where('user_id', $data_user->id)->with('city_obj')->get();
        }else{
            return redirect()->back()->with("error",'Shipper details not available. please try again later.');
        }
                $month = ['January','February','March','April','May','June','July','August','September','October','November','December'];

            // dd($month);                
                                // dd($data);
                // $data = Booking::select(DB::raw("(COUNT(*)) as count"),DB::raw("MONTHNAME(pickup_date) as monthname"))
                //             ->whereYear('pickup_date', date('Y'))
                //             ->groupBy('monthname')->with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->where('user_id',$shipper_id)->get()->toArray();
                // dd($data);
        // $month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        $url_param = $request->input('status');
        $search_param = $request->input('search');
        if ($request->ajax()) {

            $search_param = $request->post('searching_data');
            $user =   Auth::user();
            $start_date = '';
            $end_date = '';
            $pickup_date = $request->post('pickup_date');
            if(isset($pickup_date) && !empty($pickup_date)){
            $start_date = date("Y-m-d", mktime(0, 0, 0, $request->post('pickup_date'), 01));
            $end_date = date("Y-m-t", mktime(0, 0, 0, $request->post('pickup_date')));
            }
            $data = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->where('user_id',$shipper_id)->where(function($query) use($start_date,$end_date){
                if(isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date)){
                    // $query->where('pickup_date','>=',$start_date);   
                    // $query->where('pickup_date','<=',$end_date);   
                    $query->whereBetween('pickup_date',[$start_date,$end_date]);
                }
            })->where(function($query){
                $query->where(['status' => 'cancelled']);
                $query->orWhere(['status' => 'completed','payment_status' => 'completed']);
            })->orderBy('id','desc')->get();
            // dd($start_date);
            
            
             
                
            return DataTables::of($data)
                // ->filter(function ($query) use ($request) {
                //     if ($request->get('status') != '') {
                //         $query->where('status', $request->get('status'));
                //     }
                    
                // })
                ->addIndexColumn()
                ->editColumn('action', function ($row) {
                    $pickup_address = "'" . $row['pickup_address'] . "'";
                    $pickup_lat = "'" . $row['pickup_lat'] . "'";
                    $pickup_lng = "'" . $row['pickup_lng'] . "'";
                    $drop_location = "'" . $row['booking_location'][0]['drop_location'] . "'";
                    $drop_lat = 0;
                    $drop_lng = 0;
                    if(isset($row['booking_location'][0]['drop_lat']) && !empty($row['booking_location'][0]['drop_lat'])){
                        $drop_lat = "'" . $row['booking_location'][0]['drop_lat'] . "'";
                    }
                    if(isset($row['booking_location'][0]['drop_lng']) && !empty($row['booking_location'][0]['drop_lng'])){
                        $drop_lng = "'" . $row['booking_location'][0]['drop_lng'] . "'";
                    }

                    if($row['status'] != 'pending' || $row['status'] != 'created'){
                        // $btn = '
                        // &nbsp;&nbsp;<a href="javascript:void(0)"  onclick="delete_shipment(this);return false;" data-newurl="manage-shipment/delete" data-id="'.$row['id'].'" data-token="'. csrf_token() .'" style="background-color: white;color: black;" class="text-danger btn btn-outline-danger btn-xs text-default justify-content-end "> <i class="fa fa-trash"></i></a>
                        // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                        
                        $btn = '<a href="'.url('admin/shipper/shipment-detail',$row['id'] ).'" style="color:#9B51E0;text-decoration:underline;font-weight: bold;"><b>'.'View Details'.'</b></a>';
                    }else{
                        // $btn = '
                        // &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="open_map('.$pickup_address.','.$pickup_lat.','.$pickup_lng.','.$drop_location.','.$drop_lat.','.$drop_lng.',this);return false;" style="background-color: white;color: black;" class="text-danger btn btn-outline-dark btn-xs text-default justify-content-end "> <img src="'.url('shipper/images/flag.svg').'" alt="" class="img-fluid"></a>';
                    }
                    return $btn;

                })
                ->addColumn('start_date',function($row){
                    $start_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $start_date = date('d/m/Y',strtotime($row['pickup_date'])) .'<br> TO <br>'. date('d/m/Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $start_date;
                })
                ->addColumn('end_date',function($row){
                    $end_date = '-';
                    if(isset($row) && !empty($row) && !empty($row['booking_location'][0]['delivered_at'])){
                        $end_date = date('F d, Y',strtotime($row['booking_location'][0]['delivered_at']));
                    }
                    return $end_date;
                })
                ->addColumn('shipment_type', function ($row){
                    if($row['is_public'] == '1'){
                        $shipment_type = '<b>Direct</b><br> Public';
                    }else{
                        $shipment_type = '<b>Direct</b><br> Private';
                    }
                    if($row['booking_type'] == 'multiple_shipment'){
                        if($row['is_public'] == '1'){
                            $shipment_type = '<b>Multiple</b><br> Public';
                        }else{
                            $shipment_type = '<b>Multiple</b><br> Private';
                        }
                    }
                    return $shipment_type;

                })
                ->addColumn('drop_location',function($row){
                    $drop_location = '-';
                    if(isset($row) && !empty($row)){
                        $company_name = isset($row['booking_location'][0]['company_name']) ? $row['booking_location'][0]['company_name'] : "-";
                        $drop_location = isset($row['booking_location'][0]['drop_location'])?$row['booking_location'][0]['drop_location']:"-";
                        $drop_location = '<b>'.$company_name.'</b><br>'.$drop_location;
                    }
                    return $drop_location;
                })
                ->editColumn('amount',function($row){
                    return '€ '.$row['amount'];
                })
                ->editColumn('status',function($row){
                    $status = '';
                    if($row['status'] == 'create'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        $status .= '<br><small class="text-danger">'.__("Waiting for truck").'</small>';
                    }elseif($row['status'] == 'scheduled'){
                        $status = '<label class="scheduled-status" style="padding: 5px 10px;">'.__("Scheduled").'</label>';
                    }elseif($row['status'] == 'pending'){
                        $status = '<label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">'.__("Pending").'</label>';
                        if($row['is_bid'] == '1'){
                            $status .= '<br><small class="text-danger">'.__("Waiting for bid").'</small>';
                        }else{
                            $status .= '<br><small class="text-danger">'.__("Waiting for book").'</small>';
                        }
                    }elseif($row['status'] == 'in-process'){
                        $status = '<label class="in-process-status" style="padding: 5px 10px;">'.__("In-process").'</label>';
                    }elseif($row['status'] == 'completed'){
                        $status = '<label class="completed-status" style="padding: 5px 10px;">'.__("Completed").'</label>';
                    }else{
                        $status = '<label class="cancelled-status" style="padding: 5px 10px;">'.__("Cancelled").'</label>';
                    }
                    return $status;
                })

                ->rawColumns(['shipment_type','action','status','drop_location','pickup_address','start_date' ,'action','status','drop_location' ,'pickup_date'])
                ->make("*");

        }  else {
            $columns = [
                            ['data' => 'shipment_type', 'name' => "shipment_type",'title' => __("Type"),'width'=>'10%','searchable' => false, 'orderable' => false], 
                            ['data' => 'id', 'name' => "id",'title' => __("Order Id"),'width'=>'60px'], 
                            ['data' => 'amount', 'name' => __("amount"),'title' => __("Price"),'width'=>'60px'],
                            ['data' => 'status', 'name' => __("status"),'title' => __("Status"),'width'=>'80px'],
                            ['data' => 'start_date', 'name' => __("pickup_date"),'title' => __("Date"),'width'=>'15%','searchable' => false, 'orderable' => false],
                            // ['data' => 'end_date', 'name' => __("end_date"),'title' => __("End Date"),'width'=>'100px', 'searchable' => false, 'orderable' => false],
                            ['data' => 'pickup_address', 'name' => __("pickup_address"),'title' => __("Pickup Location"),'width'=>'17.5%', 'searchable' => false, 'orderable' => false],
                            ['data' => 'drop_location', 'name' => __("drop_location"),'title' => __("Delivery Location"),'width'=>'17.5%','searchable' => false, 'orderable' => false],
                            ['data' => 'action', 'name' =>__("Function"),'title'=>__("Function"), 'searchable' => false, 'orderable' => false ]
                       ];
                       
                       $shipper_id = $shipper_id;
            $params['dateTableFields'] = $columns;
            
            $params['dateTableUrl'] = url('/admin/shipper/generate-invoice',$shipper_id);
            $params['dateTableTitle'] = "View Shipments";
            $params['addUrl'] = "manage-shipment";
            $params['data'] = $data_user;
            $params['dataTableId'] = time();
            $params['headingTitle'] = "Company Profile";
            $params['PaymentheadingTitle'] = "Payment Section";
            $params['FacilitiesheadingTitle'] = "Company Facilities";
            $params['ProductheadingTitle'] = "Company Product Master";
            $params['multiple_data'] = $multiple_data;
            $params['search_param'] = $search_param;

            return view('admin.pages.shipper.get_invoice', $params,compact('data_user','shipper_id'));
        }
       
    }
    public function shipment_detail($shipment_id)
    {
        if(isset($shipment_id) && !empty($shipment_id)){
            $booking= Booking::whereId($shipment_id)->orderBy('id','desc')->limit(1)->get()->toArray();
            if(isset($booking) && !empty($booking)){
                $booking_locations = BookingLocations::with('products')->whereBookingId($shipment_id)->get()->toArray();
                $booking_truck = BookingTruck::with(['truck_type'])->whereBookingId($shipment_id)->get()->toArray();
                $booking = $booking[0];
                if($booking['status'] == 'create'){
                    return redirect(url('admin/shipper/shipment-search',$booking['id']));
                }
                return view('admin.pages.shipper.shipment_details',compact('booking','booking_locations','booking_truck'));
            }else{
                return redirect()->back()->with('error',__('Shipment not found!'));
            }
        }else{
            return redirect()->back()->with('error',__('Shipment not found!'));
        }
    }

    public function shipment_search($shipment_id)
    {
        $booking = Booking::whereId($shipment_id)->orderBy('booking.id','desc')->limit(1)->first();
        $booking_locations = BookingLocations::with('products')->whereBookingId($shipment_id)->get()->toArray();
        $drop_location = BookingLocations::whereBookingId($shipment_id)->orderBy('id','desc')->first();
        $booking_truck = BookingTruck::with(['truck_type'])->whereBookingId($shipment_id)->get()->toArray();
        if(empty($booking))
        {
            return redirect(url('admin/shipper'))->with('error',__('Shipment not found!'));
        }
        $location_aray = array();
        if(isset($booking_locations) && !empty($booking_locations)){
            for($i=0;$i<count($booking_locations);$i++){
                array_push($location_aray,[$booking_locations[$i]['drop_location'],$booking_locations[$i]['drop_lat'],$booking_locations[$i]['drop_lng'],$i+1]);  
            } 
        }
        return view('admin.pages.shipper.shipment_ready_publish',compact('booking','location_aray','booking_locations','booking_truck'));
    }

    public function publish_shipment(Request $request,$id){
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
        ],[
            'required' => 'This value is required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors()->first())
                        ->withInput();
        }
        $data = $request->all();

        $booking = Booking::where('id', $id)->first();
        if(isset($data['all_bidding']) && !empty($data['all_bidding']) && $data['all_bidding'] == 'on'){
            $is_bid = 1;
        }else{
            $is_bid = 0;
        }
        $booking->is_bid = $is_bid;
        $booking->amount = $data['amount'];
        $booking->status = 'pending';
        $booking->is_public = 1;
        $booking->update();

        return redirect(url('admin/shipper'))->with('success',__('Your shipment is published'));
    }
    public function send_mail(Request $request,$shipper_id){
        // echo '<pre>';print_r($request->all());die();
        $validator = Validator::make($request->all(), [
            'invoice_number' => '',
            'user_id' => '',
            'start_date' => '',
            'end_date' => '',
            'email' => '',
            'total_amount' => '',
            'status' => '',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator->errors())
                        ->withInput();
        }
        $invoice = new Invoice();
        $pickup_date = Booking::with(['booking_location' => function ($query1){$query1->orderBy('id','desc');}])->orderBy('pickup_date')->get();
        $start_date = '';
        $end_date = '';
        $pickup_date = $request->post('pickup_date');
        if(isset($pickup_date) && !empty($pickup_date)){
            $start_date = date("Y-m-d", mktime(0, 0, 0, $request->post('pickup_date'), 01));
            $end_date = date("Y-m-t", mktime(0, 0, 0, $request->post('pickup_date')));
        }
        $data_user = User::select('*')->where('id', $shipper_id)->first();
        $check_is_exists = Invoice::where('user_id',$request->user_id)->where('start_date',$request->start_date)->where('end_date',$request->end_date)->exists();
        if($check_is_exists){
            $invoice->invoice_number = Str::random(8);
            $invoice->user_id = $data_user->id;
            $invoice->start_date = $start_date;
            $invoice->end_date = $end_date;
            $invoice->email = $request->input('email');
            $invoice->total_amount = 0;
            $invoice->status =  ($data_user->status == 1) ? '1' : '0';
            $invoice->save();
            $invoice = $invoice->id;
            // $encrypted = Crypt::encrypt($invoice);
            $encrypted = base64_encode($data_user->id.".".$start_date.".".$end_date.".".$invoice);
            
            $link = url('shipper-comission', $encrypted);;
            // Mail::send(new \App\Mail\SendMail( $link));
            
            $request->email=$request->email;
            $response_data['email'] = $request->email;
            $response_data['invoice_data'] = $invoice;
            $response_data['link'] = $link;
            dispatch(new \App\Jobs\ShipperCommisionInvoiceJob($response_data));
            // dd($invoice);
        }else{

            $invoice = $invoice->id;
            // $encrypted = Crypt::encrypt($invoice);
            $encrypted = base64_encode($data_user->id.".".$start_date.".".$end_date.".".$invoice);
            
            $link = url('shipper-comission', $encrypted);;
            // Mail::send(new \App\Mail\SendMail( $link));
            
            $request->email=$request->email;
            $response_data['email'] = $request->email;
            $response_data['invoice_data'] = $invoice;
            $response_data['link'] = $link;
            dispatch(new \App\Jobs\ShipperCommisionInvoiceJob($response_data));
        }
        return redirect()->back()->with('success','Your Mail Send Successfully.',compact('pickup_date'));
    }
}
