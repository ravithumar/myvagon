<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\PackageType;
use \Validator;

class PackageTypeController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null) {
       
           
            
            if ($request->ajax()) {
                $data = PackageType::select('*');
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('status', function ($row) {
                       
                        if($row['status'] == 1)
                            $btn = '<a  class="text-success grab"  data-url="status/change" data-status="'. $row['status'] .'" data-table="package_type" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-success">Active</span></a>';
                        else
                            $btn = '<a  class="text-danger grab" data-url="status/change" data-status="'. $row['status'] .'" data-table="package_type" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="status_change(this);return false;" data-token="' . csrf_token() . '"><span class="label label-danger">Inactive</span></a>';
                        
                        return $btn;
                    })



                    ->editColumn('action', function ($row) {
                        $btn = 
                        '<a class="text-danger grab btn btn-outline-danger btn-xs"  data-newurl="package/type/delete" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="' . csrf_token() . '" ><i class="fa fa-trash"></i></a>
                         &nbsp;&nbsp;
                        <a href="' . route('admin.package.type.index', $row['id']) . '" class="text-info grab btn btn-outline-info btn-xs"><i class="fa fa-pencil"></i></a>
                        ';
                        return $btn;
                    })
                    ->rawColumns(['status', 'action'])
                    ->make(true);
            } else {
                
                if(isset($id) && $id != null){
                    $params['edit_param'] = PackageType::find($id);
                }
            
                $columns = [
                    ['data' => 'id', 'name' => "Id", 'title' => __("Id")],
                    ['data' => 'name', 'name' => __("Name"), 'title' => __("Name")],
                    // ['data' => 'charge', 'name' => __("charge"), 'title' => __("Charge")],
                    ['data' => 'status', 'name' => __("Status"), 'title' => __("Status"), 'searchable' => true, 'orderable' => true],
                    ['data' => 'action', 'name' => __("Action"), 'title' => __("Action"), 'searchable' => false, 'orderable' => false],
                  
                    
                
                ];
                $params['dateTableFields'] = $columns;
                $params['dateTableUrl'] = route('admin.package.type.index');
                $params['dateTableTitle'] = "Package";
                $params['addUrl'] = "package";
                $params['dataTableId'] = time();


                return view('admin.pages.package.index', $params);
            }
       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255|regex:/^[a-zA-Z]+$/u',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        PackageType::create($request->all());
        return redirect()->route('admin.package.type.index')->with('success', __('Package created successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(PackageType $package) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, PackageType $package, $id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PackageType $package, $id) {
     
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255|regex:/^[a-zA-Z]+$/u',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $input = $request->all();
        $package = PackageType::whereId($id)->first();
        $package->fill($input)->save();
        return redirect()->route('admin.package.type.index')->with('success', __('Package update successfully.'));
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        PackageType::whereId($id)->delete();
        return \Response::json('success', 200);

    }

    
   
}
