<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use App\Models\PackageType;
use App\Models\TruckUnit;
use \Validator;

class TruckUnitController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null) {
       
           
            
            if ($request->ajax()) {
                $data = TruckUnit::select('*');
                return Datatables::of($data)
                    ->addIndexColumn()

                    ->editColumn('action', function ($row) {
                        $btn = 
                        '<a class="text-danger grab btn btn-outline-danger btn-xs"  data-newurl="truck/unit/delete" data-id="' . $row["id"] . '" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="' . csrf_token() . '" ><i class="fa fa-trash"></i></a>
                         &nbsp;&nbsp;
                        <a href="' . route('admin.truck.unit.index', $row['id']) . '" class="text-info grab btn btn-outline-info btn-xs"><i class="fa fa-pencil"></i></a>
                        ';
                        return $btn;
                    })
                    ->rawColumns(['status', 'action'])
                    ->make(true);
            } else {
                
                if(isset($id) && $id != null){
                    $params['edit_param'] = TruckUnit::find($id);
                }
            
                $columns = [
                    ['data' => 'id', 'name' => "Id", 'title' => __("Id")],
                    ['data' => 'name', 'name' => __("Name"), 'title' => __("Name")],
                    // ['data' => 'charge', 'name' => __("charge"), 'title' => __("Charge")],
                    // ['data' => 'status', 'name' => __("Status"), 'title' => __("Status"), 'searchable' => true, 'orderable' => true],
                    ['data' => 'action', 'name' => __("Action"), 'title' => __("Action"), 'searchable' => false, 'orderable' => false],
                  
                    
                
                ];
                $params['dateTableFields'] = $columns;
                $params['dateTableUrl'] = route('admin.truck.unit.index');
                $params['dateTableTitle'] = "Truck Unit";
                $params['addUrl'] = "truck";
                $params['dataTableId'] = time();


                return view('admin.pages.truck_unit.index', $params);
            }
       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $truck_unit['name'] = $request->post('name'); 
        $truck_unit['unit'] = ''; 
        TruckUnit::create($truck_unit);
        return redirect()->route('admin.truck.unit.index')->with('success', __('Truck Unit created successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(PackageType $package) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, PackageType $package, $id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TruckUnit $truck_unit, $id) {
     
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $input = $request->all();
        $truck_unit = TruckUnit::whereId($id)->first();
        $truck_unit->fill($input)->save();
        return redirect()->route('admin.truck.unit.index')->with('success', __('Truck Unit update successfully.'));
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        TruckUnit::whereId($id)->delete();
        return \Response::json('success', 200);
    }

    
   
}
