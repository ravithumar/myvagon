<?php
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
use NotificationHelper;

class UserController extends Controller
{
    public function index(Request $request) {

        if ($request->ajax())
        {
            $data = User::select('*');
            return Datatables::of($data)
            ->addIndexColumn()
            ->editColumn('action', function ($row)
            {
                $btn = '<a href="javascript:void(0)"><i class="fa fa-search "></i></a>';
                return $btn;
            })
            ->make(true);
        }
        else
        {
            $columns = [
                        ['data' => 'id','name' => "Id",'title' => 'Id'],
                        ['data' => 'name', 'name' => __("Name"),'title' => __("Name")],
                        ['data' => 'action', 'name' => "Action", 'title' => __("Action"),'searchable'=>false,'orderable'=>false]];
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('admin.users.index');
            $params['dateTableTitle'] = "Users";
            $params['dataTableId'] = time();
            return view('admin.pages.users.index',$params);
        }
    }
    
}

