<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\Transaction;
use App\Models\SystemConfig;
use App\Models\ApplicationConfig;
use CommonHelper;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Country;


class DashboardController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $role = Role::select('*')->where('name','shipper')->first();
        $user_role = UserRole::select('user_id')->where('role_id', $role->id)->pluck('user_id');
        $data = User::select('*')->whereIn('id', $user_role)->orderBy('id','desc')->count();

        $dispatcher_role = Role::select('*')->where('name','dispatcher')->first();
        $dispatcher = UserRole::select('user_id')->where('role_id', $dispatcher_role->id)->pluck('user_id');
        $dispatcher_data = User::select('*')->whereIn('id', $dispatcher)->orderBy('id','desc')->count();
        // dd($dispatcher_data);
        $user = User::whereIn('type', ['freelancer'])->count();
        $booking = Booking::whereIn('is_bid',['1'])->count();
        
        $book_now = Booking::whereIn('is_bid',['0'])->count();
        $booking_inprogress = Booking::where('status', 'in-progress')->count();
        $booking_completed = Booking::where('status', 'completed')->count();
        $booking_cancel = Booking::where('status', 'cancelled')->count();
        $booking_scheduled = Booking::where('status', 'scheduled')->count();
        $booking_pending = Booking::where('status', 'pending')->count();

        $freelancer_role = Role::select('*')->where('name','carrier')->first();
        $freelancer_user_role = UserRole::select('user_id')->where('role_id', $freelancer_role->id)->pluck('user_id');
        $freelancer_data = User::select('*')->whereIn('id', $freelancer_user_role)->orderBy('id','desc')->count();

        $total = Booking::where('status', 'in-progress')->ORwhere('status','completed')->ORwhere('status','cancelled')->ORwhere('status','scheduled')->ORwhere('status','pending')->count();
// dd($total);

        $month = ['January','February','March','April','May','June'];

        $user = [];
        foreach ($month as $key => $value) {
            $user[] = Transaction::where(\DB::raw("DATE_FORMAT(date, '%M')"),$value)->count();
        }
        // print_r($user);
        // $total_amount = Transaction::where('status','completed')->sum('amount');
        $total_amount = [];
        foreach ($month as $key => $value) {
            $total_amount[] = Transaction::where(\DB::raw("DATE_FORMAT(date, '%M')"),$value)->where('status','completed')->sum('amount');
        }
        
        $amount = Transaction::sum('amount');
        // echo '<pre>';print_r($amount);die();

        return view('admin.pages.dashboard', compact('book_now', 'booking_inprogress', 'booking_completed', 'booking_cancel','role','user_role','data','user','booking','booking_scheduled','booking_pending','dispatcher_role','dispatcher','dispatcher_data','total','freelancer_role','freelancer_user_role','freelancer_data','amount'))->with('month',json_encode($month,JSON_NUMERIC_CHECK))->with('user',json_encode($user,JSON_NUMERIC_CHECK))->with('total_amount',json_encode($total_amount,JSON_NUMERIC_CHECK));
    }

    #STATUS_ACTIVE_INACTIVE
    public function status($table, $id, $newstatus){
        
        if($newstatus == 1){
            $data = \DB::table($table)->where('id',$id) 
            ->update([
                'status' => 0
             ]);
        }else{
            $data = \DB::table($table)->where('id',$id) 
            ->update([
                'status' => 1
             ]);
        }
       
      
        return \Response::json($newstatus, 200);
    }
    public function testData()
    {
      return view('test');
    }
    

    public function terms_condition_index(){
        $data_update =SystemConfig::where(['path' => 'termscondition'])->value('value');
        return view('admin.pages.cms.terms_condition',compact('data_update'));
    }
    
    public function terms_condition(Request $request,SystemConfig $data){
       
    $validator = \Validator::make($request->all(), [
        'termscondition' => 'required',
    ]);

    $update['value'] = request()->input('termscondition');
    SystemConfig::where('path','termscondition')->update($update);
    return redirect()->route('admin.terms_condition')->with('success', __('App Configuration details updated successfully.'));
    }

    public function about_us_index() {
        $data_update =SystemConfig::where(['path' => 'aboutus'])->value('value');
        // $data_token = Request::except(['_token']);
        return view('admin.pages.cms.about_us',compact('data_update'));
    }

    public function about_us(Request $request,SystemConfig $data){
       
    $validator = \Validator::make($request->all(), [
        'aboutus' => 'required',
    ]);

    $update['value'] = request()->input('aboutus');
    SystemConfig::where('path','aboutus')->update($update);
    return redirect()->route('admin.about_us')->with('success', __('App Configuration details updated successfully.'));
    }



    public function privacy_policy_index() {
        $data_update =SystemConfig::where(['path' => 'privacy_policy'])->value('value');
        // $data_token = Request::except(['_token']);
        return view('admin.pages.cms.privacy_policy',compact('data_update'));
    }

    public function privacy_policy(Request $request,SystemConfig $data){
       
    $validator = \Validator::make($request->all(), [
        'privacy_policy' => 'required',
    ]);

    $update['value'] = request()->input('privacy_policy');
    SystemConfig::where('path','privacy_policy')->update($update);
    return redirect()->route('admin.privacy_policy')->with('success', __('App Configuration details updated successfully.'));
    }

    public function get_privacy(){
        $data_update =SystemConfig::where(['path' => 'privacy_policy'])->value('value');
        return view('admin.pages.privacy_policy',compact('data_update'));
    }

    public function get_terms_condition(){
        $data_update =SystemConfig::where(['path' => 'termscondition'])->value('value');
        return view('admin.pages.terms_condition',compact('data_update'));
    }

    public function get_about(){
        $data_update =SystemConfig::where(['path' => 'aboutus'])->value('value');
        return view('admin.pages.about_us',compact('data_update'));
    }

    public function my_profile()
    {
        $user = Auth::user();
        $countries =  Country::whereIn('name', ['Greece'])->get();
        $user_data = User::where('id',$user->id)->first();
        
        // print_r($user_data['id']);die();
        return view('admin.pages.my_profile',compact('countries','user_data'));
    }
    

}
