<?php

namespace App\Http\Controllers\Carrier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DriverVehicle;
use App\Models\DispatcherVehicle;
use CommonHelper;
use Auth;
use App\Models\Country;
use App\Helpers;
class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['user']=User::where([['company_ref_id',Auth::user()->id],['type','company']])->where('status',0)->get();

        return view('carrier.pages.driver.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries =  Country::whereIn('name', ['Greece'])->get();
        return view('carrier.pages.driver.create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated=$request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|unique:users',
            'phone'=>'required',
        ]);
        if($request->has('profile_image')){
           /* $img = Helpers\CommonHelper::uploadImagesS3Bucket($request->file('profile_image'),'shipper/');*/
            $img= CommonHelper::imageUpload($request['profile_image'],'images/carrier/');
           
        }      
        $user=new User;
        $user->name=$request->first_name.' '.$request->last_name;
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->country=$request->country;
        $user->profile=$img;
        $user->country_code=+30;
        $user->company_ref_id=Auth::user()->id;
        $user->type='company';
        $user->save();
        
        if($request->hasFile('id_proof'))
            {
                $id_proof=CommonHelper::imageUpload($request['id_proof'],'images/carrier/');
                
            }
        if($request->hasFile('license'))
        {
                $license= CommonHelper::imageUpload($request['license'],'images/carrier/');
        }
                   
            $driver_vehicle=new DriverVehicle;
            $driver_vehicle->user_id=$user->id;
            $driver_vehicle->truck_type=0;
            $driver_vehicle->truck_sub_category=0;
            $driver_vehicle->weight=0;
            $driver_vehicle->weight_unit=0;
            $driver_vehicle->load_capacity=0;
            $driver_vehicle->load_capacity_unit=0;
            $driver_vehicle->brand=0;
            $driver_vehicle->pallets=0;
            $driver_vehicle->hydraulic_door=0;
            $driver_vehicle->cooling=0;
            $driver_vehicle->fuel_type=0;
            $driver_vehicle->truck_features=0;
            $driver_vehicle->registration_no=0;
            $driver_vehicle->trailer_registration_no=0;
            $driver_vehicle->images=0;
            $driver_vehicle->id_proof= $id_proof;
            $driver_vehicle->license= $license;
            $driver_vehicle->save();

        return redirect()->route('carrier.driver')->with('success','Driver addedd successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['user']=User::find($id);
        return view('carrier.pages.driver.show',$this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['user']=User::find($id);
        $this->data['driver_vehicle']=DriverVehicle::where('user_id',$id)->first();
        return view('carrier.pages.driver.edit',$this->data);
    }
    public function assing($id)
    {
        $this->data['user']=User::find($id);
        $this->data['driver_vehicle']=DriverVehicle::where('user_id',$id)->first();

        $this->data['driver_assing']=DriverVehicle::where('user_id',$id)->orderBy('id','DESC')->first();

         $this->data['countries'] =  Country::whereIn('name', ['Greece'])->get();
        return view('carrier.pages.driver.assing',$this->data);
    }
    public function vehicle_list($id)
    {
       $this->data['vehicle']=DispatcherVehicle::with(['truck','brand'])->where('is_assing',0)->where('status',0)->orderBy('id','desc')->paginate(5);
       $this->data['user']=User::find($id);
        return view('carrier.pages.driver.vehiclelist',$this->data);
    }
    public function assing_vehicle(Request $request)
    {

        $id=$request->vehicle_id;
        $userid=$request->user_id;
        $userData=User::where('id',$userid)->first();
        $userData->is_assing=1;
        $userData->save();
        $dispatcher=DispatcherVehicle::find($id);
        $driver_data=DriverVehicle::where('user_id',$userid)->first();

        $dispatcher->is_assing=1;
        $dispatcher->save();

        $dispatcher_vehicle=new DriverVehicle;
        $dispatcher_vehicle->dispatcher_vehicle_id=$dispatcher->id;
        $dispatcher_vehicle->user_id=$userid;
        $dispatcher_vehicle->truck_type=$dispatcher->truck_type_id;
        $dispatcher_vehicle->truck_sub_category=$dispatcher->truck_sub_category_id;
        $dispatcher_vehicle->weight=$dispatcher->overall_truck_weight;
        $dispatcher_vehicle->weight_unit=$dispatcher->overall_truck_weight_unit;
        $dispatcher_vehicle->load_capacity=$dispatcher->cargo_load_capacity;
        $dispatcher_vehicle->load_capacity_unit=$dispatcher->cargo_load_capacity_unit;
        $dispatcher_vehicle->brand=$dispatcher->brand_id;
        $dispatcher_vehicle->capacity_pallets=0;
        $dispatcher_vehicle->pallets=0;
        $dispatcher_vehicle->hydraulic_door=$dispatcher->hydraulic_door;
        $dispatcher_vehicle->fuel_type=$dispatcher->vehicle_run_on;
        $dispatcher_vehicle->registration_no=$dispatcher->truck_plate;
        $dispatcher_vehicle->trailer_registration_no=$dispatcher->trailer_plate;
        $dispatcher_vehicle->id_proof=$driver_data->id_proof;
        $dispatcher_vehicle->license=$driver_data->license;
        $dispatcher_vehicle->images=$dispatcher->vehicle_photo;
        $dispatcher_vehicle->is_assing=1;
        $dispatcher_vehicle->save();
        return \Response::json('success', 200); 
    }
    public function editVehicel($id)
    {

        $this->data['vehicle']=DispatcherVehicle::with(['truck','brand'])->where('is_assing',0)->where('status',0)->orderBy('id','desc')->paginate(5);

        $driver_vehicle['driver_vehicle']=DriverVehicle::find($id);
        dd($driver_vehicle['driver_vehicle']);
        return view('carrier.pages.driver.vehicleedit',$this->data,$driver_vehicle);
    }
    public function updateVehicel(Request $request)
    {
        $id=$request->driver_vehicle_id;
        $vehicle_id=$request->vehicle_id;

        $driver_vehicle=DriverVehicle::find($id);
        $dd=$driver_vehicle->user_id;
        $dispatcher_vehicle=DispatcherVehicle::where('id',$driver_vehicle->dispatcher_vehicle_id)->first();
        $dispatcher_vehicle->is_assing=0;
        $dispatcher_vehicle->save();
        $d_vehicle=DispatcherVehicle::find($vehicle_id);
        $d_vehicle->is_assing=1;
        $d_vehicle->save();
        $driver_vehicle->dispatcher_vehicle_id=$d_vehicle->id;
        $driver_vehicle->truck_type=$d_vehicle->truck_type_id;
        $driver_vehicle->truck_sub_category=$d_vehicle->truck_sub_category_id;
        $driver_vehicle->weight=$d_vehicle->overall_truck_weight;
        $driver_vehicle->weight_unit=$d_vehicle->overall_truck_weight_unit;
        $driver_vehicle->load_capacity=$d_vehicle->cargo_load_capacity;
        $driver_vehicle->load_capacity_unit=$d_vehicle->cargo_load_capacity_unit;
        $driver_vehicle->brand=$d_vehicle->brand_id;
        $driver_vehicle->registration_no=$d_vehicle->truck_plate;
        $driver_vehicle->hydraulic_door = $d_vehicle->hydraulic_door;
        $driver_vehicle->trailer_registration_no=$d_vehicle->trailer_plate;
        $driver_vehicle->fuel_type=$d_vehicle->vehicle_run_on;
        $driver_vehicle->images=$d_vehicle->vehicle_photo;
        $driver_vehicle->is_assing=1;
        $driver_vehicle->save();
        /*return redirect('carrier/driver/assing/'.$driver_vehicle->user_id)->with('success','Vehicle updated successfully');*/

        return response()->json($dd);

    }
    public function assignDelete(Request $request)
    {
        $is_data=DriverVehicle::where('dispatcher_vehicle_id',$request->id)->where('dispatcher_vehicle_id',$request->driver_id)->orderBy('id','DESC')->first();
        $is_data->is_assing=0;
        $is_data->save();
        $is_assing=DispatcherVehicle::find($request->id);
        $is_assing->is_assing=0;
        $is_assing->save();
        return \Response::json('success', 200);
    
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $validate = $request -> validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email' => 'required|unique:users,email,'.$request->id,
            'phone'=>'required'
            ]);
        if($request->hasFile('profile_image'))
        {
                $img= CommonHelper::imageUpload($request['profile_image'],'images/carrier/');
        }
        $user=User::find($request->id);
        $user->name=$request->first_name.' '.$request->last_name;
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->profile=isset($img) ? $img :$request->profile_image;
        $user->company_ref_id=Auth::user()->id;
        $user->country=$request->country;
        $user->country_code=+30;
        $user->type='company';
        $user->save();
        if($request->hasFile('id_proof'))
            {
                $id_proof=CommonHelper::imageUpload($request['id_proof'],'images/carrier/');
                
            }
        if($request->hasFile('license'))
        {
                $license= CommonHelper::imageUpload($request['license'],'images/carrier/');
        }
                    
            $driver_vehicle=DriverVehicle::where('user_id',$request->user_id)->first();
            $driver_vehicle->user_id=$user->id;
            $driver_vehicle->truck_type=0;
            $driver_vehicle->truck_sub_category=0;
            $driver_vehicle->weight=0;
            $driver_vehicle->weight_unit=0;
            $driver_vehicle->load_capacity=0;
            $driver_vehicle->load_capacity_unit=0;
            $driver_vehicle->brand=0;
            $driver_vehicle->pallets=0;
            $driver_vehicle->hydraulic_door=0;
            $driver_vehicle->cooling=0;
            $driver_vehicle->fuel_type=0;
            $driver_vehicle->truck_features=0;
            $driver_vehicle->registration_no=0;
            $driver_vehicle->trailer_registration_no=0;
            $driver_vehicle->images=isset($img) ? $img :$request->profile_image;
            //isset($request->required_date) ? $request->required_date : NULL,
            $driver_vehicle->id_proof=isset($id_proof) ? $id_proof :$request->id_proof;
            $driver_vehicle->license= isset($license)  ? $license  :$request->license;
            $driver_vehicle->save();
        return redirect()->route('carrier.driver')->with('success','Driver updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetchDriver(Request $request)
    {
        $driver_id=$request->driver_id;
        $driver_vehicle=DriverVehicle::where('user_id',$driver_id)->where('dispatcher_vehicle_id','!=',NULL)->orderBy('id','DESC')->first();
        return response()->json($driver_vehicle);
    }
     public function destroy(Request $request)
    {
        $driver_id=$request->driver_id;
        $driver_vehicle=DriverVehicle::where('user_id',$driver_id)->orderBy('id','DESC')->first();
        $dispatcher_vehicle=DispatcherVehicle::where('id',$driver_vehicle->dispatcher_vehicle_id)->first();
        if($dispatcher_vehicle){
            $dispatcher_vehicle->is_assing=0;
            $dispatcher_vehicle->save();    
            $driver_vehicle->delete();
        }
        $user=User::find($driver_id);
        $user->status=1;
        $user->save();
        return \Response::json('success', 200);    
    }
    public function trashDriver()
    {
        $this->data['user']=User::where([['company_ref_id',Auth::user()->id],['type','company']])->where('status',1)->get();
        return view('carrier.pages.driver.trashlist',$this->data);
    }
    public function retriveDriver(Request $request)
    {
        $driver_id=$request->driver_id;
        $user=User::find($driver_id);
        $user->status=0;
        $user->save();
        return \Response::json('success', 200);
    }
}
