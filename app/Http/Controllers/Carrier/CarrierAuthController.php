<?php

namespace App\Http\Controllers\Carrier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\Booking;
use App\Models\BookingLocations;
use App\Models\Country;
use App\Helpers;

class CarrierAuthController extends Controller
{
    public function create()
    {
    	return view('carrier.auth.register');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'password' => 'required|min:6',
            'confirm_password' => 'required|min:6'
           
        ],[
            'required' => 'This value is required.'
        ]);
        
        if ($validator->fails()) {
         
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        if($request->input('email_verify_register') == "0"){
            return redirect()->back()->withInput()->with('error', 'Please verify your email address before submit !');
        }

        if($request->input('phone_verify_register') == "0"){
            return redirect()->back()->withInput()->with('error', 'Please verify your phone number before submit !');
        }

        $user=new User;
        $user->name=$request->name;
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->password=Hash::make($request->password);
        $user->save();

        /////  Role          ///

        $role=Role::where('name','dispatcher')->first();

        $user_role=new UserRole;
        $user_role->user_id=$user->id;
        $user_role->role_id=$role['id'];
        $user_role->save(); 
    	
        return redirect('shipper/login')->with('success','Dispatcher registered successfully');
    }
     public function storeLogin(Request $request){
        $validator = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required',
        ],[
            'email.required' => 'Email or Phone is required !',
        ]);

        if (!$validator->fails()) {
           // $credentials = $request->only('email', 'password');

             $credentials = $request->only('email', 'password');

            if(is_numeric($request->get('email'))){
                $credentials =  [ 
                    'phone'=> $request->get('email'),
                    'password'=>$request->get('password')
                ];
               
            }else if (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
                $credentials = [
                    'email' => $request->get('email'), 
                    'password'=> $request->get('password'),
                ];
               
            }
            
            if (Auth::attempt($credentials)) {
                $user = Auth::user();
                if ($user->hasRole($user->id,4)) {

                    if($user->stage == "accepted"){

                        if($user->status == 1){
                             Session::put('runnint', 'running');
                             if($user->type == 'manager'){
                                $user_permission = ShipperPermission::where('user_id',$user->id)->first();
                                if(!empty($user_permission) && isset($user_permission)){
                                    Session::put('manager_permission', $user_permission);
                                }
                             }

                            return redirect()->route('carrier.dashboard');
                        }
                        Auth::logout();
                        return redirect()->back()->with('warning', 'Your profile is Inactive , please try after some time!');
                        
                    }
                    Auth::logout();
                    return redirect()->back()->with('warning', 'Your profile is under verification , please try after some time!');
                    
                }
                Auth::logout();
                return redirect()->back()->with('error', 'Can not authorized for carrier login !');
            
            }
            Auth::logout();
            return redirect()
                        ->route('shipper.login')
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => 'Entered email or password is invalid !',
                        ]);
        }
    }
    public function carrierDashboard(Request $request)
    {
       $user =   Auth::user();
        
        if($user->type == 'manager'){
            $manage_price = PermissionHelper::check_access('manage_price');
        }else{
            $manage_price = '1';
        }
        $booking_count_all = Booking::where('user_id',$user->id)->count();
        $booking_count_pending = Booking::where(['user_id'=>$user->id,'status' => 'pending'])->count();
        $booking_count_scheduled = Booking::where(['user_id'=>$user->id,'status' => 'scheduled'])->count();
        $booking_count_in_process = Booking::where(['user_id'=>$user->id,'status' => 'in-progress'])->count();
        $booking_count_completed = Booking::where(['user_id'=>$user->id,'status' => 'completed'])->count();
        // $bookings = Booking::with(['booking_location','booking_truck.driver_details'])->where('user_id',$user->id)->orderBy('id','desc')->get()->toArray();
        $bookings = Booking::with(['booking_location','booking_truck.driver_details','booking_truck.truck_details'])->orderBy('id','desc')->paginate(10);
        
        if ($request->ajax()) {
            $view = view('carrier.pages.dashboard_list',compact('bookings'))->render();
            return response()->json(['html'=>$view]);
        }

        $booking_locations = array();
        if(isset($bookings[0]['id']) && !empty($bookings[0]['id'])){ 
            $booking_locations = BookingLocations::whereBookingId($bookings[0]['id'])->get();
        }
        $location_aray = array();
        if(isset($booking_locations) && !empty($booking_locations)){
            for($i=0;$i<count($booking_locations);$i++){
                array_push($location_aray,[$booking_locations[$i]['drop_location'],$booking_locations[$i]['drop_lat'],$booking_locations[$i]['drop_lng'],$i+1]);  
            } 
        }
        return view('carrier.pages.dashboard',compact('bookings','location_aray','manage_price','booking_count_all','booking_count_pending','booking_count_scheduled','booking_count_in_process','booking_count_completed'));
    }
    public function my_profile()
    {
        $user = Auth::user();
        $countries =  Country::whereIn('name', ['Greece'])->get();
        $user_data = User::where('id',$user->id)->first();
        return view('carrier.pages.my_profile',compact('countries','user_data'));
    }
    public function update_profile(Request $request){
        $validator = Validator::make(request()->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'company_name' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'post_code' => 'required',
            'ssn_number' => 'required',
            'phone' => 'required',
            'profile' => 'file',
        ],[
            'first_name.required' => 'First name is required !',
            'last_name.required' => 'Last name is required !',
            'company_name.required' => 'Company name is required !',
            'country.required' => 'Country is required !',
            'state.required' => 'State is required !',
            'city.required' => 'City is required !',
            'post_code.required' => 'Postcode is required !',
            'ssn_number.required' => 'SSN number is required !',
            'phone.required' => 'Phone number is required !',
            'profile.required' => 'profile number is required !',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $input = $request->all();
        $user = Auth::user();
        $user_data = User::where('id',$user->id)->first();
        if($request->has('profile')){
            $img = Helpers\CommonHelper::uploadImagesS3Bucket($request->file('profile'),'shipper/');
            $user_data->profile = $img;
        }
        $user_data->first_name = $input['first_name'];
        $user_data->last_name = $input['last_name'];
        $user_data->name = $input['first_name'].' '.$input['last_name'];
        $user_data->company_name = $input['company_name'];
        $user_data->country = $input['country'];
        $user_data->state = $input['state'];
        $user_data->city = $input['city'];
        $user_data->postcode = $input['post_code'];
        $user_data->ssn = $input['ssn_number'];
        $user_data->company_phone = $input['phone'];
        $user_data->update();
        return redirect(url('carrier/my-profile'))->with('success', __('Profile update successfully.'));
    }
    public function logout(Request $request) {
        Auth::logout();
        Session::forget('manager_permission');
        Session::forget('truck_data');
        Session::forget('product_data');
        Session::forget('direct_shipment_data');
        Session::forget('search_available_truck_detail');
        Session::forget('edit_truck_data');
        Session::forget('edit_product_data');
        
        return redirect()->route('shipper.login');
    }
    public function setting()
    {
        return view('carrier.pages.setting');
    }
    public function change_password()
    {
        return view('carrier.pages.change_password');
    }
    public function update_password(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required',
        ],[
            'old_password.required' => 'Old password is required',
            'new_password.required' => 'New password is required',
            'confirm_password.required' => 'Confirm password is required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        $input = $request->all();
        $user = Auth::user();
        $user_data = User::where('id',$user->id)->first();
        $check_old_password = Hash::check($input['old_password'], $user_data->password);
        if($check_old_password){
            $user_data->password = Hash::make($input['new_password']);
            $user_data->update();
            return redirect(url('carrier/change-password'))->with('success', __('Your password changed successfully.'));
        }else{
            return redirect(url('carrier/change-password'))->with('error', __('Your old password has been wrong, please try again.'));
        }
    }
}
