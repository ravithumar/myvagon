<?php

namespace App\Http\Controllers\Carrier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TruckType;
use App\Models\TruckCategory;
use App\Models\TruckBrand;
use App\Models\PackageType;
use App\Models\TruckUnit;
use App\Models\DispatcherVehicle;
use App\Models\DispatcherVehicleCapacity;
use App\Models\DriverVehicle;
use CommonHelper;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['vehicle']=DispatcherVehicle::with(['truck','brand'])->where('status',0)->orderBy('id','DESC')->paginate(10);
        
        return view('carrier.pages.vehicle.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['truckDetails']=TruckType::all();
        $this->data['truck_type_category']=TruckCategory::all();
        $this->data['truck_brand']=TruckBrand::all();
        $this->data['package_type']=PackageType::all();
        $this->data['truck_unit']=TruckUnit::all();
        return view('carrier.pages.vehicle.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*dd($request->all());*/
          if($request->hasFile('vehicle_photo'))
            {
                $vehicle_photo=CommonHelper::imageUpload($request['vehicle_photo'],'images/carrier/');
                
            }
        $vehicle=new DispatcherVehicle();
        $vehicle->truck_type_id=$request->truck_type;
        $vehicle->truck_sub_category_id=implode(",", $request->truck_sub_category_id);
        $vehicle->overall_truck_weight=$request->overall_truck_weight;
        $vehicle->overall_truck_weight_unit=$request->overall_truck_weight_unit;
        $vehicle->cargo_load_capacity=$request->cargo_load_capacity;
        $vehicle->cargo_load_capacity_unit=$request->cargo_load_capacity_unit;
        $vehicle->brand_id=$request->truck_brand;
        $vehicle->truck_plate=$request->truck_plate;
        $vehicle->trailer_plate=$request->trailer_plate;
        $vehicle->hydraulic_door=$request->hydraulic_door;
        $vehicle->vehicle_run_on=$request->vehicle_run_on;
        $vehicle->vehicle_photo= $vehicle_photo;
        $vehicle->save();
        foreach($request->value as $key=>$value)
        {
            $capacity=new DispatcherVehicleCapacity;
            $capacity->driver_vehicle_id=$vehicle->id;
            $capacity->package_type_id=$request->package_type_id[$key];
            $capacity->value=$value;
            $capacity->save();
        }
        return redirect()->route('carrier.vehicle')->with('success','Vehicle detail added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['vehicle']=DispatcherVehicle::with(['truck','brand'])->find($id);
        
        return view('carrier.pages.vehicle.show',$this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['truckDetails']=TruckType::all();
        $this->data['truck_type_category']=TruckCategory::all();
        $this->data['truck_brand']=TruckBrand::all();
        $this->data['package_type']=PackageType::all();
        $this->data['truck_unit']=TruckUnit::all();
        $this->data['dispatcher_vehicle']=DispatcherVehicle::find($id);
        $this->data['dispatcher_capacity']=DispatcherVehicleCapacity::where('driver_vehicle_id',$id)->get();
        return view('carrier.pages.vehicle.edit',$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->hasFile('vehicle_photo'))
            {
                $vehicle_photo=CommonHelper::imageUpload($request['vehicle_photo'],'images/carrier/');
                
            }
        $dispatcher_vehicle=DispatcherVehicle::find($request->id);
        $dispatcher_vehicle->truck_type_id=$request->truck_type;
        $dispatcher_vehicle->truck_sub_category_id=implode(",",$request->truck_sub_category_id);
        $dispatcher_vehicle->overall_truck_weight=$request->overall_truck_weight;
        $dispatcher_vehicle->overall_truck_weight_unit=$request->overall_truck_weight_unit;
        $dispatcher_vehicle->cargo_load_capacity=$request->cargo_load_capacity;
        $dispatcher_vehicle->cargo_load_capacity_unit=$request->cargo_load_capacity_unit;
        $dispatcher_vehicle->brand_id=$request->truck_brand;
        $dispatcher_vehicle->truck_plate=$request->truck_plate;
        $dispatcher_vehicle->trailer_plate=$request->trailer_plate;
        $dispatcher_vehicle->vehicle_run_on=$request->vehicle_run_on;
        $dispatcher_vehicle->vehicle_photo= isset($vehicle_photo) ? $vehicle_photo :$request->vehicle_photo;
        $dispatcher_vehicle->save();
        $data=[];
        foreach($request->value as $key=>$value)
        {
        /*    $data[]=[
                'driver_vehicle_id'=>$dispatcher_vehicle->id,
                'package_type_id'=>$request->package_type_id[$key],
                'value'=>$value
            ];*/
            
            if(isset($request->dvc_id[$key])){
                $capacity= DispatcherVehicleCapacity::findOrFail($request->dvc_id[$key]);
                $capacity->driver_vehicle_id=$dispatcher_vehicle->id;
                $capacity->package_type_id=$request->package_type_id[$key];
                $capacity->value=$value;
                $capacity->save();
            }else{
                $capacity_data=new DispatcherVehicleCapacity;
                $capacity_data->driver_vehicle_id=$dispatcher_vehicle->id;
                $capacity_data->package_type_id=$request->package_type_id[$key];
                $capacity_data->value=$value;
                $capacity_data->save();
            }
        }
        return redirect()->route('carrier.vehicle')->with('success','Vehicle detail updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function getVehicle(Request $request)
    {
        $vehicle_id=$request->vehicle_id;
        $driver_vehicle=DriverVehicle::where('dispatcher_vehicle_id',$vehicle_id)->first();
        return response()->json($driver_vehicle);
    }
    public function destroy(Request $request)
    {
            $vehicle_id=$request->vehicle_id;
            $dispatcher_vehicle=DispatcherVehicle::find($vehicle_id);
            $dispatcher_vehicle->status=1;
            $dispatcher_vehicle->save();
            $driver_vehicle=DriverVehicle::where('dispatcher_vehicle_id',$vehicle_id)->orderBy('id','DESC')->first();
            if($driver_vehicle){
                $driver_vehicle->is_assing=0;
                $driver_vehicle->save();     
            }
                  
            return \Response::json('success', 200);        
    }
    public function getCategory(Request $request)
    {
        $truck_type=$request->truckType;
        $data=TruckCategory::where('truck_type_id',$truck_type)->get();
        return response()->json(['truck_category'=>$data]);
        
    }
    public function trashVehicle(Request $request)
    {
        $this->data['vehicle']=DispatcherVehicle::with(['truck','brand'])->where('status',1)->orderBy('id','DESC')->paginate(5);
        return view('carrier.pages.vehicle.trashvehicle',$this->data);

    }
    public function retriveVehicle(Request $request)
    {
        $vehicle_id=$request->vehicle_id;
        $dispatcher_vehicle=DispatcherVehicle::find($vehicle_id);
        $dispatcher_vehicle->status=0;
        $dispatcher_vehicle->is_assing=0;
        $dispatcher_vehicle->save();
        return \Response::json('success', 200); 

    }
}
