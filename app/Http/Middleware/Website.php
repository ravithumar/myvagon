<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Website
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        $user = $request->user();
        if(!($user)){
            \App::setLocale('en');
        if(\Session::get('lan') == "gr")
        {
            \App::setLocale('gr');
        }
            return redirect()->route('shipper.login');
        }
        if ($user->hasRole($user->id,1)) {
            return redirect()->route('login');
        }

        \App::setLocale('en');
        if(\Session::get('lan') == "gr")
        {
            \App::setLocale('gr');
        }

        return $next($request);
    }
}
