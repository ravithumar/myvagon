<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();

        if(!isset($user)){
            return redirect()->route('login');
        }
        
        if(isset($user)){
            if(!$user->hasRole($user->id,1))
            {
                return redirect()->route('login');
            }
        }
        // if(!$user->hasRole($user->id,1))
        // {
        //     return redirect()->route('login');
        // }
        return $next($request);
    }
}
