<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Carrier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
         $user = $request->user();
        
        if(!($user)){
            return redirect()->route('shipper.login');
        }
        if ($user->hasRole($user->id,1)) {
            return redirect()->route('login');
        }
    
        return $next($request);
    }
 }

