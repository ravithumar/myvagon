<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
       
        return [
            "id" => isset($this['id']) ? $this['id'] : '',
            "name" => isset($this['name']) ? $this['name'] : '',
            "first_name" => isset($this['first_name']) ? $this['first_name'] : '',
            "last_name" => isset($this['last_name']) ? $this['last_name'] : '',
            "email" => isset($this['email']) ? $this['email'] : '',
            "profile" => isset($this['profile']) ? $this['profile'] : '',
            "phone" => isset($this['phone']) ? $this['phone'] : '',
            "country_code" => isset($this['country_code']) ? $this['country_code'] : '',
            "self_des" => isset($this['self_des']) ? $this['self_des'] : '',
            "type" => isset($this['type']) ? $this['type'] : '',
            "company_ref_id" => isset($this['company_ref_id']) ? $this['company_ref_id'] : '',
            "company_name" => isset($this['company_name']) ? $this['company_name'] : '',
            "postcode" => isset($this['postcode']) ? $this['postcode'] : '',
            "city" => isset($this['city']) ? $this['city'] : '',
            "state" => isset($this['state']) ? $this['state'] : '',
            "country" => isset($this['country']) ? $this['country'] : '',
            "ssn" => isset($this['ssn']) ? $this['ssn'] : '',
            "company_phone" => isset($this['company_phone']) ? $this['company_phone'] : '',
            "email_verify" => isset($this['email_verify']) ? $this['email_verify'] : '',
            "phone_verify" => isset($this['phone_verify']) ? $this['phone_verify'] : '',
            "email_verified_at" => isset($this['email_verified_at']) ? $this['email_verified_at'] : '',
            "password" => isset($this['password']) ? $this['password'] : '',
            "remember_token" => isset($this['remember_token']) ? $this['remember_token'] : '',
            "status" => isset($this['status']) ? $this['status'] : '',
            "block_status" => isset($this['block_status']) ? $this['block_status'] : '',
            "stage" => isset($this['stage']) ? $this['stage'] : '',
            "licence_number" => isset($this['licence_number']) ? $this['licence_number'] : '',
            "licence_expiry_date" => isset($this['licence_expiry_date']) ? $this['licence_expiry_date'] : '',
            "busy" => isset($this['busy']) ? $this['busy'] : '',
            "created_at" => isset($this['created_at']) ? $this['created_at'] : '',
            "updated_at" => isset($this['updated_at']) ? $this['updated_at'] : '',
            "message" => isset($this['message']) ? $this['message'] : '',
            "message_time" => isset($this['message_time']) ? $this['message_time'] : '',
        ];
    }
}
