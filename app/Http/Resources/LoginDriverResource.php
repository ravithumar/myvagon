<?php

namespace App\Http\Resources;

use App\Models\Device;
use App\Models\DriverVehicle;
use App\Models\DriverPermission;
use Illuminate\Http\Resources\Json\JsonResource;

class LoginDriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        $vehicle = DriverVehicle::where('user_id', $this->id)->with('TruckType:id,name', 'TruckSubCategory:id,name', 'brands:id,name', 'VehicleCapacity')->first();
        $vehicle->truck_features = $vehicle->feature; 

        $permissions = DriverPermission::whereUserId($this->id)->first();
        
        return [
            'id'=> $this->id,
            'name' => $this->name,
            // 'phone' => $this->phone,
            'email' => $this->email,
            'profile' => $this->profile,
            'type' => $this->type,
            'country_code' => $this->country_code,
            'mobile_number' => $this->phone,
            'licence_number' => $this->licence_number,
            'licence_expiry_date' => $this->licence_expiry_date,
            'vehicle' => $vehicle,
            'permissions' => $permissions,

        ];
    }
}
