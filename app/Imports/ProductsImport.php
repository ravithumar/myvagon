<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Auth;
class ProductsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $user = Auth::user();
        $id=Product::orderBy('id','DESC')->first();
        $product_name=Product::pluck('name','id')->toArray();
        $id_sum=$id->id+1; 
        $user_id=$user->id;
        if(!(in_array($row['name'], $product_name))){
            $data=[
            'user_id'     => $user_id,
            'insert_by'    => $user_id, 
            'name' => $row['name'],
            'sku_name' => $id_sum.$row['sku_no'],
            'sku_no' => $row['sku_no'],
            'sku_type' => $row['sku_type'],
            'status' => $row['status'],
            'charge' => $row['charge']];
        return new Product($data);    
        }
        
    }
}
