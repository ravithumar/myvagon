<?php 

namespace App\Helpers;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version2X;

class SocketHelper
{
    public static function emit($channel,$data)
    {
        $socket = new Client(new Version2X('http://3.66.160.72:3000?x-api-key=password'));
        try {
            $socket->initialize();
            $socket->emit($channel,$data);
            $socket->close();
            return true;     
         } catch (Exception $e) {
            return false;
         } 
        
    }
}