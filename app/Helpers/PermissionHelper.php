<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use Image;
use File;
use Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PermissionHelper
{
    public static function check_access($module_name)
    {
        $user_permissions = Session::get('manager_permission');
        if(!empty($user_permissions)){
            if($user_permissions[$module_name] == 1){
                return true;   
            }else{
                return false;
            }
        }else{
            return true;
        }
    } 
}