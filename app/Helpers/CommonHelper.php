<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use Image;
use File;
use Storage;
use App\Models\Notification;
use App\Models\UserSettings;
use Carbon\Carbon;
use NumberFormatter;

class CommonHelper
{
    public static function resize($dir,$name,$w=200,$h=200)
    {
        $img = Image::make(public_path($dir.$name));
        $img->resize($w,$h, function ($constraint) {
            $constraint->aspectRatio();
        });
        // $img->insert('public/watermark.png');
        $new_path = public_path($dir.'_'.$w.'x'.$h.'jpg');
        $img->save($new_path);
        return $new_path;
    }

    public static function ConvertDate($date="",$formate="")
    {
            // echo Carbon::createFromFormat('d-m-Y',date('Y-m-d'));exit;       
    }

    public static function ConfigSet($key_array)
    {
        foreach ($key_array as $key => $value) {
            $update['value']=$value;
            \App\Models\SystemConfig::wherePath($key)->update($update);
        }
        return true;
    }

    public static function ConfigGet($key)
    {
        $config = \App\Models\SystemConfig::wherePath($key)->first();
        return $config->value;
    }

    public static function imageUpload($image, $dir)
    {
        
        $imageName =  rand(0000,9999).''.time().'.'.$image->extension();  
        $upload = $image->move(public_path($dir), $imageName);
        //Self::resize($dir,$imageName);
        return $upload?$imageName:"";
    }

    public static function multipleImageUpload($image, $dir) {
        $imageName = time(). '.'.$image->getClientOriginalExtension();  
        $upload = $image->move(public_path($dir), $imageName);
        return $upload?$imageName:"";
    }

    public static function moveImagesS3Bucket($image,$folder){
        #S3-bucket-upload
        $file = $folder.$image;
        $filePath = url('temp/' . $image);
        Storage::disk('s3')->put($file, file_get_contents($filePath));
        #Remove-Temp
        $image_path = $filePath;
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
    
    }

    public static function uploadImagesS3Bucket($image,$folder){
        #S3-bucket-upload
        $imageName = time(). '.'.$image->getClientOriginalExtension(); 
        $file_path = $folder.$imageName;
        $upload_done = Storage::disk('s3')->put($file_path, file_get_contents($image));
        return $upload_done?$imageName:"";
    }

    public static function removeImage($filename){
        $s3 = Storage::disk('s3');
        $s3->delete($filename);
    } 

    
     public static function getNotification($user_id)
    {
        return Notification::where('user_id',$user_id)->orderBy('id','desc')->paginate(10);
        // return Notification::where('user_id',$user_id)->where('read_status','0')->count();
    }

    public static function check_file_exist($path,$image)
    {
        // echo '<pre>';print_r($file);
        $path = $path.$image;
        $files = File::exists($path) ? $path : url('images/default.png');
        return $files;
    }


    public static function send_push($device_type = '', $user_token = '',$push_title = '',$push_message = '',$message_type = '',$message_data = "",$user_id = "")
    {
        $device_type = strtolower(trim($device_type));
        if($device_type != '' && $user_token != '')
        {
            $key = env('FCM_KEY');
            $token = $user_token; 
            $push_data = array(
                'message' => $push_message,
                'title' => $push_title,
                'sound' => "default",
                'type' => $message_type,
                'device_type' => 'android',
                'data' => $message_data
            );
            if($device_type == 'ios')
            {
                $fcmFields = array(
                    'priority' => 'high',
                    'to' => $token,
                    'notification' => array( 
                        "title"=> $push_title,
                        "message"=> $push_message,
                        "type"=> $message_type,
                        'sound' => 'default',
                        'device_type' => 'ios',
                        'data' => $message_data
                        )
                    );
            }
            else
            {
                $fcmFields = array(
                    'to' => $token,
                    'priority' => 'high',
                    'data' => $push_data, 
                    'notification' => $push_data,
                ); 
            }

            $headers = array('Authorization: key=' . $key,'Content-Type: application/json');
            // echo '<pre>';print_r($user_id);die();
            // if(isset($user_id) && !empty($user_id)){
                $notification_data['user_id'] = $user_id;
                $notification_data['title'] = $push_title;
                $notification_data['message'] = $push_message;
                $notification_data['read_status'] = '0';
                $notification_data['created_at'] = date('Y-m-d h:s:i');
                Notification::create($notification_data);
            // }

            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
            $result = curl_exec($ch );
            curl_close( $ch );
            // echo $result . "\n\n";
            // exit;

        }
    }

    public static function price_format($price,$country='GR'){
        $amount = $price;
        if($country == 'GR'){
            // $amount = number_format(str_replace(',','.',str_replace('.','',$price)),2, ',', '.');
            $fmt = numfmt_create( 'eu_GR', NumberFormatter::CURRENCY );
            $fmt->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
            $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);
            $amount  = $fmt->format(str_replace(',','',$price));
        }elseif($country == 'IN'){
            // $amount = str_replace(',','',number_format(str_replace(',','.',str_replace('.','',$price)),2, '.', ','));
            $fmt = numfmt_create( 'en_US', NumberFormatter::CURRENCY );
            $fmt->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
            $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);
            $amount  = $fmt->format(str_replace(',','.',str_replace('.','',$price)));
        }
        return $amount;
    }
    
    public static function users_setting($user_id){
        if(isset($user_id) && !empty($user_id)){
            $users_setting = UserSettings::where(['user_id' => $user_id])->first();
            return $users_setting;
        }
    }
        


}