<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShipperCommisionInvoiceEmail extends Mailable
{
    use Queueable, SerializesModels;

public $link;
public $invoice_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($link,$invoice_data)
    {
        //
        $this->link=$link;
        $this->invoice_data=$invoice_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.shipper_commision_invoice_mail');
    }
}
