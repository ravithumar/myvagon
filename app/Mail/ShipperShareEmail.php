<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShipperShareEmail extends Mailable
{
    use Queueable, SerializesModels;

public $email_or_number;
public $booking_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_or_number,$booking_id)
    {
        //
        $this->email_or_number=$email_or_number;
        $this->booking_id=$booking_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('_shipper.emails.shipper_share_email');
    }
}
