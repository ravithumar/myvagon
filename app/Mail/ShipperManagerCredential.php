<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ShipperManagerCredential extends Mailable
{
    use Queueable, SerializesModels;
   
    public $username;
    public $password;
    public $company_name;
   

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username, $password, $company_name)
    {
       $this->username = $username;
       $this->password = $password;
       $this->company_name = $company_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('_shipper.emails.shipper_manager_credential');
    }
}
