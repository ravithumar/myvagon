var base_url = "http://3.66.160.72/admin";

function delete_notiflix(e) {
    
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var url = $(e).data('url');
    var u1 = $(e).data('newurl');
    console.log(u1, id);
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to delete this record?',
        'Yes',
        'No',
        function() {
            $(".se-pre-icon").show();
            $.ajax({
                url: base_url + '/' + u1 + '/' + id,
                
                type: 'post',
                dataType: "JSON",
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function(returnData) {
                    $(".se-pre-icon").hide();
                    Notiflix.Notify.Success('Deleted');
                    tr.remove();
                }
            });
        });

}

function status_change(e){
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var new_url = $(e).data('url');
    var status = $(e).data('status');
    var table = $(e).data('table');
    console.log(status);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to change the status?',
        'Yes',
        'No',
        function() {
            $(".se-pre-icon").show();
            $.ajax({
                url: base_url + '/' + new_url + '/' + table +  '/' + id + '/' + status,
                type: 'post',
                dataType: "JSON",

                data: {
                    "id": id,
                    "_method": 'POST',
                    "_token": token,
                },

                success: function(data) {
                   
                    $(".se-pre-icon").hide();
                   
                    Notiflix.Notify.Success('Status changed');

                    if(data == 1){
                        var replace_str = '<a  class=" text-danger "  data-table="'+table+'" data-url="status/change" data-id="' + id + '"  data-status="0" data-popup="tooltip" onclick="status_change(this);return false;" ><span class="label label-danger">Inactive</span></a>';
                    }else{
                        var replace_str = '<a  class=" text-success "  data-table="'+table+'" data-url="status/change" data-id="' + id + '"  data-status="1" data-popup="tooltip" onclick="status_change(this);return false;" ><span class="label label-success">Active</span></a>';
                    }
                    $(e).replaceWith(replace_str);
                     
                }
            });
        });

}




function approve_account(e){
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var new_url = $(e).data('url');
    var status = $(e).data('status');
    var table = $(e).data('table');
    
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    if(status == "1"){
        Notiflix.Confirm.Show(
            'Confirm',
            'Are you sure that you want to Accept this Shipper Profile?',
            'Yes',
            'No',
            function() {
                Notiflix.Loading.Standard();
                $.ajax({
                    url: base_url + '/shipper/approve/' + id + '/' + status,
                    type: 'post',
                    dataType: "JSON",
    
                    data: {
                        "id": id,
                        "_method": 'POST',
                        "_token": token,
                    },
    
                    success: function(responce) {
                       
                        Notiflix.Loading.Remove();
                        Notiflix.Notify.Success('Shipper Profile Approve Successful');
                       
                        location.reload();
                         
                    }
                });
            });
    
    }else{
        Notiflix.Confirm.Show(
            'Confirm',
            'Are you sure that you want to Reject this Shipper Profile?',
            'Yes',
            'No',
            function() {
                Notiflix.Loading.Standard();
                $.ajax({
                    url: base_url + '/shipper/approve/' + id + '/' + status,
                    type: 'post',
                    dataType: "JSON",
    
                    data: {
                        "id": id,
                        "_method": 'POST',
                        "_token": token,
                    },
    
                    success: function(responce) {
                       
                        Notiflix.Loading.Remove();
                       
                        Notiflix.Notify.Success('Shipper Profile Reject Successful');
                        
                        location.reload();
                         
                    }
                });
            });
    
    }
}

function accept(e){
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var new_url = $(e).data('url');
    var status = $(e).data('status');
    var table = $(e).data('table');
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to accept this driver?',
        'Yes',
        'No',
        function() {
            Notiflix.Loading.Standard();
            $.ajax({
                url: base_url + '/driver/accept/' + id ,
                type: 'post',
                dataType: "JSON",

                data: {
                    "id": id,
                    "_method": 'POST',
                    "_token": token,
                },

                success: function(data) {
                   
                    Notiflix.Loading.Remove();
                   
                    Notiflix.Notify.Success('Driver Accepted Successful');
                    location.reload();
                    
                }
            });
        });

}

function reject(e){
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var new_url = $(e).data('url');
    var status = $(e).data('status');
    var table = $(e).data('table');
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to reject this driver?',
        'Yes',
        'No',
        function() {
            Notiflix.Loading.Standard();
            $.ajax({
                url: base_url + '/driver/reject/' + id ,
                type: 'post',
                dataType: "JSON",

                data: {
                    "id": id,
                    "_method": 'POST',
                    "_token": token,
                },

                success: function(data) {
                   
                    Notiflix.Loading.Remove();
                   
                    Notiflix.Notify.Success('Driver Rejected Successful');

                    location.reload();
                     
                }
            });
        });

}

function block_status(e){
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var new_url = $(e).data('url');
    var status = $(e).data('status');
    var table = $(e).data('table');
    
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    if(status == "0"){
        Notiflix.Confirm.Show(
            'Confirm',
            'Are you sure that you want to reject this driver profile?',
            'Yes',
            'No',
            function() {
                $(".se-pre-icon").show();
                $.ajax({
                    url: base_url + '/driver/block/' + id + '/' + status,
                    type: 'post',
                    dataType: "JSON",
    
                    data: {
                        "id": id,
                        "_method": 'POST',
                        "_token": token,
                    },
    
                    success: function(responce) {
                       
                        $(".se-pre-icon").hide();
                        if(status == 0)
                            Notiflix.Notify.Success('Driver Accepted Successful');
                        else
                            Notiflix.Notify.Success('Driver Rejected Successful');
                        
    
                        location.reload();
                         
                    }
                });
            });
    
    }else{
        Notiflix.Confirm.Show(
            'Confirm',
            'Are you sure that you want to accept this driver profile?',
            'Yes',
            'No',
            function() {
                $(".se-pre-icon").show();
                $.ajax({
                    url: base_url + '/driver/block/' + id + '/' + status,
                    type: 'post',
                    dataType: "JSON",
    
                    data: {
                        "id": id,
                        "_method": 'POST',
                        "_token": token,
                    },
    
                    success: function(responce) {
                       
                        $(".se-pre-icon").hide();
                        if(status == 0)
                            Notiflix.Notify.Success('Driver Accepted Successful');
                        else
                            Notiflix.Notify.Success('Driver Rejected Successful');
                        
    
                        location.reload();
                         
                    }
                });
            });
    
    }
   
}

function reset(){
    document.getElementById("name").value="";
}

function previewFile(input){
    var file = $("input[type=file]").get(0).files[0];

    if(file){
        var reader = new FileReader();

        reader.onload = function(){
            $("#previewImg").css("display","block");
            $("#previewImg").attr("src", reader.result);
        }

        reader.readAsDataURL(file);
    }
}

