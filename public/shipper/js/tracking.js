   var position;
   function setMarker(map, marker) {
       socket.on('live_tracking', function(data) {
           console.log("-----", data);
           position = [data.lat, data.lng];
           var latlng = new google.maps.LatLng(data.lat, data.lng);
           animatedMove(marker, .5, marker.position, latlng)
       })

   }

   function animatedMove(marker, t, current, moveto, info) {
       var lat = current.lat();
       var lng = current.lng();
       var deltalat = (moveto.lat() - current.lat()) / 100;
       var deltalng = (moveto.lng() - current.lng()) / 100;
       var delay = 500 * t;
       for (var i = 0; i < 50; i++) {
           (function(ind) {
               setTimeout(
                   function() {
                       var lat = marker.position.lat();
                       var lng = marker.position.lng();
                       lat += deltalat;
                       lng += deltalng;
                       latlng = new google.maps.LatLng(lat, lng);
                       marker.setPosition(latlng);
                       position[0] = moveto.lat();
                       position[1] = moveto.lng();
                   }, delay * ind
               );

           })(i)
       }
   }