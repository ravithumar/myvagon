$(document).ready (function () {  
    
    $("#basic-form").validate ({
        rules: 
        {           	
           
           
            first_name: {
                required:true,
                minlength : 2,
                maxlength : 25,
               // format : /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/,
            },
          
            last_name:{
                required:true,
                minlength : 2,
                maxlength : 25,
            } ,
           
        },        
        messages: 
        {
           
           
            first_name: {
                required : "Please enter first name",
                
                minlength : "Please enter minimum 2 characters in first name !",
                maxlength : "Please enter maximum 25 characters in first name !",

                format : "Sorry! special characters are not allowed in first name",
            } ,
            last_name:  {
                required : "Please enter last name",
                
                minlength : "Please enter minimum 2 characters in last name !",
                maxlength : "Please enter maximum 25 characters in last name !",

                format : "Sorry! special characters are not allowed in last name",
            }, 
          
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "first_name") {
                error.appendTo("#errorToShow");
            } else {
                error.insertAfter(element);
            }
        }

    });  


    $("#step1").validate ({
        rules: 
        {           	
           
            first_name: {
                required:true,
                minlength : 2,
                maxlength : 25,
            },
          
            last_name:{
                required:true,
                minlength : 2,
                maxlength : 25,
            } ,
           
        },        
        messages: 
        {
           
           
            first_name: {
                required : "Please enter first name",
                
                minlength : "Please enter minimum 2 characters in first name !",
                maxlength : "Please enter maximum 25 characters in first name !",

                format : "Sorry! special characters are not allowed in first name",
            } ,
            last_name:  {
                required : "Please enter last name",
                
                minlength : "Please enter minimum 2 characters in last name !",
                maxlength : "Please enter maximum 25 characters in last name !",

                format : "Sorry! special characters are not allowed in last name",
            }, 
          
        },
        

    }); 


});