

var base_url = "http://3.66.160.72/shipper";
// var base_url = "http://127.0.0.1:8000/shipper";

function delete_notiflix(e) {
    
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var url = $(e).data('url');
    var u1 = $(e).data('newurl');

    var product = $('#productid').val();

    console.log(token);
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to delete this record?',
        'Yes',
        'No',
        function() {
            Notiflix.Loading.Standard();
            $.ajax({
                url: base_url + '/' + u1 + '/' + product,
                
                type: 'post',
                dataType: "JSON",
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function(returnData) {
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Deleted');
                    tr.remove();
                    $("#edit_model").modal("hide");
                    location.reload();

                }
            });
        });

}

function delete_address(e) {
    
  var tr = $(e).parent().closest('tr');
  var id = $(e).data('id');
  var token = $(e).data('token');
  var url = $(e).data('url');
  var u1 = $(e).data('newurl');

  var product = $('#productid').val();
  Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to delete this record?',
      'Yes',
      'No',
      function() {
          Notiflix.Loading.Standard();
          $.ajax({
              url: base_url + '/' + u1 + '/' + id,
              
              type: 'post',
              dataType: "JSON",
              data: {
                  "id": id,
                  "_method": 'DELETE',
                  "_token": token,
              },
              success: function(returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Deleted');
                  // tr.remove();
                  window.location.href = base_url+"/shipper/address";
                  // $("#edit_model").modal("hide");
                  // location.reload();

              }
          });
      });

}

function delete_shipment(e) {
    
  var tr = $(e).parent().closest('tr');
  var id = $(e).data('id');
  var token = $(e).data('token');
  
  var u1 = $(e).data('newurl');

  console.log(token);
  Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to delete this record?',
      'Yes',
      'No',
      function() {
          Notiflix.Loading.Standard();
          $.ajax({
              url: base_url + '/' + u1 + '/' + id,
              
              type: 'post',
              dataType: "JSON",
              data: {
                  "id": id,
                  "_method": 'DELETE',
                  "_token": token,
              },
              success: function(returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Deleted');
                  tr.remove();
                  location.reload();

              }
          });
      });

}

function approve_account_manager(e){
  var tr = $(e).parent().closest('tr');
  var id = $(e).data('id');
  var token = $(e).data('token');
  var new_url = $(e).data('url');
  var status = $(e).data('status');
  var table = $(e).data('table');
  
 
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  if(status == "1"){
      Notiflix.Confirm.Show(
          'Confirm',
          'Are you sure that you want to Unblock this Manager Profile?',
          'Yes',
          'No',
          function() {
              Notiflix.Loading.Standard();
              $.ajax({
                  url: base_url + '/manager-management/approve/' + id + '/' + status,
                  type: 'post',
                  dataType: "JSON",
  
                  data: {
                      "id": id,
                      "_method": 'POST',
                      "_token": token,
                  },
  
                  success: function(responce) {
                     
                      Notiflix.Loading.Remove();
                      Notiflix.Notify.Success('Manager Profile Unblock Successfully');
                     
                      location.reload();
                       
                  }
              });
          });
  
  }else{
      Notiflix.Confirm.Show(
          'Confirm',
          'Are you sure that you want to Block this Manager Profile?',
          'Yes',
          'No',
          function() {
              Notiflix.Loading.Standard();
              $.ajax({
                  url: base_url + '/manager-management/approve/' + id + '/' + status,
                  type: 'post',
                  dataType: "JSON",
  
                  data: {
                      "id": id,
                      "_method": 'POST',
                      "_token": token,
                  },
  
                  success: function(responce) {
                     
                      Notiflix.Loading.Remove();
                     
                      Notiflix.Notify.Success('Manager Profile Block Successfully');
                      
                      location.reload();
                       
                  }
              });
          });
  
  }
}

function get_state(country_id,state_id=0)
{
  $.ajax({
    url: base_url+'/address/get-state',
    type: 'post',
    async: false,
    dataType: "JSON",
    data: {
        "country_id": country_id,
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(returnData) {
        if(returnData.status == true){
          var options = '<option value="">Choose State..</option>';
          var selected = '';
          $.each(returnData.data,function(key, value){
            if(state_id == value.id){
              selected = 'selected';
            }else{
              selected = '';
            }
            options += '<option value="'+value.id+'" '+selected+'>'+value.name+'</option>';
          })
          $('.state').html(options);
        }
    }
});
}

function get_city(state_id,city_id=0){
  $.ajax({
    url: base_url+'/address/get-city',
    type: 'post',
    async: false,
    dataType: "JSON",
    data: {
        "state_id": state_id,
    },
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(returnData) {
        if(returnData.status == true){
          var options = '<option value="">Choose City..</option>';
          var selected = '';
          $.each(returnData.data,function(key, value){
            if(city_id == value.id){
              selected = 'selected';
            }else{
              selected = '';
            }
            options += '<option value="'+value.id+'" '+selected+'>'+value.name+'</option>';
          })
          $('.city').html(options);
        }
    }
  });
}


$(document).ready(function(){

  $(document).on('change','.country',function(){
      var country_id = $(this).val();
      get_state(country_id);
  });
  $(document).on('change','.state',function(){
    var state_id = $(this).val();
    get_city(state_id);
  });


    $(".personal-details li a").click(function(){
        $(this).parent().prev().addClass("width-zero");
        $(this).parent().prev().prev().addClass("width-zero");
        $(this).parent().next().removeClass("add-width");
        $(this).parent().removeClass("width-zero");
        // $(this).parent().addClass("add-width");
	});
	$(".personal-details li:nth-child(2) a").click(function(){
          $(this).parent().addClass("add-width");
        // $(this).parent().addClass("add-width");
	});
	$(".register-form-next").click(function(){
			
          $('.personal-details ul li a.active').parent().addClass("width-zero");
          $('.personal-details li:nth-child(2) a').parent().addClass("add-width");
          $(this).parent().prev().prev().addClass("width-zero");
        // $(this).parent().addClass("add-width");
	});
	// $(".personal-details li a.active").click(function(){
        
	// });

});
$(".dashboardcode-bsmultiselect input.form-control").attr("placeholder", "variable");

   
 


/*$(document).ready(function(){
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 300) {
	    $(".main-menu").css("background" , "white");
	    $(".main-menu").css("position" , "fixe");
	  }

	  else{
		  $(".main-menu").css("background" , "transparent");  	

	  }
  })
})
*/
// $(document).ready(function(){


//     $(".personal-details li a").click(function(){
//         if ($(this).parent().addClass("active").length != 0)
//             $(this).parent().next().addClass("next")
           
//         else {
//             $(this).parent().addClass("prev") 
//             $(this).parent().removeClass("active")
           
//         }
//         return false;
//     });

// });
// sticky header js
$(window).scroll(function(){
  if ($(window).scrollTop() >= 150) {
    $('.main-menu').addClass('fixed');
   }
   else {
    $('.main-menu').removeClass('fixed');
   }
});

// if($(window).width() < 768)
// {
//    $(window).scroll(function() {    
//     var scroll = $(window).scrollTop();

//      //>=, not <=
//     if (scroll >= 100) {
//         //clearHeader, not clearheader - caps H
//         $(".main-menu").addClass("fixed");
//     }
//     else {
//     	 $(".main-menu").removeClass("fixed");
//     }
// 	});
// } else {
  
// }

 $('#login input:radio').click(function () {
      $('#login input:radio').parent().removeClass('active');
      $(this).parent(this).addClass('active');
  });

$('#free-sign-up input:radio').click(function () {
      $('#free-sign-up input:radio').parent().removeClass('active');
      $(this).parent(this).addClass('active');
  });
$('#language input:radio').click(function () {
      $('#language input:radio').parent().removeClass('active');
      $(this).parent(this).addClass('active');
  });

$('.destination input:radio').click(function () {
      $('.destination input:radio').parent().removeClass('active');
      $(this).parent(this).addClass('active');
  });


window.updateProduct = function(
    updateUrl,
    name,
	number,
	type,
  id
   
) {

   document.getElementById("name_view").setAttribute("value", name);
   document.getElementById("sku_number_view").setAttribute("value", number);
   document.getElementById("sku_type_view").setAttribute("value", type);
   document.getElementById('product_data_id').setAttribute("value", id);
   document.getElementById('productid').setAttribute("value", id);
   
    $("#edit_model").modal("show");
	
};


window.GetProduct = function(
  updateLink,
  name,
	number,
	type,
  id
   
) {

   document.getElementById("name_edit").setAttribute("value", name);
   document.getElementById("sku_number_edit").setAttribute("value", number);
   document.getElementById("sku_type_edit").setAttribute("value", type);
   document.getElementById('form-sub').action = updateLink;
   document.getElementById('product_data_id').setAttribute("value", id);
    $("#update_model").modal("show");
	
};


function getCity(cityid, stateid){
 
  $.ajax({
    type: "GET",
    url: '../../shipper/data/get/' + cityid + '/' + stateid,
    
    success: function success(response) {
      //document.getElementById("cityview").setAttribute("value", response.city);
      console.log(response);
    }
  });
}

window.view_address = function(
 
  id,
  name,
  address,
  postcode,
  phone,
  email,
  city,
  state,
  country,
  serv1,
  serv2,
  serv3,
  id

) {
 

  document.getElementById("name-view").setAttribute("value", name);
  document.getElementById("address-view").setAttribute("value", address);
 
  
  document.getElementById("phone-view").setAttribute("value", phone);
  document.getElementById("email-view").setAttribute("value", email);
  
  document.getElementById("cityview").setAttribute("value", city);
  document.getElementById("stateview").setAttribute("value", state);

  document.getElementById("countryview").setAttribute("value", country);
  document.getElementById("postcodeview").setAttribute("value", postcode);

  document.getElementById("productid").setAttribute("value", id);

  if(serv1 == 1){
    document.getElementById('restrooms-view').checked = true;
  }else{
    document.getElementById('restrooms-view').checked = false;
  }

  if(serv2 == 1){
    document.getElementById('food-view').checked = true;
  }else{
    document.getElementById('food-view').checked = false;
  }
  

  if(serv3 == 1){
    document.getElementById('driver-view').checked = true;
  }else{
    document.getElementById('driver-view').checked = false;
  }



  $("#view-details").modal("show");

};



window.get_address = function(
  name,
  address,
  address_lat,
  address_lng,
  postcode,
  phone,
  email,
  city,
  state,
  country,
  serv1,
  serv2,
  serv3,
  id,
  updateLink,
  city_id,
  state_id,
  country_id

) {

  get_state(country_id);
  get_city(state_id);
  document.getElementById("name-edit").setAttribute("value", name);
  // document.getElementById("address").setAttribute("value", address);
   document.getElementsByClassName("fulladdress")[1].setAttribute("value", address);
 
  document.getElementById("postcode-edit").setAttribute("value", postcode);
  document.getElementById("edit-latitude").setAttribute("value", address_lat);
  document.getElementById("edit-longitude").setAttribute("value", address_lng);
  document.getElementById("phone-edit").setAttribute("value", phone);
  document.getElementById("email-edit").setAttribute("value", email);
  document.getElementById("productid").setAttribute("value", id);
  document.getElementById("city-edit").setAttribute("value", city_id);
  
  
  $("#country-edit option[value='"+country_id+"']").attr("selected", "selected");
  
  $("#state-edit option[value='"+state_id+"']").attr("selected", "selected");
  // $("#state-edit").val(state_id).change();

  $("#city-edit option[value='"+city_id+"']").attr("selected", "selected");



  document.getElementById('form-sub').action = updateLink;

  if(serv1 == "1"){
    document.getElementById('restrooms-edit').checked = true;
  }else{
    document.getElementById('restrooms-edit').checked = false;
  }

  if(serv2 == "1"){
    document.getElementById('food-edit').checked = true;
  }else{
    document.getElementById('food-edit').checked = false;
  }
  

  if(serv3 == "1"){
    document.getElementById('rest_service-edit').checked = true;
  }else{
    document.getElementById('rest_service-edit').checked = false;
  }


  $("#edit-address").modal("show");
	
};

// $("#add-address,#edit-address,#view-address").modal({
//   show:false,
//   backdrop:'static'
// });



$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

function myFunction(){
  
  var email = document.getElementById('email').value;

  $.ajax({
    type: "GET",
    url: "/shipper/email/verify/" + email,
    
    success: function(responce){  
      
      if(responce == "success"){
        $('.verify-img').css('display','none');
        $('.verify-img2').css('display','block');
        

          
      }else{
        $('.verify-img').css('display','block');
        $('.verify-img2').css('display','none');
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) { 
        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
    }  

  });

}

// $('.feedback-slider').owlCarousel({
//     loop:true,
//     margin:50,
//     items:3,
//     nav:true,
//     autoHeight: true,
//     responsive:{
//         0:{
//             items:1,
//             nav:true
//         },
//         600:{
//             items:2,
//             nav:false
//         },
//         1000:{
//             items:3,
//             nav:true,        
//         }
       

//     }

// });

// VIDEO 2
$('.ytvideo[data-video]').one('click', function() {

  var $this = $(this);
  var width = $this.attr("width");
  var height = $this.attr("height");

  $this.html('<iframe src="https://www.youtube.com/embed/' + $this.data("video") + '?autoplay=1"></iframe>');
});

//if(Session::has('...'))

 function codeAddress() {
 
    // console.log(document.getElementById('session').val);

 }


window.onload = codeAddress;




  


