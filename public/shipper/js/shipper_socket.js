var socket = io.connect(NODE_URL, {
    query: {
        'x-api-key': "password"
    }
});

var base_url = "http://3.66.160.72/"

$(document).ready(function() {
    socket.on('connect', () => {
        console.log('Successfully connected!');
    });

     socket.on('connect_failed', function() {
         alert("Sorry, there seems to be an issue with the connection!");
      })

    socket.emit('shipper_connect', {
        user_id: user_id
    });
    socket.on('connect_failed', function() {
        alert("Sorry, there seems to be an issue with the connection!");
    });
    socket.on('accept_request', function(data) {
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;

        // html_data =
        //       '<a href="javascript:void(0);" class="dropdown-item notify-item active">' +
        //       '<div class="notify-icon bg-primary">' +
        //       '<i class="mdi mdi-account-clock"></i>' +
        //       '</div>' +
        //       '<p class="notify-details">' +
        //       data.message +
        //       '<small class="text-muted">' + data.message + '</small>' +
        //       '<small class="text-muted text-right">just now</small>' +
        //       '</p>' +
        //       '</a>';
          $('.custom-notification').prepend(html_data);

    });

    socket.on('arrived_at_location', function(data) {
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
        $('.custom-notification').prepend(html_data);

    });
    socket.on('start_loading', function(data) {
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('start_journy', function(data) {
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('cancel_booking', function(data) {
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('accept_request', function(data) {
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('post_availability_push', function(data) {
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('bid_request_push', function(data) {
        console.log(data)
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('post_book_push', function(data) {
        console.log(data)
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('bid_accept_request_push', function(data) {
        console.log(data)
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('cancel_request', function(data) {
        Notiflix.Notify.Success(data.message);
        html_data = ` <a href="javascript:void(0);" class="dropdown-item notify-item border-bottom-darken-1" >
        <div class="notify-icon"><i class="fa fa-bell text-dark"></i></div>
        <p class="notify-details">`+data.message+`</p>
        <p class="text-muted mb-0 user-msg">
            <small>`+data.message+`</small>
        </p>
    </a>`;
        var count = parseInt($('.notification-count').html());
        count++;
        $('.notification-count').html(count);
          $('.custom-notification').prepend(html_data);

    })

    socket.on('new_message', function(response) {
        console.log(response)
        Notiflix.Notify.Success('New Message arrived');
        $(".se-pre-icon").fadeIn("slow");
            $.post(base_url+`shipper/get-chat`,{receiver_id:response.sender_id},function(res,status){

                if(res.data.length>0)
                {
                    $('.conversation-list').html("");
                    $.each(res.data, function( index, value ) {
                        var class_name = "";
                        
                       if(value['sender_id'] == response.receiver_id)
                        {
                            class_name = "odd";
                        }

                        var html = `<li class="clearfix `+class_name+`">
                                <div class="chat-avatar">
                                <img src="`+base_url+`images/default.png" class="rounded-circle" style="border-radius: 17px;flex: 1 1 auto;padding: 0px;height: 47px;width: 52px;border: solid;margin-top: 7px;border-radius: 30%!important;" >
                                    <i>`+value['created_at']+`</i>
                                </div>
                                <div class="conversation-text">
                                    <div class="ctext-wrap">
                                        <i>`+value['sender_name']+`</i>
                                        <p>`+value['message']+`</p>
                                    </div>
                                </div>
                            </li>`;
                         $('.conversation-list').append(html);  
                         $(".se-pre-icon").fadeOut("slow"); 
                    });
                }
            });

    })

    socket.on('new_message')
});