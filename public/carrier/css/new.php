	
.table > tbody > tr:first-child > td {
    border: none;
}
table {
    border-collapse: collapse;
}

.table-responsive .driver_table{
	width: 1500px
}
.card{
	border-radius:50px;
}
.card .card-body.add_driver{
	border-radius: 8px;
	width: 118px
}
.driver_logo{
	width: 80px;
	
}
.driver_name{
	width: 17%;
	color: #000000;

}
.driver_name .rate{
	color: #9B51E0;
	padding-top: 10px
}
.driver_name .map{
	color: blue;
	width: 15px;
	padding-top:5px;
}
.driver_name p{
	float: right;
	color: #000000; 
}
.padding-0{
    padding-right:0;
    padding-left:0;
}
#edit_driver{
	border-radius:50px;
	background-color: #9B51E0;
	border-color: none;
	border: 2px solid #E8E8E8;
	width: 84px;
	height: 40px;
	text-align: center;font-size: 15px

}

/*p{
	color: #9B51E0;
}*/
.mile{
	color: #000000;
}
p.rate{
	margin-left: 35px;
}
.user_logo{
	width: 7%
}
.permission{
	width: 25%;
	color: #000000;
	font-weight: bold;
}
.permission .rate{
	color: #6c757d;
	font-weight: normal;
}
.truck{
	width: 35%;
}
.deadhead{
	width: 25%
}
.deadhead .truck{
	color: #9B51E0;
}

