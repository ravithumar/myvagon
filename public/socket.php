<?php 
	require '../vendor/autoload.php';

	use ElephantIO\Client;
	use ElephantIO\Engine\SocketIO\Version2X;


	$options = [
    'context' => [
        'ssl' => [
            'verify_peer' => false,
             'verify_peer_name' => false
        ]
    ]
];

	$client = new Client(new Version2X("http://3.66.160.72:3000",  $options));
	// $client = new Client(new Version2X('http://3.66.160.72:3000'));
 // print_r($client);exit;
	try {
		$client->initialize();
		// send message to connected clients
		$client->emit('broadcast', ['type' => 'notification', 'text' => 'Hello There!']);
		$client->close();
		
	} catch (Exception $e) {
		echo $e;
	}
?>