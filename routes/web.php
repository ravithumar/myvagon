<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\SystemConfigController;
use App\Http\Controllers\Admin\PackageTypeController;
use App\Http\Controllers\Admin\TruckController;
use App\Http\Controllers\Admin\TruckTypeController;
use App\Http\Controllers\Admin\ShipperController;
use App\Http\Controllers\Admin\DriverController;
use App\Http\Controllers\Admin\BookingController;
use App\Http\Controllers\Admin\TruckUnitController;
use App\Http\Controllers\Shipper\AddressController;
use App\Http\Controllers\Shipper\AuthController;
use App\Http\Controllers\Shipper\DasboardController;
use App\Http\Controllers\Shipper\ProductController;
use App\Http\Controllers\Shipper\ShipmentController;
use App\Http\Controllers\Shipper\MultipleShipmentController;
use App\Http\Controllers\Shipper\ShipmentDesignController;
use App\Http\Controllers\Shipper\ManageShipmentController;
use App\Http\Controllers\Shipper\SearchAvailableTruckController;
use App\Http\Controllers\Shipper\ManagerManagementController;
use App\Http\Controllers\Shipper\ChatController;
use App\Http\Controllers\Shipper\DahsboardNewController;
use App\Http\Controllers\Shipper\NotificationController;
use App\Http\Controllers\Shipper\InvoiceController;
use App\Http\Controllers\WebsiteController;
use App\Http\Controllers\Carrier\DriverController as CarrierDriverController;
use App\Http\Controllers\Carrier\VehicleController;
use App\Http\Controllers\Carrier\CarrierAuthController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::POST('send-mail/{id}',[ShipperController::class,'send_mail']);
Route::POST('driver_send_mail/{id}',[DriverController::class,'driver_send_mail']);
Route::get('shipper-comission/{key}',[InvoiceController::class, 'shipperCommisionInvoice']);

Route::get('/', function () {
    return view('home');
});
Route::get('testData',[DashboardController::class,'testData']);
Route::get('/carrier', function () {
    return view('carrier');
});

Route::get('/shipper-demo', function () {
    return view('shipper');
});
Route::get('/terms-and-conditions', function () {
	    return view('terms_conditions');
	});
Route::get('/privacy-policy', function () {
	    return view('privacy_policy');
	});
Route::get('/about-us', function () {
	    return view('about-us');
	});
Route::get('change/lang/{lang}',[WebsiteController::class, 'changeLang'])->name('change-lang');

Route::get('cron-availability-trash',[AuthController::class, 'cron_past_availability'])->name('cron-availability-trash');
Route::get('cron-booking-trash',[AuthController::class, 'cron_past_booking'])->name('cron-booking-trash');
Route::get('cron-scheduled-booking-trash',[AuthController::class, 'cron_past_booking_scheduled'])->name('cron-scheduled-booking-trash');
// Route::get('/', function(){
// 	return view('shipper.pages.home');
// });
Route::get('/privacy-policy',[DashboardController::class,'get_privacy']);
	Route::get('/terms-condition',[DashboardController::class,'get_terms_condition']);
	Route::get('/about-us',[DashboardController::class,'get_about']);
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
	Route::get('dashboard',[DashboardController::class,'index'])->name('dashboard');
	Route::get('terms_condition',[DashboardController::class,'terms_condition_index'])->name('terms_condition');
	Route::post('terms_condition/submit',[DashboardController::class,'terms_condition'])->name('terms_condition.submit');
	Route::get('about_us',[DashboardController::class,'about_us_index'])->name('about_us');
	Route::get('about_us/submit',[DashboardController::class,'about_us'])->name('about_us.submit');
	Route::get('my_profile',[DashboardController::class,'my_profile'])->name('my_profile');
	
	
	Route::get('privacy_policy',[DashboardController::class,'privacy_policy_index'])->name('privacy_policy');
	Route::post('privacy_policy/submit',[DashboardController::class,'privacy_policy'])->name('privacy_policy.submit');

	Route::get('dashboard/chart',[DashboardController::class,'chart']);
	Route::post('status/change/{table}/{id}/{newstatus}', [DashboardController::class,'status'])->name('status');
	

	Route::get('users',[UserController::class,'index'])->name('users.index');
	
	Route::get('system/config',[SystemConfigController::class,'index'])->name('system.config');
	Route::post('system/config/submit',[SystemConfigController::class,'update'])->name('system.config.submit');
	Route::get('app/config',[SystemConfigController::class,'app'])->name('app.config');
	Route::post('app/config/submit',[SystemConfigController::class,'submit_app'])->name('app.config.submit');
	Route::get('system/section',[SystemConfigController::class,'index_system']);
	

	#PACKAGE_TYPE
	Route::group(['prefix' => 'package/type'], function () {
		Route::get('/{id?}', [PackageTypeController::class,'index'])->name('package.type.index');
		Route::post('submit', [PackageTypeController::class,'store'])->name('package.type.submit');
		Route::get('edit/{id}', [PackageTypeController::class,'edit'])->name('package.type.edit');
		Route::post('update/{id}', [PackageTypeController::class,'update'])->name('package.type.update');
		Route::delete('delete/{id}', [PackageTypeController::class,'destroy'])->name('package.type.delete');
	});

	#PRODUCT_UNIT
	Route::group(['prefix' => 'truck/unit'], function () {
		Route::get('/{id?}', [TruckUnitController::class,'index'])->name('truck.unit.index');
		Route::post('submit', [TruckUnitController::class,'store'])->name('truck.unit.submit');
		Route::get('edit/{id}', [TruckUnitController::class,'edit'])->name('truck.unit.edit');
		Route::post('update/{id}', [TruckUnitController::class,'update'])->name('truck.unit.update');
		Route::delete('delete/{id}', [TruckUnitController::class,'destroy'])->name('truck.unit.delete');
	});

	#TRUCK_CATEGORY
	Route::group(['prefix' => 'truck/category'], function () {
		Route::get('/{id?}', [TruckController::class,'index'])->name('truck.category.index');
		Route::post('submit', [TruckController::class,'store'])->name('truck.category.submit');
		Route::get('edit/{id}', [TruckController::class,'edit'])->name('truck.category.edit');
		Route::post('update/{id}', [TruckController::class,'update'])->name('truck.category.update');
		Route::delete('delete/{id}', [TruckController::class,'destroy'])->name('truck.category.delete');
		
	});

	#TRUCK_TYPE
	Route::group(['prefix' => 'truck/type'], function () {
		Route::get('/{id?}', [TruckTypeController::class,'index'])->name('truck.type.index');
		Route::post('submit', [TruckTypeController::class,'store'])->name('truck.type.submit');
		Route::get('edit/{id}', [TruckTypeController::class,'edit'])->name('truck.type.edit');
		Route::post('update/{id}', [TruckTypeController::class,'update'])->name('truck.type.update');
		Route::delete('delete/{id}', [TruckTypeController::class,'destroy'])->name('truck.type.delete');
		
	});

	#SHIPPER
	Route::group(['prefix' => 'shipper'], function(){
		Route::get('/', [ShipperController::class,'index'])->name('shipper.index');
		Route::get('view/{id}', [ShipperController::class,'view'])->name('shipper.view');
		Route::post('update/{id}', [ShipperController::class,'update'])->name('shipper.update');
		Route::post('approve/{id}/{status}', [ShipperController::class,'approveStatus'])->name('shipper.approve.status');
		Route::get('shipment/{id}', [ShipperController::class,'shipment']);
		Route::get('shipment_demo/{id}', [ShipperController::class,'demo_shipment']);
		Route::get('shipment-comm/{id}', [ShipperController::class,'commission_fee']);
		// Route::get('generate_invoice', [ShipperController::class,'generate_invoice']);
		Route::get('generate-invoice/{id}', [ShipperController::class,'get_invoice']);
		Route::get('view_profile/{id}', [ShipperController::class,'view_profile'])->name('shipper.view_profile');
		Route::get('shipment-detail/{id}', [ShipperController::class,'shipment_detail']);
		Route::get('shipment-search/{id}', [ShipperController::class,'shipment_search']);
		Route::post('shipment-search/public/{id}', [ShipperController::class,'publish_shipment']);
	});

	#DRIVER
	Route::group(['prefix' => 'driver'], function(){
		Route::get('/', [DriverController::class,'index'])->name('driver.index'); 
		Route::get('view/{id}', [DriverController::class,'view'])->name('driver.view');
		Route::get('view_load/{id}', [DriverController::class,'view_load'])->name('driver.view_load');
		Route::get('generate_invoice/{id}', [DriverController::class,'get_invoice']);
		// Route::get('view_load/{id}', [DriverController::class,'my_loads_demo'])->name('driver.my_loads_demo');
		Route::get('view_profile/{id}', [DriverController::class,'view_profile'])->name('shipper.view_profile');
		Route::post('update/{id}', [DriverController::class,'update'])->name('driver.update');
		Route::post('accept/{id}', [DriverController::class,'accepted'])->name('driver.accepted');
		Route::post('reject/{id}', [DriverController::class,'rejected'])->name('driver.rejected');
		Route::post('block/{id}/{status}', [DriverController::class,'blockStatus'])->name('driver.block.status');
		Route::post('change-permissions/{id}', [DriverController::class,'change_permission'])->name('driver.change-permissions');
		
	});

	#BOOKING
	Route::group(['prefix' => 'booking'], function(){
		Route::get('/', [BookingController::class,'index'])->name('booking.index'); 
		Route::delete('delete/{id}', [BookingController::class,'destroy'])->name('booking.delete');
		// Route::get('view/{id}', [DriverController::class,'view'])->name('driver.view');
		// Route::post('update/{id}', [DriverController::class,'update'])->name('driver.update');
		// Route::post('accept/{id}', [DriverController::class,'accepted'])->name('driver.accepted');
		// Route::post('reject/{id}', [DriverController::class,'rejected'])->name('driver.rejected');
		// Route::post('block/{id}/{status}', [DriverController::class,'blockStatus'])->name('driver.block.status');
		
	});


	
});


Route::group(['namespace' => 'Shipper', 'prefix' => 'shipper', 'as' => 'shipper.', 'middleware' => 'auth', 'middleware' => 'shipper'], function () {

	// Route::get('/',[AuthController::class],'index')->name('home');
	Route::get('/', function () {
		return view('shipper');
	});

	Route::get('pdf/',[InvoiceController::class, 'index']);
	

	Route::get('/design/page', function () {
	    return view('shipper.pages.shipment.desing');
	});

	Route::get('privacy-policy', function () {
	    return view('shipper.pages.privacy_policy');
	});

	Route::get('terms-conditions', function () {
	    return view('shipper.pages.terms_conditions');
	});

	Route::get('about-us', function () {
	    return view('shipper.pages.about_us');
	});

	// Route::get('notification', function () {
	//     return view('shipper.pages.notification.index');
	// });

	Route::get('notification', [NotificationController::class, 'index']);

	Route::get('chat/{id}',[ChatController::class, 'index']);
	Route::post('get-chat',[ChatController::class, 'getChat'])->name('shipper.chat');
	Route::get('chat-demo/{id}',[ChatController::class, 'chat_demo']);
	Route::get('get_transaction',[ChatController::class, 'get_transaction']);

	Route::get('product/create',[ProductController::class, 'create'])->name('product.create');
	Route::get('product/{id?}',[ProductController::class, 'index'])->name('product.index');
	Route::post('product/submit',[ProductController::class, 'store'])->name('product.submit');
	Route::get('product/edit/{id}',[ProductController::class, 'edit'])->name('product.edit');
	Route::post('product/data/{id}', [ProductController::class, 'update'])->name('product.data');
	Route::get('product/show/{id}', [ProductController::class, 'show'])->name('product.show');
	Route::delete('product/delete/{id?}',[ProductController::class, 'destroy'])->name('product.delete');
	Route::post('product/import',[ProductController::class,'productImport']);

	

	Route::get('address', [AddressController::class,'index'])->name('address.index');
	Route::get('address/create', [AddressController::class,'create'])->name('address.create');
	Route::post('address/submit',[AddressController::class, 'store'])->name('address.submit');
	Route::get('address/edit/{id}',[AddressController::class, 'edit'])->name('address.edit');
	Route::post('address/update/{id}', [AddressController::class, 'update'])->name('address.update');
	Route::get('address/show/{id}', [AddressController::class, 'show'])->name('address.show');
	Route::delete('address/delete/{id?}',[AddressController::class, 'destroy'])->name('address.delete');
	Route::post('address/get-state',[AddressController::class, 'get_state']);
	Route::post('address/get-city',[AddressController::class, 'get_city']);

	Route::get('my-profile',[AuthController::class, 'my_profile']);
	Route::get('setting',[AuthController::class, 'setting']);
	Route::post('update-profile',[AuthController::class, 'update_profile']);
	Route::get('change-password',[AuthController::class, 'change_password']);
	Route::post('update-password',[AuthController::class, 'update_password']);

	// Route::get('create/shippment', [ShipmentDesignController::class, 'create']);
	Route::get('change/lang/{lang}',[AuthController::class, 'changeLan'])->name('change-lang');
	Route::get('carrier-info/{id}',[AuthController::class, 'carrier_info'])->name('carrier-info');
    Route::prefix('shipment')->group(function () {


    	Route::post('address/submit','ShipmentController@addAddress')->name('shipment.address.submit');
    	Route::get('addresses','ShipmentController@getFacilityAddress');
    	Route::get('products','ShipmentController@getProducts');
    	Route::post('product/submit','ShipmentController@addProduct')->name('shipment.product.submit');
        Route::get('search', 'ShipmentController@index');
        Route::get('search/{id}', 'ShipmentController@index')->name('shipment.search');
        Route::post('search/public/{id}', 'ShipmentController@public_shipment');
        Route::post('track-shipment', 'ShipmentController@track_shipment_link');
        Route::post('/product/data', 'ShipmentController@getProductData')->name('shipment.product.data');
        Route::any('/remove-product/{id}', 'ShipmentController@removeProduct')->name('shipment.remove-product');
        Route::get('create', 'ShipmentController@create')->name('shipment.create');
        Route::get('create/truck', 'ShipmentController@create_truck');
        Route::any('/step2/create', 'ShipmentController@create_step_2')->name('shipment.create.step2');
        Route::any('/design2/create', 'ShipmentController@design_step_2');
        Route::any('/step3/create', 'ShipmentController@create_step_3')->name('shipment.create.step3');
        Route::any('/step3/store', 'ShipmentController@store_step_3')->name('shipment.store.step3');
        Route::any('/multiple/store', 'ShipmentController@store_multiple_location')->name('shipment.store.multiple');
        Route::any('/check_validations', 'ShipmentController@check_validation');
        Route::any('/check_validations_edit', 'ShipmentController@check_validation_edit');

		Route::get('edit/{id}','ShipmentController@edit')->name('shipment.edit');
		Route::any('/step2/edit/{id}', 'ShipmentController@edit_step_2')->name('shipment.edit.step2');
		Route::any('/step3/edit/{id}', 'ShipmentController@edit_step_3')->name('shipment.edit.step3');
		Route::any('/multiple/update/{id}', 'ShipmentController@update_multiple_location')->name('shipment.update.multiple');
		Route::any('/step3/update/{id}', 'ShipmentController@update_step_3')->name('shipment.update.step3');
		Route::get('edit-search/{id}', 'ShipmentController@edit_search')->name('shipment.edit-search');
        Route::post('update-booking', 'ShipmentController@updateBookingData');
        Route::post('payment/{id}', 'ShipmentController@payment');

        Route::any('/location-sim', 'ShipmentDesignController@locations_sim')->name('shipment.location-sim');

		Route::get('live-search', 'ShipmentController@live_search')->name('live-search');

		Route::get('success_bid', function () {
	    	return view('shipper.pages.shipment.success_bid');
		});

		Route::get('success_shipment', function () {
	    	return view('shipper.pages.shipment.success_shipment');
		});
    });

	Route::group(['prefix' => 'search-available-trucks'], function(){
		Route::get('/', [SearchAvailableTruckController::class,'index'])->name('search-available-trucks.index'); 		
		Route::get('/demo', [SearchAvailableTruckController::class,'demo_page'])->name('search-available-trucks.demo'); 		
		Route::get('/{id}', [SearchAvailableTruckController::class,'index'])->name('search-available-trucks.index'); 		
		Route::get('book-now/{id}/{availability_id}', [SearchAvailableTruckController::class,'book_now'])->name('search-available-trucks.book-now'); 		
		Route::get('bid-now/{id}/{availability_id}', [SearchAvailableTruckController::class,'bid_now'])->name('search-available-trucks.bid-now'); 		
		Route::post('confirm-booking', [SearchAvailableTruckController::class,'confirm_booking'])->name('search-available-trucks.confirm-booking'); 		
		Route::post('bidding', [SearchAvailableTruckController::class,'search_available_truck_bidding'])->name('search-available-trucks.bidding'); 		
		Route::get('matched-shipment/{id}', [SearchAvailableTruckController::class,'matched_shipment'])->name('search-available-trucks.matched-shipment'); 		
		Route::post('filter', [SearchAvailableTruckController::class,'filter'])->name('search-available-trucks.filter'); 		
	});

	Route::group(['prefix' => 'manager-management'], function(){
		Route::get('/', [ManagerManagementController::class,'index'])->name('manager-management.index');
		Route::get('create/', [ManagerManagementController::class,'create'])->name('manager-management.create');
		Route::post('store/', [ManagerManagementController::class,'store'])->name('manager-management.store');
		Route::get('edit/{id}', [ManagerManagementController::class,'edit'])->name('manager-management.edit');
		Route::post('update/{id}', [ManagerManagementController::class,'update'])->name('manager-management.update');
		Route::post('approve/{id}/{status}', [ManagerManagementController::class,'approveStatus'])->name('manager-management.approve.status');
	});

	// Route::prefix('shipment')->group(function () {
		// Route::get('manage-shipment','ManageShipmentController@index')->name('manage-shipment.index');
		Route::get('manage-shipment', [ManageShipmentController::class,'index'])->name('manage-shipment.index');
		
		Route::get('manage-shipment/design', [ManageShipmentController::class,'design']);

		Route::post('manage-shipment/change-status', [ManageShipmentController::class,'change_status']);
		Route::get('manage-shipment/{id}', [ManageShipmentController::class,'details']);


		Route::get('manage-shipment/carrier-info/{id}',[ManageShipmentController::class,'carrier_info']);
		Route::delete('manage-shipment/delete/{id?}',[ManageShipmentController::class, 'destroy'])->name('manage-shipment.delete');
		Route::post('manage-shipment/accept_bid',[ManageShipmentController::class, 'accept_bid_request']);
		Route::post('manage-shipment/cancel_shipment',[ManageShipmentController::class, 'cancelShipment']);
		Route::post('manage-shipment/cancel_driver',[ManageShipmentController::class, 'cancelDriver']);
		Route::post('manage-shipment/decline_bid',[ManageShipmentController::class, 'decline_bid_request']);
		Route::post('manage-shipment/cancel_request_accept',[ManageShipmentController::class, 'cancelRequest']);
		Route::post('manage-shipment/load-map',[ManageShipmentController::class, 'map_loading']);
		Route::get('manage-shipment/test/test-time',[ManageShipmentController::class, 'test_time']);
		Route::post('manage-shipment/share/email',[ManageShipmentController::class,'share_email']);
		Route::get('manage-shipment/share-link/{id}',[ManageShipmentController::class,'share_link']);
		// Route::post('manage-shipment/load-map',[ManageShipmentController::class, 'map_loading']);
		Route::get('generate-pdf/{id}', [ManageShipmentController::class, 'generatePDF']);
		Route::get('generate-image/{id}', [ManageShipmentController::class, 'generateImage']);
	// });
    
	// Maulik Changes
	Route::prefix('multiple-shipment')->group(function () {
    	Route::post('address/submit','MultipleShipmentController@addAddress')->name('multiple-shipment.address.submit');
    	Route::get('addresses','MultipleShipmentController@getFacilityAddress');
    	Route::post('product/submit','MultipleShipmentController@addProduct')->name('multiple-shipment.product.submit');
        Route::get('search', 'MultipleShipmentController@index');
        Route::get('search/{id}', 'MultipleShipmentController@index')->name('multiple-shipment.search');
        Route::post('search/public/{id}', 'MultipleShipmentController@public_shipment');
        Route::post('/product/data', 'MultipleShipmentController@getProductData')->name('multiple-shipment.product.data');
        Route::get('create', 'MultipleShipmentController@create')->name('multiple-shipment.create');
        Route::get('create/truck', 'MultipleShipmentController@create_truck');
        Route::any('/step2/create', 'MultipleShipmentController@create_step_2')->name('multiple-shipment.create.step2');
        Route::any('/design2/create', 'MultipleShipmentController@design_step_2');
        Route::any('/step3/create', 'MultipleShipmentController@create_step_3')->name('multiple-shipment.create.step3');
        Route::any('/step3/store', 'MultipleShipmentController@store_step_3')->name('multiple-shipment.store.step3');

		Route::get('live-search', 'MultipleShipmentController@live_search')->name('multiple-shipment-live-search');
    });

});
Route::group(['prefix'=>'carrier','as'=>'carrier.','middleware'=>'auth','middleware'=>'carrier'],function(){
	  Route::get('my-profile',[CarrierAuthController::class, 'my_profile']);
	  Route::get('setting',[CarrierAuthController::class, 'setting']);
	  Route::get('change-password',[CarrierAuthController::class, 'change_password']);
	  Route::post('update-password',[CarrierAuthController::class, 'update_password']);


	  Route::get('privacy-policy', function () {
	    return view('carrier.pages.privacy_policy');
		});

	Route::get('terms-conditions', function () {
	    return view('carrier.pages.terms_conditions');
	});

	Route::get('about-us', function () {
	    return view('carrier.pages.about_us');
	});


	  Route::post('update-profile',[CarrierAuthController::class, 'update_profile']);
	  Route::get('driver',[CarrierDriverController::class,'index'])->name('driver');
	  Route::get('driver/create',[CarrierDriverController::class,'create'])->name('driver.create');
	  Route::post('driver/store',[CarrierDriverController::class,'store'])->name('driver.store');
	  Route::get('driver/edit/{id}',[CarrierDriverController::class,'edit'])->name('driver.edit');
	  Route::post('driver/update',[CarrierDriverController::class,'update'])->name('driver.update');
	  /// New Added Route 15-02-2022  ///
	  Route::get('driver/vehiclelist/{id}',[CarrierDriverController::class,'vehicle_list']);
	  Route::get('driver/assingvehicle/',[CarrierDriverController::class,'assing_vehicle']);
	  Route::get('driver/delete',[CarrierDriverController::class,'destroy']);

	  Route::get('driver/assing/{id}',[CarrierDriverController::class,'assing']);
	  Route::get('driver/assing/edit/vehicle/{id}',[CarrierDriverController::class,'editVehicel']);
	  Route::get('driver/assing/update/vehicle/',[CarrierDriverController::class,'updateVehicel']);
	  Route::get('driver/assingdeletevehicle',[CarrierDriverController::class,'assignDelete']);
	  Route::get('driver/trashdriver',[CarrierDriverController::class,'trashDriver']);
	  Route::get('driver/show/{id}',[CarrierDriverController::class,'show']);
	  Route::get('driver/fetchdriver',[CarrierDriverController::class,'fetchDriver']);
	  Route::get('driver/retrivedrive',[CarrierDriverController::class,'retriveDriver']);



	  ////
	  Route::get('vehicle',[VehicleController::class,'index'])->name('vehicle');
	  Route::get('vehicle/create',[VehicleController::class,'create'])->name('vehicle.create');
	  Route::post('vehicle/store',[VehicleController::class,'store'])->name('vehicle.store');
	  Route::get('vehicle/getcategory',[VehicleController::class,'getCategory'])->name('vehicle.getcategory');
	  Route::get('vehicle/edit/{id}',[VehicleController::class,'edit'])->name('vehicle.edit');
	  Route::post('vehicle/update',[VehicleController::class,'update'])->name('vehicle.update');
	  Route::get('vehicle/getvehicle',[VehicleController::class,'getVehicle']);
	  Route::get('vehicle/delete',[VehicleController::class,'destroy']);

	  Route::get('vehicle/trashvehicle',[VehicleController::class,'trashVehicle'])->name('trashvehicle');
	  Route::get('vehicle/showvehicle/{id}',[VehicleController::class,'show'])->name('show');
	  Route::get('vehicle/retrive/',[VehicleController::class,'retriveVehicle']);
});
Route::get('get', [ManageShipmentController::class, 'generateImage']);

require __DIR__.'/auth.php';


