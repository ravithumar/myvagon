<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\Shipper\AuthController;
use App\Http\Controllers\WebsiteController;
use App\Http\Controllers\Carrier\CarrierAuthController;

use Illuminate\Support\Facades\Route;

Route::get('/register', [RegisteredUserController::class, 'create'])
                ->middleware('guest')
                ->name('register');

Route::post('/register', [RegisteredUserController::class, 'store'])
                ->middleware('guest');



#SHIPPER



// Route::get('shipper/', [AuthController::class, 'createLogin'])->name('shipper.login');
Route::get('shipper/login', [AuthController::class, 'createLogin'])->name('shipper.login');
Route::get('change/lang/{lang}',[WebsiteController::class, 'changeLang'])->name('change-lang');
Route::get('shipper/logout', [AuthController::class, 'logout'])->name('shipper.logout');
Route::post('shipper/login', [AuthController::class, 'storeLogin'])->name('shipper.login.post');

Route::get('shipper/dashboard', [AuthController::class, 'shipperDashboard'])
        ->middleware('shipper')
        ->name('shipper.dashboard');

Route::get('shipper/dashboard/{id}', [AuthController::class, 'shipperDashboard'])
        ->middleware('shipper');



Route::get('shipper/register/{userid?}', [AuthController::class, 'create'])
                ->name('shipper.register');
             
Route::post('shipper/register', [AuthController::class, 'store'])
                ->name('register.store');

Route::post('shipper/register/urlencode({userid?})', [AuthController::class, 'storeCompanyProfile'])
                ->name('register.store.profile');

Route::post('shipper/register/facility/urlencode({userid?})', [AuthController::class, 'storeFacilityAddress'])
                ->name('register.store.facility');


Route::post('shipper/verification/{id}',  [AuthController::class, 'shipperVerification'])->name('shipper.verify');
Route::post('shipper/verified/{id}', [AuthController::class, 'shipperVerified'])->name('shipper.verified');

Route::get('shipper/email/verify/{email}', [AuthController::class, 'shipperEmailVerify'])->name('shipper.email.verify');
Route::get('shipper/email/manager_verify/{email}', [AuthController::class, 'shipperEmailVerifyManager'])->name('shipper.email.verify_manager');
Route::get('shipper/email/getprogress/{email}', [AuthController::class, 'shipperEmailProgress'])->name('shipper.getprogress');

Route::get('shipper/phone/verify/{email}', [AuthController::class, 'shipperPhoneVerify'])->name('shipper.phone.verify');


#FORGOT_PASSWORD
Route::get('shipper/forgot/password', [AuthController::class, 'createForgotPassword'])->name('shipper.forgot.password');
Route::post('shipper/forgot/password', [AuthController::class, 'forgotPasswordPost'])->name('shipper.forgot.password.post');

// #RESET-PASSWORD
// Route::get('reset-password/{token}', [AuthController::class, 'resetPassword'])->name('shipper.password.reset');
// Route::post('reset-password', [AuthController::class, 'resetPasswordPost'] )->name('shipper.password.update');



Route::get('admin/login', [AuthenticatedSessionController::class, 'create'])
                ->middleware('guest')
                ->name('login');

Route::post('admin/login', [AuthenticatedSessionController::class, 'store'])
                ->middleware('guest');

Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
                ->middleware('guest')
                ->name('password.request');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
                ->middleware('guest')
                ->name('password.email');

Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
                ->middleware('guest')
                ->name('password.reset');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
                ->middleware('guest')
                ->name('password.update');

Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->middleware('auth')
                ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
                ->middleware(['auth', 'signed', 'throttle:6,1'])
                ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware(['auth', 'throttle:6,1'])
                ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->middleware('auth')
                ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
                ->middleware('auth');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
                ->middleware('auth')
                ->name('logout');

Route::get('carrier/register',[CarrierAuthController::class,'create'])->name('carrier.register');
Route::post('carrier/store',[CarrierAuthController::class,'store'])->name('carrier.store');
Route::post('carrier/login/',[CarrierAuthController::class,'storeLogin'])->name('carrier.login.post');
Route::get('carrier/dashboard',[CarrierAuthController::class,'carrierDashboard'])->name('carrier.dashboard')->middleware('carrier');
Route::get('carrier/logout', [CarrierAuthController::class, 'logout'])->name('carrier.logout');