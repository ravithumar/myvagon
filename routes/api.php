<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\TruckController;
use App\Http\Controllers\Api\DriverAuthController;
use App\Http\Controllers\Api\ForgotPasswordController;
use App\Http\Controllers\Api\PostTruckController;
use App\Http\Controllers\Api\BookingController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/init/{type}/{version}/{driver_id}', [AuthController::class,'init']);
Route::post('/login', [AuthController::class,'login'])->middleware('localization');
Route::get('/logout', [AuthController::class,'logout'])->middleware('auth:api','localization');
Route::post('/register', [AuthController::class,'register'])->middleware('localization');
Route::post('/send/otp', [AuthController::class,'send_otp']);
Route::get('/system-date-time', [AuthController::class,'system_date_time']);
 Route::get('/gr', [AuthController::class,'lang'])->middleware('localization');


Route::post('/driver/register', [DriverAuthController::class,'register'])->middleware('localization');
Route::post('/driver/register_new', [DriverAuthController::class,'register_new'])->middleware('localization');
Route::post('/image/upload', [DriverAuthController::class,'imageUpload']);
Route::post('/driver/login', [DriverAuthController::class,'login'])->middleware('localization');
Route::post('/driver/settings', [DriverAuthController::class,'userSetting'])->middleware('auth:api','localization');
Route::post('/driver/get/settings', [DriverAuthController::class,'userGetSetting'])->middleware('auth:api','localization');
Route::post('/driver/change/password', [DriverAuthController::class,'changePassword'])->middleware('auth:api','localization');
Route::post('/driver/profile/update', [DriverAuthController::class,'driverProfile'])->middleware('auth:api');
Route::post('/driver/profile/update_bkp', [DriverAuthController::class,'driverProfileBKP'])->middleware('auth:api','localization');
Route::post('chat-users', [DriverAuthController::class,'chatUsers'])->middleware('localization');
Route::post('chat-messages', [DriverAuthController::class,'chatMessages'])->middleware('localization');

Route::post('/dispature/manage-drivers', [DriverAuthController::class,'manage_driver'])->middleware('auth:api');
Route::post('dispature/edit-permission', [DriverAuthController::class,'edit_driver_permission']);

Route::get('/driver/get/bid_list', [DriverAuthController::class,'bid_list']);
Route::get('notification-list/{driver_id}', [DriverAuthController::class,'notificationList']);

/// New Added for user setting   ///
Route::get('/usersetting/list/{id}',[DriverAuthController::class,'user_setting'])->middleware('auth:api','localization');
Route::post('usersetting/notification/update',[DriverAuthController::class,'userSettingUpdate'])->middleware('auth:api','localization');


Route::post('statistics',[BookingController::class,'statistic'])->middleware('localization');
Route::post('statistics-detail',[BookingController::class,'statisticDetail'])->middleware('localization');
/////
Route::post('/driver/post/bid', [PostTruckController::class,'post_bid'])->middleware('localization');
Route::post('/driver/post/book', [PostTruckController::class,'post_book'])->middleware('localization');


#MOBILR_OTP_VERIFY
Route::post('/phone/verify', [DriverAuthController::class,'phoneVerify'])->middleware('localization');
Route::post('/email/verify', [DriverAuthController::class,'emailVerify'])->middleware('localization');
Route::post('/email/test', [DriverAuthController::class,'emailTest']);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

 #TRUCK_CATEGORY
 Route::post('/truck/category/listing', [TruckController::class,'categoryListing'])->middleware('localization');
 Route::get('/truck/type/listing', [TruckController::class,'truckTypeListing'])->middleware('localization');
 Route::get('/package/listing', [TruckController::class,'packageListing'])->middleware('localization');
 Route::get('/truck/unit', [TruckController::class,'unitListing'])->middleware('localization');
 Route::get('/truck/features', [TruckController::class,'truckFeatures'])->middleware('localization');
 Route::get('/truck/brands', [TruckController::class,'truckBrands'])->middleware('localization');
 Route::get('/gr', [TruckController::class,'lang'])->middleware('localization');



Route::post('forgot/password', [ForgotPasswordController::class,'forgot'])->middleware('localization');
Route::post('password/reset', [ForgotPasswordController::class, 'reset'])->middleware('localization');


  #POST_TRUCK_AVAILABILITY
Route::post('/post-availability', [PostTruckController::class, 'postTruck'])->middleware('localization');


//  Route::post('/shipment/search', 'Api\PostTruckController@searchShipment');
 Route::post('/post-availability-result', 'Api\PostTruckController@AvailabilityResult')->middleware('localization');
//  Route::post('/search-loads', 'Api\PostTruckController@searchLoads');
 Route::post('/search-loads', 'Api\PostTruckController@searchLoads')->middleware('localization');

 # Bookings
 Route::post('booking-load-detail', [BookingController::class,'booking_details'])->middleware('localization');
//  Route::post('/booking/my_loads', [BookingController::class,'my_loads']);
//  Route::post('/booking/my-loads-new', [BookingController::class,'my_loads_new']);
 Route::post('/booking/my-loads-new3', [BookingController::class,'my_loads_new'])->middleware('localization');
 Route::post('/my-loads', [BookingController::class,'my_loads'])->middleware('localization');
 Route::post('/bid-requests', [BookingController::class,'bid_requests'])->middleware('localization');
 Route::post('cancel-request', [BookingController::class,'cancelRequest'])->middleware('localization');
 Route::post('cancel-bid-request', [BookingController::class,'cancelBidRequest'])->middleware('localization');
 Route::post('trash-posted-truck', [BookingController::class,'TrashPostedTruck'])->middleware('localization');
 Route::post('/bid-request-accept-reject', [BookingController::class,'acceptdeclineBiddingRequest'])->middleware('localization');
 Route::post('/bidding-request-accept', [BookingController::class,'acceptBiddingRequest'])->middleware('localization');
 Route::post('/bidding-request-decline', [BookingController::class,'declineBiddingRequest'])->middleware('localization');
 Route::post('arrived-at-location', [BookingController::class,'arrivedAtPickupLocation'])->middleware('localization'); 
 Route::post('start-loading', [BookingController::class,'startLoading'])->middleware('localization'); 
 Route::post('start-journey', [BookingController::class,'startJourney'])->middleware('localization'); 
 Route::post('complete-trip', [BookingController::class,'completeTrip'])->middleware('localization'); 
 Route::post('upload-pod', [BookingController::class,'uploadPOD'])->middleware('localization'); 
 Route::post('review-rating', [BookingController::class,'reviewRating'])->middleware('localization'); 
 Route::post('transaction-history', [BookingController::class,'transactionHistory'])->middleware('localization'); 
 Route::post('accept-payment', [BookingController::class,'acceptPayment'])->middleware('localization'); 
 Route::get('contact-us', [BookingController::class,'contactUs'])->middleware('localization'); 
//  Route::post('/booking/posted-truck', [BookingController::class,'posted_truck']);







