<?php
Breadcrumbs::for ('dashboard', function ($trail) {
    $trail->push('Dashboard', route('admin.dashboard'));
});
Breadcrumbs::for ('config', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('System Configuration', route('admin.system.config'));
});
Breadcrumbs::for ('product', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Products', route('shipper.product.index'));
});

Breadcrumbs::for ('package', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('package', route('admin.package.type.index'));
});

Breadcrumbs::for ('truck', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('truck', route('admin.truck.category.index'));
});

Breadcrumbs::for ('shipper', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('shipper', route('admin.shipper.index'));
});

Breadcrumbs::for ('driver', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('driver', route('admin.driver.index'));
});



Breadcrumbs::for ('app_config', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Application Configuration', route('admin.system.config'));
});







// // Home > About
// Breadcrumbs::for ('about', function ($trail) {
//     $trail->parent('home');
//     $trail->push('About', route('about'));
// });

// // Home > Blog
// Breadcrumbs::for ('blog', function ($trail) {
//     $trail->parent('home');
//     $trail->push('Blog', route('blog'));
// });

// // Home > Blog > [Category]
// Breadcrumbs::for ('category', function ($trail, $category) {
//     $trail->parent('blog');
//     $trail->push($category->title, route('category', $category->id));
// });

// // Home > Blog > [Category] > [Post]
// Breadcrumbs::for ('post', function ($trail, $post) {
//     $trail->parent('category', $post->category);
//     $trail->push($post->title, route('post', $post->id));
// });