        <!-- Vendor js -->


        <script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
        <!-- <script src="{{ URL::asset('assets/libs/notiflix/notiflix-2.1.2.js')}}"></script> -->
        <script src="{{ URL::asset('assets/libs/Notiflix/src/notiflix.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{ URL::asset('js/keypress.js')}}"></script>
        <!-- image zoom script -->
        <!-- Magnific Popup-->
         <script src="{{ URL::asset('shipper/js/common.js')}}"></script>

         <script>
            var NODE_URL = "{{env('NODE_URL')}}";
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script src="{{ asset('shipper/js/shipper_socket.js')}}"></script>
        <script >

			 
			     
             
        </script>
        @yield('script-bottom')


