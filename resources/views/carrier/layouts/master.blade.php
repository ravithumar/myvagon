<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>MYVAGON | SHIPPER</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="My vegon" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('carrier/css/style.css')}}">


        @include('carrier.layouts.head')
        
    </head>
    <body>

          <!-- Begin page -->
          <div id="wrapper">
      @include('carrier.layouts.topbar')
      @include('carrier.layouts.sidebar')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
      @yield('content')
                </div> <!-- content -->
    @include('carrier.layouts.footer')
            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    @include('carrier.layouts.footer-dash-script')


    <!-- <input type="text" value="{{ Session::get('running') }}" name="session" id="sessionid" /> -->

    <script>
         function codeAddress() {
            // console.log(document.getElementById('sessionid').value);
        }


        window.onload = codeAddress;




    </script>

    </body>
</html>
