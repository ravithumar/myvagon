<!-- Topbar Start -->

<style>
    .notification-list .noti-icon-badge{
        /*top: 0;*/
    }
</style>
<!-- <ul class="prev">
   <li><button class="button-menu-mobile"></button></li>
</ul> -->
<div class="navbar-custom d-flex align-items-center justify-content-between">
    <div class="item-container d-flex align-items-center">
        <div class="logo-box px-3">
            <a href="{{ route('shipper.dashboard') }}" class="logo">
               <img src="{{ URL::asset('shipper/images/logo.png')}}">
            </a>
        </div>
        <ul class="list-unstyled topnav-menu topnav-menu-left m-0 d-flex align-items-center">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                <i class="fe-menu"></i>
                </button>
            </li>
        </ul>
        <!-- <div class="top-destination ml-2 mr-2">
            <form class="app-search-form mb-0">
                    <div class="app-search-box d-flex align-items-center">
                        <div class="input-group">
                            <select name=""  class="form-control dropdown-search destinations select2">
                                
                                <option>location 1</option>
                                <option>location 2</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control search-option" placeholder="Search available trucks...">
                            <div class="input-group-append">
                                <button class="btn btn-primary search-btn position-absolute" type="submit">
                                <i class="fe-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
         </div> -->
    </div>
    <ul class="list-unstyled topnav-menu float-right mb-0 d-flex align-items-center">
    
      <!--   {{-- <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="false" aria-expanded="false">
                <i class="fa fa-comment-alt noti-icon"></i>
            </a>
        </li> --}} -->
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle  waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <i class="fe-bell noti-icon"></i>
                <span class="badge badge-danger rounded-circle noti-icon-badge">0</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-lg">
                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="m-0">
                    <span class="float-right">
                        <a href="" class="text-dark">
                            <small>Clear All</small>
                        </a>
                    </span>Notification
                    </h5>
                </div>
                <div class="slimscroll noti-scroll">
                    <!-- item-->
                    {{-- <a href="javascript:void(0);" class="dropdown-item notify-item active">
                        <div class="notify-icon">
                        <img src="assets/images/users/user-1.jpg" class="img-fluid rounded-circle" alt="" /> </div>
                        <p class="notify-details">Cristina Pride</p>
                        <p class="text-muted mb-0 user-msg">
                            <small>Hi, How are you? What about our next meeting</small>
                        </p>
                    </a> --}}
                    <!-- item-->
                    {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-primary">
                            <i class="mdi mdi-comment-account-outline"></i>
                        </div>
                        <p class="notify-details">Caleb Flakelar commented on Admin
                            <small class="text-muted">1 min ago</small>
                        </p>
                    </a> --}}
                    <!-- item-->
                    {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon">
                        <img src="assets/images/users/user-4.jpg" class="img-fluid rounded-circle" alt="" /> </div>
                        <p class="notify-details">Karen Robinson</p>
                        <p class="text-muted mb-0 user-msg">
                            <small>Wow ! this admin looks good and awesome design</small>
                        </p>
                    </a> --}}
                    <!-- item-->
                    {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-warning">
                            <i class="mdi mdi-account-plus"></i>
                        </div>
                        <p class="notify-details">New user registered.
                            <small class="text-muted">5 hours ago</small>
                        </p>
                    </a> --}}
                    <!-- item-->
                    {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-info">
                            <i class="mdi mdi-comment-account-outline"></i>
                        </div>
                        <p class="notify-details">Caleb Flakelar commented on Admin
                            <small class="text-muted">4 days ago</small>
                        </p>
                    </a> --}}
                    <!-- item-->
                    {{-- <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-secondary">
                            <i class="mdi mdi-heart"></i>
                        </div>
                        <p class="notify-details">Carlos Crouch liked
                            <b>Admin</b>
                            <small class="text-muted">13 days ago</small>
                        </p>
                    </a> --}}
                </div>
                <!-- All-->
                {{-- <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                    View all
                    <i class="fi-arrow-right"></i>
                </a> --}}
            </div>
        </li>
        <!-- <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <span class="pro-user-name text-capitalize mr-2">
                    <b>{{ Auth::user()->first_name }}</b>
                </span>
                <img src="{{asset('images/default.png')}}" alt="user-image" class="rounded-circle">
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">

                <a href="{{ url('carrier/my-profile') }}" class="dropdown-item notify-item">
                    <span>My Profile</span>
                </a>
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <span>Settings</span>
                </a>
                <a href="{{ url('shipper/change-password') }}" class="dropdown-item notify-item">
                    <span>Change Password</span>
                </a>

                <a href="{{ url('shipper/privacy-policy') }}" class="dropdown-item notify-item">
                    <span>Privacy Policy</span>
                </a>

                 <a href="{{ url('shipper/terms-conditions') }}" class="dropdown-item notify-item">
                    <span>Terms & Conditions</span>
                </a>

                 <a href="{{ url('shipper/about-us') }}" class="dropdown-item notify-item">
                    <span>About Us</span>
                </a>
                <a href="{{ route('shipper.logout')}}" class="dropdown-item notify-item">
                    <span>Logout</span>
                </a>
            </div>
        </li> -->
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <span class="pro-user-name text-capitalize mr-2">
                    <b>{{ Auth::user()->first_name }}</b>
                </span>
                @if(Auth::user()->profile != "") 
                    <img src="{{ env('S3_IMAGE_URL')}}shipper/{{ Auth::user()->profile }}" class="rounded-circle">
                @else 
                <img src="{{asset('images/default.png')}}" alt="user-image" class="rounded-circle">
                 @endif
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown " style="border-radius: 25px;width: 270px;height: 400px">
                 <div style="border-bottom: 1px solid #cccccc;">
                    <a href="{{url('carrier/my-profile')}}" class="dropdown-item notify-item">
                        <span style="color: #1F1F41;font-weight: bold;font-size:17px">{{__('My Profile')}}</span>
                    </a>
                </div>
                <div style="border-bottom: 1px solid #cccccc;margin-top: 15px">
                <a href="{{url('carrier/setting')}}" class="dropdown-item notify-item">
                    <span style="color: #1F1F41;font-weight: bold;font-size: 17px">{{__('Settings')}}</span>
                </a>
                </div>
                <div style="border-bottom: 1px solid #cccccc;margin-top: 15px">
                <a href="{{url('carrier/change-password')}}" class="dropdown-item notify-item">
                    <span style="color: #1F1F41;font-weight: bold;font-size: 17px;">{{__('Change Password')}}</span>
                </a>
                </div>
                <div style="border-bottom: 1px solid #cccccc;margin-top: 15px">
                <a href="{{url('carrier/privacy-policy')}}" class="dropdown-item notify-item">
                    <span style="color: #1F1F41;font-weight: bold;font-size: 17px;">{{__('Privacy Policy')}}</span>
                </a>
            </div>
            <div style="border-bottom: 1px solid #cccccc;margin-top: 15px">
                 <a href="{{url('carrier/terms-conditions')}}" class="dropdown-item notify-item">
                    <span style="color: #1F1F41;font-weight: bold;font-size: 17px;">{{__('Terms & Conditions')}}</span>
                </a>
            </div>
            <div style="border-bottom: 1px solid #cccccc;margin-top: 15px">
                 <a href="{{url('carrier/about-us')}}" class="dropdown-item notify-item">
                    <span style="color: #1F1F41;font-weight: bold;font-size: 17px">{{__('About Us')}}</span>
                </a>
            </div>
            <div style="margin-top: 15px">
                <a href="{{ route('carrier.logout')}}" class="dropdown-item notify-item">
                    <span style="color: #1F1F41;font-weight: bold;font-size: 17px;">{{__('Logout')}}</span>
                </a>
            </div>
            </div>
        </li>
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <span class="pro-user-name ml-1 text-uppercase lan">
                    EN <i class="mdi mdi-chevron-down"></i>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <div class="radio active text-left first">
                        <input type="radio" name="radio" id="radio5" value="option5" checked {{ session()->get('locale') == 'en' ? 'checked' : '' }}>
                        <label for="radio5"> English</label>
                    </div>
                </a>
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <div class="radio active text-left first">
                        <input type="radio" name="radio" id="radio5" value="option5" checked  {{ session()->get('locale') == 'gr' ? 'checked' : '' }}>
                        <label for="radio5"> Greek</label>
                    </div>
                </a>
            </div>
            <!-- <div class="col-md-4">
                <select class="form-control Langchange">
                    <option value="en" {{ session()->get('locale') == 'en' ? 'checked' : '' }}>English</option>
                    <option value="es" {{ session()->get('locale') == 'gr' ? 'checked' : '' }}>Greek</option>
                </select>
            </div> -->
        </li>
    </ul>
    
  <!--   <ul class="list-unstyled topnav-menu topnav-menu-left m-0 d-flex align-items-center">
        <li>
            <button class="button-menu-mobile waves-effect waves-light">
            <i class="fe-menu"></i>
            </button>
        </li>
        <li>
            <form class="app-search-form mb-0" novalidate="">
                <div class="app-search-box d-flex align-items-center">
                    <div class="input-group">
                        <select name="" class="form-control dropdown-search">
                            <option>Destination</option>
                            <option>location 1</option>
                            <option>location 2</option>
                        </select>
                    </div>
                    <div class="input-group">
                        <input type="text" class="form-control search-option" placeholder="Search available trucks...">
                        <div class="input-group-append">
                            <button class="btn btn-primary search-btn position-absolute" type="submit">
                            <i class="fe-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </li>
    </ul> -->
    
</div>
<!-- end Topbar -->
