<?php 
$user = Auth::user();
?>
<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="#">
                        <i class="fas fa-desktop"></i>
                        <span> Dashboard</span>
                    </a>
                </li>
                 <li>
                    <a href="{{route('carrier.dashboard')}}">
                        <i class="fas fa-search"></i>
                        <span> Search Loads</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-shipping-fast"></i>
                        <span> My Loads</span>
                    </a>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-truck"></i> <span>{{ __('My Fleet') }}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{route('carrier.driver')}}">
                                <span> {{__('Driver')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('carrier/driver/trashdriver')}}">
                                <span> {{__('Trash Driver')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('carrier.vehicle')}}">
                                <span> {{__('Vehicles')}}</span>
                            </a>
                        </li>
                         <li>
                            <a href="{{route('carrier.trashvehicle')}}">
                                <span> {{__('Trash Vehicles')}}</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#">
                        <i class="fas fa-truck"></i>
                        <span> Settings</span>
                    </a>
                </li>                              

                                         
                
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->