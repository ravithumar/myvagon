@extends('carrier.layouts.master')
@section('content')
<style type="text/css">
	.driver_form input[type=text],[type=email],[type=number] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
  border: none;
  border-bottom: 2px solid black;
}
[type=file] {
    position: absolute;
    filter: alpha(opacity=0);
    opacity: 0;
}
input,
[type=file] {
  border: 1px solid #CCC;
  border-radius: 3px;
  padding: 10px;
  width: 150px;
  margin: 0;
  left: 0;
  position: relative;
}
[type=file] {
  text-align: center;

  top: 0.5em;
  /* Decorative */
  background: #333;
  color: #fff;
  border: none;
  cursor: pointer;
}


</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					
				</div>
				<h4 class="page-title"></h4>
			</div>
			
		</div>
	</div>
	  <!-- @include('admin.include.flash-message') -->
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-4 offset-md-12">
							<div class="pt-3 pb-4">
								<h2>Update Driver</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 offset-md-12">
							<div class="pt-3 pb-4">
								<b style="color:#000000">Personal Details</b>
								<br>
							</div>
						</div>
						<div class="col-md-6">
							<div class="pt-3 pb-4">
								
								<form class="driver_form" method="post" action="{{url('carrier/driver/update')}}" enctype="multipart/form-data">
									@csrf
									  <div class="form-row">
									    <div class="form-group col-md-6">
									      <label for="inputEmail4">First Name</label>
									      <input type="text" name="first_name" class="form-control first_name" id="first_name" placeholder="First Name" required data-parsley-required-message="Please enter first name." data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in First Name." value="{{ $user->first_name }}" data-parsley-trigger="keyup" maxlength="40" >
									      @error('first_name')
									      <div class="alert alert-danger">{{ $message }}</div>
									      @enderror
									    </div>
									    <input type="hidden" name="id" value="{{ $user->id }}">
									    <input type="hidden" name="user_id" value="{{ $driver_vehicle->user_id }}">
									    <div class="form-group col-md-6">
									      <label for="inputPassword4">Last Name</label>
									      <input type="text" name="last_name" class="form-control last_name" id="last_name" placeholder="Last Name" required data-parsley-required-message="Please enter last name." data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in Last Name." value="{{ $user->last_name }}" data-parsley-trigger="keyup" maxlength="40">
									    </div>
									    	@error('last_name')
									      		<div class="alert alert-danger">{{ $message }}</div>
									      	@enderror
									  </div>
									  <div class="form-group">
									    <label for="inputAddress">Work Email</label>
									    <input type="email" name="email" id="email" class="form-control email" placeholder="Work Email" required data-parsley-required-message="Please enter work email." value="{{ $user->email }}" data-parsley-type-message="Please enter valid email address" data-parsley-trigger="keyup" maxlength="60" >
									    @error('email')
									      		<div class="alert alert-danger">{{ $message }}</div>
									    @enderror
									  </div>
									  <div class="form-group">
									    <label for="inputAddress2">Phone Number</label>
									    <input type="number" name="phone" class="form-control phone_number" id="phone_number" placeholder="Phone Number" required data-parsley-required-message="Please enter phone number." value="{{ $user->phone }}" maxlength="15"  data-parsley-trigger="keyup" data-parsley-maxlength-message="Only 15 number allowed.">
									  </div>
									  <div class="row">
									  	<div class="col-md-6">
									  		<div class="form-group">
									  		<label>Identity Proof Document</label>
									  		 <input type="file" name="id_proof"class="identity_proof_document" id="identity_proof_document" placeholder="Add profile picture" data-parsley-fileextension='jpeg,png,jpg,JPEG,PNG,JPG' data-parsley-errors-container="#id_proof_error_message"/>
									  		  <label for="identity_proof_document" style="float: left"><img src="{{ asset('images/carrier/'.$driver_vehicle->id_proof) }}" width="80"></label>
									  		   <input type="hidden" name="id_proof" value="{{ $driver_vehicle->id_proof }}">
									  		    <div id="id_proof_error_message" style="margin-top: 50px"></div>
									  	</div>
									  </div>
									  <div class="col-md-6">
									  	<div class="form-group">
									  		<img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="OpenImgId" style="margin-left:70px;margin-top: 40px">
									  	</div>
									  </div>
									</div>
									  <label for="licence">License</label>
									  <div class="row">
									  	<div class="col-md-6">
									  	<div class="form-group">
									  		 <input type="file" name="license" class="license" id="license" placeholder="Add profile picture" data-parsley-fileextension='jpeg,png,jpg,JPG,JPEG,PNG' data-parsley-errors-container="#license_error_message">
									  		  <label for="license" style="float: left"><img src="{{ asset('images/carrier/'.$driver_vehicle->license) }}" width="80"></label>
									  		   <input type="hidden" name="license" value="{{ $driver_vehicle->license }}">
									  		   <div id="license_error_message"></div>
									  		   
									  		  {{-- <p>(Upload vehicle paperwork & <br> EU Registration document)</p> --}}
									  	</div>
									  </div>
									  <div class="col-md-6"> 
									  	<div class="form-group">
									  		<img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="OpenImgLicense" style="margin-left:70px">
									  	</div>
									  </div>
									</div>
									 
								

									  <p>(Upload vehicle paperwork & <br> EU Registration document)</p>
									 <div class="form-row">
									 	<div class="form-group col-md-6">
									 		<button type="submit" class="btn btn-lg btn btn-primary btn-block">Update Driver</button>
									 	</div>
									 </div>
									  
								</form>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
        		var tagslistarr = requirement.split(',');
            var fileExtension = value.split('.').pop();
						var arr=[];
						$.each(tagslistarr,function(i,val){
   						 arr.push(val);
						});
            if(jQuery.inArray(fileExtension, arr)!='-1') {
              console.log("is in array");
              return true;
            } else {
              console.log("is NOT in array");
              return false;
            }
        }, 32)
        .addMessage('en', 'fileextension', 'Accept only this extension jpeg,png,jpg');
		$('.driver_form').parsley();
	});
	$("[type=file]").on("change", function(){
  // Name of file and placeholder
  var file = this.files[0].name;
  var dflt = $(this).attr("placeholder");
  if($(this).val()!=""){
    $(this).next().text(file);
  } else {
    $(this).next().text(dflt);
  }
});
	$('#OpenImgId').click(function(){ 
			$('#identity_proof_document').trigger('click'); 
	});
$('#OpenImgLicense').click(function(){ 
			$('#license').trigger('click'); 
	});
</script>
@endsection
