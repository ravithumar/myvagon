@extends('carrier.layouts.master')
@section('css')
 <link rel="stylesheet" type="text/css" href="{{asset('carrier/css/style.css')}}">
@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
          {{ Breadcrumbs::render('driver')}}
        </div>
        <h4 class="page-title"></h4>
      </div>
      
    </div>
  </div>
    @include('admin.include.flash-message')
  <div class="row">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-body">
            <div class="row">
            <div class="col-md-3 offset-md-10">
             
            </div>
          </div>
          <div class="card" style="border-radius: 18px;">
    <div class="card-body" >
      <div class="table-responsive">
        <table class="table">
            <thead>
              <tr>
                <td style="font-weight: bold">Image</td>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @if($user->count()  > 0)
              @foreach($user as $key=>$value)
              <tr>
                 <td scope="row"  class="user_logo">
                      <img src="{{ asset('images/carrier/'.$value->profile) }}" class="driver_logo">
                    </td>
                  <td>{{$value->name}}</td>
                  <td>{{$value->email}}</td>
                  <td>{{$value->phone}}</td>
                  <td><a href='{{url("carrier/driver/show",$value->id)}}' class="btn btn-primary">View</a>
                  <a href='javascript:;' class="btn btn-danger retriveDriver" data-id="{{$value->id}}">RETRIVE</a></td></td>
              </tr>
              @endforeach
              @else
              <tr>
                <td>No Drivers Available</td>
              </tr>
            @endif
            </tbody>
          </table>
        </div>

          </div>              
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
 @section('script')
<script type="text/javascript">
  $(document).ready(function(){
    $(document).on('click','.retriveDriver',function(){
      var driver_id = $(this).attr('data-id');
     
  Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to retrive this Driver?',
      'Yes',
      'No',
      function() {
          Notiflix.Loading.Standard();
          $.ajax({
              url: "{{url('carrier/driver/retrivedrive')}}",
              type: 'get',
              dataType: "JSON",
              data: {
                  "driver_id": driver_id,
              },
              success: function(returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Driver retrived successfully');
            
                   window.setTimeout(function(){location.reload()},3000)
                 

              }
          });
      });
    });
  })
</script>
@endsection
