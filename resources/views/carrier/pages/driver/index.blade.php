@extends('carrier.layouts.master')
@section('css')
<style type="text/css">
  thead{
    border-top: hidden;
  }
</style>
@endsection
@section('content')
@section('content')
<br>
<br>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
												<a class="btn btn-primary" href="{{route('carrier.driver.create')}}" style="border-radius: 11px;font-weight: bold;float: right"><span><i class="fas fa-plus"></i>Add Driver</span><a>
		</div>
	</div>
	  @if ($message = Session::get('success'))
		<div class="alert alert-success alert-block" id="message_id">
    		<button type="button" class="close" data-dismiss="alert">×</button>    
    		<strong>{{ $message }}</strong>
		</div>
	@endif
	<br>
	<div class="row">
		<div class="col-xl-12">
			<div class="card" style="border-radius: 18px;">
				<div class="card-body">
					<div class="row">
						<div class="col-md-4 offset-md-10">
							
						</div>
					</div>
					<div class="table-responsive">
					<table class="table ">
            <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Dead Head</th>
              <th>Plates No</th>
              <th>Permissions</th>
              <th>Action</th>
            </tr>
          </thead>                    
						 <tbody>
						 	@if($user->count() > 0)
						 	@foreach($user as $key=>$value)
						     <tr>
						        <td scope="row"  class="user_logo">
                      <img src="{{ asset('images/carrier/'.$value->profile) }}" class="driver_logo">
						        </td>
							    <td class="driver_name"><b>{{$value->name}}</b><br>
							    	<i class="fas fa-star rate"> </i>  3.5<br>
							    	<img src="{{asset('carrier/images/map.png')}}" class="map">  Athina 112 57,Greece
							    </td>
							    <td class="deadhead">
							    	<b class="mile"></b>
							    	@if($value->truck_type==0)
							    	<p style="color: #000000">N/A</p>
							    	@else
							    	<p>Truck with Trailer</p>
							    	@endif
							    </td>
							    <td class="truck">Truck Plates 12345 <br>Trailer Plates 12345
							    </td>
							    <td  class="permission">
							    	<img src="{{ URL::asset('shipper/images/new_key.svg')}}" class="img-fluid">Driver Permission 
							    	<p class="rate">Rates On, Booking On</p>
							    </td>
							    <td>
							    	<!-- <a class="btn btn-primary btn-xl" id="edit_driver" href='{{ url("carrier/driver/edit",$value->id) }}'>EDIT</a> -->
							    	<a class="btn btn-primary" href='{{ url("carrier/driver/assing",$value->id) }}'>EDIT</a>
							    	<a class="btn btn-danger deleteDriver" href='javascript:;' data-id="{{$value->id}}" >DELETE</a>
							    </td>
						    </tr>
						    @endforeach
						    @else
              				<tr>
                				<td>No Drivers Available</td>
              				</tr>
            				@endif
						</tbody>           
					</table>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click','.deleteDriver',function(){
      var driver_id = $(this).attr('data-id');
      $.ajax({
      	type:'get',
      	url:"{{url('carrier/driver/fetchdriver')}}",
      	data:{
      		'driver_id':driver_id
      	},
      	dataType:'json',
      	success:function(response){
      		
      		var truck_name=response.registration_no;
      		if(truck_name){
      			Notiflix.Confirm.Show(
      'Confirm',
      ''+truck_name+' vehicle has already assigned, Are you sure that you want to delete this Driver?',
      'Yes',
      'No',
      function() {
          Notiflix.Loading.Standard();
          $.ajax({
              url: "{{url('carrier/driver/delete')}}",
              type: 'get',
              dataType: "JSON",
              data: {
                  "driver_id": driver_id,
              },
              success: function(returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Driver deleted successfully');
            
                   window.setTimeout(function(){location.reload()},3000)
              }
          });
      	});
      		}else{
      				Notiflix.Confirm.Show(
      	'Confirm',
      ' Are you sure that you want to delete this Driver?',
      'Yes',
      'No',
      function() {
          Notiflix.Loading.Standard();
          $.ajax({
              url: "{{url('carrier/driver/delete')}}",
              type: 'get',
              dataType: "JSON",
              data: {
                  "driver_id": driver_id,
              },
              success: function(returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Driver deleted successfully');
            
                   window.setTimeout(function(){location.reload()},3000)
                 

              }
          });
      	});
      		}
      	}
      })
      
  
    });
    setTimeout(function(){
        $("#message_id").remove();
    }, 3000 );
});
</script>
@endsection