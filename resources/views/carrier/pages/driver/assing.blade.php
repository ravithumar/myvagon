@extends('carrier.layouts.master')
@section('content')
<style type="text/css">
  .material-switch > input[type="checkbox"] {
    display: none;
}

.material-switch > label {
  cursor: pointer;
  height: 0px;
  position: relative;
  top: 2px;
  width: 40px;
}

.material-switch > label::before {
  background: rgb(0, 0, 0);
  box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
  border-radius: 8px;
  border: 2px solid;
  content: '';
  height: 16px;
  margin-top: -8px;
  position: absolute;
  opacity: 0.3;
  transition: all 0.4s ease-in-out;
  width: 40px;

}

.material-switch > label::after {
  background: rgb(255, 255, 255);
    border: 2px solid #9D55E1;

  border-radius: 16px;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
  content: '';
  height: 24px;
  left: -4px;
  margin-top: -8px;
  position: absolute;
  top: -4px;
  transition: all 0.3s ease-in-out;
  width: 24px;
}

.material-switch > input[type="checkbox"]:checked + label::before {
  background: inherit;
  opacity: 0.5;
}

.material-switch > input[type="checkbox"]:checked + label::after {
  background: inherit;
  left: 20px;
}
[type=file] {
    position: absolute;
    filter: alpha(opacity=0);
    opacity: 0;
}
input,
[type=file] {
  border: 1px solid #CCC;
  border-radius: 3px;
  padding: 10px;
  width: 150px;
  margin: 0;
  left: 0;
  position: relative;
}
[type=file] {
  text-align: center;

  top: 0.5em;
  /* Decorative */
  background: #333;
  color: #fff;
  border: none;
  cursor: pointer;
}
</style>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
          
        </div>
        <h4 class="page-title"></h4>
      </div>
      
    </div>
  </div>
  @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block" id="message_id">
        <button type="button" class="close" data-dismiss="alert">×</button>    
        <strong>{{ $message }}</strong>
    </div>
  @endif
  <div class="row">
    <div class="col-xl-12">
      <div class="card">
<div class="row">
            <div class="col-md-4 offset-md-12">
              <div class="pt-3 pb-4">
                <h2 style="margin-left: 20px">Update Driver</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2 offset-md-12">
              <div class="pt-3 pb-4">
                <b style="color:#000000;margin-left: 20px">Personal Details</b>
                <br>
              </div>
            </div>
            <div class="col-md-6">
              <div class="pt-3 pb-4">
                
                <form class="driver_form" method="post" action="{{url('carrier/driver/update')}}" enctype="multipart/form-data">
                  @csrf
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">First Name</label>
                        <input type="text" name="first_name" class="form-control first_name" id="first_name" placeholder="First Name" required data-parsley-required-message="Please enter first name." data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in First Name." value="{{ $user->first_name }}" data-parsley-trigger="keyup" maxlength="40" >
                        @error('first_name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                      <input type="hidden" name="id" value="{{ $user->id }}">
                      <input type="hidden" name="user_id" value="{{ $driver_vehicle->user_id }}">
                      <div class="form-group col-md-6">
                        <label for="inputPassword4">Last Name</label>
                        <input type="text" name="last_name" class="form-control last_name" id="last_name" placeholder="Last Name" required data-parsley-required-message="Please enter last name." data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in Last Name." value="{{ $user->last_name }}" data-parsley-trigger="keyup" maxlength="40">
                      </div>
                        @error('last_name')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                    </div>
                    <div class="form-group">
                      <label for="inputAddress">Work Email</label>
                      <input type="email" name="email" id="email" class="form-control email" placeholder="Work Email" required data-parsley-required-message="Please enter work email." value="{{ $user->email }}" data-parsley-type-message="Please enter valid email address" data-parsley-trigger="keyup" maxlength="60" >
                      @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                     <div class="row">
                      <div class="col-md-2">
                        <div class="form-group">
                          
                                        <label>Phone Number</label>
                                          <select   style="padding: 2px !important;" name="country"  placeholder="Select Country" class="form-control" parsley-trigger="change" required value="{{ old('country') }}" id="country">
                                        @if(isset($countries))
                                              @foreach($countries as $country)
                                                <option value="{{ $country->id }}" selected>+ {{ $country->phonecode }}</option>
                                              @endforeach
                                      @endif
                                    </select>
                                          @error('country')
                                            <div class="error">{{ $message }}</div>
                                          @enderror
                                      </div>
                                    </div>
                        
                    <div class="col-md-10">
                    <div class="form-group">
                      
                      <input type="call" name="phone" class="form-control phone_number" id="phone_number" placeholder="Phone Number" required data-parsley-required-message="Please enter phone number." value="{{ $user->phone }}" maxlength="15"  data-parsley-trigger="keyup" data-parsley-maxlength-message="Only 15 number allowed." style="margin-top: 30px" onkeypress="return IsNumeric(event);">
                    </div>
                  </div>
                </div>
                    <!-- <div class="form-group">
                      <label for="inputAddress2">Phone Number</label>
                      <input type="number" name="phone" class="form-control phone_number" id="phone_number" placeholder="Phone Number" required data-parsley-required-message="Please enter phone number." value="{{ $user->phone }}" maxlength="15"  data-parsley-trigger="keyup" data-parsley-maxlength-message="Only 15 number allowed.">
                    </div> -->

                    
                    <!-- @php
                      $images_array =$driver_vehicle->images;
                      $images = implode(",",$images_array);
                  @endphp -->

                    <label>Profile Image</label>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                         <input type="file" name="profile_image"class="profile_image" id="profile_image" placeholder="Add profile picture" data-parsley-fileextension='jpeg,png,jpg,JPEG,PNG,JPG' data-parsley-trigger="keyup" data-parsley-max-file-size="50" data-parsley-errors-container="#profile_image_error_message"/>
                          <label for="identity_proof_document" style="float: left"><img src="{{ asset('images/carrier/'.$user->profile) }}" width="80"></label>
                           <input type="hidden" name="profile_image" value="{{$user->profile}}">
                            <div id="profile_image_error_message" style="margin-top: 50px"></div>
                            @error('profile_image')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="OpenImgProfile" style="margin-left:70px;">
                      </div>
                    </div>
                  </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                        <label>Identity Proof Document</label>
                         <input type="file" name="id_proof"class="identity_proof_document" id="identity_proof_document" placeholder="Add profile picture" data-parsley-fileextension='jpeg,png,jpg,JPEG,PNG,JPG' data-parsley-max-file-size="50" data-parsley-errors-container="#id_proof_error_message"/>
                          <label for="identity_proof_document" style="float: left"><img src="{{ asset('images/carrier/'.$driver_vehicle->id_proof) }}" width="80"></label>
                           <input type="hidden" name="id_proof" value="{{ $driver_vehicle->id_proof }}">
                            <div id="id_proof_error_message" style="margin-top: 50px"></div>
                            @error('id_proof')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="OpenImgId" style="margin-left:70px;margin-top: 40px">
                      </div>
                    </div>
                  </div>
                    <label for="licence">License</label>
                    <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                         <input type="file" name="license" class="license" id="license" placeholder="Add profile picture" data-parsley-fileextension='jpeg,png,jpg,JPG,JPEG,PNG' data-parsley-max-file-size="50" data-parsley-errors-container="#license_error_message">
                          <label for="license" style="float: left"><img src="{{ asset('images/carrier/'.$driver_vehicle->license) }}" width="80"></label>
                           <input type="hidden" name="license" value="{{ $driver_vehicle->license }}">
                           <div id="license_error_message"></div>
                           @error('license')
                            <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                           
                          {{-- <p>(Upload vehicle paperwork & <br> EU Registration document)</p> --}}
                      </div>
                    </div>
                    <div class="col-md-6"> 
                      <div class="form-group">
                        <img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="OpenImgLicense" style="margin-left:70px">
                      </div>
                    </div>
                  </div>
                    <p>(Upload vehicle paperwork & <br> EU Registration document)</p>
                   
                
              </div>
            </div>
          </div>
        
        <div class="card-body">
          <div style="margin-left: 400px">
           @if($driver_assing->is_assing=="0")
          <div class="row" >
            <a href='{{url("carrier/driver/vehiclelist",$user->id)}}' class="btn btn-primary assing_vehicle" style="border-radius: 250px;" ><i class="fas fa-plus"></i>
                    <span class="text ml-1"><b>Assign Vehicle</b></span>
                </a>
              </div>
          @else
          <span style="font-size: 20px">Truck Plate<b> {{$driver_assing->registration_no}}</b></span>
          <span style="font-size: 20px">Trailer Plate <b>{{$driver_assing->trailer_registration_no}}</b></span>
          <!-- <a href='{{url("carrier/driver/assingeditvehicle",$driver_assing->id)}}' class="btn btn-success" style="margin-left: 50px">Edit</a> -->
          <a href='{{url("carrier/driver/assing/edit/vehicle",$driver_assing->id)}}' class="btn btn-success">Edit</a>
           <!-- <a href='{{url("carrier/driver/assingeditvehicle",$driver_assing->id)}}' class="btn btn-success" style="margin-left: 50px">Edit</a> -->
          <a href="javascript:;" class="btn btn-danger deleteData" data-id="{{$driver_assing->dispatcher_vehicle_id}}" driver-id="{{$driver_assing->dispatcher_vehicle_id}}" id="deleteData">Delete</a>
          @endif
              </div>
              <div class="row" style="margin-top: 40px;margin-left: 70px">
                  <div class="col-3">
                    <p style="color:#1F1F41;font-weight: bold">All Notifications</p>
                  </div>
                  <div class="col-8">
                    <div class="md-form">
                        <div class="material-switch">
                          <input id="switch-default" name="switch-default" type="checkbox">
                          <label for="switch-default" class="default-color"></label>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="row" style="margin-left: 70px">
                  <div class="col-3">
                    <p style="color:#1F1F41;font-weight: bold">Messages</p>
                  </div>
                  <div class="col-8">
                    <div class="md-form">
                        <div class="material-switch">
                          <input id="switch-message" name="switch-message" type="checkbox">
                          <label for="switch-message" class="primary-color"></label>
                        </div>
                    </div>
                  </div>
              </div>

              <p style="margin-left: 82px;font-size: 12px;font-weight: bold;">Shipment Notifications</p>
              <div class="row" style="margin-left: 70px">
                  <div class="col-3">
                    <p style="color:#1F1F41;font-weight: bold">Bidding Allowed</p>
                  </div>
                  <div class="col-8">
                    <div class="md-form">
                        <div class="material-switch">
                          <input id="switch-bidingallowed" name="switch-bidingallowed" type="checkbox">
                          <label for="switch-bidingallowed" class="default-color"></label>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="row" style="margin-left: 70px">
                  <div class="col-3">
                    <p style="color:#1F1F41;font-weight: bold">Post availability</p>
                  </div>
                  <div class="col-8">
                    <div class="md-form">
                        <div class="material-switch">
                          <input id="switch-postavailability" name="switch-postavailability" type="checkbox">
                          <label for="switch-postavailability" class="default-color"></label>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="row" style="margin-left: 70px">
                  <div class="col-3">
                    <p style="color:#1F1F41;font-weight: bold">Search Load</p>
                  </div>
                  <div class="col-8">
                    <div class="md-form">
                        <div class="material-switch">
                          <input id="switch-searchload" name="switch-searchload" type="checkbox">
                          <label for="switch-searchload" class="default-color"></label>
                        </div>
                    </div>
                  </div>
              </div>
                <div class="row" style="margin-left: 70px">
                  <div class="col-3">
                    <p style="color:#1F1F41;font-weight: bold">View Price</p>
                  </div>
                  <div class="col-8">
                    <div class="md-form">
                        <div class="material-switch">
                          <input id="switch-viewprice" name="switch-viewprice" type="checkbox">
                          <label for="switch-viewprice" class="default-color"></label>
                        </div>
                    </div>
                  </div>
              </div>
                <div class="row" style="margin-left: 70px">
                  <div class="col-3">
                    <p style="color:#1F1F41;font-weight: bold">Book Load</p>
                  </div>
                  <div class="col-8">
                    <div class="md-form">
                        <div class="material-switch">
                          <input id="switch-bookload" name="switch-bookload" type="checkbox">
                          <label for="switch-bookload" class="default-color"></label>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="form-row">
                    <div class="form-group col-md-3" style="margin-left: 250px">
                      <button type="submit" class="btn btn-lg btn btn-primary btn-block">Update Driver</button>
                    </div>
                   </div>
              </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   function IsNumeric(event) {
      event = (event) ? event : window.event;
      var charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
  }
  $(document).ready(function(){
    window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
            var tagslistarr = requirement.split(',');
            var fileExtension = value.split('.').pop();
            var arr=[];
            $.each(tagslistarr,function(i,val){
               arr.push(val);
            });
            if(jQuery.inArray(fileExtension, arr)!='-1') {
              console.log("is in array");
              return true;
            } else {
              console.log("is NOT in array");
              return false;
            }
        }, 32)
        .addMessage('en', 'fileextension', 'Accept only this extension jpeg,png,jpg');
                       window.Parsley.addValidator('maxFileSize', {
  validateString: function(_value, maxSize, parsleyInstance) {
    if (!window.FormData) {
      alert('You are making all developpers in the world cringe. Upgrade your browser!');
      return true;
    }
    var files = parsleyInstance.$element[0].files;
    return files.length != 1  || files[0].size <= maxSize * 51200;
  },
  requirementType: 'integer',
  messages: {
    en: 'Maximum file size should be  %smb',
    fr: "Ce fichier est plus grand que %s Kb."
  }
});
    $('.driver_form').parsley();

    $(document).on('click','.deleteData',function(){
      
      var product = $(this).attr('data-id');
      var driver_id=$('.deleteData').attr('driver-id');
      Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to delete this assigned vehicle?',
      'Yes',
      'No',
      function() {
          Notiflix.Loading.Standard();
          $.ajax({
              url: "{{url('carrier/driver/assingdeletevehicle')}}",
              type: 'get',
              dataType: "JSON",
              data: {
                  "id": product,
                  'driver_id':driver_id
              },
              success: function(returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Assigned vehicle deleted successfully');
                  window.location = '{{ route('carrier.driver') }}'
                   window.setTimeout(function(){location.reload()},3000)
                  // $("#edit_model").modal("hide");
                  // location.reload();

              }
          });
      });
    });
     setTimeout(function(){
        $("#message_id").remove();
    }, 3000 );
  });
  $("[type=file]").on("change", function(){
  // Name of file and placeholder
  var file = this.files[0].name;
  var dflt = $(this).attr("placeholder");
  if($(this).val()!=""){
    $(this).next().text(file);
  } else {
    $(this).next().text(dflt);
  }
});
  $('#OpenImgId').click(function(){ 
      $('#identity_proof_document').trigger('click'); 
  });
$('#OpenImgLicense').click(function(){ 
      $('#license').trigger('click'); 
  });
  $('#OpenImgProfile').click(function(){ 
      $('#profile_image').trigger('click'); 
  });
</script>
@endsection
