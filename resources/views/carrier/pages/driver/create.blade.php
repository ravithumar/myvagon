@extends('carrier.layouts.master')
@section('content')
<style type="text/css">
	
[type=file] {
    position: absolute;
    filter: alpha(opacity=0);
    opacity: 0;
}
input,
[type=file] {
  border: 1px solid #CCC;
  border-radius: 3px;
  padding: 10px;
  width: 150px;
  margin: 0;
  left: 0;
  position: relative;
}
[type=file] {
  text-align: center;

  top: 0.5em;
  /* Decorative */
  background: #333;
  color: #fff;
  border: none;
  cursor: pointer;
}


</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					
				</div>
				<h4 class="page-title"></h4>
			</div>
			
		</div>
	</div>
	  <!-- @include('admin.include.flash-message') -->
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-4 offset-md-12">
							<div class="pt-3 pb-4">
								<h2>Add Driver</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 offset-md-12">
							<div class="pt-3 pb-4">
								<b style="color: #000000;">Personal Details</b>
								<br>
							</div>
						</div>
						<div class="col-md-6">
							<div class="pt-3 pb-4">
								
								<form class="driver_form" method="post" action="{{route('carrier.driver.store')}}" enctype="multipart/form-data">
									@csrf
									  <div class="form-row">
									    <div class="form-group col-md-6">
									      <label for="inputEmail4">First Name</label>
									      <input type="text" name="first_name" class="form-control first_name" id="first_name" placeholder="First Name" required data-parsley-required-message="Please enter first name." value="{{old('first_name')}}" required data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in first Name." data-parsley-trigger="keyup" data-parsley-maxlength="40" data-parsley-maxlength-message="Only 40 characters allowed." minlength="2" maxlength="70">
									      @error('first_name')
									      <div class="alert alert-danger">{{ $message }}</div>
									      @enderror
									    </div>
									    <div class="form-group col-md-6">
									      <label for="inputPassword4">Last Name</label>
									      <input type="text" name="last_name" class="form-control last_name" id="last_name" placeholder="Last Name" required data-parsley-required-message="Please enter last name." value="{{old('last_name')}}" required data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in last Name." data-parsley-trigger="keyup" data-parsley-maxlength="40" data-parsley-maxlength-message="Only 40 characters allowed." minlength="2" maxlength="70">
									    </div>
									    	@error('last_name')
									      		<div class="alert alert-danger">{{ $message }}</div>
									      	@enderror
									  </div>
									  <div class="form-group">
									    <label for="inputAddress">Work Email</label>
									    <input type="email" name="email" id="email" class="form-control email" placeholder="Work Email" required data-parsley-required-message="Please enter email." value="{{old('email')}}" data-parsley-trigger="keyup" data-parsley-maxlength="60" data-parsley-maxlength-message="Only 60 characters allowed." data-parsley-type-message="Please enter valid email" maxlength="60" >
									    @error('email')
									      		<div class="alert alert-danger">{{ $message }}</div>
									    @enderror
									  </div>
									   <div class="row">
									  	<div class="col-md-2">
									  		<div class="form-group">
                                        <label>Phone Number</label>
                                          <select   style="padding: 2px !important; " name="country"  placeholder="Select Country" class="form-control" parsley-trigger="change" required value="{{ old('country') }}" id="country" >
                                    		@if(isset($countries))
                                              @foreach($countries as $country)
                                                <option value="{{ $country->id }}" selected>+ {{ $country->phonecode }}</option>
                                              @endforeach
                                      @endif
                                    </select>
                                          @error('country')
                                            <div class="error">{{ $message }}</div>
                                          @enderror
                                      </div>
                                    </div>
									  		
									  <div class="col-md-10">
									 <div class="form-group">
									    
									    <input type="call" name="phone" class="form-control phone_number" id="phone_number" placeholder="Phone Number" required data-parsley-required-message="Please enter phone number." value="{{old('phone')}}" minlength="2" maxlength="15" data-parsley-maxlength="15" data-parsley-trigger="keyup" data-parsley-maxlength-message="Only 15 number allowed." style="margin-top:30px" onkeypress="return IsNumeric(event);">
									  </div>
									</div>
								</div>
									  <div class="row">
									  	<div class="col-md-6">
									  <div class="form-group">
									  	<label for="profile_image">Profile Image</label>
									  		 <input type="file" name="profile_image"class="profile_image" id="profile_image" placeholder="Add profile picture" required data-parsley-fileextension='jpeg,png,jpg,JPEG,PNG,JPG' data-parsley-trigger="keyup" data-parsley-errors-container="#profile_image_error_message" data-parsley-max-file-size="50" data-parsley-required-message="Please upload profile image." />
									  		  <div id="profile_image_error_message" ></div>
									  	</div>
									  </div>
									  	<div class="col-md-6">
									  	<div class="form-group">
									  	 <div class="image-upload">
									  		<img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="OpenImgProfile" style="margin-left:70px">
									  	</div>
									  </div>
									  </div>
									</div> 
									  <div class="row">
									  	<div class="col-md-6">
									  <div class="form-group">
									  	<label for="identity_proof_document">Identity Proof Document</label>
									  		 <input type="file" name="id_proof"class="identity_proof_document" id="identity_proof_document" placeholder="Add profile picture" required data-parsley-fileextension='jpeg,png,jpg,JPEG,PNG,JPG' data-parsley-trigger="keyup" data-parsley-errors-container="#id_proof_error_message" data-parsley-max-file-size="50" data-parsley-required-message="Please upload identity proof document." />
									  		  <div id="id_proof_error_message"></div>
									  	</div>
									  </div>
									  	<div class="col-md-6">
									  	<div class="form-group">
									  	 <div class="image-upload">
									  		<img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="OpenImgId" style="margin-left:70px">
									  	</div>
									  </div>
									  </div>
									</div>
									  <div class="row">
									  	<div class="col-md-6">
									  	<div class="form-group">
									  		<label for="license">License</label>
									  		 <input type="file" name="license" class="license" id="license" placeholder="Add profile picture"  
									  		 required data-parsley-fileextension='jpeg,png,jpg,JPEG,PNG,JPG' data-parsley-trigger="keyup" data-parsley-max-file-size="50" data-parsley-errors-container="#license_error_message" data-parsley-required-message="Please upload license."/>
									  		  
									  		  <div id="license_error_message"></div>
									  		  
									  	</div>
									  </div>
									  <div class="col-md-6">
									  	<div class="form-group">
									  	<div class="image-upload">
									  		<img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="OpenImgLicense" style="margin-left:70px">
									  	</div>
									  </div>
									</div>
									  </div>
									   <p>(Upload vehicle paperwork & <br> EU Registration document)</p>
									 <div class="form-row">
									 	<div class="form-group col-md-6">
									 		<button type="submit" class="btn btn-lg btn btn-primary btn-block">Add Driver</button>
									 	</div>
									 </div>
									  
								</form>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
        		var tagslistarr = requirement.split(',');
            var fileExtension = value.split('.').pop();
						var arr=[];
						$.each(tagslistarr,function(i,val){
   						 arr.push(val);
						});
            if(jQuery.inArray(fileExtension, arr)!='-1') {
              console.log("is in array");
              return true;
            } else {
              console.log("is NOT in array");
              return false;
            }
        }, 32)
        .addMessage('en', 'fileextension', 'Accept only this extension jpeg,png,jpg,JPG ');
                  window.Parsley.addValidator('maxFileSize', {
  validateString: function(_value, maxSize, parsleyInstance) {
    if (!window.FormData) {
      alert('You are making all developpers in the world cringe. Upgrade your browser!');
      return true;
    }
    var files = parsleyInstance.$element[0].files;
    return files.length != 1  || files[0].size <= maxSize * 51200;
  },
  requirementType: 'integer',
  messages: {
    en: 'Maximum file size should be  %smb',
    fr: "Ce fichier est plus grand que %s Kb."
  }
});

        
		$('.driver_form').parsley();
		
	});
	$("[type=file]").on("change", function(){
  // Name of file and placeholder
  var file = this.files[0].name;
  var dflt = $(this).attr("placeholder");
  if($(this).val()!=""){
    $(this).next().text(file);
  } else {
    $(this).next().text(dflt);
  }
});
	$('#OpenImgId').click(function(){ 
			$('#identity_proof_document').trigger('click'); 
	});
$('#OpenImgLicense').click(function(){ 
			$('#license').trigger('click'); 
	});
	$('#OpenImgProfile').click(function(){ 
			$('#profile_image').trigger('click'); 
	});
	 function IsNumeric(event) {
      event = (event) ? event : window.event;
      var charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
  }
</script>
@endsection
