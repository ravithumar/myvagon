@extends('carrier.layouts.master')
@section('content')
<div class="container-fluid">
<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<div class="">
					</div>
				</div>
				<h4 class="page-title">User Detail</h4>
			</div>
		</div>
	</div>
	<div class="card" style="width: 600px">
		<div class="card-body">
			<p style="font-size: 20px">User name:<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$user->name}}</span></p>
			<p style="font-size: 20px">User Email :<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$user->email}} </span></p>
			<p style="font-size: 20px">User Phone :<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$user->phone}} </span></p>
	
		</div>
	</div>
</div>
@endsection