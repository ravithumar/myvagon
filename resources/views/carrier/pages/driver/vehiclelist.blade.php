@extends('carrier.layouts.master')
@section('css')
<style type="text/css">
	thead{
		border-top: hidden;
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<div class="">
						<div>
							<!-- <a class="btn btn-primary text-reset" style="border-radius: 11px" href="{{route('carrier.vehicle.create')}}" ><i class="fas fa-plus" style="color: white"></i><b style="color: white">Add Vehicle</b></a> -->

						</div>
					</div>
				</div>
				<h4 class="page-title"></h4>
			</div>
		</div>
	</div>
	<br>
	<br>
	@include('admin.include.flash-message')
	<div class="card" style="border-radius: 18px;">
		<div class="card-body" >
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>Truck Type</th>
							<th>Cargo Load Capactity</th>
							<th>Overall Truck Weight</th>
							<th>Truck Brand</th>
			
							<th>Plates No</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@if($vehicle->count()> 0)
						@foreach($vehicle as $key=>$value)
						<tr>
							<td style="border-color: 000000"><img src="{{ asset('images/type/' . $value->truck->icon) }}" width="120" height="90"><br><b style="color: #000000">{{$value->truck->name}}</b></td>
							<td style="padding-top: 40px;color: #000000"><b>{{$value->cargo_load_capacity}} {{$value->cargo_load_capacity_unit}}</b></td>
							<td style="padding-top: 40px;color: #000000"><b>{{$value->overall_truck_weight}} {{$value->overall_truck_weight_unit}}</b></td>
							<td style="padding-top: 40px;color: #000000"><b>{{$value->brand->name}}</b></td>
							<td style="padding-top: 35px;color: #000000"><b>Truck Plates {{$value->truck_plate}}</b> <br><b>Trailer Plates {{$value->trailer_plate}}</b></td>
							<!-- <td><a  class="btn btn-primary btn-xl" href="{{ url('carrier/driver/assingvehicle/' . $value->id . '/' . $user->id .'')}}" onclick="return confirm('Are you sure want to assign this vehicle ?')">ASSIGN</a></td> -->
														<td><a  class="btn btn-primary btn-xl assingVehicle" href="javascript:;" data-id="{{$value->id}}" user-id="{{$user->id}}">ASSIGN</a></td>
						</tr>
						</form>
						@endforeach
						@else
							<tr>
								<td>No Any Vehicle Found Here</td>
							</tr>
						@endif
					</tbody>
				</table>
				<div class="pagination pull-right">
				{!! $vehicle->links() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		 $(document).on('click','.assingVehicle',function(){
      
      
      var vehicle_id=$(this).attr('data-id');
      var user_id=$(this).attr('user-id');
      
  Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to assign this vehicle?',
      'Yes',
      'No',
      function() {
          Notiflix.Loading.Standard();
          $.ajax({
              url: "{{url('carrier/driver/assingvehicle/')}}",
              type: 'get',
              dataType: "JSON",
              data: {
                  "vehicle_id": vehicle_id,
                  'user_id':user_id,
              },
              success: function(returnData) {
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Assigned vehicle deleted successfully');
                  // tr.remove();
                         window.location = '{{ route('carrier.driver') }}'
                   window.setTimeout(function(){location.reload()},3000)
                  // $("#edit_model").modal("hide");
                  // location.reload();

              }
          });
      });
    });
	})
</script>
@endsection