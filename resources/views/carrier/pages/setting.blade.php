@extends('carrier.layouts.master')
@section('content')
<style type="text/css">
    .material-switch > input[type="checkbox"] {
    display: none;
}

.material-switch > label {
  cursor: pointer;
  height: 0px;
  position: relative;
  top: 2px;
  width: 40px;
}

.material-switch > label::before {
  background: rgb(0, 0, 0);
  box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
  border-radius: 8px;
  border: 2px solid;
  content: '';
  height: 16px;
  margin-top: -8px;
  position: absolute;
  opacity: 0.3;
  transition: all 0.4s ease-in-out;
  width: 40px;

}

.material-switch > label::after {
  background: rgb(255, 255, 255);
    border: 2px solid #9D55E1;

  border-radius: 16px;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
  content: '';
  height: 24px;
  left: -4px;
  margin-top: -8px;
  position: absolute;
  top: -4px;
  transition: all 0.3s ease-in-out;
  width: 24px;
}

.material-switch > input[type="checkbox"]:checked + label::before {
  background: inherit;
  opacity: 0.5;
}

.material-switch > input[type="checkbox"]:checked + label::after {
  background: inherit;
  left: 20px;
}

</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    
                </div>
                <h4 class="page-title" style="color: #1F1F41">{{__('Settings')}}</h4>
            </div>
            
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
                   
                    <div class="row" style="margin-left: 20px">
                        <div class="col-md-4">
                            <b style="color: #1F1F41">{{__('Request MyVagon Admin to allow you pay via Credit/Debit card')}}</b>
                         </div>
                         <div class="col-md-2">   
                        <a href="{{url('carrier/vehicle/assing')}}" class="btn btn-primary" style="border-radius: 25px">
                                <span class="text ml-1 "><b>{{__('Send Request')}}</b></span>
                        </a>
                        </div>
                    </div>
                    <hr style="color:#D8D8D8;border-top: 1px solid #D8D8D8;margin-left: 25px">
                    <div class="row" style="margin-top: 40px;margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('All notifications')}}</p>
                             <hr style="width: 440px;border-top: 1px solid #D8D8D8;">
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-default" name="switch-default" type="checkbox">
                                    <label for="switch-default" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('Messages')}}</p>
                            <hr style="width: 440px;border-top: 1px solid #D8D8D8;">
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-message" name="switch-message" type="checkbox">
                                    <label for="switch-message" class="primary-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p style="margin-left: 82px;font-size: 12px;font-weight: bold;">{{__('Shipment Notifications')}}</p>
                    <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('Bidding received')}}</p>
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-bidingreceived" name="switch-received" type="checkbox">
                                    <label for="switch-bidingreceived" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('Shipment accepted by carrier or by freelance driver')}}</p>
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-shipmentaccepted" name="switch-shipmentaccepted" type="checkbox">
                                    <label for="switch-shipmentaccepted" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('Driver en route to pickup location')}}</p>
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-pickuplocation" name="switch-pickuplocation" type="checkbox">
                                    <label for="switch-pickuplocation" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('Driver Loading')}}</p>
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-driverloading" name="switch-driverloading" type="checkbox">
                                    <label for="switch-driverloading" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('Driver Unloading')}}</p>
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-driverloading" name="switch-driverloading" type="checkbox">
                                    <label for="switch-driverloading" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('POD Uploaded')}}</p>
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-poduploaded" name="switch-poduploaded" type="checkbox">
                                    <label for="switch-poduploaded" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('Payment Pending')}}</p>
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-paymentpending" name="switch-paymentpending" type="checkbox">
                                    <label for="switch-paymentpending" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 70px">
                        <div class="col-3">
                            <p style="color:#1F1F41;font-weight: 500">{{__('Rate Driver And Carrier')}}</p>
                        </div>
                        <div class="col-8">
                            <div class="md-form">
                                <div class="material-switch">
                                    <input id="switch-ratedriver" name="switch-ratedriver" type="checkbox">
                                    <label for="switch-ratedriver" class="default-color"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                
           
        </div>
    </div>
</div>
@endsection
