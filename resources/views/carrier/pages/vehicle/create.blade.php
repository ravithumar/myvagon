@extends('carrier.layouts.master')
@section('css')
<style type="text/css">
/*		.driver_form input[type=text],[type=email],[type=number],[type=select]{
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
  border: none;
  border-bottom:2px solid #dee2e6;
}*/
[type=file] {
    position: absolute;
    filter: alpha(opacity=0);
    opacity: 0;
}
input,
[type=file] {
  border: 1px solid #CCC;
  border-radius: 3px;
  padding: 10px;
  width: 150px;
  margin: 0;
  left: 0;
  position: relative;
}
[type=file] {
  text-align: center;
  top: 0.5em;
  background: #333;
  color: #fff;
  border: none;
  cursor: pointer;
}
select {
    border: none;
    outline: none;
    scroll-behavior: smooth;
}
#block_container
{
    text-align:center;
    width: 50px;
    height: 20px;
    background-color: black;
}
.one, .two
{
    display:inline;
}


</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					
				</div>
				<h4 class="page-title"></h4>
			</div>
			
		</div>
	</div>
	  <!-- @include('admin.include.flash-message') -->
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-4 offset-md-12">
							<div class="pt-3 pb-4">
								<h2>Add Vehicle</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 offset-md-12">
							<div class="pt-3 pb-4">
								<b style="color: #9B51E0;font-size: 25px">Vehicle Details</b>
								<br>
							</div>
						</div>
						<div class="col-md-6">
							<div class="pt-3 pb-4">
								
								<form class="driver_form" method="post" action="{{route('carrier.vehicle.store')}}" enctype="multipart/form-data">
									@csrf
										<div class="row">
											<div class="col-md-6">
									    		<div class="form-group">
									      			<label for="inputEmail4">Truck Type</label>
									      					<select class="form-control truck_type"  name="truck_type"required data-parsley-required-message="Please Select Truck Type.">
									      							<option value="">Select Truck Type</option>
									      							@foreach($truckDetails as $key=>$value)
									      							<option value="{{$value->id}}">{{$value->name}}</option>
									      							@endforeach
									      				</select>
									      				@error('truck_type')
									      					<div class="alert alert-danger">{{ $message }}</div>
									      				@enderror
									    		</div>
									    </div>
									    <div class="col-md-6">
									    	 <div class="sub-category-page form-group">
									    			<label for="inputEmail4">Truck Type Category</label>
									    					  <select class="form-control select2"  name="truck_sub_category_id[]" multiple id="truck_type_category" required data-parsley-required-message="Please Select Truck Type Category." data-parsley-errors-container="#truck_category_type_error">
									    						</select> 
									    						
									    						@error('truck_type_category')
									      						<div class="alert alert-danger">{{ $message }}</div>
									      					@enderror
									      					<div id="truck_category_type_error"></div>
									    		</div>
									    </div>
									  </div> 
									<div class="form-row">
									    <div class="form-group col-md-6">
									      	<label for="inputEmail4">Overall Truck Weight</label>
									      			<input type="number" name="overall_truck_weight" class="form-control overall_truck_weight" id="overall_truck_weight" placeholder="Overall Truck Weight" required data-parsley-required-message="Please enter Weight." value="{{old('overall_truck_weight')}}" data-parsley-trigger="keyup"  min="1"  data-parsley-min-message="Value should be greater than 0" maxlength="5" pattern="[0-9]+([\,|\.][0-9]+)?" data-parsley-pattern-message="Please enter valid value" step="0.01" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
									      			@error('overall_truck_weight')
									      				<div class="alert alert-danger">{{ $message }}</div>
									      			@enderror
									    </div>
									    <div class="form-group col-md-6">
									      	<label for="inputEmail4">Unit</label>
									      			<select class="form-control" name="overall_truck_weight_unit" required data-parsley-required-message="Please select weight of the unit." >
									      						<option value=" ">Unit</option>
									      						@foreach($truck_unit as $key=>$value)
									      						<option value="{{$value->name}}">{{$value->name}}</option>
									      						@endforeach
									      			</select>
									    </div>
												    	@error('cargo_load_capacity')
												      		<div class="alert alert-danger">{{ $message }}</div>
												      	@enderror
									  </div>
									  <div class="form-row">
									    <div class="form-group col-md-6">
									      <label for="inputEmail4">Cargo Load Capacity</label>
									      <input type="number" name="cargo_load_capacity" class="form-control overall_truck_weight" id="overall_truck_weight" placeholder="Cargo Load Capacity" required data-parsley-required-message="Please enter Cargo Load Capacity." value="{{old('overall_truck_weight')}}" data-parsley-trigger="keyup"   min="1" data-parsley-min-message="Value should be greater than 0" maxlength="5" pattern="[0-9]+([\,|\.][0-9]+)?" data-parsley-pattern-message="Please enter valid value" step="0.01" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
									      @error('overall_truck_weight')
									      <div class="alert alert-danger">{{ $message }}</div>
									      @enderror
									    </div>
									    <div class="form-group col-md-6">
									      <label for="inputPassword4">Unit</label>
									      <select class="form-control" name="cargo_load_capacity_unit" required data-parsley-required-message="Please Select weight of the unit." >
									      		<option value=" ">Unit</option>
									      		@foreach($truck_unit as $key=>$value)
									      		<option value="{{$value->name}}">{{$value->name}}</option>
									      		@endforeach
									      </select>
									    </div>
									    	@error('cargo_load_capacity')
									      		<div class="alert alert-danger">{{ $message }}</div>
									      	@enderror
									  </div>
									  <div class="form-row">
									    <div class="form-group col-md-12">
									      <label for="inputEmail4">Truck Brand</label>
									      <select class="form-control" name="truck_brand"  required data-parsley-required-message="Please enter Truck Brand.">
									      	<option value="">Please Select Truck Brand</option>
									      	@foreach($truck_brand as $key=>$value)
									      	<option value="{{$value->id}}">{{$value->name}}</option>
									      	@endforeach
									      </select>
									      @error('truck_brand')
									      <div class="alert alert-danger">{{ $message }}</div>
									      @enderror
									    </div>
									</div>
									<div class="form-row">
									    <div class="form-group col-md-6">
									      <label for="inputEmail4">Capacity (Pallets)</label>
									      <input type="number" name="value[]" class="form-control capacity_pallets" required data-parsley-required-message="Please enter Capacity pallets" placeholder="Capacity (Pallets)" maxlength="9" data-parsley-trigger="keyup"  min="1" data-parsley-min-message="Value should be greater than 0" pattern="[0-9]+([\,|\.][0-9]+)?" data-parsley-pattern-message="Please enter valid value" step="0.01" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
									      @error('capacity_pallets')
									      <div class="alert alert-danger">{{ $message }}</div>
									      @enderror
									    </div>
									    <div class="form-group col-md-5">
									      <label for="inputPassword4">Capacity (Package)</label>
									       <select class="form-control package" required data-parsley-required-message="Please enter Capacity Package." name="package_type_id[]" id="package_type_id">
									      	@foreach($package_type as $key=>$value)
									      	<option value="{{$value->id}}" data-id="{{$value->name}}">{{$value->name}}</option>
									      	@endforeach
									      </select>
									      @error('package_type_id')
									      		<div class="alert alert-danger">{{ $message }}</div>
									      	@enderror
									    </div>
									    
									    <div class="form-group col-md-1">
									    	<button class="btn btn-primary add_pellets" type="button" style="margin-top: 25px;border-radius:50%;margin-left: 13px" ><i class="fas fa-plus"></i></button>
									    </div>
									  </div>
									  <div class="add_columns">
									    	
									    </div>
									   <div class="form-row">
									    <div class="form-group col-md-6">
									      <label for="inputEmail4">Truck Plate</label>
									      <input type="text" name="truck_plate" class="form-control truck_plate" id="truck_plate" placeholder="Truck Plate" required data-parsley-required-message="Please enter Truck Plate." value="{{old('truck_plate')}}" maxlength="40">
									      @error('truck_plate')
									      <div class="alert alert-danger">{{ $message }}</div>
									      @enderror
									    </div>
									    <div class="form-group col-md-6">
									      <label for="inputPassword4">Trailer Plate</label>
									      <input type="text" name="trailer_plate" class="form-control trailer_plate" id="trailer_plate" placeholder="Trailer Plate" required data-parsley-required-message="Please enter Trailer Plate." value="{{old('trailer_plate')}}" maxlength="40">
									    </div>
									    	@error('trailer_plate')
									      		<div class="alert alert-danger">{{ $message }}</div>
									      	@enderror
									  </div>
									  <div class="row">
									  	<div class="form-group col-md-12">
									 		 <div class="label-wrapper">
                                      			<label>
												<input type="checkbox"  class="largerCheckbox" checked required data-parsley-required-message="Please Select Hydraulic Door." name="hydraulic_door" value="1"><span></span></label>
												<label for="Semi-trailer Truck" style="font-size: 20px;color: #000000">Hydraulic Door</label>
                                    		</div>
                                		</div>
                                	  </div>
                                	  
    									<div class="row">
    										<div class="col-md-12">
                                	  			<p style="font-weight: bold">Vehicle Run On</p>  
                                	  			<input type="radio" name="vehicle_run_on" value="diesel" required>Diesel <br>
                                	  			<input type="radio" name="vehicle_run_on" value="electric">Electric <br>
                                	  			<input type="radio" name="vehicle_run_on" value="hydrogen">Hydrogen
                                	  		</div>
                                	  </div>
                                	  
									  <div class="form-row">

									  	<div class="form-group col-md-6">
									  		<label for="identity_proof_document">Vehicle Photo</label>
									  		 <input type="file" name="vehicle_photo"class="identity_proof_document" id="vehicle_photo_image" placeholder="Add profile picture" required required data-parsley-fileextension='jpeg,png,jpg,JPG,PNG,JPEG' data-parsley-required-message="Please upload vehicle photo."data-parsley-errors-container="#vehicle_photo_image_error" data-parsley-max-file-size="50"/>
									  		 <div id="vehicle_photo_image_error"></div>	
									  		 
									  	</div>
									  	
									  	<div class="form-group col-md-6 image-upload">
									  		<img src="{{asset('shipper/images/upload.jpg')}}" width="80" id="vehicle_photo" style="margin-left:75px">
									  	</div>
									  </div>
									 
									 <div class="form-row">
									 	<div class="form-group col-md-6">
									 		<button type="submit" class="btn btn-lg btn btn-primary btn-block add_vahicle">Add Vehicle</button>
									 	</div>
									 </div>  
								</form>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
        		var tagslistarr = requirement.split(',');
            var fileExtension = value.split('.').pop();
						var arr=[];
						$.each(tagslistarr,function(i,val){
   						 arr.push(val);
						});
            if(jQuery.inArray(fileExtension, arr)!='-1') {
              console.log("is in array");
              return true;
            } else {
              console.log("is NOT in array");
              return false;
            }
        }, 32)
        .addMessage('en', 'fileextension', 'Accept only this extension jpeg,png,jpg ');
             window.Parsley.addValidator('maxFileSize', {
  validateString: function(_value, maxSize, parsleyInstance) {
    if (!window.FormData) {
      alert('You are making all developpers in the world cringe. Upgrade your browser!');
      return true;
    }
    var files = parsleyInstance.$element[0].files;
    return files.length != 1  || files[0].size <= maxSize * 51200;
  },
  requirementType: 'integer',
  messages: {
    en: 'Maximum file size should be  %smb',
    fr: "Ce fichier est plus grand que %s Kb."
  }
});
		$('.driver_form').parsley();
		$('.select2').select2({
			    placeholder: "Please Select Sub Category",
		});
		$('body').on('change','.truck_type',function(){
			var truckType=$(this).val();
			$.ajax({
				type:'get',
				url:"{{route('carrier.vehicle.getcategory')}}",
				data:{
					truckType:truckType
				},
				dataType:'json',
				success:function(response){
					var categoryData=response.truck_category;
					$('#truck_type_category').html('<option value="">Select Truck Category</option>'); 
					$.each(categoryData,function(key,value){
						$("#truck_type_category").append('<option value="'+value.id+'">'+value.name+'</option>');
					});			
				}
			});
		});
		$('.add_vahicle').on('click',function(){
  			var truck_type_category=$('#truck_type_category').val();
				if(truck_type_category=='')
				{
					$('.error').show();
				}
  			});
		$('#truck_type_category').on('change',function(){
  			var truck_type_category=$('#truck_type_category').val();
				if(truck_type_category!='')
				{
					$('#truck_category_type_error').hide();
				}
  			});
		$('.add_pellets').on('click',function(){
			var package_type=$('.package').html();
			var capacityValues='<div class="row">'+
								  '<div class="form-group col-md-6">'
										+'<input type="number" name="value[]" class="form-control capacity_pallets" required data-parsley-required-message="Please enter Capacity pallets" placeholder="Capacity (Pallets)" maxlength="9" data-parsley-trigger="keyup"  min="1" data-parsley-min-message="Value should be greater than 0" pattern="[0-9]+([\,|\.][0-9]+)?" data-parsley-pattern-message="Please enter valid value" step="0.01" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >'
								+'</div>'+
							     '<div class="form-group col-md-5">'+
									'<select class="form-control" name="package_type_id[]" placeholder="Capacity Package" required data-parsley-required-message="Please enter Capacity Package.">'+'<option>'+package_type+'</option>'+'</select>'+
									'</div>'+
								'<div class ="col-md-1">'+'<button class="btn btn-success removeColumn"  type="button" style="margin-top: 1px;border-radius:50%"><i class="fas fa-minus"></i></button>'+'</div>'+
								'</div>';
								$('.add_columns').append(capacityValues);
								$('select option').filter(function() {
        							return !this.value || $.trim(this.value).length == 0 || $.trim(this.text).length == 0;
    								}).remove();
		});
		$('body').on('click','.removeColumn',function(){
			$(this).closest('.row').remove();
			
		})
	});

	$("[type=file]").on("change", function(){
  // Name of file and placeholder
  var file = this.files[0].name;
  var dflt = $(this).attr("placeholder");
  if($(this).val()!=""){
    $(this).next().text(file);
  } else {
    $(this).next().text(dflt);
  }
});
		$('#vehicle_photo').click(function(){ 
			$('#vehicle_photo_image').trigger('click'); 
	});
</script>
@endsection