@extends('carrier.layouts.master')
@section('content')
<div class="container-fluid">
<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<div class="">
					</div>
				</div>
				<h4 class="page-title">Truck Detail</h4>
			</div>
		</div>
	</div>
	<div class="card" style="width: 600px">
		<div class="card-body">
			<p style="font-size: 20px">Truck name:<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$vehicle->truck->name}}</span></p>
			<p style="font-size: 20px">Cargo Load Capacity :<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$vehicle->cargo_load_capacity}} {{$vehicle->cargo_load_capacity_unit}}</span></p>
			<p style="font-size: 20px">Overall Truck  Weight :<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$vehicle->overall_truck_weight}} {{$vehicle->overall_truck_weight_unit}}</span></p>
			<p style="font-size: 20px">Truck Brand:<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$vehicle->brand->name}}</span></p>
			<p style="font-size: 20px">Truck Plate:<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$vehicle->truck_plate}}</span></p>
			<p style="font-size: 20px">Truck Plate:<span style="font-size: 20px;font-weight: 5px;color: #000000">{{$vehicle->trailer_plate}}</span></p>
		</div>
	</div>
</div>
@endsection