@extends('carrier.layouts.master')
@section('css')
<style type="text/css">
	thead{
		border-top: hidden;
	}
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					<div class="">
						<div>
							<a class="btn btn-primary text-reset" style="border-radius: 11px" href="{{route('carrier.vehicle.create')}}" ><i class="fas fa-plus" style="color: white"></i><b style="color: white">Add Vehicle</b></a>
						</div>
					</div>
				</div>
					<h4 class="page-title" ></h4><br><br><br><br>
				@if ($message = Session::get('success'))
							<div class="alert alert-success alert-block" id="message_id">
					    		<button type="button" class="close" data-dismiss="alert">×</button>    
					    		<strong>{{ $message }}</strong>
							</div>
						@endif

			</div>
		</div>
	</div>
	<br>
	<br>
	<div class="card" style="border-radius: 18px;">
		<div class="card-body" >
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>Truck Type</th>
							<th>Cargo Load Capactity</th>
							<th>Overall Truck Weight</th>
							<th>Truck Brand</th>
							
							<th>Plates No</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
            @if($vehicle->count()>0)
						@foreach($vehicle as $key=>$value)
						<tr>
							<td style="border-color: 000000"><img src="{{ asset('images/type/' . $value->truck->icon) }}" width="120" height="90"><br><b style="color: #000000">{{$value->truck->name}}</b></td>
						
							<td style="padding-top: 40px;color: #000000"><b>{{$value->cargo_load_capacity}} {{$value->cargo_load_capacity_unit}}</b></td>
							<td style="padding-top: 40px;color: #000000"><b>{{$value->overall_truck_weight}} {{$value->overall_truck_weight_unit}}</b></td>
							<td style="padding-top: 40px;color: #000000"><b>{{$value->brand->name}}</b></td>
							<td style="padding-top: 35px;color: #000000"><b>Truck Plates {{$value->truck_plate}}</b> <br><b>Trailer Plates {{$value->trailer_plate}}</b></td>
							<td>
								<a class="btn btn-primary btn-xl"  href='{{ url("carrier/vehicle/edit",$value->id) }}'>EDIT</a>
								<a class="btn btn-danger btn-xl deleteVehicle"  href='javascript:;' data-id="{{$value->id}}" data-title="{{$value->truck->name}}">DELETE</a>
							</td>
						</tr>
						@endforeach
           @else
              <tr>
                <td>No Vehicles Available</td>
              </tr>
            @endif
					</tbody>
				</table>
				<div class="pagination pull-right">
				{!! $vehicle->links() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
@section('script')
<script type="text/javascript">
	$("document").ready(function(){
		$(document).on('click','.deleteVehicle',function(){
      	var vehicle_id = $(this).attr('data-id');

      	var vehicle_name=$(this).attr('data-title'); 
      	$.ajax({
      		type:"get",
      		url:"{{url('carrier/vehicle/getvehicle')}}",
      		dataType:"JSON",
      		data:{
      			"vehicle_id":vehicle_id
      		},
      		success:function(response){
      			console.log(response);
      			var id=response.id;
      			if(id){
      					Notiflix.Confirm.Show(
      			'Confirm',
      			''+vehicle_name+' has already assinged , Are you sure that you want to delete this assigned vehicle?',
      			'Yes',
     			'No',
     			 function() {
          		Notiflix.Loading.Standard();
          		$.ajax({
              	url: "{{url('carrier/vehicle/delete')}}",
              	type: 'get',
              	dataType: "JSON",
              	data: {
                  "vehicle_id": vehicle_id,
              	},
              	success: function(returnData) {
              	console.log(returnData);
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Vehicle deleted successfully');
                   window.setTimeout(function(){location.reload()},3000)

              }
          });
      	});
      }else{
      					Notiflix.Confirm.Show(
      			'Confirm',
      			'Are you sure that you want to delete this vehicle?',
      			'Yes',
     			'No',
     			 function() {
          		Notiflix.Loading.Standard();
          		$.ajax({
              	url: "{{url('carrier/vehicle/delete')}}",
              	type: 'get',
              	dataType: "JSON",
              	data: {
                  "vehicle_id": vehicle_id,
              	},
              	success: function(returnData) {
              	console.log(returnData);
                  Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Vehicle deleted successfully');
                   window.setTimeout(function(){location.reload()},3000)

              }
          });
      	});
      			}
      		
      		}
      	})
  		
    });
    setTimeout(function(){
        $("#message_id").remove();
    }, 3000 );
});
</script>
@endsection