@extends('carrier.layouts.master')
@section('content')
<style type="text/css">
    .box-over:hover {
        border : 1px solid #9b51e0 !important;
        box-shadow: 0 0px 7px 0 rgb(155 81 224/50%);
    }
    /*.box-over{
        border : 1px solid #9b51e0 !important;
        box-shadow: 0 0px 7px 0 rgb(155 81 224/50%);
    }*/
    .overflow-height .active{
        border: 1px solid #9b51e0 !important;
        box-shadow: 0 0px 7px 0 rgb(155 81 224 / 50%);
    }
</style>
<div class="container-fluid">
    @include('admin.include.flash-message')
    <div class="row align-items-center">
        <div class="col-md-6">
            <nav aria-label="breadcrumb" >
                
            </nav>
        </div>
    </div>
    <div class="row" style="margin-top: 40px">
        <div class="col-md-2" >
            <label style="color: #3E3E5B;display: block; width: 100px;">Pick up Date</label>
            <input type="text" name="" class="form-control" style="border-radius: 12px;border: 1px solid #D8BBF2;" >
        </div>
        <div class="col-md-2">
            <label style="color: #3E3E5B">Pick Up Location</label>
            <input type="text" name="" class="form-control" style="border-radius: 12px;border: 1px solid #D8BBF2;">
        </div>
        <div class="col-md-2">
            <label style="color: #3E3E5B">Delivery Location</label>
            <input type="text" name="" class="form-control" style="border-radius: 12px;border: 1px solid #D8BBF2;">
        </div>
        <div class="col-md-1">
            <label style="color: #3E3E5B">Price</label>
            <input type="text" name="" class="form-control" placeholder="min" style="border-radius: 12px;border: 1px solid #D8BBF2;">
        </div>
        <div class="col-md-1" style="margin-top: 8px">
            <label></label>
            <input type="text" name="" class="form-control" placeholder="max" style="border-radius: 12px;border: 1px solid #D8BBF2;">
        </div>
        <div class="col-md-1">
            <label style="color: #3E3E5B">Weight</label>
            <input type="text" name="" class="form-control" placeholder="min" style="border-radius: 12px;border: 1px solid #D8BBF2;">
        </div>
        <div class="col-md-1" style="margin-top: 8px">
            <label></label>
            <input type="text" name="" class="form-control" placeholder="max" style="border-radius: 12px;border: 1px solid #D8BBF2;">
        </div>
        <div class="col-md-1" style="margin-top: 30px;margin-left: 70px;">
            <button type="button" class="btn btn-primary" style="width: 140px;border-radius: 35px;font-weight: bold">Search</button>
        </div>
    </div>
    @if(isset($bookings[0]) && !empty($bookings[0]))
    <div class="row">
        <div class="col-md-12 col-xl-12">
           <!--  <h3 class="text-muted">{{ __('Recent') }} <span class="text-dark ml-2">{{ date('d-m-Y') }}</span></h3> -->
            <div class="overflow-height" style="padding-right: 18px;">
                @include('carrier.pages.dashboard_list')
            </div>
        </div>
        <!-- <div class="col-md-12 col-xl-6">
            <h3 class="text-muted">{{ __('Map View') }}</span></h3>
            <?php $last_key1 = 0; if(isset($bookings[0]['booking_location']) && !empty($bookings[0]['booking_location'])) { $last_key1 = array_key_last($bookings[0]['booking_location']->toArray()); }?>
            <div class="map position-relative mt-3">
                <div id="map-canvas" style="height: 700px;width: 100%;border-radius: 10px;"></div>
            </div>
        </div> -->
    </div>
    @else
        <div class="row">
            <div class="col-md-12 text-center" style="margin-top: 8%;border-radius: 12px;border: 1px solid #D8BBF2;">
                <a href="{{ url('shipper/shipment/create') }}">
                    <i class="fas fa-shipping-fast fa-10x"></i>
                    <h3 class="text-muted">{{ __('Create Shipment') }}</span></h3>
                </a>
            </div>
        </div>
    @endif
</div>
@endsection
@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script type="text/javascript">
    var map;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    var locations = <?=json_encode($location_aray)?>;
    function initialize() {
        directionsDisplay = new google.maps.DirectionsRenderer();
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
            zoom: 10,
            center: new google.maps.LatLng(locations[0][1], locations[0][2]),
        });
        directionsDisplay.setMap(map);
        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        var request = {
            travelMode: google.maps.TravelMode.DRIVING
        };
        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            });
    
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
    
            if (i == 0) request.origin = marker.getPosition();
            else if (i == locations.length - 1) request.destination = marker.getPosition();
            else {
                if (!request.waypoints) request.waypoints = [];
                request.waypoints.push({
                    location: marker.getPosition(),
                    stopover: true
                });
            }
    
        }
        directionsService.route(request, function (result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(result);
            }
        });
    
        directionsDisplay = new google.maps.DirectionsRenderer({
          polylineOptions: {
            strokeColor: "#9b51e0",
          }
        });
    
     directionsDisplay.setMap(map);
    
     directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else
            alert ('failed to get directions');
     });
    }
    google.maps.event.addDomListener(window, "load", initialize);

    $(document).ready(function(){
        $(document).on('click','.booking_row',function(){
            $('.box-over').removeClass('active');
            $(this).addClass('active');
            var booking_id = $(this).attr('data-id');
            var booking_status = $(this).attr('data-status');
            var data_from_company_name = $(this).attr('data-from-company-name');
            var data_from_address_short = $(this).attr('data-from-address-short');
            var data_to_address_short = $(this).attr('data-to-address-short');
            var data_from_address = $(this).attr('data-from-address');
            var data_to_address = $(this).attr('data-to-address');
            var data_from_lat = $(this).attr('data-from-lat');
            var data_from_lng = $(this).attr('data-from-lng');
            var data_to_lat = $(this).attr('data-to-lat');
            var data_to_lng = $(this).attr('data-to-lng');
            var data_from_position = $(this).attr('data-from-position');
            var data_to_position = $(this).attr('data-to-position');
            $.ajax({
                url: "{{ url('shipper/manage-shipment/load-map') }}",
                type: 'post',
                dataType: "JSON",
                data: {
                    "id": booking_id,
                    "_method": 'POST',
                    "_token": $('name[csrf-token]').attr('content'),
                },

                success: function(data) {
                // console.log(data)
                locations = data;
                initialize();
                var map_status = '';
                    // open_map(data);
                }
            });
            // locations = [
            //     [data_from_address, data_from_lat, data_from_lng, data_from_position],
            //     [data_to_address, data_to_lat, data_to_lng, data_to_position],
            // ];
            
            // if(booking_status == 'create'){
            //     $('.map_status').removeClass('bg-red');
            //     $('.map_status').removeClass('bg-green');
            //     $('.map_status').removeClass('bg-yellow');
            //     $('.map_status').removeClass('bg-blue');
            //     $('.map_status').addClass('bg-info');
            //     $('.map_status').text('Created');
            // }
            // if(booking_status == 'pending'){
            //     $('.map_status').addClass('bg-red');
            //     $('.map_status').removeClass('bg-info');
            //     $('.map_status').removeClass('bg-green');
            //     $('.map_status').removeClass('bg-yellow');
            //     $('.map_status').removeClass('bg-blue');
            //     $('.map_status').text('Pending');
            // }
            // if(booking_status == 'scheduled'){
            //     // map_status = '<a href="javascript:void(0)" class="bg-yellow text-white py-1 px-3 d-inline-block text-uppercase font-weight-bold recent-pending-top-raduis font-15">Scheduled</a>';
            //     $('.map_status').removeClass('bg-info');
            //     $('.map_status').removeClass('bg-green');
            //     $('.map_status').removeClass('bg-red');
            //     $('.map_status').removeClass('bg-blue');
            //     $('.map_status').addClass('bg-yellow');
            //     $('.map_status').text('Scheduled');
            // }
            // if(booking_status == 'in-process'){
            //     $('.map_status').removeClass('bg-info');
            //     $('.map_status').removeClass('bg-green');
            //     $('.map_status').removeClass('bg-red');
            //     $('.map_status').removeClass('bg-yellow');
            //     $('.map_status').addClass('bg-blue');
            //     $('.map_status').text('In-process');
            // }
            // if(booking_status == 'completed'){
            //     $('.map_status').removeClass('bg-info');
            //     $('.map_status').removeClass('bg-blue');
            //     $('.map_status').removeClass('bg-red');
            //     $('.map_status').removeClass('bg-yellow');
            //     $('.map_status').addClass('bg-green');
            //     $('.map_status').text('Completed');
            // }
            // $('.map_from_company_name').text(data_from_company_name);
            // $('.map_from_location').text(data_from_address_short);
            // $('.map_to_location').text(data_to_address_short);
        })
    })
    </script>

    <script type="text/javascript">
    var page = 1;
    $('.overflow-height').scroll(function() {
        if($('.overflow-height').scrollTop() >= $('.overflow-height').height()) {
            page++;
            loadMoreData(page);
        }
    });


    function loadMoreData(page){
      $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {
                    $('.ajax-load').show();
                }
            })
            .done(function(data)
            {
                if(data.html == " "){
                    // $('.ajax-load').html("No more records found");
                    return;
                }
                // $('.ajax-load').hide();
                $(".overflow-height").append(data.html);
            })
            .fail(function(jqXHR, ajaxOptions, thrownError)
            {
                  alert('server not responding...');
            });
    }
</script>
@endsection