@include('shipper.layouts.header')

            <div class="sign-page">
                <div class="container-fluid p-0">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="img-wrapper">
                           <img src="{{ URL::asset('shipper/images/car.png')}}" class="img-fluid">
                        </div>
                        
                      </div>
                      <div class="col-md-5">
                         <div class="row">
                            
                            <div class="col-md-10 p-0">
                            @include('admin.include.flash-message')
                               <div class="sign-in-side carrier-sign-view">
                                  <ul class="nav nav-tabs justify-content-center mb-3 mb-md-4 mb-lg-5">
                                        <li class="nav-item">
                                            <a href="#dispatcher" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                Dispatcher
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#driver" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                Driver
                                            </a>
                                        </li>
                                 </ul>
                                 <div class="tab-content">
                                  <div class="tab-pane show active" id="dispatcher">                                    
                                      <form action="{{ route('carrier.store') }}" method="POST">
                                      @CSRF
                                         <div class="form-group ">
                                            <div class="icon">
                                              <img src="{{ URL::asset('shipper/images/user.svg')}}" class="img-fluid float-center">
                                            </div>
                                             <input type="text" id="name" parsley-trigger="change" data-parsley-required-message="Please Enter Name" required value="{{ old('name') }}" name="name" class="form-control border" placeholder="Full Name" minlength="2" maxlength="70" data-parsley-required-message="Please enter Name." required data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in Name." data-parsley-trigger="keyup"> 
                                             @error('name')
                                                <div class="error">{{ $message }}</div>
                                              @enderror
                                          </div>
                                          <div class="form-group">
                                              <div class="icon">
                                                <img src="{{ URL::asset('shipper/images/call-icon.svg')}}" class="img-fluid">
                                              </div>

                                              <input type="call" data-parsley-validation-threshold="1" data-parsley-maxlength="10"  data-parsley-type="digits" onkeypress="return IsNumeric(event);" name="phone" maxlength="10" parsley-trigger="change" required="" value="{{old('phone')}}" id="phone" class="form-control phone" placeholder="Mobile Number" data-parsley-required-message="Please enter Phone number." autocomplete="off">
                                              <div id="phoneError" class="text-danger"></div>
                                               <input type="hidden" name="phone_verify_register" id="phone_verify_register" value="@if(!empty(old('phone_verify_register'))) {{old('phone_verify_register')}} @else 0 @endif">
                                                      <div class="icon verified" id="verify">
                                                          <span class="img-fluid verify-img-phone text-green verify-green" @if(old('phone_verify_register') == 1)style="display: block; color:green" @else style="display: none;" @endif>Verified</span>
                                                              <a href="javascript:;"  id="verify" onclick="phone_verify()" class="verify btn-phone"  style="@if(old('phone_verify_register') == 1) display:none;@endif">@if(old('phone_verify_register') == 1) Verified @else Verify @endif</a>
                                                       </div>
                                          </div>

                                         <div class="form-group">
                                            <div class="icon">
                                                <img src="{{ URL::asset('shipper/images/work-mail.svg')}}" class="img-fluid">
                                            </div>
                                            <input type="email" name="email"  maxlength="60" parsley-trigger="change" required value="{{ old('email') }}" id="email" class="form-control" placeholder="Email"  data-parsley-required-message="Please enter Email." required="" data-parsley-type-message="Please enter a valid email" data-parsley-trigger="keyup">
                                              @error('email')
                                                <div class="error">{{ $message }}</div>
                                              @enderror
                                                <div id="emailerror" class="text-danger"></div>
                                                <input type="hidden" name="email_verify_register" id="email_verify_register" value="@if(!empty(old('email_verify_register'))) {{old('email_verify_register')}} @else 0 @endif">
                                                      <div class="icon verified" id="verify">
                                                          <span class="img-fluid verify-img text-green verify-green" @if(old('email_verify_register') == 1)style="display: block; color:green" @else style="display: none;" @endif>Verified</span>
                                                              <a href="javascript:;"  id="verify" onclick="EmailVerify()" class="verify verify-img2"  style="@if(old('email_verify_register') == 1) display:none;@endif">@if(old('email_verify_register') == 1) Verified @else Verify @endif</a>
                                                       </div>
                                         </div>
                                          <div class="form-group">
                                            <div class="icon">
                                                <a onclick="show()"><img src="{{ URL::asset('shipper/images/key.svg')}}" class="img-fluid" id="EYE"></a>
                                              </div>
                                             <input type="password"  maxlength="40" data-parsley-minlength="8" data-parsley-minlength-message="The password must contain at least 8 characters " name="password"  parsley-trigger="change" required value="{{ old('password') }}" id="password1" data-required="true"  class="form-control border" value="" placeholder="Password" data-parsley-required-message="Please enter Password." data-parsley-whitespace="squish" onkeyup="nospaces(this)" required="" data-parsley-trigger="keyup">
                                                 @error('password')
                                                    <div class="error">{{ $message }}</div>
                                                 @enderror                                      
                                              </div> 
                                             <div class="form-group">
                                            <div class="icon">
                                                <a onclick="show2()"><img src="{{ URL::asset('shipper/images/key.svg')}}" class="img-fluid" id="EYE2"></a>
                                              </div>
                                             <input type="password" maxlength="40" name="confirm_password" data-parsley-minlength="8" data-parsley-minlength-message="Password must contain at least 8 characters"   parsley-trigger="change" data-parsley-equalto="#password1" data-parsley-equalto-message="Password and Confirm Password must be same " data-required="true" required value="{{ old('confirm_password') }}" id="confirm_password" class="form-control border" value="" placeholder="Confirm password" data-parsley-required-message="Please confirm the password." onkeyup="nospaces(this)" required="" data-parsley-trigger="keyup">
                                              @error('confirm_password')
                                                <div class="error">{{ $message }}</div>
                                              @enderror              
                                          </div>
                                          
                                          <div class="form-group mt-5">
                                             <a  href="javascript:;" class="btn btn-default btn-lg btn-block carrier_registration">Sign In</button>
                                             <br>
                                             <button type="submit" href="javascript:;" class="btn black-btn">Join For Free</button>
                                          </div>                                          
                                      </form>
                                       <div class="download-app text-center">
                                            <div class="text">Download App to get started with Carrier!</div>
                                            <ul class="d-flex justify-content-center mt-4">
                                               <li>
                                                  <a href="#">
                                                    <span class="icon"><img src="{{ URL::asset('shipper/images/apple-store.svg')}}" class="img-fluid"></span>
                                                    <span class="text-wrapper"><em>Available on the </em> App Store</span>
                                                  </a>
                                               </li>
                                               <li>
                                                  <a href="#">
                                                    <span class="icon"><img src="{{ URL::asset('shipper/images/play-store.svg')}}" class="img-fluid"></span>
                                                    <span class="text-wrapper"><em>Available on the </em> Play Store</span>
                                                  </a>
                                               </li>
                                            </ul>
                                        </div>  
                                    </div>
                                    <div class="tab-pane" id="driver">
                                                                 
                                    </div> 
                                  </div>
                                                             
                               </div>
                               </div>
                            </div>
                             <div class="d-sm-none col-md-2 d-md-block"></div>
                         </div>
                    
                      </div>
                    </div>
                 </div>

            </div>

   @include('shipper.layouts.footer_script')

<!-- static opt -->
<input type="hidden" id="optcode" name="otpcode" value="456789">

<div class="modal fade" id="vefifyphone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Enter OTP Sent To Your Email Address</div>
                      <div class="verify-block">
                        <div class="container" style="display:none;" id="myAlert">
                            <div class="alert alert-success alert-dismissable" id="myAlert2" >
                              
                                Your Resend OTP Send Successfully.
                            </div>

                        </div>
                              <div class="otps d-flex mb-3">                             
                                  <form method="post" class="digit-group mt-md-3 mt-lg-4" data-group-name="digits" data-autosubmit="false" autocomplete="off" >
                                               
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-1" name="digit-1"/>
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-2" name="digit-2"  data-previous="digit-1" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-3" name="digit-3"  data-previous="digit-2" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-4" name="digit-4"  data-previous="digit-3" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-5" name="digit-5" data-previous="digit-4" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-6" name="digit-6" data-previous="digit-5" />
                                               <hr><div class="digit-error text-danger" id="digit-error"></div>
                              
                             </div>
                       <a href="#" class="resend" data-status="1" id="modalButton" onclick="showAlert();">Resend OTP</a>
                       <button type="button" onclick="verify_email_otp()" class="btn black-btn mt-4 mt-md-5">Verify</button>
                       </form>
                    </div>
              </div>
         </div>
    </div> 
</div>


<div class="modal fade" id="phone_otp_verify" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Enter OTP Sent To Your <div id="phone_fatch"></div></div>
                      <div class="verify-block">
                              <div class="otps d-flex mb-3">                             
                                  <form method="post" class="digit-group mt-md-3 mt-lg-4 verify-email" data-group-name="digits" data-autosubmit="false" autocomplete="off" >
                                               
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-1" name="digit-1"  />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-2" name="digit-2"  data-previous="phone-1" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-3" name="digit-3"  data-previous="phone-2" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);"  class="email-verify otp" maxlength="1" id="phone-4" name="digit-4"  data-previous="phone-3" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-5" name="digit-5"  data-previous="phone-4" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-6" name="digit-6" data-previous="phone-5" />
                                               <hr><div class="phone_error text-danger" id="phone_error"></div>
                              
                             </div>
                       <!-- <a href="#" onclick="getProgress()" class="resend">Resend OTP</a> -->
                       <button type="button" onclick="verify_phone_otp()" class="btn black-btn mt-4 mt-md-5">Verify</button>
                       </form>
                    </div>
              </div>
         </div>
    </div> 
</div>

<!-- New Script added Here 04-01-2022 -->
<script type="text/javascript">
   function IsNumeric(event) {
      event = (event) ? event : window.event;
      var charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
  }
  $('input.email-verify.otp').on('keyup', function() {
    if ($(this).val()) {
        $(this).next().focus();
    }
});
  $('input.phone').on('keyup', function() {
   var phone_value=$(this).val();
   if(phone_value==''){
    $('#phoneError').hide();
   }
});
$('.digit-group').find('input').each(function() {
  $(this).attr('maxlength', 1);
  $(this).on('keyup', function(e) {
    var parent = $($(this).parent());
    
    if(e.keyCode === 8 || e.keyCode === 37) {
      var prev = parent.find('input#' + $(this).data('previous'));
      
      if(prev.length) {
        $(prev).select();
      }
    } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
      var next = parent.find('input#' + $(this).data('next'));
      
      if(next.length) {
        $(next).select();
      } else {
        if(parent.data('autosubmit')) {
          parent.submit();
        }
      }
    }
  });
});

$(".verify-img2").hide();
     $("#email").keyup(function(){
    if($(this).val() == "") {
        $(".verify-img2").hide();
    } else {
        $(".verify-img2").show();
    }
});
   $(".btn-phone").hide();
    $("#phone").keyup(function(){
    if($(this).val() == "") {
        $(".btn-phone").hide();
    } else {
        $(".btn-phone").show();
    }
});
</script>
<script type="text/javascript">
  function showAlert(){
  if($("#myAlert").find("div#myAlert2").length==0){
    $("#myAlert").append("<div class='alert alert-success alert-dismissable' id='myAlert2' > Your Resend OTP Send Successfully.</div>");
  }
  $("#myAlert").css("display", "");
}
$('a.d-flex').not('.active').css('pointer-events', 'none');
</script>

   

