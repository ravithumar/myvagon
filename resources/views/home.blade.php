@include('shipper.layouts.header')

        <div class="home-page py-4">
              <div class="home-outer">
                <div class="container">
                    <div class="row align-items-center">
                       <div class="col-md-6 order-md-12">
                        
                            <div class="img-wrapper ml-auto">
                              <img src="images/home-page/shipping-car.png" class="img-fluid">
                            </div>                                               
                      </div>
                      <div class="col-md-6 order-md-1">
                         <div class="left-text-content position-relative">
                            <h1 class="font-76 font-weight-bold text-left mb-4"><span class="text-pink d-block">Logistics Marketplace</span>  from the Future</h1>
                            <p class="font-24 font-weight-normal">We connect carriers with shippers and make booking loads, managing fleet, and getting paid as easy as ever</p>
                            <a href="javascript:;" class="font-25 btn-black-cust text-uppercase color-dark-purple text-center d-inline-block font-weight-bold mt-4 w-100 p-2 p-lg-3 position-relative">Join For Free</a>
                         </div>  
                      </div>
                     
                    </div>
                    <div class="btn-block">
                      <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-7">
                           <div class="btns d-flex justify-content-between">
                             <a href="javascript:;" class="font-25 btn-black-cust text-uppercase color-dark-purple text-center d-inline-block font-weight-bold mt-2 mr-2 w-100 p-2 p-lg-3 position-relative">For Carrier</a>
                             <a href="javascript:;" class="btn-black-cust font-25 text-uppercase color-dark-purple text-center d-inline-block font-weight-bold mt-2 w-100 p-2 p-lg-3 position-relative">For Shipper</a>
                           </div>
                        </div>
                        <div class="col-md-2"></div>
                      </div>
                     </div>                    
                  </div>
              </div>
                
                <div class="collab-us my-3 my-sm-4 my-md-5 pt-3 pt-sm-4 pt-md-5">
                    <div class="container">
                     <div class="row">
                       <div class="col text-center">
                           <h2 class="font-weight-bold font-43"><span class="text-pink">Collaboration</span> unlocks new possibilities. <br>And good collaboration requires the right <span class="text-pink">tools.</span></h2>
                           <p class="font-22 mx-auto">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
                       </div>
                     </div>
                    </div>
                  </div>
                  <div class="company-view text-center">
                    <div class="container">
                      <div class="row">
                        <div class="col">
                           <div class="img-block">
                             <img src="images/home-page/responsive-view.png" class="img-fluid">
                           </div>
                           <div class="download-us text-center">                                           
                                <ul class="d-block d-sm-flex justify-content-center align-items-center mt-5 mb-3 p-0">
                                    <li class="m-2 mb-2 mb-md-0 list-unstyled">
                                        <a href="#" class="d-flex justify-content-center align-items-center p-2 bg-white">
                                             <span class="icon"><img src="images/home-page/desktop.svg" class="img-fluid"></span>
                                             <span class="text-wrapper text-left pl-3 font-24"><em class="d-block font-weight-normal font-12">Available as</em> WebApp</span>
                                        </a>
                                    </li>
                                    <li class="m-2 mb-2 list-unstyled">
                                       <a href="#" class="d-flex justify-content-center align-items-center p-2 bg-white">
                                              <span class="icon"><img src="images/apple-store.svg" class="img-fluid"></span>
                                              <span class="text-wrapper text-left pl-3 font-24"><em class="d-block font-weight-normal font-12">Available on the </em> Apple Store</span>
                                        </a>
                                    </li>
                                     <li class="m-2 mb-2 list-unstyled">
                                        <a href="#" class="d-flex justify-content-center align-items-center p-2 bg-white">
                                              <span class="icon"><img src="images/play-store.svg" class="img-fluid"></span>
                                              <span class="text-wrapper text-left pl-3 font-24"><em class="d-block font-weight-normal font-12">Available on the </em> Play Store</span>
                                        </a>
                                    </li>
                                </ul>
                           </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="try-free-block mt-3">
                     <div class="container">
                       <div class="row align-items-center">
                         <div class="col-md-6 col-lg-4">
                            <div class="block d-block d-xl-flex align-items-center mb-3 py-4 px-3 position-relative h-100">                                
                                  <span class="icon d-flex align-items-center justify-content-center rounded-circle"><img src="images/home-page/goods.svg" class="img-fluid"></span>
                                  <span class="text-wrapper pl-3">
                                    <span class="font-20 mb-2 d-block">Ship And Transport Goods Without Limitations, 24/7</span>
                                    <p class="font-14">No more tedious phone calls and emails to find and book loads.</p>
                                  </span>                                
                            </div>
                         </div>
                         <div class="col-md-6 col-lg-4">
                            <div class="block d-block d-xl-flex align-items-center mb-3 py-4 px-3 position-relative h-100">
                                  <span class="icon d-flex align-items-center justify-content-center rounded-circle"><img src="images/home-page/flash.svg" class="img-fluid"></span>
                                  <span class="text-wrapper pl-3">
                                    <span class="font-20 mb-2 d-block">Transparency And Efficiency</span>
                                    <p class="font-14">Bid and set quotes, manage and track your shipments and fleet in real time</p>
                                  </span>
                            </div>
                         </div>
                         <div class="col-md-6 col-lg-4">
                            <div class="block d-block d-xl-flex  align-items-center mb-3 py-4 px-3 position-relative h-100">
                                  <span class="icon d-flex align-items-center justify-content-center rounded-circle"><img src="images/home-page/cost-savings.svg" class="img-fluid"></span>
                                  <span class="text-wrapper pl-3">
                                    <span class="font-20 mb-2 d-block">Financial And Environmental Cost Savings</span>
                                    <p class="font-14">Avoid empty miles to reduce your costs and environmental footprint. optimize your transportation!</p>
                                  </span>
                            </div>
                         </div>
                       </div>
                       <div class="row">
                          <div class="col align-items-center text-center">
                             <a href="javascript:;" class="font-25 btn-black-cust text-uppercase color-dark-purple text-center d-inline-block font-weight-bold mt-4 w-100 p-2 p-lg-3 position-relative text-white">Try For Free</a>
                          </div>
                       </div>
                     </div>
                  </div>
                  <div class="divisions mt-5">
                     <div class="container-fluid p-0">
                       <div class="row no-gutters align-items-stretch">
                         <div class="col-md-6 bg-dark-purple ">
                           <div class="row py-5 pr-4 align-items-center">
                             <div class="col-lg-6 my-3 my-lg-5">
                                <div class="img-wrapper">
                                   <img src="images/home-page/desk-view.svg" class="img-fluid" alt="desk-view">
                                </div>  
                             </div>
                             <div class="col-lg-6 my-3 my-lg-5">
                                <div class="text-area px-2 px-lg-3">
                                  <h2 class="text-white font-weight-bold font-43">For Shippers</h2>
                                  <p class="font-20 font-weight-400">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore</p>
                                   <p class="font-20 font-weight-400">magna aliquyam erat, sed diam voluptua. At vero eos et accusam</p>
                                    <a href="javascript:;" class="learn-more font-24 text-pink text-center font-weight-bold mt-4 mx-auto d-inline-block bg-white px-3 py-2">Learn More ></a>
                                 </div>
                             </div>
                           </div>
                          
                         </div>
                         <div class="col-md-6  bg-light-grey">
                           <div class="row py-5 pl-5 align-items-center">
                             <div class="col-lg-6 order-lg-12 my-3 my-lg-5">
                                <div class="img-wrapper">
                                   <img src="images/home-page/carrier-view.svg" class="img-fluid" alt="carrier-view">
                                </div>  
                             </div>
                             <div class="col-lg-6 order-lg-1 my-3 my-lg-5">
                                <div class="text-area px-2 px-lg-3">
                                  <h2 class="color-dark-purple font-weight-bold font-43">For Carriers</h2>
                                  <p class="font-20 font-weight-400">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore</p>
                                   <p class="font-20 font-weight-400">magna aliquyam erat, sed diam voluptua. At vero eos et accusam</p>
                                    <a href="javascript:;" class="learn-more font-24 text-pink text-center font-weight-bold mt-4 mx-auto d-inline-block bg-white px-3 py-2">Learn More ></a>
                                 </div>
                             </div>
                           </div>
                         
                         </div>
                         
                       </div>
                     </div>
                  </div>
                  <div class="video-block text-center" style="background-image:url(images/home-page/video-bg.jpg)">
                     
                      <div class="text-wrapper position-relative">
                         <div class="big-text font-53 font-weight-bold mb-2 text-white">Minimize your empty miles and help protect the environment!</div>
                         <p class="m-0 text-white">It's simply a problem of empty miles: 40% of all trucks travel empty. We are literally shipping air.</p>
                         <p class="m-0 text-white">MYVAGON logistics marketplace provides  visibility on the supply and demand for shipment loads, and helps reduce your empty miles, costs and carbon footprint!</p>
                      </div>
                       <!-- YOUTUBE VIDEO -->
                      <div class="ytvideo" data-video="yAoLSRbwxL8" >                              
                          <span class="playbutton"></span>
                      </div>         
                      
                
                  </div>
                  <div class="mission-block py-4 position-relative mt-4">
                    <div class="container-fluid">
                      <div class="row align-items-center">
                        <div class="col-md-4 order-md-12">
                           <div class="img-wrapper">
                             <img src="images/transport/mission.jpg" alt="" class="img-fluid">
                           </div>
                        </div>
                        <div class="col-md-8 order-md-1">
                          <div class="row mt-3 mt-md-0">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                               <div class="left-text-content py-3">                                 
                                  <h2 class="font-43 font-weight-bold position-relative text-uppercase dark-purple mb-3 d-inline-block">mission</h2>
                                  <div class="text font-20 dark-purple mb-3">Our goal is to <span class="text-pink font-weight-600">empower the carriers</span> who bring to us the necessary goods of life, and are the backbone of our economy.</div>
                                  <div class="text font-20 dark-purple mb-3">Our goal is to <span class="text-pink font-weight-600">empower the producers</span> and shippers of the products that we use on our daily lives.</div>
                                 <p class="font-weight-bold dark-purple font-30">Our goal is to empower humanity.</p>
                               </div>
                            </div>
                          </div>
                         
                        </div>
                       
                      </div>
                    </div>
                  </div>
                  <div class="feedbacks pt-3 pt-md-5 mt-3 mt-md-5">
                     <div class="container">
                       <div class="row">
                         <div class="col-md-5 col-lg-4">
                           <h2 class="font-43 font-weight-bold position-relative">Some lovely <span class="text-pink">feedback</span> from our clients</h2>
                         </div>
                         <div class="col-md-7 col-lg-8"></div>
                       </div>

                     </div>
                  </div>
                  <div class="contact-us">

                    <div class="container">
                        <div class="row">
                         <div class="col-md-12">
                            <div class="feedback-slider owl-carousel mt-5">
                               <div class="item bg-white position-relative p-3 p-lg-4 mt-5  ml-4">
                                 <ul class="d-flex mb-3 p-0">
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                 </ul>
                                 <p class="font-22 font-weight-400">At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet dolore magna aliquyam erat, sed diam voluptua. At vero eos et a sanctus.</p>
                                 <div class="client-block d-flex align-items-center mt-3 mt-md-5">
                                      <div class="img-wrapper">
                                        <img src="images/home-page/john-wise.jpg" class="img-fluid rounded-circle">
                                      </div>
                                      <div class="text-wrapper pl-3">
                                        <span class="name d-block font-24 text-black">John Wise</span>
                                        <span class="post font-18 d-block">Manager, Subsied</span>
                                      </div>
                                 </div>
                               </div>
                               <div class="item bg-white position-relative p-3 p-lg-4 mt-5 ml-4">
                                 <ul class="d-flex mb-3 p-0">
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                 </ul>
                                 <p class="font-22 font-weight-400">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata. </p>
                                 <div class="client-block d-flex align-items-center mt-3 mt-md-5">
                                      <div class="img-wrapper">
                                        <img src="images/home-page/dustin-hardy.jpg" class="img-fluid rounded-circle">
                                      </div>
                                      <div class="text-wrapper pl-3">
                                        <span class="name d-block font-24 text-black">Dustin Hardy</span>
                                        <span class="post font-18 d-block">Manager, Subsied</span>
                                      </div>
                                 </div>
                               </div>
                               <div class="item bg-white position-relative p-3 p-lg-4 mt-5 ml-4">
                                 <ul class="d-flex mb-3 p-0">
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                 </ul>
                                 <p class="font-22 font-weight-400">Iinvidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata. sanctus est Lorem ipsum dolor sit amet</p>
                                 <div class="client-block d-flex align-items-center mt-3 mt-md-5">
                                      <div class="img-wrapper">
                                        <img src="images/home-page/earik-tran.jpg" class="img-fluid rounded-circle">
                                      </div>
                                      <div class="text-wrapper pl-3">
                                        <span class="name d-block font-24 text-black">Earik Tran</span>
                                        <span class="post font-18 d-block">Manager, Subsied</span>
                                      </div>
                                 </div>
                               </div>
                               <div class="item bg-white position-relative p-3 p-lg-4 mt-5 ml-4">
                                 <ul class="d-flex mb-3 p-0">
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                 </ul>
                                 <p class="font-22 font-weight-400">At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet dolore magna aliquyam erat, sed diam voluptua. At vero eos et a sanctus.</p>
                                 <div class="client-block d-flex align-items-center mt-3 mt-md-5">
                                      <div class="img-wrapper">
                                        <img src="images/home-page/john-wise.jpg" class="img-fluid rounded-circle">
                                      </div>
                                      <div class="text-wrapper pl-3">
                                        <span class="name d-block font-24 text-black">John Wise</span>
                                        <span class="post font-18 d-block">Manager, Subsied</span>
                                      </div>
                                 </div>
                               </div>
                            </div>
                         </div>
                       </div>
                      <div class="row text-center mt230">
                         <div class="d-sm-none col-md-2 d-md-block"></div>
                         <div class="col-md-8">
                          <h2 class="font-43 font-weight-bold mb-4 mb-sm-5">Need a Help? Don't Worry <span class="text-pink">Contact Us</span></h2>
                          <form>
                             <div class="row">
                               <div class="col-md-6">
                                 <div class="form-group mb-4">
                                   <input type="text" id="example-text" name="example-text" class="form-control font-14" placeholder="Name">
                                 </div>
                               </div>
                               <div class="col-md-6">
                                 <div class="form-group mb-4">
                                   <input type="email" id="example-email" name="example-email" class="form-control" placeholder="Email">
                                 </div>
                               </div>
                              </div>
                              <div class="row">
                                <div class="col">
                                  <div class="form-group mb-4">
                                     <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Message"></textarea>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col">
                                   <a href="javascript:;" class="font-25 btn-black-cust text-uppercase color-dark-purple text-center d-inline-block font-weight-bold mt-2 mt-sm-4 w-100 p-2 p-lg-3 position-relative text-white">Submit</a>
                                </div>
                              </div>
                          </form>
                          
                         </div>
                         <div class="d-sm-none col-md-2 d-md-block"></div>
                      </div>
                      <div class="row">
                        <div class="d-sm-none col-md-2 d-md-block"></div>
                        <div class="col-md-8">
                           <div class="newsletter bg-white py-3 px-3 px-sm-4">
                             <h3 class="font-25 mb-3">Subscribe to our Newsletter</h3>
                             
                               <div class="row">
                                 <div class="col-lg-8">
                                    <div class="email">
                                      <div class="form-group m-0">
                                        <input type="email" id="example-email" name="example-email" class="form-control font-25 p-2" placeholder="Email Address">
                                      </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-4">
                                   <a href="javascript:;" class="font-25 btn-black-cust text-uppercase color-dark-purple text-center d-inline-block font-weight-bold  w-100 p-2 p-lg-3 position-relative text-white">Subscribe</a>
                                 </div>
                               </div>
                                                                                        
                           </div>
                        </div>
                        <div class="d-sm-none col-md-2 d-md-block"></div>
                      </div>
                    </div>
                  </div>                  
                             
            </div>

@include('shipper.layouts.footer_script')

