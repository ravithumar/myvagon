@include('shipper.layouts.header')

<style>

</style>

<div class="registration-form pt-4 pb-5">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-sm-9">
            @include('admin.include.flash-message')
              <div class="container">
             

                <div class="row">
                  <div class="col-md-2 col-lg-2">
                    <div class="left-img">
                      <img src="{{ URL::asset('shipper/images/regi-form-01.svg')}}" class="img-fluid">
                    </div>
                  </div>
                 
                  <div class="col-md-8 col-lg-8">
                    <div class="step-info">
                    
                      <h2 class="title mt-0">Registration Form </h2>
                      <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in  laying out print, graphic or web designs</p>
                      <div id="basicwizard">
                        <div class="personal-details">
                          <ul class="d-flex">
                            <li class="">
                              <a href="#basictab1" data-toggle="tab" class="d-flex">
                                <div class="icon">
                                  <img src="{{ URL::asset('shipper/images/user-pink.svg')}}" class="img-fluid">
                                </div>
                                <div class="text-wrap">
                                  <h3>Personal Details</h3>
                                  <p>Please provide your details</p>
                                </div>
                              </a>
                            </li>
                            <li class="">
                              <a href="#basictab2" data-toggle="tab" class="d-flex">
                                <div class="icon">
                                  <img src="{{ URL::asset('shipper/images/bxs-buildings.svg')}}" class="img-fluid">
                                </div>
                                <div class="text-wrap">
                                  <h3>Company Details</h3>
                                  <p>Please provide your Company details</p>
                                </div>
                              </a>
                            </li>
                            <li class="">
                              <a href="#basictab3" data-toggle="tab" class="d-flex">
                                <div class="icon">
                                  <img src="{{ URL::asset('shipper/images/truck.svg')}}" class="img-fluid">
                                </div>
                                <div class="text-wrap">
                                  <h3>Shipping Details</h3>
                                  <p>Please provide your Shipping details</p>
                                </div>
                              </a>
                            </li>
                          </ul>
                        </div>
                        
                        <div class="tab-content b-0 mb-0">
                          <div class=""  id="basictab1" >
                            <div class="col-sm-12" @if($data == "step1") style="display:block" @else style="display:none"  @endif >
                              <form method="POST" action="{{ route('register.store') }}">
                              @CSRF
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/user.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="text" name="first_name"  maxlength="40" parsley-trigger="change" required value="{{ old('first_name') }}" id="first_name" class="form-control" placeholder="First Name">
                                      @error('first_name')
                                          <div class="error">{{ $message }}</div>
                                      @enderror
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/user.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="text" name="last_name"  maxlength="40" parsley-trigger="change" required value="{{ old('last_name') }}" id="last_name"  class="form-control" placeholder="Last Name">
                                      @error('last_name')
                                          <div class="error">{{ $message }}</div>
                                      @enderror
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/work-mail.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="email" name="email"  maxlength="60" parsley-trigger="change" required value="{{ old('email') }}" id="email" class="form-control" placeholder="Work Email">
                                      @error('email')
                                          <div class="error">{{ $message }}</div>
                                      @enderror
                                      <div id="emailerror" class="text-danger"></div>
                                      <input type="hidden" name="email_verify_register" id="email_verify_register" value="0">
                                      <div class="icon verified">
                                        <img src="{{ URL::asset('shipper/images/verified.svg')}}" class="img-fluid verify-img" style="display: none;">
                                        <!-- <a href="javascript:;"  id="verify" onclick="myFunction()" class="verify verify-img2" data-toggle="modal" data-target="#vefifyphone" style="right: -10px;">Verify</a> -->
                                        <a href="javascript:;"  id="verify" onclick="EmailVerify()" class="verify verify-img2"  style="right: -10px;">@if(old('email_verify_register') == 1) Verified @else Verify @endif</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/call-icon.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="call" data-parsley-validation-threshold="1" data-parsley-maxlength="8" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" onkeypress="return IsNumeric(event);" name="phone"   maxlength="8" data-parsley-type="digits" parsley-trigger="change" required value="{{ old('phone') }}" id="phone"  class="form-control" placeholder="Phone Number (Optional)">
                                      @error('phone')
                                          <div class="error">{{ $message }}</div>
                                      @enderror
                                      <div id="phoneError" class="text-danger"></div>
                                      <input type="hidden" name="phone_verify_register" id="phone_verify_register" value="0">
                                      <div class="icon verified">
                                        <img src="{{ URL::asset('shipper/images/verified.svg')}}" class="img-fluid verify-img-phone" style="display: none;">
                                        <a href="javascript:;"  class="verify btn-phone" onclick="phone_verify()">@if(old('verify-img-phone') == 1) Verified @else Verify @endif</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                       

                                        <img src="{{ URL::asset('shipper/images/password.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="password"  maxlength="40" data-parsley-minlength="8" data-parsley-minlength-message="The password must contain at least 8 character " name="password"  parsley-trigger="change" required value="{{ old('password') }}" id="password1" data-required="true"  class="form-control" value="" placeholder="New password">
                                      @error('password')
                                          <div class="error">{{ $message }}</div>
                                      @enderror

                                      <a class="icon pass" onclick="show()">
                                      <img src="{{ URL::asset('shipper/images/eye-slash-fill.svg')}}" class="img-fluid" id="EYE">   
                                      <!-- <img src="{{ URL::asset('shipper/images/eye-slash-fill.svg')}}" class="img-fluid"> -->
                                        <!-- <img src="{{ URL::asset('shipper/images/eye-fill.svg')}}" class="img-fluid"> -->
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/password.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="password" maxlength="40" name="confirm_password" data-parsley-minlength="8" data-parsley-minlength-message="The password must contain at least 8 character "   parsley-trigger="change" data-parsley-equalto="#password1" data-parsley-equalto-message="Password and Confirm Password must be same " data-required="true" required value="{{ old('confirm_password') }}" id="confirm_password" class="form-control" value="" placeholder="Confirm password">
                                      @error('confirm_password')
                                          <div class="error">{{ $message }}</div>
                                      @enderror

                                      <a onclick="show2()" class="icon pass">
                                        <img src="{{ URL::asset('shipper/images/eye-slash-fill.svg')}}" class="img-fluid" id="EYE2">
                                      </a>
                                    </div>
                                  </div>
                                </div>
                               
                                <ul class="wizard mb-0 row px-2 list-unstyled">
                                    <li class="col">
                                      <button type="submit" class="btn black-btn">Save and Continue</button>
                                    </li>
                                </ul>
                              
                              </form>


                            </div>

                          </div>
                          <div class="" id="basictab2">
                            <div class="col-sm-12" @if($data == "step2") style="display:block" @else style="display:none"  @endif>
                              <div class="company-form">
                              <form method="POST" data-parsley-validate action="{{ route('register.store.profile', $userid) }}">
                              @CSRF
                                  <div class="row">
                                    <div class="col">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                        <input type="text" maxlength="40" name="company_name"  parsley-trigger="change" required value="{{ old('company_name') }}" id="company_name" class="form-control" placeholder="Company Name">
                                        @error('company_name')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                      </div>
                                    </div>
                                  </div>

                              

                                  <div class="row">
                                    <div class="col-lg-6">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                      
                                        <select  style="padding: 2px !important;" placeholder="Select City" name="city" class="form-control select2" data-bind="volunteer-new-final-title-field" class="dropkick_select" data-parsley-required="true"  parsley-trigger="change" required  id="city">
                                        
                                        <option selected="selected" value="">Choose city..</option>
                                            @if(isset($cities))
                                              @foreach($cities as $city)
                                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                                              @endforeach
                                            @endif
                                         
                                         
                                         
                                        </select>
                                        @error('city')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                        
                                      </div>
                                    </div>

                                    <div class="col-lg-6">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        
                                        <select  style="padding: 2px !important;" name="state"  placeholder="Select State" class="form-control" parsley-trigger="change" required  id="state">
                                        <option value="">Choose state..</option>
                                        @if(isset($states))
                                            @foreach($states as $state)
                                              <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                        @error('state')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                        
                                      </div>
                                      
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        
                                        <select   style="padding: 2px !important;" name="country"  placeholder="Select Country" class="form-control" parsley-trigger="change" required value="{{ old('country') }}" id="country">
                                        <option value="">Choose country..</option>
                                        @if(isset($countries))
                                            @foreach($countries as $country)
                                              <option value="{{ $country->id }}">{{ $country->name }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                        @error('country')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                        
                                      </div>
                                    </div>

                                    <div class="col-lg-6">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                        <input type="text" data-parsley-maxlength="8" maxlength="8" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" id="simpleinput" class="form-control" placeholder="Postcode" name="postcode"  parsley-trigger="change" required value="{{ old('postcode') }}" id="postcode">
                                        @error('postcode')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                      </div>
                                      
                                    </div>
                                  </div>
                                  
                                  
                                  <div class="row">
                                    <div class="col">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        <input type="ssn-number" data-parsley-maxlength="11" maxlength="11" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" class="form-control"  name="ssn"  parsley-trigger="change" required value="{{ old('ssn') }}" id="ssn" class="form-control" placeholder="SSN Number">
                                        @error('ssn')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                      </div>
                                      
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        <input type="call"   data-parsley-maxlength="10" maxlength="8" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" name="company_phone"  parsley-trigger="change" required value="{{ old('company_phone') }}" id="company_phone" class="form-control" placeholder="Phone Number">
                                        @error('company_phone')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                      </div>
                                    </div>
                                  </div>
                                
                                  <ul class="wizard mb-0 row px-2 list-unstyled">
                                      <li class="next col">
                                        <button type="submit"  class="btn black-btn second-step  register-form-next">Save and Continue</button>
                                      </li>
                                  </ul>
                                  
                                  
                                </form>
                              </div>
                              </div> <!-- end row -->
                            </div>

                            <div class="" @if($data == "step3") style="display:block" @else style="display:none"  @endif  id="basictab3">
                              <!-- <div class="col-sm-12">
                                <div class="shipping-pro-type">
                                  <h3>Shipping Product type</h3>
                                  <div class="ship-product-type">
                                    <select name="product-types" id="products" class="form-control"  multiple="multiple" placeholder="Product Type">
                                      <option value="">Product Type1 <span class="checkmark"></span></option>
                                      <option value="">Product Type2 <span class="checkmark"></span></option>
                                      <option value="">Product Type3 <span class="checkmark"></span></option>
                                      <option value="">Product Type4 <span class="checkmark"></span></option>
                                    </select>
                                  </div> -->
                                  
                                  <!-- Note the missing multiple attribute! -->
                                  
                                <!-- </div> -->
                                <form method="POST" action="{{ route('register.store.facility', $userid) }}">
                                 @CSRF

                                  <div class="main-container">
                                    <div class="reg-facility-address">
                                      <h3>Add Facility Address</h3>
                                      <div class="shipping-form">
                                        <div class="row">
                                          <div class="col">
                                            <div class="form-group mt-3 mt-lg-5 mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                              <input type="text" name="name"  maxlength="40" parsley-trigger="change" required value="{{ old('name') }}" id="name"  class="form-control" placeholder="Nickname">
                                              @error('name')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-lg-6">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                              
                                              <select   name="city" style="padding: 2px !important;"  class="form-control" parsley-trigger="change" required value="{{ old('city') }}" id="city" >
                                              <option value="">Choose..</option>
                                                @if(isset($cities))
                                                    @foreach($cities as $city)
                                                      <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                    @endforeach
                                                @endif
                                              </select>
                                              @error('city')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                              
                                            </div>
                                          </div>
                                          <div class="col-lg-6">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                              
                                              <select   name="state"  style="padding: 2px !important;" class="form-control" parsley-trigger="change" required value="{{ old('state') }}" id="state">
                                              <option value="">Choose..</option>
                                              @if(isset($states))
                                                  @foreach($states as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                  @endforeach
                                              @endif
                                              </select>
                                              @error('state')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                              
                                            </div>
                                            
                                          </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="row">
                                          <div class="col-lg-6">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                              
                                              <select   name="country"  style="padding: 2px !important;" class="form-control" parsley-trigger="change" required value="{{ old('country') }}" id="country">
                                              <option value="">Choose..</option>
                                              @if(isset($countries))
                                                @foreach($countries as $country)
                                                  <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                @endforeach
                                              @endif
                                              </select>
                                              @error('country')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                              
                                            </div>
                                          </div>
                                          <div class="col-lg-6">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                              <input type="text" data-parsley-maxlength="8" maxlength="8" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" name="postcode2"  parsley-trigger="change" required value="{{ old('postcode2') }}" id="postcode2"  class="form-control" placeholder="Postcode">
                                              @error('postcode2')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                              <div class="icon location">
                                                <img src="{{ URL::asset('shipper/images/gps.svg')}}" class="img-fluid">
                                              </div>
                                            </div>
                                            
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                              <input type="call" data-parsley-maxlength="8" maxlength="8" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" name="phone_number"  parsley-trigger="change" required value="{{ old('phone_number') }}" id="phone_number" class="form-control" placeholder="Phone Number">
                                              @error('phone_number')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                              
                                              <input type="email" name="address_email"  maxlength="40" parsley-trigger="change" required value="{{ old('address_email') }}" id="address_email" class="form-control" placeholder="Email">
                                              @error('address_email')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col">
                                        <div class="add-facility">
                                          <div class="form-group mb-2 mb-md-3 mb-lg-4">
                                            <input type="checkbox" name="restroom_service" id="Restrooms" checked="checked">
                                            <label for="Restrooms">Restrooms</label>
                                          </div>
                                          <div class="form-group mb-2 mb-md-3 mb-lg-4">
                                            <input type="checkbox" id="Food" name="food_service" checked="checked">
                                            <label for="Food">Food</label>
                                          </div>
                                          <div class="form-group mb-2 mb-md-3 mb-lg-4">
                                            <input type="checkbox" id="Driver Rest Area" name="rest_area_service" checked="checked">
                                            <label for="Driver Rest Area">Driver Rest Area</label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                
                                  </div>
                                  <div class="bottom-container">
                                   
                                    <div class="terms-conditions mt-4">
                                      <span class="text">Terms and Conditions</span>
                                      <div class="form-group d-flex mt-1">
                                        <input type="checkbox" id="terms"  name="temrs" parsley-trigger="change" required>
                                        <label for="terms"></label>
                                        <p>Lorem Ipsum, Or Lipsum As It Is Sometimes Known</p>
                                      </div>
                                    </div>
                                      <ul class="wizard mb-0 row px-2 list-unstyled">
                                        <li class="next col">
                                          <button type="submit" class="btn purple-btn register-form-next">Save and Continue</button>
                                        </li>
                                    </ul>
                                  </div>
                                  
                                </form>
                                </div> <!-- end row -->
                              </div>
                             
                              </div> <!-- tab-content -->
                              </div> <!-- end #basicwizard-->
                            </div>
                          </div>
                          <div class="col-md-2 col-lg-2">
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end #basicwizard-->

       

@include('shipper.layouts.footer_script')


<!-- static opt -->
<input type="hidden" id="optcode" name="otpcode" value="456789">

<div class="modal fade" id="vefifyphone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Enter OTP Sent To Your Email Address</div>
                      <div class="verify-block">
                              <div class="otps d-flex mb-3">                             
                                  <form method="post" class="digit-group mt-md-3 mt-lg-4" data-group-name="digits" data-autosubmit="false" autocomplete="off">
                                               
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="digit-1" name="digit-1" data-next="digit-2" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="digit-5" name="digit-5" data-next="digit-6" data-previous="digit-4" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="digit-6" name="digit-6" data-previous="digit-5" />
                                               <hr><div class="digit-error text-danger" id="digit-error"></div>
                              
                             </div>
                       <a href="#" onclick="getProgress()" class="resend">Resend OTP</a>
                       <button type="button" onclick="verify_email_otp()" class="btn black-btn mt-4 mt-md-5">Verify</button>
                       </form>
                    </div>
              </div>
         </div>
    </div> 
</div>


<div class="modal fade" id="phone_otp_verify" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Enter OTP Sent To Your <div id="phone_fatch"></div></div>
                      <div class="verify-block">
                              <div class="otps d-flex mb-3">                             
                                  <form method="post" class="digit-group mt-md-3 mt-lg-4" data-group-name="digits" data-autosubmit="false" autocomplete="off">
                                               
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="phone-1" name="digit-1" data-next="digit-2" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="phone-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="phone-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="phone-4" name="digit-4" data-next="digit-5" data-previous="digit-3" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="phone-5" name="digit-5" data-next="digit-6" data-previous="digit-4" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" maxlength="1" id="phone-6" name="digit-6" data-previous="digit-5" />
                                               <hr><div class="phone_error text-danger" id="phone_error"></div>
                              
                             </div>
                       <!-- <a href="#" onclick="getProgress()" class="resend">Resend OTP</a> -->
                       <button type="button" onclick="verify_phone_otp()" class="btn black-btn mt-4 mt-md-5">Verify</button>
                       </form>
                    </div>
              </div>
         </div>
    </div> 
</div>

<script>
    function IsNumeric(event) {
      event = (event) ? event : window.event;
      var charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
  }

      function show() {
         var a = document.getElementById("password1");
         var b = document.getElementById("EYE");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }

      function show2(){
        
        var a = document.getElementById("confirm_password");
         var b = document.getElementById("EYE2");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }

   </script>