@include('shipper.layouts.header')

            <div class="sign-page">
                <div class="container-fluid p-0">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="img-wrapper">
                           <img src="images/car.png" class="img-fluid">
                        </div>
                        
                      </div>
                      <div class="col-md-5">
                         <div class="row">
                            
                            <div class="col-md-10 p-0">
                            @include('admin.include.flash-message')
                               <div class="sign-in-side">
                                  <h1>Sign in to Continue</h1>
                                  <ul class="nav nav-tabs justify-content-center mb-3 mb-md-4 mb-lg-5">
                                        <li class="nav-item">
                                            <a href="#shipper" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                Shipper
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#carrier" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                Carrier
                                            </a>
                                        </li>
                                 </ul>
                                 <div class="tab-content">
                                  <div class="tab-pane show active" id="shipper">
                                      <form action="{{ route('shipper.login.post') }}" method="POST">
                                      @CSRF
                                         <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                             <input type="email" id="email" parsley-trigger="change" required value="{{ old('email') }}" name="email" class="form-control" placeholder="Email or Phone#">
                                             <div class="icon">
                                                   <img src="images/user-fill.png" class="img-fluid" >                                                 
                                             </div>                              
                                              
                                          </div>
                                          <div class="form-group mb-4 ">
                                              <input type="password" name="password" parsley-trigger="change" required id="password" class="form-control" value="" placeholder="Password">
                                              <div class="icon">
                                              <a onclick="show()"><img src="images/key.svg" class="img-fluid" id="EYE">     </a>                                                
                                              </div>                                              
                                          </div>
                                          <!-- <a href="{{ route('password.request') }}" class="forgot">Forgot Password?</a> -->

                                          <div class="form-group  mt-5">
                                             <button type="submit" href="javascript:;" class="btn black-btn">Sign In</button>
                                          </div>
                                      </form>
                                  </div>
                                  <div class="tab-pane" id="carrier">
                                        <div class="download-app text-center">
                                            <div class="text">Download App to get started with Carrier!</div>
                                            <ul class=" justify-content-center mt-4">
                                               <li>
                                                  <a href="#">
                                                    <span class="icon"><img src="images/apple-store.svg" class="img-fluid"></span>
                                                    <span class="text-wrapper"><em>Available on the </em> App Store</span>
                                                  </a>
                                               </li>
                                               <li>
                                                  <a href="#">
                                                    <span class="icon"><img src="images/play-store.svg" class="img-fluid"></span>
                                                    <span class="text-wrapper"><em>Available on the </em> Play Store</span>
                                                  </a>
                                               </li>
                                            </ul>
                                        </div>                            
                                  </div>                            
                               </div>
                               </div>
                            </div>
                             <div class="d-sm-none col-md-2 d-md-block"></div>
                         </div>
                    
                      </div>
                    </div>
                 </div>

            </div>


   @include('shipper.layouts.footer_script')

   <script>
      function show() {
         var a = document.getElementById("password");
         var b = document.getElementById("EYE");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }

   </script>

