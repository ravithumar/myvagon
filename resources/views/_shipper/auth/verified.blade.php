
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/css/owl.carousel.min.css')}}">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <link href="{{ URL::asset('shipper/assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/app.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/app.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/icons.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/icons.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/css/responsive.css')}}">
  <!-- <link rel="stylesheet" href="{{ URL::asset('shipper/css/mulitple-select.min.css')}}"> -->
  <link rel="stylesheet" href="{{ URL::asset('shipper/css/multiple-select.css')}}">
  
     <!-- Plugins css -->
        <link href="{{ URL::asset('shipper/assets/libs/jquery-nice-select/nice-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
          <style>
               .was-validated{
                    border-color: red !important;
               }
               .was-validated-css{
                    display: none !important;
                    width: 100% !important;
                    margin-top: .25rem !important;
                    font-size: .75rem !important ;
                    color: #f1556c !important;
               }
          </style>
</head>
<body>
<div class="wrapper">
         
       

            <div class="sign-page">
                <div class="container-fluid p-0">

                    <div class="row align-items-center">
                      <div class="col-md-7">
                         <div class="img-wrapper">
                            <img src="{{ URL::asset('shipper/images/car.png')}}" class="img-fluid">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="row">
                           <div class="col-md-10 p-0">
                              <div class="sign-in-side">
                                  <div class="verification">
                                             <div class="big-text">Your Verification is Completed Succssfull </div>
                                             
                                  </div>
                              </div>
                           </div>
                           <div class="d-sm-none col-md-2 d-md-block"></div>
                        </div>                       
                      </div>
                    </div>
                 </div>
            </div>
            
            <script src="{{ URL::asset('shipper/js/jquery.min.js')}}"></script>
            <script src="{{ URL::asset('shipper/js/popper.min.js')}}"></script>
            <script src="{{ URL::asset('shipper/js/bootstrap.min.js')}}"></script>
            <script src="{{ URL::asset('shipper/js/owl.carousel.min.js')}}"></script>
               <script src="{{ URL::asset('shipper/js/common.js')}}"></script>
               <script src="{{ URL::asset('shipper/js/custome.js')}}"></script>
               <!-- Vendor js -->

                  <script src="{{ URL::asset('shipper/assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
                  <script src="{{ URL::asset('shipper/assets/libs/switchery/switchery.min.js')}}"></script>
                  <script src="{{ URL::asset('shipper/js/BsMultiSelect.js?v3')}}"></script>
                  <script src="{{ URL::asset('shipper/assets/libs/multiselect/jquery.multi-select.js')}}"></script>
                  <script src="{{ URL::asset('shipper/assets/libs/select2/select2.min.js')}}"></script>
                  <script src="{{ URL::asset('shipper/js/multiple-select.js')}}"></script>
                  <script src="{{ URL::asset('shipper/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
                  <script src="{{ URL::asset('shipper/assets/libs/autocomplete/jquery.autocomplete.min.js')}}"></script>
                  <script src="{{ URL::asset('shipper/js/BsMultiSelect.js')}}"></script>
                  <script src="{{ URL::asset('shipper/js/BsMultiSelect.js')}}"></script>
                  <script src="{{ URL::asset('shipper/assets/js/pages/form-advanced.init.js')}}"></script>
                  <script src="{{ URL::asset('shipper/assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
                           <!-- Plugins js-->
                  <script src="{{ URL::asset('shipper/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
                  <!-- Init js-->
                  <script src="{{ URL::asset('shipper/assets/js/pages/form-wizard.init.js')}}"></script>
                  
                  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"> </script>


                  <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
                  
        
