@include('shipper.layouts.header')
            <div class="sign-page">
                <div class="container-fluid p-0">

                    <div class="row">
                      <div class="col-md-7">
                         <div class="img-wrapper">
                            <img src="{{ URL::asset('shipper/images/car.png')}}" class="img-fluid">
                        </div>
                      </div>
                      <div class="col-md-5">
                         <div class="row">
                             <div class="col-md-10 p-0">
                               <div class="sign-in-side">
                                  <div class="forgot-block">
                                  @include('admin.include.flash-message')
                                  <form action="{{ route('shipper.forgot.password.post') }}" id="basic-form-forgot" method="POST" accept-charset="utf-8">
                                        @CSRF
                                            <div class="img-wrap text-center mb-md-4 mb-xl-5">
                                                <img src="{{ URL::asset('shipper/images/forgot-pass-pic.svg')}}" class="img-fluid"> 
                                            </div>
                                            <h1>Forgot Password</h1>
                                            <div class="text text-center">We will send OTP registered email</div>
                                            <div class="form-group mt-3 mt-md-5 " >
                                               <input type="email" parsley-trigger="change" required name="email" id="email"  value="{{ old('email') }}" class="form-control" required placeholder="Email">
                                               <div class="icon">
                                                  <img src="{{ URL::asset('shipper/images/user-fill.png')}}" class="img-fluid " style="margin-top:-50px;" >                                                   
                                               </div>                                                                            
                                            </div>
                                            <div class="form-group d-flex send-otp">
                                               <button type="submit"  class="btn black-btn">Send Otp</button>
                                            </div>
                                       </form> 
                                  </div>
                               </div>  
                             </div>
                             <div class="d-sm-none col-md-2 d-md-block"></div>                            
                         </div>                       
                      </div>
                    </div>
                 </div>

            </div>

              <!-- end #basicwizard-->
              @include('shipper.layouts.footer_script')

               
