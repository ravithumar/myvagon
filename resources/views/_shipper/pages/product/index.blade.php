@extends('shipper.layouts.shipper_master')
@section('content')

<div class="container-fluid">
  @include('admin.include.flash-message')
  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Product Master</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>
  <div class="row d-flex align-items-center products">
    <div class="col-md-6">
      <h3>Product Master</h3>
    </div>
    <div class="col-md-6">
      <a href="javascript:;" class="add-product justify-content-end d-flex" data-toggle="modal" data-target="#view-details">
        <img src="images/assets/dashbord/plus.svg" alt="chat" class="img-fluid">
        <span class="text ml-1">Add Product</span>
      </a>
    </div>
  </div>
  <div class="row">
    
    <div class="col-xl-12">
      <div class="card mt-2">
        <div class="card-body" >
          @include('admin.include.table')
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@include('shipper.include.table_script')
<!-- custome model -->
<div class="modal fade" id="view-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">Product</div>
          <div class="icon-block d-flex">
            <!-- <a href="javascript:;" class="icon-delete mx-auto text-center">
              <img src="{{ URL::asset('shipper/images/assets/dashbord/delete.svg')}}" class="img-fluid">
            </a> -->
            <!-- <a href="javascript:;" class="icon-pen mx-auto text-center">
              <img src="{{ URL::asset('shipper/images/assets/dashbord/pen.svg')}}" class="img-fluid">
            </a> -->
          </div>
        </div>
        <form action="{{ route('shipper.product.submit') }}" method="POST">
          @csrf
          <div class="form-group mb-4">
            <label for="name" class="mb-2 text-grey font-14">Product Name</label>
            <input type="text" id="name" maxlength="40" parsley-trigger="change" required name="name" class="form-control pb-2" placeholder="Enter Product Name" value="{{ old('name') }}">
            @error('name')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>
          <!-- <div class="form-group mb-4">
            <label for="first-name" class="mb-2 text-grey font-14">SKU#</label>
            <input type="number" id="sku_number" name="sku_number" maxlength="40" parsley-trigger="change" required class="form-control pb-2" value="{{ old('sku_number') }}" placeholder="Enter SKU Number">
            @error('sku_number')
            <div class="error">{{ $message }}</div>
            @enderror
          </div> -->
          <div class="form-group mb-4">
            <label for="first-name" class="mb-2 text-grey font-14">SKU Type</label>
            <input type="text" id="sku_type" name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control pb-2" value="{{ old('sku_type') }}" placeholder="Enter SKU Type">
            @error('sku_type')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>
          <div class="btn-block d-flex">
            <button type="button" class="btn close black-btn mt-3 mr-2 font-20" data-dismiss="modal" aria-label="Close">Cancel <span aria-hidden="true">&times;</span></button>
            <button type="submit" class="btn black-btn mt-3 font-20">Save</button>
          </div>
          
        </form>
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade add-more-products" id="edit_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">View Product Details</div>
          <div class="icon-block d-flex">
            <a href="javascript:;" class="icon-delete delete-this mx-auto text-center btn popup" data-toggle="modal" data-target="#delete">
              <img src="images/assets/dashbord/delete.svg" class="img-fluid">
            </a>
            
          </div>
        </div>
        
        
        <div class="form-group mb-4">
          <label for="name" class="mb-2 text-grey font-14">SKU Name</label>
          <input readonly type="text" id="name_view"  name="name" class="form-control pb-2" >
          
        </div>
        <div class="form-group mb-4">
          <label for="first-name" class="mb-2 text-grey font-14">SKU#</label>
          <input readonly type="text" id="sku_number_view" name="sku_number" maxlength="40" parsley-trigger="change" required class="form-control pb-2" >
          
        </div>
        <div class="form-group mb-4">
          <label for="first-name" class="mb-2 text-grey font-14">SKU Type</label>
          <input readonly type="text" id="sku_type_view" name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control pb-2" >
          
        </div>
        <button type="button" class="btn close black-btn mt-3 mr-2 font-20" data-dismiss="modal" aria-label="Close">Cancel <span aria-hidden="true">&times;</span></button>
        <!-- <button type="submit" class="btn black-btn mt-3 font-20">Save</button> -->
        
        
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade delete-this" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="title font-20 text-center">Do you want to continue to <span class="red">delete</span> permanently?</div>
        <form action="{{ route('shipper.product.delete', 0) }}" method="POST">
          @csrf
          <input type="hidden" name="id" id="product_data_id">
          <div class="btn-block d-flex mt-3">
            
            
            <button type="submit" class="btn close black-btn mr-2 mt-3 font-20">Yes</button>
            
            <button type="button" class="btn black-btn mt-3  font-20" data-dismiss="modal" aria-label="Close">No </button>
            
          </div>
        </form>
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade add-more-products" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">Edit Product Details</div>
          
        </div>
        <form id="form-sub"  method="POST">
          @csrf
          
          <div class="form-group mb-4">
            <label for="name" class="mb-2 text-grey font-14">Product Name</label>
            <input  type="text" id="name_edit"  name="name" class="form-control pb-2" >
            
          </div>
          <div class="form-group mb-4">
            <label for="first-name" class="mb-2 text-grey font-14">SKU#</label>
            <input  readonly type="text" id="sku_number_edit" name="sku_number" maxlength="40" parsley-trigger="change" required class="form-control pb-2" >
            
          </div>
          <div class="form-group mb-4">
            <label for="first-name" class="mb-2 text-grey font-14">SKU Type</label>
            <input  type="text" id="sku_type_edit" name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control pb-2" >
            
          </div>
          <div class="btn-block d-flex">
            <button type="button" class="btn close black-btn mt-3 mr-2 font-20" data-dismiss="modal" aria-label="Close">Cancel <span aria-hidden="true">&times;</span></button>
            <button type="submit" class="btn black-btn mt-3 font-20">Update</button>
          </div>
          
        </form>
        
      </div>
    </div>
  </div>
</div>
@endsection