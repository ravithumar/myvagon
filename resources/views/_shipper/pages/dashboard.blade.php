@extends('shipper.layouts.shipper_master')
@section('content')
<div class="container-fluid">
	<div class="row align-items-center">
		<div class="col-md-6">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">  &nbsp;&nbsp;Dashboard</a></li>
					
				</ol>
			</nav>
		</div>
	</div>
</div>
@endsection