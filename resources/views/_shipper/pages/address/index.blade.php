@extends('shipper.layouts.shipper_master')
<style>
.customselect-div{
    color: black !important;
    border-bottom: 2px solid #0a0a0a !important;
    display: block;
}
.b-color{
    color:black !important;
    font-size: 19px !important;
}
.bg-color{
    background-color: #f9f9f9 !important;
}

</style>
@section('content')

	 
	  <div class="container-fluid">
	  @include('admin.include.flash-message')
   	  	 	<div class="row align-items-center">
   	  	 		<div class="col-md-6">
   	  	 			  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">Dashboard</a></li>
                       <li class="breadcrumb-item active" aria-current="page">Address Book</li>
                    </ol>
                </nav>
   	  	 		</div>
            <div class="col-md-6"></div>   	  	 	
   	  	 	</div>
          <div class="row d-flex align-items-center products">
             <div class="col-md-6">
                <h3>Address Book</h3>
             </div>
             <div class="col-md-6">
            <!--  <a href="javascript:;" class="add-product justify-content-end d-flex" data-toggle="modal" data-target="#add-address">   
                    <img src="images/assets/dashbord/plus.svg" alt="chat" class="img-fluid">
                    <span class="text ml-1">Add Address</span>
                </a> -->
             </div>
          </div>

	<div class="row">
	
		<div class="col-xl-12">
    <div class="card mt-2">
				<div class="card-body" >
				@include('admin.include.table')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
    @include('shipper.include.table_script')


    
<!-- cutome model -->


<!-- Add address -->
<div class="modal fade add-address" id="add-address" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
              <div class="modal-body">         
                      <div class="edits mb-2 d-flex align-items-center justify-content-between">
                        <div class="title font-20 text-left">Add Facility Address</div>
                        
                    </div>
                    <form action="{{ route('shipper.address.submit') }}" method="POST">
                      @csrf
                        <div class="row">
                           <div class="col">
                              <div class="form-group mt-2 mb-4">           
                              <input type="text" maxlength="40" parsley-trigger="change" required id="name" name="name" class="form-control pb-2" placeholder="Nick Name">
                                @error('name')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col">                             
                                 <div class="form-group mt-2 mb-4">                                    
                                 <input type="text" id="address"  maxlength="40" parsley-trigger="change" required  name="address" class="form-control pb-2" placeholder="Address">
                                    @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                 </div>
                           </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mt-2 mb-4">
                                  <select  id="city" name="city" class="form-control customselect-div" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required >
                                  <option selected="selected" value="">Choose..</option>
                                            @if(isset($cities))
                                                @foreach($cities as $city)
                                                  <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                @endforeach
                                              @endif
                                  </select>
                                  @error('city')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                  
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2 mb-4">                       
                                  <select  id="state" name="state" class="form-control customselect-div" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                                  <option selected="selected" value="">Choose..</option>
                                            @if(isset($states))
                                                @foreach($states as $state)
                                                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                              @endif
                                  </select>
                                  @error('city')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                                              
                                  <div class="form-group mt-2 mb-4">
                                     <select id="country" name="country" class="form-control customselect-div"  data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                                     <option selected="selected" value="">Choose..</option>
                                            @if(isset($countries))
                                                @foreach($countries as $country)
                                                  <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                @endforeach
                                              @endif
                                      </select>
                                      @error('city')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                            
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2 mb-4">
                                  
                                <input type="text" id="postcode" name="postcode" maxlength="6" parsley-trigger="change" required class="form-control pb-2" placeholder="Postcode" >
                                  @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                  <div class="icon location">
                                     <img src="images/gps.svg" class="img-fluid">
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                               <div class="form-group mt-2 mb-4">
                                 
                               <input type="call" id="phone" name="phone" maxlength="8" parsley-trigger="change" required class="form-control pb-2" placeholder="Phone">
                                  @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                               
                               </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col">                                 
                                <div class="form-group mt-2 mb-4">
                                   
                                <input type="email" id="email" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2" placeholder="Email">
                                   @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="add-facility">
                                <div class="form-group mb-3">
                                        <input type="checkbox" value="1" name="restroom_service" id="Restrooms" checked="checked">
                                        <label for="Restrooms" class="font-18">Restrooms</label>
                                    </div>
                                     <div class="form-group mb-3">
                                        <input type="checkbox" value="1"  name="food_service" id="Food" checked="checked">
                                        <label for="Food" class="font-18">Food</label>
                                      </div>
                                      <div class="form-group">
                                         <input type="checkbox" value="1"  name="rest_area_service" id="Driver Rest Area" checked="checked">
                                         <label for="Driver Rest Area" class="font-18">Driver Rest Area</label>
                                      </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="btn-block d-flex">
                                   <button type="button" class="btn close black-btn mt-3 mr-5 font-20" data-dismiss="modal" aria-label="Close">Cancel <span aria-hidden="true">&times;</span></button>

                                   <button type="submit" class="btn black-btn mt-3 font-20">Save</button>
                                </div>
                            </div>
                           
                        </div>
                      </form>
                   
              </div>
         </div>
    </div> 
</div>

<!-- view address -->


<div class="modal fade add-address b-color" id="add-address-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered b-color" role="document">
         <div class="modal-content"> 
              <div class="modal-body">         
                      <div class="edits mb-2 d-flex align-items-center justify-content-between">
                        <div class="title font-20 text-left">View Facility Address</div>

                        <div class="icon-block d-flex">
                        <a href="javascript:;" class="icon-delete delete-this mx-auto text-center btn popup" data-toggle="modal" data-target="#delete">
                                <img src="images/assets/dashbord/delete.svg" class="img-fluid">
                        </a>
                             
                        </div>

                        
                    </div>
                      <form>
                        <div class="row">
                           <div class="col-md-12">
                              <div class="form-group mt-2 mb-4">
                                <label for="name" class="mb-2 text-grey font-14">Nick Name</label>           
                                <input type="text" readonly id="nick-name-view" name="nick-name" class="form-control pb-2 b-color bg-color" placeholder="Nick Name">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                            <div class="col">                                 
                                <div class="form-group mt-2 mb-4">
                                <label for="name" class="mb-2 text-grey font-14">Email</label>        
                                   <input type="email" id="email-view" name="email" class="form-control b-color pb-2" placeholder="Email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        
                            <div class="col-md-6">         
                                <div class="form-group mt-2 mb-4">
                                  <label for="name" class="mb-2 text-grey font-14">Postcode</label>        
                                  <input type="text" id="postcode-view" class="form-control b-color bg-color" placeholder="Postcode">
                                  <!-- <div class="icon location">
                                  <img src="images/gps.svg" class="img-fluid"> 
                                  </div> -->
                                </div>
                            </div>
                       
                       
                            <div class="col-md-6">
                               <div class="form-group mt-2 mb-4">
                               <label for="name" class="mb-2 text-grey font-14">Phone Number</label>        
                                  <input type="call" id="phone-view" name="call" class="form-control b-color pb-2" placeholder="Phone Number">
                               
                               </div>
                            </div>                            
                        </div>
                        
                        <div class="row">
                           <div class="col-md-4">                             
                                 <div class="form-group mt-2 mb-4">  
                                 <label for="name" class="mb-2 text-grey font-14">city</label>                                          
                                    <input type="text" readonly id="cityview"  class="form-control pb-2 b-color bg-color" >
                                 </div>
                           </div>
                        
                           <div class="col-md-4">                             
                                 <div class="form-group mt-2 mb-4">        
                                 <label for="name" class="mb-2 text-grey font-14">state</label>                                    
                                    <input type="text" readonly id="stateview"  class="form-control pb-2 b-color bg-color" >
                                 </div>
                           </div>

                           <div class="col-md-4">                             
                                 <div class="form-group mt-2 mb-4">     
                                 <label for="name" class="mb-2 text-grey font-14">country</label>                                       
                                    <input type="text" readonly id="countryview"  class="form-control pb-2 b-color bg-color" >
                                 </div>
                           </div>

                        </div> 


                        <div class="row">
                            <div class="col">
                            <label for="name" class="mb-2 text-grey font-14">Facility Services</label>    
                                <div class="add-facility row">
                                    
                                    <div class="form-group col-md-3 mb-3">
                                       
                                        <span >Restrooms</span><br>
                                        <span  id="span-1"></span>
                                        
                                       
                                    </div>
                                     <div class="form-group col-md-3 mb-3">
                                       
                                        <span >Food</span><br>
                                        <span  id="span-2"></span>

                                      </div>
                                      <div class="form-group col-md-4">
                                        <span >Driver Rest Area</span><br>
                                        <span  id="span-3"></span>

                                      </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-block d-flex" style="width: 115%;">
                                   <button type="button" class="btn close black-btn mt-3 mr-5 font-20" data-dismiss="modal" aria-label="Close">Cancel <span aria-hidden="true">&times;</span></button>

                                </div>
                            </div>
                           
                        </div>
                      </form>
                   
              </div>
         </div>
    </div> 
</div>

<div class="modal fade delete-this" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
              <div class="modal-body">                               
                        <div class="title font-20 text-center">Do you want to continue to <span class="red">delete</span> permanently?</div>
                        <form action="{{ route('shipper.address.delete', 0) }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="btn-block d-flex mt-3">
                        
                           
                             <button type="submit" class="btn close black-btn mr-2 mt-3 font-20">Yes</button>
                         
                             <button type="button" class="btn black-btn mt-3  font-20" data-dismiss="modal" aria-label="Close">No </button>
                             
                         </div>
                         </form>   
                   
              </div>
         </div>
    </div> 
</div>


<div class="modal fade add-address b-color" id="add-address-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   
<div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
              <div class="modal-body">         
                      <div class="edits mb-2 d-flex align-items-center justify-content-between">
                        <div class="title font-20 text-left">Edit Facility Address</div>
                        
                    </div>
                    <form id="form-sub"  method="POST">
                      @csrf

                        <div class="row">
                           <div class="col">
                              <div class="form-group mt-2 mb-4">           
                              <input type="text" maxlength="40" parsley-trigger="change" required id="name-edit" name="name" class="form-control pb-2" placeholder="Nick Name">
                                @error('name')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col">                             
                                 <div class="form-group mt-2 mb-4">                                    
                                 <input type="text" id="address-edit"  maxlength="40" parsley-trigger="change" required  name="address" class="form-control pb-2" placeholder="Address">
                                    @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                 </div>
                           </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mt-2 mb-4">
                                  <select  id="cityedit" name="city" class="form-control customselect-div" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required >
                                  <option selected="selected" value="">Choose..</option>
                                            @if(isset($cities))
                                                @foreach($cities as $city)
                                                  <option value="{{ $city->id }}" >{{ $city->name }}</option>
                                                @endforeach
                                              @endif
                                  </select>
                                  @error('city')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                  
                                </div>
                            </div>
                           
                            <div class="col-md-6">
                                <div class="form-group mt-2 mb-4">                       
                                  <select  id="state" name="state" class="form-control customselect-div" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                                  <option selected="selected" value="">Choose..</option>
                                            @if(isset($states))
                                                @foreach($states as $state)
                                                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                @endforeach
                                              @endif
                                  </select>
                                  @error('city')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                                              
                                  <div class="form-group mt-2 mb-4">
                                     <select id="country" name="country" class="form-control customselect-div"  data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                                     <option selected="selected" value="">Choose..</option>
                                            @if(isset($countries))
                                                @foreach($countries as $country)
                                                  <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                @endforeach
                                              @endif
                                      </select>
                                      @error('city')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                            
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mt-2 mb-4">
                                  
                                <input type="text" id="postcode-edit" name="postcode" maxlength="6" parsley-trigger="change" required class="form-control pb-2" placeholder="Postcode" >
                                  @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                  <div class="icon location">
                                     <img src="images/gps.svg" class="img-fluid">
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                               <div class="form-group mt-2 mb-4">
                                 
                               <input type="call" id="phone-edit" name="phone" maxlength="8" parsley-trigger="change" required class="form-control pb-2" placeholder="Phone">
                                  @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                               
                               </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col">                                 
                                <div class="form-group mt-2 mb-4">
                                   
                                <input type="email" id="email-edit" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2" placeholder="Email">
                                   @error('name')
                                    <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="add-facility">
                                <div class="form-group mb-3">
                                        <input type="checkbox" value="1" name="restroom_service" id="Restrooms" checked="checked">
                                        <label for="Restrooms" class="font-18">Restrooms</label>
                                    </div>
                                     <div class="form-group mb-3">
                                        <input type="checkbox" value="1"  name="food_service" id="Food" checked="checked">
                                        <label for="Food" class="font-18">Food</label>
                                      </div>
                                      <div class="form-group">
                                         <input type="checkbox" value="1"  name="rest_area_service" id="Driver Rest Area" checked="checked">
                                         <label for="Driver Rest Area" class="font-18">Driver Rest Area</label>
                                      </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="btn-block d-flex">
                                   <button type="button" class="btn close black-btn mt-3 mr-5 font-20" data-dismiss="modal" aria-label="Close">Cancel <span aria-hidden="true">&times;</span></button>

                                   <button type="submit" class="btn black-btn mt-3 font-20">Update</button>
                                </div>
                            </div>
                           
                        </div>
                      </form>
                   
              </div>
         </div>
    </div> 
</div>

@endsection


