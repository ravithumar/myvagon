
<table id="{{$dataTableId}}" class="table w-100 nowrap data-wrapper" >
	<thead>
		<tr>
			@foreach ($dateTableFields as $field)
			<th>
				{{ $field['title'] }}
			</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>