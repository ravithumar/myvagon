<script src="{{ URL::asset('assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/dataTables.select.min.js')}}"></script>



<link href="{{ URL::asset('shipper/assets/libs/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('shipper/assets/libs/datatables/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('shipper/assets/libs/datatables/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('shipper/assets/libs/datatables/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />

<script type="text/javascript">
$(function() {
    var table = $('#{{$dataTableId}}').DataTable({
        "sScrollY": "200px",
        "bScrollCollapse": true,
        "bJQueryUI": true,
        "aoColumnDefs": [
            { "sWidth": "auto", "aTargets": [ -1 ] }
        ],
        'responsive':true,
        language: {
            paginate: {
                previous: "<i class='mdi mdi-chevron-left'>",
                next: "<i class='mdi mdi-chevron-right'>"
            }
        },
        drawCallback: function() {
            $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
        },
        processing: true,
        serverSide: true,
        ajax: "{{$dateTableUrl}}",
        columns: JSON.parse(`<?php echo json_encode($dateTableFields);?>`)
    });


    
    $('#filter-form').on('submit', function(e) {
        var obj = {};
        var data = $(this).serialize().split("&");
        for (var key in data) {
            obj[data[key].split("=")[0]] = data[key].split("=")[1];
        }
        $.ajaxSetup({
            data: obj
        });
        table.draw();
        e.preventDefault();
    });


   

});
</script>

