<!DOCTYPE html>
<html>
<head>
  <title>MYVAGON | Shipper Panel</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="My vegon" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{ URL::asset('shipper/assets/images/favicon.ico')}}">
      
       
        
        @if (Auth::check()) 
        @include('shipper.layouts.navbar_custome')
        @include('shipper.layouts.left_sidebar')
        @else
        @include('shipper.layouts.header')
        @endif 
    </head>
    <body>

          <!-- Begin page -->
          <div id="wrapper">
      
      <!-- @include('admin.layouts.sidebar') -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
      @yield('content')
                </div> <!-- content -->
    @include('admin.layouts.footer')   

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
   
        @if (Auth::check()) 
        @include('shipper.layouts.bottom')
        @else
        @include('admin.layouts.footer-script')    
        @endif 

    </body>
</html>