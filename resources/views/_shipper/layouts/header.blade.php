 <title>MYVAGON | Shipper Panel</title>
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/css/owl.carousel.min.css')}}">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <link href="{{ URL::asset('shipper/assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/app.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/app.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/icons.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/assets/css/icons.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/css/dashboard.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('shipper/css/responsive.css')}}">
  <!-- <link rel="stylesheet" href="{{ URL::asset('shipper/css/mulitple-select.min.css')}}"> -->
  <link rel="stylesheet" href="{{ URL::asset('shipper/css/multiple-select.css')}}">
  
     <!-- Plugins css -->
        <link href="{{ URL::asset('shipper/assets/libs/jquery-nice-select/nice-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        



        <link href="{{ URL::asset('shipper/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- App css -->

        <link href="{{ URL::asset('shipper/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/css/style.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/css/responsive.css')}}" rel="stylesheet" type="text/css" />        
        <link href="{{ URL::asset('shipper/assets/libs/jquery-nice-select/nice-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
        

        
          <style>
               .was-validated{
                    border-color: red !important;
               }
               .was-validated-css{
                    display: none !important;
                    width: 100% !important;
                    margin-top: .25rem !important;
                    font-size: .75rem !important ;
                    color: #f1556c !important;
               }
          </style>
</head>
<body>
<div class="wrapper">
         
         <div class="main-menu">
                <div class="container">
                    <nav class="navbar pt-0 pb-0 navbar-expand-lg">
                           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                           <a class="navbar-brand" href="#" class="logo"><img src="{{ URL::asset('shipper/images/logo.png')}}" class="img-fluid"></a>
                           <div class="collapse navbar-collapse" id="navbarToggler">
                                   <ul class="navbar-nav ml-auto">
                                              <li class="nav-item active">
                                                    <a class="nav-link" href="{{ route('home') }}">Home </span></a>
                                               </li>
                                               <li class="nav-item">
                                                      <a class="nav-link" href="#">Carrier</a>
                                               </li>
                                               <li class="nav-item">
                                                    <a class="nav-link" href="#">Shipper</a>
                                               </li>
                                               <li class="nav-item">
                                                   <a class="nav-link" href="#">Company</a>
                                               </li>
                                               <li class="nav-item">
                                                    <a class="nav-link" href="#">Contact</a>
                                               </li>
                                               <li class="nav-item mr-0">
                                               
                                                  <li class="nav-item mr-0">
                                                       <a href="javascript:;" class="nav-link" data-toggle="modal" data-target="#login">Log in</a>                                                   
                                                  </li>                                                     
                                              </li>
                                               <li class="nav-item">
                                                    <a href="#" data-toggle="modal" data-target="#free-sign-up" class="nav-link btn btn-secondary btn-rounded waves-effect" >Free Sign Up</a>
                                               </li>
                                       </ul>
                                       <div id="language">
                                            <div class="dropdown">
                                              <button class="btn dropdown-toggle" type="button" id="language-bar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                EN
                                              </button>
                                              <div class="dropdown-menu" aria-labelledby="language-bar">
                                                <div class="radio-block">      
                                                  <div class="radio active text-left first">
                                                    <input type="radio" name="radio" id="radio5" value="option5" checked>
                                                    <label for="radio5"> English</label>
                                                  </div>
                                                  <div class="radio  text-left">
                                                       <input type="radio" name="radio" id="radio6" value="option6">
                                                       <label for="radio6">Greek</label>
                                                   </div>
                                                    
                                                  </div>
                                                </div>
                                            </div>

                                        
                                        </div>
                           </div>
                    </nav>
                </div>
            </div>


 <!-- model for loguin and signup -->
            
<div class="modal fade radio-popup" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Login  as</div>
                      <div class="radio-block">      
                        <div class="radio active mb-3  text-left">
                             <input type="radio" name="radio1" id="radio1" value="option1" checked>
                             <label for="radio1"> Shipper</label>
                        </div>
                        <div class="radio mb-3  text-left">
                             <input type="radio" name="radio1" id="radio3" value="option3">
                             <label for="radio3">Carrier</label>
                         </div>
                         <a href="{{ route('shipper.login') }}" class="btn black-btn mt-4">Proceed</a>
                        
                    </div>
              </div>
         </div>
    </div> 
</div>

<div class="modal fade radio-popup" id="free-sign-up" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Signup as</div>
                      <div class="radio-block">      
                        <div class="radio active mb-3  text-left">
                             <input type="radio" name="radio" id="radio2" value="option2" checked>
                             <label for="radio1"> Shipper</label>
                        </div>
                        <div class="radio mb-3  text-left">
                             <input type="radio" name="radio" id="radio4" value="option4">
                             <label for="radio3">Carrier</label>
                         </div>
                         <a href="{{ route('shipper.register') }}" class="btn black-btn mt-4">Proceed</a>
                        
                    </div>
              </div>
         </div>
    </div> 
</div>