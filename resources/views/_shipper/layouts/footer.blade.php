<div class="btm-footer">
   <div class="top">
      <div class="container">
        <div class="row">
           <div class="col-md-3">
              <div class="logo-wrapper">
                 <img src="{{ URL::asset('shipper/images/logo.png')}}" class="img-fluid">
              </div>
              <div class="text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore</div>
           </div>
           <div class="col-md-2"></div>
           <div class="col-md-7">
              <div class="row">
                <div class="col-sm-6 col-md-3">
                   <div class="links">
                    <h3>Quick Links</h3>
                     <ul>
                       <li><a href="javascript:;">Home</a></li>
                       <li><a href="javascript:;">About Us</a></li>
                       <li><a href="javascript:;">Service</a></li>
                     </ul>
                   </div>
                </div>
                <div class="col-sm-6 col-md-3">
                   <div class="links">
                     <h3>Social Media</h3>
                     <ul>
                       <li><a href="javascript:;">Instagram</a></li>
                       <li><a href="javascript:;">Facebook</a></li>
                       <li><a href="javascript:;">Linkedin</a></li>
                     </ul>
                   </div>
                </div>
                <div class="col-sm-6 col-md-3">
                  <div class="links">
                    <h3>Users</h3>
                     <ul>
                       <li><a href="javascript:;">Carrier</a></li>
                       <li><a href="javascript:;">Shipper</a></li>
                       <li><a href="javascript:;">Dispatcher</a></li>
                     </ul>
                   </div>
                </div>
                <div class="col-sm-6 col-md-3">
                  <div class="links">
                    <h3>Contact Us</h3>
                     <ul>
                       <li>Velestino, Greece</li>
                       <li><a href="mailto:support@myvagon.com">support@myvagon.com</a></li>
                       <li><a href="tel:+(30)123 456 7890">+(30)123 456 7890</a></li>
                     </ul>
                   </div>
                </div>
              </div>
           </div>
        </div>
     </div>
   </div>

   <div class="bottom">
     <div class="container">
       <div class="row">
         <div class="col-md-6">
            <div class="copyright">
               All rights reserved by MyVagon.            
            </div>
         </div>
         <div class="col-md-6">
           <ul class="terms d-flex justify-content-end">
              <li><a href="javascript:;">Privacy</a></li>
              <li><a href="javascript:;">Security</a></li>
              <li><a href="javascript:;">Terms</a></li>
           </ul> 
         </div>
       </div>
      </div>
   </div>
   
</div>
<div class="modal fade" id="vefifyphone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Enter OTP Sent To +91 98765-9876</div>
                      <div class="verify-block">
                              <div class="otps d-flex mb-3">                             
                                  <form method="get" class="digit-group mt-md-3 mt-lg-4" data-group-name="digits" data-autosubmit="false" autocomplete="off">
                                               <input type="text" id="digit-1" name="digit-1" data-next="digit-2" />
                                               <input type="text" id="digit-2" name="digit-2" data-next="digit-3" data-previous="digit-1" />
                                               <input type="text" id="digit-3" name="digit-3" data-next="digit-4" data-previous="digit-2" />
                                               <input type="text" id="digit-4" name="digit-4" data-next="digit-5" data-previous="digit-3" />
                                               <input type="text" id="digit-5" name="digit-5" data-next="digit-6" data-previous="digit-4" />
                                               <input type="text" id="digit-6" name="digit-6" data-previous="digit-5" />
                                  </form>
                              
                             </div>
                       <a href="#" class="resend">Resend OTP</a>
                       <button type="button" class="btn black-btn mt-4 mt-md-5">Verify</button>
                    </div>
              </div>
         </div>
    </div> 
</div>

<div class="modal fade radio-popup" id="free-sign-up" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Signup as</div>
                      <div class="radio-block">      
                        <div class="radio active mb-3  text-left">
                             <input type="radio" name="radio" id="radio2" value="option2" checked>
                             <label for="radio2"> Shipper</label>
                        </div>
                        <div class="radio mb-3  text-left">
                             <input type="radio" name="radio" id="radio4" value="option4">
                             <label for="radio4">Carrier</label>
                         </div>
                         <a href="registration-forms.php" class="btn black-btn mt-4">Proceed</a>
                        
                    </div>
              </div>
         </div>
    </div> 
</div>




</body>
</html>