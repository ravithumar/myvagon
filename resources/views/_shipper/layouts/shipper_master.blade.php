<!DOCTYPE html>
<html>
  <head>
    <title>MYVAGON</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ URL::asset('shipper/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
    
    <!-- App css -->
    <link href="{{ URL::asset('shipper/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/css/dashboard.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/css/responsive.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/assets/libs/jquery-nice-select/nice-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/assets/libs/multiselect/multi-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('shipper/assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    
  </head>
  <body>
    @include('shipper.layouts.navbar_custome')
    @include('shipper.layouts.left_sidebar')
    <div class="content-page more">
      <div class="content">
        @yield('content')
      </div>
    </div>
    @include('shipper.layouts.bottom')
    @yield('script')