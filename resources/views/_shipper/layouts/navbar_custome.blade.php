<div class="navbar-custom">
  <div class="item-container d-flex align-items-center">
    <div class="logo-box align-items-center">
      <a href="#" class="logo text-center">
        <span class="logo-lg">
          <img src="{{ URL::asset('shipper/images/logo.png')}}" alt="" class="img-fluid" height="35">
          <!-- <span class="logo-lg-text-light">UBold</span> -->
        </span>
        <span class="logo-sm">
          <!-- <span class="logo-sm-text-dark">U</span> -->
          <img src="{{ URL::asset('shipper/images/logo.png')}}" alt="" class="img-fluid" height="35">
        </span>
      </a>
    </div>
    
    <ul class="list-unstyled topnav-menu topnav-menu-left m-0 align-items-center d-flex search-bar">
      
      
      <li class="dropdown d-none d-sm-block d-lg-block">
        <a class="nav-link dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
          Destination
          <img src="{{ URL::asset('shipper/images/assets/dashbord/destination-angle-down.svg')}}" class="img-fluid">
        </a>
        <div class="dropdown-menu destination">
          <ul class="radio-block">
            
            <li class="radio active text-left pb-2 mb-3">
              
              <input type="radio" name="radio" id="radio7" value="option7">
              <label for="radio7"> All</label>
            </li>
            <li class="radio text-left pb-2 mb-3">
              
              <input type="radio" name="radio" id="radio8" value="option8">
              <label for="radio8"> Origin</label>
            </li>
            <li class="radio text-left pb-2 mb-3">
              
              <input type="radio" name="radio" id="radio9" value="option9">
              <label for="radio9"> Destination</label>
            </li>
            <li class="radio text-left pb-2 mb-3">
              
              <input type="radio" name="radio" id="radio10" value="option10">
              <label for="radio10"> Truck Type</label>
            </li>
          </ul>
          
        </div>
      </li>
      <li>
        <form class="app-search">
          <div class="app-search-box">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search available trucks">
              <div class="input-group-append">
                <button class="btn" type="submit">
                <img src="{{ URL::asset('shipper/images/assets/dashbord/search.svg')}}" class="img-fluid">
                </button>
              </div>
            </div>
          </div>
        </form>
      </li>
    </ul>
  </div>
  
  <ul class="list-unstyled topnav-menu float-right align-items-center mb-0 d-flex profile">
    
    <li class="dropdown notification-list">
      <a class="nav-link dropdown-toggle btn shipment mr-2" href="#">
        <img src="{{ URL::asset('shipper/images/assets/dashbord/create-shipping-fast.svg')}}" alt="chat" class="img-fluid">
        <span class="text ml-1">Create Shipment</span>
      </a>
    </li>
    <li class="dropdown notification-list">
      <a class="nav-link dropdown-toggle waves-effect waves-light" href="#">
        <img src="{{ URL::asset('shipper/images/assets/dashbord/chat.svg')}}" alt="Chat" class="img-fluid">
      </a>
    </li>
    <li class="dropdown notification-list">
      <a class="nav-link dropdown-toggle notify" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
        <span class="icon"><img src="{{ URL::asset('shipper/images/assets/dashbord/notification.svg')}}" class="img-fluid"><span class="badge badge-danger rounded-circle noti-icon-badge">3</span></span>
      </a>
      
    </li>
    
    <li class="dropdown notification-list">
      <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
        
        <span class="pro-user-name ml-1 mr-2">
          Bert Roger
        </span>
        <img src="{{ URL::asset('shipper/images/user-1.jpg')}}" alt="user-image" class="img-fluid">
      </a>
      <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
        <!-- item-->
        <ul>
          <li>
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <span>My Profile</span>
            </a>
          </li>
          <li>
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <span>Settings</span>
            </a>
          </li>
          <li>
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <span>Change Password</span>
            </a>
          </li>
          <li>
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <span>Privacy Policy</span>
            </a>
          </li>
          <li>
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <span>Terms and Conditions</span>
            </a>
          </li>
          <li>
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <span>About Us</span>
            </a>
          </li>
          <li>
            <a href="{{ route('shipper.logout') }}" class="dropdown-item notify-item">
              <span>Logout</span>
            </a>
          </li>
        </ul>
        <!-- item-->
        
      </div>
    </li>
    
    <li>
      <div id="language">
        
        <div class="dropdown">
          <button class="btn dropdown-toggle" type="button" id="language-bar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          EN
          </button>
          <div class="dropdown-menu" aria-labelledby="language-bar">
            <div class="radio-block">
              <div class="radio active text-left first">
                <input type="radio" name="radio" id="radio5" value="option5" checked>
                <label for="radio5"> English</label>
              </div>
              <div class="radio  text-left">
                <input type="radio" name="radio" id="radio6" value="option6">
                <label for="radio6">Greek</label>
              </div>
              
            </div>
          </div>
        </div>
        
      </div>
    </li>
    <li class="search">
      <div class="search-area d-flex">
        <input class="search-input" type="text" name="" placeholder="Search..">
        <a href="#" class="search-btn">
          
        </a>
      </div>
    </li>
  </ul>
  
</div>