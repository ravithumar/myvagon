<div class="left-side-menu">

<div class="slimscroll-menu">

    <!--- Sidemenu -->
    <div id="sidebar-menu">

        <ul class="metismenu" id="side-menu">

            <li>
                <a href="javascript: void(0);" class="d-flex">
                   <!--  <i class="dashboard"></i>    -->
                    <span class="icon mr-2"><img src="{{ URL::asset('shipper/images/assets/dashbord/dashboard.svg')}}" class="img-fluid"></span> 
                    <span> Dashboards </span>
                    <span class="menu-arrow"><img src="{{ URL::asset('shipper/images/assets/dashbord/angle-down.svg')}}" class="img-fluid"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li class="active">
                        <a href="index.html">All</a>
                    </li>
                    <li>
                        <a href="dashboard-2.html">Pending (30)</a>
                    </li>
                    <li>
                        <a href="dashboard-3.html">Scheduled (23)</a>
                    </li>
                    <li>
                        <a href="dashboard-4.html">In-Progress (32)</a>
                    </li>
                      <li>
                        <a href="dashboard-4.html">Completed (23)</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript: void(0);" class="d-flex">
                   <!--  <i class="dashboard"></i>    -->
                   <span class="icon mr-2"><img src="{{ URL::asset('shipper/images/assets/dashbord/create-shipping-fast.svg')}}" class="img-fluid"></span><span> Create Shipment </span>                                    
                </a>
                
            </li>

            <li>
                <a href="javascript: void(0);" class="d-flex">
                   <!--  <i class="dashboard"></i>    -->
                   <span class="icon mr-2"><img src="{{ URL::asset('shipper/images/assets/dashbord/manage-shipments.svg')}}" class="img-fluid"></span>
                   <span> Manage Shipments </span>                                    
                </a>
                
            </li>
            <li>
                <a href="javascript: void(0);" class="d-flex">
                   <!--  <i class="dashboard"></i>    -->
                   <span class="icon mr-2"><img src="{{ URL::asset('shipper/images/assets/dashbord/search-trucks.svg')}}" class="img-fluid"></span>
                   <span> Search Available Trucks</span>                                   
                </a>
                
            </li>
            
            <li>
                <a href="javascript: void(0);">
                    <span class="icon mr-2"><img src="{{ URL::asset('shipper/images/assets/dashbord/more.svg')}}" class="img-fluid"></span>
                    <span> More </span>
                    <span class="menu-arrow"><img src="{{ URL::asset('shipper/images/assets/dashbord/angle-down.svg')}}" class="img-fluid"></span>
                </a>
                <ul class="nav-second-level" aria-expanded="false">
                    <li>
                        <a href="javascript:;">Manager Management</a>
                    </li>
                    <li>
                        <a href="{{ route('shipper.address.index') }}">Address Book</a>
                    </li>
                    <li>
                        <a href="{{ route('shipper.product.index') }}">Product Master</a>
                    </li>
                    <li>
                        <a href="javascript:;">Leads</a>
                    </li>
                </ul>
            </li>

            
        </ul>

    </div>
    <!-- End Sidebar -->

    <div class="clearfix"></div>

</div>
<!-- Sidebar -left -->

</div>