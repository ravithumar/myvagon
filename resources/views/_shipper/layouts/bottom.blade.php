

        <script src="{{ URL::asset('shipper/assets/js/vendor.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/js/common.js')}}"></script>
    
 
        <!-- Plugins js-->
     
        <script src="{{ URL::asset('shipper/assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/switchery/switchery.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/js/BsMultiSelect.js?v3')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/multiselect/jquery.multi-select.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/switchery/switchery.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/js/multiple-select.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/js/pages/dashboard-1.init.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/dataTables.bootstrap4.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/buttons.flash.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/buttons.print.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/datatables/dataTables.select.min.js')}}"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Datatables init -->
        <script src="{{ URL::asset('shipper/assets/js/pages/datatables.init.js')}}"></script>
        <!-- App js-->
        <script src="{{ URL::asset('shipper/assets/js/app.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
        
     

         
        <script>
        
            $('form').parsley();
            $(function () {
            

            $('form').parsley().on('field:validated', function() {
                var ok = $('.parsley-error').length === 0;
                $('.bs-callout-info').toggleClass('hidden', !ok);
                $('.bs-callout-warning').toggleClass('hidden', ok);
            })
            .on('form:submit', function() {
                return false; 
            });
            });

            
           
        </script>

    </body>
</html>