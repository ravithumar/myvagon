<nav class="navbar pt-0 pb-0 navbar-expand-lg">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
<a class="navbar-brand" href="#" class="logo"><img src="{{ URL::asset('shipper/images/logo.png')}}" class="img-fluid"></a>
<div class="collapse navbar-collapse" id="navbarToggler">
    <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home </span></a>
            </li>
            <li class="nav-item">
                    <a class="nav-link" href="#">Carrier</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Shipper</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Company</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Contact</a>
            </li>
            <li class="nav-item mr-0">
                <a href="javascript:;" class="nav-link" data-toggle="modal" data-target="#login">Log in</a>                                                   
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link btn btn-secondary btn-rounded waves-effect" data-toggle="modal" data-target="#free-sign-up">Free Sign Up</a>
            </li>
        </ul>
        <div class="form-group mb-0 sm">                                                   
                <select  data-plugin="customselect">
                    <option value="0">EN</option>
                    <option value="1">FR</option>                                                        
                </select>                                                
        </div>
</div>
</nav>