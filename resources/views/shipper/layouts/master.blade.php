<?php $session_data = Auth::user();?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>MYVAGON | SHIPPER</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="My vegon" name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico')}}">

        @include('shipper.layouts.head')
        
    </head>
    <body>

          <!-- Begin page -->
          <div id="wrapper">
      @include('shipper.layouts.topbar')
      @include('shipper.layouts.sidebar')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
      @yield('content')
                </div> <!-- content -->
    @include('shipper.layouts.footer')
            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    @include('shipper.layouts.footer-dash-script')


    <!-- <input type="text" value="{{ Session::get('running') }}" name="session" id="sessionid" /> -->

    <script>
        var user_id = 0;
        <?php if(isset($session_data['id']) && !empty($session_data['id'])){?>
            user_id = '{{ $session_data['id'] }}';
        <?php }?>
         function codeAddress() {
            // console.log(document.getElementById('sessionid').value);
        }


        window.onload = codeAddress;




    </script>

    </body>
</html>
