
        @yield('css')

        <!-- App css -->
        

        <link href="{{ URL::asset('shipper/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/css/dashboard.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/Notiflix/src/notiflix.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />


        <link href="{{ URL::asset('shipper/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/clockpicker/bootstrap-clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('shipper/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
       

        
        