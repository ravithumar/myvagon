<style type="text/css">
            .no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; }
.se-pre-icon {
   position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url('//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Phi_fenomeni.gif/50px-Phi_fenomeni.gif') 
              50% 50% no-repeat rgb(249,249,249);
opacity: 0.8;

}
        </style>
<?php 
$user = Auth::user();
?>
<!-- ========== Left Sidebar Start ========== -->
<div class="se-pre-icon" >
            
        </div>
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                {{-- <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-desktop"></i> <span>{{__('Dashboard')}}</span><span class="menu-arrow"></span></a>
                                        
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                          <a href="{{route('shipper.dashboard')}}">
                           <span> {{__('All')}}</span>
                          </a>
                        </li>
                        <li>
                          <a href="javascript: void(0);">Pending (30)</a>
                        </li>
                        <li>
                          <a href="javascript: void(0);">Scheduled (23)</a>
                        </li>
                        <li>
                          <a href="javascript: void(0);">In-Progress (32)</a>
                        </li>
                        <li>
                          <a href="javascript: void(0);">Completed (23)</a>
                        </li>
                                
                    </ul>
                </li> --}}
              
                <li>
                    <a href="{{ route('shipper.dashboard') }}">
                        <i class="fas fa-desktop"></i>
                        <span> {{__('Dashboard')}}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('shipper.shipment.create') }}">
                        <i class="fas fa-shipping-fast"></i>
                        <span> {{__('Create Shipment')}}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('shipper/manage-shipment') }}">
                        <i class="fas fa-cog"></i>
                        <span> {{__('Manage Shipments')}}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('shipper/search-available-trucks') }}">
                        <i class="fas fa-search"></i>
                        <span> {{__('Search Available Trucks')}}</span>
                    </a>
                </li>                              

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-cog"></i> <span>{{__('More')}}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        {{-- @if(isset($user) && !empty($user) && $user->type != 'manager') --}}
                        <li>
                            <a href="{{route('shipper.manager-management.index')}}">
                                <span>{{__('Manager Management')}}</span>
                            </a>
                        </li>
                        {{-- @endif --}}
                        <li>
                            <a href="{{route('shipper.address.index')}}">
                                <span> {{__('Address Book')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('shipper.product.index')}}">
                                <span> {{__('Product Master')}}</span>
                            </a>
                        </li>
                        <li id="get-notification">
                            <a href="{{url('/shipper/notification')}}">
                                <span> {{__('Notifications')}}</span>
                            </a>
                        </li> 
                        
                    </ul>
                </li>                         
                
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
<script type="text/javascript">
    $(window).load(function() {
    $(".se-pre-icon").fadeOut("slow");
});
</script>