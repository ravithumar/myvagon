<?php 
$segment = Request::segment(2);
?>
@if($segment != 'register')
<div class="btm-footer">
   <div class="top">
      <div class="container">
        <div class="row">
           <div class="col-md-3">
              <div class="logo-wrapper">
                 <img src="{{ url('images/logo.png') }}" class="img-fluid">
              </div>
              <div class="text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore</div>
           </div>
           <div class="col-md-2"></div>
           <div class="col-md-7">
              <div class="row">
                <div class="col-sm-6 col-md-3">
                   <div class="links">
                    <h3>Quick Links</h3>
                     <ul>
                       <li><a href="javascript:;">Home</a></li>
                       <li><a href="javascript:;">About Us</a></li>
                       <li><a href="javascript:;">Service</a></li>
                     </ul>
                   </div>
                </div>
                <div class="col-sm-6 col-md-3">
                   <div class="links">
                     <h3>Social Media</h3>
                     <ul>
                       <li><a href="javascript:;">Instagram</a></li>
                       <li><a href="javascript:;">Facebook</a></li>
                       <li><a href="javascript:;">Linkedin</a></li>
                     </ul>
                   </div>
                </div>
                <div class="col-sm-6 col-md-3">
                  <div class="links">
                    <h3>Users</h3>
                     <ul>
                       <li><a href="javascript:;">Carrier</a></li>
                       <li><a href="javascript:;">Shipper</a></li>
                       <li><a href="javascript:;">Dispatcher</a></li>
                     </ul>
                   </div>
                </div>
                <div class="col-sm-6 col-md-3">
                  <div class="links">
                    <h3>Contact Us</h3>
                     <ul>
                       <li>Velestino, Greece</li>
                       <li><a href="mailto:support@myvagon.com">support@myvagon.com</a></li>
                       <li><a href="tel:+(30)123 456 7890">+(30)123 456 7890</a></li>
                     </ul>
                   </div>
                </div>
              </div>
           </div>
        </div>
     </div>
   </div>

   <div class="bottom">
     <div class="container">
       <div class="row">
         <div class="col-md-6">
            <div class="copyright">
               All rights reserved by MYVAGON.            
            </div>
         </div>
         <div class="col-md-6">
           <ul class="terms d-flex justify-content-end">
              <li><a href="javascript:;">Privacy</a></li>
              <li><a href="javascript:;">Security</a></li>
              <li><a href="javascript:;">Terms</a></li>
           </ul> 
         </div>
       </div>
      </div>
   </div>
   
</div>
@endif
<script src="{{ URL::asset('shipper/js/jquery.min.js')}}"></script>
  <script src="{{ URL::asset('shipper/js/popper.min.js')}}"></script>
  <script src="{{ URL::asset('shipper/js/bootstrap.min.js')}}"></script>
  <script src="{{ URL::asset('shipper/js/owl.carousel.min.js')}}"></script>
    <script src="{{ URL::asset('shipper/js/common.js')}}"></script>
    <script src="{{ URL::asset('shipper/js/custome.js')}}"></script>
    <script src="{{ URL::asset('shipper/js/OTPvalidate.js')}}"></script>
    <!-- Vendor js -->

        <script src="{{ URL::asset('shipper/assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/switchery/switchery.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/js/BsMultiSelect.js?v3')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/multiselect/jquery.multi-select.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/js/multiple-select.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/autocomplete/jquery.autocomplete.min.js')}}"></script>
        <script src="{{ URL::asset('shipper/js/BsMultiSelect.js')}}"></script>
        <script src="{{ URL::asset('shipper/js/BsMultiSelect.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/js/pages/form-advanced.init.js')}}"></script>
        <script src="{{ URL::asset('shipper/assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
                <!-- Plugins js-->
        <script src="{{ URL::asset('shipper/assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
        <!-- Init js-->
        

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"> </script>


        <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/Notiflix/src/notiflix.js')}}"></script>


        @yield('script')

        <script>
          

            $("#products").bsMultiSelect({cssPatch : {
                   choices: {columnCount:'3' },
            }});

            $('form').parsley();

            $("select[name=city]").on('change', function() {
              console.log($(this).val());
              if ($(this).val() === 'other') {
                  $("div[name=error]")
                  .attr('data-parsley-required', 'true')
                  .parsley();
              } else {
                $("div[name=error]")
                  .removeAttr('data-parsley-required')
                  .parsley().destroy();
              }
          });

          $("form").parsley({

            /*triggerAfterFailure: 'focusout changed.bs.select'*/
          });

          // $(".select2").select2({
          //     tags: true
          //   });



 $(document).ready(function(){
    $('body').on('click','.registration',function(){
      var shipper=$(this).val();
      if(shipper=='option4'){
        $('#carrier1').show();
        $('#ship1').hide();
      }else{
        $('#carrier1').hide();
        $('#ship1').show();
      }
    })
  })

  function show() {
         var a = document.getElementById("password1");
         var b = document.getElementById("EYE");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }
      
      function show2(){
        
        var a = document.getElementById("confirm_password");
         var b = document.getElementById("EYE2");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }

function nospaces(t){
  if(t.value.match(/\s/g)){
    t.value=t.value.replace(/\s/g,'');
  }
}

       </script>
