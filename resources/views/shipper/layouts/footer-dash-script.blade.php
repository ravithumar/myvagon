        <!-- Vendor js -->


        <script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>

       

        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
        <!-- <script src="{{ URL::asset('assets/libs/notiflix/notiflix-2.1.2.js')}}"></script> -->
        <script src="{{ URL::asset('assets/libs/Notiflix/src/notiflix.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{ URL::asset('js/keypress.js')}}"></script>
        <!-- image zoom script -->
        <!-- Magnific Popup-->
         <script src="{{ URL::asset('shipper/js/common.js')}}"></script>

         <script>
            var NODE_URL = "{{env('NODE_URL')}}";
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
        <script src="{{ asset('shipper/js/shipper_socket.js')}}"></script>
         @yield('script')
        <script >

			 
			     var session = `<?php echo json_encode(Session::get('running')); ?>`;
	             var segment = `{{ Request::segment(2) }}`;
	             
	            
	             if(session == `"running"`){
	             	console.log(segment, segment != `shipment`);
	              	if(segment != `shipment`){
                          var retVal = true;
	             		// var retVal =  confirm("Are you sure ! you want to exist the page");

	             		// console.log(retVal);
	             		if(retVal == false){
	             			
	             		}
				     

		             	
		             }
	             	
	             }else{
	             	console.log('here');
	             }
             

        	 $(document).ready(function() {
                $('form').parsley();

                $('.select2').select2();

               $('.my-select').select2({
                  width: "100%",
                  placeholder: 'Please Select Sub Category'
                }).trigger('change'); 
               $('.destinations').select2({
                  width: "100%",
                  placeholder: 'Destinations'
                }).val('').trigger('change'); 
               $('#country').select2({
                  width: "100%",
                  placeholder: 'Choose'
                }).val('').trigger('change');
               $('#state').select2({
                  width: "100%",
                  placeholder: 'Choose'
                }).val('').trigger('change');
               $('#city').select2({
                  width: "100%",
                  placeholder: 'Choose'
                }).val('').trigger('change');
                // $('.product_name').on('select2:select', function (e) {
                //   width: "100%",
                //   placeholder: 'Choose One'
                // }).val('').trigger('change');

                $('.product_name').on('select2:select', function (e) {
                    var data = e.params.data;
                    console.log(data);
                });

                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });
               
        });
        </script>
        @yield('script-bottom')


