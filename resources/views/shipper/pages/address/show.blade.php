@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
#map {
  width: 100%;
  height: 100vh;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="{{ url('dashboard') }}" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">{{__('Dashboard')}}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('shipper/address') }}" class="align-items-center">{{__('Address Book')}}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__('View Address')}}</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>
  {{-- @include('admin.include.flash-message') --}}

  <div class="row d-flex align-items-center products manage-shipments">
    <div class="col-md-6">
      <h3>{{__('View Address')}}</h3>
    </div>
    <div class="col-md-6">
      <ul class="d-flex align-items-center no-bids-search justify-content-end">
        <li>
          <a href="{{ route('shipper.address.edit',$address['id']) }}" class="cancel p-1 border-r6"><img src="{{ url('shipper/images/pen.svg') }}" alt="" class="img-fluid"></a>
        </li>
        <li>
          <a href="javascript:void(0)" data-newurl="address/delete" data-url="/delete" data-token="{{ csrf_token() }}" class="cancel p-1 border-r6 delete_address" data-id="{{ $address['id'] }}"><img src="http://13.36.112.48/shipper/images/delete.svg" alt="" class="img-fluid"></a>
        </li>
      </ul>
    </div>
  </div>
  <div class="row">
    
    <div class="col-xl-4">
      <div class="card">
        <div class="card-body table-responsive" >
          {{-- <form action="" method="POST"> --}}
            <div class="form-group mb-3">
              <label for="name" class="mb-2 text-grey font-14">{{__('Name')}}</label>
              <input type="text" id="name" maxlength="40" autocomplete="off" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="{{__('Enter Name')}}" value="{{ $address['name'] }}" readonly>
            </div>
  
            <div class="form-group mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('Address')}}</label>
              <input type="text" id="autocomplete" name="address" autocomplete="off" parsley-trigger="change" required class="autocomplate-input form-control form-control-without-border fulladdress" value="{{ $address['address'] }}" placeholder="{{__('Enter Address')}}" readonly>
            </div>

            <div class="form-group mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('City')}}</label>
              <input type="text" name="city" autocomplete="off" required class="autocomplate-input form-control form-control-without-border fulladdress" value="{{ $address['city'] }}" placeholder="{{__('Enter City')}}" readonly>
            </div>
  
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group  mb-3">
                  <label for="first-name" class="mb-2 text-grey font-14">{{__('Phone')}}</label>
                  <div class=" d-flex">
                    <select name="country" class="form-control" style="width: 35%;padding: 0px 0px 2px 0px;" parsley-trigger="change"  >
                          @if(isset($countries))
                              @foreach($countries as $country)
                                <option value="{{ $country->id }}" selected>+ {{ $country->phonecode }}</option>
                              @endforeach
                          @endif
                      </select>
                    <input type="call" id="phone" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="8" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="{{__('Phone')}}" value="{{ $address['phone'] }}" readonly>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group  mb-3">
                <label for="first-name" class="mb-2 text-grey font-14">{{__('Email')}}</label>
                  <input type="email" id="email" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="{{__('Email')}}" value="{{ $address['email'] }}" readonly>
                </div>
              </div>
            </div>
  
            <div class="row">
              <div class="col">
                <div class="add-facility">
                  <div class="form-group checkbox-inline">
                    <label><input type="checkbox" value="1" class="largerCheckbox" name="restroom_service" id="Restrooms" {{ $address['restroom_service'] == '1' ? 'checked' : '' }} disabled><span></span></label>
                    <label for="Restrooms" class="font-15">{{__('Restrooms')}}</label>
                  </div>
                  <div class="form-group">
                    <label><input type="checkbox" value="1" class="largerCheckbox" name="food_service" id="Food" {{ $address['food_service'] == '1' ? 'checked' : '' }} disabled><span></span></label>
                    <label for="Food" class="font-15">{{__('Food')}}</label>
                  </div>
                  <div class="form-group">
                    <label><input type="checkbox" value="1" class="largerCheckbox" name="rest_area_service" id="Driver Rest Area" {{ $address['rest_area_service'] == '1' ? 'checked' : '' }} disabled><span></span></label>
                    <label for="Driver Rest Area" class="font-15">{{__('Driver Rest Area')}}</label>
                  </div>
   
                </div>
              </div>
            </div>
  
           
            <div class="btn-block d-flex">
              <a href="{{ route('shipper.address.index') }}" class="btn w-100 btn-white mr-2" >{{__('Cancel')}}</a>
            </div>
            
          {{-- </form> --}}
        </div>
      </div>
    </div>
    <div class="col-xl-8">
      <div id="map"></div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
<!-- {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCjorOISx346EPRCKse9h8K_fZmNnWZ00&libraries=places&callback=initAutocomplete" async defer></script> --}} -->
<script type="text/javascript">
      function changeMapLocation(latitude=0,longitude=0){
        (function initMap() {
          var position = {lat: latitude, lng: longitude};
          var myLatlng = new google.maps.LatLng(latitude,longitude);
          var geocoder = new google.maps.Geocoder();
          var map = new google.maps.Map(document.getElementById("map"), {
              zoom: 19,
              center: position
          });
          var marker = new google.maps.Marker({
              position: position,
              map: map,
              draggable: false
          });
        })();
      }

      $(document).ready(function(){
            <?php if(isset($address['address_lat']) && !empty($address['address_lat']) && isset($address['address_lng']) && !empty($address['address_lng'])){?>
                changeMapLocation(<?=$address['address_lat']?>,<?=$address['address_lng']?>)
            <?php }?>
        $(document).on('click','.delete_address',function (e) {
          var id = $(e).data('id');        
          var token = $(e).data('token');
          Notiflix.Confirm.Show(
          'Confirm',
          'Are you sure that you want to delete this record?',
          'Yes',
          'No',
          function() {
              Notiflix.Loading.Standard();
              $.ajax({
                  url: "{{ url('shipper/address/delete',$address['id']) }}",
                  
                  type: 'post',
                  dataType: "JSON",
                  data: {
                      "id": id,
                      "_method": 'DELETE',
                      "_token": token,
                  },
                  success: function(returnData) {
                      Notiflix.Loading.Remove();
                      Notiflix.Notify.Success('Deleted');
                      // tr.remove();
                      window.location.href = "{{ url('shipper/address') }}";
                      // $("#edit_model").modal("hide");
                      // location.reload();

                  }
              });
          });
        });
      });
</script>
@endsection