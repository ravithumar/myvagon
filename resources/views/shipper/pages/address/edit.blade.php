@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
#map {
  width: 100%;
  height: 100vh;
}
.countrys{
  width: 35%;
  padding: 0px 0px 2px 0px;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="{{ url('shipper/dashboard') }}" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">{{__('Dashboard')}}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('shipper/address') }}" class="align-items-center"> {{__('Address Book')}}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__('Edit Address')}}</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>

  <div class="row d-flex align-items-center products">
    <div class="col-md-6">
      <h3>{{__('Edit Address')}} </h3>
    </div>
    <div class="col-md-6">
      
    </div>
  </div>
  <div class="row">
    
    <div class="col-xl-4">
      <div class="card">
        <div class="card-body table-responsive" >
          <form action="{{ url('shipper/address/update',$address['id']) }}" method="POST">
            @csrf
            <div class="form-group mb-3">
              <label for="name" class="mb-2 text-grey font-14">{{__('Name')}}</label>
              <input type="text" id="name" maxlength="40" autocomplete="off" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="{{__('Enter Name')}}" value="{{ $address['name'] }}">
              @if($errors->has('name'))
                  <div class="error text-danger">{{ $errors->first('name') }}</div>
              @endif
            </div>
  
            <div class="form-group mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('Address')}}</label>
              <input type="text" id="autocomplete" name="address" autocomplete="off" required class="autocomplate-input form-control form-control-without-border fulladdress" value="{{ $address['address'] }}" placeholder="{{__('Enter Address')}}">
              <input type="hidden" id="latitude" name="lat" value="{{ $address['address_lat'] }}">
              <input type="hidden" id="longitude" name="lng" value="{{ $address['address_lng'] }}">
              <input type="hidden" id="post_code" name="post_code" value="{{ $address['zipcode'] }}">
              @if($errors->has('lat'))
                  <div class="error text-danger">{{ $errors->first('lat') }}</div>
                @elseif($errors->has('address'))
                  <div class="error text-danger">{{ $errors->first('address') }}</div>
              @endif
            </div>

            <div class="form-group mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('City')}}</label>
              <input type="text" id="city" name="city" autocomplete="off" class="autocomplate-input form-control form-control-without-border fulladdress" value="{{ $address['city'] }}" placeholder="{{__('Enter City')}}">
              
              @if($errors->has('city'))
                <div class="error text-danger">{{ $errors->first('city') }}</div>
              @endif
            </div>
  
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group  mb-3">
                  <label for="first-name" class="mb-2 text-grey font-14">{{__('Phone')}}</label>
                  <div class=" d-flex">
                    <select name="country" class="form-control countrys" style="width: 35%;padding: 0px 0px 2px 0px;" parsley-trigger="change"  >
                          @if(isset($countries))
                              @foreach($countries as $country)
                                <option value="{{ $country->id }}" selected>+ {{ $country->phonecode }}</option>
                              @endforeach
                          @endif
                      </select>
                    <input type="call" id="phone" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="15" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="{{__('Phone')}}" value="{{ $address['phone'] }}">
                    @if($errors->has('phone'))
                      <div class="error text-danger">{{ $errors->first('phone') }}</div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group  mb-3">
                <label for="first-name" class="mb-2 text-grey font-14">{{__('Email')}}</label>
                  <input type="email" id="email" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="{{__('Email')}}" value="{{ $address['email'] }}">
                  @if($errors->has('email'))
                    <div class="error text-danger">{{ $errors->first('email') }}</div>
                  @endif
                </div>
              </div>
            </div>
  
            <div class="row">
              <div class="col">
                <div class="add-facility">
                  <div class="form-group checkbox-inline">
                    <label><input type="checkbox" value="1" class="largerCheckbox" name="restroom_service" id="Restrooms" {{ $address['restroom_service'] == '1' ? 'checked' : '' }}><span></span></label>
                    <label for="Restrooms" class="font-15">{{__('Restrooms')}}</label>
                  </div>
                  <div class="form-group">
                    <label><input type="checkbox" value="1" class="largerCheckbox" name="food_service" id="Food" {{ $address['food_service'] == '1' ? 'checked' : '' }}><span></span></label>
                    <label for="Food" class="font-15">{{__('Food')}}</label>
                  </div>
                  <div class="form-group">
                    <label><input type="checkbox" value="1" class="largerCheckbox" name="rest_area_service" id="Driver Rest Area" {{ $address['rest_area_service'] == '1' ? 'checked' : '' }}><span></span></label>
                    <label for="Driver Rest Area" class="font-15">{{__('Driver Rest Area')}}</label>
                  </div>
   
                </div>
              </div>
            </div>
  
           
            <div class="btn-block d-flex">
              <a href="{{ route('shipper.address.index') }}" class="btn w-100 btn-white mr-2" >{{__('Cancel')}}</a>
              <button type="submit" class="btn w-100 btn-primary">{{__('Save')}}</button>
            </div>
            
          </form>
        </div>
      </div>
    </div>
    <div class="col-xl-8">
      <div id="map"></div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
<script type="text/javascript">
$(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

      var placeSearch;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            console.log(pickup_place);
            var address = pickup_place.address_components;
            var street = city = state = pincode = '';
            $.each(address, function(i,val){
              console.log(val);
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
                if($.inArray('postal_code', val['types']) > -1) {
                    pincode += val['long_name'];
                }
            });
            $('#city').val(city);
            $('#latitude').val(pickup_place.geometry.location.lat());
            $('#longitude').val(pickup_place.geometry.location.lng());
            $('#post_code').val(pincode);
            changeMapLocation(pickup_place.geometry.location.lat(),pickup_place.geometry.location.lng())
        });
    });

      function changeMapLocation(latitude=0,longitude=0){
        (function initMap() {
          var position = {lat: latitude, lng: longitude};
          var myLatlng = new google.maps.LatLng(latitude,longitude);
          var geocoder = new google.maps.Geocoder();
          var map = new google.maps.Map(document.getElementById("map"), {
              zoom: 19,
              center: position
          });
          var marker = new google.maps.Marker({
              position: position,
              map: map,
              draggable: true
          });

          

          google.maps.event.addListener(marker, 'dragend', function() {
              map.panTo(marker.getPosition()); 
              geocoder.geocode({'latLng': marker.getPosition() }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                    var address_components = results[0].address_components;
                    var components={};
                    jQuery.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.long_name});});
                    var postal_code;
                    if(components.postal_code) {
                      postal_code = components.postal_code;
                    }
                  $('#autocomplete').val(results[0].formatted_address);
                  $('#latitude').val(marker.getPosition().lat());
                  $('#longitude').val(marker.getPosition().lng());
                  $('#post_code').val(postal_code);
                  }
                }
              });
          });
        })();
      }

      $(document).ready(function(){
            <?php if(isset($address['address_lat']) && !empty($address['address_lat']) && isset($address['address_lng']) && !empty($address['address_lng'])){?>
                changeMapLocation(<?=$address['address_lat']?>,<?=$address['address_lng']?>)
            <?php }?>
      })
     
</script>
@endsection