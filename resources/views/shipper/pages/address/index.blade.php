@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="{{ url('shipper/dashboard') }}" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">{{__('Dashboard')}}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__('Address Book')}}</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>
  @include('admin.include.flash-message')

  <div class="row d-flex align-items-center products">
    <div class="col-md-6">
      <h3>{{__('Address Book')}}</h3>
    </div>
    <div class="col-md-6">
      <a href="{{ route('shipper.address.create') }}" class="btn btn-primary float-right" >
        <img src="images/assets/dashbord/plus.svg" alt="chat" class="img-fluid">
        <span class="text ml-1">{{__('Add Address')}}</span>
      </a>
    </div>
  </div>
  <div class="row">
    
    <div class="col-xl-12">
      <div class="card">
        <div class="card-body table-responsive" >
          @include('admin.include.table')
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@include('shipper.include.table_script')


<!-- Add address -->
<div class="modal fade add-address" id="add-address" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">{{__('Add Facility Address')}}</div>
        </div>
        <form action="{{ route('shipper.address.submit') }}" method="POST">
          @csrf
          <div class="form-group mb-3">
            <label for="name" class="mb-2 text-grey font-14">{{__('Name')}}</label>
            <input type="text" id="name" maxlength="40" autocomplete="off" onkeypress="return lettersValidate(event)" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="{{__('Enter Name')}}" value="{{ old('name') }}">
            @error('name')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group mb-3">
            <label for="first-name" class="mb-2 text-grey font-14">{{__('Address')}}</label>
            <input type="text" id="autocomplete" name="address" autocomplete="off" onkeypress="return lettersValidate(event)" parsley-trigger="change" required class="autocomplate-input form-control form-control-without-border fulladdress" value="{{ old('address') }}" placeholder="{{__('Enter Address')}}">
            <input type="hidden" id="latitude" name="lat" value="">
            <input type="hidden" id="longitude" name="lng" value="">
            @error('address')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>

          <div class="row">
            <div class="col-md-6">

            <div class="form-group  mb-3 add-facility-address">
              <select id="country" name="country" class="form-control form-control-without-border customselect-div country select2"  data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                
                @if(isset($countries))
                @foreach($countries as $country)
                <option value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
                @endif
              </select>
              @error('city')
              <div class="error">{{ $message }}</div>
              @enderror
            
                </div>
            </div>
            <div class="col-md-6">
            
            <div class="form-group  mb-3 add-facility-address">
       
                <select  id="state" name="state" class="form-control form-control-without-border customselect-div state select2" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                
                  {{-- @if(isset($states))
                  @foreach($states as $state)
                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                  @endforeach
                  @endif --}}
                </select>
                @error('city')
                <div class="error">{{ $message }}</div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              
              <div class="form-group  mb-3 add-facility-address">
                <select  id="city" name="city" class="form-control form-control-without-border customselect-div city select2" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required >                 
                  {{-- @if(isset($cities))
                  @foreach($cities as $city)
                  <option value="{{ $city->id }}">{{ $city->name }}</option>
                  @endforeach
                  @endif --}}
                </select>
                @error('city')
                <div class="error">{{ $message }}</div>
                @enderror
                
              </div>
            </div>
           

            <div class="col-md-6">
              <div class="form-group  mb-3">
             
                <input type="text" id="postcode"  autocomplete="off" onkeypress="return numberValidate(event)" name="postcode" maxlength="6" parsley-trigger="change" required class="form-control form-control-without-border pb-2" placeholder="{{__('Postcode')}}" >
                <!-- <div class="icon location">
                  <img src="images/gps.svg" style="height: 72%;" class="img-fluid">
                </div> -->
                @error('name')
                <div class="error">{{ $message }}</div>
                @enderror
                
              
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group  mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('Phone')}}</label>
                <input type="call" id="phone" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="8" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="{{__('Phone')}}">
                @error('name')
                <div class="error">{{ $message }}</div>
                @enderror
                
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group  mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('Email')}}</label>
                <input type="email" id="email" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Email">
                @error('name')
                <div class="error">{{ $message }}</div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="add-facility">
                <div class="form-group checkbox-inline">
                  <label><input type="checkbox" value="1" class="largerCheckbox" name="restroom_service" id="Restrooms" checked="checked"><span></span></label>
                  <label for="Restrooms" class="font-15">{{__('Restrooms')}}</label>
                </div>
                <div class="form-group">
                  <label><input type="checkbox" value="1" class="largerCheckbox" name="food_service" id="Food" checked="checked"><span></span></label>
                  <label for="Food" class="font-15">{{__('Food')}}</label>
                </div>
                <div class="form-group">
                  <label><input type="checkbox" value="1" class="largerCheckbox" name="rest_area_service" id="Driver Rest Area" checked="checked"><span></span></label>
                  <label for="Driver Rest Area" class="font-15">{{__('Driver Rest Area')}}</label>
                </div>
 
              </div>
            </div>
          </div>

         
          <div class="btn-block d-flex">
            <button type="button" class="btn w-100 btn-white mr-2" data-dismiss="modal" aria-label="Close">{{__('Cancel')}}</button>
            <button type="submit" class="btn w-100 btn-primary">{{__('Save')}}</button>
          </div>
          
        </form>
        
      </div>
    </div>
  </div>
</div>

<!-- view address -->
<div class="modal fade add-address" id="view-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">Address Details</div>
          <div class="icon-block d-flex">
          
            <a href="javascript:;" class="" id="button-id" ata-popup="tooltip" 
              onclick="delete_notiflix(this);return false;"  data-newurl="address/delete" data-url="/delete" data-token="{{ csrf_token() }}">
              <i class="fa fa-trash" style="font-size: 19px !important;color:#1f1f41!important;"></i>
            </a>
            
          </div>

        </div>
       
         <input type="hidden" name="id" value="" id="productid">

          <div class="form-group mb-3">
            <label for="name" class="mb-1 text-grey font-14">Name</label>
            <input type="text" readonly id="name-view" maxlength="40" required name="name" class="form-control tx-b form-control-without-border" placeholder="Enter Name" value="{{ old('name') }}">
           
          </div>

          <div class="form-group mb-3">
            <label for="first-name" class="mb1 text-grey font-14">Address</label>
            <input type="text" readonly id="address-view" name="address" autocomplete="off" onkeypress="return lettersValidate(event)" maxlength="40" parsley-trigger="change" required class="autocomplate-input  form-control form-control-without-border" value="{{ old('address') }}" placeholder="Enter Address">
           
          </div>

          

          <div class="row">
            <div class="col-md-6">
              <div class="form-group  mb-3">
                <label for="first-name" class="mb1 text-grey font-14">Country</label>
                <input type="text" readonly id="countryview" name="city"   class="form-control form-control-without-border">
              </div>
            </div>
           

            <div class="col-md-6">
              <div class="form-group  mb-3">
                <label for="first-name" class="mb1 text-grey font-14">State</label>
                <input type="text" readonly id="stateview"  autocomplete="off"  class="form-control form-control-without-border pb-2">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group  mb-3">
                <label for="first-name" class="mb1 text-grey font-14">City</label>
                <input type="text" readonly id="cityview" name="city"   class="form-control tx-b form-control-without-border">
              </div>
            </div>
           

            <div class="col-md-6">
              <div class="form-group  mb-3">
                <label for="first-name" class="mb1 text-grey font-14">Postcode</label>
                <input type="text" readonly id="postcodeview"  autocomplete="off"  class="form-control form-control-without-border pb-2">
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col">
              <div class="form-group  mb-3">
              <label for="first-name" class="mb1 text-grey font-14">Phone</label>
                <input type="call" readonly id="phone-view" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="8" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Phone">
               
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group  mb-3">
              <label for="first-name" class="mb1 text-grey font-14">Email</label>
                <input type="email" id="email-view" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Email">
                
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="add-facility">
                <div class="form-group checkbox-inline">
                  <label>
                    <input type="checkbox" onclick="return false"  value="1" name="restroom_service" id="restrooms-view" >
                    <span></span>
                  </label>
                  <label for="Restrooms" class="font-15">Restrooms</label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" onclick="return false"  value="1"  name="food_service" id="food-view" >
                    <span></span>
                  </label>
                  <label for="Food" class="font-15">Food</label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" onclick="return false"  value="1"  name="rest_area_service" id="driver-view" >
                    <span></span>
                  </label>
                  <label for="Driver Rest Area" class="font-15">Driver Rest Area</label>
                </div>
 
              </div>
            </div>
          </div>
          <div class="btn-block d-flex">
          <button type="button" class="btn btn-white w-100" data-dismiss="modal" aria-label="Close">Cancel</button>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="modal fade add-address" id="edit-address" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">Edit Facility Address</div>
        </div>
        <form action="#" method="POST" id="form-sub">
          @csrf
          <div class="form-group mb-3">
            <label for="name" class="mb-2 text-grey font-14">Name</label>
            <input type="text" id="name-edit" autocomplete="off" onkeypress="return lettersValidate(event)" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="Enter Name" value="{{ old('name') }}">
            @error('name')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group mb-3">
            <label for="first-name" class="mb-2 text-grey font-14">Address</label>
            <input type="text" id="edit_address" name="address" autocomplete="off" onkeypress="return lettersValidate(event)" parsley-trigger="change" required class="autocomplate-input form-control form-control-without-border fulladdress" value="{{ old('address') }}" placeholder="Enter Address">
            <input type="hidden" id="edit-latitude" name="lat" value="{{ old('address_lat') }}">
            <input type="hidden" id="edit-longitude" name="lng" value="{{ old('address_lng') }}">
            @error('address')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>

          <div class="row">
            <div class="col-md-6">

            <div class="form-group  mb-3">
              <select id="country-edit" name="country" class="form-control form-control-without-border customselect-div country"  data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                <option selected="selected" value="">Choose..</option>
                @if(isset($countries))
                @foreach($countries as $country)
                <option value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
                @endif
              </select>
              @error('city')
              <div class="error">{{ $message }}</div>
              @enderror
                </div>
            </div>
            <div class="col-md-6">
            
            <div class="form-group  mb-3">
       
                <select  id="state-edit" name="state" class="form-control form-control-without-border customselect-div state" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                  <option selected="selected" value="">Choose..</option>
                  {{-- @if(isset($states))
                  @foreach($states as $state)
                  <option value="{{ $state->id }}">{{ $state->name }}</option>
                  @endforeach
                  @endif --}}
                </select>
                @error('city')
                <div class="error">{{ $message }}</div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              
              <div class="form-group  mb-3">
                <select  id="city-edit" name="city" class="form-control form-control-without-border customselect-div city" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required >
                  <option selected="selected" value="">Choose..</option>
                  @if(isset($cities))
                  @foreach($cities as $city)
                  <option value="{{ $city->id }}">{{ $city->name }}</option>
                  @endforeach
                  @endif
                </select>
                @error('city')
                <div class="error">{{ $message }}</div>
                @enderror
                
                
              </div>
            </div>
           

            <div class="col-md-6">
              <div class="form-group  mb-3">
             
                <input type="text" id="postcode-edit"  autocomplete="off" onkeypress="return numberValidate(event)" name="postcode" maxlength="6" parsley-trigger="change" required class="form-control form-control-without-border pb-2" placeholder="Postcode" >
                <!-- <div class="icon location">
                  <img src="images/gps.svg" style="height: 72%;" class="img-fluid">
                </div> -->
                @error('name')
                <div class="error">{{ $message }}</div>
                @enderror
                
              
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="form-group  mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">Phone</label>
                <input type="call" id="phone-edit" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="8" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Phone">
                @error('name')
                <div class="error">{{ $message }}</div>
                @enderror
                
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group  mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">Email</label>
                <input type="email" id="email-edit" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Email">
                @error('name')
                <div class="error">{{ $message }}</div>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
              <div class="add-facility">
                <div class="form-group checkbox-inline">
                  <label>
                    <input type="checkbox" value="1" name="restroom_edit" id="restrooms-edit" >
                    <span></span>
                  </label>
                  <label for="Restrooms" class="font-15">Restrooms</label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" value="1"  name="food_edit" id="food-edit" >
                    <span></span>
                  </label>
                  <label for="Food" class="font-15">Food</label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" value="1"  name="rest_service-edit" id="rest_service-edit" >
                    <span></span>
                  </label>
                  <label for="Driver Rest Area" class="font-15">Driver Rest Area</label>
                </div>
 
              </div>
            </div>
          </div>


         
          <div class="btn-block d-flex">
            <button type="button" class="btn w-100 btn-white mr-2" data-dismiss="modal" aria-label="Close">Cancel</button>
            <button type="submit" class="btn w-100 btn-primary">Update</button>
          </div>
          
        </form>
        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
<!-- {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCjorOISx346EPRCKse9h8K_fZmNnWZ00&libraries=places&callback=initAutocomplete" async defer></script> --}} -->
<script type="text/javascript">
  var placeSearch;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
      
      // function initAutocomplete() {

      //   var input = document.getElementsByClassName('autocomplate-input');
      //   for (var i =0; i < input.length; i++) {
           
      //     const autocomplete = new google.maps.places.Autocomplete(
      //     (input[i]),
      //         {types: ['geocode']});
      //     // autocomplete.addListener('place_changed', fillInAddress);
      //     autocomplete.addListener('place_changed', () =>{
      //       const place = autocomplete.getPlace();
      //       console.log(place);
      //     });
      //    } 

      google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            var address = pickup_place.address_components;
            var street = city = state = '';
            $.each(address, function(i,val){
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
            });
            $('#latitude').val(pickup_place.geometry.location.lat());
            $('#longitude').val(pickup_place.geometry.location.lng());
        });
    });

    google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('edit_address'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            var address = pickup_place.address_components;
            var street = city = state = '';
            $.each(address, function(i,val){
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
            });
            $('#edit-latitude').val(pickup_place.geometry.location.lat());
            $('#edit-longitude').val(pickup_place.geometry.location.lng());
        });
    });

     
      // }

      // function fillInAddress() {
      //   var place = autocomplete.getPlace();
      //   console.log(place);
      //   // console.log(place.geometry.location.lat());
      //   // console.log(place.geometry.location.lng());
      //   console.log('here place serach js');
      //   for (var i = 0; i < place.address_components.length; i++) {
      //     for (var j = 0; j < place.address_components[i].types.length; j++) {
      //       if (place.address_components[i].types[j] == "postal_code") {
                
      //           console.log(place.address_components[i].long_name,"-------");

      //       }
      //     }
      //   }
        
      // }
     
</script>
@endsection