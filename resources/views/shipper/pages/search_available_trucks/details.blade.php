@extends('shipper.layouts.master')

@section('content')
@if($booking['status'] == "pending")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(213,105,105,0.14) !important;border:1px solid #D56969;}
</style>
@endif

@if($booking['status'] == "in-progress")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(105,163,213,0.14) !important;border: 1px solid #699ed5;}
</style>
@endif

@if($booking['status'] == "completed")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(117,213,105,0.14) !important;border: 1px solid #7fd569;}
</style>
@endif

@if($booking['status'] == "cancelled")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(27,54,78,0.14) !important;border: 1px solid #252a38;}
</style>
@endif
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.bid-steps li.active .icon .inner-icon {
    /* background-color: #1F1F41; */
    background-color: #9B51E0;
    width: 12px;
    height: 12px;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">Manage Shipments</a></li>
          <li class="breadcrumb-item active" aria-current="page">Order ID #{{$booking['id']}}</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>
  @include('admin.include.flash-message')
  
  <div class="row d-flex align-items-center products manage-shipments">
    <div class="col-md-6">
    	<div class="order-status d-flex align-items-center">
		  <p class="font-20 txt-blue mb-0">Order ID #{{$booking['id']}}</p>
		  <div class="form-group mb-0 ml-3">
                                   
            <select class="form-control  font-17 txt-blue border-r9 package_type pb-1" style="background: none;appearance: none;text-align:center" name="status" required="" disabled>
              <!-- <option value="create" {{$booking['status'] == 'create' ? 'selected' : ''}}>Created</option> -->
              <option value="pending" {{$booking['status'] == 'pending' || $booking['status'] == 'create' ? 'selected' : ''}}>Pending</option>
              <option value="in-progress" {{$booking['status'] == 'in-progress' ? 'selected' : ''}}>In-progress</option>
              <option value="completed" {{$booking['status'] == 'completed' ? 'selected' : ''}}>Completed</option>
              <option value="cancelled" {{$booking['status'] == 'cancelled' ? 'selected' : ''}}>Cancelled</option>
            </select>
          </div>
    	</div>
       
    </div>
    <div class="col-md-6">
        @if(isset($booking['status']) && !empty($booking['status']) && $booking['status'] != 'completed')
        <ul class="d-flex align-items-center no-bids-search justify-content-end">
          @if($booking['status'] == 'pending')
          <!-- <li>
            <a href="#" class="btn btn-primary btn-sm mr-2 mt-0">                 
             <span class="text ml-1">Search Available Trucks</span>
           </a>
          </li> -->
          @endif
          @if(($booking['status'] == 'pending' || $booking['status'] == 'create'))
          <li>
            <a href="{{ url('shipper/shipment/edit',$booking['id']) }}" class="cancel p-1 border-r6" data-id="{{ $booking['id'] }}"><img src="{{ URL::asset('shipper/images/pen.svg')}}" alt="" class="img-fluid"></a>
          </li>
          @endif
          <li>
            <a href="javascript:void(0)" class="flag p-1 border-r6 open_map" data-id="{{ $booking['id'] }}"><img src="{{ URL::asset('shipper/images/flag.svg')}}" alt="" class="img-fluid"></a>
          </li>
          
          @if(($booking['status'] == 'pending' || $booking['status'] == 'scheduled' || $booking['status'] == 'create'))
          <li>
            <a href="javascript:void(0)" class="cancel p-1 border-r6 cancel_shipment" data-id="{{ $booking['id'] }}"><img src="{{ URL::asset('shipper/images/delete.svg')}}" alt="" class="img-fluid"></a>
          </li>
          @endif
          <li>
            <a href="#" class="share p-1 border-r6"><img src="{{ URL::asset('shipper/images/share.svg')}}" alt="" class="img-fluid"></a>
          </li>
        </ul>
        @endif
    </div>
  </div>

  <div class="row">
    
  <div class="col-lg-12 col-xl-12">
      <ul class="d-flex bid-steps pl-0 mt-3 mt-md-5 scroll-container grey-scroll" style="    margin-top: 2.5rem!important;">
          <li class="active position-relative d-inline-block">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-purple">Created</p>
                <p class="mb-0 font-14 t-black">{{ date('d/m/Y',strtotime($booking['pickup_date'])) }}</p>
                <p class="mb-0 font-14 t-black">{{ $booking['pickup_time_from'] }}</p>
              </div>                               
            </div>
          </li>
          @if($booking['status'] == 'create')
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
          @endif
         @if($booking['status'] == 'pending')
          @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center">Waiting for Bid</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center">Waiting for Booking Request</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
         @endif
         @if($booking['status']  == 'in-progress')
         @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Bid Accepted</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Booking Accepted</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Driver:</p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Pick up from {{ $booking_locations[0]['company_name'] }}</p>
                <p class="mb-0 font-14 t-black">{{ date('d/m/Y',strtotime($booking['pickup_date'])) }}</p>
                <p class="mb-0 font-14 t-black">{{ $booking['pickup_time_from'] }}</p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">In-Transit</p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
         @endif
         @if($booking['status'] == 'completed')
          @if($booking['is_bid'] == 1)
              <li class="position-relative d-inline-block active">
                <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                  <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                  <div class="text-nowrap pt-2">
                    <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Waiting for Bid</p>
                  </div>
                </div>
              </li>
            @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Waiting for Booking Request</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Driver:</p>
              </div>
            </div>
          </li>
          @if(isset($booking_locations) && !empty($booking_locations))
            @foreach($booking_locations as $locations)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap text-left pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">@if($locations['is_pickup'] == 1)Picked Up from @else Delivered at @endif {{ $locations['company_name'] }}</p>
                  <p class="mb-0 font-14 t-black">{{ date('d/m/Y',strtotime($locations['delivered_at'])) }}</p>
                  <p class="mb-0 font-14 t-black">{{ $locations['delivery_time_from'] }}</p>
                </div>
              </div>
            </li>
            @endforeach
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Payment</p>
              </div>
            </div>
          </li>

         @endif
         @if($booking['status'] == 'cancelled')
          @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Waiting for Bid</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Waiting for Booking Request</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block cancelled">
            <div class="text-wrap text-center position-relative">
              <div class="icon border-50 border-cancelled d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Cancelled Shipment</p>   
              </div>
            </div>
          </li>
         @endif
       </ul>
    </div>
    <div class="col-md-6"></div>
  </div>
  @if(isset($booking_truck) && !empty($booking_truck))
    @for($i=0;$i<count($booking_truck);$i++)
    <div class="row">
      <div class="col-md-12">
        <div class="labels-wrapper d-flex align-items-center mb-6">
          <p class="mb-0 mr-2 font-18 font500 txt-blue">@if($i==0) Truck Details : @endif</p> 
          @if($i==0 && isset($booking_truck[$i]['note']) && !empty($booking_truck[$i]['note']))<p class="txt-purple mb-0 font-13"><a href="javascript:void(0)" class="note_section" data-note="{{ $booking_truck[$i]['note'] }}" style="color:#9B51E0 !important">Note</a></p>@endif
        </div>
        <div class="labels-wrapper d-flex align-items-center mb-2 scroll-container grey-scroll">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <p class="mt-0 mb-0 font-18  txt-blue text-nowrap" style="font-family: Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif; font-weight: 600;">@if(!empty($booking_truck[$i]['truck_type']['name']) && isset($booking_truck[$i]['truck_type']['name'])){{ $booking_truck[$i]['truck_type']['name'] }} @endif</p>
          @if(isset($booking_truck[$i]['truck_type_category']) && !empty($booking_truck[$i]['truck_type_category']))
          <!-- g_truck[$i]['truck_type_category'])) -->
          <ul class="list-unstyled d-flex mb-0 scroll-container grey-scroll">
          @foreach($booking_truck[$i]['truck_type_category'] as $truck_type_category)
            <div class="label-grey border-r30 p-2 font-14 txt-blue font400 text-nowrap" style="margin-left: 10px;">{{ $truck_type_category['name'] }}</div>
          @endforeach
        </ul>
          @endif
        </div>
      </div>
    </div>
    @endfor
  @endif
  @if(isset($booking_locations) && !empty($booking_locations))

    @if($booking['booking_type'] == 'direct_shipment')
        <div class="row mt-3">
          <div class="col-xl-10">
            <div class="labels-wrapper d-flex align-items-center mb-4">
              <p class="mb-0 mr-2 font-18 font500 txt-blue">Pickup/Delivery Location Details :</p> 
            </div>
            <div class="no-bids-track-order-list">
                <ul class="list-unstyled mb-0 pl-3">
                  <?php $last_address = end($booking_locations);?>
                  @for($i=0;$i<count($booking_locations);$i++)
                    <li @if($booking_locations[$i]['is_pickup']==1) class="completed from-location position-relative icon mb-4" @elseif(isset($last_address['id']) && !empty($last_address['id']) && $last_address['id'] == $booking_locations[$i]['id']) class="completed end-location position-relative icon mb-4" @else class="completed to-location position-relative icon mb-4" @endif>
                      <div class="row">
                        <div class="col-md-4">  
                          @if($booking_locations[$i]['is_pickup']==0 && $last_address['id'] != $booking_locations[$i]['id'])
                            <div class="icon border-50 border-grey d-flex align-items-center justify-content-center position-absolute"><div class="inner-icon border-50"></div></div>
                          @endif
                        <h4 class="mt-0 mb-1">{{ $booking_locations[$i]['company_name'] }} <small class="text-muted font-16">{{ date('F d, Y',strtotime($booking_locations[$i]['delivered_at'])) }}   @if($booking_locations[$i]['delivery_time_from'] == $booking_locations[$i]['delivery_time_to']){{ $booking_locations[$i]['delivery_time_from'] }} @else {{ $booking_locations[$i]['delivery_time_from'] }} - {{ $booking_locations[$i]['delivery_time_to'] }} @endif</small> </h4>
                          <p class="txt-black">{{ $booking_locations[$i]['drop_location'] }}</p>      
                          @if(isset($booking_locations[$i]['note']) && !empty($booking_locations[$i]['note']))
                            <p class="txt-purple mr-2 font-13"><a href="javascript:void(0)" class="note_section" data-note="{{ $booking_locations[$i]['note'] }}" style="color:#9B51E0 !important">Note</a></p> 
                          @endif      
                        </div>
                        <div class="col-md-1">
                          <small class="txt-blue font-16">@if($booking_locations[$i]['is_pickup']==1) Pickup @else Delivery @endif</small>
                        </div>
                        <div class="col-md-7">
                          @if(isset($booking_locations[$i]['products']) && !empty($booking_locations[$i]['products']))
                            @foreach($booking_locations[$i]['products'] as $products)
                            <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-2 txt-blue justify-content-center text-center">
                              <li>{{ $products['product_id']['name'] }} </li>
                              <li>{{ $products['product_type']['name'] }}</li>
                              <li>{{ $products['qty'] }}</li>
                              <li>{{ $products['weight'] }} {{ $products['unit']['name'] }}</li>
                              <li class="txt-red"> @if($products['is_fragile'] == 1) Fragile @endif </li>
                            </ul>
                            @endforeach
                          @endif
                          @if(isset($products['note']) && !empty($products['note'])) 
                            <p class="txt-purple mr-2 font-13" style="float: right;"><a href="javascript:void(0)" class="note_section" data-note="{{ $booking_locations[$i]['note'] }}" style="color:#9B51E0 !important">Product Description</a></p>
                          @endif
                        </div>
                      </div>
                    </li>
                  @endfor
                </ul>
            </div>
          </div>
        </div>
      @else
        <div class="row mt-3">
          <div class="col-xl-10">
            <div class="labels-wrapper d-flex align-items-center mb-4">
              <p class="mb-0 mr-2 font-18 font500 txt-blue">Pickup/Delivery Location Details :</p> 
            </div>
            <div class="no-bids-track-order-list">
                <ul class="list-unstyled mb-0 pl-3">
                  <?php $last_address = end($booking_locations);?>
                  @for($i=0;$i<count($booking_locations);$i++)
                    <li @if($booking_locations[$i]['is_pickup']==1) class="completed from-location position-relative icon mb-4" @elseif(isset($last_address['id']) && !empty($last_address['id']) && $last_address['id'] == $booking_locations[$i]['id']) class="completed end-location position-relative icon mb-4" @else class="completed to-location position-relative icon mb-4" @endif>
                      <div class="row">
                        <div class="col-md-4">  
                          @if($booking_locations[$i]['is_pickup']==0 && $last_address['id'] != $booking_locations[$i]['id'])
                            <div class="icon border-50 border-grey d-flex align-items-center justify-content-center position-absolute"><div class="inner-icon border-50"></div></div>
                          @endif
                        <h4 class="mt-0 mb-1">{{ $booking_locations[$i]['company_name'] }} <small class="text-muted font-16">{{ date('F d, Y',strtotime($booking_locations[$i]['delivered_at'])) }}   @if($booking_locations[$i]['delivery_time_from'] == $booking_locations[$i]['delivery_time_to']){{ $booking_locations[$i]['delivery_time_from'] }} @else {{ $booking_locations[$i]['delivery_time_from'] }} - {{ $booking_locations[$i]['delivery_time_to'] }} @endif</small> </h4>
                          <p class="txt-black">{{ $booking_locations[$i]['drop_location'] }}</p>   
                          @if(isset($booking_locations[$i]['note']) && !empty($booking_locations[$i]['note']))
                            <p class="txt-purple mr-2 font-13"><a href="javascript:void(0)" class="note_section" data-note="{{ $booking_locations[$i]['note'] }}" style="color:#9B51E0 !important">Note</a></p> 
                          @endif         
                        </div>
                        <div class="col-md-1">
                          @if(isset($booking_locations[$i]['products']) && !empty($booking_locations[$i]['products']))
                            @foreach($booking_locations[$i]['products'] as $products)
                            <ul class="d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">
                              <li>@if($products['is_pickup'] == '1') Pickup @else Delivery @endif</li>
                            </ul>
                            {{-- <small class="txt-blue font-16">@if($booking_locations[$i]['is_pickup']==1) Pickup @else Delivery @endif</small> --}}
                            @endforeach
                          @endif
                        </div>
                        <div class="col-md-7">
                          @if(isset($booking_locations[$i]['products']) && !empty($booking_locations[$i]['products']))
                            @foreach($booking_locations[$i]['products'] as $products)
                            <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-2 txt-blue justify-content-center text-center">
                              <li>{{ $products['product_id']['name'] }}</li>
                              <li>{{ $products['product_type']['name'] }}</li>
                              <li>{{ $products['qty'] }}</li>
                              <li>{{ $products['weight'] }} {{ $products['unit']['name'] }}</li>
                              <li class="txt-red"> @if($products['is_fragile'] == 1) Fragile @endif </li>
                            </ul>
                            @endforeach
                          @endif
                          @if(isset($products['note']) && !empty($products['note']))
                            <p class="txt-purple mr-2 font-13" style="float: right;word-break: break-all;"><a href="javascript:void(0)" class="note_section" data-note="{{ $products['note'] }}" style="color:#9B51E0 !important">Product Description</a></p> 
                          @endif
                        </div>
                      </div>
                    </li>
                  @endfor
                </ul>
            </div>
          </div>
        </div>
      @endif 
  @endif
  <div class="row">
    <div class="col-md-12 mt-3">
      <div class="long-haul-shipment"> 
       <div class="long-block">    
         <div class="popup d-flex align-items-center" style="margin-top: -46px;">
           <h4 class="txt-purple mr-2 font-15">{{ $booking['journey_type'] }}</h4>
           <a href="javascript:void(0)" class="btn-tooltip position-relative d-flex align-items-center txt-blue">i
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Long haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements greater than 350 to 400km from 
              origin to destination are generally classified as
               long-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Medium haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 100km to 300km from 
              origin to destination are generally classified as
               medium-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Short haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 0km to 100km from 
              origin to destination are generally classified as
               short-haul movements</span></a>
            @endif
            </a>
         </div>            
         <p class="mb-1 font-16 txt-blue font-weight-light">Total Kilometers: <em class="font-weight-bold">{{ $booking['distance'] }}</em></p>
         <p class="mb-1 font-16 txt-blue font-weight-light">Journey: <em class="font-weight-bold">{{ $booking['journey'] }}</em></p>
       </div>
      
      </div>
      @if(isset($booking['amount']) && !empty($booking['amount']))
      <div class="no-bid-quote mt-3">
        <p class="d-flex align-items-center txt-blue font-18">Quote :  <strong>&euro; {{ CommonHelper::price_format($booking['amount'],'GR') }}</strong> 
          {{-- <a href="javascript:;" class="pl-2"><img src="http://13.36.112.48/public/assets/images/shipment/pen-black.svg" class="img-fluid"></a> --}}
        </p>
      </div>
      @endif

      @if(isset($driver_details) && !empty($driver_details))
      <!-- <div class="no-bid-quote mt-3">
        <p class="d-flex align-items-center txt-blue font-18">Quote1 :  <strong>&euro; {{ $booking['amount'] }}</strong> 
          {{-- <a href="javascript:;" class="pl-2"><img src="http://13.36.112.48/public/assets/images/shipment/pen-black.svg" class="img-fluid"></a> --}}
        </p>
      </div> -->


      <div class="truck-box bg-white p-2 mt-3">
       <div class="row align-items-center">
         <div class="col-md-1 tr-br">
          <p class="mb-0 font-14 txt-blue font-weight-light"><em class="font-weight-bold">Truck Details</em><br> for booking</p>
         </div>
         <div class="col-md-11">
           <?php $last_key = array_key_last($booking_locations);?>
           <div class="row">
             <div class="col-md-2">
               <p class="txt-purple mb-0">Truck Type</p><br>
               <p class="mb-0 font-14 txt-blue font-weight-light">{{ $driver_truck['name'] }}</p>
               @if(isset($driver_vehicle_details['TruckSubCategory']['name']) && !empty($driver_vehicle_details['TruckSubCategory']['name']))<p class="mb-0 font-13 txt-blue font-weight-light txt-purple">{{ $driver_vehicle_details['TruckSubCategory']['name'] }}</p> @endif
             </div>
             <div class="col-md-2">
              <p class="txt-purple mb-0">Start Date</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light">{{ date('F d, Y',strtotime($booking['pickup_date'])) }} <br> @if($booking['pickup_time_from'] != $booking['pickup_time_to']) {{$booking['pickup_time_from']}} - {{  $booking['pickup_time_to'] }} @else {{$booking['pickup_time_from']}} @endif</p>
             </div>
             <div class="col-md-2">
              <p class="txt-purple mb-0">Start Location</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light" title="{{ $booking['pickup_address'] }}">{{ (strlen($booking['pickup_address']) > 35 ? substr($booking['pickup_address'],0,35).'...' : $booking['pickup_address']) }}</p>
             </div>
             <div class="col-md-2">
              <p class="txt-purple mb-0">End Location</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light" title="{{ $booking_locations[$last_key]['drop_location'] }}">{{ (strlen($booking_locations[$last_key]['drop_location']) > 35 ? substr($booking_locations[$last_key]['drop_location'],0,35).'...' : $booking_locations[$last_key]['drop_location']) }}</p>
             </div>
             <div class="col-md-2">
              <p class="txt-purple mb-0">Carrier Info</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light">{{ $driver_details['name'] }}</p>
             </div>
             <div class="col-md-2">
              <p class="txt-purple mb-0"></p><br>
               <form action="{{ url('shipper/search-available-trucks/confirm-booking') }}" method="POST">
                @csrf
                <input type="hidden" name="booking_id" value="{{ $booking['id'] }}">
                <input type="hidden" name="availability_id" value="{{ $availability_id }}">
                 <button type="submit" class="btn btn-primary btn-sm mr-2 mt-0"><span class="text ml-1">Booking Request</span></button>
               </form>
             </div>
           </div>
         </div>
       </div>
      </div>
      @endif
    
      
    </div>
  </div>
</div>

<!-- Cancel Shipment -->
{{-- <div class="modal fade cancel-shipment" id="cancel-shipment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content border-r35">
      <div class="modal-body p-4">
        <p class="font-18 font600 txt-blue text-center">Do you want to continue to <em class="txt-red">cancel</em> shipment?</p>
        <p class="txt-red text-center"><strong>Warning:</strong> Cancellation fee and possible bad reviews to the account</p>
        <div class="btn-block d-flex mt-3">
            <button type="button" class="btn w-100 btn-white mr-2">Yes</button>
            <button type="submit" class="btn w-100 btn-primary"  data-dismiss="modal" aria-label="Close">No</button>
        </div>
        
      </div>
    </div>
  </div>
</div> --}}

<!-- Note Details Shipment -->
<div class="modal fade note-shipment" id="note-shipment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content border-r35">
      <div class="modal-body p-4">
        <p class="font-18 font600 txt-blue text-center shipment_note" style="margin-bottom:0px "></p>        
      </div>
    </div>
  </div>
</div>

<div class="modal fade manage-order-shipment-view-address p-2" id="view-map" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal1250" role="document">
    <div class="modal-content border-r20">
      <div class="modal-body p-3">
        <div class="row">
          <div class="col-md-12">
          <input type="hidden" id="longitud" value="">
      <input type="hidden" id="latitud" value="">
            <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;position: relative; overflow: hidden;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script>

function open_map(location_data)
  {
    var map;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    if(location_data != ''){
      var locations = location_data;
    }
    
    directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(location_data[0][1],location_data[0][2]),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

    directionsDisplay.setMap(map);

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else
            alert ('failed to get directions');
    });

    $('#view-map').modal('show');
  }

  function delete_notiflix(e) {
    
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var url = $(e).data('url');
    var u1 = $(e).data('newurl');

    var product = $('#productid').val();

    console.log(token);
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to delete this record?',
        'Yes',
        'No',
        function() {
            Notiflix.Loading.Standard();
            $.ajax({
                url: base_url + '/' + u1 + '/' + product,
                
                type: 'post',
                dataType: "JSON",
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function(returnData) {
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Deleted');
                    tr.remove();
                    $("#edit_model").modal("hide");
                    location.reload();

                }
            });
        });

}
$(document).ready(function(){
  $(document).on('click','.note_section',function () {
    console.log('test');
    var note = $(this).attr('data-note');
    if(note != ''){
      $('.shipment_note').text(note);
      $('#note-shipment').modal('show');
    }
  })
  $(document).on('click','.bid_accept',function(){
    var booking_id = $(this).attr('data-booking-id');
    var id = $(this).attr('data-id');
    var driver_id = $(this).attr('data-driver-id');
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to accept this driver bid request?',
        'Yes',
        'No',
        function() {
          Notiflix.Loading.Standard();
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
              url: "{{ url('shipper/manage-shipment/accept_bid') }}",
              type: 'post',
              dataType: "JSON",
              data: {
                  "id": id,
                  "booking_id": booking_id,
                  "driver_id": driver_id,
              },
              success: function(returnData) {
                  // Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Shipment assign successfully');
                  // tr.remove();
                  // $("#edit_model").modal("hide");
                  location.reload();
              }
          });
      });
  })

  $(document).on('click','.open_map',function(){
      var booking_id = $(this).attr('data-id');
      $.ajax({
        url: "{{ url('shipper/manage-shipment/load-map') }}",
        type: 'post',
        dataType: "JSON",
        data: {
            "id": booking_id,
            "_method": 'POST',
            "_token": $('name[csrf-token]').attr('content'),
        },

        success: function(data) {
          // console.log(data[0][1])
            open_map(data);
        }
    });
  });

  $(document).on('click','.cancel_shipment',function () {
    var booking_id = $(this).attr('data-id');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to remove this shipment?',
      'Yes',
      'No',
      function() {
        Notiflix.Loading.Standard();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
            url: "{{ route('shipper.manage-shipment.delete',$booking['id']) }}",
            type: 'DELETE',
            dataType: "JSON",
            data: {
                "booking_id": booking_id,
            },
            success: function(returnData) {
                // Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Shipment remove successfully');
                // tr.remove();
                // $("#edit_model").modal("hide");
                window.location.href = "{{ url('shipper/manage-shipment') }}";
            }
        });
    });
  });

  $(document).on('click','.search_available_truck',function() {
    $('#search_available_truck').show();
  })
});
</script>
@endsection