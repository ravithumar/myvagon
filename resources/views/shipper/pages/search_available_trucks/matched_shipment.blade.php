@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.sorting-btn{
    border-radius: 20px;
    /* margin: 0px 10px; */
    padding: 2px 20px;
}
.sort-button .active{
  background-color: #eadafd;
  color: #9B51E0 !important;
  -webkit-box-shadow : none;
  box-shadow : none;
}
.table td {
  border-left: none;
  border-right: none;
}
.table th {
  border-left: none;
  border-right: none;
}

.radio-block .radio.active {
    background-color: rgba(155,81,224,0.12);
    border-radius: 34px;
}
.radio-block .radio {
    padding: 15px 40px;
    line-height: normal;
}
.radio-block .radio label::before {
    width: 20px;
    height: 20px;
    border: 1px solid #707070;
}
.radio-block .radio label::after {
    background-color: #9B51E0;
    width: 14px;
    height: 14px;
    left: 5px;
    top: 3px;
}
.radio label {
    padding-left: 25px;
    font-size: 14px;
    font-weight: 600;
}
.panding-status {
    background-color: rgba(213,105,105,0.14) !important;
    border: 1px solid #D56969;
    border-radius: 10px;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:void(0);" class="d-flex align-items-center txt-blue"><img src="{{ URL::asset('shipper/images/search.svg')}}" class="img-fluid">{{ $dateTableTitle }}</a></li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">Bid</li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">Matched Shipments</li>
            </ol>
        </nav>
    </div>
  </div>
  @if(isset($driver_detail) && !empty($driver_detail) && isset($truck_detail) && !empty($truck_detail))
    <div class="row mt-2 mb-2">
      <div class="col-md-12">
        <div class="truck-box bg-white p-2">
          <div class="row align-items-center">
            <div class="col-md-1 tr-br">
              <p class="mb-0 font-14 txt-blue font-weight-light"><em class="font-weight-bold">Available Truck</em><br> for booking  </p>
            </div>
            <div class="col-md-2">
              <p class="txt-purple mb-0">Truck Type</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($truck_detail['truck_type']['name']) && !empty($truck_detail['truck_type']['name'])){{ $truck_detail['truck_type']['name'] }}@endif</p>
            </div>
            <div class="col-md-2">
              <p class="txt-purple mb-0">Start Date</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($availability_data['date']) && !empty($availability_data['date'])){{ date('F d, Y',strtotime($availability_data['date'])) }}@endif</p>
            </div>
            <div class="col-md-2">
              <p class="txt-purple mb-0">Start Location</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($availability_data['from_address']) && !empty($availability_data['from_address'])) {{ (strlen($availability_data['from_address']) > 35 ? substr($availability_data['from_address'],0,35).'...' : $availability_data['from_address']) }} @endif</p>
            </div>
            <div class="col-md-2">
              <p class="txt-purple mb-0">End Location</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($availability_data['to_address']) && !empty($availability_data['to_address'])) {{ (strlen($availability_data['to_address']) > 35 ? substr($availability_data['to_address'],0,35).'...' : $availability_data['to_address']) }} @endif</p>
            </div>
            <div class="col-md-2">
              <p class="txt-purple mb-0">Carrier Info</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($driver_detail['name']) && !empty($driver_detail['name'])) {{ $driver_detail['name'] }} @endif</p>
            </div>
            <div class="col-md-1">
              <p class="txt-purple mb-0">Bid Price</p><br>
              <p class="mb-0 font-14 txt-blue font-weight-light">€ @if(isset($availability_data['bid_amount']) && !empty($availability_data['bid_amount'])) {{ CommonHelper::price_format($availability_data['bid_amount'],'GR') }} @else {{ CommonHelper::price_format('0','GR') }} @endif</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endif
  <div class="row d-flex align-items-center products">
    <div class="col-md-6">
      <h3>Matched Shipments</h3>
    </div>
    <div class="col-md-6">
      
    </div>
  </div>
  @include('admin.include.flash-message')

  <div class="row">
    
    <div class="col-xl-12">
      <div class="card ">
        <div class="card-body table-responsive" >
          <table id="{{$dataTableId}}" class="table table-bordered table-striped">
            <thead>
              <tr>
                @foreach ($dateTableFields as $field)
                <th>
                  {{ $field['title'] }}
                </th>
                @endforeach
              </tr>
            </thead>
            <tbody>
              @if(isset($available_trucks['booking_data']) && !empty($available_trucks['booking_data']))
              @foreach($available_trucks['booking_data'] as $trucks)
              
              <?php $last_key = array_key_last($trucks['booking_location']->toArray());?>
                <tr>
                  <td>{{ $trucks['id'] }}</td>
                  <td>€ {{ CommonHelper::price_format($trucks['amount'],'GR') }}</td>
                  <td>
                    @if($trucks['status'] == 'create' || $trucks['status'] == 'pending')
                      <label class="panding-status" style="padding: 5px 10px;margin-bottom:0px">Pending</label><br>
                      <small class="text-danger">Waiting for truck</small>
                    @endif
                  </td>
                  <td>{{ date('M d,Y',strtotime($trucks['booking_location'][0]['delivered_at'])) }}</td>
                  <td>{{ date('M d,Y',strtotime($trucks['booking_location'][$last_key]['delivered_at'])) }}</td>
                  {{-- <td>{{ $trucks['booking_location'][$last_key]['drop_location'] }}</td> --}}
                  <td>{{ (strlen($trucks['booking_location'][$last_key]['drop_location']) > 35 ? substr($trucks['booking_location'][$last_key]['drop_location'],0,35).'...' : $trucks['booking_location'][$last_key]['drop_location']) }}</td>
                  {{-- <td></td> --}}
                  {{-- <td>€ {{ $trucks['bid_amount'] }}</td> --}}
                  {{-- <td>@if($trucks['is_bid'] == '1') <a href="javascript:void(0)" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" data-id="{{ $trucks['id'] }}" class=" btn btn-xs text-default justify-content-end bid_request_click"> Bid</a> @else <a href="{{ url('shipper/search-available-trucks/book-now/'.$trucks['booking']['id'].'/'.$trucks['id']) }}" style="color: #b27be7;font-weight:600;border: 1px solid #b27be7;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> Book</a> @endif</td> --}}
                  <td>@if($trucks['is_bid'] == '1') <a href="{{ url('shipper/search-available-trucks/bid-now/'.$trucks['id'].'/'.$available_trucks['id']) }}" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" data-id="{{ $trucks['id'] }}" data-booking-count="{{ $trucks['booking_match'] }}" class=" btn btn-xs text-default justify-content-end"> Accept</a> @else <a href="{{ url('shipper/search-available-trucks/book-now/'.$trucks['id'].'/'.$available_trucks['id']) }}" style="color: #b27be7;font-weight:600;border: 1px solid #b27be7;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> Book</a> @endif</td>
                  {{-- <td></td> --}}
                </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade cancel-shipment" id="bid-request" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content border-r35">
      <div class="modal-body p-4">
        <form action="{{ url('shipper/search-available-trucks/bidding') }}" name="bid_request_form" method="post" id="bid_request_form">
          @csrf
          <input type="hidden" name="availability_id" value="">
          <p class="font-18 font600 txt-blue text-center">Proceed with</p>
          <input type="hidden" name="availability_id" value="">
          <div class="radio-block">
            <div class="radio create_class active mb-3  text-left">
                 <input type="radio" name="bid_request" id="bid_request1" value="create_new" checked>
                 <label for="bid_request1"> Create New Shipment</label>
            </div>
            <div class="radio existing_class mb-3  text-left">
                <input type="radio" name="bid_request" id="bid_request2" value="show_existing">
                <label for="bid_request2"><b class="booking_match_count"></b> Matched Shipments Found</label>
            </div>
          </div>
          <div class="btn-block d-flex mt-3 text-center">
              <button type="submit" class="btn w-100 btn-primary" style="border-radius: 15px">Continue</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script>
  $(document).ready(function(){
    var table = $('#{{$dataTableId}}').DataTable({
      processing: true,
      responsive:true,
      searching:true,
      lengthChange:true,
      bInfo:true,
      "oLanguage": {
        "sEmptyTable": "No shipment available for this driver."
      }
    });

    $(document).on('change','input[name=bid_request]',function () {
      var value = $(this).val();
      if(value == 'create_new'){
        $(this).closest('.radio-block').find('.create_class').addClass('active');
        $(this).closest('.radio-block').find('.existing_class').removeClass('active');
      }else{
        $(this).closest('.radio-block').find('.create_class').removeClass('active');
        $(this).closest('.radio-block').find('.existing_class').addClass('active');
      }
    });

    // $(document).on('submit','#bid_request_form',function () {
    //   var request_page = $('input[name=bid_request]:checked').val();
    //   if(request_page == 'create_new'){
    //     $('#bid_request_form').attr('action', "{{ url('shipper/bid-create-shipment') }}");
    //   }else{

    //   }
    //   console.log(form_data);
    // })

    // $(document).on('submit','#bid_request_form',function () {
    //   var request_page = $('input[name=bid_request]:checked').val();
    //   var availability_id = $('input[name=availability_id]').val();
    //   if(request_page == 'create_new'){
    //     $('#bid_request_form').attr('action', "{{ url('shipper/bid-create-shipment') }}").submit();
    //   }else{
    //     window.location.href = "{{ url('shipper/bidding-shipment') }}";
    //   }
    //   // console.log(form_data);
    // })

    $(document).on('click','.bid_request_click',function () {
      var availability_id = $(this).attr('data-id');
      var booking_match = $(this).attr('data-booking-count');
      $('input[name=availability_id]').val(availability_id);
      $('.booking_match_count').text(booking_match);
      $('#bid-request').modal('show');
    })
  })
</script>
@endsection