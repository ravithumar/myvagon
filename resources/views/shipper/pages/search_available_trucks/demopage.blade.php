
@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.sorting-btn{
    border-radius: 20px;
    /* margin: 0px 10px; */
    padding: 2px 20px;
}
.sort-button .active{
  background-color: #eadafd;
  color: #9B51E0 !important;
  -webkit-box-shadow : none;
  box-shadow : none;
}
.table td {
  border-left: none;
  border-right: none;
}
.table th {
  border-left: none;
  border-right: none;
}

.radio-block .radio.active {
    background-color: rgba(155,81,224,0.12);
    border-radius: 34px;
}
.radio-block .radio {
    padding: 15px 40px;
    line-height: normal;
}
.radio-block .radio label::before {
    /*width: 20px;*/
    height: 20px;
    border: 1px solid #707070;
}
.radio-block .radio label::after {
    background-color: #9B51E0;
    width: 14px;
    height: 14px;
    left: 5px;
    top: 3px;
}
.radio label {
    padding-left: 25px;
    font-size: 14px;
    font-weight: 600;
}
.table thead th {
    vertical-align: middle;
    border-bottom: 2px solid #dee2e6;
}
input.select2-search__field {
    /*width: 185px;*/
}

</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ URL::asset('shipper/images/search.svg')}}" class="img-fluid">{{ $dateTableTitle }}</a></li>
            </ol>
        </nav>
    </div>
  </div>
  
  @include('admin.include.flash-message')

  <div class="row">
    <div class="col-xl-12">
      <div class="card ">
        <div class="card-body table-responsive">
          <table id="dataTableId" class="table table-bordered table-striped">
            <thead>
              <!-- <tr>
                @foreach ($dateTableFields as $field)
                <th>
                  {{ $field['title'] }}
                @endforeach
                  

                </th>
                
              </tr> -->

              <tr>
                <th>S.No.</th>
                <th>Truck Type
                    <a href="javascript:void(0);" class="txt-blue" data-toggle="modal" data-target="#truck-type" > 
                      <!-- <select name="address" required placeholder="Any" class="form-control" style="width: 110px; border-radius: 10px;">
                        <option hidden></option>
                      </select> -->
                      <input type="text" required class="form-control largerCheckbox checkmark" style="width: 110px; border-radius: 10px;" id="selected_truck_name" autocomplete="off">
                    </a>
                    <input type="hidden" id="selected_truck_type">
                </th>
                <th>Start Date 
                    <input type="date" id="start-date" class="form-control " name="pickup_date" required style=" width: 100%; border-radius: 10px;" placeholder="YYYY-MM-DD">
                </th>
                <th>Start Location
                    <a href="javascript:void(0);" class="txt-blue" data-toggle="modal" data-target="#start-filter-location" id="start-location"> 
                      <input type="text" name="address" required class="form-control fulladdress" style="width: 110px; border-radius: 10px;" autocomplete="off" id="start_address">
                    </a>
                </th>
                <th>End Location
                    <a href="javascript:void(0);" class="txt-blue" data-toggle="modal" data-target="#end-filter-location"> 
                      <input type="text"  name="address" required class="form-control" style="width: 110px; border-radius: 10px;" autocomplete="off" id="end_address">
                    </a>
                </th>
                <th>Carrier Info</th>
                <th>Price
                <a href="javascript:void(0);" class="txt-blue" data-toggle="modal" data-target="#price_filter">
                  <input type="text" required class="form-control largerCheckbox checkmark" style="width: 110px; border-radius: 10px;" autocomplete="off" id="selected_price">
                </a>
                </th>
                <th>Function</th>
              </tr>
              
            </thead>
            <tbody>
              @if(isset($available_trucks) && !empty($available_trucks))
              @foreach($available_trucks as $trucks)
                <tr>
                  <td>{{ $trucks['id'] }}</td>
                  <td>{{ $trucks['trucks_type']['name'] }}</td>
                  <td>{{ date('F d, Y',strtotime($trucks['date'])) }}</td>
                  <td>{{ $trucks['from_address'] }}
                  <td>{{ $trucks['to_address'] }}
                  <td>{{ $trucks['driver_info']['name'] }}</td>
                  {{-- <td></td> --}}
                  <td>€ {{ CommonHelper::price_format($trucks['bid_amount'],'GR') }}</td>
                  {{-- <td>@if($trucks['is_bid'] == '1') <a href="javascript:void(0)" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" data-id="{{ $trucks['id'] }}" class=" btn btn-xs text-default justify-content-end bid_request_click"> Bid</a> @else <a href="{{ url('shipper/search-available-trucks/book-now/'.$trucks['booking']['id'].'/'.$trucks['id']) }}" style="color: #b27be7;font-weight:600;border: 1px solid #b27be7;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> Book</a> @endif</td> --}}
                  <td>@if($trucks['is_bid'] == '1') <a href="javascript:void(0)" style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" data-id="{{ $trucks['id'] }}" data-booking-count="{{ $trucks['booking_match'] }}" class=" btn btn-xs text-default justify-content-end bid_request_click"> Bid</a> @else <a href="javascript:void(0)" style="color: #b27be7;font-weight:600;border: 1px solid #b27be7;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> Book</a> @endif</td>
                </tr>
                
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- view address -->
<div class="modal fade manage-order-shipment-view-address p-2" id="view-map" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal1250" role="document">
    <div class="modal-content border-r20">
      <div class="modal-body p-3">
        <div class="row">
          <div class="col-md-12">
          <input type="hidden" id="longitud" value="">
      <input type="hidden" id="latitud" value="">
            <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;position: relative; overflow: hidden;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade cancel-shipment" id="bid-request" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content border-r35">
      <div class="modal-body p-4">
        <form action="javascript:void(0)" name="bid_request_form" id="bid_request_form">
          <p class="font-18 font600 txt-blue text-center">Proceed with</p>
          <input type="hidden" name="availability_id" value="">
          <div class="radio-block">      
            <div class="radio create_class active mb-3  text-left">
                 <input type="radio" name="bid_request" id="bid_request1" value="create_new" checked>
                 <label for="bid_request1"> Create New Shipment</label>
            </div>
            <div class="radio existing_class mb-3  text-left">
                <input type="radio" name="bid_request" id="bid_request2" value="show_existing">
                <label for="bid_request2">10 Matched Shipments Found</label>
            </div>
          </div>
          <div class="btn-block d-flex mt-3 text-center">
              <button type="submit" class="btn w-100 btn-primary" style="border-radius: 15px">Continue</button>
          </div>
        </form>        
      </div>
    </div>
  </div>
</div>
<!-- Start Filter Location-->
<div class="modal fade" id="start-filter-location" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content" style="border-radius: 30px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body" style="bottom: 114px;margin: 92px;height: 239px;">
          <div class="col-md-12">
            <h3>Filter Source Location</h3>
          </div>
          <form action="javascript:void(0)" method="POST" id="search_start_location" style="margin-bottom: 1.5rem;">
            @csrf
            <div class="col-md-13" style="margin-bottom: 1.5rem;">
              <div class="form-group mb-3">
                  <input type="text" id="autocomplete" name="search" autocomplete="off" parsley-trigger="change" required class="autocomplate-input form-control form-control-without-border fulladdress"  placeholder="Search..." style="">
                  <input type="hidden" id="start_latitude" name="lat" value="">
                  <input type="hidden" id="start_longitude" name="lng" value="">
                  @if($errors->has('lat'))
                      <div class="error text-danger">{{ $errors->first('lat') }}</div>
                    @elseif($errors->has('address'))
                      <div class="error text-danger" style="margin-top: -50px;">{{ $errors->first('address') }}</div>
                  @endif
              </div>
           
            </div>
              
                <div class="row">
                  <div class="col-md-10">
                    <h5>Radius around the source location</h5>
                  </div>
                </div>
                <div class="row" style="align-items: center; ">
                  <div class="col-md-12">
                      <div class="form-group checkbox-inline">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="short_haul_shipment" id="short_haul_shipment" ><span></span></label>
                        <label for="short_haul_shipment" class="font-15">Short Haul (up to 100 km)</label>
                      </div>
                      <hr/>
                      <div class="form-group">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="medium_haul_shipment" id="medium_haul_shipment"><span></span></label>
                        <label for="medium_haul_shipment" class="font-15">Medium Haul (100 km to 300 km)</label>
                      </div>
                      <hr/>
                      <div class="form-group">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="long_haul_shipment" id="long_haul_shipment"><span></span></label>
                        <label for="long_haul_shipment" class="font-15">Long Haul (Over 300 km) </label>
                      </div>
                  </div>
                  <div class="col-md-6">
                        <button type="submit" class="btn w-100 btn-primary" style="border-radius: 20px;" id="autocomplete_search">Apply Filter</button>
                  </div>
                  <div class="col-md-6">
                      <button type="clear" class="btn w-100 btn-primary" style="border-radius: 20px;" id="btnFiterClearSearch" >Clear Filter</button>
                  </div>
                  </div>
                </div>
              </form>
        </div>
      </div>
    </div>
</div>
<!-- Price -->
<div class="modal fade" id="price_filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="border-radius: 30px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <form action="javascript:void(0)" method="POST" id="price" style="margin-bottom: 1.5rem;">
        <div class="modal-body" >
          <div class="col-md-12">
            <div class="form-group checkbox-inline">
              <div id="bid_amount" style="padding-left: 136px;margin-bottom: 20px;font-weight: 600;">
                <span>Price: </span><br>
                <span>€ <input type="text" name="Any" placeholder="Any"  id="min_amount" style="border-radius: 6px;" required="" autocomplete="off" value=""> <input type="hiddden" name="Any" placeholder="Any"  id="get_amount" hidden="" autocomplete="off" value=""></span><br>
                 
                <span>Between</span><br/>
                <span>€  <input  type="text" name="Any"  placeholder="Any"   id="max_amount" style="border-radius: 6px;" required="" autocomplete="off"></span>
              
              </div>
              <div class="col-md-6">
                <button type="submit" class="btn  btn-primary" style="border-radius: 20px;width: 113px;margin-left: 110px;" id="price_search">Apply Filter</button>
              </div>
              <div class="col-md-6">
                  <button type="clear" class="btn  btn-primary" style="border-radius: 20px;margin-bottom: -px;width: 113px;margin-left: 242px;margin-top: -60px;" id="PriceClearSearch" >Clear Filter</button>
              </div>
            
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

      
<!-- End Filter Location-->
<div class="modal fade" id="end-filter-location" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="border-radius: 30px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body" style="bottom: 114px;margin: 92px;height: 239px;">
        <div class="col-md-12">
          <h3>Filter Source Location</h3>
        </div>
        <form action="javascript:void(0)" method="POST" id="search_end_location" style="margin-bottom: 1.5rem;">
          @csrf
          <div class="col-md-13" style="margin-bottom: 1.5rem;">
            <div class="form-group mb-3">
                <input type="text" id="autocomplete1" name="search" autocomplete="off" parsley-trigger="change" required class="autocomplate-input form-control form-control-without-border fulladdress"  placeholder="Search..." style="">
                <input type="hidden" id="end_latitude" name="lat" value="">
                <input type="hidden" id="end_longitude" name="lng" value="">
                @if($errors->has('lat'))
                  <div class="error text-danger">{{ $errors->first('lat') }}</div>
                @elseif($errors->has('address'))
                  <div class="error text-danger" style="margin-top: -50px;">{{ $errors->first('address') }}</div>
                @endif
            </div>
          </div>  
          <div class="row">
            <div class="col-md-10 ">
              <h5>Radius around the source location</h5>
            </div>
          </div>
          <div class="row" style="align-items: center; ">
            <div class="col-md-12">
                <div class="form-group checkbox-inline">
                  <label><input type="checkbox" value="1" class="largerCheckbox" name="short_haul_shipment" id="short_haul_shipment"><span></span></label>
                  <label for="short_haul_shipment" class="font-15">Short Haul (up to 100 km)</label>
                </div>
                <hr/>
                <div class="form-group">
                  <label><input type="checkbox" value="1" class="largerCheckbox" name="medium_haul_shipment" id="medium_haul_shipment"><span></span></label>
                  <label for="medium_haul_shipment" class="font-15">Medium Haul (100 km to 300 km)</label>
                </div>
                <hr/>
                <div class="form-group">
                  <label><input type="checkbox" value="1" class="largerCheckbox" name="long_haul_shipment" id="long_haul_shipment"><span></span></label>
                  <label for="long_haul_shipment" class="font-15">Long Haul (Over 300 km) </label>
                </div>
            </div>
            <div class="col-md-6">
                  <button type="submit" class="btn w-100 btn-primary" style="border-radius: 20px;width: 100px;" id="autocomplete_search">Apply Filter</button>
            </div>
            <div class="col-md-6">
                  <button type="clear" class="btn w-100 btn-primary" style="border-radius: 20px; " id="btnFiterClearSearch" >Clear Filter</button>
            </div>
          </div>
        </div>
            </form>
      </div>
    </div>
  </div>
</div>

<!-- Truck type -->
<div class="modal fade bd-example-modal-lg" id="truck-type" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 30px; ">
      <div class="modal-header">
      <div class="row mx-auto" >
          <h3 style="Text-Align:center;margin-left: 201px;">Truck Type</h3>
        </div>
        <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body" style="border-radius: 30px;margin-top: -44px;">
       
        <div class="col-md-4 col-lg-5"></div>
        <div class="truck-category mt-4"  style="background-color: white; padding-top: 2.25rem;padding-bottom: 1rem;padding-left:1rem;padding-right:1rem;">
          @foreach($truckDetails as $key => $truck)
            <div class="checkmark mb-4 mb-lg-5">
              <div class="form-group d-flex align-items-center" style="margin-bottom: -24px;"> 
                <div class="label-wrapper">
                  <label><input type="checkbox" id="truck_type_{{ $truck['id'] }}" class="largerCheckbox" name="truck_type[]" onclick="validateInput(this)" value="{{ $truck['id'] }}" data-name="{{ $truck['name'] }}"><span></span></label><br>
                  <label for="Semi-trailer Truck"></label>
                </div>
                @php
                  $src = 'images/type/'.$truck['icon'];
                  if($key < $count){
                    $style = 'border-bottom: 1px solid silver';
                  }else{
                    $style = '';
                  }
                @endphp
                <div class="text-wrapper d-flex align-items-center pl-3 pl-lg-4">
                  <span class="wrap-both d-block">
                    <img src="{{ asset($src)}}" class="img-fluid" width="150" style="object-fit: scale-down;" height="120">
                    <span class="text d-block mt-2 txt-blue">{{ $truck['name'] }}</span>
                  </span>
                </div>
                <div class="sub-category-page pl-3" style="width: 75%">
                  <select class="form-control select2-multiple select2-hidden-accessible my-select" id="truck_categories_{{ $truck['id'] }}" name="truck_categories[{{ $truck['id'] }}][]" multiple="multiple" placeholder="Please Select Truck Type" data-parsley-required-message="Please select any subcategory" style="width: 185px;">
                    @foreach ($truck['category'] as $truck_cat)
                      <option value="{{ $truck_cat['id'] }}" data-badge="" style="width: 185px;">{{ $truck_cat['name'] }}</option>
                    @endforeach
                  </select>
                  <p class="text-danger" id="errorShow_{{ $truck['id'] }}" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> Please select any subcategory</p>
                </div>
              </div>
            </div>
          @endforeach
          <p class="text-danger" id="errorShow" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> Please select any truck type</p>
        </div>
        <div class="col-md-4 center-block">
          <button type="submit" class="btn btn-primary center-block" style="border-radius: 20px; width: 200px;" id="btnFiterSubmitSearch" >Apply Filter</button>
          
        </div>
        <div class="col-md-4">
        <button type="clear" class="btn btn-primary center-block" style="border-radius: 20px; width: 200px;margin-left: 214px;margin-top: -62px;" id="btnFiterClearSearch" onclick="resetForm();">Clear Filter</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/js/pages/form-pickers.init.js')}}"></script>

<script type="text/javascript">

  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

  function filter(start_date,min_amount,max_amount,truck_type,start_location,end_location){
    $.ajax({
        type : 'post',
        url : '{{url("shipper/search-available-trucks/filter")}}',
        data:{'start_date':start_date,'min_amount':min_amount,'max_amount':max_amount,'truck_type':truck_type,'start_location':start_location,'end_location':end_location},
          success:function(data){
            $('#dataTableId tbody').html(JSON.parse(data));
          }
      });
  }
  $(document).ready(function () {
    $('#start-date').flatpickr({minDate: "today",disableMobile: "true",});
    $('.my-select').trigger("change");
    
    $(document).on('change','#start-date',function () {
      var start_date = $('#start-date').val();
      var min_amount = $('#min_amount').val();
      var max_amount = $('#max_amount').val();
      
      var truck_type = $('#selected_truck_type').val();
        var start_lat = $('#start_latitude').val();
      var start_lng = $('#start_longitude').val();
      var short_haul_shipment = $('#search_start_location input[name=short_haul_shipment]:checked').val();
      if(short_haul_shipment == 1){
        short_haul_shipment = 'Short haul shipment'
      }
      var medium_haul_shipment = $('#search_start_location input[name=medium_haul_shipment]:checked').val();
      if(medium_haul_shipment == 1){
        medium_haul_shipment = 'Medium haul shipment'
      }
      var long_haul_shipment = $('#search_start_location input[name=long_haul_shipment]:checked').val();
      if(long_haul_shipment == 1){
        long_haul_shipment = 'Long haul shipment'
      }

      var final_array_start_location = [start_lat,start_lng,short_haul_shipment,medium_haul_shipment,long_haul_shipment];

      var end_lat = $('#end_latitude').val();
      var end_lng = $('#end_longitude').val();
      var end_short_haul_shipment = $('#search_end_location input[name=short_haul_shipment]:checked').val();
      if(end_short_haul_shipment == 1){
        end_short_haul_shipment = 'Short haul shipment'
      }
      var end_medium_haul_shipment = $('#search_end_location input[name=medium_haul_shipment]:checked').val();
      if(end_medium_haul_shipment == 1){
        end_medium_haul_shipment = 'Medium haul shipment'
      }
      var end_long_haul_shipment = $('#search_end_location input[name=long_haul_shipment]:checked').val();
      if(end_long_haul_shipment == 1){
        end_long_haul_shipment = 'Long haul shipment'
      }
      var final_array_end_location = [end_lat,end_lng,end_short_haul_shipment,end_medium_haul_shipment,end_long_haul_shipment];

      filter(start_date,min_amount,max_amount,truck_type,final_array_start_location,final_array_end_location);
    })

    /* $(document).on('keyup','#min_amount',function () {
      var start_date = $('#start-date').val();
      var min_amount = $('#min_amount').val();
      var max_amount = $('#max_amount').val();
      
      var truck_type = $('#selected_truck_type').val();
        var start_lat = $('#start_latitude').val();
      var start_lng = $('#start_longitude').val();
      var get_amount = document.getElementById("min_amount");
      var store_amount = get_amount.value;
      var price_val = document.getElementById("get_amount");
      price_val.innerHTML = store_amount;
      $('#selected_price').val();
      var short_haul_shipment = $('#search_start_location input[name=short_haul_shipment]:checked').val();
      if(short_haul_shipment == 1){
        short_haul_shipment = 'Short haul shipment'
      }
      var medium_haul_shipment = $('#search_start_location input[name=medium_haul_shipment]:checked').val();
      if(medium_haul_shipment == 1){
        medium_haul_shipment = 'Medium haul shipment'
      }
      var long_haul_shipment = $('#search_start_location input[name=long_haul_shipment]:checked').val();
      if(long_haul_shipment == 1){
        long_haul_shipment = 'Long haul shipment'
      }

      var final_array_start_location = [start_lat,start_lng,short_haul_shipment,medium_haul_shipment,long_haul_shipment];

      var end_lat = $('#end_latitude').val();
      var end_lng = $('#end_longitude').val();
      var end_short_haul_shipment = $('#search_end_location input[name=short_haul_shipment]:checked').val();
      if(end_short_haul_shipment == 1){
        end_short_haul_shipment = 'Short haul shipment'
      }
      var end_medium_haul_shipment = $('#search_end_location input[name=medium_haul_shipment]:checked').val();
      if(end_medium_haul_shipment == 1){
        end_medium_haul_shipment = 'Medium haul shipment'
      }
      var end_long_haul_shipment = $('#search_end_location input[name=long_haul_shipment]:checked').val();
      if(end_long_haul_shipment == 1){
        end_long_haul_shipment = 'Long haul shipment'
      }
      var final_array_end_location = [end_lat,end_lng,end_short_haul_shipment,end_medium_haul_shipment,end_long_haul_shipment];

      filter(start_date,min_amount,max_amount,truck_type,final_array_start_location,final_array_end_location);
    }) */

    /* $(document).on('keyup','#max_amount',function () {
      var start_date = $('#start-date').val();
      var min_amount = $('#min_amount').val();
      var max_amount = $('#max_amount').val();

      var truck_type = $('#selected_truck_type').val();
        var start_lat = $('#start_latitude').val();
      var start_lng = $('#start_longitude').val();
      var short_haul_shipment = $('#search_start_location input[name=short_haul_shipment]:checked').val();
      if(short_haul_shipment == 1){
        short_haul_shipment = 'Short haul shipment'
      }
      var medium_haul_shipment = $('#search_start_location input[name=medium_haul_shipment]:checked').val();
      if(medium_haul_shipment == 1){
        medium_haul_shipment = 'Medium haul shipment'
      }
      var long_haul_shipment = $('#search_start_location input[name=long_haul_shipment]:checked').val();
      if(long_haul_shipment == 1){
        long_haul_shipment = 'Long haul shipment'
      }

      var final_array_start_location = [start_lat,start_lng,short_haul_shipment,medium_haul_shipment,long_haul_shipment];

      var end_lat = $('#end_latitude').val();
      var end_lng = $('#end_longitude').val();
      var end_short_haul_shipment = $('#search_end_location input[name=short_haul_shipment]:checked').val();
      if(end_short_haul_shipment == 1){
        end_short_haul_shipment = 'Short haul shipment'
      }
      var end_medium_haul_shipment = $('#search_end_location input[name=medium_haul_shipment]:checked').val();
      if(end_medium_haul_shipment == 1){
        end_medium_haul_shipment = 'Medium haul shipment'
      }
      var end_long_haul_shipment = $('#search_end_location input[name=long_haul_shipment]:checked').val();
      if(end_long_haul_shipment == 1){
        end_long_haul_shipment = 'Long haul shipment'
      }
      var final_array_end_location = [end_lat,end_lng,end_short_haul_shipment,end_medium_haul_shipment,end_long_haul_shipment];

      filter(start_date,min_amount,max_amount,truck_type,final_array_start_location,final_array_end_location);
    }) */

    $(document).on('click','#price_search',function () {
      var start_date = $('#start-date').val();
      var min_amount = $('#min_amount').val();
      var max_amount = $('#max_amount').val();
      
      var truck_type = $('#selected_truck_type').val();
        var start_lat = $('#start_latitude').val();
      var start_lng = $('#start_longitude').val();
      var get_amount = document.getElementById("min_amount");
      var store_amount = get_amount.value;
      var price_val = document.getElementById("get_amount");
      price_val.innerHTML = store_amount;
      $('#selected_price').val();
      var short_haul_shipment = $('#search_start_location input[name=short_haul_shipment]:checked').val();
      if(short_haul_shipment == 1){
        short_haul_shipment = 'Short haul shipment'
      }
      var medium_haul_shipment = $('#search_start_location input[name=medium_haul_shipment]:checked').val();
      if(medium_haul_shipment == 1){
        medium_haul_shipment = 'Medium haul shipment'
      }
      var long_haul_shipment = $('#search_start_location input[name=long_haul_shipment]:checked').val();
      if(long_haul_shipment == 1){
        long_haul_shipment = 'Long haul shipment'
      }

      var final_array_start_location = [start_lat,start_lng,short_haul_shipment,medium_haul_shipment,long_haul_shipment];

      var end_lat = $('#end_latitude').val();
      var end_lng = $('#end_longitude').val();
      var end_short_haul_shipment = $('#search_end_location input[name=short_haul_shipment]:checked').val();
      if(end_short_haul_shipment == 1){
        end_short_haul_shipment = 'Short haul shipment'
      }
      var end_medium_haul_shipment = $('#search_end_location input[name=medium_haul_shipment]:checked').val();
      if(end_medium_haul_shipment == 1){
        end_medium_haul_shipment = 'Medium haul shipment'
      }
      var end_long_haul_shipment = $('#search_end_location input[name=long_haul_shipment]:checked').val();
      if(end_long_haul_shipment == 1){
        end_long_haul_shipment = 'Long haul shipment'
      }
      var final_array_end_location = [end_lat,end_lng,end_short_haul_shipment,end_medium_haul_shipment,end_long_haul_shipment];

      $('#price_filter').modal('hide');

      $('#selected_price').val(min_amount +' - '+ max_amount);
      filter(start_date,min_amount,max_amount,truck_type,final_array_start_location,final_array_end_location);
    })

    $('.largerCheckbox').on('change',function () {
      var checkbox_value = $(this).is(':checked');
      if(checkbox_value == true){
          $(this).closest('.checkmark').find('select > option').prop("selected","selected");
          $(this).closest('.checkmark').find('.my-select').trigger("change");
      }else{
          $(this).closest('.checkmark').find('select > option').prop("selected",false);
          $(this).closest('.checkmark').find('.my-select').trigger("change");
      }
    });

    $("#btnFiterSubmitSearch").click(function()
    {
        let errorShow = document.getElementById('errorShow');
        $cbx_group = $("input:checkbox[name='truck_type[]']");
        $cbx_group = $("input:checkbox[id^='truck_type']"); // name is not always helpful ;)

        if($cbx_group.is(":checked")){
            $cbx_group.prop('required', false);
            errorShow.style.display = 'none';
         
        }else{
            $cbx_group.prop('required', true);
            errorShow.style.display = 'block';
           
            return false;
        }
        var form_data = $(".largerCheckbox").val();
        var val = [];
        var data = [];
        $('.checkmark .largerCheckbox:checked').each(function(i){
          val[i] = $(this).val();
          data[i] = $(this).attr('data-name');
        });
        $('#selected_truck_name').val(data.toString());
        $('#selected_truck_type').val(val.toString());
        $('#truck-type').modal('hide');
        var start_date = $('#start-date').val();
        var min_amount = $('#min_amount').val();
        var max_amount = $('#max_amount').val();
        var truck_type = $('#selected_truck_type').val();
        var start_lat = $('#start_latitude').val();
      var start_lng = $('#start_longitude').val();
      var short_haul_shipment = $('#search_start_location input[name=short_haul_shipment]:checked').val();
      if(short_haul_shipment == 1){
        short_haul_shipment = 'Short haul shipment'
      }
      var medium_haul_shipment = $('#search_start_location input[name=medium_haul_shipment]:checked').val();
      if(medium_haul_shipment == 1){
        medium_haul_shipment = 'Medium haul shipment'
      }
      var long_haul_shipment = $('#search_start_location input[name=long_haul_shipment]:checked').val();
      if(long_haul_shipment == 1){
        long_haul_shipment = 'Long haul shipment'
      }

      var final_array_start_location = [start_lat,start_lng,short_haul_shipment,medium_haul_shipment,long_haul_shipment];

      var end_lat = $('#end_latitude').val();
      var end_lng = $('#end_longitude').val();
      var end_short_haul_shipment = $('#search_end_location input[name=short_haul_shipment]:checked').val();
      if(end_short_haul_shipment == 1){
        end_short_haul_shipment = 'Short haul shipment'
      }
      var end_medium_haul_shipment = $('#search_end_location input[name=medium_haul_shipment]:checked').val();
      if(end_medium_haul_shipment == 1){
        end_medium_haul_shipment = 'Medium haul shipment'
      }
      var end_long_haul_shipment = $('#search_end_location input[name=long_haul_shipment]:checked').val();
      if(end_long_haul_shipment == 1){
        end_long_haul_shipment = 'Long haul shipment'
      }
      var final_array_end_location = [end_lat,end_lng,end_short_haul_shipment,end_medium_haul_shipment,end_long_haul_shipment];
        filter(start_date,min_amount,max_amount,truck_type,final_array_start_location,final_array_end_location);
    });

    $(document).on('submit','#search_start_location',function () {
      var start_lat = $('#start_latitude').val();
      var start_address = ($('#autocomplete').val());
      
      $('#start_address').val(start_address.toString());
      
      var start_lng = $('#start_longitude').val();
      var short_haul_shipment = $('#search_start_location input[name=short_haul_shipment]:checked').val();
      if(short_haul_shipment == 1){
        short_haul_shipment = 'Short haul shipment'
      }
      var medium_haul_shipment = $('#search_start_location input[name=medium_haul_shipment]:checked').val();
      if(medium_haul_shipment == 1){
        medium_haul_shipment = 'Medium haul shipment'
      }
      var long_haul_shipment = $('#search_start_location input[name=long_haul_shipment]:checked').val();
      if(long_haul_shipment == 1){
        long_haul_shipment = 'Long haul shipment'
      }

      var final_array_start_location = [start_lat,start_lng,short_haul_shipment,medium_haul_shipment,long_haul_shipment];
      console.log(final_array_start_location);

      var end_lat = $('#end_latitude').val();
      var end_lng = $('#end_longitude').val();
      var end_short_haul_shipment = $('#search_end_location input[name=short_haul_shipment]:checked').val();
      if(end_short_haul_shipment == 1){
        end_short_haul_shipment = 'Short haul shipment'
      }
      var end_medium_haul_shipment = $('#search_end_location input[name=medium_haul_shipment]:checked').val();
      if(end_medium_haul_shipment == 1){
        end_medium_haul_shipment = 'Medium haul shipment'
      }
      var end_long_haul_shipment = $('#search_end_location input[name=long_haul_shipment]:checked').val();
      if(end_long_haul_shipment == 1){
        end_long_haul_shipment = 'Long haul shipment'
      }
      var final_array_end_location = [end_lat,end_lng,end_short_haul_shipment,end_medium_haul_shipment,end_long_haul_shipment];

      var start_date = $('#start-date').val();
      var min_amount = $('#min_amount').val();
      var max_amount = $('#max_amount').val();
      var truck_type = $('#selected_truck_type').val();

      $('#start-filter-location').modal('hide');
      filter(start_date,min_amount,max_amount,truck_type,final_array_start_location,final_array_end_location);

    })

    $(document).on('submit','#search_end_location',function () {
      var end_lat = $('#end_latitude').val();
      var end_address = ($('#autocomplete1').val());
      $('#end_address').val(end_address.toString());
      var end_lng = $('#end_longitude').val();
      var end_short_haul_shipment = $('#search_end_location input[name=short_haul_shipment]:checked').val();
      if(end_short_haul_shipment == 1){
        end_short_haul_shipment = 'Short haul shipment'
      }
      var end_medium_haul_shipment = $('#search_end_location input[name=medium_haul_shipment]:checked').val();
      if(end_medium_haul_shipment == 1){
        end_medium_haul_shipment = 'Medium haul shipment'
      }
      var end_long_haul_shipment = $('#search_end_location input[name=long_haul_shipment]:checked').val();
      if(end_long_haul_shipment == 1){
        end_long_haul_shipment = 'Long haul shipment'
      }
      var final_array_end_location = [end_lat,end_lng,end_short_haul_shipment,end_medium_haul_shipment,end_long_haul_shipment];

      var start_lat = $('#start_latitude').val();
      var start_lng = $('#start_longitude').val();
      var short_haul_shipment = $('#search_start_location input[name=short_haul_shipment]:checked').val();
      if(short_haul_shipment == 1){
        short_haul_shipment = 'Short haul shipment'
      }
      var medium_haul_shipment = $('#search_start_location input[name=medium_haul_shipment]:checked').val();
      if(medium_haul_shipment == 1){
        medium_haul_shipment = 'Medium haul shipment'
      }
      var long_haul_shipment = $('#search_start_location input[name=long_haul_shipment]:checked').val();
      if(long_haul_shipment == 1){
        long_haul_shipment = 'Long haul shipment'
      }

      var final_array_start_location = [start_lat,start_lng,short_haul_shipment,medium_haul_shipment,long_haul_shipment];
      var start_date = $('#start-date').val();
      var min_amount = $('#min_amount').val();
      var max_amount = $('#max_amount').val();
      var truck_type = $('#selected_truck_type').val();
      $('#start-filter-location').modal('hide');
      filter(start_date,min_amount,max_amount,truck_type,final_array_start_location,final_array_end_location);
    })
  })

</script>
<script type="text/javascript">
	$(document).ready(function () {
		
    var truck_type = [];
});
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
<script type="text/javascript">
      google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
        // var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete1'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            var address = pickup_place.address_components;
            $('#start_latitude').val(pickup_place.geometry.location.lat());
            $('#start_longitude').val(pickup_place.geometry.location.lng());
        });
    });

    google.maps.event.addDomListener(window, 'load', function () {
        // var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete1'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            var address = pickup_place.address_components;
            $('#end_latitude').val(pickup_place.geometry.location.lat());
            $('#end_longitude').val(pickup_place.geometry.location.lng());
        });
    });
</script>
<script type="text/javascript">
    function validateInput(e){
        let errorShow = document.getElementById('errorShow');
        errorShow.style.display = 'none';
        let cat_id = "truck_categories_"+e.value;
        let error_show_id = 'errorShow_'+e.value;
        let error_show = document.getElementById(error_show_id);
        $truck_cat = $("#"+cat_id);
        if(e.checked == true){
            $truck_cat.prop('required', true);
            // error_show.style.display = 'block';
            return false;
        }else{
            $truck_cat.prop('required', false);
            // error_show.style.display = 'none';
            return false;
        }
    }

    function validateSubcategory(e, truck_id){
        let error_show_id = 'errorShow_'+truck_id;
        let cat_id = "truck_categories_"+truck_id;
        $truck_cat = $("input:checkbox[id^='"+cat_id+"']");
        let error_show = document.getElementById(error_show_id);

        if($truck_cat.is(":checked"))
        {
            error_show.style.display = 'none';
            $("#truck_type_"+truck_id).prop("checked", true);
            return false;
        }else{
            error_show.style.display = 'block';
            $("#truck_type_"+truck_id).prop("checked", false);
            return false;
        }
    }


$(document).ready(function()
{
    
});
$('.dropdown-menu').click(function(e){
  e.stopPropagation();
})
           
</script>
<script type="text/javascript">
$('.my-select').on('change', function () {
    var selected = $(this);
    var selected_val = $(this).val();
    if(selected_val.length > 0){
        $(this).closest('.checkmark').find('.largerCheckbox').prop('checked',true);
    }else{
        $(this).closest('.checkmark').find('.largerCheckbox').prop('checked',false);
    }
});

// $('.largerCheckbox').on('change',function () {
//     var checkbox_value = $(this).is(':checked');
//     if(checkbox_value == true){
//         $(this).closest('.checkmark').find('select > option').prop("selected","selected");
//         $(this).closest('.checkmark').find('.my-select').trigger("change");
//     }else{
//         $(this).closest('.checkmark').find('select > option').prop("selected",false);
//         $(this).closest('.checkmark').find('.my-select').trigger("change");
//     }
// })
</script>

@endsection



