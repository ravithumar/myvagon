@extends('shipper.layouts.master')

@section('content')
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

<style>
.checked {
  color: orange;
}
.review {
    align-content: right;
    /*margin: inherit;*/
    margin-left: -28px;
}

.user-review{
	align-content: right;
}

.btn-grey{
    background-color:#D8D8D8;
	color:#FFF;
}
.rating-block{
	background-color:#FAFAFA;
	border:1px solid #EFEFEF;
	padding:15px 15px 20px 15px;
	border-radius:3px;
}
.bold{
	font-weight:700;
}
.padding-bottom-7{
	padding-bottom:7px;
}

.review-block {
    /* background-color: #FAFAFA; */
    border: 1pxsolid #EFEFEF;
    padding: 15px;
    border-radius: 3px;
    margin-bottom: 15px;
    width: 50%;
    height: 50%;
    padding: 0px;
    color: black;
}
.review-block-name{
	font-size:12px;
	margin:10px 0;
}
.review-block-date{
	font-size:12px;
}
.review-block-rate{
	font-size:13px;
	/*margin-bottom:15px;*/
	margin-left: -119px;
}
.review-block-title{
	font-size:15px;
	font-weight:700;
	margin-bottom:10px;
}
.review-block-description{
	font-size:13px;
	margin-left: -116px;
}
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 96
px
;
    margin-left: -7
px
;
    padding-bottom: -30px;
}
.row.float-right {
    /* float: left; */
    padding-top: -2px;
    margin-top: -251px;
    width: 133%;
}
.progress {
    
    height: 1.75rem;
    
}
.pull-right {
    float: left;
    margin: 8px;
}

</style>
</head>
@if($user->type == 'freelancer')
	@section('script')
	 	<script> $('#company-details').hide()</script> 
	 	<script>$('#driver-details').show()</script> 
	@endsection
@else
	@section('script') 
	 	<script>$('#company-details','#driver-details').show()</script> 
	@endsection
@endif
<!-- <div class="container-fluid"> -->
    <div class="row align-items-center">
         <div class="col">
            <nav aria-label="breadcrumb">
       			<ol class="breadcrumb m-0">
        			<!-- <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">Order Managemnt</a></li> -->
        			{{-- <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">Order Managemnt</a></li>

        			<li class="breadcrumb-item active txt-blue" aria-current="page">Order ID # {{$booking_truck->driver_id }}  </li>
        			<li class="breadcrumb-item active txt-blue" aria-current="page">Carrier and Driver Details</li> --}}
       			</ol>
   			</nav>
         </div>
    </div>

<div class="container-fluid company-details" id="company-details">
    <div class="row d-flex align-items-center">
    <div class="col-md-6">
    	<div class="order-status d-flex align-items-center">
		  <p class="font-20 txt-blue mb-0">Carrier Company Details</p>
		</div>
	</div>
	</div>
	<hr/>
	<div class="row">
		<div class="inline-fields" style="display: inline-block;">
	        @if($user->profile == '')
				<img src="{{ url('images/default.png') }}" class="img-rounded" style="border-radius: 8px; flex: 1 1 auto; padding: 10px; border-radius: 4px; flex: 1 1 auto; padding: 10px; height: 76px;">
			@else
	        <img src="{{ env('S3_IMAGE_URL') }}driver/{{ $user->profile }}" class="img-rounded" style="border-radius: 8px; flex: 1 1 auto; padding: 10px; border-radius: 4px; flex: 1 1 auto; padding: 10px; height: 76px;">
	        @endif
		</div> 
		<div class="inline-fields" style="display: inline-block;">
	        <h3>{{ $user->name }}</h3>
			<p>{{ $user->email }}</p>
		</div> 
		<div class=".col-sm-4 .col-sm-push-8" style="display: inline-block;">
		    <button type="button" class="btn btn-primary mb-2" style="margin-left: 35px;
    margin-top: 19px; background-color: white; border-radius: 10px;" >Chat With Carrier</button>
		</div>
		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-3 review">
				<h4 >3/5 
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star" style="color: #c2acef;"></span>
						<span class="fa fa-star" style="color: #c2acef;"></span></h4>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color: #9B51E0;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #9B51E0;     ">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #9B51E0;">
							
						  </div>
						</div>
					</div>
					
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color:#b36ef3;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #b36ef3;     width: 114px;">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #b36ef3;">
							
						  </div>
						</div>
					</div>
					
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color: #b295cd;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #b295cd; width: 80px;    ">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #b295cd;">
							
						  </div>
						</div>
					</div>
					
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color: #c1aed3;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #c1aed3; width: 50px;    ">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #c1aed3;">
							
						  </div>
						</div>
					</div>
					
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color: #bbb0c5;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #bbb0c5; width: 20px;    ">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #bbb0c5;">
							
						  </div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	
		<div class="row float-right ">

	<!-- <div class="container text-right"> -->
		<hr/>
		<div class="review-block">
			<h3>Review (400) </h3>
			<div class="row">
				<div class="col-sm-3">
					<img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded" style="border-radius: 18px;">
					
				</div>
				<div class="col-sm-9">
					<div class="review-block-rate">
						<div class="review-block-date"><b>Rachel Agency</b>  &nbsp;&nbsp;January 29, 2016 &nbsp; 8:00 AM
							&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<br/><br/>
					</div>
					</div>
					<div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-sm-3">
					<img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded" style="border-radius: 18px;">
				</div>
				<div class="col-sm-9">
					<div class="review-block-rate">
						<div class="review-block-date"><b>Rachel Agency</b>  &nbsp;&nbsp;January 29, 2016 &nbsp; 8:00 AM
							&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star"></span>
						<span class="fa fa-star"></span>
						<br/><br/>
					</div>
					</div>
					<div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
				</div>
			</div>
			<div class="right">
        		<a href="?i=4">Show All</a>
    		</div>
		</div>
	</div>
</div>
</div>
<div class="container-fluid driver-details" id="driver-details">
	<div class="row d-flex align-items-center">
    <div class="col-md-6">
    	<div class="order-status d-flex align-items-center">
		  <p class="font-20 txt-blue mb-0">Driver Details</p>
		</div>
	</div>
	</div>
	<hr/>
	<div class="row">
		<div class="inline-fields" style="display: inline-block;">
			@if($user->profile == '')
				<img src="{{ url('images/default.png') }}" class="img-rounded" style="border-radius: 8px; flex: 1 1 auto; padding: 10px; border-radius: 4px; flex: 1 1 auto; padding: 10px; height: 76px;">
			@else
	        	<img src="{{ env('S3_IMAGE_URL') }}driver/{{ $user->profile }}" class="img-rounded" style="border-radius: 8px; flex: 1 1 auto; padding: 10px; border-radius: 4px; flex: 1 1 auto; padding: 10px; height: 76px;">
	        @endif
		</div> 
		<div class="inline-fields" style="display: inline-block;">
	        <h3>{{ $user->name }}</h3>
			<p>{{ $user->email }}</p>
		</div> 
		<div class=".col-sm-4 .col-sm-push-8" style="display: inline-block;">
		    <button type="button" class="btn btn-primary mb-2 txt-blue" style="margin-left: 35px;
    margin-top: 19px; background-color: white; border-radius: 10px;" >Chat With Driver</button>
		</div>
		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-3 review">
				<h4 >3/5 
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star" style="color: #c2acef;"></span>
						<span class="fa fa-star" style="color: #c2acef;"></span></h4>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color: #9B51E0;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #9B51E0;     ">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #9B51E0;">
							
						  </div>
						</div>
					</div>
					
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color:#b36ef3;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #b36ef3;     width: 114px;">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #b36ef3;">
							
						  </div>
						</div>
					</div>
					
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color: #b295cd;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #b295cd; width: 80px;    ">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #b295cd;">
							
						  </div>
						</div>
					</div>
					
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color: #c1aed3;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #c1aed3; width: 50px;    ">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #c1aed3;">
							
						  </div>
						</div>
					</div>
					
				</div>
				<div class="pull-left">
					<div class="pull-left" style="width:35px; line-height:1;">
						<div style="height:9px; margin: 14px 9px;">4 <span class="glyphicon glyphicon-star"></span></div>
					</div>
					<div class="pull-left" style="width:180px;">
						<span class="sr-only" style=" background-color: #bbb0c5;">80% Complete (danger)</span>
						<div class="progress " style=" margin:8px 0; background-color: #bbb0c5; width: 20px;    ">
						  <div class="progress-bar bg-primary bg-opacity-50" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80% background-color: #bbb0c5;">
							
						  </div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	
		<div class="row float-right ">

	<!-- <div class="container text-right"> -->
		<hr/>
		<div class="review-block" id="myList">
			<h3>Review (400) </h3>
			<div class="row moreBox blogBox">
				<div class="col-sm-3">
				   <img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded" style="border-radius: 18px;">
				</div>
				<div class="col-sm-9">
					<div class="review-block-rate">
						<div class="review-block-date"><b style="font-size: small;">Rachel Agency</b>  &nbsp;&nbsp;January 29, 2016 &nbsp; 8:00 AM
							&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star" style="color: #c2acef;"></span>
						<span class="fa fa-star" style="color: #c2acef;"></span>
						<br/><br/>
					</div>
					</div>
					<div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
				</div>
			</div>
			<hr/>
			<div class="row moreBox blogBox">
				<div class="col-sm-3">
					<img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded" style="border-radius: 18px;">
				</div>
				<div class="col-sm-9">
					<div class="review-block-rate">
						<div class="review-block-date"><b style="font-size: small;">Rachel Agency</b>  &nbsp;&nbsp;January 29, 2016 &nbsp; 8:00 AM
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="fa fa-star checked" style="color: #9B51E0;"></span>
							<span class="fa fa-star checked" style="color: #9B51E0;"></span>
							<span class="fa fa-star checked" style="color: #9B51E0;"></span>
							<span class="fa fa-star" style="color: #c2acef;"></span>
							<span class="fa fa-star" style="color: #c2acef;"></span>
							
						</div>
					</div>
					<div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
				</div>
			</div>
			<hr/>
			<div class="row moreBox item" id="moreBox">
				<div class="col-sm-3">
					<img src="http://dummyimage.com/60x60/666/ffffff&text=No+Image" class="img-rounded" style="border-radius: 18px;">
				</div>
				<div class="col-sm-9">
					<div class="review-block-rate">
						<div class="review-block-date"><b style="font-size: small;">Rachel Agency</b>  &nbsp;&nbsp;January 29, 2016 &nbsp; 8:00 AM
							&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star checked" style="color: #9B51E0;"></span>
						<span class="fa fa-star" style="color: #c2acef;"></span>
						<span class="fa fa-star" style="color: #c2acef;"></span>
						<br/><br/>
					</div>
					</div>
					<div class="review-block-description">this was nice in buy. this was nice in buy. this was nice in buy. this was nice in buy this was nice in buy this was nice in buy this was nice in buy this was nice in buy</div>
				</div>
			</div>
			<div class="right">
        		<a href="?i=4" id="loadMore" class="showmemore" onclick="toggleText();" href="javascript:void(0);">Show All</a>
    		</div>
    		<div class="right">
    			<a href="?i=4" id="HideMore" class="showmemore" onclick="toggleText();" href="javascript:void(0);">Hide All</a>
    		</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
	$("#HideMore").hide();
	$(document).ready(function(){   
    var size_item = $('.moreBox').length;
    var v=2;
    $('.moreBox').hide(); 
    $('.moreBox:lt('+v+')').show();
	 // select the first ten
  	$("#loadMore").click(function(e) { 
    e.preventDefault();
    $(".item:hidden").slice(0, 1).show(); 
    $("#loadMore").hide();
    $("#HideMore").show();
  });
  	$("#HideMore").click(function(e) { 
    e.preventDefault();
    $(".item").slice(0, 1).hide(); 
    $("#HideMore").hide();
    $("#loadMore").show();
  });

  	

});

</script>


@endsection