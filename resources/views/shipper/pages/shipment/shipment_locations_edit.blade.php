@if(!empty(Session::get('edit_truck_data')))
<?php $session_data = Session::get('edit_truck_data');?>
@endif
@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;

}
.delivery_type:after {
    width: 12px;
    height: 12px;
}
.select2-container{
  width:100% !important;
}
.modal-content li.parsley-required {
    margin-top: -10px;
    position: absolute;
    left: 22px;
    top: initial;
    margin-top: -52px;
}
.parsley-errors-list>li {
    list-style: none;
    color: #f1556c;
    margin-top: 5px;
    padding-left: 20px;
    position: relative;
    top: 48px;
    border-collapse: separate;
}
.modal-body {
    position: relative;
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1rem;
    padding: 18px;
}
#map {
  width: 100%;
  height: 100vh;
  /*margin-top: -547px;*/
  /*left: 0 !important;*/
  /*top: 30px !important;*/
  /*border-top-width: 24px;*/
  /*position: absolute;*/
  /*transform: rotate(90deg);*/
  /*margin: 500px;*/
  /*margin-top: 500px;*/
  /*margin-top: -643px; */
  /* width: 390px; */
    /*float: left;*/
    /* height: 690px; */
    /*overflow-y: scroll;*/
/*-webkit-mask-position: right;*/
    /* margin: 407px;
    margin-top: -700px; */
}
.col-xl-8 {
    /*float: none;*/
     max-width: none; 
    max-height: 25px;
}
mobile.parsley-error {
    margin-top: 20px;
}
li.parsley-required {
    margin-top: -10px;
    position: absolute;
    left: 22px;
}
.modal-content {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    /* width: 166%; */
    pointer-events: auto;
    background-color: #fff;
    background-clip: padding-box;
    border: 0 solid transparent;
    border-radius: -0.8rem;
    outline: 1;
    /* margin: -138px; */
}
@media (min-width: 576px){
  .modal-dialog {
    max-width: 882px;
    margin: 1.75rem auto;
  }
}
.card-body {
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    /* padding: 1.5
rem
; */
}
.card-body.table-responsive {
    /*padding: 0px;*/
    /*width: 153%;*/
}

</style>
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col">
          <nav aria-label="breadcrumb">
            @if(isset($session_data['driver_detail']) && !empty($session_data['driver_detail']))
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:void(0);" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{__('Search Available Trucks')}}</a></li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">{{__('Bid')}}</li>
                <li class="breadcrumb-item active txt-blue" aria-current="page"><a href="{{ url('shipper/shipment/step2/edit',$id) }}" class="txt-blue">Product Information</a></li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">{{__('Pickup')}} &amp; {{__('Delivery Information')}}</li>
              </ol>
            @else
            <ol class="breadcrumb m-0">
              <li class="breadcrumb-item"><a href="javascript:void(0);" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{__('Edit Shipment')}}</a></li>
              <li class="breadcrumb-item active txt-blue" aria-current="page"><a href="{{ url('shipper/shipment/edit',$id) }}" class="txt-blue">{{__('Truck Type')}}</a></li>
              <li class="breadcrumb-item active txt-blue" aria-current="page"><a href="{{ url('shipper/shipment/step2/edit',$id) }}" class="txt-blue">{{__('Product Information')}}</a></li>
              <li class="breadcrumb-item active txt-blue" aria-current="page">{{__('Pickup')}} &amp; {{__('Delivery Information')}}</li>
              </ol>
            @endif
          </nav>
        </div>
      </div>

      @if(isset($session_data) && !empty($session_data))
        @if(isset($session_data['driver_detail']) && !empty($session_data['driver_detail']))
          <div class="row mt-2 mb-2">
            <div class="col-md-12">
              <div class="truck-box bg-white p-2">
                <div class="row align-items-center">
                  <div class="col-md-1 tr-br">
                    <p class="mb-0 font-14 txt-blue font-weight-light"><em class="font-weight-bold">{{__('Available Truck')}}</em><br>{{__('for booking')}}   </p>
                  </div>
                  <div class="col-md-2">
                    <p class="txt-purple mb-0">{{__('Truck Type')}}</p><br>
                    <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['driver_vehicle_detail']['truck_type']['name']) && !empty($session_data['driver_vehicle_detail']['truck_type']['name'])){{ $session_data['driver_vehicle_detail']['truck_type']['name'] }}@endif</p>
                    <p class="mb-0 font-13 txt-blue font-weight-light txt-purple">@if(isset($session_data['driver_vehicle_detail']['truck_sub_category']['name']) && !empty($session_data['driver_vehicle_detail']['truck_sub_category']['name'])){{ $session_data['driver_vehicle_detail']['truck_sub_category']['name'] }}@endif</p>
                  </div>
                  <div class="col-md-2">
                    <p class="txt-purple mb-0">{{__('Start Date')}}</p><br>
                    <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['date']) && !empty($session_data['availability_data']['date'])){{ date('F d, Y',strtotime($session_data['availability_data']['date'])) }}@endif <br> @if(isset($session_data['availability_data']['time']) && !empty($session_data['availability_data']['time'])) {{$session_data['availability_data']['time']}} @endif</p>
                  </div>
                  <div class="col-md-2">
                    <p class="txt-purple mb-0">{{__('Start Location')}}</p><br>
                    <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['from_address']) && !empty($session_data['availability_data']['from_address'])) {{ (strlen($session_data['availability_data']['from_address']) > 35 ? substr($session_data['availability_data']['from_address'],0,35).'...' : $session_data['availability_data']['from_address']) }} @endif</p>
                  </div>
                  <div class="col-md-2">
                    <p class="txt-purple mb-0">{{__('End Location')}}</p><br>
                    <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['to_address']) && !empty($session_data['availability_data']['to_address'])) {{ (strlen($session_data['availability_data']['to_address']) > 35 ? substr($session_data['availability_data']['to_address'],0,35).'...' : $session_data['availability_data']['to_address']) }} @endif</p>
                  </div>
                  <div class="col-md-2">
                    <p class="txt-purple mb-0">{{__('Carrier Info')}}</p><br>
                    {{-- <p class="mb-0 font-14 txt-blue font-weight-light"><a href="javascript:void(0)">Test Driver</a></p> --}}
                    <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['driver_detail']['name']) && !empty($session_data['driver_detail']['name'])) {{ $session_data['driver_detail']['name'] }} @endif</p>
                  </div>
                  <div class="col-md-1">
                    <p class="txt-purple mb-0">{{__('Bid Price')}}</p><br>
                    <p class="mb-0 font-14 txt-blue font-weight-light">€ @if(isset($session_data['availability_data']['bid_amount']) && !empty($session_data['availability_data']['bid_amount'])) {{ $session_data['availability_data']['bid_amount'] }} @else 0 @endif</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endif
      @endif
      <div class="row d-flex align-items-center products">
        <div class="col-md-8 col-lg-6">
          <h3 class="txt-blue">{{__('Pickup')}} &amp; {{__('Delivery Information')}}</h3>
          {{-- <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p> --}}
        </div>
        <div class="col-md-4 col-lg-5"></div>
      </div>
     <div class="pickup-content">
      <div class="row">
        <div class="col">
                @php
                    if(count($products)>1){
                        $visible = 'pointer-events: none;';
                        $product = 'Products';
                        $warning = '';
                        $type = 'have';
                    }else{
                        $visible = 'pointer-events: none;';
                        $product = 'Product';
                        // $warning = 'So You can only select direct shipment';
                        $warning = '';
                        $type = 'has';
                    }
                @endphp

            <h3 class="txt-blue">{{ count($products) }} {{ $product }} {{ $type }} been added for <img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid"></h3>
            <p class="text-danger">{{ $warning }}</p>
            <div class="row">
              <div class="col-md-12">
                  <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                  <img src="http://13.36.112.48/assets/images/shipment/plus-pink.svg" alt="chat" class="img-fluid">
                  <span class="ml-1 mr-3" style="color: #9B51E0;">{{__('Create New Facility Address')}}</span>
                </a>
              </div>
            </div>
            @include('admin.include.flash-message')
           <ul class="nav nav-tabs-black">
                <li class="nav-item">
                        <button class="nav-link @if($booking_data['booking_type'] == 'direct_shipment') active @endif btn font-10 mt-3 btn-outline-primary tablinks" onclick="Stops(event,'DirectShip')">{{__('Direct Shipments')}}</button>
                </li>
                <li class="nav-item">
                        <button class="ml-2 nav-link @if($booking_data['booking_type'] == 'multiple_shipment') active @endif multiple_stops font-16 btn font-10 mt-3 btn-outline-primary tablinks" onclick="Stops(event,'MultipleShip')">{{__('Multiple Shipments')}}</a>
                </li>
           </ul>
           <hr style="border: none; border-bottom: 1px solid silver;">
           <div class="tabcontent" id="DirectShip" @if($booking_data['booking_type'] == 'direct_shipment') style="display:block;" @else style="display: none;" @endif>
                <form action="{{ route('shipper.shipment.update.step3',$id) }}" method="post" id="final_form">
                 @csrf
                    <div class="tab-panel show active" id="direct-shipment">
                      @if(isset($booking_locations_data) && !empty($booking_locations_data) && $booking_data['booking_type'] == 'direct_shipment')
                        <div class="details-content">
                          <p class="font-16 color-dark-purple mb-3 txt-blue">{{__('Pickup Details')}} 
                          {{-- <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                      <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                      <span class="ml-1 mr-3" style="color: #9B51E0;">Create New Facility Address</span>
                                  </a> --}}
                                </p>
                          <div class="block">
                            <div class="row">
                              <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                  <select name="pickupAddress" required id="errorShowPickup" class="form-control location-input select2">
                                      <option value="" selected>{{__('Select address')}}</option>
                                      @foreach ($addresses as $key => $address)
                                      <option value="{{$address->id}}" {{ $address->address == $booking_locations_data[0]['drop_location'] ? 'selected' : '' }}>{{$address->name}}</option>
                                      @endforeach
                                  </select>
                                  
                                  <p class="text-danger" id="errorShowPickup" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> {{__('This value is required')}}.</p>
                                    
                              </div>
                              <div class="col-sm-3 col-md-3 col-lg-3">
                                  <div class="form-group mb-4">
                                      {{-- <input type="date" id="pickup_date" name="pickup_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> --}}
                                      <input type="text" id="basic-datepicker111" class="form-control form-control-without-border pb-2" name="pickup_date" required style="background: none; width: 100%;" placeholder="DD/MM/YYYY" value="{{ $booking_locations_data[0]['delivered_at'] }}" onchange="DirectshipmentValidate()">
                                  </div>
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                  <div class="form-group mb-4 d-flex">
                                        <label><input type="checkbox" id="pickup_time_range_check" class="largerCheckbox" name="pickup_time_range_check" value="1" onclick="showPickUpRange()" @if($booking_locations_data[0]['delivery_time_from'] != $booking_locations_data[0]['delivery_time_to']) checked @endif><span></span></label>
                                        <label for="range" class="txt-blue">{{__('Range')}}</label>
                                        {{-- Range Div --}}
                                        <div id="pickup_time_range" class="row ml-1" style="flex-wrap: unset !important;@if($booking_locations_data[0]['delivery_time_from'] == $booking_locations_data[0]['delivery_time_to']) display:none; @else display:flex @endif">


                                            <input type="text" id="basic-pickup-timepicker1111" name="pickup_hour_from" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%" @if($booking_locations_data[0]['delivery_time_from'] != $booking_locations_data[0]['delivery_time_to']) required="required" value="{{ date('H:i',strtotime($booking_locations_data[0]['delivery_time_from'])) }}" @endif onchange="check_date_range()">


                                            <input type="hidden" name="pickup_time_from">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_from" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select> --}}

                                            <span class="txt-blue ml-2 mr-2">{{__('TO')}}</span>
                                            <input type="text" id="basic-pickup-timepicker2222" name="pickup_hour_to" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%" @if($booking_locations_data[0]['delivery_time_from'] != $booking_locations_data[0]['delivery_time_to']) required="required" value="{{ date('H:i',strtotime($booking_locations_data[0]['delivery_time_to'])) }}" @endif onchange="check_date_range()">
                                            <input type="hidden" name="pickup_time_to">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_to" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select> --}}
                                        </div>
                                        {{-- Single time div --}}
                                        <div id="pickup_single_time" class="row" @if($booking_locations_data[0]['delivery_time_from'] == $booking_locations_data[0]['delivery_time_to']) style="display: flex;" @else style="display: none" @endif>
                                            <input type="text" id="basic-pickup-timepicker3333" name="pickup_hour" class="form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;" @if($booking_locations_data[0]['delivery_time_from'] == $booking_locations_data[0]['delivery_time_to']) required="required" value="{{ date('H:i',strtotime($booking_locations_data[0]['delivery_time_from'])) }}" @endif>
                                            <input type="hidden" name="pickup_time_format">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="pickup_time_format" required>
                                                    <option value="am">AM</option>
                                                    <option value="pm">PM</option>
                                            </select> --}}

                                        </div>
                                  </div>
                              </div>
                                <div class="d-sm-none col-lg-3 d-lg-block"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="note-area md-lg-5">
                                      <ul class="d-flex m-1 p-1 mb-2">
                                        <a href="javascript:;" class="add-one font-18 d-flex pickup-show_button" id="add_note" onclick="showImage();">
                                            <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="hide" style="visibility:hidden ">
                                            <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="hide_show">
                                            <span class="text ml-1 txt-blue" style="text-decoration: underline; margin-top: 4px; " >{{__('Add')}}&nbsp;{{__('Note')}}</span>
                                            <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="PickupClearNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="padding-top: 8px;"></a></label>
                                         </a><br>
                                        </ul>
                                        <textarea rows="2" name="pickup_note" id="pickUpNote" class="form-control" @if(isset($booking_locations_data[0]['note']) && !empty($booking_locations_data[0]['note'])) style="display:block" @else style="resize: none; display:none;" @endif placeholder="{{__('Enter Pickup Note')}}" >@if(isset($booking_locations_data[0]['note']) && !empty($booking_locations_data[0]['note'])) {{ $booking_locations_data[0]['note'] }} @endif</textarea>
                                    </div>
                                </div>
                               <div class="col-md-7"></div>
                            </div>
                          </div>
                      </div>
                      <hr style="border: none; border-bottom: 1px solid silver;">
                      <div class="details-content mt-4">
                          <p class="font-16 color-dark-purple mb-3 txt-blue">{{__('Delivery Details')}} 
                            {{-- <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                      <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                      <span class="ml-1 mr-3" style="color: #9B51E0;">Create New Facility Address</span>
                                  </a> --}}
                                </p>
                          <div class="block">
                            <div class="row">
                              <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                  <?php $last_key = array_key_last($booking_locations_data);?>
                                  <select name="deliveryAddress" required id="errorShowDrop" class="form-control form-control-without-border select2">
                                      <option value="" selected>{{__('Select address')}}</option>
                                      @foreach ($addresses as $key => $address)
                                      <option value="{{$address->id}}" {{ $address->address == $booking_locations_data[$last_key]['drop_location'] ? 'selected' : '' }} >{{$address->name}}</option>
                                      @endforeach
                                  </select>
                                  <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                                  <p class="text-danger" id="same_address_error" style="display: none">Delivery address and pickup address are same, please select another address.</p>
                              </div>
                              <div class="col-sm-3 col-md-3 col-lg-3">
                                  <div class="form-group mb-4">
                                      {{-- <input type="date" id="delivery_date" name="delivery_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> --}}
                                      <input type="text" id="basic-delivery-datepicker111" class="form-control form-control-without-border pb-2" name="delivery_date" required style="background: none; width: 100%;" placeholder="{{__('DD/MM/YYYY')}}" value="{{ $booking_locations_data[$last_key]['delivered_at'] }}" onchange="DirectshipmentValidate()">

                                        <p class="text-danger" id="errorShowDatePicker" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  
                                        End date must be come after the start date.</p>
                                      <p class="text-danger" id="errorShowDatePickerSame" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start date and end date not be same.</p>
                                    

                                    </div>
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                  <div class="form-group mb-4 d-flex">
                                        <label><input type="checkbox" id="delivery_time_range_check" class="largerCheckbox" name="delivery_time_range_check" value="1" onclick="showDeliveryRange()" @if($booking_locations_data[$last_key]['delivery_time_from'] != $booking_locations_data[$last_key]['delivery_time_to']) checked @endif><span></span></label>
                                        <label for="range" class="txt-blue">{{__('Range')}}</label>
                                        {{-- Range Div --}}
                                        <div id="delivery_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                            <input type="text" id="basic-pickup-timepicker4444" name="delivery_hour_from" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%" @if($booking_locations_data[$last_key]['delivery_time_from'] != $booking_locations_data[$last_key]['delivery_time_to']) required="required" value="{{ date('H:i',strtotime($booking_locations_data[$last_key]['delivery_time_from'])) }}" @endif onchange="check_date_range()">

                                            <input type="hidden" name="delivery_time_from">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_from" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select> --}}
                                            <span class="txt-blue ml-2 mr-2">TO</span>
                                            <input type="text" id="basic-pickup-timepicker5555" name="delivery_hour_to" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%" @if($booking_locations_data[$last_key]['delivery_time_from'] != $booking_locations_data[$last_key]['delivery_time_to']) required="required" value="{{ date('H:i',strtotime($booking_locations_data[$last_key]['delivery_time_from'])) }}" @endif onchange="check_date_range()">
                                            <input type="hidden" name="delivery_time_to">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_to" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select> --}}
                                        </div>
                                        {{-- Single time div --}}
                                        <div id="delivery_single_time" class="row" style="display: flex;">
                                            <input type="text" id="basic-pickup-timepicker6666" name="delivery_hour" class="form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;" @if($booking_locations_data[$last_key]['delivery_time_from'] == $booking_locations_data[$last_key]['delivery_time_to']) required="required" value="{{ date('H:i',strtotime($booking_locations_data[$last_key]['delivery_time_from'])) }}" @endif onchange="DirectshipmentValidate()">
                                            <input type="hidden" name="delivery_time_format">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="delivery_time_format" required>
                                                    <option value="am">AM</option>
                                                    <option value="pm">PM</option>
                                            </select> --}}
                                        </div>
                                  </div>
                                  <p class="text-danger" id="errorShowSameTime" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start time and end time not be same.</p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-5">
                                  <div class="note-area md-lg-5">
                                    <ul class="d-flex m-1 p-1 mb-2">
                                      <a href="javascript:;" class="add-one font-18 d-flex show_button_text" id="add_note" onclick="delivery_note();">
                                          <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="minus_delivery" style="visibility:hidden ">
                                            <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="plus_delivery">
                                          <span class="text ml-1 txt-blue" style="text-decoration: underline;" >{{__('Add')}}&nbsp;{{__('Note')}}</span>
                                          <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="DeliveryClearNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="padding-top: 8px;"></a></label>
                                          {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                                        </a><br>
                                      </ul>
                                      <textarea rows="2" name="delivery_note" id="text_id" class="form-control" @if(isset($booking_locations_data[$last_key]['note']) && !empty($booking_locations_data[$last_key]['note'])) style="display:block" @else style="resize: none; display:none;" @endif placeholder="{{__('Enter Delivery Note')}}">@if(isset($booking_locations_data[$last_key]['note']) && !empty($booking_locations_data[$last_key]['note'])) {{ $booking_locations_data[$last_key]['note'] }} @endif</textarea>
                                  </div>
                              </div>
                              <div class="col-md-7"></div>
                            </div>
                          </div>
                      </div>
                      {{-- <a href="javascript:;" class="black-btn font-16 mt-5" data-toggle="modal" data-target="#edit-shipment">Insert Quote</a> --}}
                      <div class="btn-block d-flex">
                          <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" id="continuebtn">{{__('Insert Quote')}}</button>
                      </div>

                     @else
                        <div class="details-content">
                          <p class="font-16 color-dark-purple mb-3 txt-blue">{{__('Pickup Details')}} 
                          {{-- <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                      <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                      <span class="ml-1 mr-3" style="color: #9B51E0;">Add Facility Address</span>
                                  </a> --}}
                                </p>
                          <div class="block">
                            <div class="row">
                              <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                  <select name="pickupAddress" required id="errorShowPickup" class="form-control location-input select2">
                                      <option value="" selected>{{__('Select address')}}</option>
                                      @foreach ($addresses as $key => $address)
                                      <option value="{{$address->id}}">{{$address->name}}</option>
                                      @endforeach
                                  </select>
                                  
                                  <p class="text-danger" id="errorShowPickup" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                                    
                              </div>
                              <div class="col-sm-3 col-md-3 col-lg-3">
                                  <div class="form-group mb-4">
                                      {{-- <input type="date" id="pickup_date" name="pickup_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> --}}
                                      <input type="text" id="basic-datepicker" class="form-control form-control-without-border pb-2" name="pickup_date" required style="background: none; width: 100%;" placeholder="{{__('DD/MM/YYYY')}}">
                                  </div>
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                  <div class="form-group mb-4 d-flex">
                                        <label><input type="checkbox" id="pickup_time_range_check" class="largerCheckbox" name="pickup_time_range_check" value="1" onclick="showPickUpRange()"><span></span></label>
                                        <label for="range" class="txt-blue">{{__('Range')}}</label>
                                        {{-- Range Div --}}
                                        <div id="pickup_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">


                                            <input type="text" id="basic-pickup-timepicker1111" name="pickup_hour_from" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%" onchange="check_date_range()">


                                            <input type="hidden" name="pickup_time_from">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_from" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select> --}}

                                            <span class="txt-blue ml-2 mr-2">{{__('TO')}}</span>
                                            <input type="text" id="basic-pickup-timepicker2222" name="pickup_hour_to" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%" onchange="check_date_range()">
                                            <input type="hidden" name="pickup_time_to">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_to" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select> --}}
                                        </div>
                                        {{-- Single time div --}}
                                        <div id="pickup_single_time" class="row" style="display: flex;">
                                            <input type="text" id="basic-pickup-timepicker3333" name="pickup_hour"   
                                            class="form-control-without-border pb-2 ml-4" placeholder="{{__('HH:MM')}}" style="background: none;" required="required" onchange="DirectshipmentValidate()">
                                            <input type="hidden" name="pickup_time_format">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="pickup_time_format" required>
                                                    <option value="am">AM</option>
                                                    <option value="pm">PM</option>
                                            </select> --}}

                                        </div>
                                  </div>
                              </div>
                                <div class="d-sm-none col-lg-3 d-lg-block"></div>
                            </div>
                            <div class="row">
                              <div class="col-md-5">
                                  <div class="note-area md-lg-5">
                                      <ul class="d-flex m-0 p-0 mb-2">
                                        <a href="javascript:;" class="add-one font-18 d-flex multi_pickup-show_button" id="add_note" onclick="multi_delivery_note();">
                                          <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="multi_minus_delivery" style="visibility:hidden ">
                                            <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="multi_plus_delivery">
                                        <span class="text ml-1 txt-blue" style="text-decoration: underline;" >{{__('Add')}}&nbsp;{{__('Note')}}</span>
                                        <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="ClearMultiNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="padding-top: 8px;"></a></label>
                                        {{-- <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changePickUpNote()"><img src="{{ asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                                        <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearPickUpNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label> --}}
                                      </ul>
                                      
                                      <textarea rows="2" name="pickup_note" id="multi_pickUpNote" class="form-control" @if(isset($booking_locations_data[0]['note']) && !empty($booking_locations_data[0]['note'])) style="display:block" @else style="resize: none; display:none;" @endif placeholder="{{__('Enter Pickup Note')}}" >@if(isset($booking_locations_data[0]['note']) && !empty($booking_locations_data[0]['note'])) {{ $booking_locations_data[0]['note'] }} @endif</textarea>
                                  </div>
                              </div>
                              <div class="col-md-7"></div>
                            </div>
                          </div>
                      </div>
                      <hr style="border: none; border-bottom: 1px solid silver;">
                      <div class="details-content mt-4">
                          <p class="font-16 color-dark-purple mb-3 txt-blue">{{__('Delivery Details')}} 
                            {{-- <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                      <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                      <span class="ml-1 mr-3" style="color: #9B51E0;">{{__('Add Facility Address')}}</span>
                                  </a> --}}
                                </p>
                          <div class="block">
                            <div class="row">
                              <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                  <select name="deliveryAddress" required id="errorShowDrop" class="form-control form-control-without-border select2">
                                      <option value="" selected>{{__('Select address')}}</option>
                                      @foreach ($addresses as $key => $address)
                                      <option value="{{$address->id}}">{{$address->name}}</option>
                                      @endforeach
                                  </select>
                                  <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                                  <p class="text-danger" id="same_address_error" style="display: none">Delivery address and pickup address are same, please select another address.</p>
                              </div>
                              <div class="col-sm-3 col-md-3 col-lg-3">
                                  <div class="form-group mb-4">
                                      {{-- <input type="date" id="delivery_date" name="delivery_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> --}}
                                      <input type="text" id="basic-delivery-datepicker" class="form-control form-control-without-border pb-2" name="delivery_date" required style="background: none; width: 100%;" placeholder="{{__('DD/MM/YYYY')}}">

                                        <p class="text-danger" id="errorShowDatePicker" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  
                                        End date must be come after the start date.</p>
                                      <p class="text-danger" id="errorShowDatePickerSame" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start date and end date not be same.</p>
                                    

                                    </div>
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                  <div class="form-group mb-4 d-flex">
                                        <label><input type="checkbox" id="delivery_time_range_check" class="largerCheckbox" name="delivery_time_range_check" value="1" onclick="showDeliveryRange()"><span></span></label>
                                        <label for="range" class="txt-blue">{{__('Range')}}</label>
                                        {{-- Range Div --}}
                                        <div id="delivery_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                            <input type="text" id="basic-pickup-timepicker4444" name="delivery_hour_from" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%" onchange="check_date_range()">

                                            <input type="hidden" name="delivery_time_from">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_from" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select> --}}
                                            <span class="txt-blue ml-2 mr-2">TO</span>
                                            <input type="text" id="basic-pickup-timepicker5555" name="delivery_hour_to" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%" onchange="check_date_range()">
                                            <input type="hidden" name="delivery_time_to">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_to" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                            </select> --}}
                                        </div>
                                        {{-- Single time div --}}
                                        <div id="delivery_single_time" class="row" style="display: flex;">
                                            <input type="text" id="basic-pickup-timepicker6666" name="delivery_hour" class="form-control-without-border pb-2 ml-4" placeholder="{{__('HH:MM')}}" style="background: none;" required="required" onchange="DirectshipmentValidate()">
                                            <input type="hidden" name="delivery_time_format">
                                            {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="delivery_time_format" required>
                                                    <option value="am">AM</option>
                                                    <option value="pm">PM</option>
                                            </select> --}}
                                        </div>
                                  </div>
                                  <p class="text-danger" id="errorShowSameTime" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start time and end time not be same.</p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-5">
                                  <div class="note-area md-lg-5">
                                      <a href="javascript:;" class="add-one font-18 d-flex show_button" id="add_note" onclick="delivery_multi_note();">
                                        <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="delivery_minus" style="visibility:hidden ">
                                          <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="delivery_plus">
                                          <span class="text ml-1 txt-blue" style="text-decoration: underline;" >{{__('Add')}}&nbsp;{{__('Note')}}</span>
                                          <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="multiClearNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="padding-top: 8px;padding-top: 8px;margin-top: -52px;margin-left: 142px;"></a></label>
                                          {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                                        </a><br>
                                      <textarea rows="2" name="delivery_note" id="multi_text_id" class="form-control" style="resize: none; display:none;" placeholder="Enter Delivery Note"></textarea>
                                  </div>
                              </div>
                              <div class="col-md-7"></div>
                            </div>
                          </div>
                      </div>
                      {{-- <a href="javascript:;" class="black-btn font-16 mt-5" data-toggle="modal" data-target="#edit-shipment">Insert Quote</a> --}}
                      <div class="btn-block d-flex">
                          <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" id="continuebtn">{{__('Insert Quote')}}</button>
                      </div>
                      @endif
                        
                    </div>
                </form>
           </div>
           <div class="tabcontent" id="MultipleShip" @if($booking_data['booking_type'] == 'multiple_shipment') style="display:block;" @else style="display: none;" @endif>
            <form action="{{ route('shipper.shipment.update.multiple',$id) }}" method="post" id="multiple-form">
              @csrf
              <div class="row">
                <div class="col">
                  <div class="pro-info input_fields_wrap">
                    <div class="pro-content  mb-4 location_info">
                      <input type="hidden" class="multiple_shipment_location_length" name="multiple_shipment_location_length" @if(isset($booking_locations_data) && !empty($booking_locations_data) && count($booking_locations_data) > 0) value="{{ count($booking_locations_data) }}" @else value="1" @endif>
                      @if(isset($booking_locations_data) && !empty($booking_locations_data) && $booking_data['booking_type'] == 'multiple_shipment')
                        @foreach($booking_locations_data as $location_key => $booking_location)
                          <div class="row locations" id="location{{ $location_key+1 }}">
                            <div class="col">
                              <p class="font-16 color-dark-purple txt-blue">{{__('Location')}}<span id="locationLabel1"> {{ $location_key+1 }} {{__('Details')}}</span></p>
                              <div class="row mt-2 mt-md-3 mt-lg-4">
                                <div class=" col-sm-3 col-md-3 col-lg-3 dropdown " >
                                  <select class="form-control multi-location-input select2" required style= "width: 100%;" name="location_{{ $location_key+1 }}">
                                    <option value="">{{__('Select address')}}</option>
                                    @foreach ($addresses as $key => $address)
                                      <option value="{{$address->id}}" {{ $address->address == $booking_location['drop_location'] ? 'selected' : '' }}>{{$address->name}}</option>
                                    @endforeach
                                  </select>
                                  <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                  <div class="form-group mb-4">
                                    <input type="text" id="datetimepicker4{{ $location_key+1 }}" class="form-control form-control-without-border pb-2 multi-location-date" name="date_{{ $location_key+1 }}" required style="background: none; width: 100%;" placeholder="{{__('DD/MM/YYYY')}}" class="active" value="{{ $booking_location['delivered_at'] }}">
                                  </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 mt-1 pickup_rang">
                                  <div class="form-group mb-4 d-flex location_rang_div">
                                    <label><input type="checkbox" id="location_time_range_check_" class="largerCheckbox" name="location_time_range_check_{{ $location_key+1 }}" value="1" {{ $booking_location['delivery_time_from'] != $booking_location['delivery_time_to'] ? 'checked' : '' }}><span></span></label>
                                    <label for="range" class="txt-blue">{{__('Range')}}</label>
                                    <div id="location_time_range" class="row ml-1" style="flex-wrap: unset !important;{{ $booking_location['delivery_time_from'] != $booking_location['delivery_time_to'] ? 'display:flex;' : 'display:none;' }}">
                                      <input type="text" id="basic-location-timepicker1{{ $location_key+1 }}" name="location_hour_from_{{ $location_key+1 }}" class="form-control-without-border pb-2 ml-2 w-0 multi-location-time-from" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%" value="@if($booking_location['delivery_time_from'] != $booking_location['delivery_time_to']){{ date('H:i',strtotime($booking_location['delivery_time_from'])) }}@endif" {{ ($booking_location['delivery_time_from'] != $booking_location['delivery_time_to'] ? 'required' : '') }}>
                                      <input type="hidden" name="location_time_from_{{ $location_key+1 }}">
                                      <span class="txt-blue ml-2 mr-2">TO</span>
                                      <input type="text" id="basic-location-timepicker2{{ $location_key+1 }}" name="location_hour_to_{{ $location_key+1 }}" class="form-control-without-border pb-2 ml-2 w-0 multi-location-time-to" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%" value="@if($booking_location['delivery_time_from'] != $booking_location['delivery_time_to']){{ date('H:i',strtotime($booking_location['delivery_time_to'])) }}@endif" {{ ($booking_location['delivery_time_from'] != $booking_location['delivery_time_to'] ? 'required' : '') }}>
                                      <input type="hidden" name="location_time_to_{{ $location_key+1 }}">
                                    </div>
                                    <div id="location_single_time" class="row" style="{{ $booking_location['delivery_time_from'] == $booking_location['delivery_time_to'] ? 'display:flex;' : 'display:none;' }}">
                                      <input type="text" id="basic-location-timepicker3{{ $location_key+1 }}" name="location_hour_{{ $location_key+1 }}" class="form-control-without-border pb-2 ml-4 multi-location-time" placeholder="{{__('HH:MM')}}" style="background: none;" {{ ($booking_location['delivery_time_from'] == $booking_location['delivery_time_to'] ? 'required' : '') }} value="@if($booking_location['delivery_time_from'] == $booking_location['delivery_time_to']){{ date('H:i',strtotime($booking_location['delivery_time_from'])) }}@endif">
                                      <input type="hidden" name="pickup_time_format_{{ $location_key+1 }}">
                                    </div>
                                  </div>
                                </div>
                                <div class="d-sm-none col-lg-3 d-lg-block"></div>
                              </div>
                              <div class="row">
                                <div class="col-md-5">
                                  <div class="note-area md-lg-5">
                                    <ul class="d-flex m-0 p-0 mb-2">
                                      <a href="javascript:;" class="add-one font-18 d-flex text-show_button" id="add_text" onclick="text_delivery_note();">
                                        <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="text_minus_delivery" style="visibility:hidden ">
                                            <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="text_plus_delivery">
                                      <span class="text ml-1 txt-blue" style="text-decoration: underline;" >{{__('Add')}}&nbsp;{{__('Note')}}</span>
                                      <!-- <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changelocation1Note()" style="display:none"><img src="{{asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label> -->
                                      <label class="mr-2"><a href="javascript:;" class="txt-blue text_clear" onclick="clearlocation1Note()"><img src="{{asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="padding-top: 8px;padding-top: 8px;"></a></label>

                                    </ul>
                                    <div class="form-group">
                                      <textarea class="form-control text_pickUpNote text_clear" id="location{{ $location_key+1 }}Note" name="locationNote_{{ $location_key+1 }}" rows="2">{{ $booking_location['note'] }}</textarea>
                                    </div>
                                  </div>
                                </div>
                                <!-- <div class="note-area md-lg-5">
                                      <ul class="d-flex m-0 p-0 mb-2">
                                        <a href="javascript:;" class="add-one font-18 d-flex multi_pickup-show_button" id="add_note">
                                          <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="multi_minus_delivery" style="visibility:hidden ">
                                            <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="multi_plus_delivery">
                                        <span class="text ml-1 txt-blue" style="text-decoration: underline;" onclick="multi_delivery_note();">Add Note</span>
                                        {{-- <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changePickUpNote()"><img src="{{ asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                                        <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearPickUpNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label> --}}
                                      </ul>
                                      
                                      <textarea rows="2" name="pickup_note" id="multi_pickUpNote" class="form-control" @if(isset($booking_locations_data[0]['note']) && !empty($booking_locations_data[0]['note'])) style="display:block" @else style="resize: none; display:none;" @endif placeholder="Enter Pickup Note" >@if(isset($booking_locations_data[0]['note']) && !empty($booking_locations_data[0]['note'])) {{ $booking_locations_data[0]['note'] }} @endif</textarea>
                                  </div> -->
                                <div class="col-md-7"></div>
                              </div>
                              <p class="font-14 color-dark-purple mt-2 mb-3 txt-blue">{{__('Product Information')}}</p>
                              @if(isset($booking_location['products']) && !empty($booking_location['products']))
                                @foreach($booking_location['products'] as $key => $product)
                                  {{-- @if(isset($booking_location['products']) && !empty($booking_location['products'])) --}}
                                    <?php $product_single_array = [];?>
                                  {{-- @endif --}}
                                  <div class="row mt-lg-3">
                                    <div class="col-md-2 product_checkbox_div">
                                      <label class="ml-3"><input type="checkbox" id="location{{ $location_key+1 }}products" class="largerCheckbox location_products_info" name="location_products_{{ $location_key+1 }}[{{ $key }}]" value="{{ $product['product_id']['id'] }}" @if(!isset($product['is_inserted']) && empty($product['is_inserted'])) checked  @endif ><span></span></label>
                                      <label for="range" class="font-14 txt-blue ml-3" id="location_products">@if(isset($product['product_id']['name']) && !empty($product['product_id']['name'])){{ $product['product_id']['name'] }} @else - @endif</label>
                                    </div>
                                    <div class="col-md-2 delivery_type_div" @if(!isset($product['is_inserted']) && empty($product['is_inserted'])) style="display: block" @else style="display: none" @endif>
                                      <input type="radio" id="delivery_type{{ $location_key+1 }}" name="shipment_type_{{ $location_key+1 }}[{{ $key }}]" style="height:20px; width:20px; vertical-align: text-bottom; " class="delivery_type pb-2" value="pickup" {{ $product['is_pickup'] == '1' ? 'checked' : '' }}>
                                      <label for="range" class="font-14 ">Pickup</label>
                                      <input type="radio" id="delivery_type{{ $location_key+1 }}" name="shipment_type_{{ $location_key+1 }}[{{ $key }}]" style="height:20px; width:20px; vertical-align: text-bottom;" class="delivery_type pb-2 ml-2" value="delivery" {{ $product['is_pickup'] == '0' ? 'checked' : '' }}>
                                      <label for="range" class="font-14 ">Delivery</label><br>
                                    </div>
                                    <div class="col-md-1 quantity_div" @if(!isset($product['is_inserted']) && empty($product['is_inserted'])) style="display: block" @else style="display: none" @endif>
                                      <input type="text" id="quantity" name="quantity_{{ $location_key+1 }}[{{ $key }}]"  class="form-control form-control-without-border pb-2 multiple_quantity quantity_{{ $key }}" style="background: none; width: 90%;" @if(isset($product['qty']) && !empty($product['qty'])) value="{{ $product['qty'] }}" @else value="1" @endif placeholder="{{__('Quantity')}}" >
                                      @if($location_key == '0')
                                        <input type="hidden" id="quantity" name="old_quantity_1[{{ $key }}]" class="multiple_quantity" value="{{ $actual_product_quantity[$product['product_id']['id']]['package_quantity'] }}" placeholder="{{__('Quantity')}}">
                                        <input type="hidden" name="old_product_1[{{ $key }}]" class="" value="{{ $product['product_id']['id'] }}" >
                                      @endif
                                    </div>
                                    <div class="col-md-1 quantity_unit_div" @if(!isset($product['is_inserted']) && empty($product['is_inserted'])) style="display: block" @else style="display: none" @endif>
                                      @if(isset($packageData) && !empty($packageData))
                                        @foreach($packageData as $key1 => $value)
                                          @if($value['id'] == $product['product_type']['id'])
                                            <input type="text" id="quantity_unit" name="quantity_unit_{{ $location_key+1 }}[{{ $key }}]" class="form-control form-control-without-border pb-2" style="background: none; width: 90%;" placeholder="{{__('Package Unit')}}" value="{{ $value['name'] }}" readonly>
                                          @endif
                                        @endforeach
                                      @endif
                                    </div>
                                    <div class="col-md-2 weight_div" @if(!isset($product['is_inserted']) && empty($product['is_inserted'])) style="display: block" @else style="display: none" @endif >
                                      {{-- <div class="row mt-3 mt-md-3 mt-lg-4" > --}}
                                      <input type="text" id="weight" name="weight_{{ $location_key+1 }}[{{ $key }}]" class="form-control form-control-without-border pb-2 multiple_weight weight_{{ $key }}" style="background: none; width: 90%;" placeholder="{{__('Weight')}}" value="{{ $product['weight'] }}">
                                      @if($location_key == '0')
                                        <input type="hidden" id="old_weight" name="old_weight_1[{{ $key }}]" class="multiple_weight" value="{{ $actual_product_quantity[$product['product_id']['id']]['weight'] }}">
                                      @endif
                                      {{-- </div> --}}
                                    </div>
                                    <div class="col-md-2  unit_div" @if(!isset($product['is_inserted']) && empty($product['is_inserted'])) style="display: block" @else style="display: none" @endif >
                                      {{-- <div class="row mt-3 mt-md-3 mt-lg-4" > --}}
                                        <select class="form-control form-control-without-border pb-1 ml-2 multiple_unit" style="background: none; " name="unit_{{ $location_key+1 }}[{{ $key }}]">
                                          <option  disabled selected>{{__('Unit')}}</option>
                                          @foreach ($unitData as $unit)
                                            <option value="{{ $unit['id'] }}" {{ $product['unit']['id'] == $unit['id'] ? 'selected' : '' }}>{{ $unit['name'] }}</option>
                                          @endforeach
                                        </select>
                                      {{-- </div> --}}
                                    </div>
                                  </div>
                                @endforeach
                              @endif                            
                              <p class="text-danger product_error"></p>
                              <!-- </div> -->
                              <div class="d-sm-none col-lg-3 d-lg-block"></div>
                            </div>
                            <div class="specs-check d-flex mt-3">
                            </div>
                            <div class="row">
                              <div class="col-md-7 col-lg-7 col-xl-5">
                                <div class="form-group">
                                </div>
                              </div>
                              <!-- <div class="col-md-5 col-lg-5 col-xl-7">
                                <div class="delete-this" id="locationDelete">
                                    <a style="display: inline-block !important; font-weight:600; !important;" id="locationDelete" href="javascript:;" class="btn btn-light delete-one font-12  d-flex remove_location">
                                      <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                      <span class="text ml-1">Delete</span>
                                    </a>
                                </div>
                              </div> -->
                            </div>
                          </div>
                        @endforeach
                      @else
                        <div class="row locations" id="location1">
                          <div class="col">
                            <p class="font-16 color-dark-purple txt-blue">{{__('Location')}}<span id="locationLabel1"> 1 {{__('Details')}}</span></p>
                            <div class="row mt-2 mt-md-3 mt-lg-4">
                              <div class=" col-sm-3 col-md-3 col-lg-3 dropdown " >
                                <select class="form-control multi-location-input select2" required style= "width: 100%;" name="location_1">
                                  <option value="">{{__('Select address')}}</option>
                                  @foreach ($addresses as $key => $address)
                                    <option value="{{$address->id}}">{{$address->name}}</option>
                                  @endforeach
                                </select>
                                <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                              </div>
                              <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group mb-4">
                                  <input type="text" id="datetimepicker4" class="form-control form-control-without-border pb-2 multi-location-date" name="date_1" required style="background: none; width: 100%;" placeholder="{{__('DD/MM/YYYY')}}" class="active">
                                </div>
                              </div>
                              <div class="col-sm-6 col-md-6 col-lg-6 mt-1 pickup_rang">
                                <div class="form-group mb-4 d-flex location_rang_div">
                                  <label><input type="checkbox" id="location_time_range_check_" class="largerCheckbox" name="location_time_range_check_1" value="1"><span></span></label>
                                  <label for="range" class="txt-blue">{{__('Range')}}</label>
                                  <div id="location_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                    <input type="text" id="basic-location-timepicker1" name="location_hour_from_1" class="form-control-without-border pb-2 ml-2 w-0 multi-location-time-from" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%" >
                                    <input type="hidden" name="location_time_from_1">
                                    <span class="txt-blue ml-2 mr-2">{{__('TO')}}</span>
                                    <input type="text" id="basic-location-timepicker2" name="location_hour_to_1" class="form-control-without-border pb-2 ml-2 w-0 multi-location-time-to" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%" >
                                    <input type="hidden" name="location_time_to_1">
                                  </div>
                                  <div id="location_single_time" class="row" style="display: flex;">
                                    <input type="text" id="basic-location-timepicker3" name="location_hour_1" class="form-control-without-border pb-2 ml-4 multi-location-time" placeholder="{{__('HH:MM')}}" style="background: none;" required="required">
                                    <input type="hidden" name="pickup_time_format_1">
                                  </div>
                                </div>
                              </div>
                              <div class="d-sm-none col-lg-3 d-lg-block"></div>
                            </div>
                            <div class="row">
                              <div class="col-md-5">
                                <div class="note-area md-lg-5">
                                  <ul class="d-flex m-1 p-1 mb-2">
                                    <a href="javascript:;" class="add-one font-18 d-flex location-show_button" id="add_note" onclick="location_note();">
                                        <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="minus_location" style="visibility:hidden ">
                                        <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="plus_location">
                                        <span class="text ml-1 txt-blue" style="text-decoration: underline; margin-top: 4px;" >{{__('Add')}}&nbsp;{{__('Note')}}</span>
                                        <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="LocationClearNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="padding-top: 8px;"></a></label>
                                        {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                                     </a><br>
                                    </ul>
                                    <textarea rows="2" name="locationNote_1" id="location1Note" class="form-control multi_location_note" style="display:none; resize: none;" placeholder="{{__('Enter Note')}}"></textarea>
                                </div>
                              </div>
                              <div class="col-md-7"></div>
                            </div>
                            <p class="font-14 color-dark-purple mt-2 mb-3 txt-blue">Product Information</p>
                            @if(isset($products) && !empty($products))
                              @foreach($products as $key => $product)
                                <div class="row mt-lg-3">
                                  <div class="col-md-2 product_checkbox_div">
                                    <label class="ml-3"><input type="checkbox" id="location1products" class="largerCheckbox location_products_info" name="location_products_1[{{ $key }}]" value="{{ $product['product_id'] }}" ><span></span></label>
                                    <label for="range" class="font-14 txt-blue ml-3" id="location_products">@if(isset($product['product_name']) && !empty($product['product_name'])){{ $product['product_name'] }} @else - @endif</label>
                                  </div>
                                  <div class="col-md-2 delivery_type_div" style="display:none;">
                                    <input type="radio" id="delivery_type1" name="shipment_type_1[{{ $key }}]" style="height:20px; width:20px; vertical-align: text-bottom; " class="delivery_type pb-2" value="pickup" checked>
                                    <label for="range" class="font-14 ">{{__('Pickup')}}</label>
                                    <input type="radio" id="delivery_type1" name="shipment_type_1[{{ $key }}]" style="height:20px; width:20px; vertical-align: text-bottom;" class="delivery_type pb-2 ml-2" value="delivery" disabled>
                                    <label for="range" class="font-14 ">{{__('Delivery')}}</label><br>
                                  </div>
                                  <div class="col-md-1 quantity_div" style="display:none">
                                    <input type="text" id="quantity" name="quantity_1[{{ $key }}]"  class="form-control form-control-without-border pb-2 multiple_quantity quantity_{{ $key }}" style="background: none; width: 90%;" @if(isset($product['package_quantity']) && !empty($product['package_quantity'])) value="{{ $product['package_quantity'] }}" @else value="1" @endif placeholder="{{__('Quantity')}}" >
                                    <input type="hidden" id="quantity" name="old_quantity_1[{{ $key }}]" class="multiple_quantity" @if(isset($product['package_quantity']) && !empty($product['package_quantity'])) value="{{ $product['package_quantity'] }}" @else value="1" @endif placeholder="{{__('Quantity')}}">
                                  </div>
                                  <div class="col-md-1 quantity_unit_div" style="display:none">
                                    @if(isset($packageData) && !empty($packageData))
                                      @foreach($packageData as $key1 => $value)
                                        @if($value['id'] == $product['package'])
                                          <input type="text" id="quantity_unit" name="quantity_unit_1[{{ $key }}]" class="form-control form-control-without-border pb-2" style="background: none; width: 90%;" placeholder="{{__('Package Unit')}}" value="{{ $value['name'] }}" readonly>
                                        @endif
                                      @endforeach
                                    @endif
                                  </div>
                                  <div class="col-md-2 weight_div" style="display:none;" >
                                    {{-- <div class="row mt-3 mt-md-3 mt-lg-4" > --}}
                                    <input type="text" id="weight" name="weight_1[{{ $key }}]" class="form-control form-control-without-border pb-2 multiple_weight weight_{{ $key }}" style="background: none; width: 90%;" placeholder="{{__('Weight')}}"  value="{{ $product['weight'] }}">
                                    <input type="hidden" id="old_weight" name="old_weight_1[{{ $key }}]" class="multiple_weight" value="{{ $product['weight'] }}">
                                    {{-- </div> --}}
                                  </div>
                                  <div class="col-md-2  unit_div" style="display:none;" >
                                    {{-- <div class="row mt-3 mt-md-3 mt-lg-4" > --}}
                                      <select class="form-control form-control-without-border pb-1 ml-2 multiple_unit" style="background: none; " name="unit_1[{{ $key }}]">
                                        <option  disabled selected>{{__('Unit')}}</option>
                                        @foreach ($unitData as $unit)
                                          <option value="{{ $unit['id'] }}" {{ $product['unit'] == $unit['id'] ? 'selected' : '' }}>{{ $unit['name'] }}</option>
                                        @endforeach
                                      </select>
                                    {{-- </div> --}}
                                  </div>
                                </div>
                              @endforeach
                            @endif
                            <p class="text-danger product_error"></p>
                            <!-- </div> -->
                            <div class="d-sm-none col-lg-3 d-lg-block"></div>
                          </div>
                          <div class="specs-check d-flex mt-3">
                          </div>
                          <div class="row">
                            <div class="col-md-7 col-lg-7 col-xl-5">
                              <div class="form-group">
                              </div>
                            </div>
                            <!-- <div class="col-md-5 col-lg-5 col-xl-7">
                              <div class="delete-this" id="locationDelete">
                                  <a style="display: inline-block !important; font-weight:600; !important;" id="locationDelete" href="javascript:;" class="btn btn-light delete-one font-12  d-flex remove_location">
                                    <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                    <span class="text ml-1">Delete</span>
                                  </a>
                              </div>
                            </div> -->
                          </div>
                        </div>
                      @endif
                    </div>
                  </div>
                  <div class="extra_locations" id="extra_locations"></div>
                  <a href="javascript:;" class="add-more-one font-20 d-flex add_field_button btn btn-outline-info add_stops" onclick="clone()" id="addLocation" style="border-radius: 16px; width: 20%; padding-left: 6%;">
                    <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="Plus" class="img-fluid">
                    <span class="text ml-1 text-decoration-none">{{__('Add stop')}}</span>
                  </a>

                  <div class="col-md-12" style="margin-top: 20px">
                    <p class="text-danger final_product_error"></p>
                  </div>

                  <div class="btn-block d-flex">
                    {{-- <button type="submit" class="btn font-10 mt-3 btn-primary multiple_shipment_submit" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" id="continuebtn">Insert Quote</button> --}}
                    <button type="button" class="btn font-10 mt-3 btn-primary multiple_shipment_submit" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" id="continuebtn">{{__('Insert Quote')}}</button>
                  </div>    
                </div>  
              </form>
          </div>
        </div>
     </div>
    </div>

    <div class="pro-content  mb-4 location_info" style="display:none"> 
      <div class="row locations" id="location_hidden" style="background: none; margin-top: 2%;">
          <div class="col">
            <!-- <div class="pro-info input_fields_wrap"> -->
              <p class="font-16 color-dark-purple txt-blue">{{__('Location')}}<span id="locationLabel1"> 1 {{__('Details')}}<span></p>
                <div class="row mt-2 mt-md-3 mt-lg-4">
                    <div class=" col-sm-3 col-md-3 col-lg-3 dropdown " >
                        <select class="form-control location-input" required style= "width: 100%;" placeholder="DD/MM/YYYY">
                            <option value="">{{__('Select address')}}</option>
                            @foreach ($addresses as $key => $address)
                            <option value="{{$address->id}}">{{$address->name}}</option>
                            @endforeach
                        </select>
                        <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                  </div>
                  <div class="col-sm-3 col-md-3 col-lg-3">
                      <div class="form-group mb-4 location_rang_div datepicker">
                          <!-- <input type="text" id="basic-delivery-datepicker2" name="delivery_date1" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> -->
                          <input type="text" id="datetimepicker4" class="form-control form-control-without-border pb-2 multi-location-date" name="delivery_date1" required style="background: none; width: 100%;border-radius:0" placeholder="{{__('DD/MM/YYYY')}}" class="active">
                      </div>
                  </div>
                  <div class="col-sm-6 col-md-6 col-lg-6 mt-1 pickup_rang">
                      <div class="form-group mb-4 d-flex location_rang_div">
                          <label><input type="checkbox" id="location_time_range_check_" class="largerCheckbox" name="location_time_range_check_[]" value="1"><span></span></label>
                          <label for="range" class="txt-blue">{{__("Range")}}</label>
                          <div id="location_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                              <input type="text" id="basic-location-timepicker1" name="location_hour_from[]" class="form-control-without-border pb-2 ml-2 w-0 multi-location-time-from" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%">
                              <input type="hidden" name="location_time_from">
                              <span class="txt-blue ml-2 mr-2">{{__('TO')}}</span>
                              <input type="text" id="basic-location-timepicker2" name="location_hour_to[]" class="form-control-without-border pb-2 ml-2 w-0 multi-location-time-to" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%">
                              <input type="hidden" name="location_time_to">
                          </div>
                          <div id="location_single_time" class="row" style="display: flex;">
                              <input type="text" id="basic-location-timepicker3" name="location_hour[]" class="form-control-without-border pb-2 ml-4 multi-location-time " placeholder="{{__('HH:MM')}}" style="background: none;" required="required">
                              <input type="hidden" name="pickup_time_format[]">
                          </div>
                      </div>
                  </div>
                  <div class="d-sm-none col-lg-3 d-lg-block"></div>
              </div>
              <div class="row">
              <div class="col-md-5">
                <div class="note-area md-lg-5 ">
                  <ul class="d-flex m-1 p-1 mb-2">
                    <a href="javascript:;" class="add-one font-18 d-flex multiple-location-show_button" id="add_note_multi" >
                      <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="multi_minus_location" style="visibility:hidden ">
                      <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="multi_plus_location">
                        <span class="text ml-1 txt-blue" style="text-decoration: underline;margin-top: 4px;" id="Multiple_Location_Note" >{{__('Add')}}&nbsp;{{__('Note')}}</span>
                        <label class="mr-2"><a href="javascript:;" class="txt-blue" id="Multi_LocationClearNote"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="padding-top: 8px;"></a></label>
                     </a><br>
                    </ul>
                    <textarea class="form-control multi_location_note" id="Multiplelocation1Note" name="location1Note" rows="2" placeholder="{{__('Enter Note')}}" style="display:none; resize: none;"></textarea>
                </div>
              </div>
              <div class="col-md-7"></div>
            </div>
            <p class="font-14 color-dark-purple mt-2 mb-3 txt-blue">{{__('Product Information')}}</p>
            @if(isset($products) && !empty($products))
              @foreach($products as $key => $product)
                <div class="row mt-lg-3 product_info_{{ $key }}">
                  <div class="col-md-2 product_checkbox_div">
                    <label class="ml-3"><input type="checkbox" id="location1products" class="largerCheckbox location_products_info" name="location_products_1[]" value="{{ $product['product_id'] }}" ><span></span></label>
                    <label for="range" class="font-14 txt-blue ml-3" id="location_products">@if(isset($product['product_name']) && !empty($product['product_name'])){{ $product['product_name'] }} @else - @endif</label>
                  </div>
                  <div class="col-md-2 delivery_type_div" style="display:none;">
                      <input type="radio" id="delivery_type1" name="shipment_type_1[]" style="height:20px; width:20px; vertical-align: text-bottom; " class="delivery_type multi-location-type1 pb-2" value="pickup" checked>
                      <label for="range" class="font-14 ">{{__('Pickup')}}</label>
                      <input type="radio" id="delivery_type1" name="shipment_type_1[]" style="height:20px; width:20px; vertical-align: text-bottom;" class="delivery_type multi-location-type2 pb-2 ml-2" value="delivery">
                      <label for="range" class="font-14 ">{{__('Delivery')}}</label><br>
                  </div>
                  <div class="col-md-1 quantity_div" style="display:none">
                    {{-- <input type="text" id="quantity" name="quantity_1[]" onkeypress="return numberValidate(event)" class="form-control form-control-without-border pb-2 multiple_quantity" style="background: none; width: 90%;" @if(isset($product['package_quantity']) && !empty($product['package_quantity'])) value="{{ $product['package_quantity'] }}" @else value="1" @endif placeholder="{{__('Quantity')}}" min="1" max="{{ $product['package_quantity'] }}"> --}}
                    <input type="text" id="quantity" name="quantity_1[]"  class="form-control form-control-without-border pb-2 multiple_quantity" style="background: none; width: 90%;" @if(isset($product['package_quantity']) && !empty($product['package_quantity'])) value="{{ $product['package_quantity'] }}" @else value="1" @endif placeholder="{{__('Quantity')}}" min="1">
                  </div>
                  <div class="col-md-1 quantity_unit_div" style="display:none">
                      @if(isset($packageData) && !empty($packageData))
                        @foreach($packageData as $key1 => $value)
                        @if($value['id'] == $product['package'])
                          <input type="text" id="quantity_unit" name="quantity_unit_1[]" class="form-control form-control-without-border pb-2 multi_quantity_unit" style="background: none; width: 90%;" placeholder="{{__('Package Unit')}}" value="{{ $value['name'] }}" readonly>
                        @endif
                        @endforeach
                      @endif
                  </div>
                  <div class="col-md-2 weight_div" style="display:none;" >
                    {{-- <div class="row mt-3 mt-md-3 mt-lg-4" > --}}
                      {{-- <input type="text" id="weight" name="weight_1[]" onkeypress="return numberValidate(event)" class="form-control form-control-without-border pb-2 multiple_weight" style="background: none; width: 90%;" placeholder="{{__('Weight')}}" min="1" max="{{ $product['weight'] }}" value="{{ $product['weight'] }}"> --}}
                      <input type="text" id="weight" name="weight_1[]" class="form-control form-control-without-border pb-2 multiple_weight" style="background: none; width: 90%;" placeholder="{{__('Weight')}}" value="{{ $product['weight'] }}">
                    {{-- </div> --}}
                  </div>
                  <div class="col-md-2 unit_div" style="display:none;" >
                    {{-- <div class="row mt-3 mt-md-3 mt-lg-4" > --}}
                      <select class="form-control form-control-without-border pb-1 ml-2 multiple_unit" style="background: none; " name="unit_1[]">
                        <option  disabled selected>{{__('Unit')}}</option>
                          @foreach ($unitData as $unit)
                              <option value="{{ $unit['id'] }}" {{ $product['unit'] == $unit['id'] ? 'selected' : '' }}>{{ $unit['name'] }}</option>
                          @endforeach
                      </select>
                    {{-- </div> --}}

                  </div>
                </div>
              @endforeach
            @endif
            <p class="text-danger product_error"></p>
            <div class="specs-check d-flex mt-3">
            </div>
            <div class="row">
               <div class="col-md-7 col-lg-7 col-xl-5">
                 <div class="form-group">
    
                 </div>
               </div>
    
               <div class="col-md-5 col-lg-5 col-xl-7">
                  <div class="delete-this" id="locationDelete" style="float: right">
                     <a style="display: inline-block !important; font-weight:600; !important;" id="locationDelete" href="javascript:;" class="btn btn-light delete-one font-12  d-flex remove_location">
                       <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                       <span class="text ml-1">{{__('Delete')}}</span>
                     </a>
                  </div>
               </div>
            </div>
          </div>
      </div>
    </div>
<div class="modal fade add-address" id="add-address"   aria-labelledby="exampleModalCenterTitle" >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">{{ __('Create New Facility Address')}}</div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-6">
          <div class="card">
            <div class="card-body table-responsive" id="from">
              {{-- <form action="{{ route('shipper.shipment.address.submit') }}" method="POST" id="address_form"> --}}
              <form action="javascript:void(0)" id="address_form" method="POST">
                @csrf
                <div class="form-group mb-3">
                  <label for="name" class="mb-2 text-grey font-14">{{__('Name')}}</label>
                  <input type="text" id="name" maxlength="40" autocomplete="off" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="{{__('Enter Name')}}" >
                  <span class="error text-danger address_errors" id="name_error"></span>
                </div>
  
                <div class="form-group mb-3">
                  <label for="first-name" class="mb-2 text-grey font-14" >{{__('Address')}}</label>
                  <input type="text" id="autocomplete" name="address" autocomplete="off" parsley-trigger="change" required class="autocomplate-input form-control form-control-without-border fulladdress"  placeholder="{{__('Enter Address')}}">
                  <input type="hidden" id="latitude" name="lat" value="">
                  <input type="hidden" id="longitude" name="lng" value="">
                  <input type="hidden" id="post_code" name="post_code" value="">
                  <span class="error text-danger address_errors" id="lat_error"></span>
                </div>

                <div class="form-group mb-3">
                  <label for="first-name" class="mb-2 text-grey font-14">{{__('City')}}</label>
                  <input type="text" id="city" name="city" autocomplete="off" class="autocomplate-input form-control form-control-without-border fulladdress" placeholder="{{__('Enter City')}}">
                  
                  <span class="error text-danger address_errors" id="city_error"></span>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group  mb-3">
                      <label for="first-name" class="mb-2 text-grey font-14">{{__('Phone')}}</label>
                      <input type="call" id="phone" name="phone" autocomplete="off" maxlength="15" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="{{__('Phone')}}">
                      <span class="error text-danger address_errors" id="phone_error"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group  mb-3">
                    <label for="first-name" class="mb-2 text-grey font-14">{{__('Email')}}</label>
                      <input type="email" id="email" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="{{__('Email')}}">
                      <span class="error text-danger address_errors" id="email_error"></span>
                    </div>
                  </div>
                </div>
  
                <div class="row">
                  <div class="col">
                    <div class="add-facility">
                      <div class="form-group checkbox-inline">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="restroom_service" id="Restrooms" checked="checked"><span></span></label>
                        <label for="Restrooms" class="font-15">{{__('Restrooms')}}</label>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="food_service" id="Food" checked="checked"><span></span></label>
                        <label for="Food" class="font-15">{{__('Food')}}</label>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="rest_area_service" id="Driver Rest Area" checked="checked"><span></span></label>
                        <label for="Driver Rest Area" class="font-15">{{__('Driver Rest Area')}}</label>
                      </div>
       
                    </div>
                  </div>
                </div>
            
                <div class="btn-block d-flex">
                  <button type="button" class="btn w-100 btn-white mr-2 cancel-btn" data-dismiss="modal" aria-label="Close">{{ __('Cancel')}}</button>
                  {{-- <a href="#" onclick="document.getElementById('address_form').reset(); document.getElementById('from').value = null;  return false;" class="btn w-100 btn-white mr-2 cancel-btn" aria-label="Close" data-dismiss="modal" id="close-map">Cancel</a> --}}
                  <button type="submit" class="btn w-100 btn-primary">{{ __('Save')}}</button>
                </div>
            <!-- <div class="btn-block d-flex">
              <a href="{{ route('shipper.address.index') }}" class="btn w-100 btn-white mr-2" >Cancel</a>
              <button type="submit" class="btn w-100 btn-primary">Save</button>
            </div>
             -->
              </form>
            </div>
          </div>
      
        </div>
        <div class="col-xl-6">
          <div id="map"></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script> --}}
<script src="{{ URL::asset('shipper/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/js/pages/form-pickers.init.js')}}"></script>

<script type="text/javascript">

    function preventBack() { window.history.forward(); }  
    setTimeout("preventBack()", 0);  
    window.onunload = function () { null };
  $(document).ready(function(){
        //$(".location_products_info").click(function () {
        $(document).on("click",".location_products_info",function(){
            if ($(this).is(":checked")) {
                $(this).closest('.row').find('.delivery_type_div').show();
                $(this).closest('.row').find('.quantity_div').show();
                $(this).closest('.row').find('.multiple_quantity').attr('required',true);
                $(this).closest('.row').find('.quantity_unit_div').show();
                $(this).closest('.row').find('.weight_div').show();
                $(this).closest('.row').find('.multiple_weight').attr('required',true);
                $(this).closest('.row').find('.unit_div').show();
                $(this).closest('.row').find('.multiple_unit').attr('required',true);
            } else {
                $(this).closest('.row').find('.delivery_type_div').hide();
                $(this).closest('.row').find('.quantity_div').hide();
                $(this).closest('.row').find('.multiple_quantity').attr('required',false);
                $(this).closest('.row').find('.quantity_unit_div').hide();
                $(this).closest('.row').find('.weight_div').hide();
                $(this).closest('.row').find('.multiple_weight').attr('required',false);
                $(this).closest('.row').find('.unit_div').hide();
                $(this).closest('.row').find('.multiple_unit').attr('required',false);
            }
        });
    });
</script>
<script type="text/javascript">
  $(document).ready(function(){
        $(document).on("click",".text-show_button",function(){
            if ($(this).is(":checked")) {
                $(this).closest('.row').find('.text_pickUpNote').toggle();
                $(this).closest('.row').find('.text_clear').val('');                 
            } else {
                $(this).closest('.row').find('.text_pickUpNote').toggle();
               $(this).closest('.row').find('.text_clear').val('');  
            }
        });
    });

  $(document).ready(function(){
        $(document).on("click",".text_clear",function(){
            if ($(this).is(":checked")) {
                $(this).closest('.row').find('.text_clear').val('');                 
            } else {
               $(this).closest('.row').find('.text_clear').val('');  
            }
        });
    });
  
</script>
<script type="text/javascript">
  $(function () {
          // $("#datetimepicker4").flatpickr({minDate: "today"});
          var pickup_date = $('#datetimepicker4').val();
           $("#datetimepicker5").flatpickr({minDate: "today",disableMobile: "true"});
          var pickup_date = $('#datetimepicker5').val();
      });

  function check_date_range() {
    var pickup_from = $('#basic-pickup-timepicker1111').val();
    var pickup_to = $('#basic-pickup-timepicker2222').val();
    var drop_from = $('#basic-pickup-timepicker4444').val();
    var drop_to = $('#basic-pickup-timepicker5555').val();
    var pickup_date = $('#basic-datepicker111').val();
    var dropoff_date = $('#basic-delivery-datepicker111').val();
    if(pickup_from == pickup_to){
      document.getElementById('errorShowSameTime').style.display = 'block';
      $('#continuebtn').prop('disabled', true);
    }else if(drop_to == drop_from){
      document.getElementById('errorShowSameTime').style.display = 'block';
      $('#continuebtn').prop('disabled', true);
    }else{
      if(pickup_date == dropoff_date){
        if(drop_from >= pickup_to && drop_to >= drop_from){
          document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
        }else{
          document.getElementById('errorShowSameTime').style.display = 'block';
          $('#continuebtn').prop('disabled', true);
        }

      }
    }
  }
   
</script> 
<script type="text/javascript">
  function PickupClearNote(){
    document.getElementById('pickUpNote').value = "";
    document.getElementById('pickUpNote').readOnly =false;
  }

  function ClearMultiNote(){
    document.getElementById('multi_pickUpNote').value = "";
    document.getElementById('multi_pickUpNote').readOnly =false;
  }

  function DeliveryClearNote() {
        document.getElementById('text_id').value = "";
        document.getElementById('text_id').readOnly =false;
    }

    function multiClearNote() {
        document.getElementById('multi_text_id').value = "";
        document.getElementById('multi_text_id').readOnly =false;
    }

    function LocationClearNote(){
    document.getElementById('location1Note').value = "";
    document.getElementById('location1Note').readOnly =false;
  }

  function clearlocation1Note() {
        document.getElementsByClassName('text_clear').value = "";
        document.getElementsByClassName('text_clear').readOnly =false;
    }

    
</script>
<script>

  $('#basic-datepicker111').flatpickr({minDate: "today",disableMobile: "true",});
  $('#basic-delivery-datepicker111').flatpickr({minDate: "today",disableMobile: "true",});
  $('#basic-pickup-timepicker6666').on("input", function() {
      // var pickup_date = $('#basic-datepicker').val();
      // var dropoff_date = $('#basic-delivery-datepicker').val();
      // var stime = $('#basic-pickup-timepicker3333').val();   
      // var etime = this.value;
      // if(pickup_date == dropoff_date){
      //   if(stime >= etime){
      //   document.getElementById('errorShowSameTime').style.display = 'block';
      //    $('#continuebtn').prop('disabled', true);
      //   }else{
      //     document.getElementById('errorShowSameTime').style.display = 'none';
      //     $('#continuebtn').prop('disabled', false);
      //   }
      // }else{
      //   document.getElementById('errorShowSameTime').style.display = 'none';
      //     $('#continuebtn').prop('disabled', false);
      // }
      DirectshipmentValidate();
     
  });

  $('#basic-delivery-datepicker').on("input", function() {
    // var pickup_date = $('#basic-datepicker').val();
    //   var dropoff_date = $('#basic-delivery-datepicker').val();
    //   var stime = $('#basic-pickup-timepicker3333').val();   
    //   var etime = $('#basic-pickup-timepicker6666').val();
    //   // console.log(stime,etime);
    //   if(pickup_date == dropoff_date){
    //     if(stime >= etime){
    //     document.getElementById('errorShowSameTime').style.display = 'block';
    //      $('#continuebtn').prop('disabled', true);
    //     }else{
    //       document.getElementById('errorShowSameTime').style.display = 'none';
    //       $('#continuebtn').prop('disabled', false);
    //     }
    //   }else{
    //     document.getElementById('errorShowSameTime').style.display = 'none';
    //       $('#continuebtn').prop('disabled', false);
    //   }
    DirectshipmentValidate();
     
  });

  $('select[name=pickupAddress]').on("change", function() {
    // var pickup_address = $(this).val();
    // var dropoff_address = $('select[name=deliveryAddress]').val();
    // if(pickup_address == dropoff_address){
    //   $('#same_address_error').show();
    //   $('#continuebtn').prop('disabled', true);
    // }else{
    //   $('#same_address_error').hide();
    //   $('#continuebtn').prop('disabled', false);
    // }
    DirectshipmentValidate();
  });

  $('select[name=deliveryAddress]').on("change", function() {
    // var dropoff_address = $(this).val();
    // var pickup_address = $('select[name=pickupAddress]').val();
    // if(pickup_address == dropoff_address){
    //   $('#same_address_error').show();
    //   $('#continuebtn').prop('disabled', true);
    // }else{
    //   $('#same_address_error').hide();
    //   $('#continuebtn').prop('disabled', false);
    // }
    DirectshipmentValidate();
  });

  


     let pickupid;

    function addPickupAddress(val, addressName, address){
        pickupid = val;

        let pickupAddress = document.getElementById('pickupAddress');
        let pickUpAddressTag = document.getElementById('PickupdropdownMenuLink');
        document.getElementById('pickupAddress').value = val;
        pickUpAddressTag.text = addressName+' '+address;
        document.getElementById('errorShowPickup').style.display = 'none';
        return false;
    }

    function addDeliveryAddress(val, addressName, address){
        if(pickupid == val){
          document.getElementById('errorShowDeliveryPickup').style.display = 'block';
        }else{
          document.getElementById('errorShowDeliveryPickup').style.display = 'none';
        }

        let deliveryAddress = document.getElementById('deliveryAddress');
        let deliveryAddressTag = document.getElementById('deliveryDropdownMenuLink');
        document.getElementById('deliveryAddress').value = val;
        deliveryAddressTag.text = addressName+' '+address;
        document.getElementById('errorShowDelivery').style.display = 'none';
        return false;
    }

    $(document).on("change","#location_time_range_check_",function(){
        var location_rang_div = $(this).closest(".location_rang_div");
        if($(this).is(':checked') == true){
            location_rang_div.find("#location_time_range").show();
            location_rang_div.find("#location_single_time").hide();
            location_rang_div.find('.multi-location-time').attr('required',false);
            location_rang_div.find('.multi-location-time-from').attr('required',true);
            location_rang_div.find('.multi-location-time-to').attr('required',true);
        }else{
            location_rang_div.find("#location_time_range").hide();
            location_rang_div.find("#location_single_time").show();
            location_rang_div.find('.multi-location-time').attr('required',true);
            location_rang_div.find('.multi-location-time-from').attr('required',false);
            location_rang_div.find('.multi-location-time-to').attr('required',false);
        }
    });

    function DirectshipmentValidate(){
    var pickup_address = $('select[name=pickupAddress]').val();
    var dropoff_address = $('select[name=deliveryAddress]').val();
    if(pickup_address == dropoff_address && pickup_address != '' && dropoff_address != ''){
      $('#same_address_error').show();
      $('#continuebtn').prop('disabled', true);
      return false;
    }else{
      $('#same_address_error').hide();
    }
    var pickup_date = $('#basic-datepicker111').val();
    var dropoff_date = $('#basic-delivery-datepicker111').val();
    var pickup_range_select = $('#pickup_time_range_check:checked').val();
    var dropoff_range_select = $('#delivery_time_range_check:checked').val();
   /* var stime = $('#basic-pickup-timepicker3333').val();   
    var etime = $('#basic-pickup-timepicker6666').val();*/
    // console.log(stime,etime);

    /// New Coded Here 08-02-2022
    var start_time = $('#basic-pickup-timepicker3333').val();  
    var end_time = $('#basic-pickup-timepicker6666').val();

          const convertTime12to24 = (start_time) => {
          const [time, modifier] = start_time.split(' ');

  let [hours, minutes] = time.split(':');

  if (hours === '12') {
    hours = '00';
  }

  if (modifier === 'PM') {
    hours = parseInt(hours, 10) + 12;
  }

  return `${hours}.${minutes}`;
}
   const convertTime12 = (end_time) => {
  const [time, modifier] = end_time.split(' ');

  let [hours, minutes] = time.split(':');

  if (hours === '12') {
    hours = '00';
  }

  if (modifier === 'PM') {
    hours = parseInt(hours, 10) + 12;
  }
  return `${hours}.${minutes}`;
}
var sstring=convertTime12to24(start_time);
var estring=convertTime12(end_time);
var stime=parseFloat(sstring);
var etime=parseFloat(estring);
    if(pickup_date != '' && dropoff_date != ''){
      if(pickup_date == dropoff_date || pickup_date <= dropoff_date){
        /*if(stime >= etime && pickup_date == dropoff_date){
          document.getElementById('errorShowSameTime').style.display = 'block';
          $('#continuebtn').prop('disabled', true);
          return false;
        }*/
        if(stime >= etime && pickup_date == dropoff_date){
          console.log('et4')
          document.getElementById('errorShowSameTime').style.display = 'block';
          $('#continuebtn').prop('disabled', true);
          return false;
        }else if(stime == etime && pickup_date == dropoff_date){
          console.log('et4');
          console.log('test');
          console.log('hi');
          console.log(stime);
          console.log(etime);
          document.getElementById('errorShowSameTime').style.display = 'block';
          $('#continuebtn').prop('disabled', true);
          return false;
        }
      }else{
        document.getElementById('errorShowSameTime').style.display = 'block';
        $('#continuebtn').prop('disabled', true);
        return false;
      } 
    }
    
    $('#same_address_error').hide();
    document.getElementById('errorShowSameTime').style.display = 'none';
    $('#continuebtn').prop('disabled', false);
  }
/////////
    var time_counter = 0;
    <?php if(isset($booking_locations_data) && !empty($booking_locations_data) && $booking_data['booking_type'] == 'multiple_shipment'){?>
      time_counter = <?=count($booking_locations_data);?>;
    <?php }?>
    if(time_counter>0){
      for(var t=1;t<=time_counter;t++){
        $('#basic-location-timepicker1'+t).flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "h:i K",
            disableMobile: "true"
        });    
        $('#basic-location-timepicker2'+t).flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "h:i K",
            disableMobile: "true"
        });
        $('#basic-location-timepicker3'+t).flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "h:i K",
                disableMobile: "true"
        });

        $("#datetimepicker4"+t).flatpickr({minDate: "today",disableMobile: "true"});
      }
    }else{
      $('#basic-location-timepicker1').flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "h:i K",
            disableMobile: "true"
        });    
        $('#basic-location-timepicker2').flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "h:i K",
            disableMobile: "true"
        });
        $('#basic-location-timepicker3').flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "h:i K",
                disableMobile: "true"
        });
        $("#datetimepicker4").flatpickr({minDate: "today",disableMobile: "true"});
    }

    // $('#basic-location-timepicker1').flatpickr({
    //         enableTime: true,
    //         noCalendar: true,
    //         dateFormat: "H:i",
    // });
    // $('#basic-location-timepicker2').flatpickr({
    //         enableTime: true,
    //         noCalendar: true,
    //         dateFormat: "H:i",
    // });
    // $('#basic-location-timepicker3').flatpickr({
    //         enableTime: true,
    //         noCalendar: true,
    //         dateFormat: "H:i",
    // });


    $(document).ready(function()
        {
            $("#continuebtn").click(function()
            {
                let pickUpvalue = document.getElementById('pickupAddress');
                let deliveryvalue = document.getElementById('deliveryAddress');
                var type = true;

                if(pickUpvalue.value === '0'){
                    document.getElementById('errorShowPickup').style.display = 'block';
                    type = false;
                }
                else{
                    document.getElementById('errorShowPickup').style.display = 'none';
                    type = false;
                }


                if(deliveryvalue.value === '0'){
                    document.getElementById('errorShowDelivery').style.display = 'block';
                    type = false;
                }
                else{
                    document.getElementById('errorShowDelivery').style.display = 'none';
                    type = false;
                }

                let stime = document.getElementById('basic-pickup-timepicker3333');
                let etime = document.getElementById('basic-pickup-timepicker6666');
    

        });

        $(document).on('click','.remove_location',function () {
          $(this).closest('.locations').remove();
          var length = $('.multiple_shipment_location_length').val();
          length = parseInt(length) - 1;
          $('.multiple_shipment_location_length').val(length);
          change_name_id();
        })

        $(document).on('submit','#address_form',function(){
          // var form_data = new FormData(this);
          $('.address_errors').text('');
          var form = $(this);
          $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
              type: "POST",
              url: '{{ route('shipper.shipment.address.submit') }}',
              data: form.serialize(), // <--- THIS IS THE CHANGE
              dataType: "html",
              success: function(data){
                var response = JSON.parse(data);
                if(response.status == false){
                  $.each(response.message, function(key,val){
                    if(val != ''){
                      $('#'+key+'_error').html(val[0]);
                    }else{
                      $('#'+key+'_error').html('');
                    }
                  })
                }else{
                  get_facility_address();
                  $('#address_form').trigger('reset');
                  // $('#country').val('').trigger('change');
                  // $('#state').val('').trigger('change');
                  // $('#city').val('').trigger('change');
                  $('.cancel-btn').click();
                  get_facility_address();
                }
              },
              error: function() { alert("Error posting feed."); }
            });
        });
    });

    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };  

    $(document).on('click','.multiple_shipment_submit',function(e){
      var total_locations = $('.row .locations').length;
      // console.log(total_locations)
      if(total_locations < 2){
        $('#location1 .product_error').text('Please add at least two locations.');
        return false;
      }
      var extra_products_length = $('.extra_locations .locations').length;
      var data = {};
      // var formData = $('#multiple-form').serializeArray();
      // var location_1_product_length = $("#location1 .location_products_info:checked").length;
      // formData.forEach(item=>{
      //   data[item.name] = item.value;
      // });
      // if(location_1_product_length == 0){
      //   $('#location1 .product_error').text('Please select at least one product.');
      //   return false;
      // }else{
      //   $('#location1 .product_error').text('');
      // }
      if(total_locations > 0){
        var number_of_locations = 0;
        var array = [];
        for(var i = 0; i < total_locations; i++ ){
          number_of_locations = parseInt(i+1);
          // console.log(number_of_locations);
          var location_product_length = $("#location"+number_of_locations+" .location_products_info:checked").length;
          if(location_product_length == 0){
            $('#location'+number_of_locations+' .product_error').text('Please select at least one product.');
            return false;
          }else{
            $('#location'+number_of_locations+' .product_error').text('');
          }
        }
      }
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        type: "post",
        url: '{{ url('shipper/shipment/check_validations_edit') }}',
        data: $('#multiple-form').serializeObject(),
        success: function(data){
          var res = JSON.parse(data);
          // console.log(res)
          if(res != ''){
            $('.final_product_error').text(res);
            return false;
          }else{
            $('.final_product_error').text('');
            $('#multiple-form').submit();
          }
        },
        error: function() { alert("Error posting feed."); }
      });
      // return false;

      // console.log(total_locations);
      // $('#MultipleShip .extra_locations .locations').each(function(key, value){
      //   var row_key = parseInt(key) + 2;
      //   console.log(row_key);
      //   var length = $(value).find('.product_checkbox_div .location_products_info').length;
      //   if(length != '' || length != 0){
      //     var total_pickup = 0;
      //     for(var j=0;j<length;j++){
      //       $('#location')
      //       console.log(j);
      //     }
      //   }
      // })
      
    })

    // $(document).on('submit','#multiple-form',function(e){
    //   var extra_products_length = $('.extra_locations .row').length;
    //   var formData = $(this).serializeObject();

    //   console.log(formData);
    //   console.log(extra_products_length);
    // })

    function get_facility_address()
    {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        type: "get",
        url: '{{ url('shipper/shipment/addresses') }}',
        dataType: "html",
        success: function(data){
          var response = JSON.parse(data);
          var option = '<option value="">Select Address</option>';
          var address_option = '';
          var phone_option = '';
          var email_option = '';
          $.each(response, function(key,value){
            option += '<option value="'+value.id+'">'+value.name+'</option>';
            address_option += '<option value="'+value.id+'">'+value.autocomplete+'</option>';
            phone_option += '<option value="'+value.id+'">'+value.phone+'</option>';
            email_option += '<option value="'+value.id+'">'+value.email+'</option>';
          });
          $('select[name=pickupAddress]').html(option).trigger('change');
          $('select[name=deliveryAddress]').html(option).trigger('change');
          $('.multiple-shipment-locations').html(option).trigger('change');
        },
        error: function() { alert("Error posting feed."); }
      });
    }
</script>
<script type="text/javascript">
  
        $('#basic-pickup-timepicker3333').flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "h:i K",
            disableMobile: "true",

        });
        $('#basic-pickup-timepicker1111').flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "h:i K",
                disableMobile: "true",

        });
        $('#basic-pickup-timepicker2222').flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "h:i K",
                disableMobile: "true",

        });
// delivery details timepicker

      $('#basic-pickup-timepicker6666').flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "h:i K",
            disableMobile: "true",

      });
      $('#basic-pickup-timepicker4444').flatpickr({
              enableTime: true,
              noCalendar: true,
              dateFormat: "h:i K",
              disableMobile: "true",

      });
      $('#basic-pickup-timepicker5555').flatpickr({
              enableTime: true,
              noCalendar: true,
              dateFormat: "h:i K",
              disableMobile: "true",

      });
</script>
<script>

  function change_name_id()
  {
    var row_key = 0;
    var count = 0;
    $('#MultipleShip .extra_locations .locations').each(function(key, value){
      <?php if(isset($booking_locations_data) && !empty($booking_locations_data) && $booking_data['booking_type'] == 'multiple_shipment'){?>
        count = <?=count($booking_locations_data);?>;
      <?php }?>
        if(count <= 0){
          count = 1;
        }
        row_key = parseInt(key) + parseInt(count)+1; 
        $(value).find('#locationLabel1').text(' '+row_key+' Details');
        console.log(value);
        $(value).find('.row .locations').attr('id','location'+row_key);
        $(value).find('.location-input').attr('name','location_'+row_key);
        $(value).find('.multi-location-date').attr('name','date_'+row_key);
        $(value).find('.multi-location-time-from').attr('name','location_hour_from_'+row_key);
        $(value).find('.multi-location-time-to').attr('name','location_hour_to_'+row_key);
        $(value).find('.multi-location-time').attr('name','location_hour_'+row_key);     
        $(value).find('#location_time_range_check_').attr('name','location_time_range_check_'+row_key);
        $(value).find('.multi_location_note').attr('name','locationNote_'+row_key);
        $(value).find('#Multiplelocation1Note').attr('id','Multiplelocation1Note'+row_key);
        $(value).find('#add_text').attr('id','add_text'+row_key);
        $(value).find('.text_pickUpNote').attr('class','text_pickUpNote'+row_key);
        $(value).find('#add_note_multi').attr('id','add_note_multi'+row_key);
        $(value).find('#Multi_LocationClearNote').attr('id','Multi_LocationClearNote'+row_key);
        $(value).find('#add_note_multi').attr('id','add_note_multi'+row_key);
        $(value).find('#multi_minus_location').attr('id','multi_minus_location'+row_key);
        $(value).find('#multi_plus_location').attr('id','multi_plus_location'+row_key);
        $(value).find('#clearlocation1Note').attr('id','clearlocation1Note'+row_key);
        $(value).find('.text_pickUpNote').attr('class','text_pickUpNote'+row_key);
        var length = $(value).find('.product_checkbox_div .location_products_info').length;
        if(length != '' || length != 0){
          for(var j=0;j<length;j++){
            $(value).find('.product_info_'+j+' .product_checkbox_div .location_products_info').attr('name','location_products_'+row_key+'['+j+']');
            $(value).find('.product_info_'+j+' .delivery_type_div .multi-location-type1').attr('name','shipment_type_'+row_key+'['+j+']');
            $(value).find('.product_info_'+j+' .delivery_type_div .multi-location-type2').attr('name','shipment_type_'+row_key+'['+j+']');
            $(value).find('.product_info_'+j+' .quantity_div .multiple_quantity').attr('name','quantity_'+row_key+'['+j+']');
            $(value).find('.product_info_'+j+' .quantity_div .multiple_quantity').addClass('quantity_'+j);
            $(value).find('.product_info_'+j+' .quantity_unit_div .multi_quantity_unit').attr('name','quantity_unit_'+row_key+'['+j+']');
            $(value).find('.product_info_'+j+' .weight_div .multiple_weight').attr('name','weight_'+row_key+'['+j+']');
            $(value).find('.product_info_'+j+' .unit_div .multiple_unit').attr('name','unit_'+row_key+'['+j+']');
          }
        }
    }); 
    $('input[name="date_'+row_key+'"]').flatpickr({minDate: "today",disableMobile: "true"});
    $('input[name="location_hour_from_'+row_key+'"]').flatpickr({enableTime: true,noCalendar: true,dateFormat: "h:i K",disableMobile: "true"});
    $('input[name="location_hour_to_'+row_key+'"]').flatpickr({enableTime: true,noCalendar: true,dateFormat: "h:i K",disableMobile: "true"});
    $('input[name="location_hour_'+row_key+'"]').flatpickr({enableTime: true,noCalendar: true,dateFormat: "h:i K",disableMobile: "true"});
    
    // calculation(row_key);
     $('textarea[id="Multiplelocation1Note'+row_key+'"]');
     $('textarea[class="text_pickUpNote'+row_key+'"]');
    // calculation(row_key);
    
    $("#add_note_multi"+row_key).click(function(){ $("#Multiplelocation1Note"+row_key).toggle() })
    $("#add_text"+row_key).click(function(){ $(".text_pickUpNote"+row_key).toggle() })
    $('#Multi_LocationClearNote'+row_key).click(function(){
      document.getElementById('Multiplelocation1Note'+row_key).value = "";
      document.getElementById('Multiplelocation1Note'+row_key).readOnly =false;
      })


    $('#add_note_multi'+row_key).click(function(){
      document.getElementById('multi_minus_location'+row_key).style.visibility=    document.getElementById('multi_minus_location'+row_key).style.visibility == 'visible'? 'hidden' : 'visible';

      document.getElementById('multi_plus_location'+row_key).style.visibility=    document.getElementById('multi_plus_location'+row_key).style.visibility == 'hidden'? 'visible' : 'hidden';
    })
  }

  function calculation(key)
  {
    // var row_key = 0
    // $('#MultipleShip .extra_locations .locations').each(function(key, value){
    //   row_key = parseInt(key) + 1;
    //   var length = $(value).find('.product_checkbox_div .location_products_info').length;
    //   for(var j=0;j<length;j++){
    //     var input_quantity = $("input[name='quantity_"+row_key+"["+j+"]']").val();
    //     var input_weight = $("input[name='weight_"+row_key+"["+j+"]']").val();
    //     console.log(input_quantity);
    //     console.log(input_weight);
    //   }

    // })
    var new_key = key;
    var old_key = parseInt(key-1);
    // console.log(old_key);
    var total_products = $('#location'+old_key).find('.product_checkbox_div .location_products_info:checked').length;
    // console.log(total_products)
    if(total_products != '' || total_products != 0){
      for(var j=0;j<total_products;j++){
        console.log(j)
        var actual_quantity = $("input[name='old_quantity_1["+j+"]']").val();
        var actual_weight = $("input[name='old_weight_1["+j+"]']").val();
        var previous_weight = $("input[name='weight_"+old_key+"["+j+"]']").val();
        var previous_quantity = $("input[name='quantity_"+old_key+"["+j+"]']").val();

        // $("input[name='quantity_"+new_key+"["+j+"]']").attr('max',parseFloat(actual_quantity) - parseFloat(previous_quantity));
        $("input[name='quantity_"+new_key+"["+j+"]']").val(parseFloat(actual_quantity) - parseFloat(previous_quantity));
        // $("input[name='weight_"+new_key+"["+j+"]']").attr('max',parseFloat(actual_weight) - parseFloat(previous_weight));
        $("input[name='weight_"+new_key+"["+j+"]']").val(parseFloat(actual_weight) - parseFloat(previous_weight));
        $("input[name='shipment_type_"+new_key+"["+j+"]'][value=pickup]").prop('checked',true);
        // var old_shipment_type = $("input[name='shipment_type_"+old_key+"["+j+"]']").val();
        // console.log(previous_weight);
        // console.log(previous_quantity);
    //     var new_shipment_type = $("input[name='shipment_type_"+new_key+"["+j+"]']").val();
    //     var old_quantity = $("input[name='quantity_"+old_key+"["+j+"]']").val();
    //     if(new_shipment_type == 'pickup'){
    //       if(old_key == 1){
    //         var new_weight = parseInt(actual_weight) - parseInt(old_weight);
    //         var new_quantity = parseInt(actual_quantity) - parseInt(old_quantity);
    //         if(new_quantity == 0 || new_weight == 0){
    //           $("input[name='location_products_"+new_key+"["+j+"]']").attr('disabled',true)
    //         }else{
    //           $("input[name='location_products_"+new_key+"["+j+"]']").attr('disabled',false)
              
    //           $("input[name='old_quantity_1["+j+"]']").val(new_quantity);
    //           $("input[name='old_weight_1["+j+"]']").val(new_weight);
    //           $("input[name='weight_"+new_key+"["+j+"]']").val(new_weight);
    //           $("input[name='quantity_"+new_key+"["+j+"]']").val(new_quantity);
    //           if(new_weight > 0){
    //             $("input[name='shipment_type_"+new_key+"["+j+"]']").val('pickup');
    //           }
    //         }
    //       }else{
    //         var new_weight = parseInt(actual_weight) - parseInt(old_weight);
    //         var new_quantity = parseInt(actual_quantity) - parseInt(old_quantity);
    //         if(new_weight != 0 || new_quantity != 0){
    //           $("input[name='weight_"+new_key+"["+j+"]']").val(new_weight);
    //           $("input[name='quantity_"+new_key+"["+j+"]']").val(new_quantity);
    //           $("input[name='old_quantity_1["+j+"]']").val(new_quantity);
    //           $("input[name='old_weight_1["+j+"]']").val(new_weight);
    //         }
    //       }
    //     }
      }
    }
  }

  let i=0;
  <?php if(isset($booking_locations_data) && !empty($booking_locations_data) && $booking_data['booking_type'] == 'multiple_shipment'){?>
    i = <?=count($booking_locations_data);?>;
    i+=1;
  <?php }?>
    if(i <= 0){
      i = 2;
    }
    // console.log(i)
   function clone()
   {
    
          var original = document.getElementById('location_hidden');
           console.log('i',i);
          var clone = original.cloneNode(true);
          // let deleteElement=clone.getElementsByTagName('a')[4];
          //  var original = document.getElementById('location1' );addLocation
           
           clone.id = "location"+ i  ;
           $('.multiple_shipment_location_length').val(i);
           
          //  let locationLabelElement=clone.getElementsByTagName('span')[0];
          //  let addressSelectElement=clone.getElementsByTagName('select')[0];
          //  let deliveryDateInputElement=clone.getElementsByTagName('input')[19];
          //  let rangeInputElement=clone.getElementsByTagName('input')[1];
          //  let hourInputElement=clone.getElementsByTagName('input')[2];
          //  let locationNoteElement=clone.getElementsByTagName('textarea')[0];
          //  let pickUpInputElement=clone.getElementsByTagName('input')[7];
          // let addElement = document.getElementById('addLocation');

          // Final Code for Change Name of Input Start -------------------------------------------------
          // clone.getElementsByTagName('input')[8].name='date_'+i;
          // clone.getElementsByTagName('input')[9].name='location_time_range_check_'+i;
          // clone.getElementsByTagName('input')[10].name='location_hour_from_'+i;
          // clone.getElementsByTagName('input')[12].name='location_hour_to_'+i;
          // clone.getElementsByTagName('input')[14].name='location_hour_'+i;
          // addressSelectElement.name='location_'+i;
          // addressSelectElement.id = 'errorShowPickup'+i;
          // locationNoteElement.name='locationNote_'+i;
          // locationNoteElement.id='location'+i+'Note';

          // var unit_div_ele = clone.getElementsByClassName("unit_div");
          // for ( var j=0; j<unit_div_ele.length; j++ ) {
          //   unit_div_ele[j].getElementsByTagName('select')[0].name = 'unit_'+i+'['+j+']';
          // }

          // var weight_div_ele = clone.getElementsByClassName("weight_div");
          // for ( var j=0; j<weight_div_ele.length; j++ ) {
          //   weight_div_ele[j].getElementsByTagName('input')[0].name = 'weight_'+i+'['+j+']';
          // }

          // var quantity_unit_div_ele = clone.getElementsByClassName("quantity_unit_div");
          // for ( var j=0; j<quantity_unit_div_ele.length; j++ ) {
          //   quantity_unit_div_ele[j].getElementsByTagName('input')[0].name = 'quantity_unit_'+i+'['+j+']';
          // }

          // var quantity_div_ele = clone.getElementsByClassName("quantity_div");
          // for ( var j=0; j<quantity_div_ele.length; j++ ) {
          //   quantity_div_ele[j].getElementsByTagName('input')[0].name = 'quantity_'+i+'['+j+']';
          // }

          // var delivery_type_div_ele = clone.getElementsByClassName("delivery_type_div");
          // for ( var j=0; j<delivery_type_div_ele.length; j++ ) {
          //   delivery_type_div_ele[j].getElementsByTagName('input')[0].name = 'shipment_type_'+i+'['+j+']';
          //   delivery_type_div_ele[j].getElementsByTagName('input')[1].name = 'shipment_type_'+i+'['+j+']';
          // }
          
          // var product_checkbox_div_ele = clone.getElementsByClassName("product_checkbox_div");
          // for ( var j=0; j<product_checkbox_div_ele.length; j++ ) {
          //   product_checkbox_div_ele[j].getElementsByTagName('input')[0].name = 'location_products_'+i+'['+j+']';
          // }
          // Final Code for Change Name of Input End -------------------------------------------------

            
           
            // locationLabelElement.textContent=' '+i+' Details';

          //  rangeInputElement.id='location'+i+'_time_range_check';
           
          //  pickUpInputElement.name='delivery_type'+i;
          //  deleteElement.id = "locationDelete"+ i;
          //  deleteElement.style.display='block';
          //  deleteElement.style.float='right';
           document.getElementById('extra_locations').appendChild(clone);
           
           

           let newI=i;
           i+=1;
           let productCount = <?php echo count($products); ?>;
           if(i>5){
              console.log('close');
              $('.add_stops').attr('style', 'display:none !important');
              //  addElement.setAttribute('style', 'display:none !important');
           }
          //  document.getElementById('locationDelete'+newI).addEventListener('click',function(){
              
          //     document.getElementById("location"+ newI).remove();
          //    i-=1;
          //    $('.multiple_shipment_location_length').val(i-1);
          //    if(newI<=5){
          //        addElement.setAttribute('style', 'display:inline !important;border-radius: 16px; width: 20%;padding-left: 6%;padding-right : 6%;');
          //    }
          //  });
          

          //  document.getElementById('location'+newI+'_time_range_check').addEventListener('click',function(){
          //      var time_range = document.getElementById('location'+newI+'_time_range');
          //      var single_time = document.getElementById('location'+newI+'_single_time');
          //      if(this.checked == true){
          //         time_range.style.display = "flex";
          //         single_time.style.display = "none";
          //      }else{
          //         time_range.style.display = "none";
          //         single_time.style.display = "flex";
          //      }
          //  });
           change_name_id()
   }
</script>
<script>$(".pickup-show_button").click(function(){$("#pickUpNote").toggle()})</script>
<script>$(".multi_pickup-show_button").click(function(){$("#multi_pickUpNote").toggle()})</script>
<script>$(".show_button_text").click(function(){$("#text_id").toggle()})</script>
<script>$(".show_button").click(function(){$("#multi_text_id").toggle()})</script>
<script>$(".location-show_button").click(function(){$("#location1Note").toggle()})</script>
<script>$(".multi_location-show_button").click(function(){$("#multi_location1Note").toggle()})</script>
<script> 
    function showImage(){
        document.getElementById('hide').style.visibility=    document.getElementById('hide').style.visibility == 'visible'? 'hidden' : 'visible';

        document.getElementById('hide_show').style.visibility=    document.getElementById('hide_show').style.visibility == 'hidden'? 'visible' : 'hidden';
    }
    function delivery_note(){
      document.getElementById('minus_delivery').style.visibility=    document.getElementById('minus_delivery').style.visibility == 'visible'? 'hidden' : 'visible';

        document.getElementById('plus_delivery').style.visibility=    document.getElementById('plus_delivery').style.visibility == 'hidden'? 'visible' : 'hidden';
    }

    function text_delivery_note(){
      document.getElementById('text_minus_delivery').style.visibility=    document.getElementById('text_minus_delivery').style.visibility == 'visible'? 'hidden' : 'visible';

        document.getElementById('text_plus_delivery').style.visibility=    document.getElementById('text_plus_delivery').style.visibility == 'hidden'? 'visible' : 'hidden';
    }

    function multi_delivery_note(){
      document.getElementById('multi_minus_delivery').style.visibility=    document.getElementById('multi_minus_delivery').style.visibility == 'visible'? 'hidden' : 'visible';

        document.getElementById('multi_plus_delivery').style.visibility=    document.getElementById('multi_plus_delivery').style.visibility == 'hidden'? 'visible' : 'hidden';
    }

    function delivery_multi_note(){
      document.getElementById('delivery_minus').style.visibility=    document.getElementById('delivery_minus').style.visibility == 'visible'? 'hidden' : 'visible';

        document.getElementById('delivery_plus').style.visibility=    document.getElementById('delivery_plus').style.visibility == 'hidden'? 'visible' : 'hidden';
    }

    function location_note(){
      document.getElementById('minus_location').style.visibility=    document.getElementById('minus_location').style.visibility == 'visible'? 'hidden' : 'visible';

        document.getElementById('plus_location').style.visibility=    document.getElementById('plus_location').style.visibility == 'hidden'? 'visible' : 'hidden';
    }
   
   </script>
    <script type="text/javascript">
    //Pickup functions
    function showPickUpRange() {
        var checkBox = document.getElementById("pickup_time_range_check");

        var time_range = document.getElementById("pickup_time_range");
        var single_time = document.getElementById("pickup_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
            $('input[name=pickup_hour]').attr('required','');
            $('input[name=pickup_hour_from]').attr('required','required');
            $('input[name=pickup_hour_to]').attr('required','required');
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
            $('input[name=pickup_hour]').attr('required','required');
            $('input[name=pickup_hour_from]').attr('required','');
            $('input[name=pickup_hour_to]').attr('required','');
        }
    }
    

    function changePickUpNote() {
        document.getElementById('pickUpNote').readOnly =false;
    }

    function clearPickUpNote() {
        document.getElementById('pickUpNote').value = "";
        document.getElementById('pickUpNote').readOnly =false;
    }

    //Delivery functions
    function showDeliveryRange() {
        var checkBox = document.getElementById("delivery_time_range_check");

        var time_range = document.getElementById("delivery_time_range");
        var single_time = document.getElementById("delivery_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
            $('input[name=delivery_hour]').attr('required','');
            $('input[name=delivery_hour_from]').attr('required','required');
            $('input[name=delivery_hour_to]').attr('required','required');
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
            $('input[name=delivery_hour]').attr('required','required');
            $('input[name=delivery_hour_from]').attr('required','');
            $('input[name=delivery_hour_to]').attr('required','');
        }
    }

    function changeDeliveryNote() {
        document.getElementById('deliveryNote').readOnly =false;
    }

    function clearDeliveryNote() {
        document.getElementById('deliveryNote').value = "";
        document.getElementById('deliveryNote').readOnly =false;
    }

    // Multiple Location page functions

    function showLocationRange() {
        var checkBox = document.getElementById("location1_time_range_check");

        var time_range = document.getElementById("location1_time_range");
        var single_time = document.getElementById("location1_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
        }
    }

    function changelocation1Note() {
        document.getElementById('location1Note').readOnly =false;
    }

    function clearlocation1Note() {
        document.getElementById('location1Note').value = "";
        document.getElementById('location1Note').readOnly =false;
    }
    </script>

    <script>
    function Stops(evt, stops) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(stops).style.display = "block";
      evt.currentTarget.className += " active";
    }
</script>

<script>

    $('#basic-pickup-timepicker7').flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "h:i K",
        disableMobile: "true",
    });

    $('#basic-pickup-timepicker8').flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "h:i K",
        disableMobile: "true",
    });

    $('#basic-pickup-timepicker9').flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "h:i K",
        disableMobile: "true",
    });

  $('#basic-delivery-datepicker2').flatpickr({minDate: "today",disableMobile: "true",});
   $('#basic-pickup-timepicker8').on("input", function() {
      var pickup_date = $('#basic-delivery-datepicker2').val();
      var stime = $('#basic-pickup-timepicker8').val();   
      var etime = $('#basic-pickup-timepicker9').val(); 
      // console.log(stime,etime);
      if(pickup_date != ''){
        if(stime >= etime){
        document.getElementById('errorShowSameTime').style.display = 'block';
         $('#continuebtn').prop('disabled', true);
        }else{
          document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
        }
      }else{
        document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
      }
     
  });

  // $(document).on('click','.multi-location-date',function () {
  //   $(this).flatpickr({minDate: "today"});
  // })

  $('#basic-delivery-datepicker2').on("input", function() {
    
  });
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
<!-- {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCjorOISx346EPRCKse9h8K_fZmNnWZ00&libraries=places&callback=initAutocomplete" async defer></script> --}} -->
<script type="text/javascript">
      var placeSearch;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            console.log(pickup_place);
            var address = pickup_place.address_components;
            var street = city = state = pincode = '';
            $.each(address, function(i,val){
              console.log(val);
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
                if($.inArray('postal_code', val['types']) > -1) {
                    pincode += val['long_name'];
                }
            });
            $('#city').val(city);
            $('#latitude').val(pickup_place.geometry.location.lat());
            $('#longitude').val(pickup_place.geometry.location.lng());
            $('#post_code').val(pincode);
            changeMapLocation(pickup_place.geometry.location.lat(),pickup_place.geometry.location.lng())
        });
    });

      function changeMapLocation(latitude=0,longitude=0){
        (function initMap() {
          var position = {lat: latitude, lng: longitude};
          var myLatlng = new google.maps.LatLng(latitude,longitude);
          var geocoder = new google.maps.Geocoder();
          var map = new google.maps.Map(document.getElementById("map"), {
              zoom: 19,
              center: position,
              width: 20,
            height: 50,
          });
          var marker = new google.maps.Marker({
              position: position,
              map: map,
              draggable: true
          });

          google.maps.event.addListener(marker, 'dragend', function() {
              map.panTo(marker.getPosition()); 
              geocoder.geocode({'latLng': marker.getPosition() }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                    var address_components = results[0].address_components;
                    var components={};
                    jQuery.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.long_name});});
                    var postal_code;
                    if(components.postal_code) {
                      postal_code = components.postal_code;
                    }
                  $('#autocomplete').val(results[0].formatted_address);
                  $('#latitude').val(marker.getPosition().lat());
                  $('#longitude').val(marker.getPosition().lng());
                  $('#post_code').val(postal_code);
                  }
                }
              });
          });
           


          // google.maps.event.addListener(marker, 'click', function() {
          //   if (marker.formatted_address) {
          //     console.log(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
          //     infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
          //   } else {
          //     infowindow.setContent(address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
          //   }
          //   infowindow.open(map, marker);
          // });
        })();

        
      }
  //     $(document).ready(function() {  //The page has loaded.
  //  // $("#map").hide();   
  //   $("#from").click(function() {   
  //   $("#map").show();   
  //   });
  // });
     
</script>
@endsection
