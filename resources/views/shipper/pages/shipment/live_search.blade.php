@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Address Book</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>
  @include('admin.include.flash-message')

  <div class="row d-flex align-items-center products">
    <div class="col-md-6">
      <h3>Address Book</h3>
    </div>
    <div class="col-md-6">
      <a href="javascript:;" class="btn btn-primary float-right" data-toggle="modal" data-target="#add-address">
        <img src="images/assets/dashbord/plus.svg" alt="chat" class="img-fluid">
        <span class="text ml-1">Add Address</span>
      </a>
    </div>
  </div>
  <div class="row">
    
    <div class="col-xl-12">
      <div class="card ">
        <div class="card-body" >
        <h2>Laravel AJAX Autocomplete Search with Select2</h2>

        <select class="livesearch form-control" name="livesearch"></select>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')

<script type="text/javascript">
$(document).ready(function(){

  $('.livesearch').select2({
        placeholder: 'Select movie',
        ajax: {
            url: '/ajax-autocomplete-search',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
})
     
</script>
@endsection