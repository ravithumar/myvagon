@if(!empty(Session::get('edit_truck_data')))
<?php 
$session_data = Session::get('edit_truck_data');
$session_data_product = array();
if(Session::get('edit_product_data')){
$session_data_product = Session::get('edit_product_data');
}
//  echo '<pre>';print_r($session_data_product);die();
 ?>
@endif
@extends('shipper.layouts.master')
@section('content')
<style>
  .select2-container--default .select2-selection--single{
    background-color: #f3eff7;
    border: 0px solid #aaa;
    border-radius: 0px;
    border-bottom: 1px solid #202042;
    border-radius: inherit;
    height: calc(1.5em + .9rem + 2px);
  }
</style>
   {{-- <div class="content-page create-shipment-page">
   	  <div class="content"> --}}
   	  	 <div class="container-fluid">
   	  	 	<div class="row align-items-center">
   	  	 		<div class="col">
   	  	 			  <nav aria-label="breadcrumb">
                  @if(isset($session_data['driver_detail']) && !empty($session_data['driver_detail']))
                    <ol class="breadcrumb m-0">
                      <li class="breadcrumb-item"><a href="javascript:void(0);" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{__('Search Available Trucks')}}</a></li>
                      <li class="breadcrumb-item active txt-blue" aria-current="page">{{__('Bid')}}</li>
                      <li class="breadcrumb-item active txt-blue" aria-current="page">{{__('Product Information')}}</li>
                    </ol>
                    @else
                      <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{__('Edit Shipment')}}</a></li>
                        <li class="breadcrumb-item active txt-blue" aria-current="page"><a href="{{ url('shipper/shipment/edit',$id) }}" class="txt-blue">{{__('Truck Type')}}</a></li>
                        <li class="breadcrumb-item active txt-blue" aria-current="page">{{__('Product Information')}}</li>
                      </ol>
                  @endif
                    {{-- <ol class="breadcrumb m-0">
                       <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{__('Edit Shipment')}}</a></li>
                       <li class="breadcrumb-item active txt-blue" aria-current="page"><a href="{{ url('shipper/shipment/edit',$id) }}" class="txt-blue">Truck Type</a></li>
                       <li class="breadcrumb-item active txt-blue" aria-current="page">{{__('Product Information')}}</li>
                    </ol> --}}
                </nav>
   	  	 		</div>
   	  	 	</div>
          @if(isset($session_data) && !empty($session_data))
            @if(isset($session_data['driver_detail']) && !empty($session_data['driver_detail']))
              <div class="row mt-2 mb-2">
                <div class="col-md-12">
                  <div class="bg-white p-2">
                    <div class="row align-items-center">
                      <div class="col-md-1 tr-br">
                        <p class="mb-0 font-14 txt-blue font-weight-light"><em class="font-weight-bold">{{__('Available Truck')}}</em><br> {{__('for booking')}}  </p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('Truck Type')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['driver_vehicle_detail']['truck_type']['name']) && !empty($session_data['driver_vehicle_detail']['truck_type']['name'])){{ $session_data['driver_vehicle_detail']['truck_type']['name'] }}@endif</p>
                        <p class="mb-0 font-13 txt-blue font-weight-light txt-purple">@if(isset($session_data['driver_vehicle_detail']['truck_sub_category']['name']) && !empty($session_data['driver_vehicle_detail']['truck_sub_category']['name'])){{ $session_data['driver_vehicle_detail']['truck_sub_category']['name'] }}@endif</p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('Start Date')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['date']) && !empty($session_data['availability_data']['date'])){{ date('F d, Y',strtotime($session_data['availability_data']['date'])) }}@endif <br> @if(isset($session_data['availability_data']['time']) && !empty($session_data['availability_data']['time'])) {{$session_data['availability_data']['time']}} @endif</p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('Start Location')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['from_address']) && !empty($session_data['availability_data']['from_address'])) {{ (strlen($session_data['availability_data']['from_address']) > 35 ? substr($session_data['availability_data']['from_address'],0,35).'...' : $session_data['availability_data']['from_address']) }} @endif</p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('End Location')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['to_address']) && !empty($session_data['availability_data']['to_address'])) {{ (strlen($session_data['availability_data']['to_address']) > 35 ? substr($session_data['availability_data']['to_address'],0,35).'...' : $session_data['availability_data']['to_address']) }} @endif</p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('Carrier Info')}}</p><br>
                        {{-- <p class="mb-0 font-14 txt-blue font-weight-light"><a href="javascript:void(0)">Test Driver</a></p> --}}
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['driver_detail']['name']) && !empty($session_data['driver_detail']['name'])) {{ $session_data['driver_detail']['name'] }} @endif</p>
                      </div>
                      <div class="col-md-1">
                        <p class="txt-purple mb-0">{{__('Bid Price')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">€ @if(isset($session_data['availability_data']['bid_amount']) && !empty($session_data['availability_data']['bid_amount'])) {{ $session_data['availability_data']['bid_amount'] }} @else 0 @endif</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endif
          @endif
          <div class="row d-flex align-items-center products">
             <div class="col-md-8 col-lg-6">
                <h3>{{__('Product Information')}}</h3>
                {{-- <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p> --}}
             </div>
             <div class="col-md-4 col-lg-5"></div>
          </div>
          <form action="{{ route('shipper.shipment.edit.step3',$id) }}" method="POST" name="product_form">
            @csrf 
            <div class="row">
              <div class="col-md-12">
                {{-- <a href="javascript:;" class="add-more-one mt-2 d-flex add_field_button"  id="addProduct" data-target="#add-new-product" data-toggle="modal">
                  <img src="{{ url('assets/images/shipment/plus-black.svg') }}" alt="Plus" class="img-fluid">
                  <span class="text ml-1 text-decoration-none">{{__('Add New Products')}}</span>
                </a> --}}
                <a href="javascript:void(0);" class="txt-blue" data-toggle="modal" data-target="#add-new-product">
                  <img src="http://13.36.112.48/assets/images/shipment/plus-black.svg" alt="chat" class="img-fluid">
                  <span class="ml-1 mr-3" style="color: #9B51E0;">{{__('Add New Products')}}</span>
                </a>
              </div>
            </div>
          <div class="row">
             <div class="col">
                <div class="pro-info input_fields_wrap">
                    <div class="pro-content mt-3 mb-4 product_info">
                      <div class="extra_products" id="extra_products">
                        @if(isset($session_data_product) && !empty($session_data_product))
                        @foreach ($session_data_product as $key => $value)
                          <div class="row pl-4 products pb-3" style="background-color:#f3eff7; margin-top: 2%;">
                            <div class="col">
                              <div class="row mt-3 mt-md-4 mt-lg-5 main_product_div">
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                   <div class="form-group mb-4">
                                    
                                     <select class="form-control form-control-without-border product_name pb-1 hidden_products" name="product[]" id="product_id_{{ $key+1 }}" style="background: none;" required>
                                        <option disabled selected>{{__('Product Name')}}</option>
                                        @foreach ($productData as $product)
                                            <option value="{{ $product['id'] }}" {{ $value['product_id'] == $product['id'] ? 'selected' : '' }}>{{ $product['name'] }}</option>
                                        @endforeach
                                     </select>
                                   </div>
                                </div>
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                  <div class="form-group mb-4">
                                    <select class="form-control form-control-without-border product_sku pb-1" style="background: none;" id="sku_no_{{ $key+1 }}" name="sku[]" required>
                                        <option  disabled selected>{{__('SKU #')}}</option>
                                        @foreach ($productData as $product)
                                            <option value="{{ $product['id'] }}" {{ $value['product_id'] == $product['id'] ? 'selected' : '' }}>{{ $product['sku_no'] }}</option>
                                        @endforeach
                                     </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                  <div class="form-group mb-4">
                                    <select class="form-control form-control-without-border product_sku_type pb-1" style="background: none;" id="sku_type_{{ $key+1 }}" name="sku_type[]" disabled required>
                                        <option  disabled selected>{{__('SKU Type')}}</option>
                                        @foreach ($productData as $product)
                                            <option value="{{ $product['id'] }}" {{ $value['product_id'] == $product['id'] ? 'selected' : '' }}>{{ $product['sku_type'] }}</option>
                                        @endforeach
                                     </select>
                                   </div>
                                </div>
                                <div class="d-sm-none col-lg-3 d-lg-block"></div>
                            </div>
                            <div class="row mt-3 mt-md-4 mt-lg-5">
                                  <div class="col-sm-4 col-md-6 col-lg-3">
                                     <div class="form-group mb-4">
                                       {{-- <input type="text" id="package_type" name="package_type" class="form-control font-18 form-control-without-border pb-2"  style="background: none;" placeholder="Package Type"> --}}
                                       <select class="form-control form-control-without-border package_type pb-1" style="background: none;" name="package[]" id="package_{{ $key+1 }}" required>
                                        <option  disabled selected>{{__('Package Type')}}</option>
                                        @foreach ($packageData as $package)
                                            <option value="{{ $package['id'] }}" {{$package['id'] == $value['package'] ? 'selected' : ''}}>{{ $package['name'] }}</option>
                                        @endforeach
                                       </select>
                                     </div>
                                  </div>
                                 <div class="col-sm-4 col-md-6 col-lg-3">
                                    <div class="form-group mb-4">
                      
                                      <input type="text" id="package_quantity_{{ $key+1 }}" name="package_quantity[]" required class="form-control form-control-without-border pb-2 package_quantity" style="background: none;" data-parsley-pattern="[0-9]+[.,]?[0-9]*" placeholder="Package Quantity" value="{{ $value['package_quantity'] }}" maxlength="8">
                                    </div>
                                 </div>
                                 <div class="">
                                    <div class="form-group mb-4">
                                        <input type="text" id="weight_{{ $key+1 }}" name="weight[]" class="form-control form-control-without-border pb-2 mr-1 weight" style="background: none; width: 97%;" placeholder="Weight" data-parsley-pattern="[0-9]+[.,]?[0-9]*" value="{{ $value['weight'] }}" required maxlength="8">
                                    </div>
                                 </div>
                                 <div class="col-sm-1 col-md-1 col-lg-1">
                                    <div class="form-group mb-4">
                                        <div class="qty ml-0" style="width: 223px !important;">
                                            <select class="form-control form-control-without-border unit pb-1" style="background: none;" name="unit[]" id="unit_{{ $key+1 }}" required>
                                                <option  disabled>Unit</option>
                                                @foreach ($unitData as $unit)
                                                    <option value="{{ $unit['id'] }}" {{$unit['id'] == $value['unit'] ? 'selected' : ''}}>{{ $unit['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                  <div class="d-sm-none col-lg-3 d-lg-block"></div>
                              </div>
                                <div class="specs-check d-flex mt-3">
                                    <div class="form-group">
                                        <label><input type="checkbox" id="Fragile{{ $key+1 }}" value="1" class="largerCheckbox fragile" name="fragile_{{ $key }}" {{ $value['fragile'] == '1' ? 'checked' : '' }}><span></span></label>
                                        <label for="" class="txt-blue fragil_label">{{__('Fragile')}}</label>
                                     </div>
                                      <div class="form-group ml-3 ml-md-4 ml-lg-5">
                                        <label><input type="checkbox" id="Sensitive{{ $key+1 }}" value="1" class="largerCheckbox sensitive" name="sensitive_{{ $key }}" {{ $value['sensitive'] == '1' ? 'checked' : '' }}><span></span></label>
                                        <label for="" class="txt-blue sensitive_label">{{__('Sensitive to odour')}}</label>
                                     </div>
                                </div>
                              <div class="row align-items-center">
                                <div class="col-md-7 col-lg-7 col-xl-5">
                                  <div class="form-group mb-0">
                                      <textarea class="form-control product_description" id="exampleFormControlTextarea1" rows="3" placeholder="Product Description" name="product_description[]">{{ $value['product_description'] }}</textarea>
                                  </div>
                                </div>
                                <div class="col-md-5 col-lg-5 col-xl-7">
                                  <div class="delete-this" style="float: right;">
                                     <a style="@if($key == 0) display:none !important; @endif font-weight:600;float: left !important;" id="productDelete1" href="javascript:;" class="btn btn-light delete-one font-12 d-flex" onclick="remove_product(this);" data-id="{{ $value['product_id'] }}" data-booking-id="{{ $id }}">
                                       <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                       <span class="text ml-1">Delete</span>
                                     </a>
                                  </div>
                               </div>
                              </div>
                            </div>
                      
                          </div>
                          @endforeach
                        @else

                        @if(isset($inserted_products) && !empty($inserted_products))
                          @foreach ($inserted_products as $key => $value)
                          <div class="row pl-4 products pb-3" style="background-color:#f3eff7; margin-top: 2%;">
                            <div class="col">
                              <div class="row mt-3 mt-md-4 mt-lg-5 main_product_div">
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                   <div class="form-group mb-4">
                                    
                                     <select class="form-control form-control-without-border product_name pb-1 hidden_products" name="product[]" id="product_id_{{ $key+1 }}" style="background: none;" required>
                                        <option disabled selected>{{__('Product Name')}}</option>
                                        @foreach ($productData as $product)
                                            <option value="{{ $product['id'] }}" {{ $value['product_id']['id'] == $product['id'] ? 'selected' : '' }}>{{ $product['name'] }}</option>
                                        @endforeach
                                     </select>
                                   </div>
                                </div>
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                  <div class="form-group mb-4">
                                    <select class="form-control form-control-without-border product_sku pb-1" style="background: none;" id="sku_no_{{ $key+1 }}" name="sku[]" required>
                                        <option  disabled selected>{{__('SKU #')}}</option>
                                        @foreach ($productData as $product)
                                            <option value="{{ $product['id'] }}" {{ $value['product_id']['id'] == $product['id'] ? 'selected' : '' }}>{{ $product['sku_no'] }}</option>
                                        @endforeach
                                     </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                  <div class="form-group mb-4">
                                    <select class="form-control form-control-without-border product_sku_type pb-1" style="background: none;" id="sku_type_{{ $key+1 }}" name="sku_type[]" disabled required>
                                        <option  disabled selected>{{__('SKU Type')}}</option>
                                        @foreach ($productData as $product)
                                            <option value="{{ $product['id'] }}" {{ $value['product_id']['id'] == $product['id'] ? 'selected' : '' }}>{{ $product['sku_type'] }}</option>
                                        @endforeach
                                     </select>
                                   </div>
                                </div>
                                <div class="d-sm-none col-lg-3 d-lg-block"></div>
                            </div>
                            <div class="row mt-3 mt-md-4 mt-lg-5">
                                  <div class="col-sm-4 col-md-6 col-lg-3">
                                     <div class="form-group mb-4">
                                       {{-- <input type="text" id="package_type" name="package_type" class="form-control font-18 form-control-without-border pb-2"  style="background: none;" placeholder="Package Type"> --}}
                                       <select class="form-control form-control-without-border package_type pb-1" style="background: none;" name="package[]" id="package_{{ $key+1 }}" required>
                                        <option  disabled selected>{{__('Package Type')}}</option>
                                        @foreach ($packageData as $package)
                                            <option value="{{ $package['id'] }}" {{$package['id'] == $value['product_type']['id'] ? 'selected' : ''}}>{{ $package['name'] }}</option>
                                        @endforeach
                                       </select>
                                     </div>
                                  </div>
                                 <div class="col-sm-4 col-md-6 col-lg-3">
                                    <div class="form-group mb-4">
                      
                                      <input type="text" id="package_quantity_{{ $key+1 }}" name="package_quantity[]" required class="form-control form-control-without-border pb-2 package_quantity" style="background: none;" data-parsley-pattern="[0-9]+[.,]?[0-9]*" placeholder="Package Quantity" value="{{ $value['qty'] }}" maxlength="8">
                                    </div>
                                 </div>
                                 <div class="">
                                    <div class="form-group mb-4">
                                        <input type="text" id="weight_{{ $key+1 }}" name="weight[]" class="form-control form-control-without-border pb-2 mr-1 weight" style="background: none; width: 97%;" placeholder="Weight" data-parsley-pattern="[0-9]+[.,]?[0-9]*" value="{{ $value['weight'] }}" required maxlength="8">
                                    </div>
                                 </div>
                                 <div class="col-sm-1 col-md-1 col-lg-1">
                                    <div class="form-group mb-4">
                                        <div class="qty ml-0" style="width: 223px !important;">
                                            <select class="form-control form-control-without-border unit pb-1" style="background: none;" name="unit[]" id="unit_{{ $key+1 }}" required>
                                                <option  disabled>{{__('Unit')}}</option>
                                                @foreach ($unitData as $unit)
                                                    <option value="{{ $unit['id'] }}" {{$unit['id'] == $value['unit']['id'] ? 'selected' : ''}}>{{ $unit['name'] }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                  <div class="d-sm-none col-lg-3 d-lg-block"></div>
                              </div>
                                <div class="specs-check d-flex mt-3">
                                    <div class="form-group">
                                        <label><input type="checkbox" id="Fragile{{ $key+1 }}" value="1" class="largerCheckbox fragile" name="fragile_{{ $key }}" {{ $value['is_fragile'] == '1' ? 'checked' : '' }}><span></span></label>
                                        <label for="" class="txt-blue fragil_label">{{__('Fragile')}}</label>
                                     </div>
                                      <div class="form-group ml-3 ml-md-4 ml-lg-5">
                                        <label><input type="checkbox" id="Sensitive{{ $key+1 }}" value="1" class="largerCheckbox sensitive" name="sensitive_{{ $key }}" {{ $value['is_sensetive'] == '1' ? 'checked' : '' }}><span></span></label>
                                        <label for="" class="txt-blue sensitive_label">{{__('Sensitive to odour')}}</label>
                                     </div>
                                </div>
                              <div class="row align-items-center">
                                <div class="col-md-7 col-lg-7 col-xl-5">
                                  <div class="form-group mb-0">
                                    <!-- <a href="javascript:;" class="add-one font-18 mt-3 d-flex show_button"  onclick="showImage();">
                                  <ul class="d-flex m-1 p-1 mb-2">
                                    <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="hide" style="visibility:hidden;">
                                     <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="hide_show">
                                     
                                     <label class="txt-blue" style="text-decoration: underline;" > {{__('Add')}} &nbsp;{{__('Description')}}</label>
                                     <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="margin-top: 28px;"></a></label>
                                     {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                                     </ul>
                                  </a> -->
                                  
                                      <textarea class="form-control product_description" id="exampleFormControlTextarea1" rows="3" placeholder="Product Description" name="product_description[]">{{ $value['note'] }}</textarea>
                                  </div>
                                </div>
                                <div class="col-md-5 col-lg-5 col-xl-7">
                                  <div class="delete-this" style="float: right;">
                                     <a style="@if($key == 0) @endif font-weight:600;float: left !important;" id="productDelete1" href="javascript:void(0);" class="btn btn-light delete-one font-12 d-flex" onclick="remove_product(this);" data-id="{{ $value['product_id']['id'] }}" data-booking-id="{{ $id }}">
                                       <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                       <span class="text ml-1">{{__('Delete')}}</span>
                                     </a>
                                  </div>
                               </div>
                              </div>
                            </div>
                      
                          </div>
                          @endforeach
                          @else
                          <div class="row pl-4 products pb-3" style="background-color:#f3eff7; margin-top: 2%;" id="product1">
                            <div class="col">
                              <div class="row mt-3 mt-md-4 mt-lg-5 main_product_div">
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                   <div class="form-group mb-4">
                                     <select class="form-control form-control-without-border product_name" name="product[]" id="product_id_1" style="background: none;" required>
                                        <option disabled value="" selected>{{__('Product Name')}}</option>
                                        @foreach ($productData as $product)
                                            <option value="{{ $product['id'] }}" {{old('product_id_1') == $product['id'] ? 'selected' : ''}}>{{ $product['name'] }}</option>
                                        @endforeach
                                     </select>
                                   </div>
                                </div>
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                  <div class="form-group mb-4">
                                    <select class="form-control form-control-without-border product_sku" style="background: none;" id="sku_no_1" name="sku[]" required>
                                        <option value="" disabled selected>{{__('SKU #')}}</option>
                                        @foreach ($productData as $product)
                                            <option data-id="{{ $product['id'] }}" value="{{ $product['id'] }}" {{old('sku_no_1') == $product['id'] ? 'selected' : ''}}>{{ $product['sku_no'] }}</option>
                                        @endforeach
                                     </select>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-md-6 col-lg-3">
                                  <div class="form-group mb-4">
                                    <select class="form-control form-control-without-border product_sku_type pb-1" style="background: none;" id="sku_type_1" name="sku_type[]" disabled required>
                                        <option  disabled selected>{{__('SKU Type')}}</option>
                                        @foreach ($productData as $product)
                                            <option value="{{ $product['id'] }}" {{old('sku_type_1') == $product['id'] ? 'selected' : ''}}>{{ $product['sku_type'] }}</option>
                                        @endforeach
                                     </select>
                                   </div>
                                </div>
                                <div class="d-sm-none col-lg-3 d-lg-block"></div>
                            </div>
                            <div class="row mt-3 mt-md-4 mt-lg-5">
                                  <div class="col-sm-4 col-md-6 col-lg-3">
                                     <div class="form-group mb-4">
                                       <select class="form-control form-control-without-border package_type pb-1" style="background: none;" name="package[]" required>
                                        <option  disabled selected>{{__('Package Type')}}</option>
                                        @foreach ($packageData as $package)
                                            <option value="{{ $package['id'] }}" {{$package['id'] == '1' ? 'selected' : ''}}>{{ $package['name'] }}</option>
                                        @endforeach
                                       </select>
                                     </div>
                                  </div>
                                 <div class="col-sm-4 col-md-6 col-lg-3">
                                    <div class="form-group mb-4">
                                      <input type="text" id="package_quantity" name="package_quantity[]" data-parsley-pattern="[0-9]+[.,]?[0-9]*" required class="form-control form-control-without-border pb-2 package_quantity" style="background: none;" placeholder="{{__('Package Quantity')}}" value="{{old('package_quantity_1')}}" maxlength="8">
                                    </div>
                                 </div>
                                 <div class="">
                                    <div class="form-group mb-4">
                                        <input type="text" id="weight" name="weight[]" data-parsley-pattern="[0-9]+[.,]?[0-9]*" class="form-control form-control-without-border pb-2 mr-1 weight" style="background: none; width: 97%;" placeholder="{{__('Weight')}}" value="{{old('weight_1')}}" required maxlength="8">
                                    </div>
                                 </div>
                                 <div class="col-sm-1 col-md-1 col-lg-1">
                                    <div class="form-group mb-4">
                                        <div class="qty ml-0" style="width: 223px !important;">
                                            <select class="form-control form-control-without-border unit pb-1" style="background: none;" name="unit[]" required>
                                                <option  disabled>{{__('Unit')}}</option>
                                                @foreach ($unitData as $unit)
                                                    <option value="{{ $unit['id'] }}" {{$unit['id'] == '3' ? 'selected' : ''}}>{{ $unit['name'] }}</option>
                                                @endforeach
                                               </select>
                                        </div>
                                    </div>
                                </div>
                                  <div class="d-sm-none col-lg-3 d-lg-block"></div>
                              </div>
                                <div class="specs-check d-flex mt-3">
                                    <div class="form-group">
                                        <label><input type="checkbox" id="Fragile1"  class="largerCheckbox fragile" name="fragile_0" value="1"><span></span></label>
                                        <label for="Fragile1" class="txt-blue fragil_label">{{__('Fragile')}}</label>
                                     </div>
                                      <div class="form-group ml-3 ml-md-4 ml-lg-5">
                                        <label><input type="checkbox" id="Sensitive1"  class="largerCheckbox sensitive" name="sensitive_0" value="1"><span></span></label>
                                        <label for="Sensitive1" class="txt-blue sensitive_label">{{__('Sensitive to odour')}}</label>
                                     </div>
                                </div>
                              <div class="row">
                                <div class="col-md-7 col-lg-7 col-xl-5">
                                  <div class="form-group mb-0">
                                      <textarea class="form-control product_description" id="exampleFormControlTextarea1" rows="3" placeholder="Product Description" name="product_description[]"></textarea>
                                  </div>
                                </div>
                                <div class="col-md-5 col-lg-5 col-xl-7">
                                   <div class="delete-this" style="float: right;">
                                      <a style="display: none !important; font-weight:600;float: left !important;" id="productDelete1" href="javascript:;" class="btn btn-light delete-one font-12 d-flex remove_product">
                                        <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                        <span class="text ml-1">{{__('Delete')}}</span>
                                      </a>
                                   </div>
                                </div>
                              </div>
                            </div>
    
                          </div>
                        @endif

                        @endif
                        
                      </div>
                    </div>
                </div>
                @if(count($productData) > 1)
                {{-- <a href="javascript:;" class="add-more-one mt-3 d-flex add_field_button" onclick='clone()' id="addProduct">
                       <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="Plus" class="img-fluid">
                       <span class="text ml-1 text-decoration-none">Add More Products</span> --}}
                {{-- </a> --}}
                <a href="javascript:;" class="add-more-one font-20 mt-3 add_field_button btn btn-outline-info" onclick="clone()" id="addProduct" style="border-radius: 16px;">
                  <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="Plus" class="img-fluid">
                  <span class="text ml-1 text-decoration-none">{{__('Add More Products')}}</span>
                 </a>
                @endif
                <div class="btn-block d-flex">
                    <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" onclick="formValidation()">{{__('Continue')}}</button>
                </div>
          </div>
        </form>

   	  	 </div>
   	  {{-- </div>
   </div> --}}
	 <div class="clone" style="display:none">
		<div class="row pl-4 products pb-3" style="background-color:#f3eff7; margin-top: 2%;" id="product_hidden">
			<div class="col">
				<div class="row mt-3 mt-md-4 mt-lg-5 main_product_div">
					<div class="col-sm-4 col-md-6 col-lg-3">
						 <div class="form-group mb-4">
              
							 <select class="form-control form-control-without-border product_name pb-1 hidden_products" name="" id="" style="background: none;" required>
									<option disabled selected>{{__('Product Name')}}</option>
									@foreach ($productData as $product)
											<option value="{{ $product['id'] }}">{{ $product['name'] }}</option>
									@endforeach
							 </select>
						 </div>
					</div>
					<div class="col-sm-4 col-md-6 col-lg-3">
						<div class="form-group mb-4">
							<select class="form-control form-control-without-border product_sku pb-1" style="background: none;" id="" name="" required>
									<option  disabled selected>{{__('SKU #')}}</option>
									@foreach ($productData as $product)
											<option value="{{ $product['id'] }}">{{ $product['sku_no'] }}</option>
									@endforeach
							 </select>
							</div>
					</div>
					<div class="col-sm-4 col-md-6 col-lg-3">
						<div class="form-group mb-4">
							<select class="form-control form-control-without-border product_sku_type pb-1" style="background: none;" id="" name="" disabled required>
									<option  disabled selected>{{__('SKU Type')}}</option>
									@foreach ($productData as $product)
											<option value="{{ $product['id'] }}">{{ $product['sku_type'] }}</option>
									@endforeach
							 </select>
						 </div>
					</div>
					<div class="d-sm-none col-lg-3 d-lg-block"></div>
			</div>
			<div class="row mt-3 mt-md-4 mt-lg-5">
						<div class="col-sm-4 col-md-6 col-lg-3">
							 <div class="form-group mb-4">
								 {{-- <input type="text" id="package_type" name="package_type" class="form-control font-18 form-control-without-border pb-2"  style="background: none;" placeholder="Package Type"> --}}
								 <select class="form-control form-control-without-border package_type pb-1" style="background: none;" name="" required>
									<option  disabled selected>{{__('Package Type')}}</option>
									@foreach ($packageData as $package)
											<option value="{{ $package['id'] }}" {{$package['id'] == '1' ? 'selected' : ''}}>{{ $package['name'] }}</option>
									@endforeach
								 </select>
							 </div>
						</div>
					 <div class="col-sm-4 col-md-6 col-lg-3">
							<div class="form-group mb-4">

								<input type="text" id="" name="" required class="form-control form-control-without-border pb-2 package_quantity" style="background: none;" data-parsley-pattern="[0-9]+[.,]?[0-9]*" placeholder="Package Quantity" maxlength="8">
							</div>
					 </div>
					 <div class="">
							<div class="form-group mb-4">
									<input type="text" id="" name="" class="form-control form-control-without-border pb-2 mr-1 weight" style="background: none; width: 97%;" placeholder="Weight" data-parsley-pattern="[0-9]+[.,]?[0-9]*" required maxlength="8">
							</div>
					 </div>
					 <div class="col-sm-1 col-md-1 col-lg-1">
							<div class="form-group mb-4">
									<div class="qty ml-0" style="width: 223px !important;">
											<select class="form-control form-control-without-border unit pb-1" style="background: none;" name="" required>
													<option  disabled>{{__('Unit')}}</option>
													@foreach ($unitData as $unit)
															<option value="{{ $unit['id'] }}" {{$unit['id'] == '3' ? 'selected' : ''}}>{{ $unit['name'] }}</option>
													@endforeach
                      </select>
									</div>
							</div>
					</div>
						<div class="d-sm-none col-lg-3 d-lg-block"></div>
				</div>
					<div class="specs-check d-flex mt-3">
							<div class="form-group">
									<label><input type="checkbox" id="" value="1" class="largerCheckbox fragile" name="fragile"><span></span></label>
									<label for="" class="txt-blue fragil_label">{{__('Fragile')}}</label>
							 </div>
								<div class="form-group ml-3 ml-md-4 ml-lg-5">
									<label><input type="checkbox" id="" value="1" class="largerCheckbox sensitive" name="sensitive"><span></span></label>
									<label for="" class="txt-blue sensitive_label">{{__('Sensitive to odour')}}</label>
							 </div>
					</div>
				<div class="row align-items-center">
					<div class="col-md-7 col-lg-7 col-xl-5">
						<div class="form-group mb-0">
              <!-- <ul class="d-flex m-1 p-1 mb-2">
                    <a href="javascript:;" class="add-one font-18 d-flex multiple-location-show_button" id="add_note_multi" >
                      <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="multi_minus_location" style="visibility:hidden ">
                      <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="multi_plus_location">
                        <span class="txt-blue font-18 " style="text-decoration: underline;margin-top: 4px;" id="Multiple_Location_Note" >{{__('Add')}}&nbsp;{{__('Description')}}</span>
                        <label class="mr-2"><a href="javascript:;" class="txt-blue" id="Multi_LocationClearNote"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="padding-top: 8px;"></a></label>
                        </a>
                     </ul>
                    -->
                    
                
                <textarea class="form-control product_description" id="Multiplelocation1Note" rows="3" placeholder="Product Description" name="product_description[]">@if(!empty($session_product['product_description']) && isset($session_product['product_description'])) {{ $session_product['product_description'] }} @endif</textarea>
								
						</div>
					</div>
					<div class="col-md-5 col-lg-5 col-xl-7">
						 <div class="delete-this" style="float: right;">
								<a style="display: inline-block !important; font-weight:600;float: left !important;" id="productDelete1" href="javascript:;" class="btn btn-light delete-one font-12  d-flex remove_product">
									<img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
									<span class="text ml-1">{{__('Delete')}}</span>
								</a>
						 </div>
					</div>
				</div>
			</div>

		</div>
	 </div>

   <div class="modal fade" id="add-new-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="edits mb-2 d-flex align-items-center justify-content-between">
            <div class="title font-20 text-left">{{__('Product')}}</div>
          </div>
          <form action="javascript:void(0)" method="POST" id="add_new_product">
            @csrf
            <div class="form-group mb-4">
              <label for="name" class="mb-2 text-grey font-14">{{__('Product Name')}}</label>
              <input type="text" id="name" maxlength="40" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="Enter Product Name" value="{{ old('name') }}">
              @error('name')
              <div class="error">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group mb-4">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('SKU Type')}}</label>
              <input type="text" id="sku_type" name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ old('sku_type') }}" placeholder="Enter SKU Type">
              @error('sku_type')
              <div class="error">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group mb-4">
              <label for="sku_no" class="mb-2 text-grey font-14">{{__('SKU No')}}</label>
              <input type="text" id="sku_no" name="sku_no" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ old('sku_no') }}" placeholder="Enter SKU No">
              @error('sku_type')
              <div class="error">{{ $message }}</div>
              @enderror
            </div>
            <div class="btn-block d-flex">
              <button type="button" class="btn w-100 btn-white mr-2 add-product-cancel" data-dismiss="modal" aria-label="Close">{{__('Cancel')}}</button>
              <button type="submit" class="btn w-100 btn-primary">{{__('Save')}}</button>
            </div>
            
          </form>
          
        </div>
      </div>
    </div>
  </div>

   @endsection

   @section('script')

   {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
<script>$(".show_button").click(function(){$("#exampleFormControlTextarea1").toggle()})</script>
<script> 
    function showImage(){
        document.getElementById('hide').style.visibility=    document.getElementById('hide').style.visibility == 'visible'? 'hidden' : 'visible';

        document.getElementById('hide_show').style.visibility=    document.getElementById('hide_show').style.visibility == 'hidden'? 'visible' : 'hidden';
    }
    function clearNote() {
        document.getElementById('exampleFormControlTextarea1').value = "";
        document.getElementById('exampleFormControlTextarea1').readOnly =false;
    }
   </script>

    <script>
      function preventBack() { window.history.forward(); }  
      setTimeout("preventBack()", 0);  
      window.onunload = function () { null };

    function addProductName(obj,val, productName, product){
        $(obj).find('.product_name').val(val);
        $(obj).find('.product_dropdown').text(productName);

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax
        ({
        type: "POST",
        url: "{{url('shipper/shipment/product/data')}}",
        data: {id: val},
        cache: false,
        success: function(data)
        {
            $(obj).find('.product_sku').val(JSON.parse(data).id);
            $(obj).find('.product_sku_type').val(JSON.parse(data).id);
        }
        });
    }

       let i=2;
       keyArray = []; 
       function change_name_id()
       {
         var select2_key = 1;
         var row_key = 0;
         $('.extra_products .products').each(function(key, value){
					 row_key = parseInt(key) + 1;  
            var row_key2 = parseInt(row_key) - 1;       
            $(value).attr('id','');
					 $(value).find('.products').attr('id','product'+row_key)

           $(value).find('.product_name').attr('name','product[]');
           $(value).find('.product_name').attr('id','product_id_'+row_key);

           $(value).find('.product_dropdown').attr('id','product_dropdown_'+row_key);

					 $(value).find('.product_sku').attr('name','sku[]');
           $(value).find('.product_sku').attr('id','sku_no_'+row_key);

					 $(value).find('.product_sku_type').attr('name','sku_type[]');
           $(value).find('.product_sku_type').attr('id','sku_type_'+row_key);

					 $(value).find('.package_type').attr('name','package[]');
           $(value).find('.package_type').attr('id','package_'+row_key);
					 
					 $(value).find('.package_quantity').attr('name','package_quantity[]');
           $(value).find('.package_quantity').attr('id','package_quantity_'+row_key);
					 
					 $(value).find('.weight').attr('name','weight[]');
           $(value).find('.weight').attr('id','weight_'+row_key);

					 $(value).find('.unit').attr('name','unit[]');
           $(value).find('.unit').attr('id','unit_'+row_key);

					 $(value).find('.product_description').attr('name','product_description[]');

					 $(value).find('.fragile').attr('name','fragile_'+row_key2);
           $(value).find('.fragil_label').attr('for','Fragile'+row_key);
           $(value).find('.fragile').attr('id','Fragile'+row_key);

					 $(value).find('.sensitive').attr('name','sensitive_'+row_key2);
           $(value).find('.sensitive_label').attr('for','Sensitive'+row_key);
           $(value).find('.sensitive').attr('id','Sensitive'+row_key);
           $(value).find('#add_note_multi').attr('id','add_note_multi'+row_key);
           $(value).find('.multiple-location-show_button').attr('class','multiple-location-show_button'+row_key);
           
           $(value).find('.multi_location_note').attr('name','locationNote_'+row_key);
          $(value).find('#Multiplelocation1Note').attr('id','Multiplelocation1Note'+row_key);
        
          $(value).find('#Multi_LocationClearNote').attr('id','Multi_LocationClearNote'+row_key);
          $(value).find('#multi_minus_location').attr('id','multi_minus_location'+row_key);
          $(value).find('#multi_plus_location').attr('id','multi_plus_location'+row_key);
           select2_key = row_key;
								
         });
         $(".multiple-location-show_button"+row_key).click(function(){ $("#Multiplelocation1Note"+row_key).toggle() })
    $('#Multi_LocationClearNote'+row_key).click(function(){
      document.getElementById('Multiplelocation1Note'+row_key).value = "";
      document.getElementById('Multiplelocation1Note'+row_key).readOnly =false;
      })


    $('.multiple-location-show_button'+row_key).click(function(){
      document.getElementById('multi_minus_location'+row_key).style.visibility=    document.getElementById('multi_minus_location'+row_key).style.visibility == 'visible'? 'hidden' : 'visible';

      document.getElementById('multi_plus_location'+row_key).style.visibility=    document.getElementById('multi_plus_location'+row_key).style.visibility == 'hidden'? 'visible' : 'hidden';
    })
        //  $('select[name="product_id_'+select2_key+'"]').select2();
        //  console.log('select[name="product_id_'+select2_key+'"]');
       }

       $(document).on('click','.remove_product',function(){
          $(this).closest('.products').remove();
          var row_key = $('.products').length;
          if(row_key <= 6){
              $('#addProduct').show();
          }
      });

        function clone()
        {
          var original = document.getElementById('product_hidden');
          var clone = original.cloneNode(true);
          let deleteElement=clone.getElementsByTagName('a')[0];
          let addElement = document.getElementById('addProduct');
          deleteElement.id = "productDelete"+ i;
          deleteElement.style.display='inline-block';
          deleteElement.style.float='left';
          document.getElementById('extra_products').appendChild(clone);
          let newI=i;
          i+=1;
          if(i > 5){
              addElement.setAttribute('style', 'display:none !important');
          }
          // $('.select2').removeAttr('data-select2-id');
          // $('span.select2').remove();
          // $('.select2').select2({width:'100%'});
          // $('.select3').removeAttr('data-select2-id');
          // $('.select3').select2({width:'100%'});

          // $('.hidden_products').select2("destroy");
          // $('.hidden_products').select2({ //apply select2 to my element
          //     allowClear: true
          // });
          change_name_id();
        }

        function get_products()
        {
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
          $.ajax({
            type: "get",
            url: "{{ url('shipper/shipment/products') }}",
            dataType: "html",
            success: function(data){
              var response = JSON.parse(data);
              var product_option = '';
              var product_sku_option = '';
              var product_sku_type_option = '';
              $.each(response, function(key,value){
                product_option += '<option value="'+value.id+'">'+value.name+'</option>';
                product_sku_option += '<option value="'+value.id+'">'+value.sku_no+'</option>';
                product_sku_type_option += '<option value="'+value.id+'">'+value.sku_type+'</option>';
              });
              $('.product_name').append(product_option);
              $('.product_sku').append(product_sku_option);
              $('.product_sku_type').append(product_sku_type_option);
              // $('select[name=deliveryAddress]').html(option).trigger('change');
            },
            error: function() { alert("Error posting feed."); }
          });
        }

        function validateNumbers(e) {
          //get posted resource name value
          // var inputString = document.getElementById("resourceName").value;
          console.log($(this).val());
          //should be in the word\word\word format
          // var pattern=/^[a-z]+\\[a-z]+\\[a-z]+$/
          //If the inputString is NOT a match
          // if (!pattern.test(inputString)) {
          //   alert("not a match");
          // } else {
          //   alert("match");
          // }
        }

        function remove_product(e) {
    
          var id = $(e).data('id');
          var booking_id = $(e).data('booking-id');
          console.log(booking_id);
          Notiflix.Confirm.Show(
              'Confirm',
              'Are you sure that you want to delete this product?',
              'Yes',
              'No',
              function() {
                  Notiflix.Loading.Standard();
                  $.ajaxSetup({
                    headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
                  $.ajax({
                      url: "{{ url('shipper/shipment/remove-product') }}/"+id,
                      type: 'post',
                      dataType: "JSON",
                      data: {
                          "id": id,
                          "booking_id": booking_id,
                          "_method": 'DELETE',
                      },
                      success: function(returnData) {
                        console.log(returnData)
                          Notiflix.Loading.Remove();
                          if(returnData == 'success'){
                            Notiflix.Notify.Success('Product deleted.');
                            $(e).closest('.products').remove();
                          }else{
                            Notiflix.Notify.Failure('Something went wrong!');
                          }
                          // tr.remove();
                          // $("#edit_model").modal("hide");
                          // location.reload();
        
                      }
                  });
              });
        
        }
    </script>

    <script type="text/javascript">

        $(document).ready(function()
        {
          // $('.product_name').select2();

          $(document).on('change','.product_name',function(){
            var id = $(this).val();
            $(this).closest('.row').find('.product_sku').val(id);
            // $(this).closest('.row').find('.product_sku').select2().trigger('change');
            $(this).closest('.row').find('.product_sku_type').val(id);
          });

          $(document).on('change','.product_sku',function(){
            var id = $(this).val();
            $(this).closest('.row').find('.product_name').val(id);
            // $(this).closest('.row').find('.product_name').select2().trigger('change');
            $(this).closest('.row').find('.product_sku_type').val(id);                
          });

          // $(document).on('change','.product_sku',function(){
          //   var id = $(this).val();
          //   console.log(id);
          //   var product = $(this);
          //       // var dataString = 'id='+ id;
          //       // console.log(dataString);
          //       $.ajaxSetup({
          //             headers: {
          //               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          //             }
          //       });
          //       $.ajax
          //       ({
          //         type: "POST",
          //         url: "{{url('shipper/shipment/product/data')}}",
          //         data: {id: id},
          //         cache: false,
          //         success: function(data)
          //         {
          //             // var obj = JSON.parse(data);
          //             // console.log(obj.id);
          //             product.closest('.row').find('.product_name').val(JSON.parse(data).id);
          //             product.closest('.row').find('.product_name').select2().trigger('change');
          //             product.closest('.row').find('.product_sku_type').val(JSON.parse(data).id);
          //         }
          //       });
          // })

          // $("#product_id_1").change(function()
          // {
          //     var id=$(this).val();
          //     $.ajaxSetup({
          //           headers: {
          //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          //           }
          //     });
          //     $.ajax
          //     ({
          //     type: "POST",
          //     url: "{{url('shipper/shipment/product/data')}}",
          //     data: {id: id},
          //     cache: false,
          //     success: function(data)
          //     {
          //       document.getElementById('sku_no_1').value = JSON.parse(data).id;
          //       $('#sku_no_1').select2().trigger('change');
          //       document.getElementById('sku_type_1').value = JSON.parse(data).id;
          //     }
          //     });
          // });

          // $("#sku_no_1").change(function()
          // {
          //     var id=$(this).val();
          //     $.ajaxSetup({
          //           headers: {
          //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          //           }
          //     });
          //     $.ajax
          //     ({
          //     type: "POST",
          //     url: "{{url('shipper/shipment/product/data')}}",
          //     data: {id: id},
          //     cache: false,
          //     success: function(data)
          //     {
          //       document.getElementById('product_id_1').value = JSON.parse(data).id;
          //       $('#product_id_1').select2().trigger('change');
          //       document.getElementById('sku_type_1').value = JSON.parse(data).id;
          //     }
          //     });
          // });

          $(document).on('submit','#add_new_product',function () {
            var form = $(this);
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
              type: "POST",
              url: "{{ route('shipper.shipment.product.submit') }}",
              data: form.serialize(), // <--- THIS IS THE CHANGE
              dataType: "html",
              success: function(data){
                var response = JSON.parse(data);
                if(response.status == false){
                  $.each(response.messages, function(key,val){
                    if(val != ''){
                      $('#'+key+'_error').html(val);
                    }else{
                      $('#'+key+'_error').html('');
                    }
                  })
                }else{
                  get_products();
                  // get_facility_address();
                  $('#add_new_product').trigger('reset');
                  // $('#country').val('').trigger('change');
                  // $('#state').val('').trigger('change');
                  // $('#city').val('').trigger('change');
                  $('.add-product-cancel').click();
                }
              },
              error: function() { alert("Error posting feed."); }
            });
          });

        });
    </script>

   @endsection
