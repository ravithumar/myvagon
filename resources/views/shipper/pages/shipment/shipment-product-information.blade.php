@extends('shipper.layouts.master')
@section('content')
   {{-- <div class="content-page create-shipment-page">
   	  <div class="content"> --}}
   	  	 <div class="container-fluid">
   	  	 	<div class="row align-items-center">
   	  	 		<div class="col">
   	  	 			  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0">
                       <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">Create Shipment</a></li>
                       <li class="breadcrumb-item active txt-blue" aria-current="page">Truck Type</li>
                       <li class="breadcrumb-item active txt-blue" aria-current="page">Product</li>
                    </ol>
                </nav>
   	  	 		</div>
   	  	 	</div>
          <div class="row d-flex align-items-center products">
             <div class="col-md-8 col-lg-6">
                <h3>Product Information</h3>
                <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p>
             </div>
             <div class="col-md-4 col-lg-5"></div>
          </div>
          <form action="{{ route('shipper.shipment.create.step3') }}" method="POST">
            @csrf
          <div class="row">
             <div class="col">
                <div class="pro-info mt-4 input_fields_wrap">
                    <div class="pro-content mt-4 mb-4 product_info">
                      <div class="row pl-4" style="background-color:#f3eff7; margin-top: 2%;" id="product1">
                        <div class="col">
                          <div class="row mt-3 mt-md-4 mt-lg-5">
                            <div class="col-sm-4 col-md-6 col-lg-3">
                               <div class="form-group mb-4">
                                 <select class="form-control  form-control-without-border pb-1" name="product_id_1" id="product_id_1" style="background: none;" required>
                                    <option disabled selected>Product Name</option>
                                    @foreach ($productData as $product)
                                        <option value="{{ $product['id'] }}">{{ $product['name'] }}</option>
                                    @endforeach
                                 </select>
                               </div>
                            </div>
                            <div class="col-sm-4 col-md-6 col-lg-3">
                              <div class="form-group mb-4">
                                 <select class="form-control  form-control-without-border pb-1" style="background: none;" id="sku_no_1" disabled>
                                    <option  disabled selected>SKU #</option>
                                    @foreach ($productData as $product)
                                        <option value="{{ $product['id'] }}">{{ $product['sku_no'] }}</option>
                                    @endforeach
                                 </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-6 col-lg-3">
                              <div class="form-group mb-4 d-flex">
                                <select class="form-control  form-control-without-border pb-1" style="background: none;" id="sku_type_1" disabled>
                                    <option  disabled selected>SKU Type</option>
                                    @foreach ($productData as $product)
                                        <option value="{{ $product['id'] }}">{{ $product['sku_type'] }}</option>
                                    @endforeach
                                 </select>
                               </div>
                            </div>
                            <div class="d-sm-none col-lg-3 d-lg-block"></div>
                        </div>
                        <div class="row mt-3 mt-md-4 mt-lg-5">
                              <div class="col-sm-4 col-md-6 col-lg-3">
                                 <div class="form-group mb-4">
                                   {{-- <input type="text" id="package_type" name="package_type" class="form-control  form-control-without-border pb-2"  style="background: none;" placeholder="Package Type"> --}}
                                   <select class="form-control  form-control-without-border pb-1" style="background: none;" name="package_1" required>
                                    <option  disabled selected>Package Type</option>
                                    @foreach ($packageData as $package)
                                        <option value="{{ $package['id'] }}">{{ $package['name'] }}</option>
                                    @endforeach
                                   </select>
                                 </div>
                              </div>
                             <div class="col-sm-4 col-md-6 col-lg-3">
                               <div class="form-group mb-4">
                                  <input type="text" id="package_quantity" name="package_quantity_1" required class="form-control  form-control-without-border pb-2" style="background: none;" placeholder="Package Quantity">
                                </div>
                             </div>
                             <div class="col-sm-4 col-md-6 col-lg-3">
                               <div class="form-group mb-4 d-flex">
                                  <input type="text" id="weight" name="weight_1" class=" form-control-without-border pb-2 mr-1" style="background: none; width: 80%;" placeholder="Weight">
                                  <div class="qty ml-0">
                                    <select class="form-control  form-control-without-border pb-1" style="background: none;" name="unit_1" required>
                                        <option  disabled selected>Unit</option>
                                        @foreach ($unitData as $unit)
                                            <option value="{{ $unit['id'] }}">{{ $unit['name'] }}</option>
                                        @endforeach
                                       </select>
                                  </div>

                                </div>
                             </div>
                              <div class="d-sm-none col-lg-3 d-lg-block"></div>
                          </div>
                            <div class="specs-check d-flex mt-3">
                                <div class="form-group">
                                    <label><input type="checkbox" id="Fragile" class="largerCheckbox" name="fragile_1" value="1"><span></span></label>
                                    <label for="Fragile" class=" pl-2 txt-blue">Fragile</label>
                                 </div>
                                  <div class="form-group ml-3 ml-md-4 ml-lg-5">
                                    <label><input type="checkbox" id="sensitive" class="largerCheckbox" name="sensitive_1" value="1"><span></span></label>
                                    <label for="sensitive" class=" pl-2 txt-blue">Sensitive to odour</label>
                                 </div>
                            </div>
                          <div class="row">
                            <div class="col-md-7 col-lg-7 col-xl-5">
                              <div class="form-group">
                                  <textarea class="form-control " id="exampleFormControlTextarea1" rows="3" placeholder="Product Description" name="product_description_1"></textarea>
                              </div>
                            </div>
                            <div class="col-md-5 col-lg-5 col-xl-7">
                               <div class="delete-this">
                                  <a style="display: none !important; font-weight:600" id="productDelete1" href="javascript:;" class="btn btn-light delete-one font-12 mt-3 d-flex align-items-right remove_product">
                                    <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                    <span class="text ml-1">Delete</span>
                                  </a>
                               </div>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>
                    <div class="extra_products" id="extra_products"></div>
                </div>
                <a href="javascript:;" class="add-more-one  mt-3 d-flex add_field_button" onclick='clone()' id="addProduct">
                       <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="Plus" class="img-fluid">
                       <span class="text ml-1 text-decoration-none">Add More Products</span>
                </a>
                <div class="btn-block d-flex">
                    <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment">Continue</button>
                </div>
          </div>
        </form>

   	  	 </div>
   	  {{-- </div>
   </div> --}}

   @endsection

   @section('script')

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script>

       let i=2;

        function clone()
        {
                // console.log('i',i);
                var original = document.getElementById('product1' );
                var clone = original.cloneNode(true);
                clone.id = "product"+ i  ;
                let deleteElement=clone.getElementsByTagName('a')[0];
                let productSelectElement=clone.getElementsByTagName('select')[0];
                let skuNoSelectElement=clone.getElementsByTagName('select')[1];
                let skuTypeSelectElement=clone.getElementsByTagName('select')[2];
                let packageSelectElement=clone.getElementsByTagName('select')[3];
                let packageQuantityInputElement=clone.getElementsByTagName('input')[0];
                let weightInputElement=clone.getElementsByTagName('input')[1];
                let unitSelectElement=clone.getElementsByTagName('select')[4];
                let fragileCheckElement=clone.getElementsByTagName('input')[2];
                let sensitiveCheckElement=clone.getElementsByTagName('input')[3];
                let productTextElement=clone.getElementsByTagName('textarea')[0];

                let addElement = document.getElementById('addProduct');

                productSelectElement.name='product_id_'+i;
                productSelectElement.id= 'product_id_'+i;
                skuNoSelectElement.id= 'sku_no_'+i;
                skuTypeSelectElement.id= 'sku_type_'+i;
                packageSelectElement.name='package_'+i;
                packageQuantityInputElement.name='package_quantity_'+i;
                weightInputElement.name='weight_'+i;
                unitSelectElement.name='unit_'+i;
                fragileCheckElement.name='fragile_'+i;
                sensitiveCheckElement.name='sensitive_'+i;
                productTextElement.name='product_description_'+i;

                deleteElement.id = "productDelete"+ i;
                deleteElement.style.display='block';
                deleteElement.style.float='right';
                // document.getElementById('extra_products').parentNode.appendChild(clone);
                document.getElementById('extra_products').appendChild(clone);
                // console.log('i after',i);
                let newI=i;
                i+=1;
                if(i>5){
                    addElement.setAttribute('style', 'display:none !important');
                }
                document.getElementById('productDelete'+newI).addEventListener('click',function(){
                    // console.log('delete product ',newI);
                    document.getElementById("product"+ newI).remove();
                    i-=1;
                    if(newI<=5){
                        addElement.setAttribute('style', 'display:block !important');
                    }
                });

                document.getElementById('product_id_'+newI).addEventListener('change',function(){
                    var id=$(this).val();
                    // var dataString = 'id='+ id;
                    // console.log(dataString);
                    $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });
                    $.ajax
                    ({
                    type: "POST",
                    url: "{{url('shipper/shipment/product/data')}}",
                    data: {id: id},
                    cache: false,
                    success: function(data)
                    {
                        // var obj = JSON.parse(data);
                        // console.log(obj.id);
                        // console.log(data);
                        document.getElementById('sku_no_'+newI).value = JSON.parse(data).id;
                        document.getElementById('sku_type_'+newI).value = JSON.parse(data).id;

                    }
                    });

                });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function()
        {
            $("#product_id_1").change(function()
            {
                var id=$(this).val();
                // var dataString = 'id='+ id;
                // console.log(dataString);
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
                $.ajax
                ({
                type: "POST",
                url: "{{url('shipper/shipment/product/data')}}",
                data: {id: id},
                cache: false,
                success: function(data)
                {
                    // var obj = JSON.parse(data);
                    // console.log(obj.id);
                    // console.log(data);
                    document.getElementById('sku_no_1').value = JSON.parse(data).id;
                    document.getElementById('sku_type_1').value = JSON.parse(data).id;

                }
                });

            });

        });
    </script>

   @endsection
