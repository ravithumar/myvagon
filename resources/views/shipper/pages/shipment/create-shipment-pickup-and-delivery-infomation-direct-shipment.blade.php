@extends('shipper.layouts.master')

@section('content')

    <div class="container-fluid">
             <div class="row align-items-center">
                 <div class="col">
                       <nav aria-label="breadcrumb">
               <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">Create Shipment</a></li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">Truck Type</li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">Commodity Information</li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">Pickup &amp; Delivery Information</li>
               </ol>
           </nav>
                 </div>
             </div>
        <div class="row d-flex align-items-center products">
            <div class="col-md-8 col-lg-6">
            <h3 class="txt-blue">Pickup &amp; Delivery Information</h3>
            <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p>
            </div>
            <div class="col-md-4 col-lg-5"></div>
        </div>
     <div class="pickup-content">
      <div class="row">
        <div class="col">
                @php
                    if(count($products)>1){
                        $visible = 'pointer-events: none;';
                        $product = 'Products';
                        $warning = '';
                    }else{
                        $visible = 'pointer-events: none;';
                        $product = 'Product';
                        $warning = 'So You can only select direct shipment';
                    }
                @endphp

            <h3 class="txt-blue">{{ count($products) }} {{ $product }} has been added for <img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid"></h3>
            <p class="text-danger">{{ $warning }}</p>
           <ul class="nav nav-tabs-black">
                <li class="nav-item">
                        <a href="#direct-shipment" data-toggle="tab" aria-expanded="false" class="nav-link active btn font-10 mt-3 btn-outline-primary">Direct Shipments</a>
                </li>
                <li class="nav-item">
                        <a href="#multiple-stops" data-toggle="tab" aria-expanded="true" style="{{ $visible }}" class="ml-2 nav-link multiple_stops font-16 btn font-10 mt-3 btn-outline-primary">Multiple Stops</a>
                </li>
           </ul>
           <hr style="border: none; border-bottom: 1px solid silver;">
           <div class="tab-content">
               <form action="{{ route('shipper.shipment.store.step3') }}" method="post">
                @csrf
                    <div class="tab-pane show active" id="direct-shipment">
                        <div class="details-content">
                           <p class="font-16 color-dark-purple mb-3 txt-blue">Pickup Details</p>
                           <div class="block">
                              <div class="row">

                                <div class="col-sm-3 col-md-3 col-lg-3 dropdown">

                                    <a class="dropdown-toggle form-control  form-control-without-border pb-1 pl-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none;">
                                      Pickup Location <i class="fa fa-angle-down ml-5" style="padding-left: 150px;"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-md p-2" aria-labelledby="dropdownMenuLink">
                                        <div class="dropdown-item noti-title">
                                              <h5 class="m-0">
                                                 <a href="javascript:;" class="txt-blue" data-toggle="modal" data-target="#add-address">
                                                <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                                <span class="ml-1" style="color: #9B51E0;">Add Facility Address</span>
                                              </a>
                                              </h5>
                                         </div>
                                              <hr class="m-0" style="border-top: 1px solid #ccc;">
                                             @foreach ($addresses as $key => $address)
                                             @php
                                                   
                                             @endphp
                                               <div class="form-group m-b-1">
                                                     <b class="w-100 d-block">{{ $address['name'] }}</b> 
                                                     <small>{{ $address['address'] }}</small>
                                               <hr class="m-0 mt-1" style="border-top: 1px solid #ccc;">
                                               </div>
                                             @endforeach
                                   </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="form-group mb-4">
                                        <input type="date" id="pickup_date" name="pickup_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                    <div class="form-group mb-4 d-flex">
                                          <label><input type="checkbox" id="pickup_time_range_check"  class="largerCheckbox" name="range" value="1" onclick="showPickUpRange()"><span></span></label>
                                          <label for="range" class="txt-blue">Range</label>
                                          {{-- Range Div --}}
                                          <div id="pickup_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                              <input type="time" id="pickup_hour_from" name="pickup_hour_from" class=" form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%">
                                              <select class="form-control  form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_from" required>
                                                  <option value="am">AM</option>
                                                  <option value="pm">PM</option>
                                             </select>
                                              <span class="txt-blue ml-2 mr-2">TO</span>
                                              <input type="time" id="pickup_hour_to" name="pickup_hour_to" class=" form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%">
                                              <select class="form-control  form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_to" required>
                                                  <option value="am">AM</option>
                                                  <option value="pm">PM</option>
                                             </select>
                                          </div>
                                          {{-- Single time div --}}
                                          <div id="pickup_single_time" class="" style="display: flex;">
                                              <input type="time" id="pickup_hour" name="pickup_hour" class="font-18 form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;">
                                              <select class="form-control  form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="pickup_time_format" required>
                                                      <option value="am">AM</option>
                                                      <option value="pm">PM</option>
                                              </select>

                                          </div>
                                    </div>
                                </div>
                                  <div class="d-sm-none col-lg-3 d-lg-block"></div>
                              </div>
                              <div class="row">
                                <div class="col-md-5">
                                    <div class="note-area md-lg-5">
                                       <ul class="d-flex m-0 p-0 mb-2">
                                          <label class="mr-2 txt-blue">Note</label>
                                          <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changePickUpNote()"><img src="{{ asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                                          <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearPickUpNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label>
                                       </ul>
                                       <div class="form-group">
                                           <textarea class="form-control" id="pickUpNote" rows="2" readonly="readonly" name="pickup_note">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </textarea>
                                       </div>
                                    </div>
                                </div>
                               <div class="col-md-7"></div>
                              </div>
                           </div>
                        </div>
                        <hr style="border: none; border-bottom: 1px solid silver;">
                        <div class="details-content mt-4">
                           <p class="color-dark-purple mb-3 txt-blue">Delivery Details</p>
                           <div class="block">
                              <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                    <a class="dropdown-toggle form-control  form-control-without-border pb-1 pl-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none;">
                                      Delivery Location <i class="fa fa-angle-down ml-5" style="padding-left: 150px;"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-md p-2" >
                                        <div class="dropdown-item noti-title">
                                              <h5 class="m-0">
                                                 <a href="javascript:;" class="txt-blue" data-toggle="modal" data-target="#add-address">
                                                <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                                <span class="ml-1" style="color: #9B51E0;">Add Facility Address</span>
                                              </a>
                                              </h5>
                                         </div>
                                              <hr class="m-0" style="border-top: 1px solid #ccc;">
                                             @foreach ($addresses as $key => $address)
                                             @php
                                                   
                                             @endphp
                                               <div class="form-group m-b-1">
                                                     <b class="w-100 d-block">{{ $address['name'] }}</b> 
                                                     <small>{{ $address['address'] }}</small>
                                               <hr class="m-0 mt-1" style="border-top: 1px solid #ccc;">
                                               </div>
                                             @endforeach
                                   </div>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="form-group mb-4">
                                        <input type="date" id="delivery_date" name="delivery_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY">
                                      </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                    <div class="form-group mb-4 d-flex">
                                          <label><input type="checkbox" id="delivery_time_range_check" class="largerCheckbox" name="range" value="1" onclick="showDeliveryRange()"><span></span></label>
                                          <label for="range" class="txt-blue">Range</label>
                                          {{-- Range Div --}}
                                          <div id="delivery_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                              <input type="time" id="delivery_hour_from" name="delivery_hour_from" class=" form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%">
                                              <select class="form-control form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_from" required>
                                                  <option value="am">AM</option>
                                                  <option value="pm">PM</option>
                                             </select>
                                              <span class="font-18 txt-blue ml-2 mr-2">TO</span>
                                              <input type="time" id="delivery_hour_to" name="delivery_hour_to" class="font-18 form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%">
                                              <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_to" required>
                                                  <option value="am">AM</option>
                                                  <option value="pm">PM</option>
                                             </select>
                                          </div>
                                          {{-- Single time div --}}
                                          <div id="delivery_single_time" class="" style="display: flex;">
                                              <input type="time" id="delivery_hour" name="delivery_hour" class="font-18 form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;">
                                              <select class="form-control  form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="delivery_time_format" required>
                                                      <option value="am">AM</option>
                                                      <option value="pm">PM</option>
                                              </select>

                                          </div>
                                    </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-5">
                                    <div class="note-area md-lg-5">
                                        <a href="javascript:;" class="add-one  d-flex show_button" id="add_note">
                                            <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                                            <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add Note</span>
                                            {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                                         </a><br>
                                         <textarea rows="2" name="delivery_note" id="text_id" class="form-control" style="display:none" placeholder="Enter Delivery Note"></textarea>
                                    </div>
                                </div>
                               <div class="col-md-7"></div>
                              </div>
                           </div>
                        </div>
                        {{-- <a href="javascript:;" class="black-btn font-16 mt-5" data-toggle="modal" data-target="#edit-shipment">Insert Quote</a> --}}
                        <div class="btn-block d-flex">
                            <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment">Insert Quote</button>
                        </div>
                    </div>
                </form>
                    <div class="tab-pane" id="multiple-stops">
                        <div class="row">
                            <div class="col">
                               <div class="pro-info mt-4 input_fields_wrap">
                                   <div class="pro-content mt-4 mb-4 location_info">
                                     <div class="row pl-4" style="background-color:#f3eff7; margin-top: 2%;" id="location1">
                                        <div class="col">
                                           <p class="font-20 color-dark-purple mt-2 mb-3 txt-blue">Location<span id="locationLabel1"> 1 Details<span></p>
                                         <div class="row mt-2 mt-md-3 mt-lg-4">
                                           <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                            <a class="dropdown-toggle form-control font-18 form-control-without-border pb-1 pl-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none;">
                                                Facility Address <i class="fa fa-angle-down ml-5" style="padding-left: 150px;"></i>
                                              </a>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                  <ul class="m-0 p-0" style="border: 1px silver;">
                                                    <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                                        <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                                        <span class="ml-1 font-20" style="color: #9B51E0;">Add Facility Address</span>
                                                      </a>
                                                      <hr style="border: none; border-bottom: 1px solid silver;">
                                                      @foreach ($addresses as $key => $address)
                                                      @php
                                                             if($key < $count){
                                                                 $style = 'border-bottom: 2px solid silver';
                                                             }else{
                                                                 $style = '';
                                                             }
                                                      @endphp
                                                        <div class="form-group mb-3" style="{{ $style }}">
                                                             {{-- <input type="checkbox" id="Curtainside" class="largerCheckbox" name="truck_type_category[]" value="{{ $truck_cat['id'] }}"> --}}
                                                             <label><input type="radio" class="addressOptions" id="address1" name="address" value="1"><span></span></label>
                                                             <label for="address" class="font-14 ml-1  txt-blue">Abc Inc <br> <em>Mithimnis 30, Athina 112 57, Greece</em></label>
                                                        </div>
                                                      @endforeach
                                               </ul>
                                             </div>
                                           </div>
                                           <div class="col-sm-3 col-md-3 col-lg-3">
                                                <div class="form-group mb-4">
                                                    <input type="date" id="delivery_date1" name="delivery_date1" required class="form-control font-18 form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                                <div class="form-group mb-4 d-flex">
                                                      <label><input type="checkbox" id="location1_time_range_check" class="largerCheckbox" name="location1_time_range_check" value="1" onclick="showLocationRange()"><span></span></label>
                                                      <label for="range" class="font-18 txt-blue">Range</label>
                                                      {{-- Range Div --}}
                                                      <div id="location1_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                                          <input type="time" id="Hour" name="Hour" class="font-18 form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%">
                                                          <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="time_format" required>
                                                              <option value="am">AM</option>
                                                              <option value="pm">PM</option>
                                                         </select>
                                                          <span class="font-18 txt-blue ml-2 mr-2">TO</span>
                                                          <input type="time" id="Hour" name="Hour" class="font-18 form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%">
                                                          <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="time_format" required>
                                                              <option value="am">AM</option>
                                                              <option value="pm">PM</option>
                                                         </select>
                                                      </div>
                                                      {{-- Single time div --}}
                                                      <div id="location1_single_time" class="" style="display: flex;">
                                                          <input type="time" id="Hour" name="Hour" class="font-18 form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;">
                                                          <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="time_format" required>
                                                                  <option value="am">AM</option>
                                                                  <option value="pm">PM</option>
                                                          </select>

                                                      </div>
                                                </div>
                                            </div>
                                           <div class="d-sm-none col-lg-3 d-lg-block"></div>
                                       </div>
                                       <div class="row">
                                        <div class="col-md-5">
                                            <div class="note-area md-lg-5">
                                               <ul class="d-flex m-0 p-0 mb-2">
                                                  <label class="mr-2 font-18 txt-blue">Note</label>
                                                  <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changelocation1Note()""><img src="{{ asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                                                  <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearlocation1Note()""><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label>
                                               </ul>
                                               <div class="form-group">
                                                   <textarea class="form-control font-18" id="location1Note" name="location1Note" rows="2" readonly="readonly">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </textarea>
                                               </div>
                                            </div>
                                        </div>
                                       <div class="col-md-7"></div>
                                      </div>
                                       <div class="row mt-1 mt-md-2 mt-lg-3">
                                             <div class="col-sm-2 col-md-2 col-lg-2">
                                                <p class="font-20 color-dark-purple mt-2 mb-3 txt-blue">Product Information</p>
                                                @foreach ($products as $key => $product)
                                                <label class="ml-3"><input type="checkbox" id="location1products" class="largerCheckbox" name="location1products[]" value="{{ $product['product_id'] }}""><span></span></label>
                                                <label for="range" class="font-18 txt-blue ml-3">{{ $product['product_name'] }}</label><br>
                                                @endforeach

                                             </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2 mt-1 mt-md-2 mt-lg-3">
                                              <div class="form-group mb-4">
                                                 <input type="radio" id="delivery_type1" name="delivery_type1" style="height:30px; width:30px; vertical-align: top;" class="delivery_type pb-2" value="pickup">
                                                 <label for="range" class="font-18 txt-blue">Pickup</label>
                                                 <input type="radio" id="delivery_type1" name="delivery_type1" style="height:30px; width:30px; vertical-align: top;" class="delivery_type pb-2 ml-2" value="delivery">
                                                 <label for="range" class="font-18 txt-blue">Delivery</label><br>
                                                </div>
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                                <div class="form-group mb-4 ml-3">
                                                   <input type="text" id="quantity" name="quantity_1" required class="form-control font-18 form-control-without-border pb-2" style="background: none;" placeholder="Quantity">
                                                 </div>
                                              </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                              <div class="form-group mb-4 d-flex ml-3">
                                                 <input type="text" id="weight" name="weight_1" class="font-18 form-control-without-border pb-2 mr-1" style="background: none; width: 80%;" placeholder="Weight">
                                                 <div class="qty ml-0">
                                                   <select class="form-control font-18 form-control-without-border pb-1 ml-2" style="background: none; width : 120%;" name="unit_1" required>
                                                       <option  disabled selected>Unit</option>
                                                        @foreach ($unitData as $unit)
                                                            <option value="{{ $unit['id'] }}">{{ $unit['name'] }}</option>
                                                        @endforeach
                                                      </select>
                                                 </div>

                                               </div>
                                            </div>
                                             <div class="d-sm-none col-lg-3 d-lg-block"></div>
                                         </div>
                                           <div class="specs-check d-flex mt-3">

                                           </div>
                                         <div class="row">
                                           <div class="col-md-7 col-lg-7 col-xl-5">
                                             <div class="form-group">

                                             </div>
                                           </div>
                                           <div class="col-md-5 col-lg-5 col-xl-7">
                                              <div class="delete-this mb-5">
                                                 <a style="display: none !important; font-weight:600" id="locationDelete1" href="javascript:;" class="btn btn-light delete-one font-12 mt-3 d-flex align-items-right remove_product">
                                                   <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                                   <span class="text ml-1">Delete</span>
                                                 </a>
                                              </div>
                                           </div>
                                         </div>
                                       </div>

                                    </div>

                                   </div>
                                   <div class="extra_locations" id="extra_locations"></div>
                               </div>
                               {{-- @if(count($products) > 1) --}}
                               <a href="javascript:;" class="add-more-one font-20 mt-3 d-flex add_field_button btn btn-outline-info" onclick='clone()' id="addLocation" style="border-radius: 16px; width: 20%; padding-left: 6%;">
                                      <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="Plus" class="img-fluid">
                                      <span class="text ml-1 text-decoration-none">Add stop</span>
                               </a>
                               {{-- @endif --}}
                               <div class="btn-block d-flex mt-5">
                                <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment">Insert Quote</button>
                            </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal fade add-address" id="add-address" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                          <div class="modal-content">
                            <div class="modal-body">
                              <div class="edits mb-2 d-flex align-items-center justify-content-between">
                                <div class="title font-20 text-left">Add Facility Address</div>
                              </div>
                              <form action="{{ route('shipper.address.submit') }}" method="POST">
                                @csrf
                                <div class="form-group mb-3">
                                  <label for="name" class="mb-2 text-grey font-14">Name</label>
                                  <input type="text" id="name" maxlength="40" autocomplete="off" onkeypress="return lettersValidate(event)" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="Enter Name" value="{{ old('name') }}">
                                  @error('name')
                                  <div class="error">{{ $message }}</div>
                                  @enderror
                                </div>

                                <div class="form-group mb-3">
                                  <label for="first-name" class="mb-2 text-grey font-14">Address</label>
                                  <input type="text" id="address" name="address" autocomplete="off" onkeypress="return lettersValidate(event)" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ old('address') }}" placeholder="Enter Address">
                                  @error('address')
                                  <div class="error">{{ $message }}</div>
                                  @enderror
                                </div>

                                <div class="row">
                                  <div class="col-md-6">

                                  <div class="form-group  mb-3">
                                  <select  id="city" name="city" class="form-control form-control-without-border customselect-div" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required >
                                        <option selected="selected" value="">Choose..</option>
                                        @if(isset($cities))
                                        @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                        @endif
                                      </select>
                                      @error('city')
                                      <div class="error">{{ $message }}</div>
                                      @enderror
                                      </div>
                                  </div>
                                  <div class="col-md-6">

                                  <div class="form-group  mb-3">

                                      <select  id="state" name="state" class="form-control form-control-without-border customselect-div" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                                        <option selected="selected" value="">Choose..</option>
                                        @if(isset($states))
                                        @foreach($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                        @endif
                                      </select>
                                      @error('city')
                                      <div class="error">{{ $message }}</div>
                                      @enderror
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-6">

                                    <div class="form-group  mb-3">
                                      <select id="country" name="country" class="form-control form-control-without-border customselect-div"  data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                                        <option selected="selected" value="">Choose..</option>
                                        @if(isset($countries))
                                        @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                        @endif
                                      </select>
                                      @error('city')
                                      <div class="error">{{ $message }}</div>
                                      @enderror

                                    </div>
                                  </div>


                                  <div class="col-md-6">
                                    <div class="form-group  mb-3">

                                      <input type="text" id="postcode"  autocomplete="off" onkeypress="return numberValidate(event)" name="postcode" maxlength="6" parsley-trigger="change" required class="form-control form-control-without-border pb-2" placeholder="Postcode" >
                                      <!-- <div class="icon location">
                                        <img src="images/gps.svg" style="height: 72%;" class="img-fluid">
                                      </div> -->
                                      @error('name')
                                      <div class="error">{{ $message }}</div>
                                      @enderror


                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col">
                                    <div class="form-group  mb-3">
                                    <label for="first-name" class="mb-2 text-grey font-14">Phone</label>
                                      <input type="call" id="phone" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="8" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Phone">
                                      @error('name')
                                      <div class="error">{{ $message }}</div>
                                      @enderror

                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group  mb-3">
                                    <label for="first-name" class="mb-2 text-grey font-14">Email</label>
                                      <input type="email" id="email" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Email">
                                      @error('name')
                                      <div class="error">{{ $message }}</div>
                                      @enderror
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col">
                                    <div class="add-facility">
                                      <div class="form-group checkbox-inline">
                                        <input type="checkbox" value="1" name="restroom_service" id="Restrooms" checked="checked">
                                        <label for="Restrooms" class="font-15">Restrooms</label>
                                      </div>
                                      <div class="form-group">
                                        <input type="checkbox" value="1"  name="food_service" id="Food" checked="checked">
                                        <label for="Food" class="font-15">Food</label>
                                      </div>
                                      <div class="form-group">
                                        <input type="checkbox" value="1"  name="rest_area_service" id="Driver Rest Area" checked="checked">
                                        <label for="Driver Rest Area" class="font-15">Driver Rest Area</label>
                                      </div>

                                    </div>
                                  </div>
                                </div>


                                <div class="btn-block d-flex">
                                  <button type="button" class="btn w-100 btn-white mr-2" data-dismiss="modal" aria-label="Close">Cancel</button>
                                  <button type="submit" class="btn w-100 btn-primary">Save</button>
                                </div>

                              </form>

                            </div>
                          </div>
                        </div>
                      </div>
           </div>
        </div>

     </div>
    </div>
@endsection

@section('script')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

<script>

    let i=2;

     function clone()
     {
            //  console.log('i',i);
             var original = document.getElementById('location1' );
             var clone = original.cloneNode(true);
             clone.id = "location"+ i  ;
             let deleteElement=clone.getElementsByTagName('a')[4];
             let locationLabelElement=clone.getElementsByTagName('span')[0];
             let addressSelectElement=clone.getElementsByTagName('select')[0];
             let deliveryDateInputElement=clone.getElementsByTagName('input')[0];
             let rangeInputElement=clone.getElementsByTagName('input')[1];
             let hourInputElement=clone.getElementsByTagName('input')[2];
             let locationNoteElement=clone.getElementsByTagName('textarea')[0];
             let updateNoteElement=clone.getElementsByTagName('a')[2];
             let clearNoteElement=clone.getElementsByTagName('a')[3];
            //  let productsInputElement=clone.getElementsByTagName('input')[5];
            //  let pickUpInputElement=clone.getElementsByTagName('input')[7];
            //  let deliveryInputElement=clone.getElementsByTagName('input')[8];
             let quantityInputElement=clone.getElementsByTagName('input')[9];
             let weightInputElement=clone.getElementsByTagName('input')[10];
             let unitSelectElement=clone.getElementsByTagName('select')[3];

            //  console.log(clone.children[0]);

             let addElement = document.getElementById('addLocation');

             addressSelectElement.name='product_id_'+i;
             locationLabelElement.textContent=' '+i+' Details';
             deliveryDateInputElement.name='delivery_date'+i;
             deliveryDateInputElement.id='delivery_date'+i;
             rangeInputElement.name='location'+i+'_time_range_check';
             rangeInputElement.id='location'+i+'_time_range_check';
             locationNoteElement.name='location'+i+'Note';
             locationNoteElement.id='location'+i+'Note';
             quantityInputElement.name='quantity_'+i;
            //  pickUpInputElement.name='delivery_type'+i;
            //  deliveryInputElement.name='delivery_type'+i;
             weightInputElement.name='weight_'+i;
             unitSelectElement.name='unit_'+i;

             updateNoteElement.id = "changelocation"+ i;
             updateNoteElement.onclick = '';
             clearNoteElement.id = "clearlocation"+ i;
             clearNoteElement.onclick = '';

             deleteElement.id = "locationDelete"+ i;
             deleteElement.style.display='block';
             deleteElement.style.float='right';
            //  console.log(clone);
             // document.getElementById('extra_products').parentNode.appendChild(clone);
             document.getElementById('extra_locations').appendChild(clone);
             // console.log('i after',i);
             const elementsThatHaveId = [...clone.querySelectorAll('[id]')];
             elementsThatHaveId.forEach((element)=>{
                //  console.log('element',element.id);
                if(element.name == 'location1products[]'){
                    element.name = 'location'+i+'products[]';
                }
                if(element.name == 'delivery_type1')
                {
                    element.name = 'delivery_type'+i;
                }
                if(element.id == 'location1_time_range'){
                    element.id = 'location'+i+'_time_range';
                }
                if(element.id == 'location1_single_time'){
                    element.id = 'location'+i+'_single_time';
                }

                // element.id = element.id+'_'+i;
             })
            //  console.log( [...clone.querySelectorAll('[id]')]);

             let newI=i;
             i+=1;
             let productCount = <?php echo count($products); ?>;
            //  console.log(productCount);
             if(i>productCount){
                 addElement.setAttribute('style', 'display:none !important');
             }
             document.getElementById('locationDelete'+newI).addEventListener('click',function(){
                 // console.log('delete product ',newI);
                 document.getElementById("location"+ newI).remove();
                 i-=1;
                 if(newI<=productCount){
                     addElement.setAttribute('style', 'display:inline !important;border-radius: 16px; width: 20%;padding-left: 6%;padding-right : 6%;');
                 }
             });
             document.getElementById('changelocation'+newI).addEventListener('click',function(){
                 document.getElementById('location'+newI+'Note').readOnly =false;
             });
             document.getElementById('clearlocation'+newI).addEventListener('click',function(){
                 document.getElementById('location'+newI+'Note').value = "";
                 document.getElementById('location'+newI+'Note').readOnly =false;
             });

             document.getElementById('location'+newI+'_time_range_check').addEventListener('click',function(){
                //  console.log(this.checked);
                 var time_range = document.getElementById('location'+newI+'_time_range');
                 var single_time = document.getElementById('location'+newI+'_single_time');
                 if(this.checked == true){
                    time_range.style.display = "flex";
                    single_time.style.display = "none";
                 }else{
                    time_range.style.display = "none";
                    single_time.style.display = "flex";
                 }
             });
     }
 </script>

<script>$(".show_button").click(function(){$("#text_id").toggle()})</script>
    <script type="text/javascript">
    //Pickup functions
    function showPickUpRange() {
        var checkBox = document.getElementById("pickup_time_range_check");

        var time_range = document.getElementById("pickup_time_range");
        var single_time = document.getElementById("pickup_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
        }
    }

    function changePickUpNote() {
        document.getElementById('pickUpNote').readOnly =false;
    }

    function clearPickUpNote() {
        document.getElementById('pickUpNote').value = "";
        document.getElementById('pickUpNote').readOnly =false;
    }

    //Delivery functions
    function showDeliveryRange() {
        var checkBox = document.getElementById("delivery_time_range_check");

        var time_range = document.getElementById("delivery_time_range");
        var single_time = document.getElementById("delivery_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
        }
    }

    function changeDeliveryNote() {
        document.getElementById('deliveryNote').readOnly =false;
    }

    function clearDeliveryNote() {
        document.getElementById('deliveryNote').value = "";
        document.getElementById('deliveryNote').readOnly =false;
    }

    //Multiple Location page functions

    function showLocationRange() {
        var checkBox = document.getElementById("location1_time_range_check");

        var time_range = document.getElementById("location1_time_range");
        var single_time = document.getElementById("location1_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
        }
    }

    function changelocation1Note() {
        document.getElementById('location1Note').readOnly =false;
    }

    function clearlocation1Note() {
        document.getElementById('location1Note').value = "";
        document.getElementById('location1Note').readOnly =false;
    }
    </script>

@endsection
