@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.delivery_type:after {
    width: 12px;
    height: 12px;
}
.delivery_type:checked:after {
    /* background-color: #9B51E0; */
}
.pac-container {
    z-index: 9999;
}
.modal-body {
    position: relative;
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1rem;
    padding: 18px;
}
#map {
  width: 100%;
  height: 100vh;
  /*margin-top: -547px;*/
  /*left: 0 !important;*/
  /*top: 30px !important;*/
  /*border-top-width: 24px;*/
  /*position: absolute;*/
  /*transform: rotate(90deg);*/
  /*margin: 500px;*/
  /*margin-top: 500px;*/
  /*margin-top: -643px; */
  width: 390px;
    /*float: left;*/
    height: 690px;
    /*overflow-y: scroll;*/
/*-webkit-mask-position: right;*/
    margin: 407px;
    margin-top: -700px;
}
.col-xl-8 {
    /*float: none;*/
     /*max-width: none; */
    max-height: 25px;
}
.modal-content {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    width: 166%;
    pointer-events: auto;
    background-color: #fff;
    background-clip: padding-box;
    border: 0 solid transparent;
    border-radius: -0.8rem;
    outline: 1;
    margin: -138px;
}
.card-body {
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    /* padding: 1.5
rem
; */
}
.card-body.table-responsive {
    /*padding: 0px;*/
    width: 153%;
}
</style>
<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb m-0">
              <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{ __('Create Shipment') }}</a></li>
              <li class="breadcrumb-item active txt-blue" aria-current="page">{{ __('Truck Type') }}</li>
              <!-- lang-changes -->
              <!-- <li class="breadcrumb-item active txt-blue" aria-current="page">{{ __('Truck Type') }} </li> -->
              <!-- lang-changes -->
              <li class="breadcrumb-item active txt-blue" aria-current="page"> {{ __('Commodity Information') }}</li>
              <li class="breadcrumb-item active txt-blue" aria-current="page">{{ __('Pickup & Delivery Information') }}</li>
              </ol>
            </nav>
        </div>
    </div>
    <div class="row d-flex align-items-center products">
        <div class="col-md-8 col-lg-6">
        <h3 class="txt-blue">{{ __('Pickup & Delivery Information') }}</h3>
        <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p>
        </div>
        <div class="col-md-4 col-lg-5"></div>
    </div>
    <div class="pickup-content">
        <div class="row">
            <div class="col">
              @php
                  if(count($products)>1){
                      $visible = 'pointer-events: none;';
                      $product = 'Products';
                      $warning = '';
                  }else{
                      $visible = 'pointer-events: none;';
                      $product = 'Product';
                      $warning = 'So You can only select direct shipment';
                  }
              @endphp

              <h3 class="txt-blue">{{ count($products) }} {{ $product }} {{ __('has been added for') }} <img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid"></h3>
              <p class="text-danger">{{ $warning }}</p>
              @include('admin.include.flash-message')
              <div class="tab">
                <ul class="nav nav-tabs-black">
                  <li class="nav-item">
                    <a href="#direct-shipment" data-toggle="tab" aria-expanded="false" style="overflow: hidden; background-color: #ccc;" class="nav-link active btn font-10 mt-3 btn-outline-primary" class="tablinks" onclick="Stops(event, 'DirectShip')"> {{ __('Direct Shipments') }}</a>
                  </li>
                  <li class="nav-item" id="formButton">
                    <a href="#multiple-stops" data-toggle="tab" aria-expanded="true"  class="ml-4 nav-link multiple_stops font-16 btn font-10 mt-3 btn-outline-primary" id="formButton" class="tablinks" onclick="Stops(event, 'MultipleShip')">{{ __('Multiple Shipments') }}</a>
                  </li>
                </ul>
              </div>
              <hr style="border: none; border-bottom: 1px solid silver;">
              <div class="tabcontent" id="DirectShip" style="display:block;">
                  <form action="" method="post" id="final_form">
                  @csrf
                    <div class="tab-panel show active" id="direct-shipment">
                        <div class="details-content">
                            <p class="font-16 color-dark-purple mb-3 txt-blue">{{ __('Pickup Details')}} 
                            <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                            <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                            <span class="ml-1 mr-3" style="color: #9B51E0;">{{ __('Add Facility Address') }}</span>
                            </a></p>

                           <!--  <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                                <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                                <span class="ml-1 mr-3" style="color: #9B51E0;">{{ __('Add Facility Address')}}</span>
                                                </a> -->

                            <div class="block">
                              <div class="row">
                                  <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                    <select name="pickupAddress" required id="errorShowPickup" class="form-control location-input select2">
                                        <option value="">{{__('Select address')}}</option>
                                        @foreach ($addresses as $key => $address)
                                        <option value="{{$address->id}}">{{$address->name}}</option>
                                        @endforeach
                                    </select>
                                    <p class="text-danger" id="errorShowPickup" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> {{ __('This value is required') }}</p>
                                  </div>
                                  <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="form-group mb-4" id="errorShowPickup">
                                        {{-- <input type="date" id="pickup_date" name="pickup_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> --}}
                                        <input type="text" id="basic-datepicker" class="form-control form-control-without-border pb-2" name="pickup_date" required style="background: none; width: 100%;" placeholder="{{ __('DD/MM/YYYY') }}">
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                      <div class="form-group mb-4 d-flex">
                                          <label><input type="checkbox" id="pickup_time_range_check" class="largerCheckbox" name="pickup_time_range_check" value="1" onclick="showPickUpRange()"><span></span></label>
                                          <label for="range" class="txt-blue">{{ __('Range') }}</label>
                                          {{-- Range Div --}}
                                          <div id="pickup_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                              <input type="text" id="basic-pickup-timepicker1" name="pickup_hour_from" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%" >
                                              <input type="hidden" name="pickup_time_from">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_from" required>
                                                  <option value="am">{{ __('AM') }}</option>
                                                  <option value="pm">{{ __('PM') }}</option>
                                              </select> --}}

                                              <span class="txt-blue ml-2 mr-2">{{ __('TO') }}</span>
                                              <input type="text" id="basic-pickup-timepicker2" name="pickup_hour_to" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%">
                                              <input type="hidden" name="pickup_time_to">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_to" required>
                                                  <option value="am">{{ __('AM') }}</option>
                                                  <option value="pm">{{ __('PM') }}</option>
                                             </select> --}}
                                          </div>
                                          {{-- Single time div --}}
                                          <div id="pickup_single_time" class="row" style="display: flex;">
                                              <input type="text" id="basic-pickup-timepicker3" name="pickup_hour"   
                                              class="form-control-without-border pb-2 ml-4" placeholder="{{__('HH:MM')}}" style="background: none;" required="required">
                                              <input type="hidden" name="pickup_time_format">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="pickup_time_format" required>
                                                      <option value="am">{{ __('AM') }}</option>
                                                      <option value="pm">{{ __('PM') }}</option>
                                              </select> --}}
                                          </div>
                                      </div>
                                  </div>
                                  <div class="d-sm-none col-lg-3 d-lg-block"></div>
                              </div>
                              <div class="row">
                                <div class="col-md-5">
                                    <div class="note-area md-lg-5">
                                        <ul class="d-flex m-0 p-0 mb-2">
                                          <label class="mr-2 txt-blue"> {{ __('Note') }}</label>
                                          <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changePickUpNote()"><img src="{{ asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                                          <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearPickUpNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label>
                                        </ul>
                                        <div class="form-group">
                                          <textarea class="form-control" id="pickUpNote" rows="2" name="pickup_note" ></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7"></div>
                              </div>
                          </div>
                        </div>
                        <hr style="border: none; border-bottom: 1px solid silver;">
                        <div class="details-content mt-4">
                             <!--  <p class="font-16 color-dark-purple mb-3 txt-blue">{{ __('Delivery Details')}} <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                              <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                              <span class="ml-1 mr-3" style="color: #9B51E0;">{{ __('Add Facility Address') }}</span>
                              </a></p> -->

                          <p class="font-16 color-dark-purple txt-blue">{{ __('Delivery Details')}} <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                          <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                          <span class="ml-1 mr-3" style="color: #9B51E0;">{{ __('Add Facility Address')}}</span>
                          </a></p>
                          <div class="block">
                              <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                  <select name="deliveryAddress" required id="errorShowDrop" class="form-control location-input select2">
                                    <option value="" >{{__('Select address')}}</option>
                                    @foreach ($addresses as $key => $address)
                                    <option value="{{$address->id}}">{{$address->name}}</option>
                                    @endforeach
                                  </select>
                                  <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>{{ __('This value is required')}}<p>
                                </div>

                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="form-group mb-4">
                                        {{-- <input type="date" id="delivery_date" name="delivery_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> --}}
                                        <input type="text" id="basic-delivery-datepicker" class="form-control form-control-without-border pb-2" name="delivery_date" required style="background: none; width: 100%;" placeholder="{{ __('DD/MM/YYYY') }}">

                                         <p class="text-danger" id="errorShowDatePicker" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  
                                         End date must be come after the start date.</p>
                                        <p class="text-danger" id="errorShowDatePickerSame" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start date and end date not be same.</p>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                  <div class="form-group mb-4 d-flex">
                                    <label><input type="checkbox" id="delivery_time_range_check" class="largerCheckbox" name="delivery_time_range_check" value="1" onclick="showDeliveryRange()"><span></span></label>
                                    <label for="range" class="txt-blue">{{ __('Range') }}</label>
                                    {{-- Range Div --}}
                                    <div id="delivery_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                        <input type="text" id="basic-pickup-timepicker4" name="delivery_hour_from" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%">

                                        <input type="hidden" name="delivery_time_from">
                                        {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_from" required>
                                            <option value="am">AM</option>
                                            <option value="pm">PM</option>
                                       </select> --}}
                                        <span class="txt-blue ml-2 mr-2">{{ __('TO') }}</span>
                                        <input type="text" id="basic-pickup-timepicker5" name="delivery_hour_to" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%">
                                        <input type="hidden" name="delivery_time_to">
                                        {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_to" required>
                                            <option value="am">AM</option>
                                            <option value="pm">PM</option>
                                       </select> --}}
                                    </div>
                                    {{-- Single time div --}}
                                    <div id="delivery_single_time" class="row" style="display: flex;">
                                        <input type="text" id="basic-pickup-timepicker6" name="delivery_hour" class="form-control-without-border pb-2 ml-4" placeholder="{{__('HH:MM')}}" style="background: none;" required="required">
                                        <input type="hidden" name="delivery_time_format">
                                        {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="delivery_time_format" required>
                                                <option value="am">AM</option>
                                                <option value="pm">PM</option>
                                        </select> --}}
                                    </div>
                                  </div>
                                    <p class="text-danger" id="errorShowSameTime" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start time and end time not be same.</p>

                                </div>
                            </div>
                            <div class="row">
                              <div class="col-md-5">
                                  <div class="note-area md-lg-5">
                                    <a href="javascript:;" class="add-one font-18 d-flex show_button" id="add_note">
                                    <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                                    <span class="text ml-1 txt-blue" style="text-decoration: underline;">{{ __('Add Note') }}</span>
                                    {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                                    </a><br>
                                    <textarea rows="2" name="delivery_note" id="text_id" class="form-control" style="display:none" placeholder="Enter Delivery Note"></textarea>
                                  </div>
                              </div>
                              <div class="col-md-7"></div>
                            </div>
                        </div>
                      </div>
                        {{-- <a href="javascript:;" class="black-btn font-16 mt-5" data-toggle="modal" data-target="#edit-shipment">{{ __('Insert Quote')}}</a> --}}
                        <div class="btn-block d-flex">
                            <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" id="continuebtn">{{ __('Insert Quote')}}</button>
                        </div>
                    </div>
                    </form>
                  </div>
                </p>
              </p>
            </div>
          </div>
<form action="" method="post" id="final_form" onSubmit="return validate(form)">
    <div class="tabcontent" id="MultipleShip" style="display:none;">
      <input type="hidden" class="multiple_shipment_location_length" name="multiple_shipment_location_length" value="1">  
       {{-- <div class="location-details"> --}}
        <div class="row">
            <div class="col">
                <div class="pro-info input_fields_wrap">
                    <div class="pro-content  mb-4 location_info">
                        <div class="row" id="location1">
                            <div class="col">
                                <p class="font-16 color-dark-purple txt-blue">{{ __('Location')}}<span id="locationLabel1"> 1 {{ __('Details') }}<span><a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                <span class="ml-1 mr-3" style="color: #9B51E0;">{{ __('Add Facility Address')}}</span>
                                </a></p>
                                <div class="row mt-2 mt-md-3 mt-lg-4">
                                    <div class=" col-sm-3 col-md-3 col-lg-3 dropdown " >
                                       <select class="form-control location-input select2" style= "width: 100%;" placeholder="{{ __('DD/MM/YYYY')}}" name="location_add">
                                            <option value="">{{__('Select address')}}</option>
                                            @foreach ($addresses as $key => $address)
                                            <option value="{{$address->id}}">{{$address->name}}</option>
                                            @endforeach
                                        </select> 
                                        <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>{{ __('This value is required')}}</p>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="display: none;">
                                            <ul class="m-0 p-0" style="border: 1px silver;">
                                                <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                                <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                                <span class="ml-1 font-20" style="color: #9B51E0;">{{ __('Add Facility Address')}}</span>
                                                </a>
                                                <hr style="border: none; border-bottom: 1px solid silver;">
                                                @foreach ($addresses as $key => $address)
                                                @php
                                                       if($key < $count){
                                                           $style = 'border-bottom: 2px solid silver';
                                                       }else{
                                                           $style = '';
                                                       }
                                                @endphp
                                                <div class="form-group mb-3" style="{{ $style }}">
                                                     {{-- <input type="checkbox" id="Curtainside" class="largerCheckbox" name="truck_type_category[]" value="{{ $truck_cat['id'] }}"> --}}
                                                     <label><input type="radio" class="addressOptions" id="address1" name="address" value="1"><span></span></label>
                                                     <label for="address" class="font-14 ml-1  txt-blue">Abc Inc <br> <em>Mithimnis 30, Athina 112 57, Greece</em></label>
                                                </div>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 col-lg-3">
                                        <div class="form-group mb-4">
                                            <!-- <input type="text" id="basic-delivery-datepicker2" name="delivery_date1" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> -->
                                            <input type="text" id="datetimepicker4" class="form-control form-control-without-border pb-2 " name="delivery_date1" required style="background: none; width: 100%;" placeholder="{{ __('DD/MM/YYYY')}}" class="active">

                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6 mt-1 pickup_rang">
                                        <div class="form-group mb-4 d-flex location_rang_div">
                                            <label><input type="checkbox" id="location_time_range_check_" class="largerCheckbox" name="location_time_range_check_[]" value="1" name="range"><span></span></label>
                                            <label for="range" class="txt-blue">{{ __('Range')}}</label>
                        
                                            <div id="location_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                                <input type="text" id="basic-location-timepicker1" name="location_hour_from[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%" >
                                                <input type="hidden" name="location_time_from[]">
                                                <span class="txt-blue ml-2 mr-2">{{ __('TO')}}</span>
                                                <input type="text" id="basic-location-timepicker2" name="location_hour_to[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%">
                                                <input type="hidden" name="location_time_to[]">
                                            </div>
                        
                                            <div id="location_single_time" class="row" style="display: flex;">
                                                <input type="text" id="basic-location-timepicker3" name="location_hour[]" class="form-control-without-border pb-2 ml-4" placeholder="{{__('HH:MM')}}" style="background: none;" required="required">
                                                <input type="hidden" name="pickup_time_format[]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-sm-none col-lg-3 d-lg-block"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="note-area md-lg-5">
                                           <ul class="d-flex m-0 p-0 mb-2">
                                              <label class="mr-2 txt-blue">{{ __('Note')}}</label>
                                              <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changelocation1Note()" style="display:none"><img src="{{asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                                              <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearlocation1Note()"><img src="{{asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid" style="display:none"></a></label>
                                           </ul>
                                           <div class="form-group">
                                               <textarea class="form-control" id="location1Note" name="location1Note" rows="2"></textarea>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7"></div>
                                </div>
                                <div class="row mt-10 mt-md-12 mt-lg-3 hidden_div product_checkbox_div">
                                    <div class="col-sm-8 col-md-8 col-lg-2 product_checkbox_div" >
                                        <p class="font-14 color-dark-purple mt-2 mb-3 txt-blue">{{ __('Product Information')}}</p>
                                        @foreach ($products as $key => $product)
                                          <label class="ml-3"><input type="checkbox" id="location1products" class="largerCheckbox location1products" name="location1products[]" value="{{ $product['id'] }}" ><span></span></label>

                                          <label for="range" class="font-14 txt-blue ml-3" id="location_products">{{ $product['name'] }}</label><br>
                                        @endforeach
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2 mt-1 mt-md-2 mt-lg-3 show_Delivery" style="display:none;" id="show_Delivery" >
                                        <div class="row mt-3 mt-md-3 mt-lg-4" >
                                          <input type="radio" id="pickup_type1" name="pickup_type1" style="height:20px; width:20px; vertical-align: text-bottom; " class="delivery_type pb-2" value="pickup" checked onClick="pickup()">
                                          <label for="range" class="font-14 ">{{ __('Pickup')}}</label>
                                          <input type="radio" id="delivery_type1" name="delivery_type1" style="height:20px; width:20px; vertical-align: text-bottom;" class="delivery_type pb-2 ml-2" value="delivery" disabled>
                                          <label for="range" class="font-14 ">{{ __('Delivery')}}</label><br>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2 col-lg-2 show_Quantity" style="display:none;">
                                      <div class="row mt-3 mt-md-3 mt-lg-4" >
                                        <input type="text" id="quantity1" name="quantity_1[]" required class="form-control form-control-without-border pb-2" style="background: none; width: 90%;" placeholder="{{ __('Quantity')}}" min="1" value="30">
                                      </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-2 show_Weight" style="display:none;" >
                                      <div class="row mt-3 mt-md-3 mt-lg-4" >
                                        <input type="text" id="weight" name="weight_1[]" class="form-control form-control-without-border pb-2" style="background: none; width: 110%;" placeholder="{{ __('Weight')}}" min="1" value="30">
                                      </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-1  show_Unit" style="display:none; width: 100%;" >
                                      <div class="row mt-3 mt-md-3 mt-lg-4" >
                                        <select class="form-control form-control-without-border pb-1 ml-2 " style="background: none; " name="unit_1[]" required>
                                          <option  disabled selected>{{ __('Unit')}}</option>
                                            @foreach ($unitData as $unit)
                                                <option value="{{ $unit['id'] }}">{{ $unit['name'] }}</option>
                                            @endforeach
                                        </select>
                                      </div>
                                    </div>
                                    <div class="d-sm-none col-lg-3 d-lg-block"></div>
                                </div>
                                <div class="specs-check d-flex mt-3"></div>
                                <div class="row">
                                    <div class="col-md-7 col-lg-7 col-xl-5">
                                        <div class="form-group"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="extra_locations" id="extra_locations"></div>
                        <a href="javascript:;" class="add-more-one font-20 mt-3 d-flex add_field_button btn btn-outline-info add_stops" onclick="clone()" id="addLocation" style="border-radius: 16px; width: 20%; padding-left: 6%;">
                        <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="Plus" class="img-fluid">
                        <span class="text ml-1 text-decoration-none">{{ __('Add stop')}}</span>
                        </a>
                      <div class="btn-block d-flex">
                          <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" id="continuebtn">{{ __('Insert Quote')}}</button>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade add-address" id="add-address" tabindex="-1"  aria-labelledby="exampleModalCenterTitle" >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">{{ __('Add Facility Address')}}</div>
        </div>
        <div class="col-xl-4">
          <div class="card">
            <div class="card-body table-responsive" >
            {{-- <form action="{{ route('shipper.shipment.address.submit') }}" id="address_form" method="POST"> --}}
              <form action="javascript:void(0)" id="address_form" method="POST">
                @csrf
                <div class="form-group mb-3">
                  <label for="name" class="mb-2 text-grey font-14">Name</label>
                  <input type="text" id="name" maxlength="40" autocomplete="off" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="Enter Name" value="{{ old('name') }}">
                  @if($errors->has('name'))
                      <div class="error text-danger">{{ $errors->first('name') }}</div>
                  @endif
                </div>
  
                <div class="form-group mb-3">
                  <label for="first-name" class="mb-2 text-grey font-14">Address</label>
                  <input type="text" id="autocomplete" name="address" autocomplete="off" parsley-trigger="change" required class="autocomplate-input form-control form-control-without-border fulladdress" value="{{ old('address') }}" placeholder="Enter Address">
                  <input type="hidden" id="latitude" name="lat" value="">
                  <input type="hidden" id="longitude" name="lng" value="">
                  <input type="hidden" id="post_code" name="post_code" value="">
                  @if($errors->has('lat'))
                      <div class="error text-danger">{{ $errors->first('lat') }}</div>
                    @elseif($errors->has('address'))
                      <div class="error text-danger">{{ $errors->first('address') }}</div>
                  @endif
                </div>
      
                <div class="row">
                  <div class="col">
                    <div class="form-group  mb-3">
                      <label for="first-name" class="mb-2 text-grey font-14">Phone</label>
                      <input type="call" id="phone" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="8" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Phone">
                      @if($errors->has('phone'))
                        <div class="error text-danger">{{ $errors->first('phone') }}</div>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group  mb-3">
                    <label for="first-name" class="mb-2 text-grey font-14">Email</label>
                      <input type="email" id="email" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Email">
                      @if($errors->has('email'))
                        <div class="error text-danger">{{ $errors->first('email') }}</div>
                      @endif
                    </div>
                  </div>
                </div>
  
                <div class="row">
                  <div class="col">
                    <div class="add-facility">
                      <div class="form-group checkbox-inline">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="restroom_service" id="Restrooms" checked="checked"><span></span></label>
                        <label for="Restrooms" class="font-15">Restrooms</label>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="food_service" id="Food" checked="checked"><span></span></label>
                        <label for="Food" class="font-15">Food</label>
                      </div>
                      <div class="form-group">
                        <label><input type="checkbox" value="1" class="largerCheckbox" name="rest_area_service" id="Driver Rest Area" checked="checked"><span></span></label>
                        <label for="Driver Rest Area" class="font-15">Driver Rest Area</label>
                      </div>
       
                    </div>
                  </div>
                </div>
            </div>
            <div class="btn-block d-flex">
              <button type="button" class="btn w-100 btn-white mr-2 cancel-btn" data-dismiss="modal" aria-label="Close">{{ __('Cancel')}}</button>
              <button type="submit" class="btn w-100 btn-primary">{{ __('Save')}}</button>
            </div>
            <!-- <div class="btn-block d-flex">
              <a href="{{ route('shipper.address.index') }}" class="btn w-100 btn-white mr-2" >Cancel</a>
              <button type="submit" class="btn w-100 btn-primary">Save</button>
            </div>
             -->
          </form>
        </div>
      </div>
    </div>
    <div class="col-xl-8">
      <div id="map"></div>
    </div>
      </div>
    </div>
    
  </div>
</div>
     <!-- <div class="col-xl-8">
      <div id="map" style="overflow: visible;"></div>
    </div> -->      
<div class="pro-content  mb-4 location_info" style="display:none">
  <div class="row locations" id="location_hidden" style="background: none; margin-top: 2%;">
      <div class="col">
        <!-- <div class="pro-info input_fields_wrap"> -->
          <p class="font-16 color-dark-purple txt-blue">{{ __('Location')}}<span id="locationLabel1"> 1 {{ __('Details') }}<span><a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
            <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
            <span class="ml-1 mr-3" style="color: #9B51E0;">{{ __('Add Facility Address')}}</span>
            </a></p>
            <div class="row mt-2 mt-md-3 mt-lg-4">
                <div class=" col-sm-4 col-md-6 col-lg-3" >
                   <select class="form-select form-select-sm form-select-solid product_name pb-1" style= "width: 100%; background: none;" data-control="select2">
                        <option value="">{{__('Select address')}}</option>
                        @foreach ($addresses as $key => $address)
                        <option value="{{$address->id}}">{{$address->name}}</option>
                        @endforeach
                    </select> 

                    <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> {{ __('This value is required')}}.</p>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="display: none;">
                        <ul class="m-0 p-0" style="border: 1px silver;">
                            <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                            <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                            <span class="ml-1 font-20" style="color: #9B51E0;">{{ __('Add Facility Address')}}</span>
                            </a>
                            <hr style="border: none; border-bottom: 1px solid silver;">
                            @foreach ($addresses as $key => $address)
                            @php
                                   if($key < $count){
                                       $style = 'border-bottom: 2px solid silver';
                                   }else{
                                       $style = '';
                                   }
                            @endphp
                            <div class="form-group mb-3" style="{{ $style }}">
                                 {{-- <input type="checkbox" id="Curtainside" class="largerCheckbox" name="truck_type_category[]" value="{{ $truck_cat['id'] }}"> --}}
                                 <label><input type="radio" class="addressOptions" id="address1" name="address" value="1"><span></span></label>
                                 <label for="address" class="font-14 ml-1  txt-blue">Abc Inc <br> <em>Mithimnis 30, Athina 112 57, Greece</em></label>
                            </div>
                            @endforeach
                        </ul>
                    </div>
              </div>
              <div class="col-sm-3 col-md-3 col-lg-3">
                <!-- <div class="form-group mb-4"> -->
                    <!-- <input type="text" id="basic-delivery-datepicker2" name="delivery_date1" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> -->
                    <input type="text" id="datetimepicker4" class="form-control form-control-without-border pb-2 " name="delivery_date1" required style="background: none; width: 100%;" placeholder="{{ __('DD/MM/YYYY')}}" class="active">
                <!-- </div> -->
              </div>
              <div class="col-sm-6 col-md-6 col-lg-6 mt-1 pickup_rang">
                  <div class="form-group mb-4 d-flex location_rang_div">
                      <label><input type="checkbox" id="location_time_range_check_" class="largerCheckbox" name="location_time_range_check_[]" value="1"><span></span></label>
                      <label for="range" class="txt-blue">{{ __('Range')}}</label>
  
                      <div id="location_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                          <input type="text" id="basic-location-timepicker1" name="location_hour_from[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none; width: 30%" >
                          <input type="hidden" name="location_time_from[]">
                          <span class="txt-blue ml-2 mr-2">{{ __('TO')}}</span>
                          <input type="text" id="basic-location-timepicker2" name="location_hour_to[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="{{__('HH:MM')}}" style="background: none;  width: 30%">
                          <input type="hidden" name="location_time_to[]">
                      </div>
  
                      <div id="location_single_time" class="row" style="display: flex;">
                          <input type="text" id="basic-location-timepicker3" name="location_hour[]" class="form-control-without-border pb-2 ml-4" placeholder="{{__('HH:MM')}}" style="background: none;" required="required">
                          <input type="hidden" name="pickup_time_format[]">
                      </div>
                  </div>
              </div>
              <div class="d-sm-none col-lg-3 d-lg-block"></div>
          </div>
          <div class="row">
          <div class="col-md-5">
              <div class="note-area md-lg-5">
                 <ul class="d-flex m-0 p-0 mb-2">
                    <label class="mr-2 txt-blue">{{ __('Note')}}</label>
                    <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changelocation1Note()"><img src="{{asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                    <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearlocation1Note()"><img src="{{asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label>
                 </ul>
                 <div class="form-group">
                     <textarea class="form-control" id="location1Note" name="location1Note" rows="2"></textarea>
                 </div>
              </div>
          </div>
         <div class="col-md-7"></div>
        </div>
        <div class="row mt-10 mt-md-12 mt-lg-3 hidden_div" >
            <div class="col-sm-8 col-md-8 col-lg-2" >
                <p class="font-14 color-dark-purple mt-2 mb-3 txt-blue">{{ __('Product Information')}}</p>
                @foreach ($products as $key => $product)
                    <label class="ml-3"><input type="checkbox" id="location1products" class="largerCheckbox  location1products" name="location1products[]" value="{{ $product['id'] }}" ><span></span></label>
                    <label for="range" class="font-14 txt-blue ml-3" id="location_products">{{ $product['name'] }} </label><br>

                @endforeach
            </div>
                                                    
            <div class="col-sm-2 col-md-2 col-lg-2 mt-1 mt-md-2 mt-lg-3 show_Delivery2" id="show_Delivery2">
                <div class="row mt-3 mt-md-3 mt-lg-4" >
                  <input type="radio" id="delivery_type_2" name="delivery_type_2" style="height:20px; width:20px; vertical-align: text-bottom; " class="delivery_type pb-2" value="pickup">
                  <label for="range" class="font-14 ">{{ __('Pickup')}}</label>
                  <input type="radio" id="delivery_type_2" name="delivery_type_2" style="height:20px; width:20px; vertical-align: text-bottom;" class="delivery_type pb-2 ml-2" value="delivery">
                  <label for="range" class="font-14 ">{{ __('Delivery')}}</label><br>
                </div>
            </div>

            <div class="col-sm-2 col-md-2 col-lg-2 show_Quantity2">
              <div class="row mt-3 mt-md-3 mt-lg-4" >
                <input type="text" id="quantity2" name="quantity_1" required class="form-control form-control-without-border pb-2" onkeyup="onChange();" onkeypress="return numberValidate(event)" style="background: none; width: 90%;" @if(isset($product['quantity_1']) && !empty($product['quantity_1']))($product['quantity_1'] - $product['quantity_1']) @endif placeholder="{{ __('Quantity')}}" min="1">
              </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-2 show_Weight2" >
              <div class="row mt-3 mt-md-3 mt-lg-4" >
                <input type="text" id="weight" name="weight_1" class="form-control form-control-without-border pb-2" style="background: none; width: 110%;" onkeypress="return numberValidate(event)" placeholder="{{ __('Weight')}}" min="1">
              </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-1  show_Unit2" style=" width: 100%;" >
              <div class="row mt-3 mt-md-3 mt-lg-4" >
                <select class="form-control form-control-without-border pb-1 ml-2 " style="background: none; " name="unit_1" required>
                  <option  disabled selected>{{ __('Unit')}}</option>
                    @foreach ($unitData as $unit)
                        <option value="{{ $unit['id'] }}">{{ $unit['name'] }}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="d-sm-none col-lg-3 d-lg-block"></div>
        </div>
        <div class="specs-check d-flex mt-3">
        </div>
        <div class="row">
           <div class="col-md-7 col-lg-7 col-xl-5">
             <div class="form-group">
             </div>
           </div>
           <div class="col-md-5 col-lg-5 col-xl-7">
              <div class="delete-this" id="locationDelete">
                 <a style="display: inline-block !important; font-weight:600; !important;" id="locationDelete" href="javascript:;" class="btn btn-light delete-one font-12  d-flex remove_location">
                   <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                   <span class="text ml-1">{{ __('Delete')}}</span>
                 </a>
              </div>
           </div>
        </div>
      </div>
  </div>
</div>
</span>
</span>
</p>
</div>
</div>
</div>
@endsection
@section('script')
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="{{ URL::asset('shipper/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/js/pages/form-pickers.init.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
        $("#location1products").click(function () {
              $(".show_Delivery").toggle();
              $(".show_Quantity").toggle();
              $(".show_Weight").toggle();
              $(".show_Unit").toggle();
        });  
    });
</script>
<script type="text/javascript">
 function onChange() {
  var FirstqtyValue = document.getElementById('quantity1').value;
  var result = parseInt(30) - parseInt(FirstqtyValue);
  if (!isNaN(result)) {
      document.getElementById('quantity2').value = result;
  }
  if('quantity1' == 0 && 'quantity2' == 0){
           $(".hidden_div").hide();
        }
 }
</script>
<script type="text/javascript">
  function validate(form) {
    var errmsg = "Oops, you're required to complete the following fields! \n";

    if (form.location_add.value === "") {
        errmsg = errmsg + " - Choose Address\n";
    }

    if (form.delivery_date1.value === "") {
        errmsg = errmsg + " - delivery date\n";
    }

    if (form.range.value === "") {

        errmsg = errmsg + " - delivery date\n";
    }

    if (form.range.value === "") {

        errmsg = errmsg + " - delivery date\n";
    }

    if (errmsg === "Oops, you're required to complete the following fields! \n") {
        form.submit();
    } else {
        alert(errmsg);
        return false;
    }
}
</script>

<script type="text/javascript">
   $(document).ready(function(){
        $("#location2products").click(function () {
              $(".show_Delivery2").toggle();
              $(".show_Quantity2").toggle();
              $(".show_Weight2").toggle(); 
              $(".show_Unit2").toggle();
        });  
    });
</script>
<script type="text/javascript">
     $(function () {
             $("#datetimepicker4").flatpickr({minDate: "today"});
             var pickup_date = $('#datetimepicker4').val();
              $("#datetimepicker5").flatpickr({minDate: "today"});
             var pickup_date = $('#datetimepicker5').val();
         });
      
</script> 
<script type="text/javascript">
  $(document).on('click','.remove_location',function(){
          $(this).closest('.locations').remove();
          var length = $('.multiple_shipment_location_length').val();
          length = parseInt(length) - 1;
          $('.multiple_shipment_location_length').val(length);
          
          
      });

  // $(document).on('click','.multiple_shipment_submit',function(){
  //     alert('test');
  //     var extra_products_length = $('.extra_locations .row').length;

  //     console.log(extra_products_length);
  //   })
</script>
<script>
  $('#basic-datepicker').flatpickr({minDate: "today"});
  $('#basic-delivery-datepicker').flatpickr({minDate: "today"});
  $('#basic-pickup-timepicker6').on("input", function() {
      var pickup_date = $('#basic-datepicker').val();
      var dropoff_date = $('#basic-delivery-datepicker').val();
      var stime = $('#basic-pickup-timepicker3').val();   
      var etime = this.value;
      // console.log(stime,etime);
      if(pickup_date == dropoff_date){
        if(stime >= etime){
        document.getElementById('errorShowSameTime').style.display = 'block';
         $('#continuebtn').prop('disabled', true);
        }else{
          document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
        }
      }else{
        document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
      }
  });

  $('#basic-delivery-datepicker').on("input", function() {
    var pickup_date = $('#basic-datepicker').val();
      var dropoff_date = $('#basic-delivery-datepicker').val();
      var stime = $('#basic-pickup-timepicker3').val();   
      var etime = $('#basic-pickup-timepicker6').val();
      // console.log(stime,etime);
      if(pickup_date == dropoff_date){
        if(stime >= etime){
        document.getElementById('errorShowSameTime').style.display = 'block';
         $('#continuebtn').prop('disabled', true);
        }else{
          document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
        }
      }else{
        document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
      }
  });

  $('select[name=pickupAddress]').on("change", function() {
    var pickup_address = $(this).val();
    var dropoff_address = $('select[name=deliveryAddress]').val();
    if(pickup_address == dropoff_address){
      $('#same_address_error').show();
      $('#continuebtn').prop('disabled', true);
    }else{
      $('#same_address_error').hide();
      $('#continuebtn').prop('disabled', false);
    }
  });

  $('#basic-delivery-datepicker2').flatpickr({minDate: "today"});
   $('#basic-pickup-timepicker8').on("input", function() {
      var pickup_date = $('#basic-delivery-datepicker2').val();
      var stime = $('#basic-pickup-timepicker8').val();   
      var etime = $('#basic-pickup-timepicker9').val(); 
      // console.log(stime,etime);
      if(pickup_date != ''){
        if(stime >= etime){
        document.getElementById('errorShowSameTime').style.display = 'block';
         $('#continuebtn').prop('disabled', true);
        }else{
          document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
        }
      }else{
        document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
      }
     
  });

  $('select[name=deliveryAddress]').on("change", function() {
    var dropoff_address = $(this).val();
    var pickup_address = $('select[name=pickupAddress]').val();
    if(pickup_address == dropoff_address){
      $('#same_address_error').show();
      $('#continuebtn').prop('disabled', true);
    }else{
      $('#same_address_error').hide();
      $('#continuebtn').prop('disabled', false);
    }
  });

     let pickupid;

    function addPickupAddress(val, addressName, address){
        pickupid = val;

        let pickupAddress = document.getElementById('pickupAddress');
        let pickUpAddressTag = document.getElementById('PickupdropdownMenuLink');
        document.getElementById('pickupAddress').value = val;
        pickUpAddressTag.text = addressName+' '+address;
        document.getElementById('errorShowPickup').style.display = 'none';
        return false;
    }

    function addDeliveryAddress(val, addressName, address){
        if(pickupid == val){
          document.getElementById('errorShowDeliveryPickup').style.display = 'block';
        }else{
          document.getElementById('errorShowDeliveryPickup').style.display = 'none';
        }

        let deliveryAddress = document.getElementById('deliveryAddress');
        let deliveryAddressTag = document.getElementById('deliveryDropdownMenuLink');
        document.getElementById('deliveryAddress').value = val;
        deliveryAddressTag.text = addressName+' '+address;
        document.getElementById('errorShowDelivery').style.display = 'none';
        return false;
    }

    $(document).on("change","#location_time_range_check_",function(){
        var location_rang_div = $(this).closest(".location_rang_div");
        if($(this).is(':checked') == true){
            location_rang_div.find("#location_time_range").show();
            location_rang_div.find("#location_single_time").hide();
            location_rang_div.find('input[name=location_hour[]]').attr('required','');
            location_rang_div.find('input[name=location_hour_from[]]').attr('required','required');
            location_rang_div.find('input[name=location_hour_to[]]').attr('required','required');
        }else{
            location_rang_div.find("#location_time_range").hide();
            location_rang_div.find("#location_single_time").show();
            location_rang_div.find('input[name=location_hour[]]').attr('required','required');
            location_rang_div.find('input[name=location_hour_from[]]').attr('required','');
            location_rang_div.find('input[name=location_hour_to[]]').attr('required','');
        }
    });

    $('#basic-location-timepicker1').flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
    });
    $('#basic-location-timepicker2').flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
    });
    $('#basic-location-timepicker3').flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
    });

    $(document).ready(function()
        {
            $("#continuebtn").click(function()
            {
                let pickUpvalue = document.getElementById('pickupAddress');
                let deliveryvalue = document.getElementById('deliveryAddress');
                var type = true;

                if(pickUpvalue.value === '0'){
                    document.getElementById('errorShowPickup').style.display = 'block';
                    type = false;
                }
                else{
                    document.getElementById('errorShowPickup').style.display = 'none';
                    type = false;
                }


                if(deliveryvalue.value === '0'){
                    document.getElementById('errorShowDelivery').style.display = 'block';
                    type = false;
                }
                else{
                    document.getElementById('errorShowDelivery').style.display = 'none';
                    type = false;
                }

                let stime = document.getElementById('basic-pickup-timepicker3');
                let etime = document.getElementById('basic-pickup-timepicker6');
        });

        $(document).on('submit','#address_form',function(){
          // var form_data = new FormData(this);
          var form = $(this);
          $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
              type: "POST",
              url: '{{ route('shipper.shipment.address.submit') }}',
              data: form.serialize(), // <--- THIS IS THE CHANGE
              dataType: "html",
              success: function(data){
                var response = JSON.parse(data);
                if(response.status == false){
                  $.each(response.messages, function(key,val){
                    if(val != ''){
                      $('#'+key+'_error').html(val);
                    }else{
                      $('#'+key+'_error').html('');
                    }
                  })
                }else{
                  get_facility_address();
                  $('#address_form').trigger('reset');
                  $('#country').val('').trigger('change');
                  $('#state').val('').trigger('change');
                  $('#city').val('').trigger('change');
                  $('.cancel-btn').click();
                }
              },
              error: function() { alert("Error posting feed."); }
            });
        });
    });

    
</script>
<script type="text/javascript">
  // 
              

</script>

<script type="text/javascript">
  function calculation(val) {
    // console.log(alert("testing"));
    var new_value = val;
    var old_val = parseInt(val-1);
    var total_products = $('#location'+old_val).find('.product_checkbox_div .location1products').length;
    if(total_products != "" || total_products != 0){
      for (i=1; i<=total_products; i++) {
        var actual_qty = $("input[name='quantity_1["+i+"]']").val();
        var actual_weight = $("input[name='weight_1["+i+"]']").val();
        var old_weight = $("input[name='weight_"+old_val+"["+i+"]']").val();
        var old_shipment = $("input[name='pickup_type"+old_val+"["+i+"]']").val();
        var new_shipment = $("input[name='pickup_type"+new_val+"["+i+"]']").val();
        var old_qty = $("input[name='quantity_"+old_val+"["+i+"]']").val();
        if(new_shipment == "pickup"){
          // if(old_val == 1){
          //   var new_weight = parseInt(actual_weight) - parseInt(old_weight);
          //   var new_qty = parseInt(actual_qty) - parseInt(actual_qty);
          //   if(new_qty == 0 || new_weight == 0){
          //     $("input[name='location1products"+new_key+"["+i+"]']").attr('disabled',true)
          //   }else{
          //     $("input[name='location1products"+new_key+"["+i+"]']").attr('disabled',false)
          //     $("input[name='weight_"+new_key+"["+i+"]']").val(new_weight);
          //     $("input[name='quantity_"+new_key+"["+i+"]']").val(new_qty);
          //   }
          // }
        }
        console.log(total_products);
      }
    }
    
    
     
}
</script>
<script>

    let i=2;

     function clone()
     {
            var original = document.getElementById('location_hidden');
            //  console.log('i',i);
            var clone = original.cloneNode(true);
            let deleteElement=clone.getElementsByTagName('a')[4];
             var original = document.getElementById('location1' );
             
             clone.id = "location"+ i  ;
             
             let locationLabelElement=clone.getElementsByTagName('span')[0];
             let addressSelectElement=clone.getElementsByTagName('select')[0];
             let deliveryDateInputElement=clone.getElementsByTagName('input')[0];
             let rangeInputElement=clone.getElementsByTagName('input')[1];
             let hourInputElement=clone.getElementsByTagName('input')[2];
             let locationNoteElement=clone.getElementsByTagName('textarea')[0];
             let updateNoteElement=clone.getElementsByTagName('a')[2];
             let clearNoteElement=clone.getElementsByTagName('a')[3];
             let pickUpInputElement=clone.getElementsByTagName('input')[7];

             let addElement = document.getElementById('addLocation');

             addressSelectElement.name='address_'+i;
              addressSelectElement.id = 'errorShowPickup'+i;
             locationLabelElement.textContent=' '+i+' {{ __('Details')}}';
             deliveryDateInputElement.text='errorShowPickup'+i;
              deliveryDateInputElement.text='datetimepicker5'+i;
             deliveryDateInputElement.id='delivery_date_'+i;
             rangeInputElement.name='location'+i+'_time_range_check';
             rangeInputElement.id='location'+i+'_time_range_check';
             locationNoteElement.name='location'+i+'Note';
             locationNoteElement.id='location'+i+'Note';
             // quantityInputElement.name='quantity_'+i;
             pickUpInputElement.name='delivery_type'+i;
             
             // deliveryInputElement.name='delivery_type'+i;
             // weightInputElement.name='weight_'+i;
             // unitSelectElement.name='unit_'+i;

             updateNoteElement.id = "changelocation"+ i;
             updateNoteElement.onclick = '';
             clearNoteElement.id = "clearlocation"+ i;
             clearNoteElement.onclick = '';
            //clone.find('input[name="delivery_date1"]').flatpickr({minDate: "today"});
            // console.log(clone);
             deleteElement.id = "locationDelete"+ i;
             deleteElement.style.display='block';
             deleteElement.style.float='right';
            //  console.log(clone);
             // document.getElementById('extra_products').parentNode.appendChild(clone);
             document.getElementById('extra_locations').appendChild(clone);
             $("#location"+i).find('input[name="delivery_date1"]').flatpickr({minDate: "today"});
             $("#location"+i).find('input[name="location_hour_from[]"]').flatpickr({enableTime: true,noCalendar: true,dateFormat: "H:i",});
             $("#location"+i).find('input[name="location_hour_to[]"]').flatpickr({enableTime: true,noCalendar: true,dateFormat: "H:i",});
             $("#location"+i).find('input[name="location_hour[]"]').flatpickr({enableTime: true,noCalendar: true,dateFormat: "H:i",});

             let newI=i;
             i+=1;
             let productCount = <?php echo count($products); ?>;
             if(i>productCount){
                 addElement.setAttribute('style', 'display:none !important');
             }
            //  console.log(productCount);
             if(i>5){
                 addElement.setAttribute('style', 'display:none !important');
             }
             document.getElementById('locationDelete'+newI).addEventListener('click',function(){
                console.log('delete product ',newI);
                document.getElementById("location"+ newI).remove();
               i-=1;
               if(newI<=5){
                   addElement.setAttribute('style', 'display:inline !important;border-radius: 16px; width: 20%;padding-left: 6%;padding-right : 6%;');
               }
             });
             document.getElementById('changelocation'+newI).addEventListener('click',function(){
                 document.getElementById('location'+newI+'Note').readOnly =false;
             });
             document.getElementById('clearlocation'+newI).addEventListener('click',function(){
                 document.getElementById('location'+newI+'Note').value = "";
                 document.getElementById('location'+newI+'Note').readOnly =false;
             });

             document.getElementById('location'+newI+'_time_range_check').addEventListener('click',function(){
                //  console.log(this.checked);
                 var time_range = document.getElementById('location'+newI+'_time_range');
                 var single_time = document.getElementById('location'+newI+'_single_time');
                 if(this.checked == true){
                    time_range.style.display = "flex";
                    single_time.style.display = "none";
                 }else{
                    time_range.style.display = "none";
                    single_time.style.display = "flex";
                 }
             });
              document.getElementById('show_Delivery2').addEventListener('click', function(evt) {
                  var target = evt.target;
                  // console.log(alert("hey"));
                    if (target.id === 'delivery_type_2') {
                        $('quantity' - 'changelocation'+newI)
                      }
                    
              }, false);       
                  onChange() 
            calculation()
            get_facility_address()
     }
 </script>

<script>
$(".show_button").click(function(){$("#text_id").toggle()})
</script>
    <script type="text/javascript">
    //Pickup functions
    function showPickUpRange() {
        var checkBox = document.getElementById("pickup_time_range_check");

        var time_range = document.getElementById("pickup_time_range");
        var single_time = document.getElementById("pickup_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
            $('input[name=pickup_hour]').attr('required','');
            $('input[name=pickup_hour_from]').attr('required','required');
            $('input[name=pickup_hour_to]').attr('required','required');
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
            $('input[name=pickup_hour]').attr('required','required');
            $('input[name=pickup_hour_from]').attr('required','');
            $('input[name=pickup_hour_to]').attr('required','');
        }
    }

    function changePickUpNote() {
        document.getElementById('pickUpNote').readOnly =false;
    }

    function clearPickUpNote() {
        document.getElementById('pickUpNote').value = "";
        document.getElementById('pickUpNote').readOnly =false;
    }

    //Delivery functions
    function showDeliveryRange() {
        var checkBox = document.getElementById("delivery_time_range_check");

        var time_range = document.getElementById("delivery_time_range");
        var single_time = document.getElementById("delivery_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
            $('input[name=delivery_hour]').attr('required','');
            $('input[name=delivery_hour_from]').attr('required','required');
            $('input[name=delivery_hour_to]').attr('required','');
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
            $('input[name=delivery_hour]').attr('required','required');
            $('input[name=delivery_hour_from]').attr('required','');
            $('input[name=delivery_hour_to]').attr('required','');
        }
    }

    function changeDeliveryNote() {
        document.getElementById('deliveryNote').readOnly =false;
    }

    function clearDeliveryNote() {
        document.getElementById('deliveryNote').value = "";
        document.getElementById('deliveryNote').readOnly =false;
    }

    // Multiple Location page functions

    
    // $(document).on("change","#pickup_time_range_check",function(){
    //     var pickup_rang_div = $(this).closest(".pickup_rang_div");
    //     if($(this).is(':checked') == true){
    //         pickup_rang_div.find("#pickup_time_range").show();
    //         pickup_rang_div.find("#pickup_single_time").hide();
    //         pickup_rang_div.find('input[name=pickup_hour[]]').attr('required','');
    //         pickup_rang_div.find('input[name=pickup_hour_from[]]').attr('required','required');
    //         pickup_rang_div.find('input[name=pickup_hour_to[]]').attr('required','required');

    //     }else{
    //         pickup_rang_div.find("#pickup_time_range").hide();
    //         pickup_rang_div.find("#pickup_single_time").show();
    //         pickup_rang_div.find('input[name=pickup_hour[]]').attr('required','required');
    //         pickup_rang_div.find('input[name=pickup_hour_from[]]').attr('required','');
    //         pickup_rang_div.find('input[name=pickup_hour_to[]]').attr('required','');
    //     }

    // });
    </script>

    <script>
    function Stops(evt, stops,color) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        // tablinks[i].className = tablinks[i].className.replace(" active", "");
        tablinks[i].className = tablinks[i].className.replace(" active", "");
        tablinks[i].style.backgroundColor = "";
      }
      document.getElementById(stops).style.display = "block";
      // evt.currentTarget.className += " active";
      // elmnt.style.backgroundColor = color;
    }
</script>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
<!-- {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCjorOISx346EPRCKse9h8K_fZmNnWZ00&libraries=places&callback=initAutocomplete" async defer></script> --}} -->
<script type="text/javascript">
      var placeSearch;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            console.log(pickup_place);
            var address = pickup_place.address_components;
            var street = city = state = pincode = '';
            $.each(address, function(i,val){
              console.log(val);
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
                if($.inArray('postal_code', val['types']) > -1) {
                    pincode += val['long_name'];
                }
            });
            $('#latitude').val(pickup_place.geometry.location.lat());
            $('#longitude').val(pickup_place.geometry.location.lng());
            $('#post_code').val(pincode);
            changeMapLocation(pickup_place.geometry.location.lat(),pickup_place.geometry.location.lng())
        });
    });

      function changeMapLocation(latitude=0,longitude=0){
        (function initMap() {
          var position = {lat: latitude, lng: longitude};
          var myLatlng = new google.maps.LatLng(latitude,longitude);
          var geocoder = new google.maps.Geocoder();
          var map = new google.maps.Map(document.getElementById("map"), {
              zoom: 19,
              center: position,
              width: 20,
            height: 50,
          });
          var marker = new google.maps.Marker({
              position: position,
              map: map,
              draggable: true
          });

          google.maps.event.addListener(marker, 'dragend', function() {
              map.panTo(marker.getPosition()); 
              geocoder.geocode({'latLng': marker.getPosition() }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  if (results[0]) {
                    var address_components = results[0].address_components;
                    var components={};
                    jQuery.each(address_components, function(k,v1) {jQuery.each(v1.types, function(k2, v2){components[v2]=v1.long_name});});
                    var postal_code;
                    if(components.postal_code) {
                      postal_code = components.postal_code;
                    }
                  $('#autocomplete').val(results[0].formatted_address);
                  $('#latitude').val(marker.getPosition().lat());
                  $('#longitude').val(marker.getPosition().lng());
                  $('#post_code').val(postal_code);
                  }
                }
              });
          });

          // google.maps.event.addListener(marker, 'click', function() {
          //   if (marker.formatted_address) {
          //     console.log(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
          //     infowindow.setContent(marker.formatted_address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
          //   } else {
          //     infowindow.setContent(address + "<br>coordinates: " + marker.getPosition().toUrlValue(6));
          //   }
          //   infowindow.open(map, marker);
          // });
        })();
      }
     
</script>

@endsection
