@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
	.about-section {
  padding: 50px;
  text-align: center;
  color: white;
  font-family: Arial, Helvetica, sans-serif;
  font-family: Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif;
    font-weight: 600;
      font-size: 17px;
}
.about-section h1{
  color: white;
}
.btn-outline-deline {
  background-color: white; 
  color: #9B51E0; 
  border: 1px solid #9B51E0;
}

.btn-outline-deline:hover {
  background-color: #9B51E0;
  color: white;
}
</style>
<body>
<div class="container">
  <div class="about-section">
  	<img src="{{ URL::asset('shipper/images/Group_56281.png') }}" style="width: 33%;
    padding-right: 63px;">
    <h1 class="txt-purple">Successful<span class="txt-black"> bidding</span></h1>
    <h2>Carrier Will Confirm in some time</h2>
    <p class="txt-black">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
  </div>
  <div class="row">
        <div class="col-md-6">
  			<a href="{{ route('shipper.dashboard') }}"><button class="btn btn-outline-deline" style="border-radius: 11px;width: 70%;margin-left: 69%;border-radius: 24px;height: 54px;font-weight: bold;">Go To Dashboard</button></a>
  		</div>
  </div>
  <div class="row">
  		<div class="col-md-6">
  			<a href="{{ route('shipper.shipment.create') }}"><button class="btn btn-outline-deline" style="border-radius: 11px;width: 70%;margin-left: 69%;border-radius: 24px;height: 54px;font-weight: bold;margin-top: 23px;">Create New Shipment</button></a>
  		</div>
  </div>
</div>
@endsection