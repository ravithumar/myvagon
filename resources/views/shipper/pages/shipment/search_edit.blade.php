@if(!empty(Session::get('search_available_truck_detail')))
<?php $session_data = Session::get('search_available_truck_detail'); ?>
@endif
@extends('shipper.layouts.master')

@section('content')
<style>
  
</style>
<div class="container-fluid">
 
  <div class="row align-items-center">
    <div class="col-md-12">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript:void(0);" class="d-flex align-items-center"><i class="fas fa-shipping-fast pr-3"></i>{{__('Create Shipment')}}</a></li>
          @if(!isset($session_data['driver_detail']) && empty($session_data['driver_detail']))
          <li class="breadcrumb-item" aria-current="page"><a href="{{ url('shipper/shipment/edit/'.$booking['id'].'?quote=1') }}">{{__('Truck Type')}}</a></li>
          @endif
          <li class="breadcrumb-item" aria-current="page"><a href="{{ url('shipper/shipment/step2/edit/'.$booking['id'].'?quote=1') }}">{{__('Product Information')}}</a></li>
          <li class="breadcrumb-item" aria-current="page"><a href="{{ url('shipper/shipment/step3/edit/'.$booking['id'].'?quote=1') }}">{{__('Pickup & Delivery Information')}}</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="javascript:void(0);">{{__('Insert Your Quote')}}</a></li>
        </ol>
      </nav>
    </div>
  </div>
  <div class="row">
  	<div class="col-md-4">
  		<h4 class="text-dark mb-0">{{__('Insert Your Quote')}}</h4>
  		<!-- <small>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</small> -->
  	</div>
  	<div class="col-md-12 my-4">
      <input type="hidden" id="longitud" value="">
      <input type="hidden" id="latitud" value="">
  		 <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;"></div>
  	</div>
  	<div class="col-md-12">
  		<h4>Review  Details</h4>
  		<div class="table-responsive">
            <table class="table mb-0">
                <thead class="thead-light">
                    <tr>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">{{__('S.No.')}}</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">{{__('Address Details')}}</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">{{__('Pickup/Delivery')}}</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">{{__('Product')}}</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">{{__('Quantity')}}</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">{{__('Weight')}}</th>
                    </tr>
                </thead>
                <tbody>
                  @if(isset($booking_locations) && !empty($booking_locations))
                    @for($i=0;$i<count($booking_locations);$i++)
                      @if($booking['booking_type'] == 'direct_shipment')
                      <tr>
                        <td scope="row" class="text-dark">{{$i+1}}</td>
                        <td class="border-left text-dark">{{ $booking_locations[$i]['drop_location'] }}<br/><span class="text-muted">{{ date('M d, Y',strtotime($booking_locations[$i]['delivered_at'])) }} @if($booking_locations[$i]['delivery_time_from'] == $booking_locations[$i]['delivery_time_to']) {{ $booking_locations[$i]['delivery_time_from'] }} @else {{ $booking_locations[$i]['delivery_time_from'] }} - {{ $booking_locations[$i]['delivery_time_to'] }} @endif</span></td>
                        <td class="border-left text-dark" >@if($booking_locations[$i]['is_pickup'] == '1') Pickup @else Delivery @endif</td>
                        <td class="text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="border-left text-dark border-top-0" style="">{{$products['product_id']['name']}}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                        <td class="align-middle text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="border-left text-dark border-top-0" style="">{{$products['qty']}} {{ $products['product_type']['name'] }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                        <td class="align-middle text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="border-left text-dark border-top-0" style="">{{$products['weight']}} {{ $products['unit']['name'] }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      @else
                      <tr>
                        <td scope="row" class="text-dark">{{$i+1}}</td>
                        <td class="border-left text-dark">{{ $booking_locations[$i]['drop_location'] }}<br/><span class="text-muted">{{ date('M d, Y',strtotime($booking_locations[$i]['delivered_at'])) }} @if($booking_locations[$i]['delivery_time_from'] == $booking_locations[$i]['delivery_time_to']) {{ $booking_locations[$i]['delivery_time_from'] }} @else {{ $booking_locations[$i]['delivery_time_from'] }} - {{ $booking_locations[$i]['delivery_time_to'] }} @endif</span></td>
                        <td class="border-left text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="text-dark border-top-0" style="">@if($products['is_pickup'] == '0') Delivery @else Pickup @endif</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                        <td class="border-left text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="text-dark border-top-0" style="">{{$products['product_id']['name']}}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                        <td class="border-left align-middle text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="text-dark border-top-0" style="">{{$products['qty']}} {{ $products['product_type']['name'] }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                        <td class="border-left align-middle text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="text-dark border-top-0" style="">{{$products['weight']}} {{ $products['unit']['name'] }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      @endif
                    @endfor
                  @endif
                </tbody>
            </table>
        </div>
  	</div>
  	<div class="col-md-12">
      <div class="long-haul-shipment"> 
       <div class="long-block">    
         <div class="popup d-flex align-items-center">
           <h4 class="txt-purple mr-2">@if(isset($booking['journey_type'])  && !empty($booking['journey_type'])) {{ $booking['journey_type'] }} @endif</h4>
           <a href="#" class="btn-tooltip position-relative d-flex align-items-center txt-blue">i
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Long haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements greater than 350 to 400km from 
              origin to destination are generally classified as
               long-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Medium haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 100km to 300km from 
              origin to destination are generally classified as
               medium-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Short haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 0km to 100km from 
              origin to destination are generally classified as
               short-haul movements</span></a>
            @endif
         </div>            
         <p class="mb-1 font-16 txt-blue font-weight-light">{{__('Total Kms')}}.: <em class="font-weight-bold">@if(isset($booking['distance'])  && !empty($booking['distance'])) {{ $booking['distance'] }} @endif</em></p>
         <p class="mb-1 font-16 txt-blue font-weight-light">Journey: <em class="font-weight-bold">@if(isset($booking['journey'])  && !empty($booking['journey'])) {{ $booking['journey'] }} @endif</em></p>
       </div>
       
        <div class="row mt-4">
          <div class="col-lg-5">
            @if(!isset($session_data['driver_detail']) && empty($session_data['driver_detail']))
            <form action="javascript:void(0)" id="send_track_id_form" method="POST">
              @csrf
              <div class="form-group d-flex">
                <div class="label-wrapper">
                  <label><input type="checkbox" id="share_track_link" class="largerCheckbox" name="share_track_link" onclick="validateInput1(this)" value="1" {{ $booking['is_tracking'] == '1' ? 'checked' : '' }}><span></span></label>
                </div>
                <div class="text-wrapper d-flex pl-1">
                  <div class="wrap-both">
                     
                    <p class="mb-0 font-14 txt-blue">{{__('Send Tracking Shipment link to Delivery Point')}}</p>
                    <!-- <small class="text-muted">Check this option for shipment broadcasting to carrier/freelance driver or bidding option</small> -->
                  </div>
                </div>              
              </div>
              <input type="hidden" value="{{ $booking['id'] }}" name="booking_id">
              <div class="form-group position-relative mb-3 share_track_email" style="display: none;">
                <input type="text" name="email" class="border-bottom form-control custom-input call-icon position-relative pl-4 font500" placeholder="Email" value="{{$booking['email_or_number']}}" required>
                <div class="icon position-absolute" style="top:0; margin: 7px;"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                <small class="text-muted" style="margin-left: 25px;">{{__('Link would be sent once shipment is created')}}</small><br>
                <span class="text-danger email_phone_error"></span>
              </div>
              <div class="form-group position-relative mb-3 share_track_button" style="display: none;">
                <button type="submit" class="btn btn-blue btn-rounded send-track-link" style="margin-left:0px"><span class="text ml-1 font-14">{{__('Submit')}}</span></button>
              </div>
            </form>
            <form action="{{ url('shipper/shipment/search/public/'.$booking['id']) }}" method="post" class="my-3">
            @csrf
            <div class="form-group d-flex">
              <div class="label-wrapper">
                <label><input type="checkbox" id="public_shipment" class="largerCheckbox" name="public_shipment" onclick="validateInput(this)" value="4" data-parsley-multiple="truck_type" @if(isset($booking['is_public']) && !empty($booking['is_public']) && $booking['is_public'] == '1') checked @endif><span></span></label>
              </div>
              <div class="text-wrapper d-flex pl-1">
                <div class="wrap-both">
                  <p class="mb-0 font-14 txt-blue">{{__('Public Shipment')}}</p>
                  <small class="text-muted">Check this option for shipment broadcasting to carrier/freelance driver or bidding option</small>
                </div>
              </div>              
            </div>
            <div class="form-group mb-3 allow_bidding_div">
              <div class="allow-bidding d-flex align-items-center">
                <div class="text font-14 font600 txt-blue mr-3">{{__('Allow Bidding')}} :</div>
                <label class="switch mb-0">
                  <input type="checkbox" name="all_bidding" @if(isset($booking['is_bid']) && !empty($booking['is_bid']) && $booking['is_bid'] == '1') checked @endif>
                  <span class="slider round"></span>
                </label>
              </div>
            </div> 
            <div class="form-group allow_bidding_1_div" >
              <div class="price_div">
                <strong class="font-24 txt-blue">€</strong>            
                <textarea class="form-control border-r13 font-24 font500" name="amount" id="exampleFormControlTextarea1" rows="1" placeholder="Update Quote" data-parsley-pattern="[0-9]+[.,]?[0-9]*" required>{{ $booking['amount'] }}</textarea>
              </div>    
            </div>  
            <div class="form-group allow_bidding_2_div" >
              <button type="submit" class="btn btn-blue btn-rounded"><span class="text ml-1 font-14">{{__('Update Shipment')}}</span></button>
            </div>          
            </form>
            @endif
          </div>
          <div class="col-lg-7"></div>
        </div>
      </div>

      @if(isset($session_data) && !empty($session_data))
            @if(isset($session_data['driver_detail']) && !empty($session_data['driver_detail']))
              <div class="row mt-2 mb-2">
                <div class="col-md-12">
                  <div class="truck-box bg-white p-2">
                    <div class="row align-items-center">
                      <div class="col-md-1 tr-br">
                        <p class="mb-0 font-14 txt-blue font-weight-light"><em class="font-weight-bold">Available Truck</em><br> for booking  </p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('Truck Type')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['driver_vehicle_detail']['truck_type']['name']) && !empty($session_data['driver_vehicle_detail']['truck_type']['name'])){{ $session_data['driver_vehicle_detail']['truck_type']['name'] }}@endif</p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('Start Date')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['date']) && !empty($session_data['availability_data']['date'])){{ date('F d, Y',strtotime($session_data['availability_data']['date'])) }}@endif</p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('Start Location')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['from_address']) && !empty($session_data['availability_data']['from_address'])) {{ (strlen($session_data['availability_data']['from_address']) > 35 ? substr($session_data['availability_data']['from_address'],0,35).'...' : $session_data['availability_data']['from_address']) }} @endif</p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('End Location')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['availability_data']['to_address']) && !empty($session_data['availability_data']['to_address'])) {{ (strlen($session_data['availability_data']['to_address']) > 35 ? substr($session_data['availability_data']['to_address'],0,35).'...' : $session_data['availability_data']['to_address']) }} @endif</p>
                      </div>
                      <div class="col-md-2">
                        <p class="txt-purple mb-0">{{__('Carrier Info')}}</p><br>
                        {{-- <p class="mb-0 font-14 txt-blue font-weight-light"><a href="javascript:void(0)">Test Driver</a></p> --}}
                        <p class="mb-0 font-14 txt-blue font-weight-light">@if(isset($session_data['driver_detail']['name']) && !empty($session_data['driver_detail']['name'])) {{ $session_data['driver_detail']['name'] }} @endif</p>
                      </div>
                      <div class="col-md-1">
                        <p class="txt-purple mb-0">{{__('Bid Price')}}</p><br>
                        <p class="mb-0 font-14 txt-blue font-weight-light">€ @if(isset($session_data['availability_data']['bid_amount']) && !empty($session_data['availability_data']['bid_amount'])) {{ CommonHelper::price_format($session_data['availability_data']['bid_amount'],'GR') }} @else {{ CommonHelper::price_format('0','GR') }} @endif</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row long-haul-shipment">
                <div class="col-md-12">
                <form action="{{ url('shipper/shipment/search/public/'.$booking['id']) }}" method="post" class="my-3">
                  @csrf
                  <div class="form-group d-flex">
                    <!-- <div class="label-wrapper">
                      <label><input type="checkbox" id="public_shipment" class="largerCheckbox" name="public_shipment" onclick="validateInput(this)" value="4" data-parsley-multiple="truck_type"><span></span></label>
                    </div> -->
                    <!-- <div class="text-wrapper d-flex pl-1">
                      <div class="wrap-both">
                        <p class="mb-0 font-14 txt-blue">Public Shipment</p>
                        <small class="text-muted">Check this option for shipment broadcasting to carrier/freelance driver or bidding option</small>
                      </div>
                    </div>               -->
                  </div>
                  <div class="form-group mb-3 allow_bidding_div" style="display: block">
                    <div class="allow-bidding d-flex align-items-center">
                      <div class="text font-14 font600 txt-blue mr-3">@if(isset($session_data['availability_data']['is_bid']) && !empty($session_data['availability_data']['is_bid']) && $session_data['availability_data']['is_bid'] == '1') Bid Starting Price : @if(isset($session_data['availability_data']['bid_amount']) && !empty($session_data['availability_data']['bid_amount'])) {{CommonHelper::price_format($session_data['availability_data']['bid_amount'],'GR')}} @else {{ CommonHelper::price_format('0','GR') }} @endif @else Booking Price : @endif</div>
                      <!-- <label class="switch mb-0"> -->
                        <input type="hidden" name="all_bidding" value="@if(isset($session_data['availability_data']['is_bid']) && !empty($session_data['availability_data']['is_bid']) && $session_data['availability_data']['is_bid'] == '1') on @endif">
                        <!-- <span class="slider round"></span> -->
                      <!-- </label> -->
                    </div>
                  </div> 
                  <div class="form-group allow_bidding_1_div" style="display: block;">
                    <div class="price_div">
                      <strong class="font-24 txt-blue">€</strong>            
                      <textarea class="form-control border-r13 font-24 font500" name="amount" id="exampleFormControlTextarea1" rows="1" placeholder="Insert Quote" data-parsley-pattern="[0-9]+[.,]?[0-9]*" @if(isset($session_data['availability_data']['is_bid']) && !empty($session_data['availability_data']['is_bid']) && $session_data['availability_data']['is_bid'] == '0') readonly @endif required data-parsley-required-message="Please enter price">@if(isset($session_data['availability_data']['bid_amount']) && !empty($session_data['availability_data']['bid_amount'])){{trim($session_data['availability_data']['bid_amount'])}}@else 0 @endif</textarea>
                    </div>    
                  </div>  
                  <div class="form-group allow_bidding_2_div" style="display: block">
                    <button type="submit" class="btn btn-blue btn-rounded"><span class="text ml-1 font-14">@if(isset($session_data['availability_data']['is_bid']) && !empty($session_data['availability_data']['is_bid']) && $session_data['availability_data']['is_bid'] == '1') Place Bid @else Book @endif </span></button>
                  </div>          
                </form>
                </div>
              </div>
            @endif
          @endif
          @if(!isset($session_data['driver_detail']) && empty($session_data['driver_detail']))
      <div class="col-md-3 available_btn" @if(isset($booking['is_public']) && !empty($booking['is_public']) && $booking['is_public'] == '1') style="display: none" @else style="display: block" @endif>
        @if(isset($manage_price) && !empty($manage_price))
          <a href="#search_available_truck" class="btn btn-blue btn-rounded btn-block search_available">
            <span class="text ml-1">{{__('Search Available Trucks')}}</span>
          </a>
        @endif
      </div>
      @endif
      <div class="row mt-5" id="search_available_truck" style="display: none;">
        <div class="col-xl-12">
          <div class="card ">
            <div class="card-body table-responsive" >
              <table id="{{$dataTableId}}" class="table table-bordered table-striped" style="width: 100%">
                <thead>
                  <tr>
                    @foreach ($dateTableFields as $field)
                    <th>
                      {{ $field['title'] }}
                    </th>
                    @endforeach
                  </tr>
                </thead>
                <tbody>
                  @if(isset($available_trucks) && !empty($available_trucks))
                  @foreach($available_trucks as $trucks)
                    <?php 
                    $first_key = array_key_first($trucks['booking']['trucks']['locations']);
                    $last_key = $last_key = array_key_last($trucks['booking']['trucks']['locations']);?>
                    <tr>
                      <td>{{ $trucks['id'] }}</td>
                      <td>{{ $trucks['trucks_type']['name'] }}</td>
                      <td>{{ date('F d, Y',strtotime($trucks['booking']['pickup_date'])) }}</td>
                      <td title="{{ $trucks['booking']['trucks']['locations'][$first_key]['drop_location'] }}">{{ (strlen($trucks['booking']['trucks']['locations'][$first_key]['drop_location']) > 35 ? substr($trucks['booking']['trucks']['locations'][$first_key]['drop_location'],0,35).'...' : $trucks['booking']['trucks']['locations'][$first_key]['drop_location']) }}</td>
                      <td title="{{ $trucks['booking']['trucks']['locations'][$last_key]['drop_location'] }}">{{ (strlen($trucks['booking']['trucks']['locations'][$last_key]['drop_location']) > 35 ? substr($trucks['booking']['trucks']['locations'][$last_key]['drop_location'],0,35).'...' : $trucks['booking']['trucks']['locations'][$last_key]['drop_location']) }}</td>
                      <td>{{ $trucks['driver_name'] }}</td>
                      <td>€ {{ CommonHelper::price_format($trucks['bid_amount'],'GR') }}</td>
                      <td>@if($trucks['is_bid'] == '1') <a @if($trucks['booking']['is_bid'] == '0') href="{{ url('shipper/search-available-trucks/book-now/'.$trucks['booking']['id'].'/'.$trucks['id']) }}" @else href="{{ url('shipper/search-available-trucks/bid-now/'.$trucks['booking']['id'].'/'.$trucks['id']) }}" @endif style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> Bid</a> @else <a href="{{ url('shipper/search-available-trucks/book-now/'.$trucks['booking']['id'].'/'.$trucks['id']) }}" style="color: #b27be7;font-weight:600;border: 1px solid #b27be7;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> Book</a> @endif</td>
                    </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  	</div>
  </div>  
</div>
@endsection
@section('script');
  <link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
  <script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
  <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script>
  $(document).ready(function () {
    var table = $('#{{$dataTableId}}').DataTable({
      processing: true,
      responsive:true,
      searching:true,
      lengthChange:true,
      bInfo:true,
    });
    
    $(document).on('click','.search_available',function() {
      $('#search_available_truck').show();
    })
  })
</script>
<script type="text/javascript">
validateInput1();
var map;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var locations = <?=json_encode($location_aray)?>;
function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(<?=$booking['pickup_lat']?>,<?=$booking['pickup_lng']?>),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

 directionsDisplay.setMap(map);

 directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
    }
    else
        alert ('failed to get directions');
 });
}
google.maps.event.addDomListener(window, "load", initialize);

function validateInput(e)
{
  // if($('#public_shipment').is(":checked")){
  //   $('.available_btn').hide();
  //   $('.allow_bidding_2_div').show();
  //   $('.allow_bidding_1_div').show();
  //   $('.allow_bidding_div').show();
  // }else{
  //   $('.available_btn').show();
  //   $('.allow_bidding_2_div').hide();
  //   $('.allow_bidding_1_div').hide();
  //   $('.allow_bidding_div').hide();
  // }
}

// function restrict_amount(tis) {
//   var prev = tis.getAttribute("data-prev");
//   prev = (prev != '') ? prev : '';
//   if (Math.round(tis.value*100)/100!=tis.value)
//   tis.value=prev;
//   tis.setAttribute("data-prev",tis.value)
// }

function restrict_amount(el){
 var ex = /^[0-9]+\.?[0-9]*$/;
 if(ex.test(el.value)==false){
   el.value = el.value.substring(0,el.value.length - 1);
  }
}

function validateInput1(e)
{
  if($('#share_track_link').is(":checked")){
    $('.share_track_email').show();
    $('.share_track_button').show();
  }else{
    $('.share_track_email').hide();
    $('.share_track_button').hide();
  }
}

$(document).on('submit','#send_track_id_form',function(){
  var email = $('input[name=email]').val();
  var mailFormat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (!mailFormat.test(email)) {
      $('.email_phone_error').text('Please enter valid email.');
      return false;
  }else{
    $('.email_phone_error').text('');
  }

  var form = $(this);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $.ajax({
    type: "POST",
    url: "{{ url('shipper/shipment/track-shipment') }}",
    data: form.serialize(), // <--- THIS IS THE CHANGE
    dataType: "html",
    success: function(data){
      var response = JSON.parse(data);
      if(response.status == false){
        $.each(response.messages, function(key,val){
          if(val != ''){
            $('#'+key+'_error').html(val);
          }else{
            $('#'+key+'_error').html('');
          }
        })
      }else{
        get_products();
        // get_facility_address();
        $('#add_new_product').trigger('reset');
        // $('#country').val('').trigger('change');
        // $('#state').val('').trigger('change');
        // $('#city').val('').trigger('change');
        $('.add-product-cancel').click();
      }
    },
    error: function() { alert("Error posting feed."); }
  }); 
});

</script>
@endsection