@extends('shipper.layouts.master')
@section('content')
<style>
  .select2-container--default .select2-selection--single{
    background-color: #f3eff7;
    border: 0px solid #aaa;
    border-radius: 0px;
    border-bottom: 1px solid #202042;
    border-radius: inherit;
    height: calc(1.5em + .9rem + 2px);
  }
</style>
   {{-- <div class="content-page create-shipment-page">
   	  <div class="content"> --}}
   	  	 <div class="container-fluid">
   	  	 	<div class="row align-items-center">
   	  	 		<div class="col">
   	  	 			  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0">
                       <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">Create Shipment</a></li>
                       <li class="breadcrumb-item active txt-blue" aria-current="page">Truck Type</li>
                       <li class="breadcrumb-item active txt-blue" aria-current="page">Product</li>
                    </ol>
                </nav>
   	  	 		</div>
   	  	 	</div>
          <div class="row d-flex align-items-center products">
             <div class="col-md-8 col-lg-6">
                <h3>Product Information</h3>
                <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p>
             </div>
             <div class="col-md-4 col-lg-5"></div>
          </div>
          <form action="{{ route('shipper.shipment.create.step3') }}" method="POST" name="product_form">
            @csrf 
          <div class="row">
             <div class="col">
              <a href="javascript:;" class="add-more-one mt-3 d-flex add_field_button"  id="addProduct" data-target="#add-new-product" data-toggle="modal">
                <img src="http://13.36.112.48/assets/images/shipment/plus-black.svg" alt="Plus" class="img-fluid">
                <span class="text ml-1 text-decoration-none">Add New Products</span>
              </a>
                <div class="pro-info mt-4 input_fields_wrap">
                    <div class="pro-content mt-4 mb-4 product_info">
                      <div class="row pl-4 products" style="background-color:#f3eff7; margin-top: 2%;" id="product1">
                        <div class="col">
                          <div class="row mt-3 mt-md-4 mt-lg-5 main_product_div">
                            <div class="col-sm-4 col-md-6 col-lg-3">
                               <div class="form-group mb-4">
                                
                                 <select class="form-control form-control-without-border product_name my-select" name="product_id_1" id="product_id_1" style="background: none;" data-parsley-required-message="Please enter Product Name." required>
                                    <option disabled value="" data-parsley-required-message="Please enter Product Name."selected>Product Name</option>
                                    @foreach ($productData as $product)
                                        <option value="{{ $product['id'] }}" data-parsley-required-message="Please enter Product Name.">{{ $product['name'] }}</option>
                                    @endforeach
                                 </select>
                               </div>
                              
                            </div>
                            <div class="col-sm-4 col-md-6 col-lg-3">
                              <div class="form-group mb-4">
                                <select class="form-control form-control-without-border product_sku" style="background: none;" id="sku_no_1" name="sku_no_1" required>
                                    <option value="" disabled selected>SKU #</option>
                                    @foreach ($productData as $product)
                                        <option data-id="{{ $product['id'] }}" value="{{ $product['id'] }}">{{ $product['sku_no'] }}</option>
                                    @endforeach
                                 </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-6 col-lg-3">
                              <div class="form-group mb-4 d-flex">
                                <select class="form-control form-control-without-border product_sku_type pb-1" style="background: none;" id="sku_type_1" name="sku_type" disabled required>
                                    <option  disabled selected>SKU Type</option>
                                    @foreach ($productData as $product)
                                        <option value="{{ $product['id'] }}">{{ $product['sku_type'] }}</option>
                                    @endforeach
                                 </select>
                               </div>
                            </div>
                            <div class="d-sm-none col-lg-3 d-lg-block"></div>
                        </div>
                        <div class="row mt-3 mt-md-4 mt-lg-5">
                              <div class="col-sm-4 col-md-6 col-lg-3">
                                 <div class="form-group mb-4">
                                   {{-- <input type="text" id="package_type" name="package_type" class="form-control font-18 form-control-without-border pb-2"  style="background: none;" placeholder="Package Type"> --}}
                                   <select class="form-control form-control-without-border package_type pb-1" style="background: none;" name="package_1" required>
                                    <option  disabled selected>Package Type</option>
                                    @foreach ($packageData as $package)
                                        <option value="{{ $package['id'] }}">{{ $package['name'] }}</option>
                                    @endforeach
                                   </select>
                                 </div>
                              </div>
                             <div class="col-sm-4 col-md-6 col-lg-3">
                                <div class="form-group mb-4">
                                  <input type="text" id="package_quantity" name="package_quantity_1" onkeypress="return numberValidate(event)" required class="form-control form-control-without-border pb-2 package_quantity" style="background: none;" placeholder="Package Quantity">
                                </div>
                             </div>
                             <div class="">
                                <div class="form-group mb-4">
                                    <input type="text" id="weight" name="weight_1" onkeypress="return numberValidate(event)" class="form-control form-control-without-border pb-2 mr-1 weight" style="background: none; width: 97%;" placeholder="Weight" required>
                                </div>
                             </div>
                             <div class="col-sm-1 col-md-1 col-lg-1">
                                <div class="form-group mb-4">
                                    <div class="qty ml-0" style="width: 223px !important;">
                                        <select class="form-control form-control-without-border unit pb-1" style="background: none;" name="unit_1" required>
                                            <option  disabled selected>Unit</option>
                                            @foreach ($unitData as $unit)
                                                <option value="{{ $unit['id'] }}">{{ $unit['name'] }}</option>
                                            @endforeach
                                           </select>
                                    </div>
                                </div>
                            </div>
                              <div class="d-sm-none col-lg-3 d-lg-block"></div>
                          </div>
                            <div class="specs-check d-flex mt-3">
                                <div class="form-group">
                                    <label><input type="checkbox" id="Fragile" class="largerCheckbox fragile" name="fragile_1" value="1"><span></span></label>
                                    <label for="Fragile" class="txt-blue">Fragile</label>
                                 </div>
                                  <div class="form-group ml-3 ml-md-4 ml-lg-5">
                                    <label><input type="checkbox" id="sensitive" class="largerCheckbox sensitive" name="sensitive_1" value="1"><span></span></label>
                                    <label for="sensitive" class="txt-blue">Sensitive to odour</label>
                                 </div>
                            </div>
                          <div class="row">
                            <div class="col-md-7 col-lg-7 col-xl-5">
                              <div class="form-group">
                                  <textarea class="form-control product_description" id="exampleFormControlTextarea1" rows="3" placeholder="Product Description" name="product_description_1"></textarea>
                              </div>
                            </div>
                            <div class="col-md-5 col-lg-5 col-xl-7">
                               <div class="delete-this">
                                  <a style="display: none !important; font-weight:600" id="productDelete1" href="javascript:;" class="btn btn-light delete-one font-12 mt-3 d-flex align-items-right remove_product">
                                    <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                    <span class="text ml-1">Delete</span>
                                  </a>
                               </div>
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>
                    <div class="extra_products" id="extra_products"></div>
                </div>
                @if(count($productData) > 1)
                <a href="javascript:;" class="add-more-one mt-3 d-flex add_field_button" onclick='clone()' id="addProduct">
                       <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="Plus" class="img-fluid">
                       <span class="text ml-1 text-decoration-none">Add More Products</span>
                </a>
                @endif
                <div class="btn-block d-flex">
                    <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" onclick="formValidation()">Continue</button>
                </div>
          </div>
        </form>

   	  	 </div>
   	  {{-- </div>
   </div> --}}
	 <div class="clone" style="display:none">
		<div class="row pl-4 products" style="background-color:#f3eff7; margin-top: 2%;" id="product_hidden">
			<div class="col">
				<div class="row mt-3 mt-md-4 mt-lg-5 main_product_div">
					<div class="col-sm-4 col-md-6 col-lg-3">
						 <div class="form-group mb-4">
            
							 <select class="form-control form-control-without-border product_name pb-1 hidden_products" name="product_id_1" id="" style="background: none;" required>
									<option disabled selected>Product Name</option>
									@foreach ($productData as $product)
											<option value="{{ $product['id'] }}">{{ $product['name'] }}</option>
									@endforeach
							 </select>
						 </div>
					</div>
					<div class="col-sm-4 col-md-6 col-lg-3">
						<div class="form-group mb-4">
							<select class="form-control form-control-without-border product_sku pb-1" style="background: none;" id="sku_no_1" name="sku_no" required>
									<option  disabled selected>SKU #</option>
									@foreach ($productData as $product)
											<option value="{{ $product['id'] }}">{{ $product['sku_no'] }}</option>
									@endforeach
							 </select>
							</div>
					</div>
					<div class="col-sm-4 col-md-6 col-lg-3">
						<div class="form-group mb-4 d-flex">
							<select class="form-control form-control-without-border product_sku_type pb-1" style="background: none;" id="sku_type_1" name="sku_type" disabled required>
									<option  disabled selected>SKU Type</option>
									@foreach ($productData as $product)
											<option value="{{ $product['id'] }}">{{ $product['sku_type'] }}</option>
									@endforeach
							 </select>
						 </div>
					</div>
					<div class="d-sm-none col-lg-3 d-lg-block"></div>
			</div>
			<div class="row mt-3 mt-md-4 mt-lg-5">
						<div class="col-sm-4 col-md-6 col-lg-3">
							 <div class="form-group mb-4">
								 {{-- <input type="text" id="package_type" name="package_type" class="form-control font-18 form-control-without-border pb-2"  style="background: none;" placeholder="Package Type"> --}}
								 <select class="form-control form-control-without-border package_type pb-1" style="background: none;" name="package_1" required>
									<option  disabled selected>Package Type</option>
									@foreach ($packageData as $package)
											<option value="{{ $package['id'] }}">{{ $package['name'] }}</option>
									@endforeach
								 </select>
							 </div>
						</div>
					 <div class="col-sm-4 col-md-6 col-lg-3">
							<div class="form-group mb-4">
								<input type="text" id="package_quantity" name="package_quantity_1" required class="form-control form-control-without-border pb-2 package_quantity" style="background: none;" onkeypress="return numberValidate(event)" placeholder="Package Quantity">
							</div>
					 </div>
					 <div class="">
							<div class="form-group mb-4">
									<input type="text" id="weight" name="weight_1" class="form-control form-control-without-border pb-2 mr-1 weight" style="background: none; width: 97%;" placeholder="Weight" onkeypress="return numberValidate(event)" required>
							</div>
					 </div>
					 <div class="col-sm-1 col-md-1 col-lg-1">
							<div class="form-group mb-4">
									<div class="qty ml-0" style="width: 223px !important;">
											<select class="form-control form-control-without-border unit pb-1" style="background: none;" name="unit_1" required>
													<option  disabled selected>Unit</option>
													@foreach ($unitData as $unit)
															<option value="{{ $unit['id'] }}">{{ $unit['name'] }}</option>
													@endforeach
												 </select>
									</div>
							</div>
					</div>
						<div class="d-sm-none col-lg-3 d-lg-block"></div>
				</div>
					<div class="specs-check d-flex mt-3">
							<div class="form-group">
									<label><input type="checkbox" id="Fragile" class="largerCheckbox fragile" name="fragile_1" value="1"><span></span></label>
									<label for="Fragile" class="txt-blue">Fragile</label>
							 </div>
								<div class="form-group ml-3 ml-md-4 ml-lg-5">
									<label><input type="checkbox" id="sensitive" class="largerCheckbox sensitive" name="sensitive_1" value="1"><span></span></label>
									<label for="sensitive" class="txt-blue">Sensitive to odour</label>
							 </div>
					</div>
				<div class="row">
					<div class="col-md-7 col-lg-7 col-xl-5">
						<div class="form-group">
								<textarea class="form-control product_description" id="exampleFormControlTextarea1" rows="3" placeholder="Product Description" name="product_description_1"></textarea>
						</div>
					</div>
					<div class="col-md-5 col-lg-5 col-xl-7">
						 <div class="delete-this">
								<a style="display: block !important; font-weight:600;float: right" id="productDelete1" href="javascript:;" class="btn btn-light delete-one font-12 mt-3 d-flex align-items-right remove_product">
									<img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
									<span class="text ml-1">Delete</span>
								</a>
						 </div>
					</div>
				</div>
			</div>

		</div>
	 </div>

   <div class="modal fade" id="add-new-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="edits mb-2 d-flex align-items-center justify-content-between">
            <div class="title font-20 text-left">Product</div>
          </div>
          <form action="{{ route('shipper.shipment.product.submit') }}" method="POST">
            @csrf
            <div class="form-group mb-4">
              <label for="name" class="mb-2 text-grey font-14">Product Name</label>
              <input type="text" id="name" maxlength="40" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="Enter Product Name" value="{{ old('name') }}">
              @error('name')
              <div class="error">{{ $message }}</div>
              @enderror
            </div>
            <div class="form-group mb-4">
              <label for="first-name" class="mb-2 text-grey font-14">SKU Type</label>
              <input type="text" id="sku_type" name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ old('sku_type') }}" placeholder="Enter SKU Type">
              @error('sku_type')
              <div class="error">{{ $message }}</div>
              @enderror
            </div>
            <div class="btn-block d-flex">
              <button type="button" class="btn w-100 btn-white mr-2" data-dismiss="modal" aria-label="Close">Cancel</button>
              <button type="submit" class="btn w-100 btn-primary">Save</button>
            </div>
            
          </form>
          
        </div>
      </div>
    </div>
  </div>

   @endsection

   @section('script')

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


    <script>
    function addProductName(obj,val, productName, product){
        $(obj).find('.product_name').val(val);
        $(obj).find('.product_dropdown').text(productName);

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax
        ({
        type: "POST",
        url: "{{url('shipper/shipment/product/data')}}",
        data: {id: val},
        cache: false,
        success: function(data)
        {
            $(obj).find('.product_sku').val(JSON.parse(data).id);
            $(obj).find('.product_sku_type').val(JSON.parse(data).id);
        }
        });
    }

       let i=2;
       keyArray = []; 
       function change_name_id()
       {
         $('.extra_products .products').each(function(key, value){
					 var row_key = parseInt(key) + 2;

					 $(value).find('.products').attr('id','product'+row_key)

           $(value).find('.product_name').attr('name','product_id_'+row_key);
           $(value).find('.product_name').attr('id','product_id_'+row_key);

           $(value).find('.product_dropdown').attr('id','product_dropdown_'+row_key);

					 $(value).find('.product_sku').attr('name','sku_no_'+row_key);
           $(value).find('.product_sku').attr('id','sku_no_'+row_key);

					 $(value).find('.product_sku_type').attr('name','sku_type_'+row_key);
           $(value).find('.product_sku_type').attr('id','sku_type_'+row_key);

					 $(value).find('.package_type').attr('name','package_'+row_key);
           $(value).find('.package_type').attr('id','package_'+row_key);
					 
					 $(value).find('.package_quantity').attr('name','package_quantity_'+row_key);
           $(value).find('.package_quantity').attr('id','package_quantity_'+row_key);
					 
					 $(value).find('.weight').attr('name','weight_'+row_key);
           $(value).find('.weight').attr('id','weight_'+row_key);

					 $(value).find('.unit').attr('name','unit_'+row_key);
           $(value).find('.unit').attr('id','unit_'+row_key);

					 $(value).find('.product_description').attr('name','product_description_'+row_key);
					 $(value).find('.fragile').attr('name','fragile_'+row_key);
					 $(value).find('.sensitive').attr('name','sensitive_'+row_key);
								
         });
       }

       $(document).on('click','.remove_product',function(){
          $(this).closest('.products').remove();
          var row_key = $('.products').length;
          if(row_key <= 6){
              $('#addProduct').show();
          }
      });

        function clone()
        {
          var original = document.getElementById('product_hidden');
          var clone = original.cloneNode(true);
          let deleteElement=clone.getElementsByTagName('a')[0];
          let addElement = document.getElementById('addProduct');
          deleteElement.id = "productDelete"+ i;
          deleteElement.style.display='block';
          deleteElement.style.float='right';
          document.getElementById('extra_products').appendChild(clone);
          let newI=i;
          i+=1;
          if(i > 5){
              addElement.setAttribute('style', 'display:none !important');
          }

          $('.hidden_products').select2();
          // $('.select2').removeAttr('data-select2-id');
          // $('span.select2').remove();
          // $('.select2').select2({width:'100%'});
          // $('.select3').removeAttr('data-select2-id');
          // $('.select3').select2({width:'100%'});

          // $('.hidden_products').select2("destroy");
          // $('.hidden_products').select2({ //apply select2 to my element
          //     allowClear: true
          // });
          change_name_id();
        }
    </script>

    <script type="text/javascript">

        $(document).ready(function()
        {
          // $('.product_name').select2();

          $(document).on('change','.product_name',function(){
            var id = $(this).val();
            $(this).closest('.row').find('.product_sku').val(id);
            // $(this).closest('.row').find('.product_sku').select2().trigger('change');
            $(this).closest('.row').find('.product_sku_type').val(id);
          });

          $(document).on('change','.product_sku',function(){
            var id = $(this).val();
            $(this).closest('.row').find('.product_name').val(id);
            // $(this).closest('.row').find('.product_name').select2().trigger('change');
            $(this).closest('.row').find('.product_sku_type').val(id);                
          });

          // $(document).on('change','.product_sku',function(){
          //   var id = $(this).val();
          //   console.log(id);
          //   var product = $(this);
          //       // var dataString = 'id='+ id;
          //       // console.log(dataString);
          //       $.ajaxSetup({
          //             headers: {
          //               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          //             }
          //       });
          //       $.ajax
          //       ({
          //         type: "POST",
          //         url: "{{url('shipper/shipment/product/data')}}",
          //         data: {id: id},
          //         cache: false,
          //         success: function(data)
          //         {
          //             // var obj = JSON.parse(data);
          //             // console.log(obj.id);
          //             product.closest('.row').find('.product_name').val(JSON.parse(data).id);
          //             product.closest('.row').find('.product_name').select2().trigger('change');
          //             product.closest('.row').find('.product_sku_type').val(JSON.parse(data).id);
          //         }
          //       });
          // })

          // $("#product_id_1").change(function()
          // {
          //     var id=$(this).val();
          //     $.ajaxSetup({
          //           headers: {
          //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          //           }
          //     });
          //     $.ajax
          //     ({
          //     type: "POST",
          //     url: "{{url('shipper/shipment/product/data')}}",
          //     data: {id: id},
          //     cache: false,
          //     success: function(data)
          //     {
          //       document.getElementById('sku_no_1').value = JSON.parse(data).id;
          //       $('#sku_no_1').select2().trigger('change');
          //       document.getElementById('sku_type_1').value = JSON.parse(data).id;
          //     }
          //     });
          // });

          // $("#sku_no_1").change(function()
          // {
          //     var id=$(this).val();
          //     $.ajaxSetup({
          //           headers: {
          //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          //           }
          //     });
          //     $.ajax
          //     ({
          //     type: "POST",
          //     url: "{{url('shipper/shipment/product/data')}}",
          //     data: {id: id},
          //     cache: false,
          //     success: function(data)
          //     {
          //       document.getElementById('product_id_1').value = JSON.parse(data).id;
          //       $('#product_id_1').select2().trigger('change');
          //       document.getElementById('sku_type_1').value = JSON.parse(data).id;
          //     }
          //     });
          // });

        });
    </script>

   @endsection
