<?php $session_data = array();?>
@if(!empty(Session::get('truck_data')))
<?php 
$session_data = Session::get('truck_data');
//  echo '<pre>';print_r($session_data);die();
 ?>
@endif
@extends('shipper.layouts.master')
@section('content')
   {{-- <div class="content-page create-shipment-page">
   	  <div class="content">--}}
   	  	 <div class="container-fluid">
            @include('admin.include.flash-message')
   	  	 	<div class="row align-items-center">
   	  	 		<div class="col">
   	  	 			  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0">
                       <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{__('Create Shipment')}}</a></li>
                       <li class="breadcrumb-item active txt-blue" aria-current="page">{{__('Truck Type')}}</li>
                    </ol>
                </nav>
   	  	 		</div>
   	  	 	</div>
           
           
            <div class="row d-flex align-items-center products">
             <div class="col-md-8 col-lg-6">
                <h3 class="txt-blue m-0">{{__('Choose Your Truck Requirement')}}</h3>
                {{-- <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p> --}}
             </div>
             <div class="col-md-4 col-lg-5"></div>
          </div>
          <form action="{{ route('shipper.shipment.create.step2') }}" method="POST">
            @csrf
          <div class="row">
             <div class="col">
                <div class="select-trucks mt-2">
                   <div class="title-txt font-16 txt-blue">{{__('Select Truck type with sub category')}}</div>

                   <div class="truck-category my-4"  style="background-color: white; padding-top: 2.25rem;padding-bottom: 1rem;padding-left:1rem;padding-right:1rem;">
                    @foreach($truckDetails as $key => $truck)
                        @if(isset($session_data) && !empty($session_data))
                        <div class="checkmark mb-4 mb-lg-5">
                            <div class="form-group d-flex align-items-center">
                                 <div class="label-wrapper">
                                    {{-- <input type="checkbox"  id="{{ $truck['name'] }}" class="largerCheckbox" name="truck_type[]" value="{{ $truck['id'] }}"> --}}
                                    <label><input type="checkbox" id="truck_type_{{ $truck['id'] }}" class="largerCheckbox" name="truck_type[]" onclick="validateInput(this)" value="{{ $truck['id'] }}" @if(isset($session_data['truck_type']) && !empty($session_data['truck_type'])){{ in_array($truck['id'],$session_data['truck_type']) ? 'checked' : '' }} @endif><span></span></label><br>
                                    <label for="Semi-trailer Truck"></label>
                                 </div>
                                 @php
                                     $src = 'images/type/'.$truck['icon'];
 
                                     if($key < $count){
                                         $style = 'border-bottom: 1px solid silver';
                                     }else{
                                         $style = '';
                                     }
                                 @endphp
                                 <div class="text-wrapper d-flex align-items-center pl-3 pl-lg-4">
                                     <span class="wrap-both d-block">
                                         <img src="{{ asset($src)}}" class="img-fluid" width="150" style="object-fit: scale-down;" height="120">
                                         <span class="text d-block mt-2 txt-blue">1{{ $truck['name'] }}</span>
                                     </span>
                                 </div>
                                 {{-- <div class="dropdown" style="width: 22%; right: -10%;">
                                    <a class="dropdown-toggle form-control form-control-without-border pb-1 pl-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none;">
                                      {{__('Select Sub-Category')}} <i class="fa fa-angle-down ml-5" style="font-size:24px;float: right;"></i>
                                    </a>
                                    <p class="text-danger" id="errorShow_{{ $truck['id'] }}" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> {{__('Please select any subcategory')}}</p>
 
                                    <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuLink">
                                     <ul class="m-0 p-0" >
                                         @foreach ($truck['category'] as $truck_cat)
                                             <div class="form-group mb-3" style="{{ $style }}"> --}}
                                                 {{-- <input type="checkbox" id="Curtainside" class="largerCheckbox" name="truck_type_category[]" value="{{ $truck_cat['id'] }}"> --}}
                                                 {{-- <label><input type="checkbox" id="truck_categories_{{ $truck['id'] }}" class="largerCheckbox" onclick="validateSubcategory(this, {{ $truck['id'] }})" name="truck_categories[{{ $truck['id'] }}][]" value="{{ $truck_cat['id'] }}"><span></span></label>
                                                 <label for="category_name" class="txt-blue">{{ $truck_cat['name'] }}</label>
                                             </div>
                                         @endforeach
                                     </ul>
                                   </div>
                                 </div> --}}
                                 <div class="sub-category-page pl-3" style="width: 75%">
                                     <select class="form-control select2-multiple select2-hidden-accessible my-select" id="truck_categories_{{ $truck['id'] }}" name="truck_categories[{{ $truck['id'] }}][]" multiple="multiple" placeholder="Please Select Truck Type" data-parsley-required-message="Please select any subcategory">
                                       @foreach ($truck['category'] as $truck_cat)
                                       <option value="{{ $truck_cat['id'] }}" data-badge="" @if(isset($session_data['truck_categories'][$truck['id']]) && !empty($session_data['truck_categories'][$truck['id']])) {{ in_array($truck_cat['id'],$session_data['truck_categories'][$truck['id']]) ? 'selected' : '' }} @endif>{{ $truck_cat['name'] }}</option>
                                       @endforeach
                                       {{-- <option value="O2" data-badge="">Option2</option>
                                       <option value="O3" data-badge="">Option3</option>
                                       <option value="O4" data-badge="">Option4</option>
                                       <option value="O5" data-badge="">Option5</option>
                                       <option value="O6" data-badge="">Option6</option>
                                       <option value="O7" data-badge="">Option7</option> --}}
                                     </select>
                                     <p class="text-danger" id="errorShow_{{ $truck['id'] }}" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> {{__('Please select any subcategory')}}</p>
                                   </div>
                             </div>
                       </div>
                        @else
                        <div class="checkmark mb-4 mb-lg-5">
                            <div class="form-group d-flex align-items-center">
                                 <div class="label-wrapper">
                                    {{-- <input type="checkbox"  id="{{ $truck['name'] }}" class="largerCheckbox" name="truck_type[]" value="{{ $truck['id'] }}"> --}}
                                    <label><input type="checkbox" id="truck_type_{{ $truck['id'] }}" class="largerCheckbox" name="truck_type[]" onclick="validateInput(this)" value="{{ $truck['id'] }}"><span></span></label><br>
                                    <label for="Semi-trailer Truck"></label>
                                 </div>
                                 @php
                                     $src = 'images/type/'.$truck['icon'];
 
                                     if($key < $count){
                                         $style = 'border-bottom: 1px solid silver';
                                     }else{
                                         $style = '';
                                     }
                                 @endphp
                                 <div class="text-wrapper d-flex align-items-center pl-3 pl-lg-4">
                                     <span class="wrap-both d-block">
                                         <img src="{{ asset($src)}}" class="img-fluid" width="150" style="object-fit: scale-down;" height="120">
                                         <span class="text d-block mt-2 txt-blue">{{ $truck['name'] }}</span>
                                     </span>
                                 </div>
                                 {{-- <div class="dropdown" style="width: 22%; right: -10%;">
                                    <a class="dropdown-toggle form-control form-control-without-border pb-1 pl-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none;">
                                      {{__('Select Sub-Category')}} <i class="fa fa-angle-down ml-5" style="font-size:24px;float: right;"></i>
                                    </a>
                                    <p class="text-danger" id="errorShow_{{ $truck['id'] }}" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> {{__('Please select any subcategory')}}</p>
 
                                    <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuLink">
                                     <ul class="m-0 p-0" >
                                         @foreach ($truck['category'] as $truck_cat)
                                             <div class="form-group mb-3" style="{{ $style }}"> --}}
                                                 {{-- <input type="checkbox" id="Curtainside" class="largerCheckbox" name="truck_type_category[]" value="{{ $truck_cat['id'] }}"> --}}
                                                 {{-- <label><input type="checkbox" id="truck_categories_{{ $truck['id'] }}" class="largerCheckbox" onclick="validateSubcategory(this, {{ $truck['id'] }})" name="truck_categories[{{ $truck['id'] }}][]" value="{{ $truck_cat['id'] }}"><span></span></label>
                                                 <label for="category_name" class="txt-blue">{{ $truck_cat['name'] }}</label>
                                             </div>
                                         @endforeach
                                     </ul>
                                   </div>
                                 </div> --}}
                                 <div class="sub-category-page pl-3" style="width: 75%">
                                     <select class="form-control select2-multiple select2-hidden-accessible my-select" id="truck_categories_{{ $truck['id'] }}" name="truck_categories[{{ $truck['id'] }}][]" multiple="multiple" placeholder="Please Select Truck Type" data-parsley-required-message="Please select any subcategory">
                                       @foreach ($truck['category'] as $truck_cat)
                                       <option value="{{ $truck_cat['id'] }}" data-badge="">{{ $truck_cat['name'] }}</option>
                                       @endforeach
                                       {{-- <option value="O2" data-badge="">Option2</option>
                                       <option value="O3" data-badge="">Option3</option>
                                       <option value="O4" data-badge="">Option4</option>
                                       <option value="O5" data-badge="">Option5</option>
                                       <option value="O6" data-badge="">Option6</option>
                                       <option value="O7" data-badge="">Option7</option> --}}
                                     </select>
                                     <p class="text-danger" id="errorShow_{{ $truck['id'] }}" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> {{__('Please select any subcategory')}}</p>
                                   </div>
                            </div>
                        </div>
                        @endif
                      @endforeach
                      <p class="text-danger" id="errorShow" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> {{__('Please select any truck type')}}</p>
                   </div>
                   <p style="color:#9B51E0; font-weight:bold;">Or</p>                   
                   <div class="other-specs mt-3 mt-md-4">
                      <!--  <p class="font-18 color-dark-purple txt-blue">{{__('Other Specifications')}}</p>
                       <div class="specs-check d-flex mt-3">
                          <div class="form-group ml-md-4 d-flex align-items-center">
                             {{-- <input type="checkbox" id="Hydraulic Door" class="largerCheckbox" name="other_specifications" value="Hydraulic Door"> --}}
                             <label class="mb-0"><input type="checkbox" id="Hydraulic Door" class="largerCheckbox" name="hydraulic" value="1" @if(isset($session_data['hydraulic']) && !empty($session_data['hydraulic']) && $session_data['hydraulic'] == '1') checked @endif><span></span></label>
                             <label for="Hydraulic Door" class="pl-2 txt-blue mb-0">{{__('Hydraulic Door')}}</label>
                          </div>
                           {{-- <div class="form-group ml-3 ml-md-4 ml-lg-5"> --}}
                             {{-- <input type="checkbox" id="Cooling" class="largerCheckbox" name="other_specifications" value="Cooling"> --}}
                             {{-- <label><input type="checkbox" id="Cooling" class="largerCheckbox" name="cooling" value="1"><span></span></label>
                             <label for="Cooling" class="pl-2 txt-blue">{{__('Cooling')}}</label>
                          </div> --}}
                       </div> -->
                      <div class="specs-check d-flex align-items-center mb-2">
                         <div class="form-group d-flex align-items-center mb-0">                             
                             <label class="mb-0"><input type="checkbox" id="chkOther" class="largerCheckbox" name="chkOther" value="1" data-parsley-multiple="chkOther"><span></span></label>
                             <label for="chkOther" class="pl-2 txt-blue mb-0">Other</label>
                          </div>
                          <div id="dvOther" class="ml-2" style="display: none">                            
                            <input type="text" id="txtTruckDescription" placeholder="Truck Description" class="px-3 py-2" />
                          </div>
                      </div>

                       {{-- <button class="show_button">{{__('Show/hide textarea')}}</button><br> --}}
                       <a href="javascript:;" class="add-one font-18 d-flex show_button" id="add_note" onclick="showImage();">
                        <ul class="d-flex m-1 p-1 mb-2">
                          <img src="{{ asset('shipper/images/minus.svg')}}" alt="chat" class="img-fluid plus-icon"  id="hide" style="visibility:hidden;">
                           <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid plus-icon" id="hide_show">
                           
                           <label class="txt-blue" style="text-decoration: underline;width: 75%;" >  {{__('Add')}}&nbsp;{{__('Note')}}</label>
                           <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearNote()"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label>
                           {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                        </a><br>
                        </ul>
                        <textarea rows="5" name="shipment_note" id="text_id" class="form-control" style="@if(isset($session_data['shipment_note']) && !empty($session_data['shipment_note'])) display:block; @else display:none; @endif  resize: none;" placeholder="{{__('Enter Shipment Note')}}">@if(isset($session_data['shipment_note']) && !empty($session_data['shipment_note'])){{ $session_data['shipment_note'] }}@endif</textarea>
                      
                        {{-- <button type="submit" class="black-btn font-16 mt-5">{{__('Continue')}}</button> --}}
                        <div class="btn-block d-flex">
                            <button type="submit" class="btn font-10 mt-3 btn-primary" id="continuebtn" style="width: 350px;height: 50px;">{{__('Continue')}}</button>
                        </div>
                       {{-- <a href="create-shipment-product-information.php" class="black-btn font-16 mt-5">{{__('Continue')}}</a> --}}
                   </div>
                </div>

             </div>
          </div>
          </form>
   	  	 </div>
   	  {{-- </div>
   </div> --}}
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>

   @endsection

   @section('script')
   <script>$(".show_button").click(function(){$("#text_id").toggle()})</script>
   <script> 
    function showImage(){
        document.getElementById('hide').style.visibility=    document.getElementById('hide').style.visibility == 'visible'? 'hidden' : 'visible';

        document.getElementById('hide_show').style.visibility=    document.getElementById('hide_show').style.visibility == 'hidden'? 'visible' : 'hidden';
    }
   </script>
   <script type="text/javascript">
   function preventBack() { window.history.forward(); }  
    setTimeout("preventBack()", 0);  
    window.onunload = function () { null };
    
    window.onbeforeunload = function() {
    return true;
};
// Remove navigation prompt
window.onbeforeunload = null;
    /* function validateInput(e){
        let errorShow = document.getElementById('errorShow');
        errorShow.style.display = 'none';

        let cat_id = "truck_categories_"+e.value;
        let error_show_id = 'errorShow_'+e.value;
        let error_show = document.getElementById(error_show_id);
        $truck_cat = $("input:checkbox[id^='"+cat_id+"']");
        if(e.checked == true){
            $truck_cat.prop('required', true);
            error_show.style.display = 'block';
            return false;
        }else{
            $truck_cat.prop('required', false);
            error_show.style.display = 'none';
            return false;
        }
    } */

    function validateInput(e){
        let errorShow = document.getElementById('errorShow');
        errorShow.style.display = 'none';
        let cat_id = "truck_categories_"+e.value;
        let error_show_id = 'errorShow_'+e.value;
        let error_show = document.getElementById(error_show_id);
        $truck_cat = $("#"+cat_id);
        if(e.checked == true){
            $truck_cat.prop('required', true);
            // error_show.style.display = 'block';
            return false;
        }else{
            $truck_cat.prop('required', false);
            // error_show.style.display = 'none';
            return false;
        }
    }

    function validateSubcategory(e, truck_id){
        let error_show_id = 'errorShow_'+truck_id;
        let cat_id = "truck_categories_"+truck_id;
        $truck_cat = $("input:checkbox[id^='"+cat_id+"']");
        let error_show = document.getElementById(error_show_id);

        if($truck_cat.is(":checked"))
        {
            error_show.style.display = 'none';
            $("#truck_type_"+truck_id).prop("checked", true);
            return false;
        }else{
            error_show.style.display = 'block';
            $("#truck_type_"+truck_id).prop("checked", false);
            return false;
        }
    }

function clearNote() {
        document.getElementById('text_id').value = "";
        document.getElementById('text_id').readOnly =false;
    }
            $(document).ready(function()
            {
                $("#continuebtn").click(function()
                {
                    let errorShow = document.getElementById('errorShow');
                    $cbx_group = $("input:checkbox[name='truck_type[]']");
                    $cbx_group = $("input:checkbox[id^='truck_type']"); // name is not always helpful ;)

                    if($cbx_group.is(":checked")){
                        $cbx_group.prop('required', false);
                        errorShow.style.display = 'none';
                     
                    }else{
                        $cbx_group.prop('required', true);
                        errorShow.style.display = 'block';
                       
                        return false;
                    }

                  
                });
            });
            $('.dropdown-menu').click(function(e){
              e.stopPropagation();
            })
           
   </script>
<script type="text/javascript">
$('.my-select').on('change', function () {
    var selected = $(this);
    var selected_val = $(this).val();
    if(selected_val.length > 0){
        $(this).closest('.checkmark').find('.largerCheckbox').prop('checked',true);
    }else{
        $(this).closest('.checkmark').find('.largerCheckbox').prop('checked',false);
    }
});

$('.largerCheckbox').on('change',function () {
    var checkbox_value = $(this).is(':checked');
    if(checkbox_value == true){
        $(this).closest('.checkmark').find('select > option').prop("selected","selected");
        $(this).closest('.checkmark').find('.my-select').trigger("change");
    }else{
        $(this).closest('.checkmark').find('select > option').prop("selected",false);
        $(this).closest('.checkmark').find('.my-select').trigger("change");
    }
})

$("#chkOther").click(function () {
            if ($(this).is(":checked")) {
                $("#dvOther").show();                
            } else {
                $("#dvOther").hide();        
            }
        });
</script>

   @endsection
