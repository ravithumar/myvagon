<div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;position: relative; overflow: hidden;"></div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/canvas2image@1.0.5/canvas2image.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCjorOISx346EPRCKse9h8K_fZmNnWZ00"></script>

<script type="text/javascript">
       
       
    var driver_lat = `{{isset($driver_location->lat)?$driver_location->lat:""}}`;
    var driver_lng = `{{isset($driver_location->lng)?$driver_location->lng:""}}`;
        var location_data = @json($location_array);

        console.log(location_data);
        open_map(location_data);
        function open_map(location_data)
  {
    var map;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    if(location_data != ''){
      var locations = location_data;
    }
    var markerOption = {
      clickable: false,
      flat: true,
    
      visible: true,
      map: map
    };
    directionsDisplay = new google.maps.DirectionsRenderer({ markerOptions: markerOption });
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(location_data[0][1],location_data[0][2]),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });


        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true,
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

    directionsDisplay.setMap(map);

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else
            alert ('failed to get directions');
    });

   /*var latlng = new google.maps.LatLng(driver_lat, driver_lng);
   var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon:truck
    });
    marker.setMap(map);
    setMarker(map,marker);

    $('#view-map').modal('show');*/
  }

</script>
