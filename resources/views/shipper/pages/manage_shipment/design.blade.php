@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">Manage Shipments</a></li>
          <li class="breadcrumb-item active" aria-current="page">Order ID #567898</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>
  @include('admin.include.flash-message')
  
  <div class="row d-flex align-items-center products manage-shipments">
    <div class="col-md-6">
    	<div class="order-status d-flex align-items-center">
		  <p class="font-20 txt-blue mb-0">Order ID #567898</p>
		  <div class="form-group mb-0 ml-3">
                                   
            <select class="form-control  font-17 txt-blue border-r9 package_type pb-1" style="background: none;" name="package_1" required="">
              <option disabled="" selected="">Pending</option>
              <option value="1">Normal</option>
              <option value="2">Premium</option>
            </select>
          </div>
    	</div>
       
    </div>
    <div class="col-md-6">

    	 <ul class="d-flex align-items-center no-bids-search justify-content-end">
    	 	<li>
    	 		<a href="#" class="btn btn-primary btn-sm mr-2 mt-0">                 
            <span class="text ml-1">Search Available Trucks</span>
          </a>
    	 	</li>
    	 	<li>
    	 		<a href="#" class="flag p-1 border-r6"><img src="{{ URL::asset('shipper/images/flag.svg')}}" alt="" class="img-fluid"></a>
    	 	</li>
    	 	<li>
    	 		<a href="#" class="cancel p-1 border-r6"  data-toggle="modal" data-target="#cancel-shipment"><img src="{{ URL::asset('shipper/images/cancel-no-bids.svg')}}" alt="" class="img-fluid"></a>
    	 	</li>
    	 	<li>
    	 		<a href="#" class="share p-1 border-r6"><img src="{{ URL::asset('shipper/images/share.svg')}}" alt="" class="img-fluid"></a>
    	 	</li>
        

    	 </ul>

    </div>
  </div>

  <div class="row">
    
    <div class="col-lg-7 col-xl-6">
      <ul class="d-flex bid-steps pl-0 mt-3 mt-md-5">
          <!-- <li class="active position-relative">
                              <div class="d-block text-center position-relative">
                                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                                <div class="text-wrap text-left pt-2">
                                  <p class="font-16 font600 mb-0 txt-blue">Created</p>
                                  <p class="mb-0 font-14 t-black">March 17, 2021</p>
                                  <p class="mb-0 font-14 t-black">8:30am</p>
                                </div>                               
                              </div>
         </li>
         <li class="position-relative">
                              <div class="d-block text-center position-relative">
                                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                                <div class="text-wrap pt-2">
                                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center"><img src="{{ URL::asset('shipper/images/global.svg')}}" alt="" class="img-fluid pr-1">Waiting for Bid</p>
                                
                                </div>
                              </div>
         </li>
         <li class="position-relative">
                              <div class="d-block text-center position-relative">
                                <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
                              </div>
         </li>
         <li class="position-relative">
                              <div class="d-block text-center position-relative">
                                <div class="icon border-50 border-cancelled d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                                <div class="text-wrap pt-2">
                                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center"><img src="{{ URL::asset('shipper/images/global.svg')}}" alt="" class="img-fluid pr-1">Waiting for Bid</p>
                                
                                </div>
                              </div>
         </li> -->
         <li class="position-relative active d-inline-block">           
            <div class="text-wrap text-left position-relative">
                 <div class="icon border-50 d-flex align-items-center justify-content-center mb-1"><div class="inner-icon border-50"></div></div>
                 <p class="font-16 font600 mb-0 txt-blue">Created</p>
                 <p class="mb-0 font-14 t-black">March 17, 2021</p>
                 <p class="mb-0 font-14 t-black">8:30am</p>
            </div>            
         </li>
         <li class="position-relative active d-inline-block">           
            <div class="text-wrap text-left  position-relative">
                 <div class="icon border-50 d-flex align-items-center justify-content-center mb-1"><div class="inner-icon border-50"></div></div>
                 <p class="font-16 font600 mb-0 txt-blue">Created</p>
                 <p class="mb-0 font-14 t-black">March 17, 2021</p>
                 <p class="mb-0 font-14 t-black">8:30am</p>
            </div>            
         </li>
         <li class="position-relative cancelled d-inline-block">           
            <div class="text-wrap text-left  position-relative">
                 <div class="icon border-50 d-flex align-items-center justify-content-center mb-1"><div class="inner-icon border-50"></div></div>
                 <p class="font-16 font600 mb-0 txt-blue">Created</p>
                 <p class="mb-0 font-14 t-black">March 17, 2021</p>
                 <p class="mb-0 font-14 t-black">8:30am</p>
            </div>            
         </li>
       </ul>
    </div>
    <div class="col-lg-5 col-xl-6"></div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="labels-wrapper d-flex align-items-center mb-4">
        <p class="mb-0 mr-2 font-18 font500 txt-blue">Truck Details :</p>
        <p class="mb-0 mr-2 font-14 font500 txt-blue">Truck With Trailer</p>
        <div class="label-grey border-r30 p-2 mr-2 font-14 font400 txt-blue">Refrigerated With Cooling</div>
        <div class="label-grey border-r30 p-2 font-14 txt-blue font400">Flatbed Trailer</div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-10">
      <div class="no-bids-track-order-list">
          <ul class="list-unstyled mb-0 pl-4">
               <li class="completed from-location position-relative icon mb-4">
                 <div class="row">
                   <div class="col-md-4">                   
                     <h4 class="mt-0 mb-1">Abc Inc <small class="text-muted font-16">March 17, 2021   8:00am - 9:00am</small> </h4>
                     <p class="txt-black">Mithimnis 30, Athina 112 57, Greece</p>
                   </div>
                   <div class="col-md-1">
                    <small class="txt-blue font-16">Pickup</small>
                   </div>
                   <div class="col-md-7">
                     <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">
                       <li>Commodity 1</li>
                       <li>Package type 1</li>
                       <li>Quantity 1</li>
                       <li>Weight 1</li>
                       <li class="txt-red">Fragile</li>
                     </ul>
                     <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">
                       <li>Commodity 2</li>
                       <li>Package type 2</li>
                       <li>Quantity 2</li>
                       <li>Weight 2</li>
                       <li></li>
                       
                     </ul>
                     <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">
                       <li>Commodity 3</li>
                       <li>Package type 3</li>
                       <li>Quantity 3</li>
                       <li>Weight 3</li>
                      <li></li>
                     </ul>                     
                   </div>
                 </div>
                </li>
                <li class="completed to-location position-relative icon mb-4">
                  <div class="row">
                   <div class="col-md-4">                   
                    <div class="icon border-50 border-grey d-flex align-items-center justify-content-center position-absolute"><div class="inner-icon border-50"></div></div>
                  <h4 class="mt-0 mb-1">ABC Inc <small class="text-muted font-16">March 17, 2021   8:00am - 9:00am</small> </h4>
                  <p class="txt-black mb-0">Mithimnis 30, Athina 112 57, Greece</p>
                   </div>
                   <div class="col-md-1">
                    <small class="txt-blue font-16">Delivery</small>
                   </div>
                   <div class="col-md-7">
                     <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">
                       <li>Commodity 1</li>
                       <li>Package type 1</li>
                       <li>Quantity 1</li>
                       <li>Weight 1</li>
                       <li class="txt-red">Fragile</li>
                     </ul>
                                         
                   </div>
                 </div>
                  
                </li>
                <li class="completed to-location position-relative icon mb-4">
                  <div class="row">
                   <div class="col-md-4">                   
                    <div class="icon border-50 border-grey d-flex align-items-center justify-content-center position-absolute"><div class="inner-icon border-50"></div></div>
                  <h4 class="mt-0 mb-1">ABC Inc <small class="text-muted font-16">March 17, 2021   8:00am - 9:00am</small> </h4>
                  <p class="txt-black mb-0">Mithimnis 30, Athina 112 57, Greece</p>
                   </div>
                   <div class="col-md-1">
                    <small class="txt-blue font-16">Delivery</small>
                   </div>
                   <div class="col-md-7">
                     <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">
                       <li>Commodity 2</li>
                       <li>Package type 2</li>
                       <li>Quantity 2</li>
                       <li>Weight 2</li>
                       <li></li>
                     </ul>
                                         
                   </div>
                 </div>
                  
                </li>
                <li class="completed end-location position-relative icon mb-4">
                  <div class="row">
                   <div class="col-md-4">                   
                  
                      <h4 class="mt-0 mb-1">ABC Inc <small class="text-muted font-16">March 17, 2021   8:00am - 9:00am</small> </h4>
                      <p class="txt-black mb-0">Mithimnis 30, Athina 112 57, Greece</p>
                   </div>
                   <div class="col-md-1">
                     <small class="txt-blue font-16">Delivery</small>
                   </div>
                   <div class="col-md-7">
                     <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">
                       <li>Commodity 3</li>
                       <li>Package type 3</li>
                       <li>Quantity 3</li>
                       <li>Weight 3</li>
                       <li></li>
                     </ul>
                                         
                   </div>
                 </div>
                  
                </li>
          </ul>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 mt-3">
      <div class="long-haul-shipment"> 
       <div class="long-block">    
         <div class="popup d-flex align-items-center">
           <h4 class="txt-purple mr-2 font-15">Long haul shipment</h4>
           <a href="#" class="btn-tooltip position-relative d-flex align-items-center txt-blue">i
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements greater than 350 to 400km from 
origin to destination are generally classified as
 long-haul movements</span></a>
         </div>            
         <p class="mb-1 font-14 txt-blue font-weight-light">Total Miles: <em class="font-weight-bold">500</em></p>
         <p class="mb-1 font-14 txt-blue font-weight-light">Journey: <em class="font-weight-bold">48hr</em></p>
       </div>
      
      </div>
      <div class="no-bid-quote mt-3">
        <p class="d-flex align-items-center txt-blue font-18">Quote :  <strong>&euro; 500</strong> <a href="javascript:;" class="pl-2"><img src="http://13.36.112.48/public/assets/images/shipment/pen-black.svg" class="img-fluid"></a></p>
      </div>
    
      
    </div>
  </div>
</div>

<!-- Cancel Shipment -->
<div class="modal fade cancel-shipment" id="cancel-shipment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content border-r35">
      <div class="modal-body p-4">
        <p class="font-18 font600 txt-blue text-center">Do you want to continue to <em class="txt-red">cancel</em> shipment?</p>
        <p class="txt-red text-center"><strong>Warning:</strong> Cancellation fee and possible bad reviews to the account</p>
        <div class="btn-block d-flex mt-3">
            <button type="button" class="btn w-100 btn-white mr-2">Yes</button>
            <button type="submit" class="btn w-100 btn-primary"  data-dismiss="modal" aria-label="Close">No</button>
        </div>
        
      </div>
    </div>
  </div>
</div>

@endsection
@section('script')
@endsection