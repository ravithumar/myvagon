
@extends('shipper.layouts.master')

@section('content')
@if($booking['status'] == "pending")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(213,105,105,0.14) !important;border:1px solid #D56969;}
</style>
@endif

@if($booking['status'] == "in-progress")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(105,163,213,0.14) !important;border: 1px solid #699ed5;}
.pickup_address{color:  #9B51E0 !important;}
.no-bids-track-order-list .from-location:before{content: "";position: absolute;left: -36px;top: 0px;background-image: url(../images/no-bids-from-location1.svg);background-repeat: no-repeat;width:21px;height:26px;}
.address{color:  #1f1f41 !important;}
.location{color:  #000000 !important;}
          .pickup-details{background: #EDE3F6 !important;}
</style>
  
@endif

@if($booking['status'] == "completed")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(117,213,105,0.14) !important;border: 1px solid #7fd569;}

</style>
@endif
@if($booking['status'] == "completed")
<style type="text/css">
  .pickup-content {background: #EDE3F6 !important;}
small.completed{color:#9B51E0;font-weight: bold;}
 
.no-bids-track-order-list .from-location:before{content: "";position: absolute;left: -36px;top: 0px;background-image: url(../images/no-bids-from-location1.svg);background-repeat: no-repeat;width:21px;height:26px;}
.no-bids-track-order-list .from-location:after {content: ''; border-left: 1px dashed #9B51E0; position: absolute; left: -26px; top: 30px; width: 1px; height: 100%;}
.no-bids-track-order-list .to-location:after{content: ''; border-left: 1px dashed #9B51E0; position: absolute; left: -25px; top: 25px; width: 1px; height: 100%;}
.no-bids-track-order-list .end-location:before {content: ""; position: absolute; left: -34px; top: 0px; background-image: url(../images/end-flag1.svg); background-repeat: no-repeat; width: 23px; height: 26px;}

.no-bids-track-order-list ul li .icon {width: 19px;height: 19px;background-color: #ffffff;left:-22px;border-color: #9B51E0}
.no-bids-track-order-list ul li .icon  .inner-icon {width: 13px; height: 13px; background-color: #9B51E0;border-color: #9B51E0}
</style>
@endif

@if($booking['status'] == "cancelled")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(27,54,78,0.14) !important;border: 1px solid #252a38;}
</style>
@endif
@if($booking['status'] == "scheduled")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgb(213 210 105 / 14%) !important;border: 1px solid #c9d569;}
</style>
@endif
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.bid-steps li.active .icon .inner-icon {
    background-color: #1F1F41;
    width: 12px;
    height: 12px;
}
.table td, .table th {
    padding: 0.85rem;
    vertical-align: revert;
    border-top: 1px solid #dee2e6;
}
.btn-outline-deline {
  background-color: white; 
  color: #f44336; 
  border: 1px solid #f44336;
}

.btn-outline-deline:hover {
  background-color: #f44336;
  color: white;
}
.scroll-container {
    overflow: hidden;
    overflow-x: auto;
}
.sorting-btn{
    border-radius: 20px;
    /* margin: 0px 10px; */
    padding: 2px 20px;
}
.sort-button .active{
  background-color: #eadafd;
  color: #9B51E0 !important;
  -webkit-box-shadow : none;
  box-shadow : none;
}
.table td {
  border-left: none;
  border-right: none;
}
.table th {
  border-left: none;
  border-right: none;
}


.radio-block .radio.active {
    
    border-radius: 34px;
}
.radio-block .radio {
    padding: 15px 40px;
    line-height: normal;
}
.radio-block .radio label::before {
    width: 20px;
    height: 20px;
    border: 1px solid #707070;
}
.radio-block .radio label::after {
    background-color: #9B51E0;
    width: 12px;
    height: 12px;
    left: -15px;
    top: 3px;
    margin: 1px;
}

.table thead th {
    vertical-align: top;
    border-bottom: 2px solid #dee2e6;
}
.radio label {
    padding-left: 25px;
    font-size: 14px;
    font-weight: 600;
}
.txt-black{color: #1F1F41 !important; font-weight: 700; }
</style>

<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">{{__('Manage Shipments')}}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__('Order ID')}} #{{$booking['id']}}</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6">
      <p class="font-20 txt-blue mb-0 mr-2 float-right">{{ $booking['is_public'] == '1' ? 'Public - ' : 'Private - ' }} {{ $booking['is_bid'] == '1' ? 'Bid Request' : 'Book Request' }}</p>
    </div>
  </div>
  @include('admin.include.flash-message')
  
  <div class="row d-flex align-items-center products manage-shipments">
    <div class="col-md-6">
    	<div class="order-status d-flex align-items-center">
		  <p class="font-20 txt-blue mb-0">{{__('Order ID')}} #{{$booking['id']}}</p>
		  <div class="form-group mb-0 ml-3">
        <select class="form-control  font-17 txt-blue border-r9 package_type pb-1" style="background: none;appearance: none;text-align:center;width: auto" name="status" required="" disabled>
          <option value="pending" {{$booking['status'] == 'pending' ? 'selected' : ''}}>{{__('Pending')}}</option>
          <option value="scheduled" {{$booking['status'] == 'scheduled' ? 'selected' : ''}}>{{__('Scheduled')}}</option>
          <option value="in-progress" {{$booking['status'] == 'in-progress' ? 'selected' : ''}}>{{__('In-Progress')}}</option>
          <option value="completed" {{$booking['status'] == 'completed' ? 'selected' : ''}}>{{__('Completed')}}</option>
          <option value="cancelled" {{$booking['status'] == 'cancelled' ? 'selected' : ''}}>{{__('Cancelled')}}</option>
        </select>
      </div>
      @if($booking['status'] == 'completed' && $booking['pod_image'] != NULL && !empty($booking['pod_image']))
      <a class="btn btn-light btn-sm border-r9 font-17 txt-blue pod_image" style="margin-left: 30px;width: 140px;border: 1px solid #454545;" href="" target="_blank" data-toggle="modal" data-target="#pod_model">POD
      </a>
      @endif
      
      @if($booking['status']=='cancelled')
      <p  class="font-12 txt-red mb-0" style="margin-left: 30px">{{__('Note:Cancellation fee of') }} € {{$booking['cancellation_charge']}} {{__('will be billed to your account')}}</p>
      @endif
    </div>
  </div>
    <div class="col-md-6">
        @if(isset($booking['status']) && !empty($booking['status']) && $booking['status'] != 'completed')
        <ul class="d-flex align-items-center no-bids-search justify-content-end">
          @if($booking['status'] == 'pending')
          @if(!empty($search_driver))
          <li>
            {{-- <a href="{{ url('shipper/search-available-trucks') }}" class="btn btn-primary btn-sm mr-2 mt-0">                  --}}
            <a href="#search_available_truck" class="btn btn-primary btn-sm mr-2 mt-0 search_available_truck">                 
             <span class="text ml-1">{{__('Search Available Trucks')}}</span>
           </a>
          </li>
          @endif
          @endif
          @if(!empty($cancel_shipment) && ($booking['status'] == 'pending' || $booking['status'] == 'create'))
          <li>
            <a href="{{ url('shipper/shipment/edit',$booking['id']) }}" class="cancel p-1 border-r6" data-id="{{ $booking['id'] }}"><img src="{{ URL::asset('shipper/images/pen.svg')}}" alt="" class="img-fluid"></a>
          </li>
          @endif
          <li>
            <a href="javascript:void(0)" class="flag p-1 border-r6 open_map" data-id="{{ $booking['id'] }}"><img src="{{ URL::asset('shipper/images/flag.svg')}}" alt="" class="img-fluid"></a>
          </li>
          @if(!empty($cancel_shipment) && ($booking['status'] == 'pending' || $booking['status'] == 'scheduled' || $booking['status'] == 'create'))
          <li>
            <a href="javascript:void(0)" class="cancel p-1 border-r6 cancel_shipment" data-id="{{ $booking['id'] }}"><img src="{{ URL::asset('shipper/images/delete.svg')}}" alt="" class="img-fluid"></a>
          </li>
          @endif
           @if(!empty($cancel_shipment) && ($booking['status'] == 'pending' || $booking['status'] == 'scheduled' || $booking['status'] == 'create'))
          <li>
            <a class="share p-1 border-r6 share_button" href="javascript:void(0)" data-id="{{ $booking['id'] }}"><img src="{{ URL::asset('shipper/images/share.svg')}}" alt="" class="img-fluid"></a>
          </li>
           @endif
          @if(isset($cancel_request) && !empty($cancel_request))
          <li>
            <a class="cancel-request" href="javascript:void(0)" data-id="{{ $cancel_request['id'] }}" data-booking-id="{{ $cancel_request['booking_id'] }}"><img src="{{ URL::asset('shipper/images/Cancel.png')}}" alt="" class="img-fluid"></a>
          </li>
          @endif
          @if(!empty($cancel_shipment) && ($booking['status'] == 'pending' || $booking['status'] == 'scheduled' || $booking['status'] == 'create'))
          <li>
              <a type="submit" href="javascript:void(0)" class="cancel p-1 border-r6 cancel_status_shipment" data-id="{{ $booking['id'] }}"><img src="{{ URL::asset('shipper/images/cancel-black.svg')}}" alt="" class="img-fluid" name="cancellation_charge"></a>
          </li>
          @endif
        </ul>
        @endif
        @if($booking['status'] == 'completed' && $booking['payment_status'] == 'pending' && $booking['amount'] > 0)
          <a class="btn btn-primary btn-sm border-r9 font-17" style="float:right;margin-left: 30px;width: 140px;" href="javascript:void(0);" target="_blank" data-toggle="modal" data-target="#payment">PAY €{{ CommonHelper::price_format($booking['amount'],'GR') }}</a>
        @endif
    </div>
  </div>

  <div class="row">
    
  <div class="col-lg-12 col-xl-12">
        <ul class="d-flex bid-steps pl-0 mt-3 mt-md-5 scroll-container grey-scroll" style="margin-top: 2.5rem!important;">
          <li class="active position-relative d-inline-block">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-purple txt-black">{{__('Created')}}</p>
                <p class="mb-0 font-14 t-black">{{ date('d/m/Y',strtotime($booking['created_at'])) }}</p>
                <p class="mb-0 font-14 t-black">{{ date('H:i A',strtotime($booking['created_at'])) }}</p>
              </div>                               
            </div>
          </li>
          @if($booking['status'] == 'create')
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
          @endif
         @if($booking['status'] == 'pending')
          @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Waiting for Bid')}}</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Waiting for Booking Request')}}</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
         @endif

         @if($booking['status'] == 'scheduled')
          @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Bid Accepted')}}</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Booking Accepted')}}</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap pt-2">
                <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Carrier')}}: @if(isset($booking_truck) && !empty($booking_truck)) @foreach($booking_truck as $truck) @if(isset($truck['driver_details']['name']) && !empty($truck['driver_details']['name'])) <a href="{{ url('shipper/manage-shipment/carrier-info',$truck['driver_details']['id']) }}" class="txt-black">{{ $truck['driver_details']['name'] }}</a> @endif  @endforeach @endif </p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-nowrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
         @endif

         @if($booking['status']  == 'in-progress')
         @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">Private Bid Accepted</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">Booking Accepted</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap pt-2">
                <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">Carrier : @if(isset($booking_truck) && !empty($booking_truck)) @foreach($booking_truck as $truck) @if(isset($truck['driver_details']['name']) && !empty($truck['driver_details']['name'])) <a href="{{ url('shipper/manage-shipment/carrier-info',$truck['driver_details']['id']) }}" class="txt-black">{{ $truck['driver_details']['name'] }}</a> @endif  @endforeach @endif</p>
              </div>
            </div>
          </li>
          <!-- <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center">Pick up from {{ $booking_locations[0]['company_name'] }}</p>
                <p class="mb-0 font-14 t-black">{{ date('d/m/Y',strtotime($booking['pickup_date'])) }}</p>
                <p class="mb-0 font-14 t-black">{{ $booking['pickup_time_from'] }}</p>
              </div>
            </div>
          </li> -->
          @if(isset($booking_locations) && !empty($booking_locations))
            @foreach($booking_locations as $locations)
              @if($locations['arrived_at']!=NULL)
                   <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap text-left pt-2">
                      <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">
                        @if($locations['is_pickup'] == 1)
                          Picked Up from {{ $locations['company_name']}}  
                        @elseif($locations['is_pickup'] == 0) 
                        {{ $locations['company_name']}} 
                        @endif
                      <p class="mb-0 font-14 t-black">{{ date('d/m/Y',strtotime($locations['delivered_at'])) }}</p>
                      <p class="mb-0 font-14 t-black">{{ $locations['delivery_time_from'] }}</p>
                      </p>
                    </div>
                  </div>
                </li>
              @endif
            @endforeach
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('In-Transit')}}</p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-nowrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
         @endif
         @if($booking['status'] == 'completed')
          @if($booking['is_bid'] == 1)
              <li class="position-relative d-inline-block active">
                <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                  <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                  <div class="text-nowrap pt-2">
                    <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Bid Accepted')}}</p>
                  </div>
                </div>
              </li>
            @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Booking Accepted')}}</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap pt-2">
                <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">Carrier: @if(isset($booking_truck) && !empty($booking_truck)) @foreach($booking_truck as $truck) @if(isset($truck['driver_details']['name']) && !empty($truck['driver_details']['name'])) <a href="{{ url('shipper/manage-shipment/carrier-info',$truck['driver_details']['id']) }}" class=" txt-black">{{ $truck['driver_details']['name'] }}</a> @endif  @endforeach @endif</p>
              </div>
            </div>
          </li>
          @if(isset($booking_locations) && !empty($booking_locations))
            @foreach($booking_locations as $locations)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap text-left pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">@if($locations['is_pickup'] == 1){{__('Picked Up from')}} @else {{__('Delivered at')}} @endif {{ $locations['company_name'] }}</p>
                  <p class="mb-0 font-14 t-black">{{ date('d/m/Y',strtotime($locations['delivered_at'])) }}</p>
                  <p class="mb-0 font-14 t-black">{{ $locations['delivery_time_from'] }}</p>
                </div>
              </div>
            </li>
            @endforeach
          @endif
          @if($booking['pod_image'] == NULL && empty($booking['pod_image']))
              <li class="position-relative d-inline-block active">
                <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                  <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                  <div class="text-wrap pt-2">
                    <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('POD pending')}}</p>
                  </div>
                </div>
              </li>
            @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap text-left pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('POD Uploaded')}}</p>
                   <p class="mb-0 font-14 t-black">{{ date('d/m/Y',strtotime($locations['delivered_at'])) }}</p>
                   <p class="mb-0 font-14 t-black" style="text-align: left">{{ $locations['delivery_time_from'] }}</p>
                </div>
              </div>
            </li>
          @endif
          @if($booking['payment_status'] == 'pending' || empty($booking['payment_status']))
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Payment Pending')}}</p>
              </div>
            </div>
          </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap text-left pt-2">
                  <p class="font-16 font600 mb-0 txt-purple d-flex align-items-center txt-black">{{__('Payment Completed')}}</p>
                </div>
              </div>
            </li>
          @endif

         @endif
         @if($booking['status'] == 'cancelled')
          @if($booking['is_bid'] == 1)
          <form action="{{ url('shipper/manage-shipment',$booking['id']) }}" method="get" name="cancellation_charge">
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center txt-black">{{__('Waiting for Bid')}}</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-nowrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center txt-black">{{__('Waiting for Booking Request')}}</p>
                </div>
              </div>
            </li>
          </form>
          @endif
          <li class="position-relative d-inline-block cancelled">
            <div class="text-wrap text-center position-relative">
              <div class="icon border-50 border-cancelled d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-nowrap pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center txt-black">{{__('Cancelled Shipment')}}</p>   
              </div>
            </div>
          </li>
         @endif
        </ul>
    </div>
    <div class="col-md-6"></div>
  </div>
  @if(isset($booking_truck) && !empty($booking_truck))
    @for($i=0;$i<count($booking_truck);$i++)
    <div class="row">
      <div class="col-md-12">
        <div class="labels-wrapper d-flex align-items-center mb-6">
          <p class="mb-0 mr-2 font-18 font500 txt-blue">@if($i==0) {{__('Truck Details')}} : @endif</p> 
          @if($i==0 && isset($booking_truck[$i]['note']) && !empty($booking_truck[$i]['note']))<p class="txt-purple mb-0 font-13"><a href="javascript:void(0)" class="note_section" title="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Truck Note" data-note="{{ $booking_truck[$i]['note'] }}" style="color:#9B51E0 !important">Note</a></p>@endif
        </div>
        <div class="labels-wrapper d-flex align-items-center mb-2  grey-scroll">
          <div class="ml-2 d-flex align-items-center">
              <p class="mt-0 font-18  txt-blue mb-0 text-nowrap" style="font-family: Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif; font-weight: 600;">@if(!empty($booking_truck[$i]['truck_type']['name']) && isset($booking_truck[$i]['truck_type']['name'])){{ $booking_truck[$i]['truck_type']['name'] }} @endif</p>
            @if(isset($booking_truck[$i]['truck_type_category']) && !empty($booking_truck[$i]['truck_type_category']))
            <ul class="list-unstyled d-flex mb-0 scroll-container grey-scroll row text-center text-uppercase">
            @foreach($booking_truck[$i]['truck_type_category'] as $truck_type_category)
              <li class="label-grey border-r30 p-2 font-14 txt-blue font400 text-nowrap nav-item" style="margin-left: 20px;">{{ $truck_type_category['name'] }}</li>
            @endforeach
            </ul>
            @endif
          </div>
          
        </div>
      </div>
    </div>

    @endfor
  @endif
  @if(isset($booking_locations) && !empty($booking_locations))
    @if($booking['booking_type'] == 'direct_shipment')
      <div class="row mt-3">
        <div class="col-xl-10">
          <div class="labels-wrapper d-flex align-items-center mb-4">
            <p class="mb-0 mr-2 font-18 font500 txt-blue">{{__('Pickup/Delivery Location Details')}} :</p> 
          </div>
          <div class="no-bids-track-order-list ml-3">
              <ul class="list-unstyled mb-0 pl-3">
                <?php $last_address = end($booking_locations);?>
                @for($i=0;$i<count($booking_locations);$i++)
                  <li @if($booking_locations[$i]['is_pickup']==1) class="completed from-location position-relative icon mb-4" @elseif(isset($last_address['id']) && !empty($last_address['id']) && $last_address['id'] == $booking_locations[$i]['id']) class="completed end-location position-relative icon mb-4" @else class="completed to-location position-relative icon mb-4" @endif>
                    <div class="row">
                      <div class="col-md-4">  
                        @if($booking_locations[$i]['is_pickup']==0 && $last_address['id'] != $booking_locations[$i]['id'])
                          <div class="icon border-50 border-grey d-flex align-items-center justify-content-center position-absolute"><div class="inner-icon border-50"></div></div>
                        @endif
                      <h4 class="mt-0 mb-1 @if(!empty($booking_locations[$i]['arrived_at'])) location @endif">{{ $booking_locations[$i]['company_name'] }} <small class="text-muted font-16 @if(!empty($booking_locations[$i]['arrived_at'])) location @endif">{{ date('d/m/Y',strtotime($booking_locations[$i]['delivered_at'])) }}   @if($booking_locations[$i]['delivery_time_from'] == $booking_locations[$i]['delivery_time_to']){{ $booking_locations[$i]['delivery_time_from'] }} @else {{ $booking_locations[$i]['delivery_time_from'] }} - {{ $booking_locations[$i]['delivery_time_to'] }} @endif</small> </h4>

                        <p class="txt-black @if(!empty($booking_locations[$i]['arrived_at'])) location @endif">{{ $booking_locations[$i]['drop_location'] }}</p>  
                         @if(isset($booking_locations[$i]['note']) && !empty($booking_locations[$i]['note']))
                                <a href="javascript:void(0)" class="note_section" title="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Location Note" data-note="{{ $booking_locations[$i]['note'] }}" style="font-weight: bold;color:#9B51E0 !important">Note</a>
                        @endif
                      </div>
                      <div class="col-md-2 d-flex align-items-center">
                        <small class="mb-3 mb-md-0 completed font-16  @if(!empty($booking_locations[$i]['arrived_at'])) pickup_address  @endif">@if($booking_locations[$i]['is_pickup']==1) Pickup @else Delivery @endif </small>

                      </div>
                      <div class="col-md-6">
                        @if(isset($booking_locations[$i]['products']) && !empty($booking_locations[$i]['products']))
                          @foreach($booking_locations[$i]['products'] as $products)
                          <div class="scroll-container">
                            <ul class="@if(!empty($booking_locations[$i]['arrived_at'])) pickup-details @endif pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-2 txt-blue justify-content-center text-center scroll-container grey-scroll">
                              <li>{{ $products['product_id']['name'] }}</li>
                              {{-- <li>{{ $products['product_type']['name'] }}</li> --}}
                              <li>{{ $products['qty'] }} @if(isset($products['product_type']['name']) && !empty($products['product_type']['name'])) {{ $products['product_type']['name'] }} @endif</li>
                              <li style="width: 122px;">{{ $products['weight'] }} {{ $products['unit']['name'] }}</li>
                              <li class="txt-red"> @if($products['is_fragile'] == 1) Fragile @endif </li>
                              <li class="txt-red" style="width: 222px;"> @if($products['is_sensetive'] == 1) Sensitive to odour @endif </li>
                             </ul>
                          </div>
                          @endforeach
                        @endif
                        @if(isset($products['note']) && !empty($products['note']))
                              <p class="txt-purple mr-2 font-13" style="float: right;word-break: break-all;"><a href="javascript:void(0)" class="note_section" data-note="{{ $products['note'] }}" title="Product Description" style="color:#9B51E0 !important">Product Description</a></p> 
                          @endif
                      </div>
                    </div>
                  </li>
                @endfor
              </ul>
          </div>
        </div>
      </div>
    @else
      <div class="row mt-3">
        <div class="col-xl-10">
          <div class="labels-wrapper d-flex align-items-center mb-4">
            <p class="mb-0 mr-2 font-18 font500 txt-blue">{{__('Pickup/Delivery Location Details')}} :</p> 
          </div>
          <div class="no-bids-track-order-list">
              <ul class="list-unstyled mb-0 pl-3">
                <?php $last_address = end($booking_locations);?>
                @for($i=0;$i<count($booking_locations);$i++)
                  <li @if($booking_locations[$i]['is_pickup']==1) class="completed from-location position-relative icon mb-4" @elseif(isset($last_address['id']) && !empty($last_address['id']) && $last_address['id'] == $booking_locations[$i]['id']) class="completed end-location position-relative icon mb-4" @else class="completed to-location position-relative icon mb-4" @endif>
                    <div class="row">
                      <div class="col-md-4">  
                        @if($booking_locations[$i]['is_pickup']==0 && $last_address['id'] != $booking_locations[$i]['id'])
                          <div class="icon border-50 border-grey d-flex align-items-center justify-content-center position-absolute"><div class="inner-icon border-50"></div></div>
                        @endif
                      <h4 class="mt-0 mb-1 @if(!empty($booking_locations[$i]['arrived_at'])) location @endif">{{ $booking_locations[$i]['company_name'] }} <small class="text-muted font-16 @if(!empty($booking_locations[$i]['arrived_at'])) location @endif">{{ date('d/m/Y',strtotime($booking_locations[$i]['delivered_at'])) }}   @if($booking_locations[$i]['delivery_time_from'] == $booking_locations[$i]['delivery_time_to']){{ $booking_locations[$i]['delivery_time_from'] }} @else {{ $booking_locations[$i]['delivery_time_from'] }} - {{ $booking_locations[$i]['delivery_time_to'] }} @endif</small> </h4>
                      <p class="txt-black @if(!empty($booking_locations[$i]['arrived_at'])) location @endif">{{ $booking_locations[$i]['drop_location'] }}</p>   
                        @if(isset($booking_locations[$i]['note']) && !empty($booking_locations[$i]['note']))
                          <div class="col mt-7">
                                <a href="javascript:void(0)" class="note_section" title="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Location Note" data-note="{{ $booking_locations[$i]['note'] }}" style="font-weight: bold;color:#9B51E0 !important">Note</a>
                          </div>
                        @endif       
                      </div>
                      <div class="col-md-1">
                        @if(isset($booking_locations[$i]['products']) && !empty($booking_locations[$i]['products']))
                          @foreach($booking_locations[$i]['products'] as $products)
                          <ul class="d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">

                            <li class="txt-blue font-16 @if(!empty($booking_locations[$i]['arrived_at'])) pickup_address  @endif">@if($products['is_pickup'] == '1') Pickup @else Delivery @endif</li>
                            
                          </ul>
                          {{-- <small class="txt-blue font-16">@if($booking_locations[$i]['is_pickup']==1) Pickup @else Delivery @endif</small> --}}
                          @endforeach
                        @endif
                      </div>
                      <div class="col-md-7">
                        @if(isset($booking_locations[$i]['products']) && !empty($booking_locations[$i]['products']))
                          @foreach($booking_locations[$i]['products'] as $products)
                          <ul class="@if(!empty($booking_locations[$i]['arrived_at'])) pickup-details @endif pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-2 txt-blue justify-content-center text-center">
                            <li>{{ $products['product_id']['name'] }} </li>
                            {{-- <li>{{ $products['product_type']['name'] }}</li> --}}
                            <li>{{ $products['qty'] }} @if(isset($products['product_type']['name']) && !empty($products['product_type']['name'])) {{ $products['product_type']['name'] }} @endif</li>
                            <li style="width: 122px;">{{ $products['weight'] }} {{ $products['unit']['name'] }} @if(isset($products['note']) && !empty($products['note'])) <br> <p class="txt-purple mb-0 font-13"></p> @endif</li>
                            <li class="txt-red"> @if($products['is_fragile'] == 1) Fragile @endif  </li>
                            <li class="txt-red" style="width: 122px;"> @if($products['is_sensetive'] == 1) Sensitive to odour @endif </li>
                          </ul>
                          @endforeach
                        @endif
                        @if(isset($products['note']) && !empty($products['note']))
                              <p class="txt-purple mr-2 font-13" style="float: right;"><a href="javascript:void(0)" class="note_section" data-note="{{ $products['note'] }}" title="Product Description" style="color:#9B51E0 !important">Product Description</a></p> 
                          @endif
                      </div>
                    </div>
                  </li>
                @endfor
              </ul>
          </div>
        </div>
      </div>
    @endif
  @endif
  <div class="row">
    <div class="col-md-12 mt-3">
      <div class="long-haul-shipment"> 
       <div class="long-block">    
         <div class="popup d-flex align-items-center" style="margin-top: -46px;">
           <h4 class="txt-purple mr-2 font-15" >{{ $booking['journey_type'] }}</h4>
           <a href="javascript:void(0)" class="btn-tooltip position-relative d-flex align-items-center txt-blue">i
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Long haul shipment.')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements greater than 150 from 
              origin to destination are generally classified as
               long-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Medium haul shipment.')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 50km to 150km from 
              origin to destination are generally classified as
               medium-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Short haul shipment.')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 0km to 50km from 
              origin to destination are generally classified as
               short-haul movements</span></a>
            @endif
            </a>
         </div>            
         <p class="mb-1 font-16 txt-blue font-weight-light">{{__('Total Kilometers')}}: <em class="font-weight-bold">{{ $booking['distance'] }}</em></p>
         <p class="mb-1 font-16 txt-blue font-weight-light">{{__('Journey')}}: <em class="font-weight-bold">{{ $booking['journey'] }}</em></p>
       </div>
      
      </div>
      @if(isset($booking['amount']) && !empty($booking['amount']))
      @if(!empty($manage_price))
      <div class="no-bid-quote mt-3">
        <p class="d-flex align-items-center txt-blue font-18">{{__('Quote')}} :  <strong>&euro; {{ CommonHelper::price_format($booking['amount'],'GR') }}</strong> 
          {{-- <a href="javascript:;" class="pl-2"><img src="http://13.36.112.48/public/assets/images/shipment/pen-black.svg" class="img-fluid"></a> --}}
        </p>
      </div>
      @endif
      @endif

      @if(isset($booking_requests) && !empty($booking_requests))
      <?php 
      $first_key = array_key_first($booking_requests);
      $last_key = array_key_last($booking_requests);?>
        @foreach($booking_requests as $booking_req)
        <div class="truck-box bg-white p-2 mb-3">
          <div class="row align-items-center">
            <div class="col-md-2 tr-br">
              <p class="mb-0 font-14 txt-blue font-weight-light"><em class="font-weight-bold">{{__('Truck Details')}}</em><br> for @if(isset($booking_req['is_bid']) && !empty($booking_req['is_bid']) && $booking_req['is_bid'] == 1) {{__('bidding')}} @else {{__('booking')}} @endif</p>
            </div>
            <div class="col-md-10">
              <div class="row" style="margin-right:6px;margin-top:6px; ">
                <div class="col-md-2">
                  <p class="txt-purple mb-0">{{__('Start Date')}}</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">{{ date('d/m/Y',strtotime($booking_req['availability']['date'])) }} <br> @if($booking['pickup_time_from'] != $booking['pickup_time_to']){{ $booking['pickup_time_from'] }} - {{ $booking['pickup_time_to'] }} @else {{ $booking['pickup_time_from'] }} @endif</p>
                </div>
                <div class="col-md-3">
                  <p class="txt-purple mb-0">{{__('Start Location')}}</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">{{ (strlen($booking_req['availability']['from_address']) > 35 ? substr($booking_req['availability']['from_address'],0,35).'...' : $booking_req['availability']['from_address']) }}</p>
                </div>
                <div class="col-md-3">
                  <p class="txt-purple mb-0">{{__('End Location')}}</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">{{ (strlen($booking_req['availability']['to_address']) > 35 ? substr($booking_req['availability']['to_address'],0,35).'...' : $booking_req['availability']['to_address']) }}</p>
                </div>
                <div class="col-md-2">
                  <p class="txt-purple mb-0">{{__('Carrier Info')}}</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light"><a href="{{ url('shipper/carrier-info',$booking_req['driver']['id']) }}">{{ $booking_req['driver']['name'] }}</a></p>
                </div>
                <div class="col-md-2">
                  <p class="txt-purple mb-0">@if(isset($booking_req['is_bid']) && !empty($booking_req['is_bid']) && $booking_req['is_bid'] == 1) {{__('Bid Request Price')}}   @else {{__('Book Request Price')}} @endif </p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">€ {{ CommonHelper::price_format($booking_req['amount'],'GR') }}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      @endif

      @if(isset($booking_bid) && !empty($booking_bid))
      <?php 
      $first_key = array_key_first($booking_locations);
      $last_key = array_key_last($booking_locations);?>
        <hr style="border-top:1px solid #959393">
        <div class="row">
          <div class="col-md-15">
            <h3 class="container txt-black">{{__('Bid Requests')}} </h3>
          </div>
          <div class="col-md-2">
            <h4 style="position: absolute;">(Order Id #{{$booking['id']}})</h4>
          </div>
          <div class="col-md-8">
            <h4 class="d-flex align-items-center txt-purple font-18" style="">{{__('Bid Starting Price')}} :   <strong> &euro; {{ CommonHelper::price_format($booking['amount'],'GR') }}</strong></h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped dltrc" style="background-color: white;margin-top: 5vh;box-shadow: 0px 0px 10px rgb(114 114 113 / 50%);
    border-radius: 21px;">
              <thead>
                <th style="padding: 18px;">{{__('S.No.')}}</th>
                <th style="width: 20%;">{{__('Type of Carrier')}}</th>
                <th style="width: 22%;">{{__('Name')}}</th>
                <th style="width: 10%;">{{__('Rating')}}</th>
                <th style="width: 10%;">{{__('Offer Price')}}</th>
                <th></th>
              </thead>
              <tbody>
                @foreach($booking_bid as $k => $bid)
                  <tr>
                    <td class="txt-black" style="width: 1%;white-space: nowrap;padding: 18px;">{{$k+1}}</td>
                    <td class="txt-black">@if(isset($bid['driver']['type']) && !empty($bid['driver']['type']) && $bid['driver']['type'] == 'freelancer') Freelancer Driver @else Company @endif</td>
                    <td class="txt-black" style="display: flex;">
                      @if(isset($bid['driver']['profile']) && !empty($bid['driver']['profile']))
                        <img src="{{env('S3_IMAGE_URL')}}driver/{{$bid['driver']['profile']}}" class="img-rounded" style="border-radius: 8px; border-radius: 8px; flex: 1 1 auto; padding: 10px; height: 76px;"><h3 style="width: 75%;padding-top: 17px;text-decoration:underline;">{{ $bid['driver']['name'] }}</h3>
                      @else
                        <img src="{{url('images/default.png')}}" class="img-rounded " style="border-radius: 8px; flex: 1 1 auto; padding: 10px; height: 76px;"><h3 style="width: 75%;padding-top: 17px;text-decoration:underline;">{{ $bid['driver']['name'] }}</h3>
                      @endif
                    </td>
                    <td>
                      <h5><span class="fa fa-star checked" style="color: #9B51E0;"></span>3/5 <span style="text-decoration:underline;"> (400) </span></h5>
                    </td>
                    <td class="txt-black" >€ {{ CommonHelper::price_format($bid['amount'],'GR') }}</td>
                    <td>

                      <button class="btn btn-outline-deline bid_decline" data-id="{{$bid['id']}}" data-driver-id="{{$bid['driver_id']}}" data-booking-id="{{$bid['booking_id']}}" data-availability-id="{{ $bid['availability_id'] }}" style="border-radius: 11px;width: auto;font-weight: bold;">{{__('Decline')}}</button>

                      <button class="btn btn-outline-success bid_accept" data-id="{{$bid['id']}}" data-driver-id="{{$bid['driver_id']}}" data-booking-id="{{$bid['booking_id']}}" data-availability-id="{{ $bid['availability_id'] }}" style="border-radius: 11px;width: auto;margin-left: 15px;font-weight: bold;">{{__('Accept')}}</button>
                    </td>
                  </tr>        
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        @foreach($booking_bid as $bid)
        <!-- <div class="truck-box bg-white p-2 mb-3">
          <div class="row align-items-center">
            <div class="col-md-1 tr-br">
              <p class="mb-0 font-14 txt-blue font-weight-light"><em class="font-weight-bold">Truck Details</em><br> for @if(isset($booking['is_bid']) && !empty($booking['is_bid']) && $booking['is_bid'] == 1) bidding @else booking @endif</p>
            </div>
            <div class="col-md-11">
              <div class="row" style="margin-right:6px ">
                <div class="col-md-2">
                  <p class="txt-purple mb-0">Truck Type</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">{{ $bid['driver_vehicle']['truck_type']['name'] }}</p>
                </div>
                <div class="col-md-2">
                  <p class="txt-purple mb-0">Start Date</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">{{ date('d/m/Y',strtotime($booking['pickup_date'])) }} <br> @if($booking['pickup_time_from'] != $booking['pickup_time_to']){{ $booking['pickup_time_from'] }} - {{ $booking['pickup_time_to'] }} @else {{ $booking['pickup_time_from'] }} @endif</p>
                </div>
                <div class="col-md-2">
                  <p class="txt-purple mb-0">Start Location</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">{{ (strlen($booking_locations[$first_key]['drop_location']) > 35 ? substr($booking_locations[$first_key]['drop_location'],0,35).'...' : $booking_locations[$first_key]['drop_location']) }}</p>
                </div>
                <div class="col-md-2">
                  <p class="txt-purple mb-0">End Location</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">{{ (strlen($booking_locations[$last_key]['drop_location']) > 35 ? substr($booking_locations[$last_key]['drop_location'],0,35).'...' : $booking_locations[$last_key]['drop_location']) }}</p>
                </div>
                <div class="col-md-2">
                  <p class="txt-purple mb-0">Carrier Info</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light"><a href="{{ url('shipper/carrier-info',$bid['driver']) }}">{{ $bid['driver']['name'] }}</a></p>
                </div>
                <div class="col-md-1">
                  <p class="txt-purple mb-0">Bid Price</p><br>
                  <p class="mb-0 font-14 txt-blue font-weight-light">€ {{ $bid['amount'] }}</p>
                </div>
                <div class="col-md-1">
                  <p class="txt-purple mb-0"></p><br>
                  <form action="{{ url('shipper/manage-shipment/confirm-bidding') }}" method="POST">
                    @csrf
                    <button type="button" class="btn btn-primary btn-sm mr-2 mt-0 bid_accept" data-id="{{$bid['id']}}" data-driver-id="{{$bid['driver_id']}}" data-booking-id="{{$bid['booking_id']}}" data-availability-id="{{ $bid['availability_id'] }}"><span class="text ml-1">Accept</span></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        @endforeach
      @endif
    
      
    </div>
  </div>
</div>

  <div class="row" id="search_available_truck" style="display: none;">
    
    <div class="col-xl-12">
      <div class="card ">
        <div class="card-body table-responsive" >
          <table id="{{$dataTableId}}" class="table table-bordered table-striped" style="width: 100%">
            <thead>
              <tr>
                @foreach ($dateTableFields as $field)
                <th>
                  {{ $field['title'] }}
                </th>
                @endforeach
              </tr>
            </thead>
            <tbody>
              @if(isset($available_trucks) && !empty($available_trucks))
              @foreach($available_trucks as $trucks)
                <?php 
                $first_key = array_key_first($trucks['booking']['trucks']['locations']);
                $last_key = array_key_last($trucks['booking']['trucks']['locations']);?>
                <tr>
                  <td>{{ $trucks['id'] }}</td>
                  <td>{{ $trucks['trucks_type']['name'] }}</td>
                  <td>{{ date('d/m/Y',strtotime($trucks['booking']['pickup_date'])) }}</td>
                  <td title="{{ $trucks['booking']['trucks']['locations'][$first_key]['drop_location'] }}">{{ (strlen($trucks['booking']['trucks']['locations'][$first_key]['drop_location']) > 35 ? substr($trucks['booking']['trucks']['locations'][$first_key]['drop_location'],0,35).'...' : $trucks['booking']['trucks']['locations'][$first_key]['drop_location']) }}</td>
                  <td title="{{ $trucks['booking']['trucks']['locations'][$last_key]['drop_location'] }}">{{ (strlen($trucks['booking']['trucks']['locations'][$last_key]['drop_location']) > 35 ? substr($trucks['booking']['trucks']['locations'][$last_key]['drop_location'],0,35).'...' : $trucks['booking']['trucks']['locations'][$last_key]['drop_location']) }}</td>
                  <td>{{ $trucks['driver_name'] }}</td>
                  <td>€ {{ CommonHelper::price_format($trucks['bid_amount'],'GR') }}</td>
                  <td>@if($trucks['is_bid'] == '1') <a @if($trucks['booking']['is_bid'] == '0') href="{{ url('shipper/search-available-trucks/book-now/'.$trucks['booking']['id'].'/'.$trucks['id']) }}" @else href="{{ url('shipper/search-available-trucks/bid-now/'.$trucks['booking']['id'].'/'.$trucks['id']) }}" @endif style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> {{__('Bid')}}</a> @else <a href="{{ url('shipper/search-available-trucks/book-now/'.$trucks['booking']['id'].'/'.$trucks['id']) }}" style="color: #b27be7;font-weight:600;border: 1px solid #b27be7;border-radius:5px" class=" btn btn-xs text-default justify-content-end "> {{__('Book')}}</a> @endif</td>
                </tr>
              @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Note Details Shipment -->
<div class="modal fade note-shipment" id="note-shipment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content border-r35">
      <div class="modal-header">
        <p class="font-15 font600 txt-blue text-center shipment_title" style="margin-bottom:0px; word-break: break-all;padding-left: 34%;"></p> 
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
      <div class="modal-body p-4">
        <p class="font-18 font600 txt-blue text-center shipment_note" style="margin-bottom:0px; word-break: break-all;"></p>        
      </div>
    </div>
  </div>
</div>
<!-- view address -->
<div class="modal fade manage-order-shipment-view-address p-2" id="view-map" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal1250" role="document">
    <div class="modal-content border-r20">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
      <div class="modal-body p-3">
        <div class="row">
          <div class="col-md-12">
          <input type="hidden" id="longitud" value="">
      <input type="hidden" id="latitud" value="">
            <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;position: relative; overflow: hidden;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- view  Share button -->
<div class="modal fade float-right" id="share_email" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal1250" role="document">
    <div class="modal-content border-r20">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
      <div class="modal-body">
          @php
          $data=\App\Models\Booking::where('id',$booking['id'])->get();
        @endphp
        @if(isset($data) && !empty($data))
        @foreach($data as $key=>$value)
        <ul>
          @foreach (explode(',',$value->email_or_number) as $email)
          @if(isset($email) && !empty($email))
          <li>{{ $email }}</li>
          @endif
          @endforeach
        </ul>
        @endforeach
        @endif
        <form method="post" action="{{url('shipper/manage-shipment/share/email')}}" class="share_email_form">
          @csrf
          <input type="hidden" name="id" id="id" class="form-control id">
          <input type="email" name="email" class="form-control" required data-parsley-required-message="Please enter work email." value="{{old('email')}}" data-parsley-type-message="Please enter valid email address" maxlength="60"  data-parsley-trigger="keyup">
          <br>
          <button type="submit" class="btn btn-success">Share Email</button>
        </form>
      </div>
    </div>
  </div>
</div>
@if($booking['status'] == 'scheduled' || $booking['status'] == 'in-progress' || $booking['status'] == 'completed')
  @if(isset($booking_truck) && !empty($booking_truck)) 
    @foreach($booking_truck as $truck) 
      @if($truck['driver_details']['type'] == 'freelancer') 
        <div class="container-fluid"> 
          <div class="row mt-2" id="driver-details" >
            <div class="col-xl-10">
              <div>                
                <div class="row">
                   <div class="col-md-12">
                     <p class="mb-0 font-18 font500 txt-blue"><b>{{__('Driver Details')}} :</b> </p>
                     <div class="mt-2 chat-wrap d-flex align-items-center ml-2">
                       <div class="inline-fields">
                        @if(isset($truck['driver_details']['profile']) && !empty($truck['driver_details']['profile'])) 
                          <img src="{{ env('S3_IMAGE_URL').'driver/'.$truck['driver_details']['profile'] }}" class="img-rounded" style="border-radius: 10px;flex: 1 1 auto;padding: 10px;border: solid;">
                        @else
                          <img src="{{ url('images/default.png') }}" class="img-rounded" style="border-radius: 10px;flex: 1 1 auto;padding: 10px;border: solid;">
                        @endif
                    </div> 
                    <div class="ml-2">
                        <p class="font-16 font600 mb-0 txt-blue"> <span>{{ $truck['driver_details']['name'] }}</span>  </p>
                         <div class="stars">
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star" style="color: #c2acef;"></span>
                            <span class="fa fa-star" style="color: #c2acef;"></span>
                            <span>(400)</span> 
                         </div>                          
                           @if(isset($booking['schedule_time']) && !empty($booking['schedule_time']))
                            <?php
                              $current_time = time();
                              $diff = $current_time - $booking['schedule_time'];
                              $min_diff = 0;
                              if($diff > 0){
                                $min_diff = $diff/60;
                              }
                            ?>
                            @if(isset($min_diff) && !empty($min_diff) && $min_diff <= 30)                             
                                <button type="button" class="mt-2 btn btn-primary cancel_driver" data-booking-id="{{ $booking['id'] }}" data-driver-id="{{ $truck['driver_details']['id'] }}" style="border-radius: 10px;background: #ffffff;color: black;font-weight: 600;" >{{__("Cancel Driver")}}</button>                            
                            @endif
                          @endif                           
                            <a href="{{ url('shipper/chat-demo/'.$booking['id'].'?receiver_id='.$truck['driver_details']['id'])}}" class="btn btn-primary mt-2" style="border-radius: 10px;background: #ffffff;color: black;font-weight: 600;">{{__("Chat With Driver")}}</a>                          
                    </div> 
                 

                     </div>
                   </div>                   
                
              </div>
                 
              
              </div>
              </div>
            </div>
          </div>
        </div>
      @else
      <div class="container-fluid"> 
        <div class="row mt-3" id="driver-details" >
            
                
                  <div class="row mt-3">
                    <p class="mb-0 mr-2 font-18 font500 txt-blue"><b>{{__('Carrier and Driver Details')}} :</b> <span class="font-12"style="font-size: smaller;color: #797979;font-weight: 400;"> </span></p> 
                    <div class="col-md-10" style="margin: -52px 73px 109px 217px;">
                      <div class="inline-fields" style="display: inline-block;">
                        @if($user->profile == '')
                        <img src="{{ url('images/default.png') }}" class="img-rounded" style="border-radius: 10px;flex: 1 1 auto;padding: 10px;height: 61px;width: 71px;border: solid;margin: 13px;">
                        @else
                        <img src="{{ url('images/default.png') }}" class="img-rounded" style="border-radius: 10px;flex: 1 1 auto;padding: 10px;height: 61px;width: 71px;border: solid;margin: 13px;">
                        @endif
                      </div> 
                      <div class="" style="display: inline-block;">
                          <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center"><span>{{     $truck['driver_details']['name'] }}</span></p>
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star" style="color: #c2acef;"></span>
                            <span class="fa fa-star" style="color: #c2acef;"></span>
                            <span>(400)</span>
                      </div> 
                    </div>
                    <div class=".col-sm-4 .col-sm-push-8" style="display: inline-block;">
                        <button type="button" class="btn btn-primary mb-2" style="margin-left: 174%;border-radius: 10px;background: #ffffff;color: black;font-weight: 600;width: 100%;margin-top: -205px;" >{{__("Chat With Carrier")}}</button>
                    </div>
                
            <div style="margin-top: -16%;margin-left: 43%;">
                <p class="mb-0 mr-2 font-18 font500 txt-blue"> 
                  <div class="row mt-3">
                    <div class="col-md-10" style="margin: -15px 31px 25px -10px;">
                      <div class="inline-fields" style="display: inline-block;">
                         @if(isset($truck['driver_details']['profile']) && !empty($truck['driver_details']['profile'])) 
                            <img src="{{ env('S3_IMAGE_URL').'driver/'.$truck['driver_details']['profile'] }}" class="img-rounded" style="border-radius: 10px;flex: 1 1 auto;padding: 10px;height: 61px;width: 71px;border: solid;margin: 13px;">
                          @else
                            <img src="{{ url('images/default.png') }}" class="img-rounded" style="border-radius: 10px;flex: 1 1 auto;padding: 10px;height: 61px;width: 71px;border: solid;margin: 13px;">
                          @endif
                            
                      </div> 
                      <div class="" style="display: inline-block;">
                          <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center"> <span>{{ $truck['driver_details']['name'] }}</span>  </p>
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star checked" style="color: #9B51E0;"></span>
                            <span class="fa fa-star" style="color: #c2acef;"></span>
                            <span class="fa fa-star" style="color: #c2acef;"></span>
                            <span>(400)</span>
                      </div> 
                    </div>
                    <div class=".col-sm-4 .col-sm-push-8" style="display: inline-block;">
                        <button type="button" class="btn btn-primary mb-2" style="margin-left: 27%;border-radius: 10px;background: #ffffff;color: black;font-weight: 600;width: 100%;margin-top: -27px;" >{{__("Chat With Driver")}}</button>
                    </div>
                    <div class=".col-sm-4 .col-sm-push-8" style="display: inline-block;"> 
                    <button type="button" class="btn btn-primary mb-2 cancel_driver" data-booking-id="{{ $booking['id'] }}" data-driver-id="{{ $truck['driver_details']['id'] }}" style="margin-left: 105%;border-radius: 10px;background: #ffffff;color: black;font-weight: 600;width: 100%;margin-top: -182px;" >{{__("Cancel Driver")}}</button>
                  </div>
                </div>
                </p> 
            </div>
            </div>
        </div>
      </div>
      @endif
    @endforeach
  @endif 
@endif

<!-- pod model -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" id="pod_model" aria-hidden="true" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style=" top: 77px;">
            <div class="modal-header">
            <div class="row mx-auto" >
                <h3 style="margin-left: 190px;">Proof of Delivery</h3>
            </div>
            <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span></button>
          </div>
          <form  action="javascript:void(0)" id="date" style="margin-bottom: 1.5rem;">
            <div class="container-fluid">
              <div class="row">
                <div class="col">
                  <p class="breadcrumb-item active font-16" aria-current="page" style="margin: 2px;">#{{$booking['id']}}</p>
                  @if(isset($booking['pod_image']) && !empty($booking['pod_image']))
                    <img src="{{env('S3_IMAGE_URL')}}POD/{{$booking['pod_image']}}" style="width: 92%;"> 
                  @endif
                </div>
                <div class="col" style="margin-top: 16%;">
                  <h5 class="d-flex" style="margin-left: 52px;color: black;"> Save POD as</h5>
                    <div class="modal-body">         
                      <div class="radio-block">      
                        <div class="radio existing_class mb-1  text-left" style="padding-top: 0px;">
                            <input type="radio" name="radio2" id="radio2" value="radio2" checked>
                            <label for="radio2"><b class="booking_match_count"></b> {{__('PDF')}}</label>
                        </div>
                        <div class="radio create_class active mb-1  text-left" style="padding-top: 0px;">
                             <input type="radio" name="radio2" id="radio4" value="radio4" >
                             <label for="radio4"> {{__('Image')}}</label>
                        </div>
                        <div class="btn-block d-flex mt-3 text-center">
                          <!-- <a href='{{url("shipper/generate-pdf",$booking["id"] )}}' class="btn w-100 btn-primary" style="background-color: white;color: #1f1f41;border-color: #1f1f41;font-weight: 600;border-radius: 20px;">{{__('Download')}}</a> -->
                          <a href='{{url("shipper/generate-pdf",$booking["id"] )}}' class="btn w-100 btn-primary" id="pdf-down" style="background-color: white;color: #1f1f41;border-color: #1f1f41;font-weight: 600;border-radius: 20px;">{{__('PDF Download')}}</a>
                          <a href='{{url("shipper/generate-image",$booking["id"] )}}' class="btn w-100 btn-primary" id="img-down" style="background-color: white;color: #1f1f41;border-color: #1f1f41;font-weight: 600;border-radius: 20px;">{{__('Image Download')}}</a>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal fade cancel-shipment" id="payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content border-r35">
      <div class="col-md-12" style="top: 15px">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
      </div>
      <div class="modal-body p-4">
        <form action="{{ url('shipper/shipment/payment',$booking['id']) }}" name="payment_form" method="post" id="payment_form">
          @csrf
          <p class="font-18 font600 txt-blue text-center">Choose Payment Method</p>
          <input type="hidden" name="amount" value="{{ $booking['amount'] }}">
          <input type="hidden" name="driver_id" value="@if(isset($booking_truck) && !empty($booking_truck)) @foreach($booking_truck as $truck) @if(isset($truck['driver_details']['id']) && !empty($truck['driver_details']['id'])){{$truck['driver_details']['id']}}@endif @endforeach @endif">
          <div class="radio-block">
            <div class="radio create_class active mb-3  text-left">
                 <input type="radio" name="payment_type" id="payment_type" value="cash" checked>
                 <label for="cash"> Cash</label>
            </div>
            {{-- <div class="radio existing_class mb-3  text-left">
                <input type="radio" name="bid_request" id="bid_request2" value="show_existing">
                <label for="bid_request2"><b class="booking_match_count"></b> Matched Shipments Found</label>
            </div> --}}
          </div>
          <div class="btn-block d-flex mt-3 text-center">
              <button type="submit" class="btn w-100 btn-primary" style="border-radius: 15px">Proceed</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
     
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script>
var map;
    var pickup = `{{asset('shipper/images/pickup.png')}}`;
    var truck = `{{asset('shipper/images/truck.png')}}`;
    var driver_lat = `{{isset($driver_location->lat)?$driver_location->lat:""}}`;
    var driver_lng = `{{isset($driver_location->lng)?$driver_location->lng:""}}`;
function open_map(location_data)
  {
    var map;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    if(location_data != ''){
      var locations = location_data;
    }
    var markerOption = {
      clickable: false,
      flat: true,
      icon: pickup,
      visible: true,
      map: map
    };
    directionsDisplay = new google.maps.DirectionsRenderer({ markerOptions: markerOption });
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(location_data[0][1],location_data[0][2]),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });


        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true,
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

    directionsDisplay.setMap(map);

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else
            alert ('failed to get directions');
    });

   var latlng = new google.maps.LatLng(driver_lat, driver_lng);
   var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon:truck
    });
    marker.setMap(map);
    setMarker(map,marker);

    $('#view-map').modal('show');
  }

  function delete_notiflix(e) {
    
    var tr = $(e).parent().closest('tr');
    var id = $(e).data('id');
    var token = $(e).data('token');
    var url = $(e).data('url');
    var u1 = $(e).data('newurl');

    var product = $('#productid').val();

    console.log(token);
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to delete this record?',
        'Yes',
        'No',
        function() {
            Notiflix.Loading.Standard();
            $.ajax({
                url: base_url + '/' + u1 + '/' + product,
                
                type: 'post',
                dataType: "JSON",
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token,
                },
                success: function(returnData) {
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Deleted');
                    tr.remove();
                    $("#edit_model").modal("hide");
                    location.reload();

                }
            });
        });

}
$(document).ready(function(){
  $('.share_email_form').parsley();
  var table = $('#{{$dataTableId}}').DataTable({
      processing: true,
      responsive:true,
      searching:true,
      lengthChange:true,
      bInfo:true,
    });

  $(document).on('click','.bid_accept',function(){
    var booking_id = $(this).attr('data-booking-id');
    var id = $(this).attr('data-id');
    var driver_id = $(this).attr('data-driver-id');
    var availability_id = $(this).attr('data-availability-id');
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to accept this driver bid request?',
        'Yes',
        'No',
        function() {
          Notiflix.Loading.Standard();
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
              url: "{{ url('shipper/manage-shipment/accept_bid') }}",
              type: 'post',
              dataType: "JSON",
              data: {
                  "id": id,
                  "booking_id": booking_id,
                  "driver_id": driver_id,
                  "availability_id": availability_id,
              },
              success: function(returnData) {
                console.log(returnData);
                if(returnData.error){
                  Notiflix.Notify.Failure(returnData.message);
                }else{
                  Notiflix.Notify.Success(returnData.message);
                }
                  // Notiflix.Loading.Remove();
                  // Notiflix.Notify.Failure('Shipment assign successfully');
                  // tr.remove();
                  // $("#edit_model").modal("hide");
                  location.reload();
              }
          });
      });
  })
  
  $(document).on('click','.cancel_driver',function(){
    var booking_id = $(this).attr('data-booking-id');
    var driver_id = $(this).attr('data-driver-id');
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to cancel this driver?',
        'Yes',
        'No',
        function() {
          Notiflix.Loading.Standard();
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
              url: "{{ url('shipper/manage-shipment/cancel_driver') }}",
              type: 'post',
              dataType: "JSON",
              data: {
                  "booking_id": booking_id,
                  "driver_id": driver_id,
              },
              success: function(returnData) {
                if(returnData.error){
                  Notiflix.Notify.Failure(returnData.message);
                }else{
                  Notiflix.Notify.Success(returnData.message);
                }
                  location.reload();
              }
          });
      });
  })
  
  $(document).on('click','.cancel-request',function(){
    var cancel_id = $(this).attr('data-id');
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to accept this driver bid request?',
        'Yes',
        'No',
        function() {
          Notiflix.Loading.Standard();
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
              url: "{{ url('shipper/manage-shipment/cancel_request_accept') }}",
              type: 'post',
              dataType: "JSON",
              data: {
                  "cancel_id": cancel_id,
              },
              success: function(returnData) {
                  Notiflix.Notify.Success('Shipment cancel successfully');
                  location.reload();
              }
          });
      });
  })
  
  $(document).on('click','.bid_decline',function(){
    var booking_id = $(this).attr('data-booking-id');
    var id = $(this).attr('data-id');
    var driver_id = $(this).attr('data-driver-id');
    var availability_id = $(this).attr('data-availability-id');
    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to decline this driver bid request?',
        'Yes',
        'No',
        function() {
          Notiflix.Loading.Standard();
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
              url: "{{ url('shipper/manage-shipment/decline_bid') }}",
              type: 'post',
              dataType: "JSON",
              data: {
                  "id": id,
                  "booking_id": booking_id,
                  "driver_id": driver_id,
                  "availability_id": availability_id,
              },
              success: function(returnData) {
                  // Notiflix.Loading.Remove();
                  Notiflix.Notify.Success('Shipment decline successfully');
                  // tr.remove();
                  // $("#edit_model").modal("hide");
                  location.reload();
              }
          });
      });
  })

  $(document).on('click','.open_map',function(){
      var booking_id = $(this).attr('data-id');
      $.ajax({
        url: "{{ url('shipper/manage-shipment/load-map') }}",
        type: 'post',
        dataType: "JSON",
        data: {
            "id": booking_id,
            "_method": 'POST',
            "_token": $('name[csrf-token]').attr('content'),
        },

        success: function(data) {
          // console.log(data[0][1])
            open_map(data);
        }
    });
  });

  $(document).on('click','.cancel_shipment',function () {
    var booking_id = $(this).attr('data-id');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to remove this shipment?',
      'Yes',
      'No',
      function() {
        Notiflix.Loading.Standard();
        /*$(".se-pre-icon").show();*/
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
            url: "{{ route('shipper.manage-shipment.delete',$booking['id']) }}",
            type: 'DELETE',
            dataType: "JSON",
            data: {
                "booking_id": booking_id,
            },
            success: function(returnData) {
                // Notiflix.Loading.Remove();
                /*$(".se-pre-icon").hide();*/
                Notiflix.Notify.Success('Shipment remove successfully');
                // tr.remove();
                // $("#edit_model").modal("hide");
                window.location.href = "{{ url('shipper/manage-shipment') }}";
            }
        });
    });
  });
  
  $(document).on('click','.cancel_status_shipment',function () {
    var booking_id = $(this).attr('data-id');
    Notiflix.Confirm.Show(
      'Confirm',
      'Are you sure that you want to cancel this shipment?',
      'Yes',
      'No',
      function() {
        Notiflix.Loading.Standard();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
            url: "{{ url('shipper/manage-shipment/cancel_shipment') }}",
            type: 'POST',
            dataType: "JSON",
            data: {
                "booking_id": booking_id,
            },
            success: function(returnData) {
                // Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Shipment cancelled successfully');
                // tr.remove();
                // $("#edit_model").modal("hide");
                window.location.href = "{{ url('shipper/manage-shipment') }}";
            }
        });
    });
  });

  $(document).on('click','.search_available_truck',function() {
    $('#search_available_truck').show();
  });

  $(document).on('click','.note_section',function () {
    var note = $(this).attr('data-note');
    var title = $(this).attr('title'); 
    
    console.log(note);
    if(note != ''){
      $('.shipment_note').text(note);
      $('.shipment_title').text(title);
      $('#note-shipment').modal('show');
    }
  });

  $(document).on('click','.share_button',function(){
        $('#share_email').modal('show');
        var id =$(this).attr('data-id');
        $('.id').val(id);
     });
});
</script>
<script src="{{ URL::asset('shipper/js/tracking.js')}}"></script>
<script>
$(document).ready(function(){
  $("#img-down").hide();
  $("#radio2").click(function(){
    $("#img-down").hide();
    $("#pdf-down").show();
  });
  $("#radio4").click(function(){
    $("#pdf-down").hide();
    $("#img-down").show();
  });
});
</script>
@endsection