@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.sorting-btn{
    border-radius: 20px;
    /* margin: 0px 10px; */
    padding: 2px 20px;
}
.sort-button .active{
  background-color: #eadafd;
  color: #9B51E0 !important;
  -webkit-box-shadow : none;
  box-shadow : none;
}
.table td {
  border-left: none;
  border-right: none;
}
.table th {
  border-left: none;
  border-right: none;
}
.panding-status{
  background-color: rgba(213,105,105,0.14) !important;
  border: 1px solid #D56969;
  border-radius: 10px;
}

.in-process-status{
  background-color: rgba(105,163,213,0.14) !important;
  border: 1px solid #699ed5;
  border-radius: 10px;
}

.completed-status{
  background-color: rgba(117,213,105,0.14) !important;
  border: 1px solid #7fd569;
  border-radius: 10px;
}

.cancelled-status{
  background-color: rgba(27,54,78,0.14) !important;
  border: 1px solid #252a38;
  border-radius: 10px;
}

.created-status{
  background-color: rgb(105 213 198 / 14%) !important;
  border: 1px solid #69c6d5;
  border-radius: 10px;
}

.scheduled-status{
  background-color: #FAF3E5 !important;
  border: 1px solid #c9d569;
  border-radius: 10px;
}

table.dataTable thead > tr > th.sorting_asc, table.dataTable thead > tr > th.sorting_desc, table.dataTable thead > tr > th.sorting, table.dataTable thead > tr > td.sorting_asc, table.dataTable thead > tr > td.sorting_desc, table.dataTable thead > tr > td.sorting{
  padding-right: 0px !important 
}
.cardData {
  width: 170px;
  height: 210px;
  position: absolute;
  top: 20%;
  right:90%;
  z-index: 9;
  margin-top: 40px;
  display: none;
  border-radius:25px;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">{{__('Manage Shipments')}}</a></li>
            </ol>
        </nav>
    </div>
  </div>
  @include('admin.include.flash-message')
  
  <div class="row d-flex align-items-center sort-button">
    <div class="col-md-11" style="margin:7px 0px 25px 0px;">
      <button type="button" class="btn txt-blue sorting-btn @if(!isset($url_param) && empty($url_param)) active @endif filter_btn" data-value="">All</button>
      <!-- <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="create">Created</button> -->
      <button type="button" class="btn txt-blue sorting-btn @if(isset($url_param) && !empty($url_param) && $url_param == 'pending') active @endif filter_btn" data-value="pending">{{__('Pending')}}</button>
      <button type="button" class="btn txt-blue sorting-btn @if(isset($url_param) && !empty($url_param) && $url_param == 'scheduled') active @endif filter_btn" data-value="scheduled">{{__('Scheduled')}}</button>
      <button type="button" class="btn txt-blue sorting-btn @if(isset($url_param) && !empty($url_param) && $url_param == 'in-progress') active @endif filter_btn" data-value="in-progress">{{__('In-Progress')}}</button>
      <button type="button" class="btn txt-blue sorting-btn @if(isset($url_param) && !empty($url_param) && $url_param == 'completed') active @endif filter_btn" data-value="completed">{{__('Completed')}}</button>
      <button type="button" class="btn txt-blue sorting-btn @if(isset($url_param) && !empty($url_param) && $url_param == 'cancelled') active @endif filter_btn" data-value="cancelled">{{__('Cancelled')}}</button>
    </div>
     <div class="col-md-1">
      <div >
      <button class="btn btn-light waves-effect waves-light px-2 sort_by" style="font-size:1em;
      display:block;
      left:-60px;
      margin-top:5px;
      width:100px;margin-right:120px;margin-bottom: 20px;font-weight: bold;color:#1F1F41">Sort by   <img src="{{ URL::asset('/images/down-arrow.svg')}}" alt="" class="img-fluid" width="15"></button>
      </div>
      <div class="list-unstyled d-flex mb-0 scroll-container grey-scroll row text-center text-uppercase">
        <div class="cardData sortFunctionalityMenu">
          <div class="card " style="width: 170px;height: 210px;border: 2px solid #cccccc;border-radius:25px;">
            <div class="card-body">
              <div style="border-bottom: 1px solid #cccccc;padding-bottom:10px">
              <a href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="sorting_data" data-sorting="created_at" >Created Date</a><br>
              </div>
               <input type="hidden" name="" data-id="0" id="zero_value">
              <div style="margin-top: 15px;border-bottom: 1px solid #cccccc;padding-bottom:10px">
              <a href="javascript:;" style="padding-right: 35px; color: #222244;text-transform: capitalize;font-weight: bold" class="sorting_data" data-sorting="pickup_date">Start Date</a>
              </div>
              <div style="margin-top: 15px;border-bottom: 1px solid #cccccc;padding-bottom:10px">
              <a href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 1px;font-weight: bold" class="sorting_data" data-sorting="complete_date">Complete Date</a>
            </div>
               <div style="margin-top: 15px;padding-bottom:10px">
               <a href="#" style="color: #222244;text-transform: capitalize;padding-right: 40px;font-weight: bold" class="sorting_data_remain">Manager</a>
             </div>
            </div>
          </div>
        </div>
      </div>
      </div>
  </div>

  <div class="row">
    
    <div class="col-xl-12">
      <div class="card table-responsive">
        <div class="card-body" >
          @include('admin.include.table')
        </div>
      </div>
    </div>
  </div>
</div>

<!-- view address -->
<div class="modal fade manage-order-shipment-view-address p-2" id="view-map" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal1250" role="document">
    <div class="modal-content border-r20">
      <div class="modal-body p-3">
        <div class="row">
          <div class="col-md-12">
          <input type="hidden" id="longitud" value="">
      <input type="hidden" id="latitud" value="">
            <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;position: relative; overflow: hidden;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@include('shipper.include.table_script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script>
  // function initialize(pickup_address,pickup_lat,pickup_lng,drop_address,drop_lat,drop_lng) {
    
    
  // }
  // google.maps.event.addDomListener(window, "load", initialize);

  function open_map(location_data)
  {
    var map;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    if(location_data != ''){
      var locations = location_data;
    }
    console.log(location_data);
    directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(location_data[0][1],location_data[0][2]),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

    directionsDisplay.setMap(map);

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else
            alert ('failed to get directions');
    });

    $('#view-map').modal('show');
  }
  $(document).ready(function(){

    $(document).on('change','.change-status',function(){
      var booking_id = $(this).attr('data-id');
      var status = $(this).val();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to change the status?',
        'Yes',
        'No',
        function() {
            Notiflix.Loading.Standard();
            $.ajax({
                url: "{{ url('shipper/manage-shipment/change-status') }}",
                type: 'post',
                dataType: "JSON",

                data: {
                    "id": booking_id,
                    "status": status,
                    "_method": 'POST',
                    "_token": $('name[csrf-token]').attr('content'),
                },

                success: function(data) {
                   
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Status changed');
                    location.reload();
                }
            });
        });
    })

    $(document).on('click','.open_map',function(){
      var booking_id = $(this).attr('data-id');
      $.ajax({
        url: "{{ url('shipper/manage-shipment/load-map') }}",
        type: 'post',
        dataType: "JSON",
        data: {
            "id": booking_id,
            "_method": 'POST',
            "_token": $('name[csrf-token]').attr('content'),
        },

        success: function(data) {
          // console.log(data[0][1])
            open_map(data);
        }
    });
  });
     $('.sort_by').click(function(e){
       e.preventDefault();
       e.stopPropagation();
      $('.sortFunctionalityMenu').toggle();
    });
    $('body').click( function() {
        $('.sortFunctionalityMenu').hide();
    });
})
</script>
@endsection