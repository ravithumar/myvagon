<?php $i= 0;?>
@foreach($bookings as $booking)
    <?php $last_key = 0; $first_key = 0; if(isset($booking['booking_location']) && !empty($booking['booking_location'])) { $first_key = array_key_first($booking['booking_location']->toArray()); $last_key = array_key_last($booking['booking_location']->toArray()); }?>
    <div class="row border box-over bg-white @if($i == '0') active @endif pl-2 py-2 mt-3 mx-1 pr-0 rounded-lg booking_row" data-id="{{ $booking['id'] }}" data-from-company-name="{{ $booking['booking_location'][$first_key]['company_name'] }}" data-from-address-short="{{ strlen($booking['booking_location'][$first_key]['drop_location']) > 60 ? substr($booking['booking_location'][$first_key]['drop_location'],0,60)."..." : $booking['booking_location'][$first_key]['drop_location'] }}" data-to-address-short="{{ strlen($booking['booking_location'][$last_key]['drop_location']) > 60 ? substr($booking['booking_location'][$last_key]['drop_location'],0,60)."..." : $booking['booking_location'][$last_key]['drop_location'] }}" data-status="{{ $booking['status'] }}" data-from-address="{{ $booking['booking_location'][$first_key]['drop_location'] }}" data-to-address="{{ $booking['booking_location'][$last_key]['drop_location'] }}" data-from-lat="{{ $booking['booking_location'][$first_key]['drop_lat'] }}" data-from-lng="{{ $booking['booking_location'][$first_key]['drop_lng'] }}" data-to-lat="{{ $booking['booking_location'][$last_key]['drop_lat'] }}" data-to-lng="{{ $booking['booking_location'][$last_key]['drop_lng'] }}" data-from-position="1" data-to-position="2">
        <div class="col-md-8 col-sm-8 col-12">
            <div class="d-flex">
                <p class="font-18 font-weight-400">#{{ $booking['id'] }}</p>
                <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="font-16 pl-3 text-dark font-weight-bold text-decoration"><u>View Detail</u></a>
            </div>
        <div class="track-order-list">
                <ul class="list-unstyled mb-0 pl-3">
                <li class="completed from-location position-relative icon">
                    <h4 class="mt-0 mb-1">{{ $booking['booking_location'][0]['company_name'] }} <small class="text-muted font-16">{{ date('d-m-Y',strtotime($booking['booking_location'][0]['delivered_at'])) }}  @if(isset($booking['booking_location'][0]['delivered_at']) && !empty($booking['booking_location'][0]['delivered_at']) && $booking['booking_location'][0]['delivery_time_from'] == $booking['booking_location'][0]['delivery_time_to']) {{ $booking['booking_location'][0]['delivery_time_from'] }} @else {{ $booking['booking_location'][0]['delivery_time_from'] }} - {{ $booking['booking_location'][0]['delivery_time_to'] }} @endif</small> </h4>
                    <p class="text-dark">{{ strlen($booking['booking_location'][0]['drop_location']) > 40 ? substr($booking['booking_location'][0]['drop_location'],0,40)."..." : $booking['booking_location'][0]['drop_location'] }}</p>
                </li>
                <li class="completed to-location position-relative icon">
                    <h4 class="mt-0 mb-1">{{ $booking['booking_location'][$last_key]['company_name'] }} <small class="text-muted font-16">{{ date('d-m-Y',strtotime($booking['booking_location'][$last_key]['delivered_at'])) }}  @if(isset($booking['booking_location'][$last_key]['delivered_at']) && !empty($booking['booking_location'][$last_key]['delivered_at']) && $booking['booking_location'][$last_key]['delivery_time_from'] == $booking['booking_location'][$last_key]['delivery_time_to']) {{ $booking['booking_location'][$last_key]['delivery_time_from'] }} @else {{ $booking['booking_location'][$last_key]['delivery_time_from'] }} - {{ $booking['booking_location'][$last_key]['delivery_time_to'] }} @endif</small> </h4>
                    <p class="text-dark mb-0">{{ strlen($booking['booking_location'][$last_key]['drop_location']) > 40 ? substr($booking['booking_location'][$last_key]['drop_location'],0,40)."..." : $booking['booking_location'][$last_key]['drop_location'] }}</p>
                </li>
                
            </ul>
        </div>
        </div>
        <div class="col-md-4 col-sm-4 col-12 text-md-right text-left pr-0">
            @if(isset($booking['status']) && !empty($booking['status']))
                @if($booking['status'] == 'create')
                    <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-red text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending">Pending</a>
                @endif
                @if($booking['status'] == 'pending')
                    <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-red text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending">Pending</a>
                @endif
                @if($booking['status'] == 'scheduled')
                    <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-yellow text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending">Scheduled</a>
                @endif
                @if($booking['status'] == 'in-progress')
                    <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-blue text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending">In-Process</a>
                @endif 
                @if($booking['status'] == 'completed')
                    <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-green text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending">Completed</a>
                @endif
                @if($booking['status'] == 'cancelled')
                    <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-dark text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending">Cancelled</a>
                @endif 
            @endif
            {{-- <p class="mt-2 mb-0 text-decoration pr-2 text-primary"><u><b>Bid Requests</b>: 30</u></p> --}}
            @if($booking['status'] == 'scheduled' || $booking['status'] == 'in-progress' || $booking['status'] == 'completed')
                @if(isset($booking['booking_truck']['driver_details']) && !empty($booking['booking_truck']['driver_details']))
                    <p class="color-purple mt-2 mb-0 text-decoration pr-2"><b>Carrier: </b><u>{{ $booking['booking_truck']['driver_details']['name'] }}</u></p>
                @endif
            @endif
            <h3 class="color-dark-purple my-2 font-weight-bold pr-2">@if(!empty($manage_price) && isset($booking['amount']) && !empty($booking['amount'])) € {{ CommonHelper::price_format($booking['amount'],'GR') }} @else - @endif</h3>
            <p class="font-12 mb-0 pr-2">{{ date('M d, Y',strtotime($booking['pickup_date'])) }}</p>
        </div> 
    </div>
    <?php $i++;?>
@endforeach
            