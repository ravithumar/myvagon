@extends('shipper.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row align-items-center">
        <div class="col">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ URL::asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{__('More')}}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('shipper.manager-management.index') }}" class="align-items-center txt-blue">{{__('Manager Management')}}</a></li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">{{ __($dateTableTitle) }}</li>
             </ol>
          </nav>
        </div>
    </div>
	<div class="row d-flex align-items-center products" style="margin-bottom:20px">
        <div class="col-md-8 col-lg-6">
           <h3 class="txt-blue m-0">{{ __($dateTableTitle) }}</h3>
        </div>
        <div class="col-md-4 col-lg-6"></div>
     </div>
	@include('shipper.include.flash-message')

    <div class="row">
        <div class="col-lg-12 col-xl-12">
            <div class="card-box">
                <form method="post" enctype="multipart/form-data" action="{{ route('shipper.manager-management.update',$data->user_id) }}" id="add_manager">
                    @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">{{__('First Name')}}</label>
                                    <input type="text" name="first_name"  required value="{{ $data->first_name }}" class="form-control" id="firstname" placeholder="{{__('Enter First name')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lastname">{{__('Last Name')}}</label>
                                    <input type="text" name="last_name"  required value="{{ $data->last_name }}" class="form-control" id="lastname" placeholder="{{__('Enter Last name')}}">
                                </div>
                            </div>
                        </div> <!-- end row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">{{__('Work E-mail')}}</label>
                                    <input type="email" class="form-control" name="email" readonly value="{{ $data->email }}" placeholder="{{__('Enter Work E-mail')}}">
                                    <div id="emailerror" class="text-danger"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">{{__('Phone No')}}</label>
                                    <div class=" d-flex">
                                        <select name="country" class="form-control" style="width: 25%;padding: 0px;" parsley-trigger="change"  >
                                          @if(isset($countries))
                                              @foreach($countries as $country)
                                                <option value="{{ $country->id }}" selected>+ {{ $country->phonecode }}</option>
                                              @endforeach
                                          @endif
                                        </select>
                                        <input type="text" class="form-control" readonly name="phone" value="{{ $data->phone }}" onkeypress="return numberValidate(event)" placeholder="{{__('Enter Phone No')}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="fas fa-cog mr-1"></i> {{__('Manager Permissions')}} </h5>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="switch">
                                    <input type="checkbox" id="create_shipment" name="create_shipment" value="1" {{ isset($data->create_shipment) && !empty($data->create_shipment) && $data->create_shipment == '1' ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                                <label for="create_shipment">{{__('Create Shipment')}}</label>
                            </div>
                            <div class="col-md-3">
                                <label class="switch">
                                    <input type="checkbox" id="view_shipment" name="view_shipment" value="1" {{ isset($data->view_shipment) && !empty($data->view_shipment) && $data->view_shipment == '1' ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                                <label for="view_shipment">{{__('View Shipment')}}</label>
                            </div>
                            <div class="col-md-3">
                                <label class="switch">
                                    <input type="checkbox" id="cancel_shipment" name="cancel_shipment" value="1" {{ isset($data->cancel_shipment) && !empty($data->cancel_shipment) && $data->cancel_shipment == '1' ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                                <label for="cancel_shipment">{{__('Cancel Shipment')}}</label>
                            </div>
                            <div class="col-md-3">
                                <label class="switch">
                                    <input type="checkbox" id="view_products" name="view_products" value="1" {{ isset($data->view_products) && !empty($data->view_products) && $data->view_products == '1' ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                                <label for="view_products">{{__('Manage Products')}}</label>
                            </div>
                            <div class="col-md-3">
                                <label class="switch">
                                    <input type="checkbox" id="view_address" name="view_address" value="1" {{ isset($data->view_address) && !empty($data->view_address) && $data->view_address == '1' ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                                <label for="view_address">{{__('Manage Address')}}</label>
                            </div>
                            <div class="col-md-3">
                                <label class="switch">
                                    <input type="checkbox" id="search_driver" name="search_driver" value="1" {{ isset($data->search_driver) && !empty($data->search_driver) && $data->search_driver == '1' ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                                <label for="search_driver">{{__('Search Driver')}}</label>
                            </div>
                            <div class="col-md-3">
                                <label class="switch">
                                    <input type="checkbox" id="manage_price" name="manage_price" value="1" {{ isset($data->manage_price) && !empty($data->manage_price) && $data->manage_price == '1' ? 'checked' : '' }}>
                                    <span></span>
                                </label>
                                <label for="manage_price">{{__('Manage Price')}}</label>
                            </div>                            
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-2 submit_form"><i class="mdi mdi-content-save"></i> {{__('Update')}}</button>
                        </div>
                    </form>
                    <!-- end settings content-->
                </div> <!-- end tab-content -->
            </div> <!-- end card-box-->
        </div>
    </div>
</div>
@endsection
@section('script')
@endsection