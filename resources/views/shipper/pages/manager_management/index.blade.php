@extends('shipper.layouts.master')

@section('content')
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ URL::asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">{{__('More')}}</a></li>
            <li class="breadcrumb-item active txt-blue" aria-current="page">{{ __($dateTableTitle) }}</li>
         </ol>
      </nav>
    </div>
  </div>
  <div class="row d-flex align-items-center products">
    <div class="col-md-8 col-lg-6">
       <h3 class="txt-blue m-0">{{ __($dateTableTitle) }}</h3>
    </div>
    <div class="col-md-4 col-lg-6">
      <div class="btn-group float-right mt-2 mb-2">
        <a href="{{ route('shipper.manager-management.create') }}" class="btn btn-primary waves-effect waves-light px-2">
          <span class="btn-label mr-1 m-0 p-0">
              <i class="fa fa-plus"></i>
          </span>
          {{__('Add New Manager')}}
        </a>
      </div>
    </div>
 </div>
  @include('shipper.include.flash-message')

  <div class="row">
    <div class="col-md-12">
      
    </div>
  </div>
  <div class="row">
    <div class="col-xl-12">
      <div class="card table-responsive">
        <div class="card-body" >
          @include('shipper.include.table')
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@include('shipper.include.table_script')
@endsection