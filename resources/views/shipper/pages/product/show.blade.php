@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
#map {
  width: 100%;
  height: 100vh;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="{{ url('dashboard') }}" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">{{__("Dashboard")}}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('shipper/product') }}" class="align-items-center"> {{__("Product Master")}}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__("View Product")}}</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>
  {{-- @include('admin.include.flash-message') --}}

  <div class="row d-flex align-items-center products manage-shipments">
    <div class="col-md-6">
      <h3>{{__("Dashboard")}}View Product </h3>
    </div>
    <div class="col-md-6">
      <ul class="d-flex align-items-center no-bids-search justify-content-end">
        <li>
          <a href="{{ route('shipper.product.edit',$product['id']) }}" class="cancel p-1 border-r6"><img src="{{ url('shipper/images/pen.svg') }}" alt="" class="img-fluid"></a>
        </li>
        <li>
          <a href="javascript:void(0)" data-token="{{ csrf_token() }}" class="cancel p-1 border-r6 delete_product" data-id="{{ $product['id'] }}"><img src="{{ url('shipper/images/delete.svg') }}" alt="" class="img-fluid"></a>
        </li>
      </ul>
    </div>
  </div>
  <div class="row">
    
    <div class="col-xl-12">
      <div class="card">
        <div class="card-body table-responsive" >
          <div class="form-group mb-3">
            <label for="name" class="mb-2 text-grey font-14">{{__("Name")}}</label>
            <input type="text" class="form-control form-control-without-border" placeholder="{{__('Name')}}" value="{{ $product['name'] }}" readonly>
          </div>
  
          <div class="form-group mb-3">
            <label for="first-name" class="mb-2 text-grey font-14">{{__("SKU Type")}}</label>
            <input type="text" class="form-control form-control-without-border" value="{{ $product['sku_type'] }}" placeholder="{{__('Sku Type')}}" readonly>
          </div>

            <div class="form-group mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('SKU No')}}</label>
              <input type="text" class="form-control form-control-without-border" value="{{ $product['sku_no'] }}" placeholder="{{__('SKU No')}}" readonly>
            </div>
  
           
            <div class="btn-block d-flex">
              <a href="{{ route('shipper.product.index') }}" class="btn btn-white mr-2" ><button type="button" class="btn w-100 btn-primary ">{{__('Cancel')}}</button></a>
            </div>
            
          {{-- </form> --}}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

      $(document).ready(function(){
        $(document).on('click','.delete_product',function (e) {
          var id = $(e).data('id');        
          var token = $(e).data('token');
          Notiflix.Confirm.Show(
          'Confirm',
          'Are you sure that you want to delete this record?',
          'Yes',
          'No',
          function() {
              Notiflix.Loading.Standard();
              $.ajax({
                  url: "{{ url('shipper/product/delete',$product['id']) }}",
                  
                  type: 'post',
                  dataType: "JSON",
                  data: {
                      "id": id,
                      "_method": 'DELETE',
                      "_token": token,
                  },
                  success: function(returnData) {
                    console.log(returnData);
                    if(returnData.success){
                      Notiflix.Loading.Remove();
                      Notiflix.Notify.Success(returnData.success);
                      window.location.href = "{{ url('shipper/product') }}";
                    }else if(returnData.error){
                      Notiflix.Loading.Remove();
                      Notiflix.Notify.Failure(returnData.error);
                    }else{
                      Notiflix.Loading.Remove();
                      Notiflix.Notify.Failure('Some thing went wrong');
                    }
                  }
              });
          });
        });
      });
</script>
@endsection