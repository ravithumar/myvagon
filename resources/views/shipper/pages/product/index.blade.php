@extends('shipper.layouts.master')
@section('content')

<div class="container-fluid">
 
  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">{{__('Dashboard')}}</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__('Product Master')}}</li>
        </ol>
      </nav>
    </div>

    <div class="col-md-6"></div>
  </div>
   @include('admin.include.flash-message')
  <div class="row d-flex align-items-center products">
    <div class="col-md-6">
      <h3>{{__('Product Master')}}</h3>
      <form method="post" action="{{url('shipper/product/import')}}" enctype="multipart/form-data" class="import_file d-flex align-items-center mb-3 mb-lg-0">
        @csrf
        <input type="file" name="file" class="form-control h-100" required data-parsley-fileextension='xls,csv' data-parsley-errors-container="#file_error_message"/>
         <div id="file_error_message"></div>
        <br>
        <button class="btn btn-success">Import</button>
      </form>
    </div>
    <div class="col-md-6">
      <!-- <a href="javascript:;" class="btn btn-primary float-right" data-toggle="modal" data-target="#view-details"> -->
      <a href="{{ route('shipper.product.create') }}" class="btn btn-primary float-right">
        <img src="images/assets/dashbord/plus.svg" alt="chat" class="img-fluid">
        <span class="text ml-1">{{__('Add Product')}}</span>
      </a>
    </div>
  </div>
  <div class="row">
    
    <div class="col-xl-12">
      <div class="card mt-2 table-responsive">
        <div class="card-body" >
          @include('admin.include.table')
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function(){
    window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
            var tagslistarr = requirement.split(',');
            var fileExtension = value.split('.').pop();
            var arr=[];
            $.each(tagslistarr,function(i,val){
               arr.push(val);
            });
            if(jQuery.inArray(fileExtension, arr)!='-1') {
              console.log("is in array");
              return true;
            } else {
              console.log("is NOT in array");
              return false;
            }
        }, 32)
        .addMessage('en', 'fileextension', 'Please enter only excel and csv file.');
    $('.import_file').parsley();
  })
</script>
@include('shipper.include.table_script')


<!-- custome model -->
<div class="modal fade" id="view-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">{{__('Product')}}</div>
        </div>
        <form action="{{ route('shipper.product.submit') }}" method="POST">
          @csrf
          <div class="form-group mb-4">
            <label for="name" class="mb-2 text-grey font-14">{{__('Product Name')}}</label>
            <input type="text" id="name" maxlength="40" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="{{__('Enter Product Name')}}" value="{{ old('name') }}">
            @error('name')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group mb-4">
            <label for="first-name" class="mb-2 text-grey font-14">{{__('SKU Type')}}</label>
            <input type="text" id="sku_type" name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ old('sku_type') }}" placeholder="{{__('Enter SKU Type')}}">
            @error('sku_type')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group mb-4">
            <label for="sku-no" class="mb-2 text-grey font-14">{{__('SKU No')}}</label>
            <input type="text" id="sku-no" name="sku_no" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ old('sku_no') }}" placeholder="{{__('Enter SKU No')}}">
            @error('sku_no')
            <div class="error">{{ $message }}</div>
            @enderror
          </div>
          <div class="btn-block d-flex">
            <button type="button" class="btn w-100 btn-white mr-2" data-dismiss="modal" aria-label="Close">{{__('Cancel')}}</button>
            <button type="submit" class="btn w-100 btn-primary">{{__('Save')}}</button>
          </div>
          
        </form>
        
      </div>
    </div>
  </div>
</div>



<div class="modal fade add-more-products" id="edit_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">View Product Details</div>
          <div class="icon-block d-flex">
           <!--  <a href="javascript:;" class="" data-toggle="modal" data-target="#delete" style="font-size: 19px !important;"> -->
              <!-- <img src="images/assets/dashbord/delete.svg" class="img-fluid"> -->
             <!-- <i class="fa fa-trash" style="font-size: 19px !important;color:#1f1f41!important;"></i>
            </a> -->

            <a href="javascript:;" class="" id="button-id" ata-popup="tooltip" 
              onclick="delete_notiflix(this);return false;"  data-newurl="product/delete" data-url="/delete" data-token="{{ csrf_token() }}">
              <i class="fa fa-trash" style="font-size: 19px !important;color:#1f1f41!important;"></i>
            </a>
            
          </div>
        </div>
        <input type="hidden" name="productid" value="" id="productid">

        <div class="form-group mb-4">
          <label for="name" class="mb-2 text-grey font-14">Product Name</label>
          <input readonly type="text" id="name_view"  name="name" class="form-control form-control-without-border" >
          
        </div>
        <div class="form-group mb-4">
          <label for="first-name" class="mb-2 text-grey font-14">SKU Type</label>
          <input readonly type="text" id="sku_type_view" name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" >
          
        </div>
        <div class="form-group mb-4">
          <label for="first-name" class="mb-2 text-grey font-14">SKU No</label>
          <input readonly type="text" id="sku_number_view" name="sku_number" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" >
          
        </div>
        <div class="btn-block d-flex">
          <button type="button" class="btn btn-white w-100" data-dismiss="modal" aria-label="Close">Cancel</button>
        </div>
        
        
        
      </div>
    </div>
  </div>
</div>



<div class="modal fade add-more-products" id="update_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="edits mb-2 d-flex align-items-center justify-content-between">
          <div class="title font-20 text-left">Edit Product Details</div>
          
        </div>
        <form id="form-sub"  method="POST">
          @csrf
          
          <div class="form-group mb-4">
            <label for="name" class="mb-2 text-grey font-14">Product Name</label>
            <input  type="text" id="name_edit"  maxlength="40" parsley-trigger="change" required  name="name" class="form-control form-control-without-border" >
            
          </div>
          <div class="form-group mb-4">
            <label for="first-name" class="mb-2 text-grey font-14">SKU Type</label>
            <input  type="text" id="sku_type_edit" maxlength="40" parsley-trigger="change"  name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" >
          </div>
          <div class="form-group mb-4">
            <label for="sku_number_edit" class="mb-2 text-grey font-14">SKU No</label>
            <input  type="text" maxlength="40" parsley-trigger="change"  id="sku_number_edit" name="sku_no" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" >
          </div>
          <div class="btn-block d-flex">
            <button type="button" class="btn btn-white w-100 mr-2" data-dismiss="modal" aria-label="Close">Cancel</button>
            <button type="submit" class="btn btn-primary w-100">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade delete-this" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="title font-20 text-center">Do you want to continue to <span class="red">delete</span> permanently?</div>
        <form action="{{ route('shipper.product.delete', 0) }}" method="POST">
          @csrf
          <input type="hidden" name="id" id="product_data_id">
          <div class="btn-block d-flex mt-3">
            <button type="submit" class="btn btn-white w-100">Yes</button>
            <button type="button" class="btn btn-primary w-100" data-dismiss="modal" aria-label="Close">No </button>
          </div>
        </form>
        
      </div>
    </div>
  </div>
</div>
@endsection