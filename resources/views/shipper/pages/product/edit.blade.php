@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
#map {
  width: 100%;
  height: 100vh;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="{{ url('shipper/dashboard') }}" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/material-dashboard.svg')}}" class="img-fluid">{{__('Dashboard')}}</a></li>
          <li class="breadcrumb-item"><a href="{{ url('shipper/product') }}" class="align-items-center">{{__('Product Master')}} </a></li>
          <li class="breadcrumb-item active" aria-current="page">{{__('Edit Product')}}</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>

  <div class="row d-flex align-items-center products">
    <div class="col-md-6">
      <h3>{{__('Edit Product')}} </h3>
    </div>
    <div class="col-md-6">
      
    </div>
  </div>
  <div class="row">
    
    <div class="col-xl-12">
      <div class="card">
        <div class="card-body table-responsive" >
          <form action="{{ url('shipper/product/data',$product['id']) }}" method="POST">
            @csrf
            <div class="form-group mb-3">
              <label for="name" class="mb-2 text-grey font-14">{{__('Product Name')}}</label>
              <input type="text" id="name" maxlength="40" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="{{__('Enter Product Name')}}" value="{{ $product['name'] }}">
              @if($errors->has('name'))
                  <div class="error text-danger">{{ $errors->first('name') }}</div>
              @endif
            </div>

            <div class="form-group mb-3">
              <label for="first-name" class="mb-2 text-grey font-14">{{__('SKU Type')}}</label>
              <input type="text" id="sku_type" name="sku_type" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ $product['sku_type'] }}" placeholder="{{__('Enter SKU Type')}}">
              @if($errors->has('sku_type'))
                  <div class="error text-danger">{{ $errors->first('sku_type') }}</div>
              @endif
            </div>
            <div class="form-group mb-3">
              <label for="sku-no" class="mb-2 text-grey font-14">{{__('SKU No')}}</label>
              <input type="text" id="sku-no" name="sku_no" maxlength="40" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ $product['sku_no'] }}" placeholder="{{__('Enter SKU No')}}">
                
                @if($errors->has('sku_no'))
                  <div class="error text-danger">{{ $errors->first('sku_no') }}</div>
                @endif
            </div>
           
            <div class="btn-block d-flex">
              <a href="{{ route('shipper.product.index') }}" class="btn btn-white mr-2" >{{__('Cancel')}}</a>
              <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
            </div>
            
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  $(document).ready(function(){
  })
     
</script>
@endsection