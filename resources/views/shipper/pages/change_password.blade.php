@extends('shipper.layouts.master')
@section('content')
<style>
    .icon-position{
        position: absolute;
        right: 25px;
        top: 10px;
        left: auto;
    }
    input[type="password"]::placeholder {
        
        /* Firefox, Chrome, Opera */
        color: #1F1F41;
        font-weight: bold
    }
</style>
<div class="container-fluid">
    @include('admin.include.flash-message')
	<div class="row align-items-center">
		<div class="col-md-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                   <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue" style="font-size: 20px">{{__('Change Password')}}</a></li>
                </ol>
            </nav>
		</div>
	</div>
    <form name="my_profile" action="{{ url('shipper/update-password') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div class="form-group mb-4">
                    <input type="password" id="old_password" name="old_password" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('Old Password')}}" value="{{ old('old_password') }}" data-parsley-trigger="keyup" data-parsley-required-message="Please enter old password." >
                    <a href="javascript:void(0)" style="color: #1f1f41"><i class="fa fa-eye-slash icon-position" id="showOldpass" ></i></a>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div class="form-group mb-4">
                    <input type="password" id="new_password" name="new_password" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('New Password')}}" value="{{ old('new_password') }}" data-parsley-trigger="keyup" data-parsley-required-message="Please enter new password.">
                    <a href="javascript:void(0)" style="color: #1f1f41"><i class="fa fa-eye-slash icon-position" id="showNewpass"></i></a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <div class="form-group mb-4">
                    <input type="password" id="confirm_password" name="confirm_password" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('Confirm Password')}}" value="{{ old('confirm_password') }}" data-parsley-equalto="#new_password" data-parsley-equalto-message="Password and Confirm Password must be same" data-parsley-trigger="keyup" data-parsley-required-message="Please confirm the password.">
                    <a href="javascript:void(0)" style="color: #1f1f41"><i class="fa fa-eye-slash icon-position" id="showConfirmpass"></i></a>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-11">
                <button type="submit" id="cust-button" class="btn font-10 mt-3 btn-primary cust-button" style="width: 350px;height: 50px;border-radius:25px">{{__('Change Password')}}</button>
            </div>
        </div>

    </form>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#showOldpass').on('click', function(){
            var passInput=$("#old_password");
            if(passInput.attr('type')==='password')
            {
                $(this).removeClass('fa-eye-slash');
                $(this).addClass('fa-eye');
                passInput.attr('type','text');
            }else{
                $(this).addClass('fa-eye-slash');
                $(this).removeClass('fa-eye');
                passInput.attr('type','password');
            }
        })

        $('#showNewpass').on('click', function(){
            var passInput=$("#new_password");
            if(passInput.attr('type')==='password')
            {
                $(this).removeClass('fa-eye-slash');
                $(this).addClass('fa-eye');
                passInput.attr('type','text');
            }else{
                $(this).addClass('fa-eye-slash');
                $(this).removeClass('fa-eye');
                passInput.attr('type','password');
            }
        })

        $('#showConfirmpass').on('click', function(){
            var passInput=$("#confirm_password");
            if(passInput.attr('type')==='password')
            {
                $(this).removeClass('fa-eye-slash');
                $(this).addClass('fa-eye');
                passInput.attr('type','text');
            }else{
                $(this).addClass('fa-eye-slash');
                $(this).removeClass('fa-eye');
                passInput.attr('type','password');
            }
        })
    })
</script>
@endsection