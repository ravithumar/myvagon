<?php $notifications = CommonHelper::getNotification(Auth::user()->id);?>

@extends('shipper.layouts.master')

@section('content')
 
<style type="text/css">
    .hidden.sm\:flex-1.sm\:flex.sm\:items-center.sm\:justify-between {
    display: none;
}
.hidden.sm\:flex-1.sm\:flex.sm\:items-center.sm\:justify-between {
    width: 100%;
    margin: 26px;
}
</style>

<div class="container-fluid list-wrapper">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title"></h4>
            </div>
        </div>
        
        <div class="col-12 list-wrapper ">
            <div class="card-box" style="margin: 1%;height: 101%;">
                <div class="mt-3">
                    <ul class="message-list ">
                        @if(isset($notifications) && !empty($notifications))
                            @foreach($notifications as $notification)
                                <li class="unread list-item">
                                    <div class="col-mail col-mail-1">
                                        <span class="star-toggle far fa-bell text-warning"></span>
                                        <a href="" class="title">{{ $notification['title'] }}</a>
                                    </div>
                                    <div class="col-mail col-mail-2">
                                        <a href="" class="subject" style="font-weight: 100;">{{ $notification['message'] }}
                                        </a>
                                        <div class="date" style="width: 147px;"> 
                                            {{\Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <span class="text-center txt-blue">No Notification Found</span>
                        @endif
                        
                    </ul>
                    <div class="pagination pull-right">
                            {{$notifications->links() }}
                    </div>
                </div>
            </div> 
        </div> 
    </div>
</div>
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script> -->

<script type="text/javascript">
    // var items = $(".list-wrapper .list-item");
 //    var numItems = items.length;
 //    var perPage = 10;

    // items.slice(perPage).hide();

    // $('#pagination-container').pagination(10,{
    //     items: numItems,
    //     itemsOnPage: perPage,
    //     prevText: "&laquo;",
    //     nextText: "&laquo;",
    //     onPageClick: function (pageNumber) {
    //         var showFrom = perPage * (pageNumber - 1);
    //         var showTo = showFrom + perPage;
    //         items.hide().slice(showFrom, showTo);
    //     }
    // });

</script>
@endsection
