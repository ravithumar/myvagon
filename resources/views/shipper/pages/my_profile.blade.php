@extends('shipper.layouts.master')
@section('content')
<div class="container-fluid">
    @include('admin.include.flash-message')
	<div class="row align-items-center">
		<div class="col-md-6">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb m-0">
                   <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue">{{__('My Profile')}}</a></li>
                </ol>
            </nav>
		</div>
	</div>
    <form name="my_profile" action="{{ url('shipper/update-profile') }}" method="post" enctype="multipart/form-data" class="my_profile">
        @csrf
        
        <div class="row">

            <div class="col-md-2"><span style="color: #1F1F41">{{__('Personal Details')}}</span></div>
                
            <div class="col-md-3">
                <div class="form-group mb-4">
                    <input type="text" id="first_name" name="first_name" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('First Name')}}" value="{{ $user_data['first_name'] }}" minlength="2" maxlength="70" data-parsley-required-message="Please enter first name." required data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in first name." data-parsley-trigger="keyup">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group mb-4">
                    <input type="text" id="last_name" name="last_name" required="" minlength="2" maxlength="70" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('Last Name')}}" value="{{ $user_data['last_name'] }}" data-parsley-required-message="Please enter last name." data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in last name." data-parsley-trigger="keyup" >
                </div>
            </div>
            <div class="col-md-3" style="margin-top: 21px;">
               <label for="useremail" >{{__('Browse')}}</label>
                <input type="file" onchange="previewFile(this);"   class="fa fa-pencil-square-o"name="profile" id="name" style="margin-left: 55px;" data-parsley-fileextension='jpeg,png,jpg,svg,JPG,JPEG,PNG,SVG' data-parsley-errors-container="#id_proof_error_message" data-parsley-max-file-size="50"/>
                <img id="previewImg" @if($user_data->profile != "") src="{{ env('S3_IMAGE_URL')}}shipper/{{ $user_data->profile }}" style="height: 100px;width: 174px;display: block;margin-left: 69px;margin-top: -85px;" @else src="{{ url('images/default.png') }}"  alt="Placeholder" style="height: 100px;width: 174px;display: block;margin-left: 69px;margin-top: -85px;"  @endif>
                <br>
                <div id="id_proof_error_message"></div> 
            </div>
            
                
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-6">
                <div class="form-group mb-4" >
                    <input type="email" id="email" name="email" maxlength="60" data-parsley-required-message="Please enter email." data-parsley-type-message="Please enter a valid email" data-parsley-trigger="keyup" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('Email')}}" value="{{ $user_data['email'] }}"  style="margin-top: -78px;" 
                    disabled>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-6">
                <div class="form-group mb-4">

                    <input type="text" id="phone" name="phone" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('Phone')}}" value="{{ $user_data['phone'] }}" disabled>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-2"><span style="color: #1F1F41">{{__('Company Details')}}</span></div>
            <div class="col-md-6">
                <div class="form-group mb-4">
                    <input type="text" id="company_name" name="company_name" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('Company Name')}}" value="{{ $user_data['company_name'] }}" data-parsley-required-message="Please enter company name" >
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-3">
                <div class="form-group mb-4">
                    {{-- <input type="text" id="country" name="country" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="Country" value=""> --}}
                    <select class="form-control form-control-without-border country" required name="country" style="background: none;">
                        <option>Select Country</option>
                        @if(isset($countries) && !empty($countries))
                            @foreach($countries as $country)
                                <option value="{{ $country['id'] }}" {{ $user_data['country'] == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group mb-4">
                    {{-- <input type="text" id="state" name="state" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('State')}}" value=""> --}}
                    <select class="form-control form-control-without-border state" required name="state" style="background: none;">
                        <option>{{__('Select State')}}</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-3">
                <div class="form-group mb-4">
                    {{-- <input type="text" id="city" name="city" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="City" value=""> --}}
                    <select class="form-control form-control-without-border city" required name="city" style="background: none;">
                        <option>{{__('Select City')}}</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group mb-4">
                    <input type="text" id="post_code" name="post_code" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('Post Code')}}" value="{{ $user_data['postcode'] }}">
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-6">
                <div class="form-group mb-4">
                    <input type="text" id="ssn_number" name="ssn_number" required="" class="form-control form-control-without-border pb-2" style="background: none;" placeholder="{{__('SSN Number')}}" value="{{ $user_data['ssn'] }}">
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
                <div class="form-group">
                    <select  style="padding: 2px !important;margin-left: 10px;" name="country" class="form-control" parsley-trigger="change" value="{{ old('country') }}" >
                        @if(isset($countries))
                            @foreach($countries as $country)
                              <option value="{{ $country->id }}" selected>+ {{ $country->phonecode }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>

            <div class="col-md-6">

                <div class="form-group mb-4">

                    <input type="text" id="phone" name="phone" required="" maxlength="15"  class="form-control form-control-without-border pb-2" style="background: none;width: 90%;" placeholder="{{__('Phone Number')}}" value="{{ $user_data['phone'] }}">
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>

        <div class="row">
            <div class="col-md-10 text-center">
                <button type="submit" id="cust-button" class="btn font-10 mt-3 btn-primary cust-button" style="width: 350px;height: 50px;border-radius: 50px" data-toggle="modal" data-target="#edit-shipment">{{__('Submit')}}</button>
            </div>
        </div>

    </form>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        // get_state()
        <?php if(isset($user_data['country']) && !empty($user_data['country']) && $user_data['country'] == '85'){?>
            var country_id = '<?=$user_data['country']?>';
            var state_id = '<?=$user_data['state']?>';
            get_state(country_id,state_id);
        <?php } ?>

        <?php if(isset($user_data['state']) && !empty($user_data['state'])){?>
            var state_id = '<?=$user_data['state']?>';
            var city_id = '<?=$user_data['city']?>';
            get_city(state_id,city_id);
        <?php } ?>
        window.ParsleyValidator
        .addValidator('fileextension', function (value, requirement) {
                var tagslistarr = requirement.split(',');
            var fileExtension = value.split('.').pop();
                        var arr=[];
                        $.each(tagslistarr,function(i,val){
                         arr.push(val);
                        });
            if(jQuery.inArray(fileExtension, arr)!='-1') {
              console.log("is in array");
              return true;
            } else {
              console.log("is NOT in array");
              return false;
            }
        }, 32)
        .addMessage('en', 'fileextension', 'Please select valid profile picture');
        window.Parsley.addValidator('maxFileSize', {
  validateString: function(_value, maxSize, parsleyInstance) {
    if (!window.FormData) {
      alert('You are making all developpers in the world cringe. Upgrade your browser!');
      return true;
    }
    var files = parsleyInstance.$element[0].files;
    return files.length != 1  || files[0].size <= maxSize * 51200;
  },
  requirementType: 'integer',
  messages: {
    en: 'Maximum file size should be  %smb',
    fr: "Ce fichier est plus grand que %s Kb."
  }
});
        $('.my_profile').parsley();
    });
</script>
@endsection