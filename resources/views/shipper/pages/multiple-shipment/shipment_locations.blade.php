@extends('shipper.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
</style>
    <div class="container-fluid">
             <div class="row align-items-center">
                 <div class="col">
                       <nav aria-label="breadcrumb">
               <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">Create Shipment</a></li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">Truck Type</li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">Commodity Information</li>
                <li class="breadcrumb-item active txt-blue" aria-current="page">Pickup &amp; Delivery Information</li>
               </ol>
           </nav>
                 </div>
             </div>
        <div class="row d-flex align-items-center products">
            <div class="col-md-8 col-lg-6">
            <h3 class="txt-blue">Pickup &amp; Delivery Information</h3>
            <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p>
            </div>
            <div class="col-md-4 col-lg-5"></div>
        </div>
     <div class="pickup-content">
      <div class="row">
        <div class="col">
                @php
                    if(count($products)>1){
                        $visible = 'pointer-events: none;';
                        $product = 'Products';
                        $warning = '';
                    }else{
                        $visible = 'pointer-events: none;';
                        $product = 'Product';
                        $warning = 'So You can only select direct shipment';
                    }
                @endphp

            <h3 class="txt-blue">{{ count($products) }} {{ $product }} has been added for <img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid"></h3>
            <p class="text-danger">{{ $warning }}</p>
            @include('admin.include.flash-message')
           <ul class="nav nav-tabs-black">
                <li class="nav-item">
                        <a href="#direct-shipment" data-toggle="tab" aria-expanded="false" class="nav-link active btn font-10 mt-3 btn-outline-primary">Direct Shipments</a>
                </li>
               <!--  <li class="nav-item">
                        <a href="#multiple-stops" data-toggle="tab" aria-expanded="true" style="{{ $visible }}" class="ml-2 nav-link multiple_stops font-16 btn font-10 mt-3 btn-outline-primary">Multiple Stops</a>
                </li> -->
           </ul>
           <hr style="border: none; border-bottom: 1px solid silver;">
           <div class="tab-content">
                <form action="{{ route('shipper.multiple-shipment.store.step3') }}" method="post" id="final_form">
                 @csrf
                    <div class="tab-pane show active" id="direct-shipment">
                        <div class="details-content">
                           <p class="font-16 color-dark-purple mb-3 txt-blue">Pickup Details 
                              <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                  <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                  <span class="ml-1 mr-3" style="color: #9B51E0;">Add Facility Address</span>
                              </a>
                          </p>
                          <div class="block">
                              <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                    <select name="pickupAddress[]" required id="errorShowPickup" class="form-control location-input select2">
                                        <option value="" selected>{{__('Select address')}}</option>
                                        @foreach ($addresses as $key => $address)
                                        <option value="{{$address->id}}">{{$address->name}}</option>
                                        @endforeach
                                    </select>
                                    
                                    <p class="text-danger" id="errorShowPickup" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                                    
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="form-group mb-4">
                                        {{-- <input type="date" id="pickup_date" name="pickup_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> --}}
                                        <input type="text" id="basic-datepicker" class="form-control form-control-without-border pb-2" name="pickup_date[]" required style="background: none; width: 100%;" placeholder="DD/MM/YYYY">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                    <div class="form-group mb-4 d-flex pickup_rang_div">
                                          <label><input type="checkbox" id="pickup_time_range_check" class="largerCheckbox" name="pickup_time_range_check[]" value="1"><span></span></label>
                                          <label for="range" class="txt-blue">Range</label>
                                          {{-- Range Div --}}
                                          <div id="pickup_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">


                                              <input type="text" id="basic-pickup-timepicker1" name="pickup_hour_from[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%" >


                                              <input type="hidden" name="pickup_time_from[]">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_from" required>
                                                  <option value="am">AM</option>
                                                  <option value="pm">PM</option>
                                             </select> --}}

                                              <span class="txt-blue ml-2 mr-2">TO</span>
                                              <input type="text" id="basic-pickup-timepicker2" name="pickup_hour_to[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%">
                                              <input type="hidden" name="pickup_time_to[]">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="pickup_time_to" required>
                                                  <option value="am">AM</option>
                                                  <option value="pm">PM</option>
                                             </select> --}}
                                          </div>
                                          {{-- Single time div --}}
                                          <div id="pickup_single_time" class="row" style="display: flex;">
                                              <input type="text" id="basic-pickup-timepicker3" name="pickup_hour[]" class="form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;" required="required">
                                              <input type="hidden" name="pickup_time_format[]">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="pickup_time_format" required>
                                                      <option value="am">AM</option>
                                                      <option value="pm">PM</option>
                                              </select> --}}

                                          </div>
                                    </div>
                                </div>
                                  <div class="d-sm-none col-lg-3 d-lg-block"></div>
                              </div>
                              <div class="row">
                                <div class="col-md-5">
                                    <div class="note-area md-lg-5">
                                       <ul class="d-flex m-0 p-0 mb-2">
                                          <label class="mr-2 txt-blue">Note</label>
                                          <label class="mr-2"><a href="javascript:;" class="txt-blue change_pickup_note"><img src="{{ asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                                          <label class="mr-2"><a href="javascript:;" class="txt-blue clear_pickup_note"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label>
                                       </ul>
                                       <div class="form-group">
                                           <textarea class="form-control" id="pickUpNote" rows="2" name="pickup_note[]" ></textarea>
                                       </div>
                                    </div>
                                    <div class="products_detail">
                                        <br/>
                                        <a href="javascript:;" class="add-one font-18 d-flex">
                                          <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                                          <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add Products</span>
                                        </a>
                                        @if(!empty($products))
                                            @foreach ($products as $products_item)
                                              <label><input type="checkbox" class="largerCheckbox pickup_product_id_checkbox" value="{{$products_item['product_id']}}"><span></span>{{ $products_item['product_name'] }}</label><br/>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <a href="javascript:void(0)" class="add-one font-18 d-flex add_more_pickuip_detail">
                                        <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                                        <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add More Pickup Detail</span>
                                    </a>
                                </div>
                              </div>
                           </div>
                        </div>

                        <div class="append_pickup_location_div">

                        </div>
                        
                        <hr style="border: none; border-bottom: 1px solid silver;">
                        <div class="details-content mt-4">
                           <p class="font-16 color-dark-purple mb-3 txt-blue">Delivery Details <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                        <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                        <span class="ml-1 mr-3" style="color: #9B51E0;">Add Facility Address</span>
                                    </a></p>
                           <div class="block">
                              <div class="row">
                                <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                   <select name="deliveryAddress[]" required id="errorShowDrop" class="form-control form-control-without-border select2">
                                        <option value="" selected>{{__('Select address')}}</option>
                                        @foreach ($addresses as $key => $address)
                                        <option value="{{$address->id}}">{{$address->name}}</option>
                                        @endforeach
                                    </select>
                                    <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                                </div>
                                <div class="col-sm-3 col-md-3 col-lg-3">
                                    <div class="form-group mb-4">
                                        {{-- <input type="date" id="delivery_date" name="delivery_date" required class="form-control form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY"> --}}
                                        <input type="text" id="basic-delivery-datepicker" class="form-control form-control-without-border pb-2" name="delivery_date[]" required style="background: none; width: 100%;" placeholder="DD/MM/YYYY">

                                         <p class="text-danger" id="errorShowDatePicker" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  
                                         End date must be come after the start date.</p>
                                        <p class="text-danger" id="errorShowDatePickerSame" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start date and end date not be same.</p>
                                     

                                      </div>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                    <div class="form-group mb-4 d-flex delivery_rang_div">
                                          <label><input type="checkbox" id="delivery_time_range_check" class="largerCheckbox" name="delivery_time_range_check[]" value="1"><span></span></label>
                                          <label for="range" class="txt-blue">Range</label>
                                          {{-- Range Div --}}
                                          <div id="delivery_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                              <input type="text" id="basic-pickup-timepicker4" name="delivery_hour_from[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%">

                                              <input type="hidden" name="delivery_time_from[]">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_from" required>
                                                  <option value="am">AM</option>
                                                  <option value="pm">PM</option>
                                             </select> --}}
                                              <span class="txt-blue ml-2 mr-2">TO</span>
                                              <input type="text" id="basic-pickup-timepicker5" name="delivery_hour_to[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%">
                                              <input type="hidden" name="delivery_time_to[]">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="delivery_time_to" required>
                                                  <option value="am">AM</option>
                                                  <option value="pm">PM</option>
                                             </select> --}}
                                          </div>
                                          {{-- Single time div --}}
                                          <div id="delivery_single_time" class="row" style="display: flex;">
                                              <input type="text" id="basic-pickup-timepicker6" name="delivery_hour[]" class="form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;" required="required">
                                              <input type="hidden" name="delivery_time_format[]">
                                              {{-- <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="delivery_time_format" required>
                                                      <option value="am">AM</option>
                                                      <option value="pm">PM</option>
                                              </select> --}}

                                          </div>

                                           

                                    </div>
                                    <p class="text-danger" id="errorShowSameTime" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start time and end time not be same.</p>

                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-5">
                                    <div class="note-area md-lg-5">
                                        <a href="javascript:;" class="add-one font-18 d-flex show_button" id="add_note">
                                            <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                                            <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add Note</span>
                                            {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                                         </a><br>
                                        <textarea rows="2" name="delivery_note[]" id="text_id" class="form-control" style="display:none" placeholder="Enter Delivery Note"></textarea>
                                    </div>
                                    <div class="products_detail">
                                      <br/>
                                      <a href="javascript:;" class="add-one font-18 d-flex">
                                        <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                                        <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add Products</span>
                                     </a>
                                      @if(!empty($products))
                                          @foreach ($products as $products_item)
                                            <label><input type="checkbox" class="largerCheckbox delivery_product_id_checkbox" value="{{$products_item['product_id']}}"><span></span>{{ $products_item['product_name'] }}</label><br/>
                                          @endforeach
                                      @endif
                                      
                                  
                                  </div>
                                </div>
                               <div class="col-md-7">
                                  <a href="javascript:void(0)" class="add-one font-18 d-flex add_more_delivery_detail">
                                    <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                                    <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add More Delivery Detail</span>
                                  </a>
                               </div>
                              </div>
                           </div>
                        </div>


                        <div class="append_delivery_address"></div>

                        
                        {{-- <a href="javascript:;" class="black-btn font-16 mt-5" data-toggle="modal" data-target="#edit-shipment">Insert Quote</a> --}}
                        <div class="btn-block d-flex">
                            <button type="submit" class="btn font-10 mt-3 btn-primary" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment" id="continuebtn">Insert Quote</button>
                        </div>
                    </div>
                </form>
                    <div class="tab-pane" id="multiple-stops">
                        {{-- <div class="location-details"> --}}
                        <div class="row">
                            <div class="col">
                               <div class="pro-info mt-4 input_fields_wrap">
                                   <div class="pro-content mt-4 mb-4 location_info">
                                     <div class="row pl-4" style="background-color:#f3eff7; margin-top: 2%;" id="location1">
                                        <div class="col">
                                           <p class="font-20 color-dark-purple mt-2 mb-3 txt-blue">Location<span id="locationLabel1"> 1 Details<span></p>
                                         <div class="row mt-2 mt-md-3 mt-lg-4">
                                           <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                                            <a class="dropdown-toggle form-control font-18 form-control-without-border pb-1 pl-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none;">
                                                Facility Address <i class="fa fa-angle-down ml-5" style="padding-left: 150px;"></i>
                                              </a>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                  <ul class="m-0 p-0" style="border: 1px silver;">
                                                    <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                                                        <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                                                        <span class="ml-1 font-20" style="color: #9B51E0;">Add Facility Address</span>
                                                      </a>
                                                      <hr style="border: none; border-bottom: 1px solid silver;">
                                                      @foreach ($addresses as $key => $address)
                                                      @php
                                                             if($key < $count){
                                                                 $style = 'border-bottom: 2px solid silver';
                                                             }else{
                                                                 $style = '';
                                                             }
                                                      @endphp
                                                        <div class="form-group mb-3" style="{{ $style }}">
                                                             {{-- <input type="checkbox" id="Curtainside" class="largerCheckbox" name="truck_type_category[]" value="{{ $truck_cat['id'] }}"> --}}
                                                             <label><input type="radio" class="addressOptions" id="address1" name="address" value="1"><span></span></label>
                                                             <label for="address" class="font-14 ml-1  txt-blue">Abc Inc <br> <em>Mithimnis 30, Athina 112 57, Greece</em></label>
                                                        </div>
                                                      @endforeach
                                               </ul>
                                             </div>
                                           </div>
                                           <div class="col-sm-3 col-md-3 col-lg-3">
                                                <div class="form-group mb-4">
                                                    <input type="date" id="delivery_date1" name="delivery_date1" required class="form-control font-18 form-control-without-border pb-2" style="background: none; width: 100%;" placeholder="DD/MM/YYYY">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                                                <div class="form-group mb-4 d-flex">
                                                      <label><input type="checkbox" id="location1_time_range_check" class="largerCheckbox" name="location1_time_range_check" value="1" onclick="showLocationRange()"><span></span></label>
                                                      <label for="range" class="font-18 txt-blue">Range</label>
                                                      {{-- Range Div --}}
                                                      <div id="location1_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                                                          <input type="time" id="Hour" name="Hour" class="font-18 form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%">
                                                          <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="time_format" required>
                                                              <option value="am">AM</option>
                                                              <option value="pm">PM</option>
                                                         </select>
                                                          <span class="font-18 txt-blue ml-2 mr-2">TO</span>
                                                          <input type="time" id="Hour" name="Hour" class="font-18 form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%">
                                                          <select class="form-control font-18 form-control-without-border pb-1 ml-1" style="background: none;width: 30%;" name="time_format" required>
                                                              <option value="am">AM</option>
                                                              <option value="pm">PM</option>
                                                         </select>
                                                      </div>
                                                      {{-- Single time div --}}
                                                      <div id="location1_single_time" class="row" style="display: flex;">
                                                          <input type="time" id="Hour" name="Hour" class="font-18 form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;">
                                                          <select class="form-control font-18 form-control-without-border pb-1 ml-3" style="background: none;width: 35%;" name="time_format" required>
                                                                  <option value="am">AM</option>
                                                                  <option value="pm">PM</option>
                                                          </select>

                                                      </div>

                                                     
                                     

                                                </div>

                                            </div>

                                           <div class="d-sm-none col-lg-3 d-lg-block"></div>
                                       </div>
                                       <div class="row">
                                        <div class="col-md-5">
                                            <div class="note-area md-lg-5">
                                               <ul class="d-flex m-0 p-0 mb-2">
                                                  <label class="mr-2 font-18 txt-blue">Note</label>
                                                  <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="changelocation1Note()""><img src="{{ asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                                                  <label class="mr-2"><a href="javascript:;" class="txt-blue" onclick="clearlocation1Note()""><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label>
                                               </ul>
                                               <div class="form-group">
                                                   <textarea class="form-control font-18" id="location1Note" name="location1Note" rows="2" readonly="readonly">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, </textarea>
                                               </div>
                                            </div>
                                        </div>
                                       <div class="col-md-7"></div>
                                      </div>
                                       <div class="row mt-1 mt-md-2 mt-lg-3">
                                             <div class="col-sm-2 col-md-2 col-lg-2">
                                                <p class="font-20 color-dark-purple mt-2 mb-3 txt-blue">Product Information</p>
                                                @foreach ($products as $key => $product)
                                                <label class="ml-3"><input type="checkbox" id="location1products" class="largerCheckbox" name="location1products[]" value="{{ $product['product_id'] }}""><span></span></label>
                                                <label for="range" class="font-18 txt-blue ml-3">{{ $product['product_name'] }}</label><br>
                                                @endforeach

                                             </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2 mt-1 mt-md-2 mt-lg-3">
                                              <div class="form-group mb-4">
                                                 <input type="radio" id="delivery_type1" name="delivery_type1" style="height:30px; width:30px; vertical-align: top;" class="delivery_type pb-2" value="pickup">
                                                 <label for="range" class="font-18 txt-blue">Pickup</label>
                                                 <input type="radio" id="delivery_type1" name="delivery_type1" style="height:30px; width:30px; vertical-align: top;" class="delivery_type pb-2 ml-2" value="delivery">
                                                 <label for="range" class="font-18 txt-blue">Delivery</label><br>
                                                </div>
                                            </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                                <div class="form-group mb-4 ml-3">
                                                   <input type="text" id="quantity" name="quantity_1" required class="form-control font-18 form-control-without-border pb-2" style="background: none;" placeholder="Quantity">
                                                 </div>
                                              </div>
                                            <div class="col-sm-2 col-md-2 col-lg-2">
                                              <div class="form-group mb-4 d-flex ml-3">
                                                 <input type="text" id="weight" name="weight_1" class="font-18 form-control-without-border pb-2 mr-1" style="background: none; width: 80%;" placeholder="Weight">
                                                 <div class="qty ml-0">
                                                   <select class="form-control font-18 form-control-without-border pb-1 ml-2" style="background: none; width : 120%;" name="unit_1" required>
                                                       <option  disabled selected>Unit</option>
                                                        @foreach ($unitData as $unit)
                                                            <option value="{{ $unit['id'] }}">{{ $unit['name'] }}</option>
                                                        @endforeach
                                                      </select>
                                                 </div>

                                               </div>
                                            </div>
                                             <div class="d-sm-none col-lg-3 d-lg-block"></div>
                                         </div>
                                           <div class="specs-check d-flex mt-3">

                                           </div>
                                         <div class="row">
                                           <div class="col-md-7 col-lg-7 col-xl-5">
                                             <div class="form-group">

                                             </div>
                                           </div>
                                           <div class="col-md-5 col-lg-5 col-xl-7">
                                              <div class="delete-this mb-5">
                                                 <a style="display: none !important; font-weight:600" id="locationDelete1" href="javascript:;" class="btn btn-light delete-one font-12 mt-3 d-flex align-items-right remove_product">
                                                   <img src="{{ asset('assets/images/shipment/delete-fill.svg')}}" alt="Delete" class="img-fluid">
                                                   <span class="text ml-1">Delete</span>
                                                 </a>
                                              </div>
                                           </div>
                                         </div>
                                       </div>

                                    </div>

                                   </div>
                                   <div class="extra_locations" id="extra_locations"></div>
                               </div>
                               {{-- @if(count($products) > 1) --}}
                               <a href="javascript:;" class="add-more-one font-20 mt-3 d-flex add_field_button btn btn-outline-info" onclick='clone()' id="addLocation" style="border-radius: 16px; width: 20%; padding-left: 6%;">
                                      <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="Plus" class="img-fluid">
                                      <span class="text ml-1 text-decoration-none">Add stop</span>
                               </a>
                               {{-- @endif --}}
                               <div class="btn-block d-flex mt-5">
                                <button type="submit" id="cust-button" class="btn font-10 mt-3 btn-primary cust-button" style="width: 350px;height: 50px;" data-toggle="modal" data-target="#edit-shipment">Insert Quote</button>
                            </div>
                            </div>
                        </div>

                      {{-- </div> --}}

                    </div>

                    <div class="modal fade add-address" id="add-address" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                          <div class="modal-content">
                            <div class="modal-body">
                              <div class="edits mb-2 d-flex align-items-center justify-content-between">
                                <div class="title font-20 text-left">Add Facility Address</div>
                              </div>
                              {{-- <form action="{{ route('shipper.shipment.address.submit') }}" id="address_form" method="POST"> --}}
                              <form action="javascript:void(0)" id="address_form" method="POST">
                                @csrf
                                <div class="form-group mb-3">
                                  <label for="name" class="mb-2 text-grey font-14">Name</label>
                                  <input type="text" id="name" maxlength="40" autocomplete="off" autofocus="off" onkeypress="return lettersValidate(event)" parsley-trigger="change" required name="name" class="form-control form-control-without-border" placeholder="Enter Name" value="{{ old('name') }}">
                                  @error('name')
                                  <div class="error">{{ $message }}</div>
                                  @enderror
                                </div>

                                <div class="form-group mb-3">
                                  <label for="first-name" class="mb-2 text-grey font-14">Address</label>
                                  <input type="text" id="autocomplete" name="address" autocomplete="off" autofocus="off" onkeypress="return lettersValidate(event)" parsley-trigger="change" required class="form-control form-control-without-border" value="{{ old('address') }}" placeholder="Enter Address">
                                  <input type="hidden" id="latitude" name="lat" value="">
                                  <input type="hidden" id="longitude" name="lng" value="">
                                  @error('address')
                                  <div class="error">{{ $message }}</div>
                                  @enderror
                                </div>

                                <div class="row">

                                  <div class="col-md-6">

                                    <div class="form-group  mb-3">
                                      <select id="country" name="country" class="form-control form-control-without-border customselect-div country"  data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                                        <option selected="selected" value="">Choose..</option>
                                        @if(isset($countries))
                                        @foreach($countries as $country)
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                        @endif
                                      </select>
                                      @error('city')
                                      <div class="error">{{ $message }}</div>
                                      @enderror

                                    </div>
                                  </div>

                                  <div class="col-md-6">

                                  <div class="form-group  mb-3">

                                      <select  id="state" name="state" class="form-control form-control-without-border customselect-div state" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required>
                                        <option selected="selected" value="">Choose..</option>
                                        @if(isset($states))
                                        @foreach($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                        @endif
                                      </select>
                                      @error('city')
                                      <div class="error">{{ $message }}</div>
                                      @enderror
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  
                                  <div class="col-md-6">

                                    <div class="form-group  mb-3">
                                    <select  id="city" name="city" class="form-control form-control-without-border customselect-div city" data-bind="volunteer-new-final-title-field"  data-parsley-required="true"  parsley-trigger="change" required >
                                          <option selected="selected" value="">Choose..</option>
                                          @if(isset($cities))
                                          @foreach($cities as $city)
                                          <option value="{{ $city->id }}">{{ $city->name }}</option>
                                          @endforeach
                                          @endif
                                        </select>
                                        @error('city')
                                        <div class="error">{{ $message }}</div>
                                        @enderror
                                        </div>
                                    </div>

                                  <div class="col-md-6">
                                    <div class="form-group  mb-3">

                                      <input type="text" id="postcode"  autocomplete="off" onkeypress="return numberValidate(event)" name="postcode" maxlength="6" parsley-trigger="change" required class="form-control form-control-without-border pb-2" placeholder="Postcode" >
                                      <!-- <div class="icon location">
                                        <img src="images/gps.svg" style="height: 72%;" class="img-fluid">
                                      </div> -->
                                      @error('name')
                                      <div class="error">{{ $message }}</div>
                                      @enderror


                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col">
                                    <div class="form-group  mb-3">
                                    <label for="first-name" class="mb-2 text-grey font-14">Phone</label>
                                      <input type="call" id="phone" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="8" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Phone">
                                      @error('name')
                                      <div class="error">{{ $message }}</div>
                                      @enderror

                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group  mb-3">
                                    <label for="first-name" class="mb-2 text-grey font-14">Email</label>
                                      <input type="email" id="email" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Email">
                                      @error('name')
                                      <div class="error">{{ $message }}</div>
                                      @enderror
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col">
                                    <div class="add-facility">
                                      <div class="form-group checkbox-inline">
                                        {{-- <input type="checkbox" value="1" name="restroom_service" id="Restrooms" checked="checked">
                                        <label for="Restrooms" class="font-15">Restrooms</label> --}}
                                        <label><input type="checkbox" id="Restrooms" class="largerCheckbox" name="restroom_service" value="1" checked="checked"><span></span></label>
                                        <label for="Restrooms" class="font-15">Restrooms</label>
                                      </div>
                                      <div class="form-group">
                                        {{-- <input type="checkbox" value="1"  name="food_service" id="Food" checked="checked">
                                        <label for="Food" class="font-15">Food</label> --}}
                                        <label><input type="checkbox" id="Food" class="largerCheckbox" name="food_service" value="1" checked="checked"><span></span></label>
                                        <label for="Restrooms" class="font-15">Food</label>
                                      </div>
                                      <div class="form-group">
                                        {{-- <input type="checkbox" value="1"  name="rest_area_service" id="Driver Rest Area" checked="checked">
                                        <label for="Driver Rest Area" class="font-15">Driver Rest Area</label> --}}
                                        <label><input type="checkbox" id="Driver Rest Area" class="largerCheckbox" name="rest_area_service" value="1" checked="checked"><span></span></label>
                                        <label for="Restrooms" class="font-15">Driver Rest Area</label>
                                      </div>

                                    </div>
                                  </div>
                                </div>


                                <div class="btn-block d-flex">
                                  <button type="button" class="btn w-100 btn-white mr-2 cancel-btn" data-dismiss="modal" aria-label="Close">Cancel</button>
                                  <button type="submit" class="btn w-100 btn-primary">Save</button>
                                </div>

                              </form>

                            </div>
                          </div>
                        </div>
                      </div>
           </div>
        </div>

     </div>
    </div>

{{-- Hidden Pickup Location Start --}}
<div class="hidden_pickup_location" style="display: none">
    <div class="pickup_location_div mt-4">
      <hr style="border: none;border-top: 1px solid silver;">
      <p class="font-16 color-dark-purple mb-3 txt-blue">Pickup Details 
        <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
            <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
            <span class="ml-1 mr-3" style="color: #9B51E0;">Add Facility Address</span>
        </a>
      </p>
      <div class="block">
          <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                <select name="pickupAddress[]" required id="errorShowPickup" class="form-control location-input hidden_pickup_location_select2">
                    <option value="" selected>{{__('Select address')}}</option>
                    @foreach ($addresses as $key => $address)
                    <option value="{{$address->id}}">{{$address->name}}</option>
                    @endforeach
                </select>
                
                <p class="text-danger" id="errorShowPickup" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
                
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <div class="form-group mb-4">
                    <input type="text" id="basic-datepicker" class="form-control form-control-without-border pb-2" name="pickup_date[]" required style="background: none; width: 100%;" placeholder="DD/MM/YYYY">
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                <div class="form-group mb-4 d-flex pickup_rang_div">
                      <label><input type="checkbox" id="pickup_time_range_check" class="largerCheckbox" name="pickup_time_range_check[]" value="1"><span></span></label>
                      <label for="range" class="txt-blue">Range</label>
                      {{-- Range Div --}}
                      <div id="pickup_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                          <input type="text" id="basic-pickup-timepicker1" name="pickup_hour_from[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%" >
                          <input type="hidden" name="pickup_time_from[]">
                          <span class="txt-blue ml-2 mr-2">TO</span>
                          <input type="text" id="basic-pickup-timepicker2" name="pickup_hour_to[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%">
                          <input type="hidden" name="pickup_time_to[]">
                      </div>
                      <div id="pickup_single_time" class="row" style="display: flex;">
                          <input type="text" id="basic-pickup-timepicker3" name="pickup_hour[]"   
                          class="form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;" required="required">
                          <input type="hidden" name="pickup_time_format[]">
                      </div>
                </div>
            </div>
              <div class="d-sm-none col-lg-3 d-lg-block"></div>
          </div>
          <div class="row">
            <div class="col-md-5">
                <div class="note-area md-lg-5">
                  <ul class="d-flex m-0 p-0 mb-2">
                      <label class="mr-2 txt-blue">Note</label>
                      <label class="mr-2"><a href="javascript:;" class="txt-blue change_pickup_note"><img src="{{ asset('assets/images/shipment/pen-black.svg')}}" class="img-fluid"></a></label>
                      <label class="mr-2"><a href="javascript:;" class="txt-blue clear_pickup_note"><img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid"></a></label>
                  </ul>
                  <div class="form-group">
                      <textarea class="form-control" id="pickUpNote" rows="2" name="pickup_note[]" ></textarea>
                  </div>
                </div>
                <div class="products_detail">
                  <br/>
                  <a href="javascript:;" class="add-one font-18 d-flex">
                    <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                    <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add Products</span>
                  </a>
                  @if(!empty($products))
                      @foreach ($products as $products_item)
                        <label><input type="checkbox" class="largerCheckbox pickup_product_id_checkbox" value="{{$products_item['product_id']}}"><span></span>{{ $products_item['product_name'] }}</label><br/>
                      @endforeach
                  @endif
              </div>
            </div>
          <div class="col-md-7">
            <label class="mr-2 mt-5">
              <a href="javascript:void(0)" class="txt-blue delete_pickup_location">
                <img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid">
              </a>
            </label>
          </div>
          </div>
      </div>
    </div>
    
</div>
</div>
{{-- Hidden Pickup Location End --}}

{{-- Hidden Delivery Detail Start --}}
<div class="hidden_delivery_detail" style="display: none">
    <div class="delivery_location_div">
      <hr style="border: none;border-top: 1px solid silver;">
      <div class="details-content mt-4">
        <p class="font-16 color-dark-purple mb-3 txt-blue">Delivery Details <a href="javascript:;" class="ml-3 txt-blue" data-toggle="modal" data-target="#add-address">
                     <img src="{{ asset('assets/images/shipment/plus-pink.svg')}}" alt="chat" class="img-fluid">
                     <span class="ml-1 mr-3" style="color: #9B51E0;">Add Facility Address</span>
                 </a></p>
        <div class="block">
           <div class="row">
             <div class="col-sm-3 col-md-3 col-lg-3 dropdown">
                <select name="deliveryAddress[]" required id="errorShowDrop" class="form-control form-control-without-border more_delivery_address_select2">
                     <option value="" selected>{{__('Select address')}}</option>
                     @foreach ($addresses as $key => $address)
                     <option value="{{$address->id}}">{{$address->name}}</option>
                     @endforeach
                 </select>
                 <p class="text-danger" id="errorShowDrop" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i> This value is required.</p>
             </div>
             <div class="col-sm-3 col-md-3 col-lg-3">
                 <div class="form-group mb-4">
                     <input type="text" id="basic-delivery-datepicker" class="form-control form-control-without-border pb-2" name="delivery_date[]" required style="background: none; width: 100%;" placeholder="DD/MM/YYYY">

                      <p class="text-danger" id="errorShowDatePicker" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  
                      End date must be come after the start date.</p>
                     <p class="text-danger" id="errorShowDatePickerSame" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start date and end date not be same.</p>
                  

                   </div>
             </div>
             <div class="col-sm-6 col-md-6 col-lg-6 mt-1">
                 <div class="form-group mb-4 d-flex delivery_rang_div">
                       <label><input type="checkbox" id="delivery_time_range_check" class="largerCheckbox" name="delivery_time_range_check[]" value="1"><span></span></label>
                       <label for="range" class="txt-blue">Range</label>
                       {{-- Range Div --}}
                       <div id="delivery_time_range" class="row ml-1" style="flex-wrap: unset !important;display:none;">
                           <input type="text" id="basic-pickup-timepicker4" name="delivery_hour_from[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none; width: 30%">

                           <input type="hidden" name="delivery_time_from[]">
                           
                           <span class="txt-blue ml-2 mr-2">TO</span>
                           <input type="text" id="basic-pickup-timepicker5" name="delivery_hour_to[]" class="form-control-without-border pb-2 ml-2 w-0" placeholder="HH:MM" style="background: none;  width: 30%">
                           <input type="hidden" name="delivery_time_to[]">
                           
                       </div>
                       {{-- Single time div --}}
                       <div id="delivery_single_time" class="row" style="display: flex;">
                           <input type="text" id="basic-pickup-timepicker6" name="delivery_hour[]" class="form-control-without-border pb-2 ml-4" placeholder="HH:MM" style="background: none;" required="required">
                           <input type="hidden" name="delivery_time_format">
                          

                       </div>

                        

                 </div>
                 <p class="text-danger" id="errorShowSameTime" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d"></i>  Start time and end time not be same.</p>

             </div>
           </div>
           <div class="row">
             <div class="col-md-5">
                 <div class="note-area md-lg-5">
                     <a href="javascript:;" class="add-one font-18 d-flex show_button" id="add_note">
                         <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                         <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add Note</span>
                      </a><br>
                     <textarea rows="2" name="delivery_note[]" id="text_id" class="form-control" style="display:none" placeholder="Enter Delivery Note"></textarea>
                 </div>
                 <div class="products_detail">
                  <br/>
                  <a href="javascript:;" class="add-one font-18 d-flex">
                    <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                    <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add Products</span>
                  </a>
                  @if(!empty($products))
                      @foreach ($products as $products_item)
                        <label><input type="checkbox" class="largerCheckbox delivery_product_id_checkbox" value="{{$products_item['product_id']}}"><span></span>{{ $products_item['product_name'] }}</label><br/>
                      @endforeach
                  @endif
              </div>
             </div>
            <div class="col-md-7">

              <label class="mr-2 mt-5">
                <a href="javascript:void(0)" class="txt-blue delete_delivery_location">
                  <img src="{{ asset('assets/images/shipment/delete-black.svg')}}" class="img-fluid">
                </a>
              </label>
            </div>
           </div>
        </div>
     </div>
     
    </div>
</div>
{{-- Hidden Delivery Detail End --}}
@endsection

@section('script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="{{ URL::asset('shipper/assets/libs/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ URL::asset('shipper/assets/js/pages/form-pickers.init.js')}}"></script>


<script>
  $('#basic-datepicker').flatpickr({minDate: "today"});
  $('#basic-delivery-datepicker').flatpickr({minDate: "today"});
  $('#basic-pickup-timepicker6').on("input", function() {
      var pickup_date = $('#basic-datepicker').val();
      var dropoff_date = $('#basic-delivery-datepicker').val();
      var stime = $('#basic-pickup-timepicker3').val();   
      var etime = this.value;
      console.log(stime,etime);
      if(pickup_date == dropoff_date){
        if(stime >= etime){
        document.getElementById('errorShowSameTime').style.display = 'block';
         $('#continuebtn').prop('disabled', true);
        }else{
          document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
        }
      }else{
        document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
      }
     
  });

  $('#basic-delivery-datepicker').on("input", function() {
    var pickup_date = $('#basic-datepicker').val();
      var dropoff_date = $('#basic-delivery-datepicker').val();
      var stime = $('#basic-pickup-timepicker3').val();   
      var etime = $('#basic-pickup-timepicker6').val();
      console.log(stime,etime);
      if(pickup_date == dropoff_date){
        if(stime >= etime){
        document.getElementById('errorShowSameTime').style.display = 'block';
         $('#continuebtn').prop('disabled', true);
        }else{
          document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
        }
      }else{
        document.getElementById('errorShowSameTime').style.display = 'none';
          $('#continuebtn').prop('disabled', false);
      }
     
  });

	 let pickupid;

    function addPickupAddress(val, addressName, address){
    	pickupid = val;

        let pickupAddress = document.getElementById('pickupAddress');
        let pickUpAddressTag = document.getElementById('PickupdropdownMenuLink');
        document.getElementById('pickupAddress').value = val;
        pickUpAddressTag.text = addressName+' '+address;
        document.getElementById('errorShowPickup').style.display = 'none';
        return false;
    }

    function addDeliveryAddress(val, addressName, address){
    	if(pickupid == val){
          document.getElementById('errorShowDeliveryPickup').style.display = 'block';
        }else{
          document.getElementById('errorShowDeliveryPickup').style.display = 'none';
        }

        let deliveryAddress = document.getElementById('deliveryAddress');
        let deliveryAddressTag = document.getElementById('deliveryDropdownMenuLink');
        document.getElementById('deliveryAddress').value = val;
        deliveryAddressTag.text = addressName+' '+address;
        document.getElementById('errorShowDelivery').style.display = 'none';
        return false;
    }


    $(document).ready(function(){
      $("#continuebtn").click(function(){


          /*
          
            let pickUpvalue = document.getElementById('pickupAddress');
            let deliveryvalue = document.getElementById('deliveryAddress');
            var type = true;

            if(pickUpvalue.value === '0'){
                document.getElementById('errorShowPickup').style.display = 'block';
                type = false;
            }
            else{
                document.getElementById('errorShowPickup').style.display = 'none';
                type = false;
            }

            if(deliveryvalue.value === '0'){
                document.getElementById('errorShowDelivery').style.display = 'block';
                type = false;
            }
            else{
                document.getElementById('errorShowDelivery').style.display = 'none';
                type = false;
            }

            let stime = document.getElementById('basic-pickup-timepicker3');
            let etime = document.getElementById('basic-pickup-timepicker6');
          
          */
    

        });

        $(document).on('submit','#address_form',function(){
          // var form_data = new FormData(this);
          var form = $(this);
          $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
              type: "POST",
              url: '{{ route('shipper.multiple-shipment.address.submit') }}',
              data: form.serialize(), // <--- THIS IS THE CHANGE
              dataType: "html",
              success: function(data){
                var response = JSON.parse(data);
                if(response.status == false){
                  $.each(response.messages, function(key,val){
                    if(val != ''){
                      $('#'+key+'_error').html(val);
                    }else{
                      $('#'+key+'_error').html('');
                    }
                  })
                }else{
                  get_facility_address();
                  $('#address_form').trigger('reset');
                  $('#country').val('').trigger('change');
                  $('#state').val('').trigger('change');
                  $('#city').val('').trigger('change');
                  $('.cancel-btn').click();
                }
              },
              error: function() { alert("Error posting feed."); }
            });
        });

        // Add and remove pickup location start
        $(document).on("click",".add_more_pickuip_detail",function(){
            var total_drop_pickup_location = 2;
            var total_pickup_location = $(".append_pickup_location_div .pickup_location_div").length;
            var total_delivery_location = $(".append_delivery_address .delivery_location_div").length;
            var final_drop_pickup_location = total_drop_pickup_location + parseInt(total_pickup_location) + parseInt(total_delivery_location);
            if(final_drop_pickup_location <= 4){
              var clone = $(".hidden_pickup_location").children().clone();
              clone.find('#basic-datepicker').flatpickr({minDate: "today"});
              clone.find(".hidden_pickup_location_select2").select2({"width":"100%"});
              //clone.find("#showPickUpRange").timepicker();
              $(".append_pickup_location_div").append(clone);
            }
        });
        
        $(document).on("click",".delete_pickup_location",function(){
            $(this).closest(".pickup_location_div").remove();
        });

        // Add and remove pickup location end

        // Add and remove delivery location start
        $(document).on("click",".add_more_delivery_detail",function(){
            var total_drop_pickup_location = 2;
            var total_pickup_location = $(".append_pickup_location_div .pickup_location_div").length;
            var total_delivery_location = $(".append_delivery_address .delivery_location_div").length;
            var final_drop_pickup_location = total_drop_pickup_location + parseInt(total_pickup_location) + parseInt(total_delivery_location);
            if(final_drop_pickup_location <= 4){
              var clone = $(".hidden_delivery_detail").children().clone();
              clone.find('#basic-delivery-datepicker').flatpickr({minDate: "today"});
              clone.find(".more_delivery_address_select2").select2({"width":"100%"});
              $(".append_delivery_address").append(clone);
            }
        });

        $(document).on("click",".delete_delivery_location",function(){
            $(this).closest(".delivery_location_div").remove();
        });
        // Add and remove delivery location end
    });

    function get_facility_address()
    {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        type: "get",
        url: '{{ url('shipper/multiple-shipment/addresses') }}',
        dataType: "html",
        success: function(data){
          var response = JSON.parse(data);
          var option = '<option value="">Select Address</option>';
          $.each(response, function(key,value){
            option += '<option value="'+value.id+'">'+value.name+'</option>';
          });
          $('select[name=pickupAddress]').html(option).trigger('change');
          $('select[name=deliveryAddress]').html(option).trigger('change');
        },
        error: function() { alert("Error posting feed."); }
      });
    }

    google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            var address = pickup_place.address_components;
            var street = city = state = '';
            $.each(address, function(i,val){
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
            });
            $('#latitude').val(pickup_place.geometry.location.lat());
            $('#longitude').val(pickup_place.geometry.location.lng());
        });
      });
</script>
<script type="text/javascript">

    $(document).on("click",".show_button",function(){
      var note_area  = $(this).closest(".note-area");
      note_area.find("#text_id").toggle()
    });

    $(document).on("change","#pickup_time_range_check",function(){
        var pickup_rang_div = $(this).closest(".pickup_rang_div");
        if($(this).is(':checked') == true){
            pickup_rang_div.find("#pickup_time_range").show();
            pickup_rang_div.find("#pickup_single_time").hide();
            pickup_rang_div.find('input[name=pickup_hour[]]').attr('required','');
            pickup_rang_div.find('input[name=pickup_hour_from[]]').attr('required','required');
            pickup_rang_div.find('input[name=pickup_hour_to[]]').attr('required','required');
        }else{
            pickup_rang_div.find("#pickup_time_range").hide();
            pickup_rang_div.find("#pickup_single_time").show();
            pickup_rang_div.find('input[name=pickup_hour[]]').attr('required','required');
            pickup_rang_div.find('input[name=pickup_hour_from[]]').attr('required','');
            pickup_rang_div.find('input[name=pickup_hour_to[]]').attr('required','');
        }

    });

    //Pickup functions
    /* function showPickUpRange() {
        var checkBox = document.getElementById("pickup_time_range_check");

        var time_range = document.getElementById("pickup_time_range");
        var single_time = document.getElementById("pickup_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
            $('input[name=pickup_hour]').attr('required','');
            $('input[name=pickup_hour_from]').attr('required','required');
            $('input[name=pickup_hour_to]').attr('required','required');
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
            $('input[name=pickup_hour]').attr('required','required');
            $('input[name=pickup_hour_from]').attr('required','');
            $('input[name=pickup_hour_to]').attr('required','');
        }
    } */

    $(document).on("click",".change_pickup_note",function(){
        var note_area = $(this).closest(".note-area");
        note_area.find("#pickUpNote").prop("readonly",false);
    });
    
    $(document).on("click",".clear_pickup_note",function(){
        var note_area = $(this).closest(".note-area");
        note_area.find("#pickUpNote").prop("readonly",false);
        note_area.find("#pickUpNote").val("");
    });

    /* function changePickUpNote() {
        document.getElementById('pickUpNote').readOnly =false;
    }

    function clearPickUpNote() {
        document.getElementById('pickUpNote').value = "";
        document.getElementById('pickUpNote').readOnly =false;
    } */

    //Delivery functions
    /* function showDeliveryRange() {
        var checkBox = document.getElementById("delivery_time_range_check");

        var time_range = document.getElementById("delivery_time_range");
        var single_time = document.getElementById("delivery_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
            $('input[name=delivery_hour]').attr('required','');
            $('input[name=delivery_hour_from]').attr('required','required');
            $('input[name=delivery_hour_to]').attr('required','required');
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
            $('input[name=delivery_hour]').attr('required','required');
            $('input[name=delivery_hour_from]').attr('required','');
            $('input[name=delivery_hour_to]').attr('required','');
        }
    } */

    $(document).on("change","#delivery_time_range_check",function(){
        var delivery_rang_div = $(this).closest(".delivery_rang_div");
        if($(this).is(':checked') == true){
            delivery_rang_div.find("#delivery_time_range").show();
            delivery_rang_div.find("#delivery_single_time").hide();
            delivery_rang_div.find('input[name=delivery_hour[]]').attr('required','');
            delivery_rang_div.find('input[name=delivery_hour_from[]]').attr('required','required');
            delivery_rang_div.find('input[name=delivery_hour_to[]]').attr('required','required');
        }else{
            delivery_rang_div.find("#delivery_time_range").hide();
            delivery_rang_div.find("#delivery_single_time").show();
            delivery_rang_div.find('input[name=delivery_hour[]]').attr('required','required');
            delivery_rang_div.find('input[name=delivery_hour_from[]]').attr('required','');
            delivery_rang_div.find('input[name=delivery_hour_to[]]').attr('required','');
        }

    });

    function changeDeliveryNote() {
        document.getElementById('deliveryNote').readOnly =false;
    }

    function clearDeliveryNote() {
        document.getElementById('deliveryNote').value = "";
        document.getElementById('deliveryNote').readOnly =false;
    }

    // Multiple Location page functions

    function showLocationRange() {
        var checkBox = document.getElementById("location1_time_range_check");

        var time_range = document.getElementById("location1_time_range");
        var single_time = document.getElementById("location1_single_time");
        if (checkBox.checked == true){
            time_range.style.display = "flex";
            single_time.style.display = "none";
        } else {
            time_range.style.display = "none";
            single_time.style.display = "flex";
        }
    }

    function changelocation1Note() {
        document.getElementById('location1Note').readOnly =false;
    }

    function clearlocation1Note() {
        document.getElementById('location1Note').value = "";
        document.getElementById('location1Note').readOnly =false;
    }

    </script>

@endsection
