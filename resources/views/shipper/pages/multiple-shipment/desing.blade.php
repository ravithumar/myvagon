@extends('shipper.layouts.master')

@section('content')
<style>
  
</style>
<div class="container-fluid">
 
  <div class="row align-items-center">
    <div class="col-md-12">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center"><i class="fas fa-shipping-fast pr-3"></i>Create Shipment</a></li>
          <li class="breadcrumb-item" aria-current="page"><a href="javascript:;">Truck Type</a></li>
          <li class="breadcrumb-item" aria-current="page"><a href="javascript:;">Commodity Information</a></li>
          <li class="breadcrumb-item" aria-current="page"><a href="javascript:;">Pickup & Delivery Information</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="javascript:;">Insert Your Quote</a></li>
        </ol>
      </nav>
    </div>
  </div>
  <div class="row">
  	<div class="col-md-12">
  		<h3 class="text-dark">Insert your Quote</h3>
  		<p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p>
  	</div>
  	<div class="col-md-12 my-4">
  		 <div id="map" style="height: 400px;width: 100%;"></div>
  	</div>
  	<div class="col-md-12">
  		<h3>Review  Details</h3>
  		<div class="table-responsive">
            <table class="table mb-0">
                <thead class="thead-light">
                    <tr>
                        <th class="font-20 text-dark font-weight-normal bgpurple-light">S.No.</th>
                        <th class="font-20 text-dark font-weight-normal bgpurple-light">Address Details</th>
                        <th class="font-20 text-dark font-weight-normal bgpurple-light">Pickup/Delivery</th>
                        <th class="font-20 text-dark font-weight-normal bgpurple-light">Product</th>
                        <th class="font-20 text-dark font-weight-normal bgpurple-light">Quantity</th>
                        <th class="font-20 text-dark font-weight-normal bgpurple-light">Weight</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row" class="text-dark">1</td>
                        <td class="border-left text-dark">Mithimnis 30, Athina 112 57, Greece<br/><span class="text-muted">March 17, 2021   8:30am ETA</span></td>
                        <td class="border-left align-middle text-dark">Pickup</td>
                        <td class="align-middle text-dark">Prouct 1</td>
                        <td class="align-middle text-dark">Quantity 2</td>
                        <td class="align-middle text-dark">Weight 3</td>
                    </tr>
                     <tr>
                        <td scope="row" class="border-top-0"></td>
                        <td class="border-left border-top-0"></td>
                        <td class="border-left border-top-0 text-dark">Pickup</td>
                        <td class="border-top-0 text-dark">Prouct 1</td>
                        <td class="border-top-0 text-dark">Quantity 2</td>
                        <td class="border-top-0 text-dark">Weight 3</td>
                    </tr>
                    <tr>
                        <td scope="row" class="text-dark">2</td>
                        <td class="border-left text-dark">Mithimnis 30, Athina 112 57, Greece<br/><span class="text-muted">March 17, 2021   8:30am ETA</span></td>
                        <td class="border-left text-dark">Delivery</td>
                        <td class="text-dark">Product 2</td>
                        <td class="text-dark">Quantity 2</td>
                        <td class="text-dark">Weight 3</td>
                    </tr>
               
                    <tr>
                        <td scope="row">3</td>
                        <td class="border-left text-dark">Mithimnis 30, Athina 112 57, Greece<br/><span class="text-muted">March 17, 2021   8:30am ETA</span></td>
                        <td class="border-left text-dark">Pickup</td>
                        <td class="text-dark"> </td>
                        <td class="text-dark">Quantity 2</td>
                        <td class="text-dark">Weight 3</td>
                    </tr>
              
                </tbody>
            </table>
        </div>
  	</div>
  	<div class="col-md-12 mt-4">
      <div class="long-haul-shipment"> 
       <div class="long-block">    
         <div class="popup d-flex align-items-center">
           <h4 class="txt-purple mr-2 font-15">Long haul shipment</h4>
           <a href="#" class="btn-tooltip position-relative d-flex align-items-center txt-blue">i
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements greater than 350 to 400km from 
origin to destination are generally classified as
 long-haul movements</span></a>
         </div>            
         <p class="mb-1 font-14 txt-blue font-weight-light">Total Miles: <em class="font-weight-bold">500</em></p>
         <p class="mb-1 font-14 txt-blue font-weight-light">Journey: <em class="font-weight-bold">48hr</em></p>
       </div>
        <form class="my-3">
          <div class="row">
            <div class="col-lg-5">
              <div class="form-group position-relative mb-3">
                <input type="text" name="email" class="border-bottom form-control custom-input call-icon position-relative pl-4 font500" placeholder="Mobile No or Email">
                <div class="icon position-absolute"><img src="../images/phone.png" alt="" class="img-fluid"></div>
                <p class="font-15">Link would be sent once shipment is created</p>
              </div>
              <div class="form-group d-flex">
                    <div class="label-wrapper">
                                   
                      <label><input type="checkbox" id="truck_type_4" class="largerCheckbox" name="truck_type[]" onclick="validateInput(this)" value="4" data-parsley-multiple="truck_type"><span></span></label>
                    </div>
                    <div class="text-wrapper d-flex pl-1">
                        <div class="wrap-both">
                           <p class="mb-0 font-14 txt-blue">Public Shipment</p>
                           <p class="mb-0 font-14">Check this option for shipment broadcasting to carrier/freelance driver or bidding option</p>
                        </div>
                    </div>
                   
                              
               </div>
               <div class="form-group mb-3">
                 <div class="allow-bidding d-flex align-items-center">
                    <div class="text font-14 font600 txt-blue mr-3">Allow Bidding :</div>
                    <label class="switch mb-0">
                      <input type="checkbox" checked="">
                      <span class="slider round"></span>
                    </label>
                  </div>
               </div> 
               <div class="form-group">                
                  <textarea class="form-control border-r13 font-24 font500" id="exampleFormControlTextarea1" rows="2" placeholder="Insert Quote"></textarea>
               </div>  
               <div class="form-group">
                    <a href="javascript:void(0);" class="btn btn-blue btn-sm mr-2 mt-2 border-r6 py-2">
                        <span class="text ml-1 font-14">Create Shipment</span>
                    </a>
               </div>          
            </div>
            <div class="col-lg-7"></div>

          </div>
         

        </form>
      </div>
  		

  		
  	</div>
  </div>  
</div>
@endsection
@section('script');
  <script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}" async defer></script>
<script type="text/javascript">
  function initMap() {
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 3,
    center: { lat: 0, lng: -180 },
    mapTypeId: "terrain",
  });
  const flightPlanCoordinates = [
    { lat: 23.0711557, lng: 72.5155701 },
    { lat: 23.2207847, lng: 72.6105287 },
    // { lat: -18.142, lng: 178.431 },
    // { lat: -27.467, lng: 153.027 },
  ];
  const flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: "#FF0000",
    strokeOpacity: 1.0,
    strokeWeight: 2,
  });
  flightPath.setMap(map);
}
$(document).ready(function(){
  initMap();
})
       
       
</script>
@endsection