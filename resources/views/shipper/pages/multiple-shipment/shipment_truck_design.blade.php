
@extends('shipper.layouts.master')
@section('content')
   {{-- <div class="content-page create-shipment-page">
   	  <div class="content">--}}
   	  	 <div class="container-fluid">
            @include('admin.include.flash-message')
   	  	 	<div class="row align-items-center">
   	  	 		<div class="col">
   	  	 			  <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0">
                       <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ asset('assets/images/shipment/create-shipping-fast-fill.svg')}}" class="img-fluid">Create Shipment</a></li>
                       <li class="breadcrumb-item active txt-blue" aria-current="page">Truck Type</li>
                    </ol>
                </nav>
   	  	 		</div>
   	  	 	</div>
           
           
            <div class="row d-flex align-items-center products">
             <div class="col-md-8 col-lg-6">
                <h3 class="txt-blue">Choose Your Truck Requirement</h3>
                <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p>
             </div>
             <div class="col-md-4 col-lg-5"></div>
          </div>
          <form action="{{ route('shipper.shipment.create.step2') }}" method="POST">
            @csrf
          <div class="row">
             <div class="col">
                <div class="select-trucks mt-4">
                   <div class="title-txt font-18 txt-blue">Select Truck type with sub category</div>

                   <div class="truck-category mt-4"  style="background-color: white; padding-top: 2.25rem;padding-bottom: 1rem;padding-left:1rem;">
                    @foreach($truckDetails as $key => $truck)
                      <div class="checkmark mb-4 mb-lg-5">
                           <div class="form-group d-flex align-items-center">
                                <div class="label-wrapper">
                                   {{-- <input type="checkbox"  id="{{ $truck['name'] }}" class="largerCheckbox" name="truck_type[]" value="{{ $truck['id'] }}"> --}}
                                   <label><input type="checkbox" id="truck_type_{{ $truck['id'] }}" class="largerCheckbox" name="truck_type[]" onclick="validateInput(this)" value="{{ $truck['id'] }}"><span></span></label><br>
                                   <label for="Semi-trailer Truck"></label>
                                </div>
                                @php
                                    $src = 'images/type/'.$truck['icon'];

                                    if($key < $count){
                                        $style = 'border-bottom: 1px solid silver';
                                    }else{
                                        $style = '';
                                    }
                                @endphp
                                <div class="text-wrapper d-flex align-items-center pl-3 pl-lg-4">
                                    <span class="wrap-both d-block">
                                        <img src="{{ asset($src)}}" class="img-fluid" width="150" style="object-fit: scale-down;" height="120">
                                        <span class="text d-block mt-2 txt-blue">{{ $truck['name'] }}</span>
                                    </span>
                                </div>
                                <!-- <div class="dropdown" style="width: 22%; right: -10%;">
                                   <a class="dropdown-toggle form-control form-control-without-border pb-1 pl-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none;">
                                     Select Sub-Category <i class="fa fa-angle-down ml-5" style="font-size:24px;float: right;"></i>
                                   </a>
                                   <p class="text-danger" id="errorShow_{{ $truck['id'] }}" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> Please select any subcategory</p>

                                   <div class="dropdown-menu w-100" aria-labelledby="dropdownMenuLink">
                                    <ul class="m-0 p-0" >
                                        @foreach ($truck['category'] as $truck_cat)
                                            <div class="form-group mb-3" style="{{ $style }}">
                                                {{-- <input type="checkbox" id="Curtainside" class="largerCheckbox" name="truck_type_category[]" value="{{ $truck_cat['id'] }}"> --}}
                                                <label><input type="checkbox" id="truck_categories_{{ $truck['id'] }}" class="largerCheckbox" onclick="validateSubcategory(this, {{ $truck['id'] }})" name="truck_categories[{{ $truck['id'] }}][]" value="{{ $truck_cat['id'] }}"><span></span></label>
                                                <label for="category_name" class="txt-blue">{{ $truck_cat['name'] }}</label>
                                            </div>
                                        @endforeach
                                    </ul>
                                  </div>
                                </div> --> 
                                <div class="sub-category-page pl-3" style="width: 25%">
                                  <select class="form-control select2-multiple select2-hidden-accessible my-select" id="truck_categories_{{ $truck['id'] }}" name="truck_categories[{{ $truck['id'] }}][]" multiple="multiple" placeholder="Please Select Truck Type" data-parsley-required-message="Please select any subcategory">
                                    @foreach ($truck['category'] as $truck_cat)
                                    <option value="{{ $truck_cat['id'] }}" data-badge="">{{ $truck_cat['name'] }}</option>
                                    @endforeach
                                    {{-- <option value="O2" data-badge="">Option2</option>
                                    <option value="O3" data-badge="">Option3</option>
                                    <option value="O4" data-badge="">Option4</option>
                                    <option value="O5" data-badge="">Option5</option>
                                    <option value="O6" data-badge="">Option6</option>
                                    <option value="O7" data-badge="">Option7</option> --}}
                                  </select>
                                  <p class="text-danger" id="errorShow_{{ $truck['id'] }}" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> Please select any subcategory</p>
                                </div>
                            </div>
                      </div>
                      @endforeach
                      <p class="text-danger" id="errorShow" style="display: none;"><i class="fa fa-times-circle" style="color: #ff4d4d;"></i> Please select any truck type</p>
                   </div>
                   <div class="other-specs mt-3 mt-md-4 mt-lg-5">
                       <p class="font-18 color-dark-purple txt-blue">Other Specifications</p>
                       <div class="specs-check d-flex mt-3">
                          <div class="form-group ml-md-4">
                             {{-- <input type="checkbox" id="Hydraulic Door" class="largerCheckbox" name="other_specifications" value="Hydraulic Door"> --}}
                             <label><input type="checkbox" id="Hydraulic Door" class="largerCheckbox" name="hydraulic" value="1"><span></span></label>
                             <label for="Hydraulic Door" class="pl-2 txt-blue">Hydraulic Door</label>
                          </div>
                           <div class="form-group ml-3 ml-md-4 ml-lg-5">
                             {{-- <input type="checkbox" id="Cooling" class="largerCheckbox" name="other_specifications" value="Cooling"> --}}
                             <label><input type="checkbox" id="Cooling" class="largerCheckbox" name="cooling" value="1"><span></span></label>
                             <label for="Cooling" class="pl-2 txt-blue">Cooling</label>
                          </div>
                       </div>
                       {{-- <button class="show_button">Show/hide textarea</button><br> --}}
                       <a href="javascript:;" class="add-one font-18 mt-3 d-flex show_button" id="add_note">
                           <img src="{{ asset('assets/images/shipment/plus-black.svg')}}" alt="chat" class="img-fluid">
                           <span class="text ml-1 txt-blue" style="text-decoration: underline;">Add Note</span>
                           {{-- <textarea name="shipment_note" id="shipment_note" rows="2" style="display: none;"></textarea> --}}
                        </a><br>
                        <textarea rows="5" name="shipment_note" id="text_id" class="form-control" style="display:none" placeholder="Enter Shipment Note"></textarea>
                        {{-- <button type="submit" class="black-btn font-16 mt-5">Continue</button> --}}
                        <div class="btn-block d-flex">
                            <button type="submit" class="btn font-10 mt-5 btn-primary" id="continuebtn" style="width: 350px;height: 50px;">Continue</button>
                        </div>
                       {{-- <a href="create-shipment-product-information.php" class="black-btn font-16 mt-5">Continue</a> --}}
                   </div>
                </div>

             </div>
          </div>
          </form>
   	  	 </div>
   	  {{-- </div>
   </div> --}}
   @endsection

   @section('script')
   <script>$(".show_button").click(function(){$("#text_id").toggle()})</script>
   <script type="text/javascript">
    window.onbeforeunload = function() {
    return true;
};
// Remove navigation prompt
window.onbeforeunload = null;
    function validateInput(e){
        let errorShow = document.getElementById('errorShow');
        errorShow.style.display = 'none';
        console.log('hey');
        let cat_id = "truck_categories_"+e.value;
        let error_show_id = 'errorShow_'+e.value;
        let error_show = document.getElementById(error_show_id);
        $truck_cat = $("#"+cat_id);
        if(e.checked == true){
            $truck_cat.prop('required', true);
            // error_show.style.display = 'block';
            return false;
        }else{
            $truck_cat.prop('required', false);
            // error_show.style.display = 'none';
            return false;
        }
    }

    function validateSubcategory(e, truck_id){
        console.log(truck_id);
        let error_show_id = 'errorShow_'+truck_id;
        let cat_id = "truck_categories_"+truck_id;
        $truck_cat = $("input:checkbox[id^='"+cat_id+"']");
        let error_show = document.getElementById(error_show_id);
        console.log($truck_cat.is(":checked"));
        if($truck_cat.is(":checked"))
        {
            error_show.style.display = 'none';
            $("#truck_type_"+truck_id).prop("checked", true);
            return false;
        }else{
            // error_show.style.display = 'block';
            $("#truck_type_"+truck_id).prop("checked", false);
            return false;
        }
    }

            $(document).ready(function()
            {
                $("#continuebtn").click(function()
                {
                    let errorShow = document.getElementById('errorShow');
                    $cbx_group = $("input:checkbox[name='truck_type[]']");
                    $cbx_group = $("input:checkbox[id^='truck_type']"); // name is not always helpful ;)

                    if($cbx_group.is(":checked")){
                        $cbx_group.prop('required', false);
                        errorShow.style.display = 'none';
                     
                    }else{
                        $cbx_group.prop('required', true);
                        errorShow.style.display = 'block';
                       
                        return false;
                    }

                  
                });
            });
            $('.dropdown-menu').click(function(e){
              e.stopPropagation();
            })
        $('.sub-category-page').select2({
            templateSelection: function (data) {
            if (data.id === '') { // adjust for custom placeholder values
            return 'Custom styled placeholder text';
            }

            return data.text;
            }
        });


   </script>

   @endsection
