
@extends('shipper.layouts.master')

@section('content')

<style type="text/css">
/* .clearfix {
  
  background: #1f1f41;
  
  margin-left: -17px;
} */

/* .has-search .form-control {
    padding-left: 2.375rem;
    margin: 8px;
    border-radius: 15px;
    background: black;
    color: white;
} */

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 25.375rem;
    height: 1.375rem;
    line-height: 0.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
    display: flex;
    top: 39px;
    margin-left: 76%;
}
.inbox-widget .inbox-item .inbox-item-img {
    display: block;
    float: left;
    margin-right: 29px;
    width: 40px;
}
#listing:hover {
  background-color: black;
}
#style-3::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;
}
</style>
<div class="row">
  <div class="col-xl-4 mt-3">
    <div class="card-box" style="background: #1f1f41;height: 96%;">
        <div class="form-group has-search">
            <input type="text" class="form-control" placeholder="{{__('Search...')}}" style="background: black;font-weight: 600;border-radius: 20px;width: 99%;color: white;">
            <span class="fa fa-search form-control-feedback" ></span>
        </div>
        <div class="slimScrollDiv" id="style-3" style="overflow-y: hidden;scrollbar-width: none;"><div class="inbox-widget slimscroll" style="max-height: 391px;min-height: 391px;scrollbar-width: none;">
             @foreach($users as $user)
            <div class="inbox-item" id="listing">
                <div class="row">
                    <div class="col-md-9">
                        <div class="inbox-item-container d-flex align-items-center">
                            <div class="inbox-item-img image">
                                <!-- <div class="inbox-item-img"><img src="assets/images/users/user-2.jpg" class="rounded-circle" alt=""> -->
                                    @if($user['profile'] == '')
                                      <div class="img-wrap">
                                        <img src="{{ url('images/default.png') }}" class="rounded-circle img-fluid" style="border-radius: 17px;flex: 1 1 auto;padding: 0px;border: solid;margin-top: -1px;border-radius: 30%!important;" >  
                                      </div>
                                        
                                    @else
                                        <div class="img-wrap">
                                            <img src="{{ env('S3_IMAGE_URL').'carrier/'.$user['profile'] }}" class="img-rounded img-fluid" style="border-radius: 17px;flex: 1 1 auto;padding: 0px;border: solid;margin-top:  -1px;border-radius: 30%!important;" >
                                        </div>
                                        
                                    @endif
                            </div>
                            <div class="text-wrap">
                              <p class="inbox-item-author" style="color: #ffffff;">{{$user['name']}}</p>
                              <p class="inbox-item-text">{{ date('F d,Y h:i A',strtotime($user['created_at'])) }}</p>  
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-3">
                        <p class="inbox-item-date">
                         <a href="javascript:(0);"  data-receiver="{{$user['id']}}" class="load-chat btn btn-sm btn-link text-dark font-13" style="color: #ffffff;"> <b style="color: #ffffff;">{{$user['type']}} </b></a>
                        </p>
                    </div>
                </div>

                 
                
            </div>
            @endforeach
        </div>
            <div class="slimScrollBar" style="background: rgb(158, 165, 171); width: 8px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 361.898px;"></div>
            <div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
    </div> <!-- end inbox-widget -->

    </div> <!-- end card-box-->
</div>
    <div class="col-xl-8 mt-3">
        <div class="card-box">
            <div class="col-xl-12">
                <h4 class="header-title mb-3">{{__('Order ID')}} #{{$booking['id']}}</h4>
                {{-- <h3 class="header-title mb-3" style="margin: -15px;margin-left: 0px;">{{$user['name']}} &nbsp;&nbsp;&nbsp; <span class="font-12" style="color: #adb5bd;font-weight: 200;"> {{$user['type']}} </span></h3>  --}}
                <div class="search" style="margin-top: -60px;">
                    <i class="fa fa-search" style="text-align: center;
                    margin-left: 92%;"></i>&nbsp;&nbsp;&nbsp;<i class="bi bi-three-dots-vertical"></i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                    <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/></svg>
                </div>
            </div>
            <hr>
                <div class="slimScrollDiv" style="">
                    <ul class="conversation-list slimscroll" style="max-height: 350px;min-height: 350px;overflow-y: scroll;">
                        @if(isset($messages) && !empty($messages))
                        @foreach($messages as $message)
                            <li class="clearfix @if($message['sender_id'] == $sender_id) odd @endif">
                                <div class="chat-avatar">
                                    @if($user['profile'] == '')
                                        <img src="{{ url('images/default.png') }}" class="rounded-circle img-fluid" style="border-radius: 17px;flex: 1 1 auto;padding: 0px;border: solid;margin-top: 7px;border-radius: 30%!important;" >
                                    @else
                                        <img src="{{ env('S3_IMAGE_URL').'carrier/'.$user['profile'] }}" class="img-rounded img-fluid" style="border-radius: 17px;flex: 1 1 auto;padding: 0px;border: solid;margin-top:  7px;border-radius: 30%!important;" >
                                    @endif
                                    <i>{{ date('H:i',strtotime($message['created_at'])) }}</i>
                                </div>
                                <div class="conversation-text">
                                    <div class="ctext-wrap">
                                        <i>{{ $message['sender_name'] }}</i>
                                        <p>{{ $message['message'] }}</p>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                        @endif
                    </ul>

                    <div class="slimScrollBar" style="background: rgb(158, 165, 171); width: 8px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 334.699px;">
                    </div>
                    <div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;">
                    </div>

                </div>
                <div class="row">
                    <div class="col">
                        <input type="text" class="form-control chat-input send_input" placeholder="Enter your text">
                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-danger chat-send btn-block waves-effect waves-light">{{__('Send')}}</button>
                    </div>
                </div>
        </div> <!-- .chat-conversation -->
    </div> <!-- end card-box-->
</div>
</div>   

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        var sender_id = `{{$sender_id}}`;
        var sender_name = `{{$sender_name}}`;
        var receiver_id = `{{$receiver_id}}`;
        
        $("#listing").click(function(){
            $("#chat").show();
        });
        $('.send_input').keypress(function (e) {
        var key = e.which;
            if(key == 13)  // the enter key code
            {
                var date = new Date();
                var today = new Date(date.toLocaleString("en-US", { timeZone: "Asia/Kolkata" }));
                var hour = today.getHours();
                var min = today.getMinutes();
                if(hour<10) {hour="0"+hour;}
                if(min<10) {min="0"+min;}

                var message = $('.chat-input').val();
                if(message == "")
                    return false;
                socket.emit('send_message', {
                    sender_id: sender_id,
                    receiver_id: receiver_id,
                    message:message,
                    type:'driver'
                });
                var node = ` <li class="clearfix odd">
                        <div class="chat-avatar">
                            
                                <img src="{{ url('images/default.png') }}" class="rounded-circle" style="border-radius: 17px;flex: 1 1 auto;padding: 0px;height: 47px;width: 52px;border: solid;margin-top: 7px;border-radius: 30%!important;" >
                            
                            <i>`+hour+`:`+min+`</i>
                        </div>
                        <div class="conversation-text">
                            <div class="ctext-wrap">
                                <i>`+sender_name+`</i>
                                <p>`+message+`</p>
                            </div>
                        </div>
                    </li>`;
                 $('.conversation-list').append(node); 
                //  window.scrollTo(0,$('.slimscroll').scrollHeight);
                //  $(".slimscroll").animate({ scrollTop: $('.slimscroll')[0].scrollHeight}, 1000);
                var d = $('.slimscroll');
                d.scrollTop(d.prop("scrollHeight") + 100000000);
                $('.send_input').val("");
                
            }
        });   
        $('.chat-send').click(function(e){
           
            var date = new Date();
            var today = new Date(date.toLocaleString("en-US", { timeZone: "Asia/Kolkata" }));
            var hour = today.getHours();
            var min = today.getMinutes();
            if(hour<10) {hour="0"+hour;}
            if(min<10) {min="0"+min;}

            var message = $('.chat-input').val();
            if(message == "")
                return false;
            socket.emit('send_message', {
                sender_id: sender_id,
                receiver_id: receiver_id,
                message:message,
                type:'driver'
            });
            var node = ` <li class="clearfix odd">
                    <div class="chat-avatar">
                        
                            <img src="{{ url('images/default.png') }}" class="rounded-circle" style="border-radius: 17px;flex: 1 1 auto;padding: 0px;height: 47px;width: 52px;border: solid;margin-top: 7px;border-radius: 30%!important;" >
                        
                        <i>`+hour+`:`+min+`</i>
                    </div>
                    <div class="conversation-text">
                        <div class="ctext-wrap">
                            <i>`+sender_name+`</i>
                            <p>`+message+`</p>
                        </div>
                    </div>
                </li>`;
             $('.conversation-list').append(node); 

            //  window.scrollTo(0,$('.slimscroll').scrollHeight);
            //  $(".slimscroll").animate({ scrollTop: $('.slimscroll')[0].scrollHeight}, 1000);
            var d = $('.slimscroll');
             d.scrollTop(d.prop("scrollHeight") + 10000);
             $('.chat-input').val("");
        })

        $('.load-chat').click(function(){
            var receiver_id = $(this).data('receiver');
            $.post(`{{route('shipper.shipper.chat')}}`,{receiver_id:receiver_id},function(res,status){

                if(res.data.length>0)
                {
                    $('.conversation-list').html("");
                    $.each(res.data, function( index, value ) {
                        var class_name = "";
                       if(value['sender_id'] == sender_id)
                        {
                            class_name = "odd";
                        }
                        /* var hour_min = new Date(value['created_at']);
                        var hour = hour_min.getHours();
                        var min = hour_min.getMinutes();
                        if(hour<10) {hour="0"+hour;}
                        if(min<10) {min="0"+min;} */

                        var html = `<li class="clearfix `+class_name+`">
                                <div class="chat-avatar">
                                   
                                        <img src="{{ url('images/default.png') }}" class="rounded-circle" style="border-radius: 17px;flex: 1 1 auto;padding: 0px;height: 47px;width: 52px;border: solid;margin-top: 7px;border-radius: 30%!important;" >
                                   
                                    <i>`+value['created_at']+`</i>
                                </div>
                                <div class="conversation-text">
                                    <div class="ctext-wrap">
                                        <i>`+value['sender_name']+`</i>
                                        <p>`+value['message']+`</p>
                                    </div>
                                </div>
                            </li>`;
                         $('.conversation-list').append(html);   
                    });
                }
            });
        })
    });

</script>

@endsection