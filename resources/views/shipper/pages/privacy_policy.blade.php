@extends('shipper.layouts.master')
@section('content')
<style>
.content-page {
    margin-left: 128px;
    padding: 0px 30px 29px 4px;
    min-height: 82vh;
    margin-top: 40px;
    width: 113%;
    text-align: justify;
    font-family: Arial, Helvetica, sans-serif;
  
      font-size: 17px;

}
/*body {
  
}*/
</style>
<html>
<body>
        <div class="content-page">
        	@include('admin.include.flash-message')
	    	<div class="col-sm-8 text-left"> 
	      		<h1>{{__('Privacy Policy')}}</h1>
	      		<p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	      		<hr>
	      		<h1>{{__('Test')}}</h2>
	      		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	    	</div>
	    </div>
    </body>
</html>
@endsection