<link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<style type="text/css">
 #se-pre-icon {
   position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url('//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Phi_fenomeni.gif/50px-Phi_fenomeni.gif') 
              50% 50% no-repeat rgb(249,249,249);
   opacity: 0.8;

}
-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<script type="text/javascript">
$(function() {
    var datatable_url = "{{$dateTableUrl}}";
    var is_page_length = true;
    var is_search = true;
    var is_binfo = true;
    var res = true;
    if(datatable_url == "{{url('shipper/manage-shipment')}}" || datatable_url == "{{ url('shipper/address') }}"){
        is_page_length = false;
        is_search = false;
        is_binfo = false;
        res = false;
    }
    var table = $('#{{$dataTableId}}').DataTable({
            processing: true,
            serverSide: true,
            responsive:res,
            searching:is_search,
            lengthChange:is_page_length,
            bInfo:is_binfo,
            ajax: "{{$dateTableUrl}}",
            columns: JSON.parse(`<?php echo json_encode($dateTableFields);?>`),
            oLanguage: {sProcessing: "<div id='se-pre-icon'></div>"}
        });    
    
    // var table = $('#{{$dataTableId}}').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     responsive:true,
    //     ajax: "{{$dateTableUrl}}",
    //     columns: JSON.parse(`<?php echo json_encode($dateTableFields);?>`)
    // });

    $('#filter-form').on('submit', function(e) {
        var obj = {};
        var data = $(this).serialize().split("&");
        for (var key in data) {
            obj[data[key].split("=")[0]] = data[key].split("=")[1];
        }
        $.ajaxSetup({
            data: obj
        });
        table.draw();
        e.preventDefault();
    });

    $('.filter_btn').on('click', function(e) {
        var status = $(this).attr('data-value');
        $('.filter_btn').removeClass('active');
        $(this).addClass('active');
        $.ajaxSetup({
          data: {status:status}
        });
        table.draw();
        e.preventDefault();
    });

    var status = '';
    var search = '';
    <?php if(isset($url_param) && !empty($url_param) || isset($search_param) && !empty($search_param)){?>
    // $('.filter_btn').on('click', function(e) {
        status = '<?=$url_param?>';
        search = '<?=$search_param?>';
        // var table = $('#<?=$dataTableId?>');
        // $('.filter_btn').removeClass('active');
        // $(this).addClass('active');
        $.ajaxSetup({
          data: {status:status,searching_data:search}
        });
        table.draw();
        // e.preventDefault();
    // });
    <?php } ?>
    $('.sorting_data').on('click', function(e) {

        var sorting_date = $(this).attr('data-sorting');
        console.log(sorting_date);
        var zero_value=$('#zero_value').attr('data-id');
       
        if(zero_value==0){
           $('#zero_value').attr("data-id",1);
          
        }else if(zero_value==1){
          $('#zero_value').attr('data-id',0);
        }
        console.log(zero_value);
        $.ajaxSetup({
          data: {sorting_date:sorting_date,
                zero_value:zero_value
          },
        });
        table.draw();
         $('.cardData').hide();
        e.preventDefault()
    });
});
</script>

