@include('shipper.layouts.header')
            <div class="sign-page">
                <div class="container-fluid p-0">
                    <div class="row">
                      <div class="col-md-7">
                        <div class="img-wrapper">
                           <img src="{{ URL::asset('shipper/images/car.png')}}" class="img-fluid">
                        </div>
                        
                      </div>
                      <div class="col-md-5">
                         <div class="row">
                            
                            <div class="col-md-10 p-0">
                            @include('admin.include.flash-message')
                               <div class="sign-in-side">
                                  <h1>{{ __('Sign in to Continue')}}</h1>
                                  <ul class="nav nav-tabs justify-content-center mb-3 mb-md-4 mb-lg-5">
                                        <li class="nav-item">
                                            <a href="#shipper" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                {{__('Shipper')}}
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#carrier" data-toggle="tab" aria-expanded="true" class="nav-link">
                                                {{__('Carrier')}}
                                            </a>
                                        </li>
                                 </ul>
                                 <div class="tab-content">
                                  <div class="tab-pane show active" id="shipper">
                                      <form action="{{ route('shipper.login.post') }}" method="POST">
                                      @CSRF
                                         <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                             <input type="text" id="email" parsley-trigger="change" data-parsley-required-message="Please Enter Email or Phone" required value="{{ old('email') }}" name="email" class="form-control" placeholder="{{__('Email or Phone')}}#" style="padding: 1px 111px 33px 46px;">
                                             <div class="icon">
                                                   <img src="{{ URL::asset('shipper/images/user-fill.png')}}" class="img-fluid" >                                                 
                                             </div>                              
                                              
                                          </div>
                                          <div class="form-group mb-4 ">
                                              <input type="password" data-parsley-required-message="Please Enter Password" name="password" parsley-trigger="change" required id="password" class="form-control" value="" placeholder="{{__('Password')}}" style="padding: 1px 111px 33px 46px;">
                                              <div class="icon">
                                              <a onclick="show()"><img src="{{ URL::asset('shipper/images/key.svg')}}" class="img-fluid" id="EYE">     </a>                                                
                                              </div>                                              
                                          </div>
                                          <a href="{{ route('shipper.forgot.password') }}" class="forgot">{{__('Forgot Password')}}?</a>

                                          <div class="form-group  mt-5">
                                             <button type="submit" href="javascript:;" class="btn black-btn">{{__('Sign In')}}</button>
                                          </div>
                                      </form>
                                  </div>
                                  <div class="tab-pane" id="carrier">
                                         <form action="{{ route('carrier.login.post') }}" method="POST">
                                      @CSRF
                                         <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                             <input type="text" id="email" parsley-trigger="change" data-parsley-required-message="Please Enter Email or Phone" required value="{{ old('email') }}" name="email" class="form-control" placeholder="{{__('Email or Phone')}}">
                                             <div class="icon">
                                                   <img src="{{ URL::asset('shipper/images/user-fill.png')}}" class="img-fluid" >                                                 
                                             </div>                              
                                              
                                          </div>
                                          <div class="form-group mb-4 ">
                                              <input type="password" data-parsley-required-message="Please Enter Password" name="password" parsley-trigger="change" required id="passwordData" class="form-control" value="" placeholder="{{__('Password')}}">
                                              <div class="icon">
                                              <a onclick="carriershow()"><img src="{{ URL::asset('shipper/images/key.svg')}}" class="img-fluid EYE" id="EYEData">     </a>                                                
                                              </div>                                              
                                          </div>
                                          <a href="{{ route('shipper.forgot.password') }}" class="forgot">{{__('Forgot Password')}}?</a>

                                          <div class="form-group  mt-5">
                                             <button type="submit" href="javascript:;" class="btn black-btn">{{__('Sign In')}}</button>
                                          </div>
                                      </form>                            
                                  </div>                             
                               </div>
                               </div>
                            </div>
                             <div class="d-sm-none col-md-2 d-md-block"></div>
                         </div>
                    
                      </div>
                    </div>
                 </div>

            </div>


   @include('shipper.layouts.footer_script')

   <script>
      function show() {
         var a = document.getElementById("password");
         var b = document.getElementById("EYE");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }
      function carriershow() {
         var a = document.getElementById("passwordData");
         var b = document.getElementById("EYEData");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }
   </script>

