@include('shipper.layouts.header')

            <div class="sign-page">
                <div class="container-fluid p-0">

                    <div class="row align-items-center">
                      <div class="col-md-7">
                         <div class="img-wrapper">
                            <img src="{{ URL::asset('shipper/images/car.png')}}" class="img-fluid">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="row">
                           <div class="col-md-10 p-0">
                              <div class="sign-in-side">
                                  <div class="verification">
                                             <div class="big-text">Verification is Pending </div>
                                             <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</p>
                                             <div class="form-group d-flex mt-3 mt-lg-5">
                                             <form method="POST" action="{{ route('shipper.verify', $user_data) }}">
                                              @CSRF
                                               <button type="submit" class="btn purple-btn">Complete Verification</button>
                                            </form>
                                            </div>
                                  </div>
                              </div>
                           </div>
                           <div class="d-sm-none col-md-2 d-md-block"></div>
                        </div>                       
                      </div>
                    </div>
                 </div>
            </div>
            
            @include('shipper.layouts.footer_script')