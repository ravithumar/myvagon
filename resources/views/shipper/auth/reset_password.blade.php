@include('shipper.layouts.header')
            <div class="sign-page">
                <div class="container-fluid p-0">

                    <div class="row">
                      <div class="col-md-7">
                         <div class="img-wrapper">
                            <img src="{{ URL::asset('shipper/images/car.png')}}" class="img-fluid">
                        </div>
                      </div>
                      <div class="col-md-5">
                         <div class="row">
                             <div class="col-md-10 p-0">
                               <div class="sign-in-side">
                                  <div class="forgot-block">
                                  @include('admin.include.flash-message')
                                  <form action="{{ route('shipper.forgot.password.post') }}" id="basic-form-forgot" method="POST" accept-charset="utf-8">
                                        @CSRF
                                            <div class="img-wrap text-center mb-md-4 mb-xl-5">
                                                <img src="{{ URL::asset('shipper/images/forgot-pass-pic.svg')}}" class="img-fluid"> 
                                            </div>
                                            <h1>Reset Password</h1>
                                            
                                            <div class="col-md-8 col-lg-6 px-0">
                                            <div class="form-group relative">
                                                <label class="text-white">Current Password</label>
                                                <input type="password" name="old_password" value="{{ old('old_password') }}" placeholder="Enter Current Password" class="form-control" required>
                                                <!-- <button type="submit" class="show-password"><img src="{{ URL::asset('website/images/show-password.jpg')}}" width="30" height="20" alt=""></button> -->
                                            </div>
                                            <div class="form-group relative">
                                                <label class="text-white">New Password</label>
                                                <input type="password" name="password" value="{{ old('password') }}" placeholder="Enter New Password" class="form-control" required>
                                                <!-- <button type="submit" class="show-password"><img src="{{ URL::asset('website/images/show-password.jpg')}}" width="30" height="20" alt=""></button> -->
                                            </div>
                                            <div class="form-group relative">
                                                <label class="text-white">Confirm Password</label>
                                                <input type="password" name="password_confirm" value="{{ old('password_confirm') }}" placeholder="Enter Confirm Password" class="form-control" required>
                                                <!-- <button type="submit" class="show-password"><img src="{{ URL::asset('website/images/show-password.jpg')}}" width="30" height="20" alt=""></button> -->
                                            </div>
                                            <div class="text-center pt-4 pb-5">
                                                <button type="submit" title="" class="btn-green">Submit</button>
                                            </div>
                                        </div>

                                            <div class="form-group d-flex send-otp">
                                               <button type="submit"  class="btn black-btn">Send Otp</button>
                                            </div>
                                       </form> 
                                  </div>
                               </div>  
                             </div>
                             <div class="d-sm-none col-md-2 d-md-block"></div>                            
                         </div>                       
                      </div>
                    </div>
                 </div>

            </div>

              <!-- end #basicwizard-->
              @include('shipper.layouts.footer_script')

               
