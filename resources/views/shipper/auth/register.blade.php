
@include('shipper.layouts.header')

<style>

</style>

<div class="registration-form pt-4 pb-5">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-sm-9">
            @include('admin.include.flash-message')
              <div class="container">
             

                <div class="row">
                  <div class="col-md-2 col-lg-2">
                    <div class="left-img">
                      <img src="{{ URL::asset('shipper/images/regi-form-01.svg')}}" class="img-fluid">
                    </div>
                  </div>
                 
                  <div class="col-md-8 col-lg-8">
                    <div class="step-info">
                    
                      <h2 class="title mt-0">{{__('Registration Form')}} </h2>
                      <p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in  laying out print, graphic or web designs</p>
                      <div id="basicwizard">
                        <div class="personal-details">
                          <ul class="d-flex">
                            <li class="@if(isset($data) && !empty($data) && $data != 'step1') width-zero @endif">

                              <a href="#basictab1" data-toggle="tab" class="d-flex @if(isset($data) && !empty($data) && $data == 'step1') active @endif">
                                <div class="icon" >
                                  <img src="{{ URL::asset('shipper/images/user-pink.svg')}}" class="img-fluid" >
                                </div>
                                <div class="text-wrap">
                                  <h3 >Personal Details</h3>
                                  <p >Please provide your details</p>
                                </div>
                              </a>
                            </li>
                            <li class="@if(isset($data) && !empty($data) && $data == 'step3') width-zero @else @if($data == "step2") add-width @endif @endif" >
                              <a href="#basictab2" data-toggle="tab" class="d-flex @if(isset($data) && !empty($data) && $data == 'step2') active @endif">
                                <div class="icon">
                                  <img src="{{ URL::asset('shipper/images/bxs-buildings.svg')}}" class="img-fluid">
                                </div>
                                <div class="text-wrap" >
                                  <h3 >Company Details</h3>
                                  <p >Please provide your Company details</p>
                                </div>
                              </a>
                            </li>
                            <li class=""  >
                              <a href="#basictab3" data-toggle="tab" class="d-flex @if(isset($data) && !empty($data) && $data == 'step3') active @endif">
                                <div class="icon">
                                  <img src="{{ URL::asset('shipper/images/truck.svg')}}" class="img-fluid">
                                </div>
                                <div class="text-wrap">
                                  <h3>Shipping Details</h3>
                                  <p style="width: 274%;">Please provide your Shipping details</p>
                                </div>
                              </a>
                            </li>
                          </ul>
                        </div>
                        
                        <div class="tab-content b-0 mb-0">
                          <div class=""  id="basictab1" >
                            <div class="col-sm-12" @if($data == "step1") style="display:block" @else style="display:none"  @endif >
                              <form method="POST" action="{{ route('register.store') }}">
                              @CSRF
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/user.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="text" name="first_name" minlength="2" maxlength="70" parsley-trigger="change" required value="{{ ucfirst(old('first_name')) }}" id="first_name" class="form-control" placeholder="First Name" data-parsley-required-message="Please enter first name." required="" style="text-transform: capitalize;" required data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in First Name." data-parsley-trigger="keyup">
                                      @error('first_name')
                                          <div class="error">{{ $message }}</div>
                                      @enderror
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/user.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="text" name="last_name"  maxlength="70"  minlength="2" parsley-trigger="change" data-parsley-required-message="Please enter Last name."  required value="{{ old('last_name') }}" id="last_name"  class="form-control" placeholder="Last Name" style="text-transform: capitalize;" required data-parsley-pattern="^[a-zA-Z ]+$" data-parsley-pattern-message="Please enter alphabets only in Last Name." data-parsley-trigger="keyup">
                                      @error('last_name')
                                          <div class="error">{{ $message }}</div>
                                      @enderror
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/work-mail.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="email" name="email"  maxlength="60" parsley-trigger="change" required value="{{ old('email') }}" id="email" class="form-control" placeholder="Work Email" style="padding: 1px 111px 33px 63px;" data-parsley-required-message="Please enter email." required="" data-parsley-type-message="Please enter a valid email" data-parsley-trigger="keyup">
                                      @error('email')
                                          <div class="error">{{ $message }}</div>
                                      @enderror
                                      <div id="emailerror" class="text-danger"></div>
                                      <input type="hidden" name="email_verify_register" id="email_verify_register" value="@if(!empty(old('email_verify_register'))) {{old('email_verify_register')}} @else 0 @endif">
                                      <div class="icon verified" id="verify">
                                        <img src="{{ URL::asset('shipper/images/verified.svg')}}" class="img-fluid verify-img" @if(old('email_verify_register') == 1)style="display: block;" @else style="display: none;" @endif>
                                        <!-- <a href="javascript:;"  id="verify" onclick="myFunction()" class="verify verify-img2" data-toggle="modal" data-target="#vefifyphone" style="right: -10px;">Verify</a> -->
                                        <a href="javascript:;"  id="verify" onclick="EmailVerify()" class="verify verify-img2"  style="@if(old('email_verify_register') == 1) display:none;@endif">@if(old('email_verify_register') == 1) Verified @else Verify @endif</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                	<div class="col-lg-2">
                                      	<div class="form-group mb-1 mb-sm-1 mb-md-1 mb-lg-3 mb-xl-2">
	                                        <select   style="padding: 2px !important;" name="country"  placeholder="Select Country" class="form-control" parsley-trigger="change" required value="{{ old('country') }}" id="country">
                            				@if(isset($countries))
	                                            @foreach($countries as $country)
	                                              <option value="{{ $country->id }}" selected>+ {{ $country->phonecode }}</option>
	                                            @endforeach
                              				@endif
                            				</select>
	                                        @error('country')
	                                          <div class="error">{{ $message }}</div>
	                                        @enderror
                                    	</div>
                                    </div>
                                    <div class="col-lg-10">
                                      	<div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                      		<div class="icon">
                                      			<img src="{{ URL::asset('shipper/images/call-icon.svg')}}" class="img-fluid">
                                      		</div>
                                      		<input type="call" data-parsley-validation-threshold="1" data-parsley-maxlength="10" data-parsley-trigger="keyup" 
                                      		data-parsley-type="digits" onkeypress="return IsNumeric(event);" name="phone"   maxlength="15" data-parsley-type="digits" parsley-trigger="change" required value="{{ old('phone') }}" id="phone"  class="form-control" placeholder="Phone Number (Optional)" data-parsley-required-message="Please enter Phone number." required="" autocomplete="off">
	                                      	@error('phone')
		                                          <div class="error">{{ $message }}</div>
		                                    @enderror
                                      		<div id="phoneError" class="text-danger"></div>
                                      			<input type="hidden" name="phone_verify_register" id="phone_verify_register" value="@if(!empty(old('phone_verify_register'))) {{old('phone_verify_register')}} @else 0 @endif">
                                      				<div class="icon verified">
				                                        <img src="{{ URL::asset('shipper/images/verified.svg')}}" class="img-fluid verify-img-phone" @if(old('phone_verify_register') == 1)style="display: block;" @else style="display: none;" @endif>
				                                        <a href="javascript:;"  class="verify btn-phone" onclick="phone_verify()" @if(old('phone_verify_register') == 1) style="display:none;" @endif>@if(old('verify-img-phone') == 1) Verified @else Verify @endif</a>
                                      				</div>
                                    	</div>
                                  	</div>
                                </div>
                                
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                       

                                        <img src="{{ URL::asset('shipper/images/password.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="password"  maxlength="40" data-parsley-minlength="8" data-parsley-minlength-message="Password must contain at least 8 characters " name="password"  parsley-trigger="change" required value="{{ old('password') }}" id="password1" data-required="true"  class="form-control" value="" placeholder="New password" data-parsley-required-message="Please enter Password." data-parsley-whitespace="squish" onkeyup="nospaces(this)" required="" >
                                      @error('password')
                                          <div class="error">{{ $message }}</div>
                                      @enderror

                                      <a class="icon pass" onclick="show()">
                                      <img src="{{ URL::asset('shipper/images/eye-slash-fill.svg')}}" class="img-fluid" id="EYE">   
                                      <!-- <img src="{{ URL::asset('shipper/images/eye-slash-fill.svg')}}" class="img-fluid"> -->
                                        <!-- <img src="{{ URL::asset('shipper/images/eye-fill.svg')}}" class="img-fluid"> -->
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col">
                                    <div class="form-group mb-md-4 mb-lg-3 mb-xl-5 ">
                                      <div class="icon">
                                        <img src="{{ URL::asset('shipper/images/password.svg')}}" class="img-fluid">
                                      </div>
                                      <input type="password" maxlength="40" name="confirm_password" data-parsley-minlength="8" data-parsley-minlength-message="Password must contain at least 8 characters "   parsley-trigger="change" data-parsley-equalto="#password1" data-parsley-equalto-message="Password and Confirm Password must be same " data-required="true" required value="{{ old('confirm_password') }}" id="confirm_password" class="form-control" value="" placeholder="Confirm password" data-parsley-required-message="Please confirm the password." onkeyup="nospaces(this)" required="" data-parsley-trigger="keyup">
                                      @error('confirm_password')
                                          <div class="error">{{ $message }}</div>
                                      @enderror

                                      <a onclick="show2()" class="icon pass">
                                        <img src="{{ URL::asset('shipper/images/eye-slash-fill.svg')}}" class="img-fluid" id="EYE2">
                                      </a>
                                    </div>
                                  </div>
                                </div>
                               
                                <ul class="wizard mb-0 row px-2 list-unstyled">
                                    <li class="col">
                                      <button type="submit" class="btn black-btn">Save and Continue</button>
                                    </li>
                                </ul>
                              
                              </form>


                            </div>

                          </div>
                          <div class="" id="basictab2">
                            <div class="col-sm-12" @if($data == "step2") style="display:block" @else style="display:none"  @endif>
                              <div class="company-form">
                              <form method="POST" data-parsley-validate action="{{ route('register.store.profile', $userid) }}">
                              @CSRF
                                  <div class="row">
                                    <div class="col">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                        <input type="text" maxlength="40" name="company_name"  parsley-trigger="change" required value="{{ old('company_name') }}" id="company_name" class="form-control" placeholder="Company Name">
                                        @error('company_name')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                      </div>
                                    </div>
                                  </div>

                              

                                  <div class="row">
                                    <div class="col-lg-6">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        <div class="col-lg-6">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        
                                        <select   style="padding: 2px !important;width: 283px;" name="country"  placeholder="Select Country" class="form-control" parsley-trigger="change" required value="{{ old('country') }}" id="country">
                                        <option value="">Choose country..</option>
                                        @if(isset($countries))
                                            @foreach($countries as $country)
                                              <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                        @error('country')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                        
                                      </div>
                                    </div>
                                        
                                        
                                      </div>
                                    </div>

                                    <div class="col-lg-6">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        
                                        <select  style="padding: 2px !important;" name="state"  placeholder="Select State" class="form-control" parsley-trigger="change" required  id="state">
                                        <option value="">Choose state..</option>
                                        @if(isset($states))
                                            @foreach($states as $state)
                                              <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                        @error('state')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                        
                                      </div>
                                      
                                    </div>
                                  </div>
                                  <div class="row">
                                    <select  style="padding: 2px !important;width: 39%;margin-left: 17px;" placeholder="Select City" name="city" class="form-control select2" data-bind="volunteer-new-final-title-field" class="dropkick_select" data-parsley-required="true"  parsley-trigger="change" required  id="city">
                                        
                                        <option selected="selected" value="">Choose city..</option>
                                            @if(isset($cities))
                                              @foreach($cities as $city)
                                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                                              @endforeach
                                            @endif
                                         
                                         
                                         
                                        </select>
                                        @error('city')
                                          <div class="error">{{ $message }}</div>
                                        @enderror

                                    <div class="col-lg-6">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                        <input type="text" data-parsley-maxlength="8" maxlength="8" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" id="simpleinput" class="form-control" onkeypress="return IsNumeric(event);" placeholder="Postcode" name="postcode"  parsley-trigger="change" required value="{{ old('postcode') }}" id="postcode" style="margin-left: 64px;">
                                        @error('postcode')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                      </div>
                                      
                                    </div>
                                  </div>
                                  
                                  
                                  <div class="row">
                                    <div class="col">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        <input type="ssn-number" data-parsley-maxlength="11" maxlength="11" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" class="form-control"  onkeypress="return IsNumeric(event);" name="ssn"  parsley-trigger="change" required value="{{ old('ssn') }}" id="ssn" class="form-control" placeholder="SSN Number">
                                        @error('ssn')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                      </div>
                                      
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col">
                                      <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                        <input type="call"   data-parsley-maxlength="15" maxlength="15" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" name="company_phone"  onkeypress="return IsNumeric(event);" parsley-trigger="change" required value="{{ old('company_phone') }}" id="company_phone" class="form-control" placeholder="Phone Number">
                                        @error('company_phone')
                                          <div class="error">{{ $message }}</div>
                                        @enderror
                                      </div>
                                    </div>
                                  </div>
                                
                                  <ul class="wizard mb-0 row px-2 list-unstyled">
                                      <li class="next col">
                                        <button type="submit"  class="btn black-btn second-step  register-form-next">Save and Continue</button>
                                      </li>
                                  </ul>
                                  
                                  
                                </form>
                              </div>
                              </div> <!-- end row -->
                            </div>

                            <div class="" @if($data == "step3") style="display:block" @else style="display:none"  @endif  id="basictab3">
                              <!-- <div class="col-sm-12">
                                <div class="shipping-pro-type">
                                  <h3>Shipping Product type</h3>
                                  <div class="ship-product-type">
                                    <select name="product-types" id="products" class="form-control"  multiple="multiple" placeholder="Product Type">
                                      <option value="">Product Type1 <span class="checkmark"></span></option>
                                      <option value="">Product Type2 <span class="checkmark"></span></option>
                                      <option value="">Product Type3 <span class="checkmark"></span></option>
                                      <option value="">Product Type4 <span class="checkmark"></span></option>
                                    </select>
                                  </div> -->
                                  
                                  <!-- Note the missing multiple attribute! -->
                                  
                                <!-- </div> -->
                                <form method="POST" action="{{ route('register.store.facility', $userid) }}">
                                 @CSRF

                                  <div class="main-container">
                                    <div class="reg-facility-address">
                                      <h3>Add Facility Address</h3>
                                      <div class="shipping-form">
                                        <div class="row">
                                          <div class="col">
                                            <div class="form-group mt-3 mt-lg-5 mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                              <input type="text" name="name"  maxlength="40" parsley-trigger="change" required value="{{ old('name') }}" id="name"  class="form-control" placeholder="Nickname">
                                              @error('name')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                              <input type="text" id="autocomplete" name="address" parsley-trigger="change" required value="{{ old('address') }}"  class="form-control autodata" placeholder="Address">
                                              <p style="color:red;display: none;font-size:14px" class="auto_message">Please select valid address</p>
                                              <input type="hidden" id="latitude" name="lat" value="">
                                              <input type="hidden" id="longitude" name="lng" value="">
                                              @error('address')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-lg-6">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                              
                                              <!-- <select   name="city" style="padding: 2px !important;"  class="form-control" parsley-trigger="change" required value="{{ old('city') }}" id="city" placeholder="Enter City." >
                                                <option value="">Select City</option>
                                                @if(isset($cities))
                                                    @foreach($cities as $city)
                                                      <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                    @endforeach
                                                @endif
                                              </select> -->
                                              <input type="text" name="city" id="city_demo" class="form-control" parsley-trigger="change" placeholder="Enter City" required value="{{ old('city') }}" >
                                              @error('city')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                              
                                            </div>
                                          </div>
                                          <div class="col-lg-6">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                              
                                              <!-- <select   name="state"  style="padding: 2px !important;" class="form-control" parsley-trigger="change" required value="{{ old('state') }}" id="state">
                                              <option value="">Select State</option>
                                              @if(isset($states))
                                                  @foreach($states as $state)
                                                    <option value="{{ $state->id }}">{{ $state->name }}</option>
                                                  @endforeach
                                              @endif
                                              </select> -->
                                               <input type="text" name="state" id="state_demo" class="form-control" placeholder="Enter State" required value="{{ old('state') }}">
                                              @error('state')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                              
                                            </div>
                                            
                                          </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="row">
                                          <div class="col-lg-6">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5">
                                              
                                             <!--  <select   name="country"  style="padding: 2px !important;" class="form-control" parsley-trigger="change" required value="{{ old('country') }}" id="country">
                                              <option value="">Select Country</option>
                                              @if(isset($countries))
                                                @foreach($countries as $country)
                                                  <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                                @endforeach
                                              @endif
                                              </select> -->
                                               <input type="text" name="country" id="country_demo" class="form-control" placeholder="Enter Country" required value="{{ old('country') }}"> 
                                              @error('country')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                              
                                            </div>
                                          </div>
                                          <div class="col-lg-6">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                              <input type="text" data-parsley-maxlength="8" maxlength="8" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" name="postcode2"  onkeypress="return IsNumeric(event);" parsley-trigger="change" required value="{{ old('postcode2') }}" id="postcode2"  class="form-control" placeholder="Postcode">
                                              @error('postcode2')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                              <!-- <div class="icon location" id="btnAction">
                                                <img src="{{ URL::asset('shipper/images/gps.svg')}}" class="img-fluid"  onClick="getLocation()">
                                              </div> -->
                                            </div>
                                            
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                              <input type="call" data-parsley-maxlength="15" onkeypress="return IsNumeric(event);" maxlength="15" data-parsley-trigger="keyup" 
                                      data-parsley-type="digits" name="phone_number"  parsley-trigger="change" required value="{{ old('phone_number') }}" id="phone_number" class="form-control" placeholder="Phone Number">
                                              @error('phone_number')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                            </div>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col">
                                            <div class="form-group mb-2 mb-sm-3 mb-md-4 mb-lg-3 mb-xl-5 ">
                                              
                                              <input type="email" name="address_email"  maxlength="40" parsley-trigger="change" required value="{{ old('address_email') }}" id="address_email" class="form-control" placeholder="Email">
                                              @error('address_email')
                                                  <div class="error">{{ $message }}</div>
                                              @enderror
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col">
                                        <div class="add-facility">
                                          <div class="form-group mb-2 mb-md-3 mb-lg-4">
                                            <input type="checkbox" name="restroom_service" id="Restrooms" checked="checked">
                                            <label for="Restrooms">Restrooms</label>
                                          </div>
                                          <div class="form-group mb-2 mb-md-3 mb-lg-4">
                                            <input type="checkbox" id="Food" name="food_service" checked="checked">
                                            <label for="Food">Food</label>
                                          </div>
                                          <div class="form-group mb-2 mb-md-3 mb-lg-4">
                                            <input type="checkbox" id="Driver Rest Area" name="rest_area_service" checked="checked">
                                            <label for="Driver Rest Area">Driver Rest Area</label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                
                                  </div>
                                  <div class="bottom-container">
                                   
                                    <div class="terms-conditions mt-4">
                                      <span class="text">Terms and Conditions</span>
                                      <div class="form-group d-flex mt-1">
                                        <input type="checkbox" id="terms"  name="temrs" parsley-trigger="change" data-parsley-required-message="Please select terms & Conditions" required>
                                        <label for="terms"></label>
                                        <p>Lorem Ipsum, Or Lipsum As It Is Sometimes Known</p>
                                      </div>
                                    </div>
                                      <ul class="wizard mb-0 row px-2 list-unstyled">
                                        <li class="next col">
                                          <button type="submit" class="btn purple-btn register-form-next">Save and Continue</button>
                                        </li>
                                    </ul>
                                  </div>
                                  
                                </form>
                                </div> <!-- end row -->
                              </div>
                             
                              </div> <!-- tab-content -->
                              </div> <!-- end #basicwizard-->
                            </div>
                          </div>
                          <div class="col-md-2 col-lg-2">
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- end #basicwizard-->

       

@include('shipper.layouts.footer_script')


<!-- static opt -->
<input type="hidden" id="optcode" name="otpcode" value="456789">

<div class="modal fade" id="vefifyphone" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Enter OTP Sent To Your Email Address</div>
                      <div class="verify-block">
                        <div class="container" style="display:none;" id="myAlert">
                            <div class="alert alert-success alert-dismissable" id="myAlert2" >
                              
                                Your Resend OTP Send Successfully.
                            </div>

                        </div>
                              <div class="otps d-flex mb-3">                             
                                  <form method="post" class="digit-group mt-md-3 mt-lg-4" data-group-name="digits" data-autosubmit="false" autocomplete="off" >
                                               
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-1" name="digit-1"/>
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-2" name="digit-2"  data-previous="digit-1" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-3" name="digit-3"  data-previous="digit-2" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-4" name="digit-4"  data-previous="digit-3" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-5" name="digit-5" data-previous="digit-4" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="digit-6" name="digit-6" data-previous="digit-5" />
                                               <hr><div class="digit-error text-danger" id="digit-error"></div>
                              
                             </div>
                       <a href="#" class="resend" data-status="1" id="modalButton" onclick="showAlert();">Resend OTP</a>
                       <button type="button" onclick="verify_email_otp()" class="btn black-btn mt-4 mt-md-5">Verify</button>
                       </form>
                    </div>
              </div>
         </div>
    </div> 
</div>


<div class="modal fade" id="phone_otp_verify" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content"> 
             <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-backdrop="static" data-keyboard="false">
             <span aria-hidden="true">&times;</span>
             </button>
              <div class="modal-body">         
                      <div class="title mb-3 mb-sm-4">Enter OTP Sent To Your <div id="phone_fatch"></div></div>
                      <div class="verify-block">
                              <div class="otps d-flex mb-3">                             
                                  <form method="post" class="digit-group mt-md-3 mt-lg-4 verify-email" data-group-name="digits" data-autosubmit="false" autocomplete="off" >
                                               
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-1" name="digit-1"  />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-2" name="digit-2"  data-previous="phone-1" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-3" name="digit-3"  data-previous="phone-2" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);"  class="email-verify otp" maxlength="1" id="phone-4" name="digit-4"  data-previous="phone-3" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-5" name="digit-5"  data-previous="phone-4" />
                                               <input type="text"  required onkeypress="return IsNumeric(event);" class="email-verify otp" maxlength="1" id="phone-6" name="digit-6" data-previous="phone-5" />
                                               <hr><div class="phone_error text-danger" id="phone_error"></div>

                              
                             </div>
                       <!-- <a href="#" onclick="getProgress()" class="resend">Resend OTP</a> -->
                       <button type="button" onclick="verify_phone_otp()" class="btn black-btn mt-4 mt-md-5">Verify</button>
                       </form>
                    </div>
              </div>
         </div>
    </div> 
</div>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>
<script type="text/javascript">
  var phone_number = window.intlTelInput(document.querySelector("#phone"), {
  separateDialCode: true,
  preferredCountries:["GR"],
  hiddenInput: "full",
  // utilsScript: "//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/utils.js"
});

$("form").submit(function() {
  var full_number = phone_number.getNumber(intlTelInputUtils.numberFormat.E164);
$("input[name='phone_number[full]'").val(full_number);
  alert(full_number)
  
});
</script> -->
<input type="hidden" name="is_address" id="is_address" value="0">

<script type="text/javascript">
  $('#form').parsley({
  excluded: 'input[type=button], input[type=submit], input[type=reset], :hidden'
});
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?=env('MAP_KEY')?>&libraries&libraries=places"></script>
<script>
    function IsNumeric(event) {
      event = (event) ? event : window.event;
      var charCode = (event.which) ? event.which : event.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
  }
$('input.email-verify.otp').on('keyup', function() {
    if ($(this).val()) {
        $(this).next().focus();
    }
});
      function show() {
         var a = document.getElementById("password1");
         var b = document.getElementById("EYE");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }

      function show2(){
        
        var a = document.getElementById("confirm_password");
         var b = document.getElementById("EYE2");
         if (a.type == "password") {
            a.type = "text";
           
         } else {
            a.type = "password";
           
         }
      }

      $('.digit-group').find('input').each(function() {
  $(this).attr('maxlength', 1);
  $(this).on('keyup', function(e) {
    var parent = $($(this).parent());
    
    if(e.keyCode === 8 || e.keyCode === 37) {
      var prev = parent.find('input#' + $(this).data('previous'));
      
      if(prev.length) {
        $(prev).select();
      }
    } else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
      var next = parent.find('input#' + $(this).data('next'));
      
      if(next.length) {
        $(next).select();
      } else {
        if(parent.data('autosubmit')) {
          parent.submit();
        }
      }
    }
  });
});

function nospaces(t){
  if(t.value.match(/\s/g)){
    t.value=t.value.replace(/\s/g,'');
  }
}

var placeSearch;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };
    
      google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();

            var address = pickup_place.address_components;
            var new_address=pickup_place.formatted_address;

            var  value = new_address.split(",");
            count=value.length;
            country=value[count-1];
            state=value[count-2];
            city=value[count-3];
            var z=state.split(" ");

            $('#city_demo').val(city);
            $('#state_demo').val(z[1]);
            $('#country_demo').val(country);
            $('#postcode2').val(z[2]);

            var street = city = state = '';
            $.each(address, function(i,val){
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
            });
            $('#latitude').val(pickup_place.geometry.location.lat());
            $('#longitude').val(pickup_place.geometry.location.lng());
            $('.auto_message').hide();
        });
    });

    google.maps.event.addDomListener(window, 'load', function () {
        var pickup_places = new google.maps.places.Autocomplete(document.getElementById('edit_address'));

        google.maps.event.addListener(pickup_places, 'place_changed', function () {
            var pickup_place = pickup_places.getPlace();
            var address = pickup_place.address_components;
            var street = city = state = '';
            $.each(address, function(i,val){
                if($.inArray('street_number', val['types']) > -1) {
                    street += val['long_name'];
                }
                if($.inArray('route', val['types']) > -1) {
                    street += ' '+val['long_name'];
                }
                if($.inArray('locality', val['types']) > -1) {
                    city += val['long_name'];
                }
                if($.inArray('administrative_area_level_1', val['types']) > -1) {
                    state += val['long_name'];
                }
            });
            $('#edit-latitude').val(pickup_place.geometry.location.lat());
            $('#edit-longitude').val(pickup_place.geometry.location.lng());
        });
    });

   </script>
   <script type="text/javascript">
     /*function locate(){
  document.getElementById("btnAction").disabled = true;
  document.getElementById("btnAction").innerHTML = "Processing...";
  if ("geolocation" in navigator){

    navigator.geolocation.getCurrentPosition(function(position){ 
      // console.log(alert("hey"));
      var currentLatitude = position.coords.latitude;
      var currentLongitude = position.coords.longitude;

      var infoWindowHTML = "Latitude: " + currentLatitude + "<br>Longitude: " + currentLongitude;
      var infoWindow = new google.maps.InfoWindow({map: map, content: infoWindowHTML});
      var currentLocation = { lat: currentLatitude, lng: currentLongitude };
      infoWindow.setPosition(currentLocation);
      document.getElementById("btnAction").style.display = 'none';
    });
  }
}*/
/*function getLocation(){
  
    if (navigator.geolocation){
        navigator.geolocation.getCurrentPosition(showPosition);
    }
    else{
        x.innerHTML="Geolocation is not supported by this browser.";
    }
}

function showPosition(position){
    lat=position.coords.latitude;
    lon=position.coords.longitude;

    displayLocation(lat,lon);
}
function displayLocation(latitude,longitude){
    var geocoder;
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(latitude, longitude);
    
    geocoder.geocode(
        {'latLng': latlng}, 
        function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add= results[0].formatted_address ;
                    alert(add);
                    var  value=add.split(",");
                    count=value.length;
                    country=value[count-1];
                    state=value[count-2];
                    city=value[count-3];
                    var z=state.split(" ");
                   

                    $("#city_demo").val(city);
                    $('#postcode2').val(z[2]);

                    $('#state_demo').val(z[1]);
                    $('#country_demo').val(country);
                    $('#autocomplete').val(value);
                   
                }
                else  {
                    x.innerHTML = "address not found";
                }
            }
            else {
                x.innerHTML = "Geocoder failed due to: " + status;
            }
        }
    );
}*/
$(document).ready(function(){
  $('body').on('change','.autodata',function(){
     $('#is_address').val('0');
    $('#address_valid').hide();
    
  var is_address = $("#is_address").val();
  console.log(is_address);
  if(is_address == '0')
  {
    $("#address_valid").show();
    $('.auto_message').show();
    $("#ship_data").attr('type','button');
  }
  
});

$("#address_valid").hide();
$("#ship_data").attr('type','submit');

})
   </script>
   <script type="text/javascript">
  
$(".verify-img2").hide();
     $("#email").keyup(function(){
    if($(this).val() == "") {
        $(".verify-img2").hide();
    } else {
        $(".verify-img2").show();
    }
});
     $(".btn-phone").hide();
    $("#phone").keyup(function(){
    if($(this).val() == "") {
        $(".btn-phone").hide();
    } else {
        $(".btn-phone").show();
    }
});
   </script>
   <script type="text/javascript">
    $('#alert_msg').hide();
     $(document).ready(function(){
    $('#modalButton').click(function(){
        $('#alert_msg').show();
    }) 
});
   </script>
<script type="text/javascript">
  function showAlert(){
  if($("#myAlert").find("div#myAlert2").length==0){
    $("#myAlert").append("<div class='alert alert-success alert-dismissable' id='myAlert2' > Your Resend OTP Send Successfully.</div>");
  }
  $("#myAlert").css("display", "");
}
$('a.d-flex').not('.active').css('pointer-events', 'none');
</script>