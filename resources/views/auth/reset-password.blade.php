<style type="text/css">
    .parsley-errors-list>li {
      list-style: none;
      color: #f1556c;
      margin-top: 5px;
      /* padding-left: 20px; */
      position: relative;
      margin-bottom: -21px;
  }
  </style>
<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                {{-- <x-application-logo class="w-20 h-20 fill-current text-gray-500" /> --}}
                <img src="http://3.66.160.72/shipper/images/logo.png" class="img-fluid">
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('password.update') }}">
            @csrf

            <!-- Password Reset Token -->
            <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <!-- Email Address -->
            <div>
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email', $request->email)" required readonly/>
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full" type="password" name="password" required data-parsley-required-message="Password field is required."/>
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                    type="password"
                                    name="password_confirmation" required data-parsley-required-message="Confirm password field is required." />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-button>
                    {{ __('Reset Password') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
<script src="http://3.66.160.72/shipper/js/jquery.min.js"></script>
<script src="http://3.66.160.72/assets/libs/parsleyjs/parsleyjs.min.js"></script>
<script>
    $("form").parsley({

triggerAfterFailure: 'focusout changed.bs.select'
});
</script>