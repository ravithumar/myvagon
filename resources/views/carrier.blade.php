@include('shipper.layouts.header')

    <div class="transport-page">
        <div class="money-with-us d-flex align-items-end position-relative" style="background: url(images/transport/carrier-bg.jpg) no-repeat;">
          <div class="container">
            <div class="row text-center">
              <div class="col-sm-12">
                <div class="content-center mx-auto position-relative">
                  <h1 class="font-76 font-weight-bold mb-4">Earn<span class="text-pink">Money</span> with us</h1>
                  <p class="font-24 text-black">Lorem ipsum dolor sit amet, consetetur sadipscing,<br> sed diam nonumy eirmod tempor</p>
                  <a href="javascript:void(0);" class="font-25 btn-black-cust text-uppercase text-center d-inline-block font-weight-500 mt-4 w-100 p-2 p-lg-3 position-relative">Join For Free</a>
                  <p class="last font-15">Lorem ipsum dolor sit amet, consetetur sadipscing,</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="features-area py-3">
              <div class="container">
                  <div class="row">
                    <div class="col text-center">
                      <h2 class="font-weight-bold font-43 mb-3">We have some awesome <span class="text-pink">features </span>for you</h2>
                      <p class="font-22 mx-auto">Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br> sed diam nonumy eirmod tempor</p>
                     </div>
                  </div>
                  <div class="row align-items-center pt-5">
                      <div class="col-md-4">
                        <div class="left-one pr-0 pr-xl-5">
                           <div class="block d-block d-lg-flex align-items-center mb-4 mb-lg-5 position-relative h-100  justify-content-end">
                                <span class="text pr-0 pr-lg-3 d-block font-22 font-weight-500 text-center text-lg-right">Availability and <br>capacity for all</span>
                                <span class="icon d-flex align-items-center justify-content-center rounded-circle my-3 my-lg-0 mx-auto mx-lg-0"><img src="images/transport/package.png" class="img-fluid"></span>                              
                            </div> 
                             <div class="block d-block d-lg-flex align-items-center mb-4 mb-lg-5 position-relative h-100  justify-content-end">
                                <span class="text pr-0 pr-lg-3 d-block font-22 font-weight-500 text-center text-lg-right">Supply chain management and transparency with insights and reporting</span> 
                                <span class="icon d-flex align-items-center justify-content-center rounded-circle my-3 my-lg-0 mx-auto mx-lg-0"><img src="images/transport/file-text.png" class="img-fluid"></span>
                            </div> 
                        </div>
                         
                      </div>
                      <div class="col-md-4">
                        <div class="img-wrapper"><img src="images/transport/feature-mobile.png" alt="" class="img-fluid mx-auto d-block"></div>
                      </div>
                      <div class="col-md-4">
                         <div class="right-one pl-0 pl-xl-5">
                           <div class="block d-block d-lg-flex align-items-center mb-4 mb-lg-5 position-relative h-100 justify-content-start"> 
                                <span class="icon d-flex align-items-center justify-content-center rounded-circle my-3 my-lg-0 mx-auto mx-lg-0"><img src="images/transport/file-text.png" class="img-fluid"></span>
                                <span class="text pl-0 pl-lg-3 d-block font-22 font-weight-500 text-center text-lg-left">Supply chain management and transparency with insights and reporting</span> 
                            </div> 
                           <div class="block d-block d-lg-flex align-items-center mb-4 mb-lg-5 position-relative h-100">
                                <span class="icon d-flex align-items-center justify-content-center rounded-circle my-3 my-lg-0 mx-auto mx-lg-0"><img src="images/transport/package.png" class="img-fluid"></span>                                 
                                <span class="text pl-0 pl-lg-3 d-block font-22 font-weight-500 text-center text-lg-left">Availability and<br> capacity for all</span> 
                            </div> 
                            
                        </div>
                      </div>
                  </div>
                  <div class="row text-center mt-3 mt-md-4">
                    <div class="col-sm-12">
                      <a href="javascript:;" class="sign-up font-24 text-center font-weight-600 mt-4 mx-auto d-inline-block bg-white px-3 py-2">Signup Now</a>
                    </div>
                  </div>
                  <div class="row text-center mt-3 mt-md-4">
                    <div class="col-sm-12">
                      <ul class="nav nav-tabs justify-content-center" id="transport" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active font-22 p-0" id="fleet-manager-tab" data-toggle="tab" href="#fleet-manager" role="tab" aria-controls="fleet-manager" aria-selected="true">Fleet Manager</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link p-0 font-22" id="driver-tab" data-toggle="tab" href="#driver" role="tab" aria-controls="driver" aria-selected="false">Driver</a>
                          </li>
                      </ul>
                    </div>
                  </div>
                  <div class="row justify-content-center align-items-center mt-3">
                    <div class="col-md-10">
                      <div class="tab-content p-3 p-md-4 p-lg-5 bg-white mt-3 mt-lg-5 text-center position-relative" id="myTabContent">
                          <div class="tab-pane fade show active" id="fleet-manager" role="tabpanel" aria-labelledby="home-tab">
                            <p class="font-24 font-weight-500">Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor Invidunt Ut Labore Et Dolore Magna Aliquyam Erat, Sed Diam Voluptua. At Vero Eos Et Accusam Et Justo Duo Dolores Et Ea Rebum. Stet Clita Kasd Gubergren, No Sea</p>
                          </div>
                          <div class="tab-pane fade" id="driver" role="tabpanel" aria-labelledby="profile-tab">
                           <p class="font-24 font-weight-500">Lorem Ipsum Dolor Sit Amet, Consetetur Sadipscing Elitr, Sed Diam Nonumy Eirmod Tempor 
                          </div>  
                      </div>

                    </div>
                  </div>
              </div>
        </div> 
        <div class="how-it-works pb-4">
           <div class="container">
             <div class="row text-center">
               <div class="col-sm-12">
                 <h2 class="font-weight-bold font-43 mb-3">How It <span class="text-pink">Works </span></h2>
                 <p class="font-22 mx-auto">Lorem ipsum dolor sit amet, consetetur sadipscing elitr,<br> sed diam nonumy eirmod tempor</p>
               </div>
             </div>
             <div class="row">
               <div class="col-sm-12">
                 <ul class="timeline mt-5">                  
                    <li class="timeline-inverted d-flex align-items-center justify-content-end">
                        <div class="timeline-image d-flex align-items-center justify-content-center"><div class="icon d-flex align-items-center justify-content-center"><img class="img-fluid" src="images/transport/icons/add-user.png" alt=""></div></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="font-24 font-weight-600 dark-purple">SignUp  with <span class="text-pink">MYVAGON</span> </h4>
                                <p class="dark-purple font-24 mb-0">It's free!</p>
                            </div>                            
                        </div>
                    </li>
                    <li class="d-flex align-items-center justify-content-start">
                       <div class="timeline-image d-flex align-items-center justify-content-center"><div class="icon d-flex align-items-center justify-content-center"><img class="img-fluid" src="images/transport/icons/clipboard.png" alt=""></div></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="font-24 font-weight-600 dark-purple">Complete <span class="text-pink">Registration</span> details.</h4>
                                 <p class="dark-purple font-24 mb-0">It's easy and quick!</p>
                            </div>                            
                        </div>
                    </li>
                    <li class="timeline-inverted d-flex align-items-center justify-content-end">
                        <div class="timeline-image d-flex align-items-center justify-content-center"><div class="icon d-flex align-items-center justify-content-center"><img class="img-fluid" src="images/transport/icons/outline.png" alt=""></div></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                 <h4 class="font-24 font-weight-600 dark-purple">Agree to MYVAGON's <span class="text-pink">Terms & Conditions.</span></h4>
                                
                            </div>                       
                        </div>
                    </li>
                    <li class="d-flex align-items-center justify-content-start">
                       <div class="timeline-image d-flex align-items-center justify-content-center"><div class="icon d-flex align-items-center justify-content-center"><img class="img-fluid" src="images/transport/icons/customer.png" alt=""></div></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="font-24 font-weight-600 dark-purple">Find <span class="text-pink">Available Loads</span> around you or let others know when you will be available.</h4>
                                 <p class="dark-purple font-24 mb-0">Never drive home empty again!</p>
                            </div>                            
                        </div>
                    </li>
                     <li class="timeline-inverted d-flex align-items-center justify-content-end">
                        <div class="timeline-image d-flex align-items-center justify-content-center"><div class="icon d-flex align-items-center justify-content-center"><img class="img-fluid" src="images/transport/icons/payment-method.png" alt=""></div></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                 <h4 class="font-24 font-weight-600 dark-purple"><span class="text-pink">Bid & Book </span> the load you like.</h4>
                                 <p class="dark-purple font-24 mb-0">Just like that!</p>
                                
                            </div>                       
                        </div>
                    </li>
                    <li class="d-flex align-items-center justify-content-start">
                       <div class="timeline-image d-flex align-items-center justify-content-center"><div class="icon d-flex align-items-center justify-content-center"><img class="img-fluid" src="images/transport/icons/customer.png" alt=""></div></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4 class="font-24 font-weight-600 dark-purple">Complete trip, rate facilities and upload picture of <span class="text-pink">POD </span>.</h4>
                                 <p class="dark-purple font-24 mb-0">No more paperwork!</p>
                            </div>                            
                        </div>
                    </li>
                     <li class="timeline-inverted d-flex align-items-center justify-content-end">
                        <div class="timeline-image d-flex align-items-center justify-content-center"><div class="icon d-flex align-items-center justify-content-center"><img class="img-fluid" src="images/transport/icons/payment-method.png" alt=""></div></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                 <h4 class="font-24 font-weight-600 dark-purple">Receive <span class="text-pink">Cash Or Payment </span> In The Bank Account.</h4>
                                 <p class="dark-purple font-24 mb-0">Oh, and much faster!</p>
                                
                            </div>                       
                        </div>
                    </li>
                   
                </ul>
               </div>
             </div>
           </div>
        </div>
        <div class="join-us">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-xl-8 order-xl-12">
                 <div class="img-wrapper position-relative">
                   <img src="images/transport/join-carrier.jpg" alt="" class="img-fluid">
                 </div> 
               </div>
               <div class="col-xl-4 order-xl-1">
                 <div class="text-content mt-3 mt-md-5 mt-xl-0 text-center text-xl-left">
                   <h2 class="font-weight-bold font-43 mb-3">Why join <span class="text-pink">MYWAGON </span> carrier?</h2>
                   <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, <br>sed diam nonumy eirmod tempor invidunt ut labore et dolore</p>
                   <p>magna aliquyam erat, sed diam voluptua. <br>At vero eos et accusam et justo</p>
                   <a href="javascript:;" class="learn-more font-24 text-pink text-center font-weight-bold mt-4 mx-auto d-inline-block bg-white px-3 py-2">Learn More &gt;</a>
                 </div>
               </div>
               
            </div>
          </div>
        </div>
        <div class="feedbacks pt-3 pt-md-5 mt-3 mt-md-5">
                     <div class="container">
                       <div class="row">
                         <div class="col-md-5 col-lg-4">
                           <h2 class="font-43 font-weight-bold position-relative">Some lovely <span class="text-pink">feedback</span> from our carriers</h2>
                         </div>
                         <div class="col-md-7 col-lg-8"></div>
                       </div>

                     </div>
                  </div>
                  <div class="contact-us">

                    <div class="container">
                        <div class="row">
                         <div class="col-md-12">
                            <div class="feedback-slider owl-carousel mt-5">
                               <div class="item bg-white position-relative p-3 p-lg-4 mt-5  ml-4">
                                 <ul class="d-flex mb-3 p-0">
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                 </ul>
                                 <p class="font-22 font-weight-400">At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet dolore magna aliquyam erat, sed diam voluptua. At vero eos et a sanctus.</p>
                                 <div class="client-block d-flex align-items-center mt-3 mt-md-5">
                                      <div class="img-wrapper">
                                        <img src="images/home-page/john-wise.jpg" class="img-fluid rounded-circle">
                                      </div>
                                      <div class="text-wrapper pl-3">
                                        <span class="name d-block font-24 text-black">John Wise</span>
                                        <span class="post font-18 d-block">Carrier</span>
                                      </div>
                                 </div>
                               </div>
                               <div class="item bg-white position-relative p-3 p-lg-4 mt-5 ml-4">
                                 <ul class="d-flex mb-3 p-0">
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                 </ul>
                                 <p class="font-22 font-weight-400">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata. </p>
                                 <div class="client-block d-flex align-items-center mt-3 mt-md-5">
                                      <div class="img-wrapper">
                                        <img src="images/home-page/dustin-hardy.jpg" class="img-fluid rounded-circle">
                                      </div>
                                      <div class="text-wrapper pl-3">
                                        <span class="name d-block font-24 text-black">Dustin Hardy</span>
                                        <span class="post font-18 d-block">Carrier</span>
                                      </div>
                                 </div>
                               </div>
                               <div class="item bg-white position-relative p-3 p-lg-4 mt-5 ml-4">
                                 <ul class="d-flex mb-3 p-0">
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                 </ul>
                                 <p class="font-22 font-weight-400">Iinvidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata. sanctus est Lorem ipsum dolor sit amet</p>
                                 <div class="client-block d-flex align-items-center mt-3 mt-md-5">
                                      <div class="img-wrapper">
                                        <img src="images/home-page/earik-tran.jpg" class="img-fluid rounded-circle">
                                      </div>
                                      <div class="text-wrapper pl-3">
                                        <span class="name d-block font-24 text-black">Earik Tran</span>
                                        <span class="post font-18 d-block">Carrier</span>
                                      </div>
                                 </div>
                               </div>
                               <div class="item bg-white position-relative p-3 p-lg-4 mt-5 ml-4">
                                 <ul class="d-flex mb-3 p-0">
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                    <li><img src="images/home-page/rating-yellow.svg" class="img-fluid"></li>
                                 </ul>
                                 <p class="font-22 font-weight-400">At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet dolore magna aliquyam erat, sed diam voluptua. At vero eos et a sanctus.</p>
                                 <div class="client-block d-flex align-items-center mt-3 mt-md-5">
                                      <div class="img-wrapper">
                                        <img src="images/home-page/john-wise.jpg" class="img-fluid rounded-circle">
                                      </div>
                                      <div class="text-wrapper pl-3">
                                        <span class="name d-block font-24 text-black">John Wise</span>
                                        <span class="post font-18 d-block">Carrier</span>
                                      </div>
                                 </div>
                               </div>
                            </div>
                         </div>
                       </div>
                     
                      <div class="row">
                        <div class="d-sm-none col-md-2 d-md-block"></div>
                        <div class="col-md-8">
                           <div class="newsletter bg-white py-3 px-3 px-sm-4">
                             <h3 class="font-25 mb-3">Subscribe to our Newsletter</h3>
                             
                               <div class="row">
                                 <div class="col-lg-8">
                                    <div class="email">
                                      <div class="form-group m-0">
                                        <input type="email" id="example-email" name="example-email" class="form-control font-25 p-2" placeholder="Email Address">
                                      </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-4">
                                   <a href="javascript:;" class="font-25 btn-black-cust text-uppercase color-dark-purple text-center d-inline-block font-weight-bold  w-100 p-2 p-lg-3 position-relative text-white">Subscribe</a>
                                 </div>
                               </div>
                                                                                        
                           </div>
                        </div>
                        <div class="d-sm-none col-md-2 d-md-block"></div>
                      </div>
                    </div>
                  </div> 
                             
    </div>

@include('shipper.layouts.footer_script')

