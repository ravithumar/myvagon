<link href="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{ URL::asset('assets/libs/bootstrap-table/bootstrap-table.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>

<script type="text/javascript">
$(function() {
     
    var table = $('#{{$dataTableId}}').DataTable({
        processing: true,
        serverSide: true,
        responsive:true,
        ajax: "{{$dateTableUrl}}",
        columns: JSON.parse(`<?php echo json_encode($dateTableFields);?>`)
    });  
    

    $('#filter-form').on('submit', function(e) {
        var obj = {};
        var data = $(this).serialize().split("&");
        for (var key in data) {
            obj[data[key].split("=")[0]] = data[key].split("=")[1];
        }
        $.ajaxSetup({
            data: obj
        });
        table.draw();
        e.preventDefault();
    });

    $('.filter_btn').on('click', function(e) {
        var status = $(this).attr('data-value');
        $('.filter_btn').removeClass('active');
        $(this).addClass('active');
        $.ajaxSetup({
          data: {status:status}
        });
        table.draw();
        e.preventDefault();
    });

    $('.filter-status').on('change',function(e){
        var status = $(this).val();
        $.ajaxSetup({
          data: {status:status}
        });
        table.draw();
        e.preventDefault();
    })
});
</script>

