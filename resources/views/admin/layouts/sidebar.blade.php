<!-- ========== Left Sidebar Start ========== -->
<style type="text/css">
            .no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; }
.se-pre-icon {
   position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url('//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Phi_fenomeni.gif/50px-Phi_fenomeni.gif') 
              50% 50% no-repeat rgb(249,249,249);
opacity: 0.8;

}
</style>
        <div class="se-pre-icon" ></div>
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="{{route('admin.dashboard')}}">
                        <i class="fe-airplay"></i>
                        <span>{{__('Dashboards')}}</span>
                    </a>
                </li>
                <!-- <li>
                    <a href="{{route('admin.users.index')}}">
                        <i class="fe-users"></i>
                        <span> {{__('User Management')}}</span>
                    </a>
                </li> -->
                
                <li>
                    <a href="{{ route('admin.shipper.index') }}">
                        <i class="fe-users"></i>
                        <span> {{__('Shipper Management')}}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.driver.index') }}">
                        <i class="fa fa-drivers-license"></i>
                        <span> {{__('Driver Management')}}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.booking.index') }}">
                        <i class="fa fa-truck"></i>
                        <span> {{__('All Shipments')}}</span>
                    </a>
                </li>

                
                <li>
                    <a href="{{route('admin.package.type.index')}}">
                        <i class="fa fa-cube"></i>
                        <span> {{__('Package Type')}}</span>
                    </a>
                </li>

                <li>
                    <a href="{{asset(url('/admin/system/section'))}}">
                        <i class="ti-settings"></i>
                        <span> {{__('System Section')}}</span>
                    </a>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-truck"></i> <span>{{ __('Truck') }}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{route('admin.truck.type.index')}}">
                                <span> {{__('Truck Type')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.truck.category.index')}}">
                                <span> {{__('Truck Category')}}</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('admin.truck.unit.index')}}">
                                <span> {{__('Truck Unit')}}</span>
                            </a>
                        </li>
                        
                    </ul>
                </li>
           

                 <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i> <span>{{ __('Configuration') }}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{route('admin.system.config')}}" class="waves-effect"><span>{{__('System')}}</span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.app.config')}}" class="waves-effect"><span>{{__('Application')}}</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text-o  "></i> <span>{{ __('CMS Pages') }}</span><span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li>
                            <a href="{{url('admin/about_us')}}" class="waves-effect"><span>{{__('About Us')}}</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/terms_condition')}}" class="waves-effect"><span>{{__('Terms & Condition')}}</span></a>
                        </li>
                        <li>
                            <a href="{{url('admin/privacy_policy')}}" class="waves-effect"><span>{{__('Privacy Policy')}}</span></a>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
<script src="{{ URL::asset('js/common.js')}}"></script>
<script type="text/javascript">
    $(window).load(function() {
    $(".se-pre-icon").fadeOut("slow");
});
</script>