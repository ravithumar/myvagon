        <!-- Vendor js -->
        <script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
        <!-- <script src="{{ URL::asset('assets/libs/notiflix/notiflix-2.1.2.js')}}"></script> -->
        <script src="{{ URL::asset('assets/libs/Notiflix/src/notiflix.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{ URL::asset('js/common.js')}}"></script>
        <script src="{{ URL::asset('js/keypress.js')}}"></script>

        <!-- image zoom script -->
        <!-- Magnific Popup-->
        <script src="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.js')}}"></script>

        <!-- Gallery Init-->
        <script src="{{ URL::asset('assets/js/pages/gallery.init.js')}}"></script>


        
        <script>

                $('form').parsley();

                $("select[name=city]").on('change', function() {
                  console.log($(this).val());
                  if ($(this).val() === 'other') {
                      $("div[name=error]")
                      .attr('data-parsley-required', 'true')
                      .parsley();
                  } else {
                    $("div[name=error]")
                      .removeAttr('data-parsley-required')
                      .parsley().destroy();
                  }
              });
          

        	 $(document).ready(function() {

                $('form').parsley();

                $('.select2').select2();
               

                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });


          });

        </script>
        @yield('script-bottom')