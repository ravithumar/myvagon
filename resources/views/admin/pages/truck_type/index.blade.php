@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('truck')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
	  @include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-4">
			<div class="card">
				<div class="card-body table-responsive">
                @if(isset($edit_param) && $edit_param != null)
                    <h4 class="header-title mb-2">Edit Truck Type</h4>
					<form method="post" enctype="multipart/form-data" action="{{ route('admin.truck.type.update', $edit_param->id )}}">
						@csrf
                       
						<div class="form-group">
							<label for="name">Truck Type <span class="text-danger">*</span></label>
							<input type="text" maxlength="20" name="name" parsley-trigger="change" value="{{ $edit_param->name }}" required placeholder="Enter Type" class="form-control" id="name">
							@error('name')
							<div class="error">{{ $message }}</div>
							@enderror
						</div>
                        
                        <div class="form-group">
                            <label for="name">Truck Unit<span class="text-danger">*</span></label>
                            <select name="truck_unit" class="form-control select2">
                                @if(!empty($truck_unit) )
                                @foreach ($truck_unit as $truckunit)
                                <option value="{{ $truckunit['id'] }}">{{ $truckunit['name'] }}</option>
                                @endforeach
                                @endif
                            </select>
						</div>

                        <div class="form-group">
							<label for="name">Capacity<span class="text-danger">*</span></label>
							<input type="text" maxlength="20" name="capacity" parsley-trigger="change"  value="{{ $edit_param->capacity }}" required placeholder="Capacity" class="form-control" id="name">
						</div>
						<div class="form-group">
							<label for="is_trailer">Is Trailer</label>
							<input type="checkbox" id="is_trailer" style="margin-left: 5px;" name="is_trailer" value="1" {{ $edit_param->is_trailer ? 'checked' : '' }}>
						</div>

						<div class="form-group">
							
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="inputGroupFile01"
								aria-describedby="inputGroupFileAddon01"  id="customFile" name="image" parsley-trigger="change" value="{{old('image')}}"  placeholder="Upload image" class="form-control" id="name">
								<label class="custom-file-label" for="inputGroupFile01">Choose file</label>
								
							</div>
						</div><br>
						@error('name')
						<div class="error">{{ $message }}</div>
						@enderror
						<img src="{{ $base }}/{{ $edit_param->icon}}" class="oldimg" style="height: 50%;width: 30%;"/> 

                      
                      

						<div class="form-group text-right m-b-0">
							<button class="btn btn-primary waves-effect waves-light" type="submit">
							Submit
							</button>
							<button type="reset" onclick="return reset();" class="btn btn-secondary waves-effect m-l-5">
							Reset
							</button>
						</div>
					</form>

               @else

                    <h4 class="header-title mb-2">Add Truck Type</h4>
					<form method="post" enctype="multipart/form-data" action="{{route('admin.truck.type.submit')}}">
						@csrf

                        
						<div class="form-group">
							<label for="name">Truck Type <span class="text-danger">*</span></label>
							<input type="text" maxlength="20" name="name" parsley-trigger="change" value="{{old('name')}}" required placeholder="Enter Type" class="form-control" id="name">
							@error('name')
							<div class="error">{{ $message }}</div>
							@enderror
						</div>

                       <div class="form-group">
                            <label for="type">Truck Unit<span class="text-danger">*</span></label>
                            <select data-parsley-required="true" data-parsley-required-message="This value is required."
                            class="form-control data-parsley-validate-if-empty select2" parsley-trigger="change" id="type" name="truck_unit" >
                                @if(!empty($truck_unit))
                                <option value="" disabled selected>Truck Unit</option>
                                @foreach ($truck_unit as $truckunit)

                                <option value="{{ $truckunit['id'] }}">{{ $truckunit['name'] }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        

                        <div class="form-group">
							<label for="name">Capacity<span class="text-danger">*</span></label>
							<input type="text" maxlength="20" name="capacity" parsley-trigger="change" value="{{old('name')}}" required placeholder="Capacity" class="form-control" id="name">
						</div>
						<div class="form-group">
							<label for="is_trailer">Is Trailer</label>
							<input type="checkbox" id="is_trailer" style="margin-left: 5px;" name="is_trailer" value="1">
						</div>

						<div class="form-group">
							
							<div class="custom-file">
								<input onchange="previewFile(this);"  type="file" class="custom-file-input" id="inputGroupFile01"
								aria-describedby="inputGroupFileAddon01"  name="image" parsley-trigger="change" value="{{old('image')}}" required placeholder="Upload image" class="form-control" id="name" data-parsley-required-message="Please select truck type image">
								<label class="custom-file-label" for="inputGroupFile01">Choose file</label>
								
							</div>
						</div>
						

						<img id="previewImg" src="" style="height: 100px;width: auto; display: none;">

						


						<div class="form-group text-right m-b-0">
							<button class="btn btn-primary waves-effect waves-light" type="submit">
							Submit
							</button>
							<button type="reset" onclick="return reset();" class="btn btn-secondary waves-effect m-l-5">
							Reset
							</button>
						</div>
					</form>

                @endif

					
				</div>
			</div>
		</div>

		<div class="col-xl-8">
			<div class="card">
				<div class="card-body table-responsive" >
					@include('admin.include.table')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
@include('admin.include.table_script')


@endsection