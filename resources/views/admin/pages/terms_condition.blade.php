
<html lang="en">
<style type="text/css">
@import url('https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@500;700&display=swap');

::-webkit-scrollbar {
    width: 8px;
}

::-webkit-scrollbar-track {
    background: #242836;
}

::-webkit-scrollbar-thumb {
    background: #333541;
}

* {

    margin: 0;
    padding: 0;
    box-sizing: border-box;

}

body,html {

    width: 100%;
    min-height: 100%;

}

.main-header {

    width: 100%;
    height: 75px;
    display: block;

}

.main-header ul.header-options {

    width: 100%;
    height: 100%;
    list-style: none;
    display: flex;
    align-items: center;
    justify-content: space-around;
    padding-right: 100px;

}

.main-header ul.header-options li {
    display: inline-block;
}

.main-header ul.header-options li.title {

    font-size: 18px;
    color: #fff;
    font-family: Arial;
    cursor: pointer;

}

.header-mobile {

    width: 100%;
    height: 75px;
    background-color: #181A24;
    text-align: center;
    display: none;
}

.title-mobile {
    font-size: 18px;
    color: #fff;
    font-family: Arial;
    cursor: pointer;
    padding-top: 30px;
}

a {
    text-decoration: none;
    font-size: 16px;
    color: #333541;
    font-family: Arial;
    cursor: pointer;
}

a:hover {
    font-size: 16px;
    color: #333541;
}


.card {
    margin-top: 100px;
    margin-bottom: 100px;
    height : 840px;
    width: 1200px;
    border-radius: 30px;
    margin-left: auto;
    font-family: "Noto Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    margin-right: auto;
}

.card .primary-heading {
    text-align: center;
    font-family: josefin sans;
    color: #333541;
    padding-top: 50px ;
    font-size: 4em;
    font-family: "Noto Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
}

p {
    font-family: "Noto Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    color: rgb(36, 33, 33);
    margin-left: 50px;
    margin-right: 25px;
    padding-top: 50px;
    font-size: 1.5em;
    line-height: 1.3em;
    font-weight: 500;
    font-size: 17px;
    margin: 0px 75px 27px 61px;
}

.bold {
    font-weight: 700;
    color: #333541;
}

.footer-heading {
    text-align: center;
    font-family: josefin sans;
    color: #333541;
    margin-bottom: 50px;
    font-size: 2em;
    line-height: 35px;
}

@media only screen and (max-width: 1218px) {
    .card {
        margin-top: 100px;
        margin-bottom: 100px;
        height : 2050px;
        width: 1000px;
        border-radius: 30px;
        margin-left: auto;
        margin-right: auto;
    }
}

@media only screen and (max-width: 1012px) {
    .card {
        margin-top: 100px;
        margin-bottom: 100px;
        height : 2350px;
        width: 800px;
        border-radius: 30px;
        margin-left: auto;
        margin-right: auto;
    }
}

@media only screen and (max-width: 832px) {
    .card {
        margin-top: 100px;
        margin-bottom: 100px;
        height : 2950px;
        width: 600px;
        border-radius: 30px;
        margin-left: auto;
        margin-right: auto;
    }
    .card .primary-heading {
        font-size: 3.5em;
    }

    .footer-heading {
        font-size: 1.5em;
    }
}

@media only screen and (max-width: 626px) {
    .card {
        margin-top: 100px;
        margin-bottom: 100px;
        height : 3850px;
        width: 450px;
        border-radius: 30px;
        margin-left: auto;
        margin-right: auto;
    }

    .header-mobile {
        display: block;
    }

    .main-header {
        display: none;
    }

    .card .primary-heading {
        font-size: 3em;
    }
}

@media only screen and (max-width: 480px) {
    .card {
        margin-top: 100px;
        margin-bottom: 100px;
        height : 5050px;
        width: 350px;
        border-radius: 30px;
        margin-left: auto;
        margin-right: auto;
    }

    .card .primary-heading {
        font-size: 2em;
    }
}

@media only screen and (max-width: 365px) {
    .card {
        margin-top: 100px;
        margin-bottom: 100px;
        height : 6300px;
        width: 290px;
        border-radius: 30px;
        margin-left: auto;
        margin-right: auto;
    }

    .card .primary-heading {
        font-size: 2em;
    }
}
</style>
<body>
	<div class="container-fluid">
	    <section class="row-fluid">
	        <div class="card span2">
	            <h1 class="primary-heading"><span style="color: #28a6ec">Our Terms </span>& Condition</h1>
	            <p class="paragraph" style="margin: 0px 75px 27px 61px;">
	                
                    {!! \Illuminate\Support\Str::words($data_update)  !!}
	            </p>
	        </div>
    	</section>
	</div>
</body>
</html>
