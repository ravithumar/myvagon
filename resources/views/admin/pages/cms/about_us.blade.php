@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                </div>
                <h4 class="page-title">About Us</h4>
            </div>
        </div>
    </div>
    <form action="{{route('admin.about_us.submit')}}" method="get"  enctype="multipart/form-data">
    	@csrf
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <textarea class="ckeditor form-control" name="aboutus" id="aboutus" >{{$data_update}}</textarea>
                    @error('aboutus')
                        <div class="error">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>                    
        <div class="form-group text-right m-b-0">
            <button type="submit" class="btn btn-primary waves-effect waves-light mr-2" >
                Submit
            </button>
        </div>
    </form>
</div>
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
@endsection