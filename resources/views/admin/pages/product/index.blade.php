@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('product')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
	  @include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-4">
			<div class="card">
				<div class="card-body" >
					<h4 class="header-title mb-2">Add Product</h4>
					<form method="post" action="{{route('admin.product.submit')}}">
						@csrf
						<div class="form-group">
							<label for="name">Name<span class="text-danger">*</span></label>
							<input type="text" name="name" parsley-trigger="change" value="{{old('name')}}" required placeholder="Enter name" class="form-control" id="name">
							@error('name')
							<div class="error">{{ $message }}</div>
							@enderror
						</div>
						<div class="form-group text-right m-b-0">
							<button class="btn btn-primary waves-effect waves-light" type="submit">
							Submit
							</button>
							<button type="reset" class="btn btn-secondary waves-effect m-l-5">
							Cancel
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-xl-8">
			<div class="card">
				<div class="card-body" >
					@include('admin.include.table')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
@include('admin.include.table_script')
@endsection