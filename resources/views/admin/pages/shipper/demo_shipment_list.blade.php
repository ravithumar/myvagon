@extends('admin.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.sorting-btn{
    border-radius: 20px;
    /* margin: 0px 10px; */
    padding: 2px 20px;
}
.sort-button .active{
  background-color: #eadafd;
  color: #9B51E0 !important;
  -webkit-box-shadow : none;
  box-shadow : none;
}
.table td {
  border-left: none;
  border-right: none;
}
.table th {
  border-left: none;
  border-right: none;
}
.panding-status{
  background-color: rgba(213,105,105,0.14) !important;
  border: 1px solid #D56969;
  border-radius: 10px;
}

.in-process-status{
  background-color: rgba(105,163,213,0.14) !important;
  border: 1px solid #699ed5;
  border-radius: 10px;
}

.completed-status{
  background-color: rgba(117,213,105,0.14) !important;
  border: 1px solid #7fd569;
  border-radius: 10px;
}

.cancelled-status{
  background-color: rgba(27,54,78,0.14) !important;
  border: 1px solid #252a38;
  border-radius: 10px;
}

.created-status{
  background-color: rgb(105 213 198 / 14%) !important;
  border: 1px solid #69c6d5;
  border-radius: 10px;
}

.scheduled-status{
  background-color: rgb(213 210 105 / 14%) !important;
  border: 1px solid #c9d569;
  border-radius: 10px;
}
.txt-blue {
    color: #1f1f41 !important;
    font-weight: 600;
}
.manage-order-shipment-view-address .modal1250 {
    max-width: 1250px;
}
.border-r20 {
    border-radius: 20px !important;
}
</style>
<div class="container-fluid">

  {{-- <div class="row align-items-center">
    <div class="col">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">{{ $dateTableTitle }}</a></li>
            </ol>
        </nav>
    </div>
  </div> --}}
  <div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('shipper')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
  @include('admin.include.flash-message')
  
  <div class="row d-flex align-items-center sort-button">
    <div class="col-md-12" style="margin:7px 0px 25px 0px;">
      <button type="button" class="btn txt-blue sorting-btn active filter_btn" data-value="">All</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="create">Created</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="scheduled">Scheduled</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="pending">Pending</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="in-progress">In-Progress</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="completed">Completed</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="canceled">Cancelled</button>
    </div>
  </div>

  <div class="row">
    
    <div class="col-xl-12">
      <div class="card ">
        <div class="card-body" >
          @include('admin.include.table')
        </div>
      </div>
    </div>
  </div>
</div>

<!-- view address -->

<div class="modal fade manage-order-shipment-view-address p-2" id="view-map" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal1250" role="document">
    <div class="modal-content border-r20">
      <div class="modal-body p-3">
        <div class="row">
          <div class="col-md-12">
          <input type="hidden" id="longitud" value="">
      <input type="hidden" id="latitud" value="">
            <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;position: relative; overflow: hidden;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
        </div>
        <h4 class="page-title">{{$PaymentheadingTitle}}</h4>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
        </div>
        
        <h4 class="page-title">{{$headingTitle}}</h4>

        <a href="{{ asset(url('/admin/shipper/view_profile',$data_user['id'])) }}" class="btn btn-info" style="color: #1f1f41;font-weight: 600;border-radius: 5px;background-color: #e9e9ef;border-color: #6658dd;margin-top: -7.5%;
    margin-left: 12.5%;">Show Profile</a>


      </div>
      
    </div>
  </div>
  <div class="card ">
    <div class="card-body" >
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            @if(isset($data_user) && !empty($data_user)) 
              <label for="companyname">Company Name</label>
              <input type="text" class="form-control" parsley-trigger="change" value="{{ $data_user->company_name }}" required id="companyname" name="company_name" placeholder="Enter company name" disabled>
            @endif
          </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
              @if(isset($data_user) && !empty($data_user)) 
                <label for="cwebsite">Company Contact</label>
                <input type="text" class="form-control" parsley-trigger="change" required id="cwebsite"  value="{{ $data_user->company_phone }}"  name="company_contact" placeholder="Enter Company Contact" disabled>
              @endif
            </div>
        </div> <!-- end col -->
      </div> 
      <div class="row">
        <div class="col-md-6">
            <div class="form-group">
              @if(isset($data_user) && !empty($data_user)) 
                <label>Country</label>
                <input type="text" class="form-control" value="{{ $data_user->country }}" placeholder="Enter Country" disabled>
              @endif
            </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            @if(isset($data_user) && !empty($data_user)) 
              <label>State</label>
              <input type="text" class="form-control" value="{{ $data_user->state }}" placeholder="Enter State" disabled>
            @endif
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <div class="form-group">
              @if(isset($data_user) && !empty($data_user)) 
                <label>City</label>
                <input type="text" class="form-control" value="{{ $data_user->city }}" placeholder="Enter City" disabled>
              @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
              @if(isset($data_user) && !empty($data_user))
                <label>Postcode</label>
                <input type="text" class="form-control" value="{{ $data_user->postcode }}" placeholder="Enter Postcode" disabled>
              @endif
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
        </div>
        <h4 class="page-title">{{$FacilitiesheadingTitle}}</h4>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body" >
      <table class="table table-striped" id="facilities">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Address</th>
            <th scop="col">Status</th>
            </tr>
        </thead>
          <tbody>
            @if(isset($multiple_data))
              @foreach($multiple_data as $data_new)
                <tr>
                  <td scope="row">{{ $data_new->id }}</td>
                  <td><a href="" onclick="view_address('{{ $data_new->id }}','{{ $data_new->name }}' , '{{ $data_new->address }}' ,'{{ $data_new->zipcode }}', '{{ $data_new->phone }}', '{{ $data_new->email }}', '{{ $data_new->city }}', '{{ $data_new->state }}', '{{ $data_new->country }}', '{{ $data_new->restroom_service }}', '{{ $data_new->food_service }}', '{{ $data_new->rest_area_service }}' )" data-toggle="modal" style="color: #6c757d;">{{ $data_new->name }}</a></td>
                  <td>{{ $data_new->email }}</td>
                  <td>{{ $data_new->address}}</td>
                  <td>@if($data_new->status == 1)<span class="badge badge-success">Active</span>@else <span class="badge badge-danger">Inactive</span> @endif</td>
                </tr>
              @endforeach
            @endif
          </tbody>
      </table>
    </div>
</div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
        </div>
        <h4 class="page-title">{{$ProductheadingTitle}}</h4>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body" >
      <table class="table table-striped" id="company">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            {{-- <th scope="col">Sku Name</th> --}}
            <th scope="col">Sku No</th>
            <th scop="col">Sku Type</th>
            <th scop="col">Status</th>
            </tr>
        </thead>
          <tbody>
            @foreach($products as $product)
              <tr>
                <td>{{$product->id}}</td>
                <td>{{$product->name}}</td>
                {{-- <td>{{$product->sku_name}}</td> --}}
                <td>{{$product->sku_no}}</td>
                <td>{{$product->sku_type}}</td>
                <td>@if($product->status == 1)<span class="badge badge-success">Active</span>@else <span class="badge badge-danger">Inactive</span> @endif</td>
              </tr>
            @endforeach
          </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
        </div>
        <h4 class="page-title">{{$ManagerheadingTitle}}</h4>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-12">
      <div class="card table-responsive">
        <div class="card-body" >
          <table class="table table-striped" id="manager">
          <thead>
            <tr>
            <th scope="col">S.No</th>
            <th scope="col">Name</th>
            <th scope="col">Work E-mail</th>
            <th scope="col">Phone No</th>
            <th scop="col">No of Shipments</th>
            </tr>
        </thead>
        @foreach($data_manager as $data)
        <tr>
          <td>{{ $data->id}}</td>
          <td>{{ $data->name}}</td>
          <td>{{ $data->email}}</td>
          <td>{{ $data->phone}}</td>
          <td><span  style="color: #1f1f41;font-weight:600;border: 1px solid #1f1f41;border-radius:5px;padding:0px 15px">{{ $data->busy}}</span></td>
        </tr>
        @endforeach
      </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
        </div>
        <h4 class="page-title">{{$ChatheadingTitle}}</h4>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
        </div>
        <h4 class="page-title">{{$ActiveheadingTitle}}</h4>
        
      </div>
    </div>
  </div>
  <a type="button" data-id="{{ $data->id }}" data-status="1" data-popup="tooltip" data-token="{{ csrf_token() }}" class="accept btn btn-success text-white btn-rounded waves-effect waves-light @if($data->stage == "accepted") disabled stop @endif" data-toggle="modal" data-target="#commission_modal" >
    <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Approve Shipper
  </a>
  <a type="button" data-id="{{ $data->id }}" data-status="0" data-popup="tooltip" onclick="approve_account(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-danger text-white btn-rounded waves-effect waves-light  @if($data->stage == "rejected") disabled stop @endif">
      <span class="btn-label"><i class="mdi mdi-close-circle-outline"></i></span>Reject Shipper
  </a>
</div>
<div class="modal fade" id="commission_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('admin/shipper/shipment-comm',$data['id']) }}" method="get">
        @csrf
        <div class="modal-body">
          <h4> Commission Fee </h4>
          <b>€</b> <input type="text" name="commission_fee" id="commission_fee" autocomplete="off">
        </div>
        <div class="modal-footer">
          <button type="submit" data-id="{{ $data->id }}" data-status="1" data-popup="tooltip" onclick="approve_account(this);return true;"  class="btn btn-primary @if($data->stage == "accepted") disabled stop @endif">Submit</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('script')
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
      $('#facilities').DataTable();
    } );

    $(document).ready(function() {
      $('#company').DataTable();
    } );

    $(document).ready(function() {
      $('#manager').DataTable();
    } );
</script>
@include('shipper.include.table_script')
</div>
@endsection
@section('script')
@include('admin.include.table_script')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script>
  function initialize(pickup_address,pickup_lat,pickup_lng,drop_address,drop_lat,drop_lng) {
    
    
  }
  // google.maps.event.addDomListener(window, "load", initialize);

  function open_map(pickup_address,pickup_lat,pickup_lng,drop_address,drop_lat,drop_lng)
  {
    var map;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    if(pickup_lat != '' && pickup_lng != '' && drop_lat != '' && drop_lng != ''){
      var locations = [[pickup_address,pickup_lat,pickup_lng,1],[drop_address,drop_lat,drop_lng,2]]
    }
    
    directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(pickup_lat,pickup_lng),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

    directionsDisplay.setMap(map);

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else
            alert ('failed to get directions');
    });

    $('#view-map').modal('show');
  }
  $(document).ready(function(){
    $(document).on('change','.change-status',function(){
      var booking_id = $(this).attr('data-id');
      var status = $(this).val();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to change the status?',
        'Yes',
        'No',
        function() {
            Notiflix.Loading.Standard();
            $.ajax({
                url: "{{ url('shipper/manage-shipment/change-status') }}",
                type: 'post',
                dataType: "JSON",

                data: {
                    "id": booking_id,
                    "status": status,
                    "_method": 'POST',
                    "_token": $('name[csrf-token]').attr('content'),
                },

                success: function(data) {
                   
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Status changed');
                    location.reload();
                }
            });
        });
    })
  })
</script>

@endsection
