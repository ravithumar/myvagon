@extends('admin.layouts.master')

@section('content')
<link href="{{ url('shipper/css/dashboard.css') }}" rel="stylesheet" type="text/css" />
@if($booking['status'] == "pending")
<style type="text/css">
.breadcrumb{
    padding: 1rem 0;
  }
.manage-shipments .order-status .form-control{background-color: rgba(213,105,105,0.14) !important;border:1px solid #D56969;}
</style>
@endif

@if($booking['status'] == "in-progress")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(105,163,213,0.14) !important;border: 1px solid #699ed5;}
</style>
@endif

@if($booking['status'] == "completed")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(117,213,105,0.14) !important;border: 1px solid #7fd569;}
</style>
@endif

@if($booking['status'] == "cancelled")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgba(27,54,78,0.14) !important;border: 1px solid #252a38;}
</style>
@endif

@if($booking['status'] == "scheduled")
<style type="text/css">
.manage-shipments .order-status .form-control{background-color: rgb(213 210 105 / 14%) !important;border: 1px solid #c9d569;}
</style>
@endif
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
</style>
<div class="container-fluid">

  <div class="row align-items-center">
    <div class="col-md-6">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="{{ url('admin/shipper') }}" class="d-flex align-items-center"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">Shipper Management</a></li>
          <li class="breadcrumb-item active" aria-current="page">Order ID #{{$booking['id']}}</li>
        </ol>
      </nav>
    </div>
    <div class="col-md-6"></div>
  </div>
  @include('admin.include.flash-message')
  
  <div class="row d-flex align-items-center products manage-shipments">
    <div class="col-md-6">
    	<div class="order-status d-flex align-items-center">
		  <p class="font-20 txt-blue mb-0">Order ID #{{$booking['id']}}</p>
		  <div class="form-group mb-0 ml-3">
        <select class="form-control  font-17 txt-blue border-r9 package_type pb-1" style="background: none;appearance: none;text-align:center" name="status" required="" disabled>
          <option value="pending" {{$booking['status'] == 'pending' ? 'selected' : ''}}>Pending</option>
          <option value="scheduled" {{$booking['status'] == 'scheduled' ? 'selected' : ''}}>Scheduled</option>
          <option value="in-progress" {{$booking['status'] == 'in-progress' ? 'selected' : ''}}>in-progress</option>
          <option value="completed" {{$booking['status'] == 'completed' ? 'selected' : ''}}>Completed</option>
          <option value="cancelled" {{$booking['status'] == 'cancelled' ? 'selected' : ''}}>Cancelled</option>
        </select>
      </div>
          <p class="font-20 txt-blue mb-0 ml-3">{{ $booking['is_bid'] == '1' ? 'Bid Request' : 'Book Request' }}</p>
    	</div>
       
    </div>
    <div class="col-md-6">
        @if(isset($booking['status']) && !empty($booking['status']) && $booking['status'] != 'completed')
        <ul class="d-flex align-items-center no-bids-search justify-content-end">
          @if($booking['status'] == 'pending')
          {{-- <li>
            <a href="#" class="btn btn-primary btn-sm mr-2 mt-0">                 
             <span class="text ml-1">Search Available Trucks</span>
           </a>
          </li> --}}
          @endif
          <li>
            <a href="#" class="flag p-1 border-r6"><img src="{{ URL::asset('shipper/images/flag.svg')}}" alt="" class="img-fluid"></a>
          </li>
          <li>
            <a href="#" class="cancel p-1 border-r6" ><img src="{{ URL::asset('shipper/images/cancel-no-bids.svg')}}" alt="" class="img-fluid"></a>
          </li>
          <li>
            <a href="#" class="share p-1 border-r6"><img src="{{ URL::asset('shipper/images/share.svg')}}" alt="" class="img-fluid"></a>
          </li>
        </ul>
        @endif
    </div>
  </div>

  <div class="row">
    
  <div class="col-lg-12 col-xl-12">
      <ul class="d-flex bid-steps pl-0 mt-3 mt-md-5">
          <li class="active position-relative d-inline-block">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-wrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-blue">Created</p>
                <p class="mb-0 font-14 t-black">{{ date('F d, Y',strtotime($booking['pickup_date'])) }}</p>
                <p class="mb-0 font-14 t-black">{{ $booking['pickup_time_from'] }}</p>
              </div>                               
            </div>
          </li>
         @if($booking['status'] == 'pending')
          @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Waiting for Bid</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Waiting for Booking Request</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
         @endif
         @if($booking['status'] == 'scheduled')
          @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Bid Accepted</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Booking Accepted</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-wrap pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Carrier: @if(isset($booking_truck) && !empty($booking_truck)) @foreach($booking_truck as $truck) @if(isset($truck['driver_details']['name']) && !empty($truck['driver_details']['name'])) {{ $truck['driver_details']['name'] }} @endif  @endforeach @endif </p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
         @endif
         @if($booking['status']  == 'in-progress')
         @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Bid Accepted</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Booking Accepted</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-wrap pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Carrier : @if(isset($booking_truck) && !empty($booking_truck)) @foreach($booking_truck as $truck) @if(isset($truck['driver_details']['name']) && !empty($truck['driver_details']['name'])) {{ $truck['driver_details']['name'] }} @endif  @endforeach @endif</p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-wrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Pick up from {{ $booking_locations[0]['company_name'] }}</p>
                <p class="mb-0 font-14 t-black">{{ date('F d, Y',strtotime($booking['pickup_date'])) }}</p>
                <p class="mb-0 font-14 t-black">{{ $booking['pickup_time_from'] }}</p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-red d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-wrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">In-Transit</p>
              </div>
            </div>
          </li>
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 mx-auto mb-2 d-flex align-items-center justify-content-center border-grey"></div>
            </div>
          </li>
         @endif
         @if($booking['status'] == 'completed')
          @if($booking['is_bid'] == 1)
              <li class="position-relative d-inline-block active">
                <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                  <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                  <div class="text-wrap pt-2">
                    <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Bid Accepted</p>
                  </div>
                </div>
              </li>
            @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Booking Accepted</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-wrap pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Carrier: @if(isset($booking_truck) && !empty($booking_truck)) @foreach($booking_truck as $truck) @if(isset($truck['driver_details']['name']) && !empty($truck['driver_details']['name'])) {{ $truck['driver_details']['name'] }} @endif  @endforeach @endif</p>
              </div>
            </div>
          </li>
          @if(isset($booking_locations) && !empty($booking_locations))
            @foreach($booking_locations as $locations)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap text-left pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">@if($locations['is_pickup'] == 1)Picked Up from @else Delivered at @endif {{ $locations['company_name'] }}</p>
                  <p class="mb-0 font-14 t-black">{{ date('F d, Y',strtotime($locations['delivered_at'])) }}</p>
                  <p class="mb-0 font-14 t-black">{{ $locations['delivery_time_from'] }}</p>
                </div>
              </div>
            </li>
            @endforeach
          @endif
          <li class="position-relative d-inline-block active">
            <div class="text-wrap text-center position-relative" style="margin-right: 20px">
              <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-wrap text-left pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Payment</p>
              </div>
            </div>
          </li>

         @endif
         @if($booking['status'] == 'cancelled')
          @if($booking['is_bid'] == 1)
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Waiting for Bid</p>
                </div>
              </div>
            </li>
          @else
            <li class="position-relative d-inline-block active">
              <div class="text-wrap text-center position-relative" style="margin-right: 20px">
                <div class="icon border-50 border-blue d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
                <div class="text-wrap pt-2">
                  <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Waiting for Booking Request</p>
                </div>
              </div>
            </li>
          @endif
          <li class="position-relative d-inline-block cancelled">
            <div class="text-wrap text-center position-relative">
              <div class="icon border-50 border-cancelled d-flex align-items-center justify-content-center"><div class="inner-icon border-50"></div></div>
              <div class="text-wrap pt-2">
                <p class="font-16 font600 mb-0 txt-blue d-flex align-items-center">Cancelled Shipment</p>   
              </div>
            </div>
          </li>
         @endif
       </ul>
    </div>
    <div class="col-md-6"></div>
  </div>
  @if(isset($booking_truck) && !empty($booking_truck))
    @for($i=0;$i<count($booking_truck);$i++)
    <div class="row">
      <div class="col-md-12">
        <div class="labels-wrapper d-flex align-items-center mb-4">
          <p class="mb-0 mr-2 font-18 font500 txt-blue">@if($i==0) Truck Details : @endif</p>
          <p class="mb-0 mr-2 font-14 font500 txt-blue">@if(!empty($booking_truck[$i]['truck_type']['name']) && isset($booking_truck[$i]['truck_type']['name'])){{ $booking_truck[$i]['truck_type']['name'] }} @endif</p>
          @if(isset($booking_truck[$i]['truck_type_category']) && !empty($booking_truck[$i]['truck_type_category']))
          @foreach($booking_truck[$i]['truck_type_category'] as $truck_type_category)
            <div class="label-grey border-r30 p-2 font-14 txt-blue font400">{{ $truck_type_category['name'] }}</div>
          @endforeach
          @endif
        </div>
      </div>
    </div>
    @endfor
  @endif
  @if(isset($booking_locations) && !empty($booking_locations))
    
    <div class="row">
      <div class="col-xl-10">
        <div class="no-bids-track-order-list">
            <ul class="list-unstyled mb-0 pl-3">
              <?php $last_address = end($booking_locations);?>
              @for($i=0;$i<count($booking_locations);$i++)
                <li @if($booking_locations[$i]['is_pickup']==1) class="completed from-location position-relative icon mb-4" @elseif(isset($last_address['id']) && !empty($last_address['id']) && $last_address['id'] == $booking_locations[$i]['id']) class="completed end-location position-relative icon mb-4" @else class="completed to-location position-relative icon mb-4" @endif>
                  <div class="row">
                    <div class="col-md-4">  
                      @if($booking_locations[$i]['is_pickup']==0 && $last_address['id'] != $booking_locations[$i]['id'])
                        <div class="icon border-50 border-grey d-flex align-items-center justify-content-center position-absolute"><div class="inner-icon border-50"></div></div>
                      @endif
                     <h4 class="mt-0 mb-1">{{ $booking_locations[$i]['company_name'] }} <small class="text-muted font-16">{{ date('F d, Y',strtotime($booking_locations[$i]['delivered_at'])) }}   @if($booking_locations[$i]['delivery_time_from'] == $booking_locations[$i]['delivery_time_to']){{ $booking_locations[$i]['delivery_time_from'] }} @else {{ $booking_locations[$i]['delivery_time_from'] }} - {{ $booking_locations[$i]['delivery_time_to'] }} @endif</small> </h4>
                      <p class="txt-black">{{ $booking_locations[$i]['drop_location'] }}</p>            
                    </div>
                    <div class="col-md-1">
                      <small class="txt-blue font-16">@if($booking_locations[$i]['is_pickup']==1) Pickup @else Delivery @endif</small>
                    </div>
                    <div class="col-md-7">
                      @if(isset($booking_locations[$i]['products']) && !empty($booking_locations[$i]['products']))
                        @foreach($booking_locations[$i]['products'] as $products)
                        <ul class="pickup-content d-flex align-items-center border-r15 list-unstyled p-2 mb-3 txt-blue justify-content-center text-center">
                          <li>{{ $products['product_id']['name'] }}</li>
                          <li>{{ $products['product_type']['name'] }}</li>
                          <li>{{ $products['qty'] }}</li>
                          <li>{{ $products['weight'] }} {{ $products['unit']['name'] }}</li>
                          <li class="txt-red"> @if($products['is_fragile'] == 1) Fragile @endif </li>
                        </ul>
                        @endforeach
                      @endif
                    </div>
                  </div>
                </li>
              @endfor
            </ul>
        </div>
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col-md-12 mt-3">
      <div class="long-haul-shipment"> 
       <div class="long-block">    
         <div class="popup d-flex align-items-center">
           <h4 class="txt-purple mr-2 font-15">{{ $booking['journey_type'] }}</h4>
           <a href="#" class="btn-tooltip position-relative d-flex align-items-center txt-blue">i
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Long haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements greater than 350 to 400km from 
              origin to destination are generally classified as
               long-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Medium haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 100km to 300km from 
              origin to destination are generally classified as
               medium-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Short haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 0km to 100km from 
              origin to destination are generally classified as
               short-haul movements</span></a>
            @endif
            </a>
         </div>            
         <p class="mb-1 font-14 txt-blue font-weight-light">Total Kilometers: <em class="font-weight-bold">{{ $booking['distance'] }}</em></p>
         <p class="mb-1 font-14 txt-blue font-weight-light">Journey: <em class="font-weight-bold">{{ $booking['journey'] }}</em></p>
       </div>
      
      </div>
      @if(isset($booking['amount']) && !empty($booking['amount']))
      <div class="no-bid-quote mt-3">
        <p class="d-flex align-items-center txt-blue font-18">Quote :  <strong>&euro; {{ $booking['amount'] }}</strong> 
          {{-- <a href="javascript:;" class="pl-2"><img src="http://13.36.112.48/public/assets/images/shipment/pen-black.svg" class="img-fluid"></a> --}}
        </p>
      </div>
      @endif
    
      
    </div>
  </div>
</div>

<!-- Cancel Shipment -->
{{-- <div class="modal fade cancel-shipment" id="cancel-shipment" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content border-r35">
      <div class="modal-body p-4">
        <p class="font-18 font600 txt-blue text-center">Do you want to continue to <em class="txt-red">cancel</em> shipment?</p>
        <p class="txt-red text-center"><strong>Warning:</strong> Cancellation fee and possible bad reviews to the account</p>
        <div class="btn-block d-flex mt-3">
            <button type="button" class="btn w-100 btn-white mr-2">Yes</button>
            <button type="submit" class="btn w-100 btn-primary"  data-dismiss="modal" aria-label="Close">No</button>
        </div>
        
      </div>
    </div>
  </div>
</div> --}}

@endsection
@section('script')
@endsection