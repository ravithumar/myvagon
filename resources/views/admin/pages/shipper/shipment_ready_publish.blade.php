@extends('admin.layouts.master')

@section('content')
<link href="{{ url('shipper/css/dashboard.css') }}" rel="stylesheet" type="text/css" />
<style>
  .breadcrumb{
    padding: 1rem 0;
  }
</style>
<div class="container-fluid">
 
  <div class="row align-items-center">
    <div class="col-md-12">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="{{ url('admin/booking') }}" class="d-flex align-items-center"><i class="fas fa-shipping-fast pr-3"></i>Booking Management</a></li>
          {{-- <li class="breadcrumb-item" aria-current="page"><a href="javascript:;">Truck Type</a></li> --}}
          {{-- <li class="breadcrumb-item" aria-current="page"><a href="javascript:;">Product Information</a></li> --}}
          {{-- <li class="breadcrumb-item" aria-current="page"><a href="javascript:;">Pickup & Delivery Information</a></li> --}}
          <li class="breadcrumb-item active" aria-current="page"><a href="javascript:void(0);">Insert Your Quote</a></li>
        </ol>
      </nav>
    </div>
  </div>
  <div class="row">
  	<div class="col-md-4">
  		<h4 class="text-dark mb-0">Insert your Quote</h4>
  		<!-- <small>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs</small> -->
  	</div>
  	<div class="col-md-12 my-4">
      <input type="hidden" id="longitud" value="">
      <input type="hidden" id="latitud" value="">
  		 <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;"></div>
  	</div>
  	<div class="col-md-12">
  		<h4>Review  Details</h4>
  		<div class="table-responsive">
            <table class="table mb-0">
                <thead class="thead-light">
                    <tr>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">S.No.</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">Address Details</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">Pickup/Delivery</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">Product</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">Quantity</th>
                        <th class="font-16 text-dark font-weight-normal bgpurple-light">Weight</th>
                    </tr>
                </thead>
                <tbody>
                  @if(isset($booking_locations) && !empty($booking_locations))
                    @for($i=0;$i<count($booking_locations);$i++)
                      <tr>
                        <td scope="row" class="text-dark">{{$i+1}}</td>
                        <td class="border-left text-dark">{{ $booking_locations[$i]['drop_location'] }}<br/><span class="text-muted">{{ date('M d, Y',strtotime($booking_locations[$i]['delivered_at'])) }} @if($booking_locations[$i]['delivery_time_from'] == $booking_locations[$i]['delivery_time_to']) {{ $booking_locations[$i]['delivery_time_from'] }} @else {{ $booking_locations[$i]['delivery_time_from'] }} - {{ $booking_locations[$i]['delivery_time_to'] }} @endif</span></td>
                        <td class="border-left text-dark" >@if($booking_locations[$i]['is_pickup'] == '1') Pickup @else Delivery @endif</td>
                        <td class="text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="border-left text-dark border-top-0" style="">{{$products['product_id']['name']}}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                        <td class="align-middle text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="border-left text-dark border-top-0" style="">{{$products['qty']}} {{ $products['product_type']['name'] }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                        <td class="align-middle text-dark" style="padding:0px">
                          <table class="table mb-0">
                            <tbody>
                              @foreach($booking_locations[$i]['products'] as $products)
                              <tr>
                                <td class="border-left text-dark border-top-0" style="">{{$products['weight']}} {{ $products['unit']['name'] }}</td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    @endfor
                  @endif
                </tbody>
            </table>
        </div>
  	</div>
  	<div class="col-md-12">
      <div class="long-haul-shipment"> 
       <div class="long-block">    
         <div class="popup d-flex align-items-center">
           <h4 class="txt-purple mr-2">@if(isset($booking['journey_type'])  && !empty($booking['journey_type'])) {{ $booking['journey_type'] }} @endif</h4>
           <a href="#" class="btn-tooltip position-relative d-flex align-items-center txt-blue">i
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Long haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements greater than 350 to 400km from 
              origin to destination are generally classified as
               long-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Medium haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 100km to 300km from 
              origin to destination are generally classified as
               medium-haul movements</span></a>
            @endif
            @if(isset($booking['journey_type'])  && !empty($booking['journey_type']) && $booking['journey_type'] == 'Short haul shipment')
            <span class="tooltip-popup position-absolute d-none p-2 font-13">Freight movements between 0km to 100km from 
              origin to destination are generally classified as
               short-haul movements</span></a>
            @endif
         </div>            
         <p class="mb-1 font-16 txt-blue font-weight-light">Total Kilometers: <em class="font-weight-bold">@if(isset($booking['distance'])  && !empty($booking['distance'])) {{ $booking['distance'] }} @endif</em></p>
         <p class="mb-1 font-16 txt-blue font-weight-light">Journey: <em class="font-weight-bold">@if(isset($booking['journey'])  && !empty($booking['journey'])) {{ $booking['journey'] }} @endif</em></p>
       </div>
       <form action="{{ url('admin/shipper/shipment-search/public',$booking['id']) }}" method="post" class="my-3">
        @csrf
        <div class="row">
          <div class="col-lg-5">
            <div class="form-group position-relative mb-3">
              {{-- <input type="text" name="email" class="border-bottom form-control custom-input call-icon position-relative pl-4 font500" placeholder="Mobile No or Email"> --}}
              {{-- <div class="icon position-absolute" style="top:0"><img src="{{ asset('shipper/images/phone.png')}}" alt="" class="img-fluid"></div>
              <small class="text-muted" style="margin-left: 25px;">Link would be sent once shipment is created</small> --}}
            </div>
            {{-- <div class="form-group d-flex">
                  <div class="label-wrapper">
                                 
                    <label><input type="checkbox" id="public_shipment" class="largerCheckbox" name="public_shipment" onclick="validateInput(this)" value="4" data-parsley-multiple="truck_type"><span></span></label>
                  </div>
                  <div class="text-wrapper d-flex pl-1">
                      <div class="wrap-both">
                         <p class="mb-0 font-14 txt-blue">Public Shipment</p>
                         <small class="text-muted">Check this option for shipment broadcasting to carrier/freelance driver or bidding option</small>
                      </div>
                  </div>
                 
                            
             </div> --}}
             {{-- <div class="form-group mb-3 allow_bidding_div" style="display: none">
               <div class="allow-bidding d-flex align-items-center">
                  <div class="text font-14 font600 txt-blue mr-3">Allow Bidding :</div>
                  <label class="switch mb-0">
                    <input type="checkbox" name="all_bidding">
                    <span class="slider round"></span>
                  </label>
                </div>
             </div>  --}}
             <div class="form-group allow_bidding_1_div" style="display: none;">
              <div class="price_div">
                <strong class="font-24 txt-blue">€</strong>            
                <textarea class="form-control border-r13 font-24 font500" name="amount" id="exampleFormControlTextarea1" rows="1" placeholder="Insert Quote" onkeypress="return numberValidate(event)" required></textarea>
              </div>    
             </div>  
             <div class="form-group allow_bidding_2_div" style="display: none">
                  {{-- <a href="javascript:void(0);" class="btn btn-blue btn-rounded">
                      <span class="text ml-1 font-14">Create Shipment</span>
                  </a> --}}
                  <button type="submit" class="btn btn-blue btn-rounded"><span class="text ml-1 font-14">Create Shipment</span></button>
             </div>          
          </div>
          <div class="col-lg-7"></div>

        </div>
       

      </form>
      </div>
      <div class="col-md-3 available_btn">
  		{{-- <a href="javascript:void(0);" class="btn btn-blue btn-rounded btn-block">
			   <span class="text ml-1">Search Available Trucks</span>
      </a> --}}
      </div>
  	</div>
  </div>  
</div>
@endsection
@section('script');
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script type="text/javascript">
var map;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var locations = <?=json_encode($location_aray)?>;


console.log(locations);
function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(<?=$booking['pickup_lat']?>,<?=$booking['pickup_lng']?>),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

 directionsDisplay.setMap(map);

 directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
    }
    else
        alert ('failed to get directions');
 });
}
google.maps.event.addDomListener(window, "load", initialize);

function validateInput(e)
{
  if($('#public_shipment').is(":checked")){
    console.log('true')
    $('.available_btn').hide();
    $('.allow_bidding_2_div').show();
    $('.allow_bidding_1_div').show();
    $('.allow_bidding_div').show();
  }else{
    console.log('false')
    $('.available_btn').show();
    $('.allow_bidding_2_div').hide();
    $('.allow_bidding_1_div').hide();
    $('.allow_bidding_div').hide();
  }
}
</script>
@endsection