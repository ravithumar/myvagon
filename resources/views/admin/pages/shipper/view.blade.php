@extends('admin.layouts.master')
@section('content')
<link href="{{ url('shipper/css/dashboard.css') }}" rel="stylesheet" type="text/css" />
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('shipper')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
	@include('admin.include.flash-message')

    <div class="row">
    <div class="col-lg-4 col-xl-4">
            <div class="card-box text-center">
            @if(isset($data->profile) && $data->profile != NULL && $data->profile != "")
            <img src="{{ env('S3_IMAGE_URL') }}shipper/{{$data->profile}}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
            @else
            <img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
            @endif
               

                <h4 class="mb-0">{{ $data->name }}</h4>
                <p class="text-muted">{{ $data->email }}</p>
                @if($data->status == 1)
                <button type="button" class="btn btn-success btn-xs waves-effect mb-2 waves-light" style="cursor: auto">Active</button>
                @else
                <button type="button" class="btn btn-danger btn-xs waves-effect mb-2 waves-light" style="cursor: auto">Inactive</button>
                @endif

                <div class="text-left mt-3">
                    {{-- <h4 class="font-13 text-uppercase">About Me :</h4>
                    <p class="text-muted font-13 mb-3">
                        {{ $data->self_des }}
                    </p> --}}
                    <p class="text-muted mb-2 font-13"><strong>Full Name :</strong> <span class="ml-2">
                     @if($data->first_name == NULL || $data->last_name == NULL) -- @else {{ $data->first_name }} {{ $data->last_name }} @endif </span></p>

                    <p class="text-muted mb-2 font-13"><strong>Mobile :</strong><span class="ml-2">@if($data->phone == NULL) -- @else {{ $data->phone }} @endif </span></p>

                    {{-- <p class="text-muted mb-2 font-13"><strong>Type :</strong> <span class="ml-2 ">{{ $data->type }}</span></p> --}}

                   
                </div>

                {{-- <ul class="social-list list-inline mt-3 mb-0">
                    <li class="list-inline-item">
                        <a href="{{ $data->facebook_link  }}" target="_blank" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ $data->linkedin }}" target="_blank" class="social-list-item border-primary text-primary"><i class="mdi mdi-linkedin"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ $data->twitter_link }}" target="_blank" class="social-list-item border-primary text-primary"><i class="mdi mdi-twitter"></i></a>
                    </li>
                  
                </ul> --}}
            </div> <!-- end card-box -->

                {{-- <div class="card-box">
                    <h4 class="header-title mb-3">Inbox</h4>

                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 510.994px;"><div class="inbox-widget slimscroll" style="max-height: 310px; overflow: hidden; width: auto; height: 510.994px;">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Tomaslau</p>
                            <p class="inbox-item-text">I've finished it! See you so...</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                            </p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Stillnotdavid</p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                            </p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Kurafire</p>
                            <p class="inbox-item-text">Nice to meet you</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                            </p>
                        </div>

                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Shahedk</p>
                            <p class="inbox-item-text">Hey! there I'm available...</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                            </p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Adhamdannaway</p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                            </p>
                        </div>

                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Stillnotdavid</p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                            </p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Kurafire</p>
                            <p class="inbox-item-text">Nice to meet you</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                            </p>
                        </div>
                    </div><div class="slimScrollBar" style="background: rgb(158, 165, 171); width: 8px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 214.989px;"></div><div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div> <!-- end inbox-widget -->

                </div> --}} <!-- end card-box-->

            </div>

            <div class="col-lg-8 col-xl-8">
                                <div class="card-box">
                                    <ul class="nav nav-pills navtab-bg nav-justified">
                                        <li class="nav-item">
                                            <a href="#aboutme" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                Profile Manage
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#timeline" data-toggle="tab" aria-expanded="true" class="nav-link ">
                                                Facilities
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#settings" data-toggle="tab" aria-expanded="false" class="nav-link ">
                                                Profile Update 
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane show active" id="aboutme">
                                            <h5 class="mb-4 text-uppercase"><i class="fas fa-shipping-fast mr-1"></i>
                                                Approve Shipper Profile</h5>

                                            
                                                <a type="button" data-id="{{ $data->id }}" data-status="1" data-popup="tooltip" onclick="approve_account(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-success text-white btn-rounded waves-effect waves-light @if($data->stage == "accepted") disabled stop @endif">
                                                            <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Approve Shipper
                                                </a>
                                                <a type="button" data-id="{{ $data->id }}" data-status="0" data-popup="tooltip" onclick="approve_account(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-danger text-white btn-rounded waves-effect waves-light  @if($data->stage == "rejected") disabled stop @endif">
                                                    <span class="btn-label"><i class="mdi mdi-close-circle-outline"></i></span>Reject Shipper
                                                </a>
                                                
                                        </div> <!-- end tab-pane -->
                                        <!-- end about me section content -->

                                        <div class="tab-pane " id="timeline">
                                            <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building mr-1"></i> Company Info</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="companyname">Company Name</label>
                                                        <input type="text" class="form-control" parsley-trigger="change" value="{{ $data->company_name }}" required id="companyname" name="company_name" placeholder="Enter company name" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="cwebsite">Company Contact</label>
                                                        <input type="text" class="form-control" parsley-trigger="change" required id="cwebsite"  value="{{ $data->company_phone }}"  name="company_contact" placeholder="Enter Company Contact" disabled>
                                                    </div>
                                                </div> <!-- end col -->
                                            </div> <!-- end row -->

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Country</label>
                                                        <input type="text" class="form-control" value="@if(isset($data->country_name->name) && !empty($data->country_name->name)) {{ $data->country_name->name }} @endif" placeholder="Enter Country" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <input type="text" class="form-control" value="@if(isset($data->state_name->name) && !empty($data->state_name->name)) {{ $data->state_name->name }} @endif" placeholder="Enter State" disabled>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <input type="text" class="form-control" value="@if(isset($data->city_name->name) && !empty($data->city_name->name)) {{ $data->city_name->name }} @endif" placeholder="Enter City" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Postcode</label>
                                                        <input type="text" class="form-control" value="{{ $data->postcode }}" placeholder="Enter Postcode" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                         
                                        <div class="card">
                                            <div class="card-body" >
                                            <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        <th scope="col">ID</th>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Address</th>
                                                        <th scop="col">Status</th>
                                                       
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if(isset($multiple_data))
                                                           @foreach($multiple_data as $data_new)
                                                                <tr>
                                                                <th scope="row">{{ $data_new->id }}</th>
                                                                <td><a href="" onclick="view_address('{{ $data_new->id }}','{{ $data_new->name }}' , '{{ $data_new->address }}' ,'{{ $data_new->zipcode }}', '{{ $data_new->phone }}', '{{ $data_new->email }}', '{{ $data_new->city }}', '{{ $data_new->state }}', '{{ $data_new->country }}', '{{ $data_new->restroom_service }}', '{{ $data_new->food_service }}', '{{ $data_new->rest_area_service }}' )" data-toggle="modal" style="color: #6c757d;"><p>{{ $data_new->name }}</p></a></td>
                                                                <td>{{ $data_new->email }}</td>
                                                                <td>{{ $data_new->address}}</td>
                                                                <td>@if($data_new->status == 1)<span class="badge badge-success">Active</span>@else <span class="badge badge-danger">Inactive</span> @endif</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>



                                        </div>
                                        <!-- end timeline content-->

                                        <div class="tab-pane " id="settings">
                                            <form method="post" enctype="multipart/form-data" action="{{route('admin.shipper.update', $data->id)}}">
                                            @csrf
                                                <h5 class="mb-4 text-uppercase"><i class="mdi mdi-account-circle mr-1"></i> Shipper Personal Info</h5>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstname">First Name</label>
                                                            <input type="text" name="first_name"  parsley-trigger="change" required value="{{ $data->first_name }}" class="form-control" id="firstname" placeholder="Enter first name">
                                                            @error('first_name')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="last_name">Last Name</label>
                                                            <input type="text" parsley-trigger="change" required name="last_name"  value="{{ $data->last_name }}" class="form-control" id="last_name" placeholder="Enter last name">
                                                            @error('last_name')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div> <!-- end col -->
                                                </div> <!-- end row -->

                                                {{-- <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="userbio">Self Description(Bio)</label>
                                                            <textarea class="form-control" parsley-trigger="change" required name="self_des" value="{{ $data->self_des }}" id="userbio" rows="4" placeholder="Write something...">{{ $data->self_des }}</textarea>
                                                            @error('self_des')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div> <!-- end col -->
                                                </div> <!-- end row --> --}}

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="useremail">Email Address</label>
                                                            <input type="email" class="form-control" readonly id="useremail" value="{{ $data->email }}" placeholder="Enter email">
                                                            
                                                        </div>
                                                    </div>
                                                   
                                                </div> <!-- end row -->

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="commission_fee">Commission Fee</label>
                                                            <input type="text" name="commission_fee"  parsley-trigger="change" required value="{{ $data->commission_fee }}" class="form-control" id="commission_fee" placeholder="Enter commission fee">
                                                            @error('commission_fee')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Phone no</label>
                                                            <input type="text" parsley-trigger="change" required name="phone"  value="{{ $data->phone }}" class="form-control" id="last_name" placeholder="Enter phone no">
                                                            @error('phone')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div> <!-- end col -->
                                                </div> <!-- end row -->

                                                <div class="form-group">
                                                <label for="useremail">Browse</label>

                                                    <div class="custom-file">
                                                        <input onchange="previewFile(this);"  type="file" class="custom-file-input" id="inputGroupFile01"
                                                        aria-describedby="inputGroupFileAddon01"  name="profile" parsley-trigger="change" value="{{old('profile')}}"  placeholder="Upload image" class="form-control" id="name">
                                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                        
                                                    </div>
                                                    @error('name')
                                                        <div class="error">{{ $message }}</div>
                                                    @enderror
                                                </div><br>
                                              

                                                <img id="previewImg" @if($data->profile != "") src="{{ env('S3_IMAGE_URL')}}shipper/{{ $data->profile }}" style="height: 100px;width: auto; display: block;" @else src=""  alt="Placeholder" style="height: 100px;width: auto; display: none;"  @endif>

                                                

                                                

                                                {{-- <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-earth mr-1"></i> Social</h5>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="social-fb">Facebook</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fab fa-facebook-square"></i></span>
                                                                </div>
                                                                <input type="text" name="facebook_link" parsley-trigger="change" required class="form-control" value="{{ $data->facebook_link }}" id="social-fb" placeholder="Facebook">
                                                                
                                                            </div>
                                                            @error('facebook_link')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="social-tw">Twitter</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fab fa-twitter"></i></span>
                                                                </div>
                                                                <input type="text" class="form-control" parsley-trigger="change" required name="twitter_link" value="{{ $data->twitter_link }}" id="social-tw" placeholder="Twitter">
                                                              
                                                            </div>
                                                            @error('twitter_link')
                                                                <div class="error">{{ $message }}</div>
                                                             @enderror
                                                        </div>
                                                    </div> <!-- end col -->
                                                </div> <!-- end row -->

                                                <div class="row">
                                                   
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="social-lin">Linkedin</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fab fa-linkedin"></i></span>
                                                                </div>
                                                                <input type="text" class="form-control" parsley-trigger="change" required name="linkedin" value="{{ $data->linkedin }}" id="social-lin" placeholder="Linkedin">
                                                               
                                                            </div>
                                                            @error('linkedin')
                                                                <div class="error">{{ $message }}</div>
                                                                @enderror
                                                        </div>
                                                    </div> <!-- end col -->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="social-lin">Instagram</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fab fa-instagram"></i></span>
                                                                </div>
                                                                <input type="text" class="form-control" parsley-trigger="change" required name="instagram" value="{{ $data->instagram }}" id="social-lin" placeholder="Instagram">
                                                                
                                                            </div>
                                                            @error('instagram')
                                                                <div class="error">{{ $message }}</div>
                                                                @enderror
                                                        </div>
                                                    </div> <!-- end col -->

                                                </div> <!-- end row --> --}}

                                               
                                                
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- end settings content-->

                                    </div> <!-- end tab-content -->
                                </div> <!-- end card-box-->

                            </div>

    </div>
<!-- view address -->
<div class="modal fade add-address" id="view-details" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="edits mb-2 d-flex align-items-center justify-content-between">
            <div class="title font-20 text-left">Address Details</div>
            <div class="icon-block d-flex">
            
              <a href="javascript:;" class="" data-dismiss="modal" aria-label="Close"ata-popup="tooltip" 
                onclick="" >
                <i class="fa fa-times" style="font-size: 19px !important;color:#1f1f41!important;"></i>
              </a>
              
            </div>
  
          </div>
         
           <input type="hidden" name="id" value="" id="productid">
  
            <div class="form-group mb-3">
              <label for="name" class="mb-1 text-grey font-14">Name</label>
              <input type="text" readonly id="name-view" maxlength="40" required name="name" class="form-control tx-b form-control-without-border" placeholder="Enter Name" value="{{ old('name') }}">
             
            </div>
  
            <div class="form-group mb-3">
              <label for="first-name" class="mb1 text-grey font-14">Address</label>
              <input type="text" readonly id="address-view" name="address" autocomplete="off" onkeypress="return lettersValidate(event)" maxlength="40" parsley-trigger="change" required class="autocomplate-input  form-control form-control-without-border" value="{{ old('address') }}" placeholder="Enter Address">
             
            </div>
  
            
  
            <div class="row">
              <div class="col-md-6">
                <div class="form-group  mb-3">
                  <label for="first-name" class="mb1 text-grey font-14">Country</label>
                  <input type="text" readonly id="countryview" name="city"   class="form-control form-control-without-border">
                </div>
              </div>
             
  
              <div class="col-md-6">
                <div class="form-group  mb-3">
                  <label for="first-name" class="mb1 text-grey font-14">State</label>
                  <input type="text" readonly id="stateview"  autocomplete="off"  class="form-control form-control-without-border pb-2">
                </div>
              </div>
            </div>
  
            <div class="row">
              <div class="col-md-6">
                <div class="form-group  mb-3">
                  <label for="first-name" class="mb1 text-grey font-14">City</label>
                  <input type="text" readonly id="cityview" name="city"   class="form-control tx-b form-control-without-border">
                </div>
              </div>
             
  
              <div class="col-md-6">
                <div class="form-group  mb-3">
                  <label for="first-name" class="mb1 text-grey font-14">Postcode</label>
                  <input type="text" readonly id="postcodeview"  autocomplete="off"  class="form-control form-control-without-border pb-2">
                </div>
              </div>
            </div>
  
  
            <div class="row">
              <div class="col">
                <div class="form-group  mb-3">
                <label for="first-name" class="mb1 text-grey font-14">Phone</label>
                  <input type="call" readonly id="phone-view" name="phone" autocomplete="off" onkeypress="return numberValidate(event)" maxlength="8" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Phone">
                 
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group  mb-3">
                <label for="first-name" class="mb1 text-grey font-14">Email</label>
                  <input type="email" id="email-view" autocomplete="off" name="email" maxlength="40" parsley-trigger="change" required class="form-control pb-2 form-control-without-border" placeholder="Email">
                  
                </div>
              </div>
            </div>
  
            <div class="row">
              <div class="col">
                <div class="add-facility">
                  <div class="form-group checkbox-inline">
                    <label>
                        <input type="checkbox" onclick="return false"  value="1" name="restroom_service" id="restrooms-view">
                        <span></span>
                    </label>
                    <label for="Restrooms" class="font-15">Restrooms</label>
                  </div>
                  <div class="form-group">
                    <label>
                        <input type="checkbox" onclick="return false"  value="1"  name="food_service" id="food-view" >
                        <span></span>
                    </label>
                    <label for="Food" class="font-15">Food</label>
                  </div>
                  <div class="form-group">
                      <label>
                          <input type="checkbox" onclick="return false"  value="1"  name="rest_area_service" id="driver-view" >
                          <span></span>
                      </label>
                    <label for="Driver Rest Area" class="font-15">Driver Rest Area</label>
                  </div>
   
                </div>
              </div>
            </div>
            <div class="btn-block d-flex">
          </div>
  
        </div>
      </div>
    </div>
  </div>



</div>
@endsection
@section('script')
@include('admin.include.table_script')
<script>
    function view_address(id,name,address,postcode,phone,email,city,state,country,serv1,serv2,serv3){
        $('#view-details #name-view').val(name);
        $('#view-details #address-view').val(address);
        $('#view-details #phone-view').val(phone);
        $('#view-details #email-view').val(email);
        $('#view-details #cityview').val(city);
        $('#view-details #stateview').val(state);
        $('#view-details #countryview').val(country);
        $('#view-details #postcodeview').val(postcode);
        if(serv1 == 1){
            console.log(serv1)
            $('#view-details #restrooms-view').prop('checked',true);
        }else{
            $('#view-details #restrooms-view').prop('checked',false);
        }

        if(serv2 == 1){
            $('#view-details #food-view').prop('checked',true);
        }else{
            $('#view-details #food-view').prop('checked',false);
        }
        

        if(serv3 == 1){
            $('#view-details #driver-view').prop('checked',true);
        }else{
            $('#view-details #driver-view').prop('checked',false);
        }
        $("#view-details").modal("show");
    }
</script>
@endsection