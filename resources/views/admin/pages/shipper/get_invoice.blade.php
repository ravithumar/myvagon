@extends('admin.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.sorting-btn{
    border-radius: 20px;
    /* margin: 0px 10px; */
    padding: 2px 20px;
}
.sort-button .active{
  background-color: #eadafd;
  color: #9B51E0 !important;
  -webkit-box-shadow : none;
  box-shadow : none;
}
.table td {
  border-left: none;
  border-right: none;
}
.table th {
  border-left: none;
  border-right: none;
}
.panding-status{
  background-color: rgba(213,105,105,0.14) !important;
  border: 1px solid #D56969;
  border-radius: 10px;
}

.in-process-status{
  background-color: rgba(105,163,213,0.14) !important;
  border: 1px solid #699ed5;
  border-radius: 10px;
}

.completed-status{
  background-color: rgba(117,213,105,0.14) !important;
  border: 1px solid #7fd569;
  border-radius: 10px;
}

.cancelled-status{
  background-color: rgba(27,54,78,0.14) !important;
  border: 1px solid #252a38;
  border-radius: 10px;
}

.created-status{
  background-color: rgb(105 213 198 / 14%) !important;
  border: 1px solid #69c6d5;
  border-radius: 10px;
}

.scheduled-status{
  background-color: rgb(213 210 105 / 14%) !important;
  border: 1px solid #c9d569;
  border-radius: 10px;
}
.txt-blue {
    color: #1f1f41 !important;
    font-weight: 600;
}
.manage-order-shipment-view-address .modal1250 {
    max-width: 1250px;
}
.border-r20 {
    border-radius: 20px !important;
}
.cardData {
  width: 170px;
  height: 210px;
  position: absolute;
  top: 20%;
  
  z-index: 9;
  margin-top: 40px;
  display: none;
  border-radius:25px;
}
.card{
  width: 100%;
}
 table.dataTable{
  display: none;
 }
table.dataTable{
  clear: both;
    margin-top: 6px !important;
    margin-bottom: 6px !important;
    max-width: none !important;
    border-collapse: separate !important;
    border-spacing: 0;
  /*width: 0px;*/
}

</style>
<div class="container-fluid">

  {{-- <div class="row align-items-center">
    <div class="col">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">{{ $dateTableTitle }}</a></li>
            </ol>
        </nav>
    </div>
  </div> --}}
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <div class="page-title-right">
          {{ Breadcrumbs::render('shipper')}}
        </div>
        <h4 class="page-title">{{$dateTableTitle}}</h4>
      </div>
      
    </div>
  </div>
  @include('admin.include.flash-message')
  </div>
  <div class="row mb-3">
    <div class="col-xl-2">
      {{-- <div class="col-xl-2"> --}}
        <select class="form-control select2 filter_month" name="months">
          <option value="">Select Month</option>
          <option value="1">January</option>
          <option value="2">February</option>
          <option value="3">March</option>
          <option value="4">April</option>
          <option value="5">May</option>
          <option value="6">June</option>
          <option value="7">July</option>
          <option value="8">August</option>
          <option value="9">September</option>
          <option value="10">October</option>
          <option value="11">November</option>
          <option value="12">December</option>
        </select>
        {{-- </div> --}}
    </div>
    <div class="col-xl-8"></div>
    <div class="col-xs-2">
      <button type="button" id="Invoice" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >Get Invoice</button>
    </div>
  </div>
  <!-- <div class="form-group">
    <label for="">Select Month:</label>
    <select class="selectpicker">
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="1">January</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="2">February</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="3">March</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="4">April</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="5">May</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="6">June</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="7">June</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="8">August</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="9">September</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="10">October</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="11">November</option>
        <option href="javascript:;" style="color: #222244;text-transform: capitalize;padding-right: 20px;font-weight: bold;" class="filter_btn active" data-id="created_at"  data-value="12">December</option>

    </select>
  </div> -->  
  <div class="row">
    
    <div class="col-xl-12">
      <div class="card  table-responsive">
        <div class="card-body data_table" >
          @include('admin.include.table')
          
        </div>
        <p style="margin: 26px;font-size: medium;color: black;"><b>Please Select Month.</b></p>
      </div>
    </div>
  </div>
</div>

<!-- view address -->


<div class="modal fade manage-order-shipment-view-address p-2" id="view-map" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal1250" role="document">
    <div class="modal-content border-r20">
      <div class="modal-body p-3">
        <div class="row">
          <div class="col-md-12">
          <input type="hidden" id="longitud" value="">
      <input type="hidden" id="latitud" value="">
            <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;position: relative; overflow: hidden;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send mail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{ url('send-mail',$data['id']) }}" method="POST">
          @csrf
            <div class="form-group">
              <input type='hidden' name='pickup_date' class="pickup_date">
                <label>Email:</label>
               
                <input type="email" name="email" class="form-control" placeholder="Enter Email" required>
                
                @error('email')
                  <span class="text-danger"> {{ $message }} </span>
                @enderror
            </div>
               
            <div class="form-group">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button> 
                <button type="submit" class="btn btn-success save-data">Send</button>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@include('admin.include.shipper_table_script')
</div>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script>
  function initialize(pickup_address,pickup_lat,pickup_lng,drop_address,drop_lat,drop_lng) {
    
    
  }
  // google.maps.event.addDomListener(window, "load", initialize);

  function open_map(pickup_address,pickup_lat,pickup_lng,drop_address,drop_lat,drop_lng)
  {
    var map;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    if(pickup_lat != '' && pickup_lng != '' && drop_lat != '' && drop_lng != ''){
      var locations = [[pickup_address,pickup_lat,pickup_lng,1],[drop_address,drop_lat,drop_lng,2]]
    }
    
    directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(pickup_lat,pickup_lng),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

    directionsDisplay.setMap(map);

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else
            alert ('failed to get directions');
    });

    $('#view-map').modal('show');
  }
  $(document).ready(function(){
    $(document).on('change','.change-status',function(){
      var booking_id = $(this).attr('data-id');
      var status = $(this).val();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to change the status?',
        'Yes',
        'No',
        function() {
            Notiflix.Loading.Standard();
            $.ajax({
                url: "{{ url('shipper/manage-shipment/change-status') }}",
                type: 'post',
                dataType: "JSON",

                data: {
                    "id": booking_id,
                    "status": status,
                    "_method": 'POST',
                    "_token": $('name[csrf-token]').attr('content'),
                },

                success: function(data) {
                   
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Status changed');
                    location.reload();
                }
            });
        });
    })
  })
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.dataTables_filter').hide();
  $('.dataTables_length').hide();
  $('.sort_by').click(function(e){
       e.preventDefault();
       e.stopPropagation();
      $('.sortFunctionalityMenu').toggle();
    });
    $('body').click( function() {
        $('.sortFunctionalityMenu').hide();
    });
});
</script>
<script type="text/javascript">
  $("#Invoice").hide();
  $("table").css('display','none');
  $(".data_table").css('display','none');
  $(".filter_month ").change(function(){
    $("table").show();
    $(".data_table").css('display','block');
  $("table").css('width','inherit');
  $("p").hide();
  $("#Invoice").show();
});


</script>


@endsection
