@extends('admin.layouts.master')
@section('content')
<link href="{{ url('shipper/css/dashboard.css') }}" rel="stylesheet" type="text/css" />
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
		</div>
	</div>
	@include('admin.include.flash-message')
    <div class="row">
        <div class="col-lg-4 col-xl-4">
            <div class="card-box text-center">
                @if(isset($data->profile) && $data->profile != NULL && $data->profile != "")
                <img src="{{ env('S3_IMAGE_URL') }}shipper/{{$data->profile}}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                @else
                <img src="{{ env('APP_URL') }}images/default.png" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                @endif
                <h4 class="mb-0">{{ $data->name }}</h4>
                <p class="text-muted">{{ $data->email }}</p>
                @if($data->status == 1)
                <button type="button" class="btn btn-success btn-xs waves-effect mb-2 waves-light" style="cursor: auto">Active</button>
                @else
                <button type="button" class="btn btn-danger btn-xs waves-effect mb-2 waves-light" style="cursor: auto">Inactive</button>
                @endif
                <div class="text-left mt-3">
                    {{-- <h4 class="font-13 text-uppercase">About Me :</h4>
                    <p class="text-muted font-13 mb-3">
                        {{ $data->self_des }}
                    </p> --}}
                    <p class="text-muted mb-2 font-13"><strong>Full Name :</strong> <span class="ml-2">
                     @if($data->first_name == NULL || $data->last_name == NULL) -- @else {{ $data->first_name }} {{ $data->last_name }} @endif </span></p>

                    <p class="text-muted mb-2 font-13"><strong>Mobile :</strong><span class="ml-2">@if($data->phone == NULL) -- @else {{ $data->phone }} @endif </span></p>

                    {{-- <p class="text-muted mb-2 font-13"><strong>Type :</strong> <span class="ml-2 ">{{ $data->type }}</span></p> --}}
                </div>
            </div> <!-- end card-box -->
        </div>
        <div class="col-lg-5 col-xl-5">
            <div class="card-box">
                <ul class="nav nav-pills navtab-bg nav-justified">
                    <li class="nav-item">
                        <a href="#settings" data-toggle="tab" aria-expanded="false" class="nav-link ">
                            Profile Update 
                        </a>
                    </li>
                </ul>
                <div class="tab-pane " id="settings">
                    <form method="post" enctype="multipart/form-data" action="{{route('admin.shipper.update', $data->id)}}">
                    @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstname">First Name</label>
                                    <input type="text" name="first_name"  parsley-trigger="change" required value="{{ $data->first_name }}" class="form-control" id="firstname" placeholder="Enter first name">
                                    @error('first_name')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input type="text" parsley-trigger="change" required name="last_name"  value="{{ $data->last_name }}" class="form-control" id="last_name" placeholder="Enter last name">
                                    @error('last_name')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="useremail">Email Address</label>
                                    <input type="email" class="form-control" readonly id="useremail" value="{{ $data->email }}" placeholder="Enter email">
                                    
                                </div>
                            </div>
                        </div> <!-- end row -->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="commission_fee">Commission Fee</label>
                                    <input type="text" name="commission_fee"  parsley-trigger="change" required value="{{ $data->commission_fee }}" class="form-control" id="commission_fee" placeholder="Enter commission fee">
                                    @error('commission_fee')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">Phone No</label>
                                    <input type="call" parsley-trigger="change" required name="phone"  value="{{ $data->phone }}" class="form-control" id="phone" placeholder="Enter phone">
                                    @error('phone')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->

                        <div class="form-group">
                        <label for="useremail">Browse</label>

                            <div class="custom-file">
                                <input onchange="previewFile(this);"  type="file" class="custom-file-input" id="inputGroupFile01"
                                aria-describedby="inputGroupFileAddon01"  name="profile" parsley-trigger="change" value="{{old('profile')}}"  placeholder="Upload image" class="form-control" id="name">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                
                            </div>
                            @error('name')
                                <div class="error">{{ $message }}</div>
                            @enderror
                        </div><br>
                      

                        <img id="previewImg" @if($data->profile != "") src="{{ env('S3_IMAGE_URL')}}shipper/{{ $data->profile }}" style="height: 100px;width: auto; display: block;" @else src=""  alt="Placeholder" style="height: 100px;width: auto; display: none;"  @endif>

                        <div class="text-right">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
