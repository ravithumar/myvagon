@extends('admin.layouts.master')

@section('content')

<!-- header dashboard -->
<div class="container-fluid">
<div class="row">

            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
</div>     


<div class="row">
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-primary">
                                <i class="fas fa-shipping-fast font-24 avatar-title text-primary"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($data) && !empty($data)){{ $data }} @else {{ 0 }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Shipper</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-success">
                                <i class="fas fa-users-cog font-24 avatar-title text-success"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($freelancer_data)){{ $freelancer_data }} @else {{ 0 }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Freelancer driver</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-info">
                                <i class="fas fa-shipping-fast font-24 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($dispatcher_data) && !empty($dispatcher_data)){{ $dispatcher_data }} @else {{ 0 }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Carrier dispatcher</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-warning">
                                <i class="fas fa-shipping-fast font-24 avatar-title text-warning"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">0</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Carrier Driver</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-info">
                                <i class="fab fa-amazon-pay font-24 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($book_now)) {{ $book_now }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Book Now/Book Later</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
          </div> <!-- end col-->

          <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-warning">
                                <i class="fas fa-spinner font-24 avatar-title text-warning"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($booking_inprogress)) {{ $booking_inprogress }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total  In-Progress Shipment Order</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
          </div> <!-- end col-->
      
           
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                    <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-success">
                                <i class="far fa-calendar-check font-24 avatar-title text-success"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($booking_completed)) {{ $booking_completed }} @else {{ 0 }}@endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Completed Shipment Order</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-danger">
                                <i class="fas fa-history font-24 avatar-title text-danger"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($booking_cancel)) {{ $booking_cancel }} @else {{ 0 }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Cancelled Shipment Order</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->
            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                    <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-primary">
                                <i class="fas fa-book font-24 avatar-title text-primary"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($booking)) {{ $booking }} @else {{ 0 }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Bid Job Posted</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-info">
                                <i class="fas fa-user-clock font-24 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($total) && !empty($total)){{ $total }} @else {{ 0 }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Shipment Orders</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-info">
                                <i class="fas fa-user-clock font-24 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($booking_scheduled)) {{ $booking_scheduled }} @else {{ 0 }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Schedule Shipment Order</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->

            <div class="col-md-6 col-xl-3">
                <div class="widget-rounded-circle card-box">
                    <div class="row">
                        <div class="col-6">
                            <div class="avatar-lg rounded bg-soft-info">
                                <i class="fas fa-user-clock font-24 avatar-title text-info"></i>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-right">
                                <h3 class="text-dark mt-1"><span data-plugin="counterup">@if(isset($booking_pending)) {{ $booking_pending }} @else {{ 0 }} @endif</span></h3>
                                <p class="text-muted mb-1 text-truncate">Total Pending Shipment Order</p>
                            </div>
                        </div>
                    </div> <!-- end row-->
                </div> <!-- end widget-rounded-circle-->
            </div> <!-- end col-->


 </div>
    <div class="row">
        <div class="col-xl-6">
            <div class="card">
            <div class="card-body">
                <div class="card-widgets">
                    <a href="javascript: void(0);" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                    <a data-toggle="collapse" href="#cardCollpase3" role="button" aria-expanded="false" aria-controls="cardCollpase3"><i class="mdi mdi-minus"></i></a>
                    <a href="javascript: void(0);" data-toggle="remove"><i class="mdi mdi-close"></i></a>
                        </div>
                <h4 class="header-title mb-0">Monthly Earning Graph</h4>
                <div class="panel-heading" id="cardCollpase3">
                    <div class="panel-body">
                            <div class="col-12">
                                <h3 data-plugin="counterup" style="margin-left: 41%;">@if(isset($amount)) {{ $amount }} @else {{ 0 }} @endif</h3>
                                <p class="text-muted font-13 mb-0 text-truncate"></p>
                            </div>
                        <canvas id="graph" height="280" width="500" style="margin-top: -34px;"></canvas>
                        
                    </div>  
                </div>          
            </div>
        </div>
        </div>
    </div>
</div>


</div>



<!-- end header dashboard -->



@endsection

@section('script')

        <!-- Plugins js -->
        <script src="{{ URL::asset('assets/libs/morris-js/morris-js.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/raphael/raphael.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
        <!-- Dashboard init-->
        <!-- <script src="{{ URL::asset('assets/js/pages/dashboard-4.init.js')}}"></script> -->
        <!-- <script src="{{ URL::asset('assets/js/pages/script.js')}}"></script> -->
        <script>
            var month = <?php echo $month; ?>;
            var total_amount = <?php echo $total_amount; ?>;
            console.log(total_amount);
            var barChartData = {
                labels: month,
                datasets: [{
                    label: 'Total Earning',
                    backgroundColor: "pink",
                    data: total_amount
                }]
            };

            window.onload = function() {
                var ctx = document.getElementById("graph").getContext("2d");
                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        elements: {
                            rectangle: {
                                borderWidth: 1,
                                borderColor: '#c1c1c1',
                                borderSkipped: 'bottom'
                            }
                        },
                        responsive: true,
                        title: {
                            display: true,
                            
                        }
                    }
                });
            };
        </script>
@endsection