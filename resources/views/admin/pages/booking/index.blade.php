@extends('admin.layouts.master')
@section('content')
<style>
.panding-status{
  background-color: rgba(213,105,105,0.14) !important;
  border: 1px solid #D56969;
  border-radius: 10px;
}

.in-process-status{
  background-color: rgba(105,163,213,0.14) !important;
  border: 1px solid #699ed5;
  border-radius: 10px;
}

.completed-status{
  background-color: rgba(117,213,105,0.14) !important;
  border: 1px solid #7fd569;
  border-radius: 10px;
}

.cancelled-status{
  background-color: rgba(27,54,78,0.14) !important;
  border: 1px solid #252a38;
  border-radius: 10px;
}

.created-status{
  background-color: rgb(105 213 198 / 14%) !important;
  border: 1px solid #69c6d5;
  border-radius: 10px;
}

.scheduled-status{
  background-color: rgb(213 210 105 / 14%) !important;
  border: 1px solid #c9d569;
  border-radius: 10px;
}

.manage-order-shipment-view-address .modal1250 {
    max-width: 1250px;
}
.border-r20 {
    border-radius: 20px !important;
}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('driver')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
		</div>
	</div>
	  @include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-12">
			<div class="card  table-responsive">
				<div class="card-body table-responsive">
  				<div class="row">
  					<div class="col-md-2">
  						<label>Filter</label>
  						<select class="form-control filter-status">
  							<option value="">All</option>
  							<option value="create">Created</option>
  							<option value="pending">Pending</option>
  							<option value="scheduled">Scheduled</option>
  							<option value="in-process">In Progress</option>
  							<option value="completed">Completed</option>
  							<option value="cancelled">Cancelled</option>
  						</select>
  					</div>
  				  <div class="col-md-4 offset-md-8">
  					  <div class="pt-3 pb-4">
  						<!-- <div class="input-group m-t-12">
  							<input type="text" class="form-control" value="Filter..">
  							<span class="input-group-append">
  								<button type="button" class="btn waves-effect waves-light btn-blue"><i class="fa fa-search mr-1"></i> Filter</button>
  							</span>
  						</div> -->
  					  </div>
  				  </div>
  			  </div>
				  @include('admin.include.table')
				</div>
			</div>
		</div>
	</div>
</div>
<!-- view address -->
<div class="modal fade manage-order-shipment-view-address p-2" id="view-map" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal1250" role="document">
    <div class="modal-content border-r20">
      <div class="modal-header">
      
        <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body p-3">
        <div class="row">
          <div class="col-md-12">
          <input type="hidden" id="longitud" value="">
            <input type="hidden" id="latitud" value="">
            <div id="map-canvas" style="height: 400px;width: 100%;border-radius: 10px;position: relative; overflow: hidden;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@include('admin.include.table_script')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_KEY')}}"></script>
<script>
  function initialize(pickup_address,pickup_lat,pickup_lng,drop_address,drop_lat,drop_lng) {
    
    
  }
  // google.maps.event.addDomListener(window, "load", initialize);

  function open_map(pickup_address,pickup_lat,pickup_lng,drop_address,drop_lat,drop_lng)
  {
    var map;
    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();
    if(pickup_lat != '' && pickup_lng != '' && drop_lat != '' && drop_lng != ''){
      var locations = [[pickup_address,pickup_lat,pickup_lng,1],[drop_address,drop_lat,drop_lng,2]]
    }
    
    directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 10,
        center: new google.maps.LatLng(pickup_lat,pickup_lng),
    });
    directionsDisplay.setMap(map);
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var request = {
        travelMode: google.maps.TravelMode.DRIVING
    };
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

        if (i == 0) request.origin = marker.getPosition();
        else if (i == locations.length - 1) request.destination = marker.getPosition();
        else {
            if (!request.waypoints) request.waypoints = [];
            request.waypoints.push({
                location: marker.getPosition(),
                stopover: true
            });
        }

    }
    directionsService.route(request, function (result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
        }
    });

    directionsDisplay = new google.maps.DirectionsRenderer({
      polylineOptions: {
        strokeColor: "#9b51e0",
      }
    });

    directionsDisplay.setMap(map);

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else
            alert ('failed to get directions');
    });

    $('#view-map').modal('show');
  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
  $('.dataTables_filter').hide();
  $('.dataTables_length').hide(); 
}); 
</script>
@endsection