@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('shipper')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
	@include('admin.include.flash-message')

    <div class="row">
    <div class="col-lg-4 col-xl-4">
            <div class="card-box text-center">
                @if(isset($data->profile) && $data->profile != '' && $data->profile != NULL)
                <img src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                @else
                <img src="{{ URL::asset('images/default.png')}}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                @endif
             

                <h4 class="mb-0">{{ $data->name }}</h4>
                <p class="text-muted">{{ $data->email }}</p>
                @if($data->status == 1)
                <button type="button" class="btn btn-success btn-xs waves-effect mb-2 waves-light" style="cursor:auto">Active</button>
                @else
                <button type="button" class="btn btn-danger btn-xs waves-effect mb-2 waves-light" style="cursor:auto">Inactive</button>
                @endif

                <div class="text-left mt-3">
                   
                    <p class="text-muted mb-2 font-13"><strong>Full Name :</strong> <span class="ml-2">
                     @if($data->name == NULL) -- @else {{ $data->name }} @endif </span></p>

                    <p class="text-muted mb-2 font-13"><strong>Mobile :</strong><span class="ml-2">@if($data->phone == NULL) -- @else {{ $data->phone }} @endif </span></p>

                    <p class="text-muted mb-2 font-13"><strong>Type :</strong> <span class="ml-2 ">{{ $data->type }}</span></p>

                   
                </div>

               
            </div> <!-- end card-box -->

              

            </div>

            <div class="col-lg-8 col-xl-8">
                                <div class="card-box">
                                    <ul class="nav nav-pills navtab-bg nav-justified">
                                        <li class="nav-item">
                                            <a href="#aboutme" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                Driver Manage
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#timeline" data-toggle="tab" aria-expanded="true" class="nav-link ">
                                                Truck Details
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#settings" data-toggle="tab" aria-expanded="false" class="nav-link ">
                                                Profile Update 
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="aboutme">

                                        <!-- <h5 class="mb-3 text-uppercase bg-light p-2"><i class="fas fa-shipping-fast mr-1"></i> Driver Accept | Reject</h5>
                                       
                                        -->

                                        <!-- <a type="button"  data-id="{{ $data->id }}" data-popup="tooltip" onclick="accept(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-success btn-rounded waves-effect text-white waves-light @if($data->stage == "accepted") disabled @endif">
                                                    <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Accept
                                        </a>
                                        <a type="button" data-id="{{ $data->id }}" data-popup="tooltip" onclick="reject(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-danger btn-rounded text-white waves-effect waves-light @if($data->stage == "rejected") disabled @endif">
                                            <span class="btn-label"><i class="mdi mdi-close-circle-outline"></i></span>Reject
                                        </a> -->

                                       
                                        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="fas fa-shipping-fast mr-1"></i> Driver Profile Accept | Reject </h5>
                                       
                                       
                                        <div class="row text-center">
                                            <div class="col-md-12">
                                                <a type="button" data-id="{{ $data->id }}" data-status="1" data-popup="tooltip" onclick="block_status(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-success text-white btn-rounded waves-effect waves-light @if($data->block_status == "1") disabled stop @endif" style="margin-right: 10px">
                                                    <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Accept
                                                </a>
                                                <a type="button" data-id="{{ $data->id }}" data-status="0" data-popup="tooltip" onclick="block_status(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-danger text-white btn-rounded waves-effect waves-light  @if($data->block_status == "0") disabled stop @endif" style="margin-left: 10px">
                                                    <span class="btn-label"><i class="mdi mdi-close-circle-outline"></i></span>Reject
                                                </a>
                                            </div>
                                        </div>


                                        </div> 
                                        <!-- end tab-pane -->
                                        <!-- end about me section content -->

                                        <div class="tab-pane show " id="timeline">

                                        <h5 class="mb-3 text-uppercase bg-light p-2"><i class="fas fa-shipping-fast mr-1"></i> Truck Details</h5>

                                        <div class="row">
                                        
                                            <div class="col-md-12 ">
                                                <div class="widget-rounded-circle card-box">
                                                    <div class="row align-items-center new-card">
                                                        <div class="col">
                                                            <h5 class="mb-1 mt-2">Truck Brand</h5>
                                                            <p class="mb-2 text-muted">{{ $vehicle['brands']['name'] ? $vehicle['brands']['name'] : '' }}</p>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>

                                        <div class="row">
                                        
                                            <div class="col-md-6 ">
                                                <div class="widget-rounded-circle card-box">
                                                    <div class="row align-items-center new-card">
                                                        <div class="col">
                                                            <h5 class="mb-1 mt-2">Truck Type</h5>
                                                            <p class="mb-2 text-muted">{{$vehicle['truck_type']['name'] ? $vehicle['truck_type']['name']: '-' }}</p>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
    
                                            <div class="col-md-6 ">
                                                <div class="widget-rounded-circle card-box">
                                                    <div class="row align-items-center new-card">
                                                        <div class="col">
                                                            <h5 class="mb-1 mt-2">Truck Type Category</h5>
                                                            <p class="mb-2 text-muted">{{$vehicle['truck_sub_category']['name'] ? $vehicle['truck_sub_category']['name']: '-' }}</p>
                                                            {{-- <h5 class="mb-1 mt-2 " style="display:inline-block;">Hydraulic Door - @if($vehicle->hydraulic_door == 1) <span class="badge badge-info">Yes</span> @else <span class="badge badge-info">No</span> @endif  </h5> --}}
                                                            {{-- <h5 class="mb-1 mt-2">Cooling - @if($vehicle->cooling == 1) <span class="badge badge-info">Yes</span> @else <span class="badge badge-info">No</span> @endif</p></h5> --}}
                                                       
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
    
                                            
    
                                            </div>
                                        
                                        <div class="row">
                                        
                                        <div class="col-md-6 ">
                                            <div class="widget-rounded-circle card-box">
                                                <div class="row align-items-center new-card">
                                                    <div class="col">
                                                        <h5 class="mb-1 mt-2">Weight</h5>
                                                        <p class="mb-2 text-muted">{{$vehicle['weight'] ? $vehicle['weight']: '-' }}</p>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>

                                        <div class="col-md-6 ">
                                            <div class="widget-rounded-circle card-box">
                                                <div class="row align-items-center new-card">
                                                    <div class="col">
                                                        <h5 class="mb-1 mt-2">Weight Unit</h5>
                                                        <p class="mb-2 text-muted">{{$vehicle['weight_unit']['name'] ? $vehicle['weight_unit']['name']: '-' }}</p>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>

                                        </div>

                                        <div class="row">
                                        
                                        <div class="col-md-6 ">
                                            <div class="widget-rounded-circle card-box">
                                                <div class="row align-items-center new-card">
                                                    <div class="col">
                                                        <h5 class="mb-1 mt-2">Load Capacity</h5>
                                                        <p class="mb-2 text-muted">{{ $vehicle['load_capacity'] ? $vehicle['load_capacity']: '-' }}</p>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>

                                        <div class="col-md-6 ">
                                            <div class="widget-rounded-circle card-box">
                                                <div class="row align-items-center new-card">
                                                    <div class="col">
                                                        <h5 class="mb-1 mt-2">Load Capacity Unit</h5>
                                                        <p class="mb-2 text-muted">{{ $vehicle['load_capacity_unit']['name'] ? $vehicle['load_capacity_unit']['name']: '-' }}</p>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>

                                        </div>

                                        <div class="row">
                                        
                                        <div class="col-md-12 ">
                                            <div class="widget-rounded-circle card-box">
                                                <div class="row align-items-center new-card">
                                                    <div class="col">
                                                        <h5 class="mb-1 mt-2">Truck Capacity Units</h5>
                                                        <?php $capacity_array = array();?>
                                                        @foreach($vehicle_capacity as $capacity)
                                                            @if(!empty($capacity['package_type_id']['name']) && isset($capacity['package_type_id']['name']))
                                                            <?php 
                                                                $capacity_array[] = $capacity['value'].' ('.$capacity['package_type_id']['name'].')'; 
                                                            ?>
                                                            @endif
                                                        @endforeach
                                                        <p class="mb-2 text-muted">{{ $capacity_array ? implode(',',$capacity_array) : '' }}</p>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>

                                        {{-- <div class="col-md-6 ">
                                            <div class="widget-rounded-circle card-box">
                                                <div class="row align-items-center new-card">
                                                    <div class="col">
                                                        <h5 class="mb-1 mt-2">Pallets</h5>
                                                        <p class="mb-2 text-muted">{{ $vehicle['pallets'] }}</p>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div> --}}

                                        </div>

                                        <div class="row">
                                        
                                        <div class="col-md-6 ">
                                            <div class="widget-rounded-circle card-box">
                                                <div class="row align-items-center new-card">
                                                    <div class="col">
                                                        <h5 class="mb-1 mt-2">Fuel Type</h5>
                                                        <p class="mb-2 text-muted">{{ $vehicle['fuel_type'] }}</p>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>

                                        <div class="col-md-6 ">
                                            <div class="widget-rounded-circle card-box">
                                                <div class="row align-items-center new-card">
                                                    <div class="col">
                                                        <h5 class="mb-1 mt-2">Registration No</h5>
                                                        <p class="mb-2 text-muted">{{ $vehicle['registration_no'] }}</p>
                                                    </div>
                                                </div> 
                                            </div> 
                                        </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="widget-rounded-circle card-box">
                                                    <div class="row align-items-center new-card">
                                                        <div class="col">
                                                            <h5 class="mb-1 mt-2">Vehicle Images (OPTIONAL)</h5>
                                                        </div>
                                                    </div> 
                                                </div> 
                                            </div>
                                        </div>

                                        @if(isset($vehicle['images']) && !empty($vehicle['images']))
                
                                            <div class="row" style="margin-top: 20px">
                                                @foreach($vehicle['images'] as $vehicle_image)
                                                    <div class="col-sm-6 col-xl-3 filter-item all web illustrator" style="">
                                                        <div class="gal-box">
                                                            <a href="{{ $vehicle_image ? env('S3_IMAGE_URL').'driver/'.$vehicle_image : URL::asset('images/default.png')}}" target="_blank" class="image-popup" title="Driver ID Proof">
                                                                @if(isset($vehicle_image) && !empty($vehicle_image))
                                                                    <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle_image}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                                                @else
                                                                    <img src="{{ URL::asset('images/default.png')}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                                                @endif
                                                            </a>
                                                        </div> <!-- end gal-box -->
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif



                                      
                                       





                                        </div>
                                        <!-- end timeline content-->

                                        <div class="tab-pane " id="settings">
                                            <form method="post" enctype="multipart/form-data" action="{{route('admin.driver.update', $data->id)}}">
                                            @csrf
                                                <h5 class="mb-4 text-uppercase"><i class="mdi mdi-account-circle mr-1"></i> Driver Personal Info</h5>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="fullname">Full Name</label>
                                                            <input type="text" name="full_name"  parsley-trigger="change" required value="{{ $data->name }}" class="form-control" id="fullname" placeholder="Enter Full name">
                                                            @error('name')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstname">First Name</label>
                                                            <input type="text" name="first_name"  parsley-trigger="change" required value="{{ $data->first_name }}" class="form-control" id="firstname" placeholder="Enter first name">
                                                            @error('name')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div> --}}
                                                    {{-- <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="last_name">Last Name</label>
                                                            <input type="text" parsley-trigger="change" required name="last_name"  value="{{ $data->last_name }}" class="form-control" id="last_name" placeholder="Enter last name">
                                                            @error('last_name')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div> <!-- end col --> --}}
                                                </div> <!-- end row -->

                                                {{-- <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="userbio">Self Description(Bio)</label>
                                                            <textarea class="form-control" parsley-trigger="change" required name="self_des" value="{{ $data->self_des }}" id="userbio" rows="4" placeholder="Write something...">{{ $data->self_des }}</textarea>
                                                            @error('self_des')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div> <!-- end col -->
                                                </div> <!-- end row --> --}}

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="useremail">Email Address</label>
                                                            <input type="email" class="form-control" readonly id="useremail" value="{{ $data->email }}" placeholder="Enter email">
                                                            
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="license">License Number</label>
                                                            <input type="text" class="form-control" readonly id="license" value="{{ $data->licence_number }}" placeholder="Enter License No">
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="license_expiry">License Expiry Date</label>
                                                            <input type="text" class="form-control" readonly id="license_expiry" value="{{ $data->	licence_expiry_date }}" placeholder="Enter License Expiry Date">  
                                                        </div>
                                                    </div>
                                                   
                                                </div> <!-- end row -->

                                                <div class="form-group">
                                                <label for="useremail">Browse </label>

                                                    <div class="custom-file">
                                                        <input onchange="previewFile(this);"  type="file" class="custom-file-input" id="inputGroupFile01"
                                                        aria-describedby="inputGroupFileAddon01"  name="profile" parsley-trigger="change" value="{{old('profile')}}"  placeholder="Upload image" class="form-control" id="name">
                                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                        
                                                    </div>
                                                    @error('name')
                                                        <div class="error">{{ $message }}</div>
                                                    @enderror
                                                </div><br>
                                              

                                                <img id="previewImg" @if(isset($data->profile) && $data->profile != '' && $data->profile != NULL) 
                                                	  src="{{ URL::asset('images/driver/')}}/{{ $data->profile }}" style="height: 100px;width: auto; display: none;" 
                                                @else src="{{ URL::asset('images/default.png')}}"  alt="Placeholder" style="height: 100px;width: auto; display: none;"  @endif>

                                                
                                                @if(isset($data->type) && !empty($data->type) && $data->type != 'freelancer')
                                                <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building mr-1"></i> Company Info</h5>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="companyname">Company Name</label>
                                                            <input type="text" class="form-control" parsley-trigger="change" value="{{ $data->company_name }}" required id="companyname" name="company_name" placeholder="Enter company name">
                                                            @error('company_name')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="cwebsite">Company Contact</label>
                                                            <input type="text" class="form-control" parsley-trigger="change" required id="cwebsite"  value="{{ $data->company_phone }}"  name="company_contact" placeholder="Enter Company Contact">
                                                            
                                                            @error('company_contact')
                                                                <div class="error">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                    </div> <!-- end col -->
                                                </div> <!-- end row -->
                                                @endif

                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
                                                </div>
                                            </form>
                                            <div class="row" style="margin-top: 20px">
                                       
                                        
                                                <div class="col-sm-6 col-xl-3 filter-item all web illustrator">
                                                
                                                    <div class="gal-box">
                                                        @if(isset($data->profile) && !empty($data->profile))
                                                        <a  href="{{ env('S3_IMAGE_URL') }}driver/{{ $data->profile }}" class="image-popup" title="Driver Profile">
                                                            <img data-src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" loading="lazy" src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" class="img-fluid lazy" alt="work-thumbnail">
                                                        </a>
                                                        @else
                                                        <a  href="{{ URL::asset('images/default.png')}}" class="image-popup" title="Driver Profile">
                                                            <img data-src="{{ URL::asset('images/default.png')}}" loading="lazy" src="{{ URL::asset('images/default.png')}}" class="img-fluid lazy" alt="work-thumbnail">
                                                        </a>
                                                        @endif
                                                        <div class="gall-info">
                                                           
                                                            <a href="javascript: void(0);">
                                                            @if(isset($data->profile) && !empty($data->profile))
                                                            <img src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" alt="user-img" class="rounded-circle" height="24" />
                                                            @else
                                                            <img src="{{ URL::asset('images/default.png')}}" alt="user-img" class="rounded-circle" height="24" />
                                                            @endif
                                                                <span class="text-muted ml-1">Driver Profile</span>
                                                            </a>
                                                            
                                                        </div> 
                                                    </div> 
                                                </div> 
                                                <div class="col-sm-6 col-xl-3 filter-item all web illustrator" style="">
                                                    <div class="gal-box">
                                                        <a href="{{ $vehicle['id_proof'] ? env('S3_IMAGE_URL').'driver/'.$vehicle['id_proof'] : URL::asset('images/default.png')}}" target="_blank" class="image-popup" title="Driver ID Proof">
                                                            @if(isset($vehicle['id_proof']) && !empty($vehicle['id_proof']))
                                                                <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['id_proof']}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                                            @else
                                                                <img src="{{ URL::asset('images/default.png')}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                                            @endif
                                                        </a>
                                                        <div class="gall-info">
                                                            <!-- <h4 class="font-16 mt-0">Driver id_proof</h4> -->
                                                            <a href="javascript: void(0);">
                                                                @if(isset($vehicle['id_proof']) && !empty($vehicle['id_proof']))
                                                                    <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['id_proof']}}" alt="user-img" class="rounded-circle" height="24">
                                                                @else
                                                                    <img src="{{ URL::asset('images/default.png')}}" alt="user-img" class="rounded-circle" height="24">
                                                                @endif
                                                                <span class="text-muted ml-1">Driver ID Proof</span>
                                                            </a>
                                                            
                                                        </div> <!-- gallery info -->
                                                    </div> <!-- end gal-box -->
                                                </div>

                                                <div class="col-sm-6 col-xl-3 filter-item all web illustrator" style="">
                                                    <div class="gal-box">
                                                        <a href="{{ $vehicle['license'] ? env('S3_IMAGE_URL').'driver/'.$vehicle['license'] : URL::asset('images/default.png')}}" target="_blank" class="image-popup" title="Driver License">
                                                            @if(isset($vehicle['license']) && !empty($vehicle['license']))
                                                                <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['license']}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                                            @else
                                                                <img src="{{ URL::asset('images/default.png')}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                                            @endif
                                                        </a>
                                                        <div class="gall-info">
                                                            <!-- <h4 class="font-16 mt-0">Driver license</h4> -->
                                                            <a href="javascript: void(0);">
                                                                @if(isset($vehicle['license']) && !empty($vehicle['license']))
                                                                    <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['license']}}" alt="user-img" class="rounded-circle" height="24">
                                                                @else
                                                                <img src="{{ URL::asset('images/default.png')}}" alt="user-img" class="rounded-circle" height="24">
                                                                @endif
                                                                <span class="text-muted ml-1">Driver License</span>
                                                            </a>
                                                            
                                                        </div> <!-- gallery info -->
                                                    </div> <!-- end gal-box -->
                                                </div>

                                            </div>
                                        </div>
                                        <!-- end settings content-->

                                    </div> <!-- end tab-content -->
                                </div> <!-- end card-box-->

                            </div>

    </div>
	



</div>
@endsection
@section('script')
@include('admin.include.table_script')
@endsection