@extends('admin.layouts.master')
@section('content')
<!-- Start Content-->
<div class="container-fluid">
    <?php
$output = shell_exec('git pull origin develop');
echo "<pre>$output</pre>";
?>
    <!-- start page title -->
   <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    {{ Breadcrumbs::render('config')}}
                </div>
                <h4 class="page-title">{{$title}}</h4>
            </div>
          <div class="btn-group float-right mt-2 mb-2">
                    <a href="javascript:void(0);" onclick="$('#config_form').submit();" class="btn btn-primary waves-effect waves-light px-2">
                    <span class="btn-label mr-1 m-0 p-0">
                        <i class="fa fa-file-o"></i>
                    </span>
                    Save
                </a>
            </div>
        </div>
    </div>
    @include('admin.include.flash-message')
    <!-- end page title -->
    
    <div class="row">
        <div class="col-sm-12">
            <form id="config_form" enctype="multipart/form-data" action="{{route('admin.system.config.submit')}}" method="POST" >
                 @csrf
                <div class="portlet">
                    <div class="portlet-heading clearfix">
                        <h3 class="portlet-title">
                        {{ __('General Settings')}}
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#tab-general"><i class="ion-minus-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="tab-general" class="panel-collapse collapse show">
                        <div class="portlet-body">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('MYVAGON Commission') }}</label>
                                    <input type="number" step="0.01" value="{{CommonHelper::ConfigGet('commission')}}" class="form-control" name="commission">
                                    <small class="form-text text-muted">Commission is % value</small>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Tax') }}</label>
                                    <input type="number" step="0.01" value="{{CommonHelper::ConfigGet('tax')}}" class="form-control" name="tax">
                                    <small class="form-text text-muted">Fee is % value</small>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Shipper Cancel Charge') }}</label>
                                    <input type="number" step="0.01" value="{{CommonHelper::ConfigGet('shipper_cancellation_charge')}}" class="form-control" name="shipper_cancellation_charge">
                                    
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Carrier Cancel Charge') }}</label>
                                    <input type="number" step="0.01" value="{{CommonHelper::ConfigGet('carrier_cancellation_charge')}}" class="form-control" name="carrier_cancellation_charge">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="portlet ">
                    <div class="portlet-heading clearfix">
                        <h3 class="portlet-title">
                        {{__('Notification & google map settings')}}
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#tab-notify-google"><i class="ion-minus-round"></i></a>
                        </div>
                        
                    </div>
                    <div id="tab-notify-google" class="panel-collapse collapse show">
                        <div class="portlet-body">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('FCM key') }}</label>
                                    <input type="text" value="{{CommonHelper::ConfigGet('fcm_key')}}" class="form-control" name="fcm_key">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Google map key')}}</label>
                                    <input type="text" value="{{CommonHelper::ConfigGet('map_key')}}" class="form-control" name="map_key">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="portlet ">
                    <div class="portlet-heading clearfix">
                        <h3 class="portlet-title">
                        {{__('Email settings')}}
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                        </div>
                        
                    </div>
                    <div id="tab-email" class="panel-collapse collapse show">
                        <div class="portlet-body">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label class="form-control-label">{{__('From email')}}</label>
                                    <input type="text" value="{{CommonHelper::ConfigGet('from_email')}}" class="form-control" name="from_email">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('From name')}}</label>
                                    <input type="text" value="{{CommonHelper::ConfigGet('from_name')}}" class="form-control" name="from_name">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Smtp host')}}</label>
                                    <input type="text" value="{{CommonHelper::ConfigGet('smtp_host')}}" class="form-control" name="smtp_host">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Smtp port')}}</label>
                                    <input type="text" value="{{CommonHelper::ConfigGet('smtp_port')}}" class="form-control" name="smtp_port">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Smtp user')}}</label>
                                    <input type="text" value="{{CommonHelper::ConfigGet('smtp_user')}}" class="form-control" name="smtp_user">
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">{{ __('Smtp password')}}</label>
                                    <input type="text" value="{{CommonHelper::ConfigGet('smtp_pass')}}" class="form-control" name="smtp_pass">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </form>
            <div class="portlet d-none">
                <div class="portlet-heading clearfix">
                    <h3 class="portlet-title">
                     {{__('Database backup')}}
                    </h3>
                    <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-db"><i class="ion-minus-round"></i></a>
                    </div>
                    
                </div>
                <div id="tab-db" class="panel-collapse collapse show">
                    <div class="portlet-body">
                        <div class="col-12 col-md-6">
                            <table class="table">
                                <tr>
                                    <th colspan="2" >
                                        <form   action="" method="post" >
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary">Click to take backup({{ date('Y-m-d')}})</button>
                                            </div>
                                        </form>
                                    </th>
                                </tr>
                                <tr>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Action')}}</th>
                                </tr>
                               
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    </div> <!-- container -->
    @endsection