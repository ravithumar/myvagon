@extends('admin.layouts.master')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="page-title-box">
				<h4 class="txt-black" style="font-weight: 600;font-size: 1.25rem;color: #323a46;">Truck Type </h4>
			</div>
		</div>
	</div>
	<div class="card table-responsive">
		<div class="card-body ">
			<table class="table"  id="truck_type">
		        <thead>
		            <tr>
		            <th scope="col" style="width: 78px;">ID</th>
		            <th scope="col" style="width: 290px;">Name</th>
		            <th scope="col">Image</th>
		            </tr>
		        </thead>
      			<tbody>
	      			<div class="label-wrapper">
		              @foreach($type as $key => $truck_type)
		                <tr>
		                  <td scope="row">{{ $truck_type->id }}</td>
		                  <td scope="row">{{ $truck_type->name }}</td>
		                  @php 
		                  	$src = 'images/type/'.$truck_type['icon'];
						      @endphp
		                  <td><img src="{{ asset($src)}}" scope="row" width="150" ></td><br/>
		                </tr>
		              @endforeach
		          	</div>
      			</tbody>
			</table>
  		</div>
	</div>
		
	<div class="row">
		<div class="col-xl-12">
			<div class="page-title-box">
				<h4 class="txt-black" style="font-weight: 600;font-size: 1.25rem;color: #323a46;">Truck Category </h4>
			</div>
		</div>
	</div>
	<div class="card table-responsive" >
		<div class="card-body ">
			<table class="table" id="example">
		        <thead>
		            <tr>
		            <th scope="col" style="width: 11%;">ID</th>
		            <th scope="col" style="width: 41%;">Name</th>
		            <th scope="col" style="width: 35%;">Truck Type</th>
		            </tr>
		        </thead>
      			<tbody>
	      			<div class="label-wrapper">
		              @foreach($data as  $trucks)
		                <tr>
		                  <td scope="row">{{$trucks->id}}</td>
		                  <td scope="row">{{ $trucks->name }}</td>
		                  <td scope="row">{{ $trucks->type }}</td>
		                </tr>
		              @endforeach
		          	</div>
      			</tbody>
			</table>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xl-12">
			<div class="page-title-box">
				<h4 class="txt-black" style="font-weight: 600;font-size: 1.25rem;color: #323a46;">Package Type </h4>
			</div>
		</div>
	</div>
	<div class="card table-responsive">
		<div class="card-body">
			<table class="table" id="package_type">
		        <thead>
		            <tr>
		            <th scope="col" style="width: 14%;">ID</th>
		            <th scope="col" style="width: 22%;">Name</th>
		            <th scope="col">Status</th>
		            </tr>
		        </thead>
      			<tbody>
	      			<div class="label-wrapper">
		              @foreach($package as  $package_item)
		                <tr>
		                  <td scope="row">{{$package_item->id}}</td>
		                  <td scope="row" >{{ $package_item->name }}</td>
		                  <td>@if($package_item->status == 1)<span class="badge badge-success">Active</span>@else <span class="badge badge-danger">Inactive</span> @endif</td>
		                </tr>
		              @endforeach
		          	</div>
		          	
      			</tbody>
			</table>
  		</div>
  	</div>
</div>

@endsection
@section('script')
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
		$('#example').DataTable();
	} );
	$(document).ready(function() {
		$('#truck_type').DataTable();
	} );
	$(document).ready(function() {
		$('#package_type').DataTable();
	} );
</script>
@include('shipper.include.table_script')
@endsection
