@extends('admin.layouts.master')

@section('content')
<style type="text/css">
  .pac-container {
    z-index: 9999;
}
.sorting-btn{
    border-radius: 20px;
    /* margin: 0px 10px; */
    padding: 2px 20px;
}
.sort-button .active{
  background-color: #eadafd;
  color: #9B51E0 !important;
  -webkit-box-shadow : none;
  box-shadow : none;
}
.table td {
  border-left: none;
  border-right: none;
}
.table th {
  border-left: none;
  border-right: none;
}
.panding-status{
  background-color: rgba(213,105,105,0.14) !important;
  border: 1px solid #D56969;
  border-radius: 10px;
}

.in-process-status{
  background-color: rgba(105,163,213,0.14) !important;
  border: 1px solid #699ed5;
  border-radius: 10px;
}

.completed-status{
  background-color: rgba(117,213,105,0.14) !important;
  border: 1px solid #7fd569;
  border-radius: 10px;
}

.cancelled-status{
  background-color: rgba(27,54,78,0.14) !important;
  border: 1px solid #252a38;
  border-radius: 10px;
}

.created-status{
  background-color: rgb(105 213 198 / 14%) !important;
  border: 1px solid #69c6d5;
  border-radius: 10px;
}

.scheduled-status{
  background-color: rgb(213 210 105 / 14%) !important;
  border: 1px solid #c9d569;
  border-radius: 10px;
}
.txt-blue {
    color: #1f1f41 !important;
    font-weight: 600;
}
.manage-order-shipment-view-address .modal1250 {
    max-width: 1250px;
}
.border-r20 {
    border-radius: 20px !important;
}
.box-over:hover {
        border : 1px solid #9b51e0 !important;
        box-shadow: 0 0px 7px 0 rgb(155 81 224/50%);
    }
    .box-over{
        border : 1px solid #9b51e0 !important;
        box-shadow: 0 0px 7px 0 rgb(155 81 224/50%);
    }
    .overflow-height .active{
        border: 1px solid #9b51e0 !important;
        box-shadow: 0 0px 7px 0 rgb(155 81 224 / 50%);
    }
</style>
<!-- <div class="container-fluid">
    <div class="col">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="javascript:;" class="d-flex align-items-center txt-blue"><img src="{{ URL::asset('shipper/images/manage-shipments.svg')}}" class="img-fluid">{{ $dateTableTitle }}</a></li>
            </ol>
        </nav>
    </div>
    <div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('driver')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
		</div>
	</div>
	@include('admin.include.flash-message')
  
  <div class="row d-flex align-items-center sort-button">
    <div class="col-md-12" style="margin:7px 0px 25px 0px;">
      <button type="button" class="btn txt-blue sorting-btn active filter_btn" data-value="">All</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="pending">Pending</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="scheduled">Scheduled</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="in-progress">In-Progress</button>
      <button type="button" class="btn txt-blue sorting-btn filter_btn" data-value="completed">Past</button>
    </div>
  </div>
  <div class="row">
    	<div class="col-md-12 col-xl-6">
    		<h3 class="text-muted">Recent <span class="text-dark ml-2">{{ date('d-m-Y') }}</span></h3>
            <div class="overflow-height">
                <div class="row border box-over bg-white pl-2 py-2 mt-3 mx-1 pr-0 rounded-lg booking_row"  data-from-position="1" data-to-position="2">
                    <div class="col-md-8 col-sm-8 col-12">
                    	@if(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'book'))
                        <div class="d-flex">
                        	<p class="font-14 font-weight-400" style="font-weight: bold;">{{$data['type']['bid']['id']}}</p>
                        </div>
                        @elseif(isset($data['type']) && !empty($data['type']) && ($data['type'] == 'bid'))
                        <div class="d-flex">
                        	<p class="font-14 font-weight-400" style="font-weight: bold;">{{$data['type']['book']['id']}}</p>
                        </div>
                        @endif
                        <h4 class="font-14 ">@if(isset($data['booking_location'][0]['company_name']) ? $data['booking_location'][0]['company_name'] : "-") {{$data['company_name']}} @endif</h4>
                        
            			      <div class="track-order-list">
  			                  
        				        </div>
        			       </div>
        			     
        			       <div class="col-md-4 col-sm-4 col-12 text-md-right text-left pr-0">

        				      <h3 class="color-dark-purple my-2 font-weight-bold pr-2">@if(isset($booking['amount'])) € {{ $booking['amount'] }} @else  @endif</h3>
                        @if(isset($booking['status']) && !empty($booking['status']))
                            @if($booking['status'] == 'create')
                                <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-red text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending" style="color: white;
    							               background-color: rgba(213,105,105);border-bottom-left-radius: 40px;border-top-left-radius: 40px;">Pending</a>
                            @endif
                            @if($booking['status'] == 'pending')
                                <a href="" class="bg-red text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending" style="color: white;
    							               background-color: rgba(213,105,105);border-bottom-left-radius: 40px;border-top-left-radius: 40px;">Pending</a>
                            @endif
                            @if($booking['status'] == 'scheduled')
                                <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-yellow text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending" style="color: white;
    							             background-color: rgba(213,105,105);border-bottom-left-radius: 40px;border-top-left-radius: 40px;">Scheduled</a>
                            @endif
                            @if($booking['status'] == 'in-progress')
                                <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-blue text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending" style="color: white;
    							               background-color: rgba(213,105,105);border-bottom-left-radius: 40px;border-top-left-radius: 40px;">In-Process</a>
                            @endif 
                            @if($booking['status'] == 'completed')
                                <a href="{{ url('shipper/manage-shipment',$booking['id']) }}" class="bg-green text-white py-2 pr-2 pl-3 d-inline-block text-uppercase font-weight-bold recent-pending" style="color: white;
    							               background-color: rgba(213,105,105);border-bottom-left-radius: 40px;border-top-left-radius: 40px;">Completed</a>
                            @endif 
                        @endif
                        
                        
                    </div>
        		</div>
        	</div>
        </div>
                        
                    
</div> -->
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <h4 class="page-title">{{$dateTableTitle}}</h4>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <h4 class="page-title">{{$paymentTableTitle}}
        <a href="{{ url('admin/driver/generate_invoice',$data->id) }}" class="btn btn-info" style="color: #1f1f41;font-weight: 600;border-radius: 5px;background-color: #e9e9ef;border-color: #6658dd;margin: 15px;font-size: initial;">Generate Invoice</a></h4>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <h4 class="page-title">{{$driverTableTitle}}
          <a href="{{ asset(url('/admin/driver/view_profile',$data['id'])) }}" class="btn btn-info" style="color: #1f1f41;font-weight: 600;border-radius: 5px;background-color: #e9e9ef;border-color: #6658dd;margin: 15px;font-size: initial;width: 116px;">View/Edit</a></h4>
      </div>
    </div>
  </div>
  <div class="col-xl-8">
    <div class="card">
      <div class="card-body table-responsive">
        <div class="tab-pane " id="settings">
          <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="fullname">Full Name</label>
                    <input type="text" name="full_name"  parsley-trigger="change" required value="{{ $data->name }}" class="form-control" id="fullname" placeholder="Enter Full name" readonly>
                    @error('name')
                        <div class="error">{{ $message }}</div>
                    @enderror
                </div>
            </div>
          </div> 
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label for="useremail">Email Address</label>
                      <input type="email" class="form-control" readonly id="useremail" value="{{ $data->email }}" placeholder="Enter email">
                      
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label for="license">License Number</label>
                  <input type="text" class="form-control" readonly id="license" value="{{ $data->licence_number }}" placeholder="Enter License No">
              </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="license_expiry">License Expiry Date</label>
                    <input type="text" class="form-control" readonly id="license_expiry" value="{{ $data->  licence_expiry_date }}" placeholder="Enter License Expiry Date">  
                </div>
            </div>
          </div> 
          <div class="form-group">
          </div><br>
            <img id="previewImg" @if(isset($data->profile) && $data->profile != '' && $data->profile != NULL) 
              src="{{ URL::asset('images/driver/')}}/{{ $data->profile }}" style="height: 100px;width: auto; display: none;" 
              @else src="{{ URL::asset('images/default.png')}}"  alt="Placeholder" style="height: 100px;width: auto; display: none;"  @endif>
              @if(isset($data->type) && !empty($data->type) && $data->type != 'freelancer')
                <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building mr-1"></i> Company Info</h5>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="companyname">Company Name</label>
                            <input type="text" class="form-control" parsley-trigger="change" value="{{ $data->company_name }}" required id="companyname" name="company_name" placeholder="Enter company name">
                            @error('company_name')
                                <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cwebsite">Company Contact</label>
                            <input type="text" class="form-control" parsley-trigger="change" required id="cwebsite"  value="{{ $data->company_phone }}"  name="company_contact" placeholder="Enter Company Contact">
                            
                            @error('company_contact')
                                <div class="error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
              @endif
              <div class="row" style="margin-top: 20px">
                <div class="col-sm-6 col-xl-3 filter-item all web illustrator">
                  <div class="gal-box">
                      @if(isset($data->profile) && !empty($data->profile))
                      <a  href="{{ env('S3_IMAGE_URL') }}driver/{{ $data->profile }}" class="image-popup" title="Driver Profile">
                          <img data-src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" loading="lazy" src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" class="img-fluid lazy" alt="work-thumbnail">
                      </a>
                      @else
                      <a  href="{{ URL::asset('images/default.png')}}" class="image-popup" title="Driver Profile">
                          <img data-src="{{ URL::asset('images/default.png')}}" loading="lazy" src="{{ URL::asset('images/default.png')}}" class="img-fluid lazy" alt="work-thumbnail">
                      </a>
                      @endif
                      <div class="gall-info">
                          <a href="javascript: void(0);">
                          @if(isset($data->profile) && !empty($data->profile))
                          <img src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" alt="user-img" class="rounded-circle" height="24" />
                          @else
                          <img src="{{ URL::asset('images/default.png')}}" alt="user-img" class="rounded-circle" height="24" />
                          @endif
                              <span class="text-muted ml-1">Driver Profile</span>
                          </a>
                      </div> 
                  </div> 
                </div> 
                <div class="col-sm-6 col-xl-3 filter-item all web illustrator" style="">
                    <div class="gal-box">
                        <a href="{{ $vehicle['id_proof'] ? env('S3_IMAGE_URL').'driver/'.$vehicle['id_proof'] : URL::asset('images/default.png')}}" target="_blank" class="image-popup" title="Driver ID Proof">
                            @if(isset($vehicle['id_proof']) && !empty($vehicle['id_proof']))
                                <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['id_proof']}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                            @else
                                <img src="{{ URL::asset('images/default.png')}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                            @endif
                        </a>
                        <div class="gall-info">
                            <!-- <h4 class="font-16 mt-0">Driver id_proof</h4> -->
                            <a href="javascript: void(0);">
                                
                                <span class="text-muted ml-1">Driver ID Proof</span>
                            </a>
                            
                        </div> <!-- gallery info -->
                    </div> <!-- end gal-box -->
              </div>

              <div class="col-sm-6 col-xl-3 filter-item all web illustrator" style="">
                <div class="gal-box">
                    <a href="{{ $vehicle['license'] ? env('S3_IMAGE_URL').'driver/'.$vehicle['license'] : URL::asset('images/default.png')}}" target="_blank" class="image-popup" title="Driver License">
                        @if(isset($vehicle['license']) && !empty($vehicle['license']))
                            <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['license']}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                        @else
                            <img src="{{ URL::asset('images/default.png')}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                        @endif
                    </a>
                    <div class="gall-info">
                        <!-- <h4 class="font-16 mt-0">Driver license</h4> -->
                        <a href="javascript: void(0);">
                            
                            <span class="text-muted ml-1">Driver License</span>
                        </a>
                        
                    </div> <!-- gallery info -->
                </div> <!-- end gal-box -->
              </div>

              </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <h4 class="page-title">{{$truckdetailsTableTitle}}</h4>
      </div>
    </div>
    <div class="col-xl-8">
      <div class="card">
        <div class="card-body table-responsive">
          <div class="tab-pane show " id="timeline">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="widget-rounded-circle card-box">
                        <div class="row align-items-center new-card">
                            <div class="col">
                                <h5 class="mb-1 mt-2">Truck Brand</h5>
                                <p class="mb-2 text-muted">{{ $vehicle['brands']['name'] ? $vehicle['brands']['name'] : '' }}</p>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="widget-rounded-circle card-box">
                        <div class="row align-items-center new-card">
                            <div class="col">
                                <h5 class="mb-1 mt-2">Truck Type</h5>
                                <p class="mb-2 text-muted">{{$vehicle['truck_type']['name'] ? $vehicle['truck_type']['name']: '-' }}</p>
                            </div>
                        </div> 
                    </div> 
                </div>
                <div class="col-md-6 ">
                    <div class="widget-rounded-circle card-box">
                        <div class="row align-items-center new-card">
                            <div class="col">
                                <h5 class="mb-1 mt-2">Truck Type Category</h5>
                                <p class="mb-2 text-muted">{{$vehicle['truck_sub_category']['name'] ? $vehicle['truck_sub_category']['name']: '-' }}</p>                                                       
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>
            <div class="row">
              <div class="col-md-6 ">
                  <div class="widget-rounded-circle card-box">
                      <div class="row align-items-center new-card">
                          <div class="col">
                              <h5 class="mb-1 mt-2">Weight</h5>
                              <p class="mb-2 text-muted">{{$vehicle['weight'] ? $vehicle['weight']: '-' }}</p>
                          </div>
                      </div> 
                  </div> 
              </div>
              <div class="col-md-6 ">
                  <div class="widget-rounded-circle card-box">
                      <div class="row align-items-center new-card">
                          <div class="col">
                              <h5 class="mb-1 mt-2">Weight Unit</h5>
                              <p class="mb-2 text-muted">{{$vehicle['weight_unit']['name'] ? $vehicle['weight_unit']['name']: '-' }}</p>
                          </div>
                      </div> 
                  </div> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 ">
                  <div class="widget-rounded-circle card-box">
                      <div class="row align-items-center new-card">
                          <div class="col">
                              <h5 class="mb-1 mt-2">Load Capacity</h5>
                              <p class="mb-2 text-muted">{{ $vehicle['load_capacity'] ? $vehicle['load_capacity']: '-' }}</p>
                          </div>
                      </div> 
                  </div> 
              </div>
              <div class="col-md-6 ">
                  <div class="widget-rounded-circle card-box">
                      <div class="row align-items-center new-card">
                          <div class="col">
                              <h5 class="mb-1 mt-2">Load Capacity Unit</h5>
                              <p class="mb-2 text-muted">{{ $vehicle['load_capacity_unit']['name'] ? $vehicle['load_capacity_unit']['name']: '-' }}</p>
                          </div>
                      </div> 
                  </div> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 ">
                  <div class="widget-rounded-circle card-box">
                      <div class="row align-items-center new-card">
                          <div class="col">
                              <h5 class="mb-1 mt-2">Truck Capacity Units</h5>
                              <?php $capacity_array = array();?>
                              @foreach($vehicle_capacity as $capacity)
                                  @if(!empty($capacity['package_type_id']['name']) && isset($capacity['package_type_id']['name']))
                                  <?php 
                                      $capacity_array[] = $capacity['value'].' ('.$capacity['package_type_id']['name'].')'; 
                                  ?>
                                  @endif
                              @endforeach
                              <p class="mb-2 text-muted">{{ $capacity_array ? implode(',',$capacity_array) : '' }}</p>
                          </div>
                      </div> 
                  </div> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 ">
                  <div class="widget-rounded-circle card-box">
                      <div class="row align-items-center new-card">
                          <div class="col">
                              <h5 class="mb-1 mt-2">Fuel Type</h5>
                              <p class="mb-2 text-muted">{{ $vehicle['fuel_type'] }}</p>
                          </div>
                      </div> 
                  </div> 
              </div>
              <div class="col-md-6">
                  <div class="widget-rounded-circle card-box">
                      <div class="row align-items-center new-card">
                          <div class="col">
                              <h5 class="mb-1 mt-2">Vehicle Images (OPTIONAL)</h5>
                          </div>
                      </div> 
                  </div> 
              </div>
          </div>
        </div>
          @if(isset($vehicle['images']) && !empty($vehicle['images']))
          <div class="row" style="margin-top: 20px">
              @foreach($vehicle['images'] as $vehicle_image)
                  <div class="col-sm-6 col-xl-3 filter-item all web illustrator" style="">
                      <div class="gal-box">
                          <a href="{{ $vehicle_image ? env('S3_IMAGE_URL').'driver/'.$vehicle_image : URL::asset('images/default.png')}}" target="_blank" class="image-popup" title="Driver ID Proof">
                              @if(isset($vehicle_image) && !empty($vehicle_image))
                                  <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle_image}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                              @else
                                  <img src="{{ URL::asset('images/default.png')}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                              @endif
                          </a>
                      </div> <!-- end gal-box -->
                  </div>
              @endforeach
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>                               
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <h4 class="page-title">{{$TrailerTableTitle}}</h4>
      </div>
    </div>
    <div class="col-md-6 ">
      <div class="widget-rounded-circle card-box">
          <div class="row align-items-center new-card">
              <div class="col">
                  <h5 class="mb-1 mt-2">Trailer Registration No</h5>
                  <p class="mb-2 text-muted">{{ $vehicle['registration_no'] }}</p>
              </div>
          </div> 
      </div> 
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <h4 class="page-title">{{$chatheadingTitle}}</h4>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="page-title-box">
        <h4 class="page-title">{{$ActiveheadingTitle}}</h4>
        
      </div>
    </div>
  </div>
  <div class="row text-center">
      <div class="col-md-12">
          <a type="button" data-id="{{ $data->id }}" data-status="1" data-popup="tooltip" onclick="block_status(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-success text-white btn-rounded waves-effect waves-light @if($data->block_status == "1") disabled stop @endif" style="margin-right: 10px">
              <span class="btn-label"><i class="mdi mdi-check-all"></i></span>Accept
          </a>
          <a type="button" data-id="{{ $data->id }}" data-status="0" data-popup="tooltip" onclick="block_status(this);return false;" data-token="{{ csrf_token() }}" class="btn btn-danger text-white btn-rounded waves-effect waves-light  @if($data->block_status == "0") disabled stop @endif" style="margin-left: 10px">
              <span class="btn-label"><i class="mdi mdi-close-circle-outline"></i></span>Reject
          </a>
      </div>
  </div>
</div>

@endsection