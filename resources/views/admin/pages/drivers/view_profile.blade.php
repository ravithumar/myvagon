@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
	@include('admin.include.flash-message')
    <div class="row">
    	<div class="col-lg-4 col-xl-4">
            <div class="card-box text-center">
                @if(isset($data->profile) && $data->profile != '' && $data->profile != NULL)
                <img src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                @else
                <img src="{{ URL::asset('images/default.png')}}" class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                @endif
                <h4 class="mb-0">{{ $data->name }}</h4>
                <p class="text-muted">{{ $data->email }}</p>
                @if($data->status == 1)
                <button type="button" class="btn btn-success btn-xs waves-effect mb-2 waves-light" style="cursor:auto">Active</button>
                @else
                <button type="button" class="btn btn-danger btn-xs waves-effect mb-2 waves-light" style="cursor:auto">Inactive</button>
                @endif
                <div class="text-left mt-3">
                   
                    <p class="text-muted mb-2 font-13"><strong>Full Name :</strong> <span class="ml-2">
                     @if($data->name == NULL) -- @else {{ $data->name }} @endif </span></p>

                    <p class="text-muted mb-2 font-13"><strong>Mobile :</strong><span class="ml-2">@if($data->phone == NULL) -- @else {{ $data->phone }} @endif </span></p>

                    <p class="text-muted mb-2 font-13"><strong>Type :</strong> <span class="ml-2 ">{{ $data->type }}</span></p>
                </div>
            </div> 
        </div>
        <div class="col-lg-6 col-xl-6">
	        <div class="card-box">
	            <ul class="nav nav-pills navtab-bg nav-justified">
	                <li class="nav-item">
	                    <a href="#aboutme" data-toggle="tab" aria-expanded="false" class="nav-link active">
	                        Driver Manage
	                    </a>
	                </li>
	        	</ul>
	        	<div class="tab-pane " id="settings">
                    <form method="post" enctype="multipart/form-data" action="{{route('admin.driver.update', $data->id)}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="fullname">Full Name</label>
                                <input type="text" name="full_name"  parsley-trigger="change" required value="{{ $data->name }}" class="form-control" id="fullname" placeholder="Enter Full name">
                                @error('name')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                   	</div> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="useremail">Email Address</label>
                                <input type="email" class="form-control" readonly id="useremail" value="{{ $data->email }}" placeholder="Enter email">
                            </div>
                        </div>
                    </div>
	                <div class="row">
	                    <div class="col-md-6">
	                        <div class="form-group">
	                            <label for="license">License Number</label>
	                            <input type="text" class="form-control" readonly id="license" value="{{ $data->licence_number }}" placeholder="Enter License No">
	                        </div>
	                    </div>
	                    <div class="col-md-6">
	                        <div class="form-group">
	                            <label for="license_expiry">License Expiry Date</label>
	                            <input type="text" class="form-control" readonly id="license_expiry" value="{{ $data->	licence_expiry_date }}" placeholder="Enter License Expiry Date">  
	                        </div>
	                    </div>
	                </div> 
                    <div class="form-group">
                    <label for="useremail">Browse </label>
                        <div class="custom-file">
                            <input onchange="previewFile(this);"  type="file" class="custom-file-input" id="inputGroupFile01"
                            aria-describedby="inputGroupFileAddon01"  name="profile" parsley-trigger="change" value="{{old('profile')}}"  placeholder="Upload image" class="form-control" id="name">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            
                        </div>
                        @error('name')
                            <div class="error">{{ $message }}</div>
                        @enderror
                    </div><br>
                    <img id="previewImg" @if(isset($data->profile) && $data->profile != '' && $data->profile != NULL) 
                    	  src="{{ URL::asset('images/driver/')}}/{{ $data->profile }}" style="height: 100px;width: auto; display: none;" 
                    @else src="{{ URL::asset('images/default.png')}}"  alt="Placeholder" style="height: 100px;width: auto; display: none;"  @endif>
                    @if(isset($data->type) && !empty($data->type) && $data->type != 'freelancer')
                    <h5 class="mb-3 text-uppercase bg-light p-2"><i class="mdi mdi-office-building mr-1"></i> Company Info</h5>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="companyname">Company Name</label>
                                <input type="text" class="form-control" parsley-trigger="change" value="{{ $data->company_name }}" required id="companyname" name="company_name" placeholder="Enter company name">
                                @error('company_name')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cwebsite">Company Contact</label>
                                <input type="text" class="form-control" parsley-trigger="change" required id="cwebsite"  value="{{ $data->company_phone }}"  name="company_contact" placeholder="Enter Company Contact">
                                
                                @error('company_contact')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                    @endif
                    <div class="text-right">
                        <button type="submit" class="btn btn-success waves-effect waves-light mt-2"><i class="mdi mdi-content-save"></i> Save</button>
                    </div>
                </form>
                <div class="row" style="margin-top: 20px">
                    <div class="col-sm-6 col-xl-3 filter-item all web illustrator">
                        <div class="gal-box">
                            @if(isset($data->profile) && !empty($data->profile))
                            <a  href="{{ env('S3_IMAGE_URL') }}driver/{{ $data->profile }}" class="image-popup" title="Driver Profile">
                                <img data-src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" loading="lazy" src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" class="img-fluid lazy" alt="work-thumbnail">
                            </a>
                            @else
                            <a  href="{{ URL::asset('images/default.png')}}" class="image-popup" title="Driver Profile">
                                <img data-src="{{ URL::asset('images/default.png')}}" loading="lazy" src="{{ URL::asset('images/default.png')}}" class="img-fluid lazy" alt="work-thumbnail">
                            </a>
                            @endif
                            <div class="gall-info">
                               
                                <a href="javascript: void(0);">
                                @if(isset($data->profile) && !empty($data->profile))
                                <img src="{{ env('S3_IMAGE_URL')}}driver/{{ $data->profile }}" alt="user-img" class="rounded-circle" height="24" />
                                @else
                                <img src="{{ URL::asset('images/default.png')}}" alt="user-img" class="rounded-circle" height="24" />
                                @endif
                                    <span class="text-muted ml-1">Driver Profile</span>
                                </a>
                            </div> 
                        </div> 
                    </div> 
                    <div class="col-sm-6 col-xl-3 filter-item all web illustrator" style="">
                        <div class="gal-box">
                            <a href="{{ $vehicle['id_proof'] ? env('S3_IMAGE_URL').'driver/'.$vehicle['id_proof'] : URL::asset('images/default.png')}}" target="_blank" class="image-popup" title="Driver ID Proof">
                                @if(isset($vehicle['id_proof']) && !empty($vehicle['id_proof']))
                                    <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['id_proof']}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                @else
                                    <img src="{{ URL::asset('images/default.png')}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                @endif
                            </a>
                            <div class="gall-info">
                                <!-- <h4 class="font-16 mt-0">Driver id_proof</h4> -->
                                <a href="javascript: void(0);">
                                    @if(isset($vehicle['id_proof']) && !empty($vehicle['id_proof']))
                                        <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['id_proof']}}" alt="user-img" class="rounded-circle" height="24">
                                    @else
                                        <img src="{{ URL::asset('images/default.png')}}" alt="user-img" class="rounded-circle" height="24">
                                    @endif
                                    <span class="text-muted ml-1">Driver ID Proof</span>
                                </a>
                            </div> <!-- gallery info -->
                        </div> <!-- end gal-box -->
                    </div>

                    <div class="col-sm-6 col-xl-3 filter-item all web illustrator" style="">
                        <div class="gal-box">
                            <a href="{{ $vehicle['license'] ? env('S3_IMAGE_URL').'driver/'.$vehicle['license'] : URL::asset('images/default.png')}}" target="_blank" class="image-popup" title="Driver License">
                                @if(isset($vehicle['license']) && !empty($vehicle['license']))
                                    <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['license']}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                @else
                                    <img src="{{ URL::asset('images/default.png')}}" target="_blank" class="img-fluid" alt="work-thumbnail">
                                @endif
                            </a>
                            <div class="gall-info">
                                <!-- <h4 class="font-16 mt-0">Driver license</h4> -->
                                <a href="javascript: void(0);">
                                    @if(isset($vehicle['license']) && !empty($vehicle['license']))
                                        <img src="{{ env('S3_IMAGE_URL').'driver/'.$vehicle['license']}}" alt="user-img" class="rounded-circle" height="24">
                                    @else
                                    <img src="{{ URL::asset('images/default.png')}}" alt="user-img" class="rounded-circle" height="24">
                                    @endif
                                    <span class="text-muted ml-1">Driver License</span>
                                </a>
                                
                            </div> <!-- gallery info -->
                        </div> <!-- end gal-box -->
                    </div>

                </div>
            </div>
	        </div>

    	</div>
    </div>
    
</div>
@endsection