@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('driver')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
	  @include('admin.include.flash-message')
	<div class="row">
	
		<div class="col-xl-12">
		

			<div class="card  table-responsive">
			
				<div class="card-body">
				<div class="row">
				<div class="col-md-4 offset-md-8">
					<div class="pt-3 pb-4">
						{{-- <div class="input-group m-t-12">
							<input type="text" class="form-control" value="Filter..">
							<span class="input-group-append">
								<button type="button" class="btn waves-effect waves-light btn-blue"><i class="fa fa-search mr-1"></i> Filter</button>
							</span>
						</div> --}}
						
					</div>
				</div>
			</div>

					@include('admin.include.table')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
@include('admin.include.table_script')
@endsection