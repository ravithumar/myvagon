@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="page-title-box">
				<div class="page-title-right">
					{{ Breadcrumbs::render('truck')}}
				</div>
				<h4 class="page-title">{{$dateTableTitle}}</h4>
			</div>
			
		</div>
	</div>
	  @include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-4">
			<div class="card">
				<div class="card-body" >
                @if(isset($edit_param) && $edit_param != null)
                    <h4 class="header-title mb-2">Edit Truck Unit</h4>
					<form method="post" action="{{ route('admin.truck.unit.update', $edit_param->id )}}">
						@csrf
						<div class="form-group">
							<label for="name">Name<span class="text-danger">*</span></label>
							<input type="text" maxlength="20" name="name"  parsley-trigger="change" value="{{ $edit_param->name }}" required placeholder="Enter name" class="form-control" id="name">
							@error('name')
							<div class="error">{{ $message }}</div>
							@enderror
						</div>

                        <!-- <div class="form-group" style="display: none;">
							<label for="name">Charge<span class="text-danger">*</span></label>
							<input type="text" maxlength="10" name="charge" parsley-trigger="change" value="{{ $edit_param->charge }}" required placeholder="Enter Charge" class="form-control" id="charge">
							@error('name')
							<div class="error">{{ $message }}</div>
							@enderror
						</div> -->

						<div class="form-group text-right m-b-0">
							<button class="btn btn-primary waves-effect waves-light" type="submit">
							Submit
							</button>
							<button type="reset" class="btn btn-secondary waves-effect m-l-5">
							Reset
							</button>
						</div>
					</form>

               @else

                    <h4 class="header-title mb-2">Add Truck Unit</h4>
					<form method="post" action="{{route('admin.truck.unit.submit')}}">
						@csrf
						<div class="form-group">
							<label for="name">Name<span class="text-danger">*</span></label>
							<input type="text" maxlength="20" name="name"  value="{{old('name')}}" parsley-trigger="change" required placeholder="Enter name" class="form-control" id="name">
							@error('name')
							<div class="error text-danger">{{ $message }}</div>
							@enderror
						</div>

                        <!-- <div class="form-group" style="display: none;">
							<label for="name">Charge<span class="text-danger">*</span></label>
							<input type="text" maxlength="10" name="charge" parsley-trigger="change" value="{{old('charge')}}" required placeholder="Enter Charge" class="form-control numeric" id="charge">
							@error('name')
							<div class="error">{{ $message }}</div>
							@enderror
						</div> -->

						<div class="form-group text-right m-b-0">
							<button class="btn  btn-primary waves-effect waves-light" type="submit">
							Submit
							</button>
							<button type="reset" onclick="return reset();" class="btn btn-secondary waves-effect m-l-5">
							Reset
							</button>
						</div>
					</form>

                @endif

					
				</div>
			</div>
		</div>

		<div class="col-xl-8">
			<div class="card">
				<div class="card-body table-responsive" >
					@include('admin.include.table')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
@include('admin.include.table_script')
@endsection