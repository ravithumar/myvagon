@extends('admin.layouts.master')
@section('content')
<!DOCTYPE html>
<html lang="en">

<body>
 <main id="main" class="main">
    <div class="pagetitle">
      <h1>Profile</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Profile</li>
        </ol>
      </nav>
    </div><!-- End Page Title -->
    <section class="section profile" id="MyDetailsModal">
      	<div class="row">
        	<div class="col-xl-4">
          		<div class="card">
	            	<div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
	              		<img id="previewImg" @if($user_data->profile != "") src="{{ env('S3_IMAGE_URL')}}shipper/{{ $user_data->profile }}" style="height: 100px;width: 174px;display: block;" @else src="{{ url('images/default.png') }}"  alt="Placeholder" style="height: 100px;width: 174px;display: block;"  @endif> 
			            <h2>{{$user_data->name}}</h2>
			            <h5>{{$user_data->email}}</h5>
			            <h5>{{$user_data->phone}}</h5>
			            <div class="social-links mt-2">
			                <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
			                <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
			                <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
			                <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
	              		</div>
	            	</div>
          		</div>
        	</div>
        	<div class="col-xl-8">
          		<div class="card">
            		<div class="card-body pt-3">
              			<div class="container">
			                <ul class="nav nav-tabs">
			                  <li class="active"><a data-toggle="tab" href="#home">profile overview</a></li>
			                  <li><a data-toggle="tab" href="#menu1" style="margin-left: 10px;">profile edit</a></li>
			                  <li><a data-toggle="tab" href="#menu2"  style="margin-left: 10px;">change password</a></li>
			                </ul>
                			<div class="tab-content">
                    			<div id="home" class="tab-pane fade in active" >
                      				<div class="profile-overview" style="color: black;">
				                        <h4 class="card-title"><b>Profile Overview :</b></h4>
				                        <p class="small fst-italic " style="font-weight: 500;font-size: 14px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				                        <h5 class="card-title">Profile Details :</h5>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4 ">Full Name</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['name']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4">Email</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['email']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4">Phone</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['phone']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4">Country Code</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['country_code']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4 ">Type</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['type']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4 ">Company</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['company_name']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4 ">Post Code</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['post_code']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4 ">City</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['city']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4 ">State</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['state']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4 ">Country</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['country']}}</div>
				                        </div>
				                        <div class="row">
				                          <div class="col-lg-3 col-md-4 ">Licence No</div>
				                          <div class="col-lg-9 col-md-8">{{$user_data['licence_no']}}</div>
				                        </div>
                      				</div>
                    			</div>
                    			<div id="menu1" class="tab-pane fade" >
                      				<form>
				                        <div class="row mb-3">
				                          <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
				                          <div class="col-md-8 col-lg-9">
				                            <img id="previewImg" @if($user_data->profile != "") src="{{ env('S3_IMAGE_URL')}}shipper/{{ $user_data->profile }}" style="height: 100px;width: 174px;display: block;" @else src="{{ url('images/default.png') }}"  alt="Placeholder" style="height: 100px;width: 174px;display: block;"  @endif>
				                            <div class="pt-2">
				                              <a href="#" class="btn btn-primary btn-sm" title="Upload new profile image"><i class="bi bi-upload"></i></a>
				                              <a href="#" class="btn btn-danger btn-sm" title="Remove my profile image"><i class="bi bi-trash"></i></a>
				                            </div>
				                          </div>
				                        </div>
				                        <div class="row mb-3">
				                          <label for="name" class="col-md-4 col-lg-3 col-form-label">Full Name</label>
				                          <div class="col-md-8 col-lg-9">
				                            <input name="name" type="text" class="form-control" id="name" value="{{$user_data['name']}}">
				                          </div>
				                        </div>
				                        <!-- <div class="row mb-3">
				                          <label for="about" class="col-md-4 col-lg-3 col-form-label">About</label>
				                          <div class="col-md-8 col-lg-9">
				                            <textarea name="about" class="form-control" id="about" style="height: 100px">Sunt est soluta temporibus accusantium neque nam maiores cumque temporibus. Tempora libero non est unde veniam est qui dolor. Ut sunt iure rerum quae quisquam autem eveniet perspiciatis odit. Fuga sequi sed ea saepe at unde.</textarea>
				                          </div>
				                        </div> -->
				                        <div class="row mb-3">
				                          <label for="email" class="col-md-4 col-lg-3 col-form-label">Email</label>
				                          <div class="col-md-8 col-lg-9">
				                            <input name="email" type="text" class="form-control" id="email" value="{{$user_data['email']}}">
				                          </div>
				                        </div>

				                        <div class="row mb-3">
				                          <label for="phone" class="col-md-4 col-lg-3 col-form-label">Phone</label>
				                          <div class="col-md-8 col-lg-9">
				                            <input name="phone" type="call" class="form-control" id="phone" value="{{$user_data['phone']}}">
				                          </div>
				                        </div>

				                        <div class="row mb-3">
				                          <label for="company" class="col-md-4 col-lg-3 col-form-label">Company</label>
				                          <div class="col-md-8 col-lg-9">
				                            <input name="company" type="text" class="form-control" id="company" value="{{$user_data['company_name']}}">
				                          </div>
				                        </div>
				                        
				                        <div class="row mb-3">
				                          <label for="Country" class="col-md-4 col-lg-3 col-form-label">Country</label>
				                          <div class="col-md-8 col-lg-9">
				                            <input name="country" type="text" class="form-control" id="Country" value="{{$user_data['country']}}">
				                          </div>
				                        </div>
				                        <div class="row mb-3">
				                          <label for="post_code" class="col-md-4 col-lg-3 col-form-label">Post Code</label>
				                          <div class="col-md-8 col-lg-9">
				                            <input name="post_code" type="text" class="form-control" id="post_code" value="{{$user_data['post_code']}}">
				                          </div>
				                        </div>
				                        <div class="row mb-3">
				                          <label for="city" class="col-md-4 col-lg-3 col-form-label">City</label>
				                          <div class="col-md-8 col-lg-9">
				                            <input name="city" type="text" class="form-control" id="city" value="{{$user_data['city']}}">
				                          </div>
				                        </div>
				                        <div class="row mb-3">
				                          <label for="state" class="col-md-4 col-lg-3 col-form-label">State</label>
				                          <div class="col-md-8 col-lg-9">
				                            <input name="state" type="text" class="form-control" id="state" value="{{$user_data['state']}}">
				                          </div>
				                        </div>
				                        
                        				<div class="text-center">
                          					<button type="submit" class="btn btn-primary">Save Changes</button>
                        				</div>
                      				</form><!-- End Profile Edit Form -->
                    			</div>
                			
                			<div id="menu2" class="tab-pane fade">
                  			<!-- Settings Form -->
                  				<form>
				                    <div class="row mb-3">
				                      <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Current Password</label>
				                      <div class="col-md-8 col-lg-9">
				                        <input name="password" type="password" class="form-control" id="currentPassword">
				                      </div>
				                    </div>
				                    <div class="row mb-3">
				                      <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">New Password</label>
				                      <div class="col-md-8 col-lg-9">
				                        <input name="newpassword" type="password" class="form-control" id="newPassword">
				                      </div>
				                    </div>
				                    <div class="row mb-3">
				                      <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">Re-enter New Password</label>
				                      <div class="col-md-8 col-lg-9">
				                        <input name="renewpassword" type="password" class="form-control" id="renewPassword">
				                      </div>
				                    </div>
				                    <div class="text-center">
				                      <button type="submit" class="btn btn-primary">Change Password</button>
				                    </div>
				                </form><!-- End Change Password Form -->
                			</div><!-- End settings Form -->
                    	</div>
              		</div>
            	</div>
        	</div>
    	</section>
</div>
</div>
</main><!-- End #main -->

</body>

</html>
@endsection