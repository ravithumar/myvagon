<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/canvas2image@1.0.5/canvas2image.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCjorOISx346EPRCKse9h8K_fZmNnWZ00"></script> -->

<script type="text/javascript">
var markers = [
{
"title": 'CL 3105',
"lat": '21.1702',
"lng": '72.8311',
"description": 'Pozo inyector'
}, 
    {

"title": 'CL 3046',
"lat": '22.3072',
"lng": '73.1812',
"description": 'Productor 1'
},
    {

"title": 'CL 3046',
"lat": '23.0225',
"lng": '72.5714',
"description": 'Productor 1'
},
    {

"title": 'CL 3046',
"lat": '23.0711508',
"lng": '72.5177588',
"description": 'Productor 1'
},
    {

"title": 'CL 3046',
"lat": '22.9920',
"lng": '72.3773',
"description": 'Productor 1'
},
{
    
"title": 'CL 3046',
"lat": '23.0549589',
"lng": '72.5493202',
"description": 'Productor 1'
},
{
    
"title": 'CL 3046',
"lat": '23.0711508',
"lng": '72.5177588',
"description": 'Productor 1'
},
{
    
"title": 'CL 3046',
"lat": '23.0273356',
"lng": '72.507718',
"description": 'Productor 1'
},
/*{
    "title": 'CL 3046',
"lat": '37.9838',
"lng": '23.7275',
"description": 'Productor 1'
},
{
    "title": 'CL 3046',
"lat": '38.3159583',
"lng": '23.3006463',
"description": 'Productor 1'
}*/
    ];

window.onload = function () {
initMap();
scaleControl: false;
}
var map, mapOptions;
var service = new google.maps.DirectionsService(), poly;
         var directionsService = new google.maps.DirectionsService();
         var directionsDisplay = new google.maps.DirectionsRenderer();

function initMap() {
  const map = new google.maps.Map(document.getElementById("dvMap"), {
    zoom: 12,
    center: new google.maps.LatLng(
            parseFloat(markers[0].lat),
            parseFloat(markers[0].lng)),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    disableDoubleClickZoom: true,
    scrollwheel: false,
    draggableCursor: "crosshair",
    travelMode: google.maps.TravelMode,
  });
         

  const flightPlanCoordinates = [
    { lat: 21.1702, lng: 72.8311 },
    { lat: 22.3072, lng: 73.1812 },
    { lat: 23.0225, lng: 72.5177588 },
    { lat: 22.9920, lng: 72.3773 },
    { lat: 23.0711508, lng: 72.5177588 },
  ];
  var latlngbounds = new google.maps.LatLngBounds();
  directionsDisplay.setMap(map);
for (var i = 0; i < flightPlanCoordinates.length; i++) {
            var data = flightPlanCoordinates[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title,
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
            latlngbounds.extend(marker.position);
        }
  const flightPath = new google.maps.Polyline({map:map});
var request = {
            destination: new google.maps.LatLng(21.1702, 72.8311),
            origin: new google.maps.LatLng(23.0711508, 72.5177588),
            travelMode: "DRIVING",
        };
        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);  
            }
        });
  flightPath.setMap(map);
   var bounds = new google.maps.LatLngBounds();
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);

}
function Export() {
html2canvas($("#dvMap"), {
useCORS: true,
onrendered: function (canvas) {
 var myimage=canvas.toDataURL("image/jpeg");
                     canvas.getContext("2d");
                downloadURI(myimage, "google_map.jpeg");
$("#imgMap").show();
}
});
}
    function downloadURI(uri, name) {
    var link = document.createElement("a");
 
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();   
}


</script>
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td>
<div id="dvMap" style="width: 500px; height: 500px">
</div>
</td>
<td>
&nbsp; &nbsp;
</td>
<td>
<img id="imgMap" src="" alt="" style="display: none" />
</td>
</tr>
</table>
<br />
<input type="button" id="btnExport" value="Download" onclick="Export()" />